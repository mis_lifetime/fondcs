/* #REPORTNAME=ENG RFT By Date Report */
/* #VARNAM=trndte , #REQFLG=Y */
/* #VARNAM=trndte1, #REQFLG=Y */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding all RF activity by user */
/* #HELPTEXT= Requested by Engineering */
/* #GROUPS=NOONE */

set pagesize 50000
set linesize 160


ttitle left print_time -
center 'RFT By Date From &1 To &2 ' -
right print_date skip 2 -


column devcod heading 'Device Code'
column devcod format A30
column usr_id heading 'User'
column usr_id format  A20
column vehtyp heading 'Vehicle Type'
column vehtyp format A20
column rftmod heading 'RFT   '
column rftmod format A30
column trndte heading 'Date     '
column trndte format A24
column trnqty heading 'Qty'
column trnqty format 9999999999
column lodnum heading 'Count'
column lodnum format 9999999999
column oprcod heading 'Opr Code'
column oprcod format a9



alter session set nls_date_format ='dd-mon-yyyy HH24:MI:SS';


select
usr_id, sum(lodnum) lodnum
from
(select
dlytrn.usr_id,
count(distinct(dlytrn.lodnum)) lodnum
    FROM dlytrn, rftmst
       WHERE dlytrn.trndte between '&&1' and '&&2'
        AND  dlytrn.devcod              = rftmst.devcod
        AND  dlytrn.tostol like 'TRL%'
 group by dlytrn.usr_id
union
select
dlytrn.usr_id,
count(distinct(dlytrn.lodnum)) lodnum
    FROM dlytrn, rftmst
       WHERE dlytrn.trndte between '&&1' and '&&2'
        AND  dlytrn.devcod              = rftmst.devcod
        AND  dlytrn.frstol              like '%TEL%'
group by dlytrn.usr_id)
group by usr_id
/
