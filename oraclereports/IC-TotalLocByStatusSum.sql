
/* #REPORTNAME= IC-Total Locations By Status Summary Report */
/* #HELPTEXT= This report gives a summary of all locations in */
/* #HELPTEXT= Rack, Tower, GWall, ISTG and CFPP by status.  */
/* #HELPTEXT= Requested by Inventory Control             */

set pagesize 100
set linesize 400

ttitle left print_time -
center 'IC Total Locations By Status Report  ' -   
right print_date skip 2


/* Author: Al Driver */
/* Report provides a summary of locations.  I used a */
/* package in order to obtain the counts for each area. */
/* This report is similar to the IC-Empty Locations Summary Report. */
/* I used a union for the subtotals, grandtotal and */
/* different areas to have control over the order that */
/* they appear in the report. Using one script did */
/* not allow me to order the rows as desired */

column area heading 'Area'
column area format  A12 
column total heading 'Total'
column total format A6 
column available heading  'Available'
column available format A10 
column rework heading 'Rework'
column rework format A6 
column damaged heading 'Damaged'
column damaged format A7 
column rejects heading 'Rejects'
column rejects format A7 
column qcholds heading 'QCHolds'
column qcholds format A7 
column holds heading 'Holds'
column holds format A6 
column rownumbr heading 'Row#'
column rownumbr format 999
column others heading 'Others'
column others format A6 
column comb heading 'Combination'
column comb format A11
column aretyp heading 'Area Type'
column aretyp format A10

alter session set nls_date_format ='DD-MON-YYYY';

break on aretyp skip 1 


select 'RACK' aretyp,rownum rownumbr,substr(arecod,5,4) area,to_char(UsrLocStatusSum.usrGetAll(arecod)) total , 
to_char(UsrLocStatusSum.usrGetAvailable(arecod)) available,
to_char(UsrLocStatusSum.usrGetRework(arecod)) rework,
to_char(UsrLocStatusSum.usrGetDamaged(arecod)) damaged,
to_char(UsrLocStatusSum.usrGetRejects(arecod)) rejects,
to_char(UsrLocStatusSum.usrGetQCHolds(arecod)) qcholds,
to_char(UsrLocStatusSum.usrGetHolds(arecod)) holds,
to_char(UsrLocStatusSum.usrGetOthers(arecod)) others,
to_char(UsrLocStatusSum.usrGetComb(arecod)) comb
from aremst
where arecod ||''in ('RACKHIGH','RACKCOMP','RACKMED','RACKNC','RACKSPCK','RACKSRES') 
union
select 'RACK' aretyp,7 rownumbr,'RACK-TOTAL' area,to_char(UsrLocStatusSum.usrGetAllTotal('RACK')) total ,
to_char(UsrLocStatusSum.usrGetAvailTotal('RACK')) available,
to_char(UsrLocStatusSum.usrGetReworkTotal('RACK')) rework,
to_char(UsrLocStatusSum.usrGetDamagedTotal('RACK')) damaged,
to_char(UsrLocStatusSum.usrGetRejectsTotal('RACK')) rejects,
to_char(UsrLocStatusSum.usrGetQCHoldsTotal('RACK')) qcholds,
to_char(UsrLocStatusSum.usrGetHoldsTotal('RACK')) holds,
to_char(UsrLocStatusSum.usrGetOthersTotal('RACKS')) others,
to_char(UsrLocStatusSum.usrGetCombTotal('RACK')) comb
from dual
union
select 'TOWER' aretyp,rownum+7 rownumbr,substr(arecod,6,3) area,to_char(UsrLocStatusSum.usrGetAll(arecod)) total ,
to_char(UsrLocStatusSum.usrGetAvailable(arecod)) available,
to_char(UsrLocStatusSum.usrGetRework(arecod)) rework,
to_char(UsrLocStatusSum.usrGetDamaged(arecod)) damaged,
to_char(UsrLocStatusSum.usrGetRejects(arecod)) rejects,
to_char(UsrLocStatusSum.usrGetQCHolds(arecod)) qcholds,
to_char(UsrLocStatusSum.usrGetHolds(arecod)) holds,
to_char(UsrLocStatusSum.usrGetOthers(arecod)) others,
to_char(UsrLocStatusSum.usrGetComb(arecod)) comb
from aremst
where arecod ||''in ('TOWERFCC','TOWERFCP','TOWERREP')
union
select 'TOWER' aretyp,11 rownumbr,'TOWER-TOTAL' area,to_char(UsrLocStatusSum.usrGetAllTotal('TOWER')) total ,
to_char(UsrLocStatusSum.usrGetAvailTotal('TOWER')) available,
to_char(UsrLocStatusSum.usrGetReworkTotal('TOWER')) rework,
to_char(UsrLocStatusSum.usrGetDamagedTotal('TOWER')) damaged,
to_char(UsrLocStatusSum.usrGetRejectsTotal('TOWER')) rejects,
to_char(UsrLocStatusSum.usrGetQCHoldsTotal('TOWER')) qcholds,
to_char(UsrLocStatusSum.usrGetHoldsTotal('TOWER')) holds,
to_char(UsrLocStatusSum.usrGetOthersTotal('TOWER')) others,
to_char(UsrLocStatusSum.usrGetCombTotal('TOWER')) comb
from dual
union
select substr(arecod,1,5) aretyp,rownum +11 rownumbr,substr(arecod,1,5)||'-TOTAL' area,
to_char(UsrLocStatusSum.usrGetAll(arecod)) total ,
to_char(UsrLocStatusSum.usrGetAvailable(arecod)) available,
to_char(UsrLocStatusSum.usrGetRework(arecod)) rework,
to_char(UsrLocStatusSum.usrGetDamaged(arecod)) damaged,
to_char(UsrLocStatusSum.usrGetRejects(arecod)) rejects,
to_char(UsrLocStatusSum.usrGetQCHolds(arecod)) qcholds,
to_char(UsrLocStatusSum.usrGetHolds(arecod)) holds,
to_char(UsrLocStatusSum.usrGetOthers(arecod)) others,
to_char(UsrLocStatusSum.usrGetComb(arecod)) comb
from aremst
where arecod ||''in ('CFPP','GWALL') 
union
select 'ISTG' aretyp,14 rownumbr,'ISTG-TOTAL' area,to_char(UsrLocStatusSum.usrGetAll('ISTG')) total ,
to_char(UsrLocStatusSum.usrGetAvailIstg()) available,
to_char(UsrLocStatusSum.usrGetRewrkIstg()) rework,
to_char(UsrLocStatusSum.usrGetDamgdIstg()) damaged,
to_char(UsrLocStatusSum.usrGetRejctIstg()) rejects,
to_char(UsrLocStatusSum.usrGetQCHldIstg()) qcholds,
to_char(UsrLocStatusSum.usrGetHldIstg()) holds,
to_char(UsrLocStatusSum.usrGetOtherIstg()) others,
'0' comb
from dual
union
select 'GRAND' aretyp,15 rownumbr,'GRAND TOTAL' area,to_char(UsrLocStatusSum.usrGetGrandAllTotal()) total ,
to_char(UsrLocStatusSum.usrGetGrandAvailTotal()) available,
to_char(UsrLocStatusSum.usrGetGrandReworkTotal()) rework,
to_char(UsrLocStatusSum.usrGetGrandDamagedTotal()) damaged,
to_char(UsrLocStatusSum.usrGetGrandRejectsTotal()) rejects,
to_char(UsrLocStatusSum.usrGetGrandQCHoldsTotal()) qcholds,
to_char(UsrLocStatusSum.usrGetGrandHoldsTotal()) holds,
to_char(UsrLocStatusSum.usrGetGrandOthersTotal()) others,
to_char(UsrLocStatusSum.usrGetGrandCombTotal()) comb
from dual
order by 2 
/
