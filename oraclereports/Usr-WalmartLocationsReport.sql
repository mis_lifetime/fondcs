/* #REPORTNAME=Usr- Walmart Locations Report */
/* #HELPTEXT= This report lists the locations */
/* #HELPTEXT= in tower 12 that are deferred, */
/* #HELPTEXT= currently have a count in progress, */
/* #HELPTEXT= or where the location is in error. */

column stoloc heading 'Location'
column stoloc format a20
column type heading 'Type'
column type format a20
column prtnum heading 'Part Number'
column prtnum format a20

set pagesize 300
set linesize 100

select a.stoloc, 'Deferred' type, nvl(b.prtnum, ' ') prtnum
from cntwrk a, invsum b
where a.cntsts='D'
and b.stoloc like 'T12%'
and a.stoloc=b.stoloc(+)
union
select a.stoloc, 'In Progress' type, nvl(b.prtnum, ' ') prtnum
from locmst a, invsum b
where a.cipflg=1
and b.stoloc like 'T12%'
and a.stoloc=b.stoloc(+)
union
select a.stoloc, 'Error' type, nvl(b.prtnum, ' ') prtnum
from locmst a, invsum b
where a.locsts='I'
and a.stoloc like 'T12%'
and a.stoloc=b.stoloc(+);
