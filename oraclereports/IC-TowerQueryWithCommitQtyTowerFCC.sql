/* #REPORTNAME=IC TOWERFCC On-Hand Report */
/* #HELPTEXT= Requested by Inventory Control */

 ttitle left '&1' print_time center 'IC TOWERFCC On-Hand Report ' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno
select distinct invsum.stoloc, 
       invsum.prtnum,
       invdtl.lotnum,
       invsum.untqty,
       invsum.comqty,
       invsum.invsts
from invsum, invlod, invsub, invdtl
       where invsum.comqty > 0 
       and invsum.arecod like 'TOWER%'           
       and invsum.stoloc=invlod.stoloc
       and invsum.prtnum = invdtl.prtnum
       and invsum.prt_client_id = invdtl.prt_client_id
       and invdtl.subnum=invsub.subnum
       and invsub.lodnum=invlod.lodnum
       order by invsum.prtnum; 
