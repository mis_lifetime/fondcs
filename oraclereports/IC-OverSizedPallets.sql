
/* #REPORTNAME=IC Oversized Pallet Report */
/* #HELPTEXT = Requested by Engineering */


ttitle left print_time center 'IC Oversized Pallet Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column arecod heading 'Area'
column arecod format a12
column stoloc heading 'Location'
column stoloc format a12
column untqty heading 'Unit Qty'
column untqty format 999999
column comqty heading 'Com. Qty'
column comqty format 999999
column prtnum heading 'Item'
column prtnum format a12
column vc_itmtyp heading 'Item Type'
column vc_itmtyp format a11
column untcas heading 'Unit/Case'
column untcas format 999999
column untpal heading 'Unt. Pal'
column untpal format 999999
column height heading 'Height'
column height format 999999
column width heading 'Width'
column width format 999999
column length heading 'Length'
column length format 999999
column layer heading 'Case/Layer'
column layer format 999999
column caspal heading 'Cas/Pal'
column caspal format 999999
column pcntfull heading 'Percent Full'
column pcntfull format 999.99

select a.arecod, a.stoloc, a.untqty, a.comqty, a.prtnum, b.vc_itmtyp, b.untpal,
b.untcas, c.untwid width, c.unthgt height, c.untlen length, c.caslvl layer, (b.untpal/b.untcas) caspal, 
(a.untqty/b.untpal) pcntfull 
from invsum a, prtmst b, ftpmst c  where arecod
in ('RACKCOMP','RACKHIGH','RACKMED','RACKNC','RACKSPCK','RACKSRES')
and b.prtnum=a.prtnum
and b.prt_client_id = a.prt_client_id
and b.prt_client_id ='----'
and a.prtnum=c.ftpcod
and (a.untqty/b.untpal) > 1
/
