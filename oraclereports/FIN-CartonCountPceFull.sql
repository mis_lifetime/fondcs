/* #REPORTNAME=FIN-Carton Counts Summary Report */
/* #HELPTEXT= This report lists all carton count summary information by month */
/* #HELPTEXT= This information is broken down by Piece Pick and Full Case. */
/* #HELPTEXT= Please enter date in format dd-mon-yyyy   */
/* #HELPTEXT= Requested by Controller */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To,    #REQFLG=Y */


ttitle left '&1 ' print_time -
center 'FIN - Carton Counts Summary Report' -
right print_date skip 2

btitle skip1 center 'Page:' format 999 sql.pno

column shipped heading 'Shipped'
column shipped format a12
column piece heading 'Piece Pick Cartons'
column piece format 999999
column fullcase  heading 'Full Case Cartons'
column fullcase format 999999

alter session set nls_date_format = 'dd-mon-yyyy';

set linesize 200
set pagesize 300

select substr(shpdte,4,3) shipped, 
UsrCartonCounts.usrGetPP('&1','&2') piece, 
UsrCartonCounts.usrGetFC('&1','&2') fullCase
from usr_Carton_counts
where trunc(shpdte) between '&1' and '&2'
group by substr(shpdte,4,3)
/
