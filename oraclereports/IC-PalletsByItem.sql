/* #REPORTNAME=IC Pallets By Item Report */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding the total Pallet IDs */
/* #HELPTEXT= located in the Rack Areas by item. */
/* #HELPTEXT= Requested by Order Management */
/* #GROUPS=NOONE */

/* Author: Al Driver */
/* Report uses the inventory tables to validate */
/* pallets found in locations in any Rack area. */
/* Counts lodnums to obtain the total amount of */
/* pallet ids, per area, per item. */
/* This report was created to help configure the */
/* different storage areas in the warehouse. */

ttitle left print_time center 'Pallets By Item Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column prtnum format a12
column vc_itmcls heading 'Prod Class'
column vc_itmcls format a10
column arecod heading 'Rack Area'
column arecod format a20
column lodnum heading 'Total PID''s'
column lodnum format 999999999

set linesize 200
set pagesize 10000

SELECT prtmst.prtnum, prtmst.vc_itmcls,invsum.arecod,count(distinct invlod.lodnum) lodnum
    from prtmst,invsum,invlod,invsub,invdtl
    where prtmst.prtnum = invsum.prtnum
    and prtmst.prt_client_id = invsum.prt_client_id
    and prtmst.prtnum = invdtl.prtnum
    and prtmst.prt_client_id = invdtl.prt_client_id
    and invsum.stoloc = invlod.stoloc
    and invsum.prtnum = invdtl.prtnum
    and invsum.prt_client_id = invdtl.prt_client_id
    and invlod.lodnum = invsub.lodnum
    and invsub.subnum = invdtl.subnum
    and substr(invsum.arecod,1,4) = ('RACK') 
group by prtmst.prtnum, prtmst.vc_itmcls,invsum.arecod
/
