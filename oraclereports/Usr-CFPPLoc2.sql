/* #REPORTNAME=User  CFPP by LOCATION Report */
/* #HELPTEXT= Requested by Order Management */

 ttitle left '&1' print_time center 'User  CFPP Report ' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

select invsum.stoloc, 
       invsum.prtnum, 
       invdtl.lotnum,
       invsum.untqty,
       invsum.comqty,
       (invsum.untqty - invsum.comqty) avlqty
  from invsum, invlod, invsub, invdtl  
 where invsum.arecod ||'' = 'CFPP'  
 and invsum.stoloc = invlod.stoloc
 and invsum.prtnum = invdtl.prtnum
 and invsum.prt_client_id = invdtl.prt_client_id
 and invdtl.subnum=invsub.subnum
 and invsub.lodnum=invlod.lodnum
 order by invsum.stoloc
/
