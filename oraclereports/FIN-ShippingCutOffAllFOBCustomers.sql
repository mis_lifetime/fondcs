/* #REPORTNAME= FIN- Shipping Cut-Off All FOB Customers Report */
/* #HELPTEXT= Report provides all shipping info for American Sales, */
/* #HELPTEXT= Costco, Winn Dixie, Pathmark, M Block, */ 
/* #HELPTEXT= Raleys, Fred Meyer and Kitchen Collection. */
/* #VARNAM=Begin-Date , #REQFLG=Y */
/* #VARNAM=End-Date , #REQFLG=Y */

set pagesize 200 
set linesize 300  


ttitle left print_time -
center 'FIN - Shipping Cut-Off All FOB Customers Report between &&1 and &&2 ' -   
right print_date skip 2 


column ship_id heading 'Ship ID'
column ship_id format a30
column dispatch_dte heading 'Ship Date'
column dispatch_dte format a11
column adrnam heading 'Customer Name'
column adrnam format a40 
column cponum heading 'PO Number'
column cponum format a30
column btcust heading 'Customer #'
column btcust format a20
column dollars heading '$s Shipped'
column dollars format 999999999.99
column carcod heading 'Carrier'
column carcod format a10
column frgt_terms heading 'Frgt Terms'
column frgt_terms format a10
column doc_num heading 'Waybill #'
column doc_num format a20
column traknm heading 'Tracking #'
column traknm format a30 
column srvlvl heading  'PP or Col'
column srvlvl format a10 
column division heading 'Division'
column division format a5
column ctry_name heading 'Country'
column ctry_name format a20

alter session set nls_date_format ='dd-mon-yyyy';

select ship_id,dispatch_dte,adrnam,btcust,cponum,sum(vc_cust_untcst * untqty) dollars,carcod,frgt_terms,traknm,
doc_num,srvlvl, division, ctry_name from
(select distinct shipment.ship_id,trunc(dispatch_dte) dispatch_dte ,adrmst.adrnam,btcust,cponum,ord_line.vc_cust_untcst,i.untqty,
shipment.carcod,
             decode(shipment.srvlvl,'CC','X',null) frgt_terms,
              nvl(manfst.traknm,nvl(cr.track_num,stop.track_num)) traknm, shipment.doc_num,
               shipment.srvlvl,i.dtlnum, p.division, a.ctry_name
                                      from invdtl i,adrmst, prtmst, manfst,shipment, shipment_line,  ord, ord_line,
                                      stop, trlr tr,car_move cr, usr_itemdata p, adrmst a
                                      where btcust = adrmst.host_ext_id
				      and stcust = a.host_ext_id
                                      and prtmst.prtnum = ord_line.prtnum
                                      AND prtmst.prtnum               = p.prtnum
                                      AND ord.ordnum                  = ord_line.ordnum
                                      AND ord.client_id               = ord_line.client_id
                                      AND ord_line.client_id          = shipment_line.client_id
                                      AND ord_line.ordnum             = shipment_line.ordnum
                                      AND ord_line.ordlin             = shipment_line.ordlin
                                      AND ord_line.ordsln             = shipment_line.ordsln
                                      AND shipment_line.ship_id       = shipment.ship_id
                                      AND shipment_line.ship_line_id  = i.ship_line_id
                                      AND shipment.srvlvl             = 'PP'
                                      AND i.subnum            = manfst.subnum(+)
                                      /* AND i.dtlnum            = manfst.dtlnum(+) */
                                      /* AND i.prtnum                    = manfst.prtnum(+) */
                                      AND shipment.stop_id                  = stop.stop_id
                                      AND stop.car_move_id            = cr.car_move_id
                                      AND cr.trlr_id                  = tr.trlr_id
                                      AND shipment.shpsts  ||''= 'C'
                                      AND trunc(dispatch_dte) between '&&1' and '&&2'
                                      AND btcust in (select btcust from usr_fob))
group by ship_id,dispatch_dte,adrnam,btcust,cponum,carcod,frgt_terms,traknm,
doc_num,srvlvl,division, ctry_name
/
