/* #REPORTNAME=User List Open Scan Station Pallets */ 
/* #HELPTEXT= List the pallets ID, Shipment ID, and added */ 
/* #HELPTEXT= date for all pallets at scan stations */ 
ttitle left print_time -
		center 'User List Open Scan Station Pallets Report' -
		right print_date skip 1 - 
		center print_date skip 2

btitle skip1 center 'Page:' format 999 sql.pno

column adddte heading 'Date Created'
column adddte format a16
column lstdte heading 'Listed Date'
column lstdte format a16
column pallet heading 'Pallet'
column pallet format a20
column shipid heading 'Ship ID'
column shipid format a14
column stoloc heading 'Scan Station'
column stoloc format a20
column stname heading 'Customer'
column stname format a20

alter session set nls_date_format ='dd-mon-yyyy';

select distinct (rtrim(invlod.lodnum)) pallet, 
trunc(invdtl.adddte) adddte ,
trunc(invdtl.lstdte) lstdte, 
rtrim(invlod.stoloc) stoloc, 
rtrim(sd.ship_id) shipid ,
rtrim( substr(adrmst.adrnam,1,10))  stname
from ord sh, shipment_line sd, invdtl, invsub, invlod, locmst lm, ord_line, adrmst 
where substr(invlod.stoloc,(instr(invlod.stoloc,'V')),1) !='V' and 
invlod.stoloc = lm.stoloc 
and invlod.lodnum = invsub.lodnum
and invsub.subnum=invdtl.subnum
and invdtl.ship_line_id=sd.ship_line_id
and sd.client_id=ord_line.client_id
and sd.ordnum=ord_line.ordnum
and sd.ordlin=ord_line.ordlin
and sd.ordsln=ord_line.ordsln
and sd.ordnum=sh.ordnum
and sh.client_id='----'
and sh.bt_adr_id=adrmst.adr_id
and lm.arecod in ('PALLET','OPALLET')
and (lm.stoloc like 'LANE%' or lm.stoloc = 'PRESCAN')
order by rtrim(invlod.stoloc), rtrim(sd.ship_id), trunc(invdtl.adddte),
trunc(invdtl.lstdte), rtrim( substr(adrmst.adrnam,1,10)) ;
