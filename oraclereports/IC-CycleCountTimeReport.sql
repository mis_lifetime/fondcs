/* #REPORTNAME=IC - Cycle Count Time Report */
/* #HELPTEXT = Provides information on the amount */
/* #HELPTEXT = of cycle counting done by each user. */
/* #HELPTEXT = Enter date in DD-MON-YYYY form. */
/* #VARNAM=CountDate , #REQFLG=Y */


column ord heading 'Order'
column ord format a5
column usr_id heading 'User ID'
column usr_id format a30
column min heading 'First Count'
column min format date
column max heading 'Last Count'
column max format date
column count heading 'Counts'
column count format 999999
column time heading 'Time Difference'
column time format a30

alter session set nls_date_format = 'DD-MON-YYYY HH:MI:SS AM';
set linesize 150
set pagesize 100


select ord, usr_id, min, max, count, substr(time,1,30) time
from 
(select '1' ord, 'Shift Start' usr_id, null min, null max, null count, null time
from dual
union
select '2' ord, usr_id, min(trndte) min, max(trndte) max, count(frstol) count, (floor(((max(trndte)-min(trndte))*24*60*60)/3600)
|| ' HOURS ' ||
floor((((max(trndte)-min(trndte))*24*60*60) -
floor(((max(trndte)-min(trndte))*24*60*60)/3600)*3600)/60)
|| ' MINUTES ' ||
round((((max(trndte)-min(trndte))*24*60*60) -
floor(((max(trndte)-min(trndte))*24*60*60)/3600)*3600 -
(floor((((max(trndte)-min(trndte))*24*60*60) -
floor(((max(trndte)-min(trndte))*24*60*60)/3600)*3600)/60)*60) ))
|| ' SECS ') time
from dlytrn
where oprcod='CNT'
and trndte between to_date('&&1'||' 07:30:00 AM') and to_date('&&1'||' 09:48:00 AM')
group by usr_id
union
select '3' ord, 'After First Break' usr_id, null min, null max, null count, null time
from dual
union
select '4' ord, usr_id, min(trndte) min, max(trndte) max, count(frstol) count, (floor(((max(trndte)-min(trndte))*24*60*60)/3600)
|| ' HOURS ' ||
floor((((max(trndte)-min(trndte))*24*60*60) -
floor(((max(trndte)-min(trndte))*24*60*60)/3600)*3600)/60)
|| ' MINUTES ' ||
round((((max(trndte)-min(trndte))*24*60*60) -
floor(((max(trndte)-min(trndte))*24*60*60)/3600)*3600 -
(floor((((max(trndte)-min(trndte))*24*60*60) -
floor(((max(trndte)-min(trndte))*24*60*60)/3600)*3600)/60)*60) ))
|| ' SECS ') time
from dlytrn
where oprcod='CNT'
and trndte between to_date('&&1'||' 10:03:00 AM') and to_date('&&1'||' 12:35:00 PM')
group by usr_id
union
select '5' ord, 'After Lunch' usr_id, null min, null max, null count, null time
from dual
union
select '6' ord, usr_id, min(trndte) min, max(trndte) max, count(frstol) count, (floor(((max(trndte)-min(trndte))*24*60*60)/3600)
|| ' HOURS ' ||
floor((((max(trndte)-min(trndte))*24*60*60) -
floor(((max(trndte)-min(trndte))*24*60*60)/3600)*3600)/60)
|| ' MINUTES ' ||
round((((max(trndte)-min(trndte))*24*60*60) -
floor(((max(trndte)-min(trndte))*24*60*60)/3600)*3600 -
(floor((((max(trndte)-min(trndte))*24*60*60) -
floor(((max(trndte)-min(trndte))*24*60*60)/3600)*3600)/60)*60) ))
|| ' SECS ') time
from dlytrn
where oprcod='CNT'
and trndte between to_date('&&1'||' 01:03:00 PM') and to_date('&&1'||' 02:48:00 PM')
group by usr_id
union
select '7' ord, 'After Second Break' usr_id, null min, null max, null count, null time
from dual
union
select '8' ord, usr_id, min(trndte) min, max(trndte) max, count(frstol) count, 
(floor(((max(trndte)-min(trndte))*24*60*60)/3600)
|| ' HOURS ' ||
floor((((max(trndte)-min(trndte))*24*60*60) -
floor(((max(trndte)-min(trndte))*24*60*60)/3600)*3600)/60)
|| ' MINUTES ' ||
round((((max(trndte)-min(trndte))*24*60*60) -
floor(((max(trndte)-min(trndte))*24*60*60)/3600)*3600 -
(floor((((max(trndte)-min(trndte))*24*60*60) -
floor(((max(trndte)-min(trndte))*24*60*60)/3600)*3600)/60)*60) ))
|| ' SECS ') time
from dlytrn
where oprcod='CNT'
and trndte between to_date('&&1'||' 03:03:00 PM') and to_date('&&1'||' 11:59:59 PM')
group by usr_id)
order by ord;
