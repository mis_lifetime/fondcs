/* #REPORTNAME=IC Tower Unassigned Locations Report */
/* #HELPTEXT= This report lists all locations in TowerFCP*/
/* #HELPTEXT= that are unassigned, including empty locations */
/* #HELPTEXT= Requested by Inventory Control             */


/* Report previously worked off of the usr_ictowerloc and */
/* usr_storageloc tables, which had specific locations */
/* hardcoded into them.  It was later suggested to have */
/* the report show all locations in TowerFCP that are */
/* unassigned (asgflg =0).  */


 ttitle left '&1' print_time center 'IC Tower Unassigned Locations Report ' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column stoloc heading 'Location'
column stoloc format a15
column prtnum heading 'Item'
column prtnum format a12
column untqty heading 'Unit Qty'
column untqty format 999999
column comqty heading 'Comm. Qty'
column comqty format 999999
column pndqty heading 'Pend. Qty'
column pndqty format 999999
column arecod heading 'Area'
column arecod format a10
column asgflg heading 'Assigned'
column asgflg format a8
column invsts heading 'Status'
column invsts format a6

select distinct invsum.arecod, locmst.stoloc,
       invsum.prtnum,
       invsum.untqty,
       invsum.comqty,
       invsum.pndqty,
       invsum.invsts,decode(locmst.asgflg,1,'Y','N') asgflg
  from invsum,  locmst
       where
       invsum.arecod in ('TOWERFCP')
       and invsum.stoloc = locmst.stoloc
       and asgflg = (0) 
union  /* also show any locations that are unassigned and have nothing pending as empty */
select distinct locmst.arecod,locmst.stoloc,
       NULL,0,0,locmst.pndqvl,
       decode(locmst.locsts,'E','EMPTY'),NULL
 from locmst
       where locmst.arecod = ('TOWERFCP')
       and locmst.locsts = 'E'
       and locmst.asgflg = (0)
       and locmst.pndqvl = (0)
       order by 2
/




