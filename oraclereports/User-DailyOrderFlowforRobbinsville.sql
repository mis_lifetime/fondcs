/* #REPORTNAME= User Daily Order Flow for Robbinsville Report */
/* #HELPTEXT= Report returns the dollar value of all */
/* #HELPTEXT= orders received today for the given */
/* #HELPTEXT= time range. Enter todays date in the */
/* #HELPTEXT= format DD-MON-YYYY HH:MI:SS AM or PM. */
/* #HELPTEXT= Date entered must be todays date. */
/* #HELPTEXT=  Requested by the Controller */ 
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To , #REQFLG=Y */

--Author: AL Driver

ttitle left print_time -
       center 'User Daily Order Flow for Robbinsville Report' -
       right print_date skip 2 -

column begin heading "FROM"
column begin format a25
column end heading "TO"
column end format a25
column cst heading "DOLLARS"
column cst format 999999999.90

alter session set nls_date_format = 'DD-MON-YYYY HH:MI:SS AM';

set linesize 100

select '&&1' begin, '&&2' end, decode(sum(vc_cust_untcst * ordqty),NULL,'INVALID DATE RANGE',sum(vc_cust_untcst * ordqty)) cst
              from ord, ord_line
            where ord.ordnum = ord_line.ordnum
            and ord.client_id = ord_line.client_id
            and adddte between '&&1' and '&&2'
            and substr(sysdate,1,11) = substr('&&1',1,11)  -- Date range must be todays date
            and substr(sysdate,1,11) = substr('&&2',1,11)
            and early_shpdte between trunc(ADD_MONTHS(LAST_DAY(sysdate),-1)+1) and -- Ship Date must be present month
               trunc(LAST_DAY(sysdate))+.99999
/
