/* #REPORTNAME=ENG Monthly Ticketing Report */
/* #VARNAM=begin_month , #REQFLG=Y */
/* #VARNAM=end_month , #REQFLG=Y */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding Ticketing by month. */
/* #HELPTEXT= Report lists item#, qty and customer. */
/* #HELPTEXT= Required date format is MON-YYYY */
/* #HELPTEXT= Requested by Engineering */
/* #GROUPS=NOONE */

set pagesize 2000
set linesize 100


ttitle left print_time -
center 'ENG Monthly Ticketing Report from &&1 to &&2 ' -
right print_date skip 2 -

column prtnum heading 'Item#'
column prtnum format a30
column trnqty heading 'Qty'
column trnqty format 9999999999
column adrnam heading 'Customer'
column adrnam format a40

alter session set nls_date_format ='dd-mon-yyyy HH:MI:SS AM';


select d.prtnum, sum(trnqty) trnqty, x.adrnam
                                 from adrmst x,ord a,ord_line b,shipment_line c,invdtl i,dlytrn d
                                 where x.host_ext_id = a.btcust
                                 and x.client_id = a.client_id
                                 and a.ordnum = b.ordnum
                                 and a.client_id = b.client_id
                                 and b.ordnum = c.ordnum
                                 and b.ordlin = c.ordlin
                                 and b.client_id = c.client_id
                                 and b.ordsln = c.ordsln
                                 and c.ship_line_id = i.ship_line_id
                                 and i.subnum = d.subnum
                                 and d.tostol like 'TICKET%'
  and d.trndte ||'' between to_date('01-'||'&&1'||' 12:00:00 AM') and LAST_DAY(to_date('01-'||'&&2'||' 11:59:59 PM'))
 group by d.prtnum, x.adrnam
union
select d.prtnum, sum(trnqty) trnqty, x.adrnam
                                 from adrmst@arch x,ord@arch a,ord_line@arch b,shipment_line@arch c,invdtl@arch i,dlytrn@arch d
                                 where x.host_ext_id = a.btcust
                                 and x.client_id = a.client_id
                                 and a.ordnum = b.ordnum
                                 and a.client_id = b.client_id
                                 and b.ordnum = c.ordnum
                                 and b.ordlin = c.ordlin
                                 and b.client_id = c.client_id
                                 and b.ordsln = c.ordsln
                                 and c.ship_line_id = i.ship_line_id
                                 and i.subnum = d.subnum
                                 and d.tostol like 'TICKET%'
  and d.trndte ||'' between to_date('01-'||'&&1'||' 12:00:00 AM') and LAST_DAY(to_date('01-'||'&&2'||' 11:59:59 PM'))
 group by d.prtnum, x.adrnam
/ 
