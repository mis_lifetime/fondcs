/* #REPORTNAME=IC - Multi Status Locations */
/* #HELPTEXT= This report provides invoformation on */
/* #HELPTEXT= locations that have more then one status  */
/* #HELPTEXT= of an item in it. */

column stoloc heading 'Location'
column stoloc format a20
column lodnum heading 'Load Number'
column lodnum format a21
column prtnum heading 'Part Number'
column prtnum format a20
column invsts heading 'Status'
column invsts format a20
column adddte heading 'Add Date'
column adddte format date

set linesize 200
set pagesize 100

alter session set nls_Date_format ='DD-MON-YYYY';

SELECT il.stoloc, il.lodnum, id.prtnum, 
       ( SELECT SUBSTR(d.lngdsc, 1, 20 ) 
	       FROM DSCMST d 
		  WHERE d.colval = id.invsts 
		    AND d.colnam = 'invsts' 
			AND d.locale_id='US_ENGLISH') invsts, id.adddte
  FROM(
     SELECT loc.stoloc, dtl.prtnum 
       FROM LOCMST loc, INVLOD lod, INVSUB sub, INVDTL dtl
      WHERE dtl.subnum = sub.subnum
        AND sub.lodnum = lod.lodnum
        AND lod.stoloc = loc.stoloc
        AND loc.arecod IN (
           'RACKSPCK','RACKHIGH','RACKMED',
           'RACKSRES','RACKCOMP','RACKNC',
           'TOWERREP','TOWERFCP','TOWERFCC',
           'CFPP','GWALL','ISTG' )
      GROUP BY loc.stoloc, dtl.prtnum HAVING COUNT(DISTINCT(dtl.invsts)) > 1 ) woo, INVLOD il, INVSUB isb, INVDTL id
	 WHERE il.stoloc = woo.stoloc
	   AND isb.lodnum = il.lodnum
	   AND id.subnum = isb.subnum
	   AND id.prtnum = woo.prtnum
/
