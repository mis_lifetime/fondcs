/* #REPORTNAME=ENG Discontinued Items Report */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding discontinued items */
/* #HELPTEXT= Requested by Engineering */
/* #GROUPS=NOONE */


ttitle left print_time center 'ENG Discontinued Items Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column prtnum format a12
column lngdsc heading 'Description'
column lngdsc format a30
column vc_prdcod heading 'Prod Code'
column vc_prdcod format a10
column lodnum heading 'Load Number'
column lodnum format a22
column stoloc heading 'Location'
column stoloc format a20
column arecod heading 'Area'
column arecod format a10
column untqty heading 'Qty'
column untqty format 999999999
column adddte heading 'Date'
column adddte format a15
column costs heading 'Cost'
column costs format 99,999,999.99

set pagesize 4000
set linesize 200

select im.prtnum, substr(pd.lngdsc,1,30) lngdsc,  im.stoloc, im.arecod, pm.vc_prdcod, sum(invdtl.untqty) untqty, 
sum(invdtl.untqty * nvl(pm.untcst,0)) costs
  from invdtl,invsub,invlod,invsum im, prtmst pm, prtdsc pd
  where invdtl.subnum = invsub.subnum
  and invsub.lodnum = invlod.lodnum
  and im.stoloc = invlod.stoloc
  and invdtl.prtnum = im.prtnum
  and invdtl.prt_client_id = im.prt_client_id
  and im.prtnum =pm.prtnum
  and im.prt_client_id = pm.prt_client_id
  and im.prtnum||'|----' = pd.colval
  and pd.colnam = 'prtnum|prt_client_id'
  and pd.locale_id = 'US_ENGLISH'
  and substr(pm.vc_prdcod,2,1) ='X'
  and pm.prt_client_id ='----'
  and im.stoloc not like '%PERM%'
  and im.arecod not in ('SHIP','SSTG')
  group by im.prtnum, im.arecod, im.stoloc,pm.vc_prdcod, substr(pd.lngdsc,1,30)
/
