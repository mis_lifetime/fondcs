
/* #REPORTNAME=User Item Report */
/* #VARNAM=prtnum , #REQFLG=Y */
/* #HELPTEXT= Please enter item and onhand, committed  */
/* #HELPTEXT= case pack , unit pack and location will be displayed.  */
/* #HELPTEXT = Requested by Production */

ttitle left print_time center 'User Item Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column stoloc heading 'Location'
column stoloc format a20
column prtnum heading 'Item'
column prtnum format a14
column untqty heading 'On Hand'
column untqty format 999999
column comqty heading 'Committed'
column comqty format 999999
column untcas heading 'Case Pack'
column untcas format 999999
column untpak heading 'Unit Pack'
column untpak format 999999


 
set linesize 200
set pagesize 1000

select stoloc, prtnum,
untqty, comqty, untcas, untpak
from invsum
where prtnum ='&1'
order by prtnum 
/
