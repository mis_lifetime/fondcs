/* #REPORTNAME=User  Lotted Report */
/* #HELPTEXT= This report displays the item, lot code, available quantity, */
/* #HELPTEXT= committed quantity and pending quantity. */
/* #HELPTEXT=  Please note that you need to enter the lot code.*/
/* #HELPTEXT= Requested by Production and Customer Service */

/* #VARNAM=lotnum , #REQFLG=Y */

 ttitle left '&1' print_time center 'User  Lotted Report' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a12
column lotnum heading 'Lot Num.'
column lotnum format a20
column avlqty heading 'Available'
column avlqty format 999999
column comqty heading 'Comm. Qty'
column comqty format 999999
column pndqty heading 'Pending'
column pndqty format 999999

set pagesize 300
set linesize 200

select 
       usr_prtqty_view_sum.prtnum,
       usr_prtqty_view_sum.lotnum,
       sum(usr_prtqty_view_sum.avlqty) avlqty,
       sum(usr_prtqty_view_sum.comqty) comqty,
       sum(usr_prtqty_view_sum.pndqty) pndqty
 from usr_prtqty_view_sum
       where usr_prtqty_view_sum.lotnum ='&1'
group by usr_prtqty_view_sum.prtnum,usr_prtqty_view_sum.lotnum
/
