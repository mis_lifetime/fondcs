/* #REPORTNAME = IC Archived Daily Transaction Report */
/* #HELPTEXT = Date format to be entered as dd-mon-yyyy */
/* #HELPTEXT = Requested by Inventory Control */
/* #VARNAM=prtnum , #REQFLG=Y */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To , #REQFLG=Y */


ttitle left  print_time -
       center 'IC Archived Daily Transaction Report' -
       right print_date skip 1 -
       center 'Item: ' &1 skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

alter session set nls_date_format='dd-mon-yyyy';

column prtnum heading 'Part Number'
column prtnum format a12
column trndte heading 'Trn. Date'
column trndte format a12
column lodnum heading 'Load #'
column lodnum format a20
column subnum heading 'Sub Load'
column subnum format a15
column dtlnum heading 'Det. #'
column dtlnum format a15
column lotnum heading 'Lot Code'
column lotnum format a7
column movref heading 'Move Ref.'
column movref format a25
column reacod heading 'Reason Code'
column reacod format a25
column trnqty heading 'Tran. Qty'
column trnqty format 999999
column frstol heading 'From Location'
column frstol format a20
column tostol heading 'To Location'
column tostol format a20
column usr_id heading 'User'
column usr_id format a10
column devcod heading 'Device Code'
column devcod format a10

set pagesize 5000;
set linesize 600;


SELECT
prtnum,
trndte,
lodnum,
subnum,
dtlnum,
lotnum,
movref,
reacod,
trnqty,
frstol,
tostol,
usr_id,
devcod
from dlytrn@arch
where prtnum='&1'
and trunc(trndte) between '&2' and '&3'
/

