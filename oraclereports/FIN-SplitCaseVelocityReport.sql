/* #REPORTNAME=FIN-Split Case Velocity Report */
/* #HELPTEXT= This report lists all items velocity,  */
/* #HELPTEXT= that are located in the split case area. */
/* #HELPTEXT= Report lists item, location, velocity and case size. */ 
/* #HELPTEXT= Requested by Controller.*/


 ttitle left  print_time center 'FIN - Split Case Velocity Report ' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item#'
column prtnum format a30
column arecod heading 'Area'
column arecod format a10
column stoloc heading 'Location'
column stoloc format a20
column velzon heading 'Velocity'
column velzon format a10
column caslen heading 'Case Length'
column caslen format 99999.99
column caswid heading 'Case Width'
column caswid format 99999.99
column cashgt heading 'Case Height'
column cashgt format 99999.99

set linesize 200
set pagesize 5000


SELECT distinct prtmst.prtnum, invsum.arecod,invsum.stoloc,
    decode(velzon,'A','FAST','B','MEDIUM','C','SLOW','D','DEAD','UNKNOWN') velzon,
           caslen,caswid,cashgt
            from ftpmst,prtmst,invsum,invlod,invsub,invdtl
            where invsum.arecod ||''= 'CFPP'
            and  ftpmst.ftpcod = prtmst.ftpcod
            and prtmst.prtnum = invdtl.prtnum
            and prtmst.prt_client_id = invdtl.prt_client_id
            and invdtl.subnum = invsub.subnum
            and invsub.lodnum = invlod.lodnum
            and invlod.stoloc = invsum.stoloc           
/
