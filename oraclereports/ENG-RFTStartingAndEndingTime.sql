/* #REPORTNAME=ENG RFT Starting and Ending Time Report */
/* #VARNAM=trndte , #REQFLG=Y */
/* #VARNAM=trndte1 , #REQFLG=Y */

/* #HELPTEXT= This report lists all beginning and  */
/* #HELPTEXT= ending times for all RF activity by user */
/* #HELPTEXT= Date should be entered in dd-mon-yyyy format */
/* #HELPTEXT= Requested by Engineering - October 2002 */
/* #GROUPS=NOONE */

set pagesize 50000
set linesize 160


ttitle left print_time -
left 'RFT Starting and Ending Time Between   &1  and &2' -
right print_date skip 2 -


column devcod heading 'Device Code'
column devcod format A30
column usr_id heading 'User'
column usr_id format  A20
column trndte heading 'Started     '
column trndte format A30
column finish heading 'Finished'
column finish format A30
column trnqty heading 'Qty'

alter session set nls_date_format ='dd-mon-yyyy HH24:MI:SS';


select
min(dlytrn.trndte) trndte, max(dlytrn.trndte) finish,
dlytrn.usr_id , dlytrn.devcod from dlytrn where devcod like 'TEL%'
and trndte between '&1' and '&2'
group by usr_id, devcod
/
