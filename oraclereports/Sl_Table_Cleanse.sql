-- Script cleans out old data from appropriate sl_ tables.
-- Should run each Sunday only and leave 3 weeks worth of data

delete from sl_dwnld
where trunc(dwnld_dt) < trunc(sysdate) -21
and rtrim(to_char(to_date(sysdate,'DD-MON-YY'),'Day')) = 'Sunday';

commit;  

delete from sl_evt_arg_data
where trunc(ins_dt) < trunc(sysdate) -21
and rtrim(to_char(to_date(sysdate,'DD-MON-YY'),'Day')) = 'Sunday';

--and rownum < 250000;

commit;  

delete from sl_eo_data_dtl
where trunc(ins_dt) < trunc(sysdate) -21
and rtrim(to_char(to_date(sysdate,'DD-MON-YY'),'Day')) = 'Sunday';

--and rownum < 250000;

commit;  

delete from sl_evt_data
where trunc(evt_dt) < trunc(sysdate) -21
and rtrim(to_char(to_date(sysdate,'DD-MON-YY'),'Day')) = 'Sunday';

--and rownum < 250000;

commit;  
