/* This report lists all inventory information in DCS  */
/* Report created for Inventory Control */

column prtnum heading 'Item'
column prtnum format  a20
column lngdsc heading 'Description'
column lngdsc format a30
column invsts heading 'Status'
column invsts format a6
column avail heading 'Available'
column avail format 9,999,990
column vc_prdcod heading 'Product/|Class'
column vc_prdcod format a6
column pending heading 'Pending'
column pending format 9,999,990
column vc_itmcls heading 'Item/|Class'
column vc_itmcls format a8
column vc_itmtyp heading 'Type'
column vc_itmtyp format a8
column untcst heading 'Cost'
column untcst format 999,999.000
column extcst heading 'Extended'
column extcst format 999,999.00

set linesize 200
set pagesize 50000

select a.prtnum, a.invsts, substr(c.lngdsc,1,30) lngdsc, sum(a.avlqty) avail,
sum(a.pndqty) pending, b.vc_prdcod, b.vc_itmtyp, b.vc_itmcls, 
b.untcst, sum(a.avlqty)*b.untcst extcst
from var_prtqty_view_dtl  a, prtmst b, prtdsc c
where
a.prtnum=b.prtnum
and b.prt_client_id = '----'
and a.prtnum||'|----' = c.colval
and c.colnam = 'prtnum|prt_client_id'
and c.locale_id ='US_ENGLISH'
group by a.prtnum, a.invsts, c.lngdsc,
b.vc_prdcod, b.vc_itmtyp, b.vc_itmcls, b.untcst
/
