
/* #REPORTNAME=IC Incorrect  Pallets By Area Report */

/* #VARNAM=arecod , #REQFLG=Y */

/* #HELPTEXT= This report lists the counts for areas and part family */
/* #HELPTEXT= Requested by Engineering */


ttitle left print_time center 'IC Incorrect  Pallets By Area Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column lodnum heading 'Location '
column lodnum format a20
column prtfam heading 'Part Family'
column prtfam format a12
column stoloc heading 'Location'
column stoloc format a12
column invsts heading 'Stat.'
column invsts format a5
column lotnum heading 'Lot'
column lotnum format a5
column untqty heading 'Qty'
column untqty format 999999
column prtnum heading 'Item'
column prtnum format a12

select il.lodnum, il.stoloc, pm.prtfam, id.invsts, sum(id.untqty) untqty, id.lotnum, id.prtnum,
decode(iv.comqty ,0, 'N','Y') committed
from locmst lm, aremst am,
invlod il, invsub isb, invdtl id, prtmst pm, invsum iv
where pm.prtnum=id.prtnum
and id.prtnum = iv.prtnum
and iv.prt_client_id = '----'
and iv.arecod =  am.arecod
and il.stoloc = iv.stoloc
and id.subnum=isb.subnum
and isb.lodnum=il.lodnum
and il.stoloc=lm.stoloc
and lm.arecod = '&1'
group by il.lodnum, il.stoloc, pm.prtfam, id.invsts, id.prtnum, id.lotnum, iv.comqty
/
