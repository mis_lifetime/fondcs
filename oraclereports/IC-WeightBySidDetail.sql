
set linesize 300
set pagesize 50000


/* #REPORTNAME=IC Weight By Sid Detail Report */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding weight */
/* #HELPTEXT= Requested by Inventory Control - October 2002 */
/* #VARNAM=ship_id , #REQFLG=Y */
/* #GROUPS=NOONE */


ttitle  left print_date     print_time center 'IC Weight By  Sid Detail' skip  -
btitle  center 'Page: ' format 999 sql.pno

column ship_id heading 'Ship Id.'
column ship_id format a20
column prtnum heading 'Item'
column prtnum format a11
column vc_grswgt_each heading 'Grs Wgt'
column vc_grswgt_each format 999999999
column lngdsc heading 'Description'
column lngdsc format a30
column picked heading ' Sid Qty'
column picked format 999999999
column total heading 'Total '
column total format 999999999

compute sum label 'Grand Total' of total on total



select a.ship_id, b.prtnum, substr(d.lngdsc,1,30) lngdsc,  sum(a.shpqty+a.pckqty+a.inpqty) picked,
c.vc_grswgt_each, sum((a.shpqty+a.pckqty+a.inpqty) * c.vc_grswgt_each) total  from
shipment_line a, ord_line b, prtmst c, prtdsc d
where a.ordnum = b.ordnum
and a.ordlin = b.ordlin
and a.ordsln =  b.ordsln
and a.client_id= b.client_id
and b.prtnum = c.prtnum
and b.prt_client_id = c.prt_client_id
and c.prtnum|| '|----' = d.colval
and d.colnam='prtnum|prt_client_id'
and d.locale_id ='US_ENGLISH'
and a.ship_id = '&1'
group by a.ship_id, b.prtnum, c.vc_grswgt_each, d.lngdsc
/
