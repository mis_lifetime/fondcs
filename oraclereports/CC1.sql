select trunc(a.cntdte) adddte, a.cnt_usr_id, count(distinct a.stoloc) count
from usr_cnthst a,locmst b 
where a.stoloc = b.stoloc(+) 
and a.cnttyp = 'B' and
a.cntmod = 'C' and exists (select 1 from usr_cnthst u where u.cntbat  =
a.cntbat and (u.cnttyp = 'F' or u.cnttyp='E' and u.untqty >=0)  
and u.stoloc = a.stoloc)
and trunc(a.cntdte) between '&&1' and '&&2'
group by trunc(a.cntdte),a.cnt_usr_id;
