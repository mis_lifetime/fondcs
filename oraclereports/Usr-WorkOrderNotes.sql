/* #REPORTNAME=User Work Order Notes Report */
/* #HELPTEXT= This report lists work orders and notes */
/* #HELPTEXT= Requested by Production  */
/* #GROUPS=NOONE */

set pagesize 50000
set linesize 160


ttitle left print_time -
center 'User Work Order Notes ' -
right print_date skip 2 -


column wkonum heading 'Work Order #'
column wkonum format A30
column prdqty heading 'Prod Qty'
column prdqty format  9999999999
column wkoqty heading 'Work Order Qty'
column wkoqty format 9999999999
column notlin heading 'Line   '
column notlin format A5
column nottxt heading 'Notes     '
column nottxt format A40


select a.wkonum, a.prdqty, a.wkoqty, b.notlin, b.nottxt
from wkohdr a, wkohdr_note b
where a.client_id = b.client_id
and a.wkonum = b.wkonum
and a.wkorev = b.wkorev
union
select a.wkonum, a.prdqty, a.wkoqty, b.notlin, b.nottxt
from wkohdr@arch  a, wkohdr_note@arch b
where a.client_id = b.client_id
and a.wkonum = b.wkonum
and a.wkorev = b.wkorev
/
