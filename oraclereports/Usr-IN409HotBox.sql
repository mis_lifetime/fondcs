/* #REPORTNAME=User IN409 Hot Box Report */
/* #HELPTEXT= This report lists all customers with */
/* #HELPTEXT= hot items that are not received */
/* #HELPTEXT= Requested by Senior Management */
/* #GROUPS=NOONE */

set pagesize 450
set linesize 200


ttitle left print_time -
center 'User IN409 Hot Box Report ' -
right print_date skip 2 -

/*  If no file # exists to meet the criteria, the trknum and rcvsts will be */ 
/*  populated with ----                                                     */

alter session set nls_date_format ='DD-MON-YYYY';


column priority heading 'Priority'
column priority format A8
column expdte heading 'Expected '
column expdte format A12
column done heading 'Date In - Date Done'
column done format A20
column prtnum heading 'Hot Item'
column prtnum format A20
column adrnam heading 'Customer Name'
column adrnam format A40
column trknum heading 'File #'
column trknum format  A20
column rcvsts heading 'Status'
column rcvsts format A6
column notes heading 'Notes'
column notes format A5


select
distinct a.prtnum, f.adrnam, trunc(c.expdte)+ 21  expdte , null priority, 
null done, decode(t.trlr_stat,'EX', b.trknum,'----') trknum,
decode(t.trlr_stat,'EX','Q','----') rcvsts, null notes
from 
usr_hotlist a, rcvlin b, rcvtrk c, rcvinv d, adrmst f, trlr t
where c.trlr_id = t.trlr_id(+)
and a.prtnum = b.prtnum(+)
and b.trknum = c.trknum(+)
and b.trknum = d.trknum(+)
and b.invnum = d.invnum(+)
and b.supnum = d.supnum(+)
and a.btcust = f.host_ext_id(+)
and f.client_id = '----'
and b.client_id = d.client_id(+)
and t.trlr_stat(+) = 'EX'
/
