/* #REPORTNAME=Expected Receipts Report by Truck Number */
/* #VARNAM=trknum , #REQFLG=N */
/* #HELPTEXT= Requested by Receiving. */

ttitle left print_time -
       center 'Expected Receipts by Truck Number ' -
       right print_date skip 1 -
       center 'File Number:  &1' 
btitle skip 1 center 'Page: ' format 999 sql.pno 

column prtnum heading 'Part Number'
column prtnum format a11 trunc
column lngdsc heading 'Description'
column lngdsc format a20 trunc
column adrnam heading 'Vendor'
column adrnam format a25 trunc
column ftpcod heading 'FTP Code'
column ftpcod format a11 trunc
column caslen heading 'C/L'
column caslen format '9999.9'
column caswid heading 'C/W'
column caswid format '9999.9'
column cashgt heading 'C/H'
column cashgt format '9999.9'
column untpak heading 'U/Pack'
column untpak format '999999'
column untcas heading 'U/Case'
column untcas format '999999'
column ctn    heading 'CTN'
column ctn    format 9999999
column expqty heading 'Exp|Qty'
column expqty format 9999999999

break on report
compute sum label 'Totals' of expqty on report
compute sum label 'Totals' of ctn on report

SELECT
rcvlin.prtnum,
prtdsc.lngdsc,
nvl(adrmst.adrnam,'Vendor not set up in DCS') adrnam,
ftpmst.ftpcod,
ftpmst.caslen,
ftpmst.caswid,
ftpmst.cashgt,
prtmst.untpak,
prtmst.untcas,
sum(rcvlin.expqty) / prtmst.untcas  Ctn,
sum(rcvlin.expqty) expqty
FROM rcvlin, rcvinv,
ftpmst, prtmst, prtdsc,supmst, adrmst
WHERE rcvinv.trknum = rcvlin.trknum
AND rcvinv.trknum = '&1'
AND rcvinv.supnum = rcvlin.supnum
AND rcvinv.invnum = rcvlin.invnum
and rcvinv.client_id = supmst.client_id(+)
and rcvinv.supnum = supmst.supnum(+)
and supmst.client_id = adrmst.client_id(+)
and supmst.adr_id = adrmst.adr_id(+)
AND prtmst.ftpcod = ftpmst.ftpcod(+)
AND prtmst.prtnum(+) = rcvlin.prtnum
AND prtmst.prtnum||'|----' = prtdsc.colval(+)
group
by rcvinv.trknum,
rcvlin.prtnum,
adrmst.adrnam,
prtdsc.lngdsc,
ftpmst.ftpcod,
ftpmst.caslen,
ftpmst.caswid,
ftpmst.cashgt,
prtmst.untpak,
prtmst.untcas
/
