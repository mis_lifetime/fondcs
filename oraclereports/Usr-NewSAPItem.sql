/* #REPORTNAME=Usr New SAP Item Download */
/* #HELPTEXT= This report will list the new SAP Items */


 ttitle center 'New SAP Item Report ' -

column prtnum heading 'Item'
column prtnum format a12
column lngdsc heading 'Description'
column lngdsc format a30
column upccod heading 'UPC Code'
column upccod format a20
column ndccod heading 'NDC Code'
column ndccod format a20
column ftpcod heading 'Footprint'
column ftpcod format a30
column vc_prdcod heading 'Brand'
column vc_prdcod format a5
column untpak heading 'Unit/Pack'
column untpak format 9999999999
column untcas heading 'Unit/Case'
column untcas format 9999999999
column untcst heading 'Unit Cost'
column untcst format 9999999999.99
column vc_itmgrp heading 'Item Group'
column vc_itmgrp format a10
column vc_itmtyp heading 'Item Type'
column vc_itmtyp format a10
column moddte heading 'Date Came Down'
column moddte format a15
column mod_usr_id heading 'User ID'
column mod_usr_id format a15

alter session set nls_Date_format ='dd-mon-yyyy';
set linesize 300

select a.prtnum,substr(b.lngdsc,1,30) lngdsc, a.upccod, a.ndccod, a.ftpcod, a.vc_prdcod, a.untpak,
a.untcas, a.untcst, a.vc_itmgrp, a.vc_itmtyp, a.moddte, a.mod_usr_id
from prtmst a, prtdsc b
where a.prtnum||'|----' =b.colval
and a.mod_usr_id ='slIFDG_0'
and b.colnam ='prtnum|prt_client_id'
and b.locale_id ='US_ENGLISH'
/
