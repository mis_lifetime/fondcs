/* #REPORTNAME=IC CFPP On-Hand Report */
/* #HELPTEXT= Ths report generates a list of CFPP locations. */ 
/* #HELPTEXT= Requested by Inventory Control */

/* Author: Al Driver */
/* Report provides requested information for CFPP  */
/* locations. Initially the Unit Qty column was */
/* labeled On Hand, but the on hand amount is usually untqty - comqty. */

set pagesize 50000
set linesize 200

ttitle left  print_time -
       center 'IC CFPP On-Hand Report' -
       right print_date skip 2 


column area heading 'Area'
column area format A10
column location heading 'Location'
column location format A10
column prtnum heading 'Part#'
column prtnum format A12
column description heading 'Description'
column description format A30
column cost heading 'Cost (Unit)'
column cost format 99999.999 
column lodnum heading 'Load #'
column lodnum format A30
column untqty heading 'Unit Qty'
column untqty format 999999 
column comqty heading 'Committed'
column comqty format 999999
column pndqty heading 'Pending (inbound)'
column pndqty format 999999
column status heading 'St'
column status format  A2
column lotcode heading 'Lot'
column lotcode format A5
column prtfam heading 'Part Family'
column prtfam format A11
column asgflg heading 'Assignment'
column asglfg format A6
column vc_prdcod heading 'Prod. Class'
column vc_prdcod format A12


select distinct c.arecod area,c.stoloc location,c.prtnum,substr(b.lngdsc,1,30) description,a.untcst cost,c.untqty untqty,
    c.comqty comqty,
    c.pndqty pndqty,c.invsts status,g.lotnum lotcode,a.prtfam,
    decode(d.asgflg,0,'No',1,'Yes','Error!') asgflg,a.VC_prdcod prod_class
    from
        prtmst a,prtdsc b,invsum c,locmst d,invlod e,invsub f,invdtl g
        where a.prtnum|| '|----' = b.colval
        and a.prtnum = c.prtnum
       and a.prt_client_id = c.prt_client_id
       and a.prtnum = g.prtnum
       and a.prt_client_id = g.prt_client_id      
       and c.arecod = d.arecod
       and c.stoloc = d.stoloc
       and c.stoloc = e.stoloc
       and c.prtnum = g.prtnum
       and c.prt_client_id = g.prt_client_id
       and e.lodnum = f.lodnum
       and f.subnum = g.subnum
       and b.colnam ||''='prtnum|prt_client_id'
       and b.locale_id ||''= 'US_ENGLISH'
       and d.arecod ||''= 'CFPP'
      order by 2
/

 
