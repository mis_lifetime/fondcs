/* #REPORTNAME=User Wave Parameter Report */
/* #HELPTEXT= Requested by Order Management. */

ttitle left print_time center 'User Wave Parameter Report ' -
	right print_date skip 2
		btitle  skip 1 center 'Page: ' format 999 sql.pno

column vc_wave_lane heading 'Lane'
column vc_carcod heading 'Carrier'
column vc_cstnum heading 'Customer'
column vc_max_sid_per_wave heading 'Max Sid|Per Wave'
column vc_max_cases_per_wave heading 'Max Cases|Per Wave'
column vc_fluid_load_flg heading 'Fluid|Flag'
column vc_non_wave_flg heading 'Non-Wave|Flag'

select vh.vc_wave_lane, vc_carcod, vc_cstnum, vc_max_sid_per_wave,
vc_max_cases_per_wave,vc_fluid_load_flg,vc_non_wave_flg 
from var_wavpar_hdr vh, var_wavpar_dtl vd  
where vh.vc_wave_lane=vd.vc_wave_lane 
order by vh.vc_wave_lane
/
