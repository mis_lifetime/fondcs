/* #REPORTNAME= Usr  Expected Report */

ttitle left  print_time -
       center 'Usr Expected Receipts By Date Report' -
       right print_date skip 1 -
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column arecod heading 'Area'
column arecod format a6
column untqty heading 'Qty'
column untqty format 999999
column invsts heading 'Status'
column invsts format a6



select
        locmst.arecod,
         invdtl.prtnum,
         invdtl.invsts,
         sum (invdtl.untqty) untqty
   from invdtl,
         invsub,
         invlod,
         locmst,
         aremst,
         poldat
   where invdtl.subnum = invsub.subnum
     and invsub.lodnum = invlod.lodnum
     and invdtl.prt_client_id = '----'
     and invlod.stoloc = locmst.stoloc
     and locmst.arecod = aremst.arecod
     and locmst.arecod ='EXPR'
    and aremst.pckcod = 'N'
    and aremst.arecod = poldat.rtstr1
    and poldat.polval = 'INCLUDE-NON-PICKABLE-AREA'
     and poldat.polvar = 'INVENTORY-PART-DISPLAY'
     and poldat.polcod = 'VAR'
   group by invdtl.prtnum, locmst.arecod,
            invdtl.invsts
/
