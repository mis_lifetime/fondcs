/* #REPORTNAME=FIN-Cartons Shipped  Daily - Piece Pick */
/* #HELPTEXT= This report lists all carton count Detail Piece Pick information by Day */

/* #HELPTEXT= Please enter date in format dd-mon-yyyy   */

/* #HELPTEXT= Requested by Controller */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To,    #REQFLG=Y */


ttitle left '&1 ' print_time -
center 'FIN-Carton Daily Counts Report' -
right print_date skip 2

btitle skip1 center 'Page:' format 999 sql.pno

column shipped heading 'Shipped'
column shipped format a12
column piece heading 'Piece Pick Cartons'
column piece format 999999

alter session set nls_date_format = 'dd-mon-yyyy';

set linesize 200
set pagesize 300

select trunc(shpdte) shipped, 
sum(counts) piece
from usr_Carton_counts
where trunc(shpdte) between '&1' and '&2' and coding  ='PP'
group by trunc(shpdte)
/
