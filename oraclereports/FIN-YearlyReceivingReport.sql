/* #REPORTNAME= FIN-Yearly Receiving Report */
/* #HELPTEXT= This report gives a yearly summary of */
/* #HELPTEXT= expected and received cartons,containers */
/* #HELPTEXT= and pallets. Enter year in format YYYY. */
/* #HELPTEXT= Requested by The Controller */

/* #VARNAM=year , #REQFLG=Y */


/* Author:  Al Driver */

/* Report provides appropriate counts by passing dates */
/* to the UsrYearlyReceipts package. Dates must be generated */
/* by week# of the year. Date functions are used to manipulate */
/* the dates properly for any year. Two unions are used. */
/* The first script is to calculate the dates for the 1st week */
/* of the year, and the last script is used to calculate the */
/* dates for the final week of the year.  Each year is */
/* different. */

set pagesize 100
set linesize 800

ttitle left print_time -
center 'FIN-Yearly Receiving Report  ' -
right print_date skip 2


column contdue heading '#Containers | Due'
column contdue format 999 
column ctndue heading '#Cartons | Due'
column ctndue format 9999999 
column contrcd heading '#Containers | Recd'
column contrcd format 999 
column ctnrcd heading '#Cartons | Recd'
column ctnrcd format 9999999 
column palrcd heading '#Pallets | Recd'
column palrcd format 9999999
column domctn heading '#Domestic Cartons | Recd'
column domctn format 9999999 
column domcont heading '#Domestic Containers | Recd'
column domcont format 9999999999
column intlctn heading '#Intl Cartons | Recd'
column intlctn format 999999999 
column intlcont heading '#Intl Containers | Recd'
column intlcont format 99999999
column wknum heading 'Week#'
column wknum format 999

alter session set nls_date_format ='DD-MON-YYYY';

/* NEXT_DAY & LAST_DAY functions used to get appropriate dates */
/* for first * last weeks of the year. */

select rownum wknum, usrYearlyReceipts.usrGetContDue(to_date('01-JAN-'||('&&1'),'DD-MON-YYYY'),
NEXT_DAY('31-DEC-'||('&&1'-1),'SAT')) contdue,
usrYearlyReceipts.usrGetCtnDue(to_date('01-JAN-'||('&&1'),'DD-MON-YYYY'),
NEXT_DAY('31-DEC-'||('&&1'-1),'SAT')) ctndue,
usrYearlyReceipts.usrGetContRcd(to_date('01-JAN-'||('&&1'),'DD-MON-YYYY'),
NEXT_DAY('31-DEC-'||('&&1'-1),'SAT'))  contrcd,
usrYearlyReceipts.usrGetCtnRcd(to_date('01-JAN-'||('&&1'),'DD-MON-YYYY'),
NEXT_DAY('31-DEC-'||('&&1'-1),'SAT')) ctnrcd,
usrYearlyReceipts.usrGetPalRcd(to_date('01-JAN-'||('&&1'),'DD-MON-YYYY'),
NEXT_DAY('31-DEC-'||('&&1'-1),'SAT')) palrcd,
usrYearlyReceipts.usrGetDomCont(to_date('01-JAN-'||('&&1'),'DD-MON-YYYY'),
NEXT_DAY('31-DEC-'||('&&1'-1),'SAT')) domcont,
usrYearlyReceipts.usrGetIntlCont(to_date('01-JAN-'||('&&1'),'DD-MON-YYYY'),
NEXT_DAY('31-DEC-'||('&&1'-1),'SAT')) intlcont,
usrYearlyReceipts.usrGetDomCtn(to_date('01-JAN-'||('&&1'),'DD-MON-YYYY'),
NEXT_DAY('31-DEC-'||('&&1'-1),'SAT')) domctn,
usrYearlyReceipts.usrGetIntlCtn(to_date('01-JAN-'||('&&1'),'DD-MON-YYYY'),
NEXT_DAY('31-DEC-'||('&&1'-1),'SAT')) intlctn
FROM dual
union
select rownum +1 wknum, usrYearlyReceipts.usrGetContDue(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (rownum*7)
,NEXT_DAY(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN'),'SAT') + (rownum*7)) contdue,
usrYearlyReceipts.usrGetCtnDue(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (rownum*7)
,NEXT_DAY(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN'),'SAT') + (rownum*7)) ctndue,
usrYearlyReceipts.usrGetContRcd(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (rownum*7)
,NEXT_DAY(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN'),'SAT') + (rownum*7)) contrcd,
usrYearlyReceipts.usrGetCtnRcd(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (rownum*7)
,NEXT_DAY(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN'),'SAT') + (rownum*7)) ctnrcd,
usrYearlyReceipts.usrGetPalRcd(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (rownum*7)
,NEXT_DAY(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN'),'SAT') + (rownum*7)) palrcd,
usrYearlyReceipts.usrGetDomCont(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (rownum*7)
,NEXT_DAY(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN'),'SAT') + (rownum*7)) domcont,
usrYearlyReceipts.usrGetIntlCont(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (rownum*7)
,NEXT_DAY(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN'),'SAT') + (rownum*7)) intlcont,
usrYearlyReceipts.usrGetDomCtn(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (rownum*7)
,NEXT_DAY(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN'),'SAT') + (rownum*7)) domctn,
usrYearlyReceipts.usrGetIntlCtn(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (rownum*7)
,NEXT_DAY(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN'),'SAT') + (rownum*7)) intlctn
FROM rcvtrk
where rownum < 52
union
select 53 wknum, usrYearlyReceipts.usrGetContDue(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (52 * 7),
LAST_DAY('01-DEC-'||'&&1')) contdue,
usrYearlyReceipts.usrGetCtnDue(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (52 * 7),
LAST_DAY('01-DEC-'||'&&1')) ctndue,
usrYearlyReceipts.usrGetContRcd(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (52 * 7),
LAST_DAY('01-DEC-'||'&&1'))  contrcd,
usrYearlyReceipts.usrGetCtnRcd(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (52 * 7),
LAST_DAY('01-DEC-'||'&&1')) ctnrcd,
usrYearlyReceipts.usrGetPalRcd(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (52 * 7),
LAST_DAY('01-DEC-'||'&&1')) palrcd,
usrYearlyReceipts.usrGetDomCont(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (52 * 7),
LAST_DAY('01-DEC-'||'&&1')) domcont,
usrYearlyReceipts.usrGetIntlCont(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (52 * 7),
LAST_DAY('01-DEC-'||'&&1')) intlcont,
usrYearlyReceipts.usrGetDomCtn(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (52 * 7),
LAST_DAY('01-DEC-'||'&&1')) domctn,
usrYearlyReceipts.usrGetIntlCtn(NEXT_DAY('25-DEC-'||('&&1'-1),'SUN') + (52 * 7),
LAST_DAY('01-DEC-'||'&&1')) intlctn
FROM dual 
/
