/* #REPORTNAME=User What's on a UCC*/
/* #REPORTTYPE=DCSCMD */
/* #HELPTEXT= Displays What is on a UCC*/ 
/* #HELPTEXT= Requested by Order Management */
/* #VARNAM=subucc , #REQFLG=Y */ 

set linesize 300

select b.ctnnum, b.srcloc, b.prtnum, b.appqty, b.pckqty, b.ordnum, b.ship_id
from pckwrk a, pckwrk b, locmst c
where a.subucc='&1'
and a.subnum=b.ctnnum
and c.stoloc=b.srcloc
order by c.trvseq
/
