/* #REPORTNAME=FIN-Cycle Count Location By Date Report */
/* #HELPTEXT= This report lists all locations that were counted */
/* #HELPTEXT= within a date range */ 
/* #HELPTEXT= Requested by Controller.*/
/* #VARNAM=Date-Started , #REQFLG=Y */
/* #VARNAM=Date-Ended, #REQFLG=Y */

/* report should match CC-CycleCountByUser and CC-SummaryByDate reports */

 ttitle left  print_time center 'FIN - Cycle Count Location By Date Report ' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column stoloc heading 'Location'
column stoloc format a10
column cntdte heading 'Date Counted'
column cntdte format a12
column cnt_usr_id heading 'User'
column cnt_usr_id format a30

set linesize 200
set pagesize 3000


alter session set nls_date_format ='dd-mon-yyyy';

select distinct stoloc, trunc(cntdte) cntdte, cnt_usr_id
from usr_cnthst
where trunc(cntdte) between '&1' and '&2'
and cnttyp ='E' and cntmod != 'A'  /* E for counted, excluding A for audited counts */
and exists(select 'x' from cnthst a where a.cntbat = usr_cnthst.cntbat)
order by trunc(cntdte)
/
