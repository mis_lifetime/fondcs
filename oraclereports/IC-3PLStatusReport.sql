/* #REPORTNAME=IC 3PL Status Report */
/* #HELPTEXT= This report lists all requested information */
/* #HELPTEXT= concerning pallets pending ane existing */
/* #HELPTEXT= 3PLSTAGE, TRK and HERR locations. */
/* #HELPTEXT= Requested by Vince McAneny and Joe H. */

 ttitle left  print_time center 'IC 3PL Status Report ' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column rack heading 'RACKS to 3PLSTAGE% Pallets'
column rack format 999999
column istg heading 'P and Ds Inbound to 3PLSTAGE% Pallets'
column istg format 999999
column plstg heading 'Total 3PLSTAGE% Pallets'
column plstg format 999999
column trk heading 'Total TRK% Pallets'
column trk format 999999
column herr heading 'Total HERR% Pallets'
column herr format 999999

set linesize 150 

select usr3PL.GetRacksPnd rack, usr3PL.GetIstgPnd istg, usr3PL.Get3PLSTAGE plstg, usr3PL.GetTRK trk,
usr3PL.GetHERR herr  from dual
/
