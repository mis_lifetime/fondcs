/* #REPORTNAME=IC-Lotted Inventory Report */
/* #HELPTEXT= This report displays the item, lot code, */
/* #HELPTEXT= available quantity, committed quantity, */
/* #HELPTEXT= pending quantity, status , location and area */
/* #HELPTEXT= for all inventory for all lots except NOLOT. */
/* #HELPTEXT= Requested by Inventory Control */


-- Author: Al Driver

/* utilize usr_prtqty_view_sum to obtain inventory values */
/* usr_prtqty_view_sum similar to var_prtqty_view */

 ttitle left '&1' print_time center 'IC-Lotted Inventory Report' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno


column stoloc heading 'Storage Location'
column stoloc format a20
column arecod heading 'Area'
column arecod format a10
column prtnum heading 'Part Number'
column prtnum format a30
column lotnum heading 'Lot Number'
column lotnum format a20
column invsts heading 'Inv Status'
column invsts format a10
column untqty heading 'Unit Qty'
column untqty format 999999
column comqty heading 'Committed Qty'
column comqty format 999999
column avlqty heading 'Available Qty'
column avlqty format 999999
column pndqty heading 'Pending Qty'
column pndqty format 999999

set linesize 300
set pagesize 2000

select usr_prtqty_view_sum.stoloc,
           usr_prtqty_view_sum.arecod,
           usr_prtqty_view_sum.prtnum,
           usr_prtqty_view_sum.lotnum,
           usr_prtqty_view_sum.invsts,
           sum(usr_prtqty_view_sum.untqty) untqty,
           sum(usr_prtqty_view_sum.comqty) comqty,
           sum(usr_prtqty_view_sum.avlqty) avlqty,
           sum(usr_prtqty_view_sum.pndqty) pndqty
    from usr_prtqty_view_sum
  group by stoloc,arecod,prtnum,lotnum,invsts
/
