/* #REPORTNAME=User Daily Shipped UPS /RPS Report */
/* #HELPTEXT= The Shipped Orders Summary by Date */
/* #HELPTEXT= yeilds a summary of UPS/RPS shipped orders.*/
/* #HELPTEXT= It requires a date range entered in the*/
/* #HELPTEXT= form DD-Mon-YY. Requested by Shipping */

/* #VARNAM=loddte , #REQFLG=Y */
/* #VARNAM=loddt1 , #REQFLG=Y */

ttitle left  print_time -
       center 'User Shipment By Date Range: ' -
       right print_date skip -
       center ' &1 '  to  ' &2 ' skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno 

column ordnum heading 'Order No.' just center
column ordnum format A10 
column shipid heading 'Ship Id' just center
column shipid format A12
column stname heading 'Customer Name'just center
column stname format A25
column cityst heading 'Destination' just center
column cityst format A25
column carcod heading 'Shipped VIA' just center
column carcod format A10 
column traknm heading 'Tracking #' just center
column traknm format A20
column usr_totctn heading 'CNT' just center
column usr_totctn format 9999990


set pagesize 500
set linesize 300


select rtrim(ltrim(sd.ordnum)) ordnum,
       rtrim(ltrim(sd.ship_id)) shipid,      
       rtrim(lookup1.adrnam) stname,
       rtrim(lookup1.adrcty)||', '||lookup1.adrstc cityst,
       rtrim(ltrim(sh.carcod)) carcod,
       rtrim(ltrim(mn.traknm)) traknm,
       count(distinct(i.subnum)) usr_totctn
 from invdtl i, shipment_line sd, shipment sh, ord, ord_line, stop, adrmst lookup1, manfst mn
        where i.ship_line_id            =  sd.ship_line_id
        AND ord.st_adr_id               = lookup1.adr_id
        AND ord.client_id               = lookup1.client_id
        AND ord.ordnum                  = ord_line.ordnum
  AND ord.client_id                     = ord_line.client_id
        AND ord_line.client_id          = sd.client_id
        AND ord_line.ordnum             = sd.ordnum
        AND ord_line.ordlin             = sd.ordlin
        AND ord_line.ordsln             = sd.ordsln
        AND sd.ship_id                  = sh.ship_id
        AND sd.ship_id                  = mn.ship_id
        AND sh.doc_num                  = stop.doc_num
        AND sh.shpsts                   = 'C'
        AND sh.carcod in ('UPSC','UPSG','UPSN','UPSS','RPSS')
        AND ord.ordtyp                   != 'W'
        AND sh.loddte between to_date('&&1','DD-MON-YYYY') and
                      to_date('&&2','DD-MON-YYYY')+.99999
group
    	by sd.ship_id,
        sd.ordnum,
        lookup1.adrnam,
        lookup1.adrcty,
        lookup1.adrstc,
        sh.carcod,
        mn.traknm
order
        by sd.ordnum,
        lookup1.adrnam;
