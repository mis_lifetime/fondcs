/* #REPORTNAME =IC Pending Quantities in Locations */
/* #VARNAM=stoloc , #REQFLG=Y */

column stoloc heading 'Location'
column stoloc format A15
column prtnum heading 'Item'
column prtnum format A12
column pndqty heading 'Pending Qty'
column pndqty format 99999

set pagesize 50000;
set linesize 500;



SELECT
invsum.stoloc,
invsum.prtnum, 
invsum.pndqty
from invsum, locmst
where 
invsum.stoloc  = '&1'
and invsum.stoloc=locmst.stoloc
and invsum.arecod=locmst.arecod
/
