/* #REPORTNAME= IC Picked By Area Report*/
/* #HELPTEXT= This report lists information of inventory */
/* #HELPTEXT= moved from TEL or CFPP locations to the Conveyor for a */
/* #HELPTEXT= given date range. Date to be entered in the */
/* #HELPTEXT= format dd-mon-yyyy */ 
/* #HELPTEXT = Requested by Inventory Control May 2005 */


/* #VARNAM=BEGDTE , #REQFLG=Y */
/* #VARNAM=ENDDTE , #REQFLG=Y */


ttitle left print_time center 'IC-Picked By Area Report' -
	right print_date skip 2

btitle skip 1 center 'Page: ' format 999 sql.pno

column trndte heading 'Date'
column trndte format a11
column lodnum heading 'Pallet ID'
column lodnum format a20
column subnum heading 'CTN'
column subnum format a20
column trnqty heading 'Units'
column trnqty format 999999
column arecod heading 'Source Area'
column arecod format a20
column tostol heading 'Destin Location'
column tostol format a20
column ctn heading 'Cartons'
column ctn format 999999
column qube   heading 'Cube'
column qube   format 99999.99 

alter session set nls_date_format = 'dd-mon-yyyy HH:MI:SS AM';

set linesize 200
set pagesize 10000

select substr(trndte,1,11) trndte,lodnum,count(distinct subnum) ctn,sum(trnqty) trnqty,arecod,tostol,
sum((trnqty/untcas) * (caslen*caswid*cashgt/1728)) qube
from
prtmst,ftpmst,locmst,dlytrn
where dlytrn.prtnum = prtmst.prtnum
and frstol = stoloc
and prtmst.prtnum = ftpmst.ftpcod(+)
and tostol = 'CONVEYOR'
and trndte between '&&1'||' 12:00:00 AM' and '&&2'||' 11:59:59 PM'
group by substr(trndte,1,11),lodnum,arecod,tostol
union
select substr(trndte,1,11) trndte,lodnum,count(distinct subnum) ctn,sum(trnqty) trnqty,arecod,tostol,
sum((trnqty/untcas) * (caslen*caswid*cashgt/1728)) qube            
from
prtmst,ftpmst,locmst,dlytrn@arch
where dlytrn.prtnum = prtmst.prtnum
and frstol = stoloc
and prtmst.prtnum = ftpmst.ftpcod(+)
and tostol = 'CONVEYOR'
and trndte between '&&1'||' 12:00:00 AM' and '&&2'||' 11:59:59 PM'
group by substr(trndte,1,11),lodnum,arecod,tostol
/
