/* #REPORTNAME=User Assigned Replenishments Vs. Actual Inventory  CFPP Report */
/* #HELPTEXT= Requested by Order Management */


ttitle left print_time center 'User Assigned Replenishments Vs. Actual Inventory CFPP Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column arecod heading 'Area'
column arecod format a12
column stoloc heading 'Location'
column stoloc formt a12
column replen heading 'Assigned Replen Item'
column replen format a12
column prtnum heading 'Item'
column prtnum format a12
column untqty heading 'Qty.'
column untqty format 999999
column comqty heading 'Committed Qty'
column comqty format 999999
column lotnum heading 'Lot'
column lotnum format a5
column invsts heading 'Status'
column invsts format a5

select a.arecod, a.stoloc, b.prtnum, a.prtnum, a.untqty, a.comqty, c.lotnum, a.invsts 
from invsum a, rplcfg b, invdtl c, invsub d, invlod e, locmst f
where 
a.prtnum=c.prtnum
and a.prt_client_id=c.prt_client_id
and c.subnum=d.subnum
and d.lodnum=e.lodnum
and a.arecod=f.arecod
and a.stoloc=f.stoloc
and f.arecod=b.arecod
and b.prt_client_id='----'
and a.arecod  in ('CFPP')
/
