/* #REPORTNAME=User Footprint Rawm, Pkg, FG Report  */
/* #VARNAM=Part_Family , #REQFLG=Y */
/* #HELPTEXT= Following report provides selected Footprint */
/* #HELPTEXT= information for Raw Material, Package and */
/* #HELPTEXT= Finished Good Items. Report requires a  */
/* #HELPTEXT= parameter for the part family.  */
/* #HELPTEXT = Requested by Inventory Control */

/* Report is a combination of 3 different footprint */
/* reports, initially requested by Engineering. */
/* Usr-AlexRFootPrintRawMaterial.sql, Usr-AlexRFootPrintFg.sql, */ 
/* and Usr-AlexRFootPrintPkg.sql */

ttitle left print_time center 'User Footprint Rawm, Pkg and FG Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item'
column prtnum format a30
column lngdsc heading 'Description'
column lngdsc format a38
column untpak heading 'Unit Pack'
column untpak format 999999
column untcas heading 'Case Pack'
column untcas format 999999
column caslen heading 'Master Length'
column caslen format 999999
column caswid heading 'Master Width'
column caswid format 999999
column cashgt heading 'Master Height'
column cashgt format 999999
column netwgt heading 'Net Weight'
column netwgt format 999999
column vc_itmtyp heading 'Item Type'
column vc_itmtyp format a10
column prtfam heading 'Part Family'
column prtfam format a10

set linesize 300
set pagesize 15000

select a.prtnum, substr(b.lngdsc,1,38), a.untpak, a.untcas, c.caslen, c.caswid, c.cashgt, 
a.netwgt, a.vc_itmtyp, a.prtfam
from prtmst a, prtdsc b, ftpmst c
where a.prtnum=c.ftpcod
and a.prt_client_id='----'
and a.prtnum||'|----' = b.colval
and b.locale_id='US_ENGLISH'
and b.colnam='prtnum|prt_client_id'
and a.vc_itmtyp in ('RAWM','FG','PKG')
and upper(a.prtfam) = upper('&1') 
order by a.prtnum 
/
