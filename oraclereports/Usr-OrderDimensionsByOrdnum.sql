/* #REPORTNAME=User Order Dimensions Report */
/* #HELPTEXT= The Order Dimensions Report */
/* #HELPTEXT= yeilds a list of items and their dimensions. */ 
/* #HELPTEXT= It requires a ordnum or a cponum 
/* #HELPTEXT= Requested by Senior Management- Shipping */

/* #VARNAM=ordnum , #REQFLG=N */
/* #VARNAM=cponum , #REQFLG=N */

/*set linesize 200 */

/*ttitle left  print_time -
       center 'User Order Dimensions Report' */
ttitle left print_time center 'User Order Dimensions Report' -
       right print_date skip 2

column ordnum heading 'Order Number' just center
column ordnum format A12
column prtnum heading 'Item' just center
column prtnum format A12
column cponum heading 'PO Number' just center
column cponum format A12
column CTN heading 'Cartons'
column caswgt heading 'Weight'
column caswgt format 999999.99
column cashgt heading 'Height'
column cashgt format 999999
column caslen heading 'Length'
column caslen format 999999
column caswid heading 'Width'
column caswid format 999999
column untcas heading 'U/C'
column untcas format 9999
column cube heading 'Cube'
column cube  format 99999.99
column  pallets heading 'PALLETS'
column pallets format 9999.99
break on cponum on ordnum skip 1
compute sum label 'Totals' of ctn  on cponum
compute sum label 'Totals' of cube on cponum
compute sum label 'Totals' of caswgt on cponum
compute sum label 'Report Totals' of ctn  on report 
compute sum label 'Report Totals' of cube on report 

/*set pagesize 63*/

select cponum, ord.ordnum, ordlin.prtnum, ordqty/prtmst.untcas ctn, grswgt*(ordqty/prtmst.untcas) caswgt, caswid, caslen, cashgt, prtmst.untcas, (ordqty/prtmst.untcas)*(caslen*cashgt*caswid)/1728 cube from ord, ord_line ordlin, prtmst, ftpmst where ord.ordnum = ordlin.ordnum and ord.client_id = ordlin.client_id and ordlin.prtnum = prtmst.prtnum and prtmst.ftpcod = ftpmst.ftpcod and ord.ordnum  like '&1%' and cponum like '&2%' order by ord.ordnum;
set heading off
set feedback off
ttitle left ' ' 

select 'Total Pallets: ', sum(ordqty/prtmst.untcas*caslen*cashgt*caswid/1728)/80 pallets from ord, ord_line ordlin, prtmst, ftpmst where ord.ordnum = ordlin.ordnum and ord.client_id = ordlin.client_id and ordlin.prtnum = prtmst.prtnum and prtmst.ftpcod = ftpmst.ftpcod and ord.ordnum  like '&1%' and cponum like '&2%' 


 

/
