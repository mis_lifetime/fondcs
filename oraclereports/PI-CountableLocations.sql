set linesize 300
set pagesize 50000

alter session set nls_date_format = 'dd-mon-yyyy';

/********************************************************************************/
/* #REPORTNAME=PI Countable Locations Report                                    */
/* #VARNAM=Starting-Location , #REQFLG=N                                        */
/* #VARNAM=Ending-Location , #REQFLG=N                                          */
/* #VARNAM=Count-Type , #REQFLG=N                                               */  
/* #HELPTEXT= This report will list locations in countable areas                */
/* #HELPTEXT= that do not have counts generated OR locations in                 */
/* #HELPTEXT= countable areas with deferred counts OR locations in              */
/* #HELPTEXT= countable areas with pending counts.                              */
/* #HELPTEXT= If the CountType is not selected, the report will return          */
/* #HELPTEXT= data for all  types.                                              */
/* #HELPTEXT= Data Types are P - Pending Counts ; D - Deferred Counts;          */
/* #HELPTEXT= N - Counts Not Generated; I - In Process Counts ;                 */
/* #HELPTEXT= C - Completed Counts                                              */
/* #HELPTEXT= Leaving all fields blank will generate information on all         */
/* #HELPTEXT= locations and types                                               */
/* #HELPTEXT= Populating the Count Type only will generate                      */
/* #HELPTEXT= information for that count type only.                             */
/* #HELPTEXT= Requested for Physical Inventory                                  */
/********************************************************************************/


ttitle left  print_time Center 'PI Countable Locations Report' skip 2 -
        left print_date skip 1 -
        left 'Locations:'   '  &1  to &2' skip 2 -
btitle  left 'Page: ' format 999 sql.pno

column stoloc heading 'Location'
column stoloc format a20
column counts heading 'Count Type'
column counts format a20


select usr_countablelocations_view.stoloc stoloc,
       usr_countablelocations_view.counts
from usr_countablelocations_view, locmst
where usr_countablelocations_view.stoloc = locmst.stoloc
and usr_countablelocations_view.stoloc between upper('&1') and upper('&2') 
and usr_countablelocations_view.status =upper('&3')
union
select usr_countablelocations_view.stoloc, 
       usr_countablelocations_view.counts
from usr_countablelocations_view, locmst
where usr_countablelocations_view.stoloc =locmst.stoloc
and upper('&1') is null and upper('&2') is null 
and usr_countablelocations_view.status =upper('&3')
union
select usr_countablelocations_view.stoloc,
       usr_countablelocations_view.counts
from usr_countablelocations_view, locmst
where usr_countablelocations_view.stoloc = locmst.stoloc
and upper('&1') is null and upper('&2') is null and upper('&3') is null
union
select usr_countablelocations_view.stoloc,
       usr_countablelocations_view.counts 
from usr_countablelocations_view, locmst
where usr_countablelocations_view.stoloc between upper('&1') and upper('&2')
and upper('&3') is null
order by 1,2
/
