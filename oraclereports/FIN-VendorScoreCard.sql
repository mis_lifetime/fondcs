/* #REPORTNAME=FIN-Vendor Score Card Report */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To, #REQFLG=Y */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding Vendors */
/* #HELPTEXT= Date should be entered in DD-MON-YYYY format*/
/* #HELPTEXT= Requested by The Controller */
/* #GROUPS=NOONE */

set pagesize 1000
set linesize 200


ttitle left print_time center 'FIN - Vendor Score Card Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Item Counts'
column prtnum format 999,999,999
column adrnam heading 'Vendor'
column adrnam format a40
column idnqty heading 'Qty Received'
column idnqty format 999,999,999
column begdte heading 'Start Date'
column begdte format a12
column enddte heading 'End Date'
column enddte format a12
column costs heading 'Costs'
column costs format 999,999,999.99

alter session set nls_date_format ='DD-MON-YYYY';

select count(a.prtnum) prtnum, nvl(adrmst.adrnam,'***VENDOR NEEDS ENTRY INTO DCS***') adrnam, sum(a.idnqty) idnqty, 
'&&1' begdte,'&&2' enddte, sum(a.idnqty * a.untcst) costs
from usr_vendor_receipts a,supmst, adrmst
where 
a.supnum = supmst.supnum(+)
and a.client_id = supmst.client_id(+)
and supmst.client_id = adrmst.client_id(+)
and supmst.adr_id = adrmst.adr_id(+)
and trunc(a.clsdte) between '&&1' and '&&2'
group by adrmst.adrnam
order by 4,2
/
