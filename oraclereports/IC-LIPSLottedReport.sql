/* #REPORTNAME=  IC - Lips Lotted Report  */
/* #HELPTEXT= Lists data on inventory */
/* #HELPTEXT= lotted as LIPS. */

column prtnum heading 'Item Number'
column prtnum format a20
column reqqty heading 'Requested Qty.'
column reqqty format 999,999,999,999
column untqty heading 'Current Storage Qty. Lotted'
column untqty format 999,999,999,999
column comqty heading 'Current Committed Qty. Lotted'
column comqty format 999,999,999,999
column avlqty heading 'Current Available Qty. Lotted'
column avlqty format 999,999,999,999
column shpqty heading 'Current Shipped/Staged Qty. Lotted'
column shpqty format 999,999,999,999
column lotqty heading 'Current Qty. in NOLOT'
column lotqty format 999,999,999,999

set linesize 300
set pagesize 300
alter session set nls_date_format ='DD-MON-YYYY';

select a.prtnum, a.reqqty, sum(b.untqty) untqty, sum(b.comqty) comqty,
sum(b.avlqty) avlqty, sum(b.shpqty) shpqty, sum(b.lotqty) lotqty
from
usr_lipsparts a,
(select a.prtnum, nvl(sum(b.untqty),0) untqty, nvl(sum(f.comqty),0) comqty, 
nvl(sum(b.untqty)-sum(f.comqty),0) avlqty, 0 shpqty, 0 lotqty
from usr_lipsparts a, invdtl b, invsub c, invlod d, locmst e, invsum f, aremst g
where a.prtnum=b.prtnum
and a.prt_client_id='----'
and b.lotnum='LIPS'
and b.subnum=c.subnum
and c.lodnum=d.lodnum
and d.stoloc=e.stoloc
and d.stoloc=f.stoloc(+)
and e.arecod=g.arecod
and g.fwiflg=1
and e.arecod not in ('PROD','SSTG','SHIP','SADJ')
group by a.prtnum
union
select a.prtnum, 0 untqty, 0 comqty, 0 avlqty, 0 shpqty, 0 lotqty
from usr_lipsparts a
where a.prtnum not in (select a.prtnum
from usr_lipsparts a, invdtl b, invsub c, invlod d, locmst e
where a.prtnum=b.prtnum
and a.prt_client_id='----'
and b.lotnum='LIPS'
and b.subnum=c.subnum
and c.lodnum=d.lodnum
and d.stoloc=e.stoloc
and e.arecod not in ('PROD','SSTG','SHIP'))
union
select a.prtnum, 0 untqty, 0 comqty, 
0 avlqty, nvl(sum(b.untqty),0) shpqty, 0 lotqty
from usr_lipsparts a, invdtl b, invsub c, invlod d
where a.prtnum=b.prtnum
and a.prt_client_id='----'
and a.prt_client_id=b.prt_client_id
and b.lotnum='LIPS'
and b.subnum=c.subnum
and c.lodnum=d.lodnum
and d.stoloc like 'SHIP%'
group by a.prtnum
union
select a.prtnum, 0 untqty, 0 comqty, 0 avlqty, sum(b.untqty) shpqty, 0 lotqty
from usr_lipsparts a, invdtl b, shipment_line c, shipment d, stop e,
car_move f, trlr g
where a.prtnum=b.prtnum
and a.prt_client_id='----'
and a.prt_client_id=b.prt_client_id
and b.lotnum='LIPS'
and b.ship_line_id=c.ship_line_id
and c.ship_id=d.ship_id
and d.stop_id=e.stop_id
and e.car_move_id=f.car_move_id
and f.trlr_id=g.trlr_id
and (g.dispatch_dte is null
or g.dispatch_dte >= to_date('14-APR-2008','DD-MON-YYYY'))
group by a.prtnum
union
select a.prtnum, 0 untqty, 0 comqty, 0 avlqty, 0 shpqty, sum(b.untqty) lotqty
from usr_lipsparts a, invdtl b, invsub c, invlod d, locmst e
where a.prtnum=b.prtnum
and a.prt_client_id='----'
and b.lotnum='NOLOT'
and b.subnum=c.subnum
and c.lodnum=d.lodnum
and d.stoloc=e.stoloc
and (e.arecod='GWALL' or e.arecod='CFPP' 
or e.arecod like 'RACK%' or e.arecod like 'TOWER%'
or e.arecod='RSTG' or e.arecod='ISTG')
group by a.prtnum) b
where a.prtnum=b.prtnum
group by a.prtnum, a.reqqty;
