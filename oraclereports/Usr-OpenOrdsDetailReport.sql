/* #REPORTNAME= Open Sales Orders Details Update Report */
/* #HELPTEXT= This report updates the Open Sales */
/* #HELPTEXT= Orders Details reports with todays present information. */

column cponum heading 'PO Number'
column cponum format A20
column ordnum heading 'Order'
column ordnum format A12
column ordlin heading 'Order Line'
column ordlin format A10 
column prtnum heading 'Part Number'
column prtnum format A30
column status heading 'Status'
column status format A10
column shpqty heading 'Ship Qty'
column shpqty format 9,999,999,999
column opqty heading 'Open Qty'
column opqty format 9,999,999,999
column btcust heading 'Customer'
column btcust format A8
column ship_to_name  heading 'Name'
column ship_to_name  format A30
column early_shpdte heading 'Early'
column early_shpdte format A11
column cancel_date heading 'Late'
column cancel_date format A11
column rcvuom heading 'Unit of Measure'
column rcvuom format A15

/* First truncate table that houses information */

truncate table usr_openords_detail;

insert into usr_openords_detail(cponum,ordnum,ordlin,prtnum,status,shpqty,opqty,btcust, ship_to_name,early_shpdte,
cancel_date,rcvuom, whse_id)
SELECT
        distinct rtrim(ord.cponum) cponum, 
        rtrim(ord.ordnum) ordnum,  ord_line.ordlin, rtrim(ord_line.prtnum) prtnum,     
        decode(rtrim(dscmst.lngdsc),'Staged',decode(vc_print_dte,NULL,rtrim(dscmst.lngdsc),'Printed'),rtrim(dscmst.lngdsc)) status,
        shipment_line.shpqty shpqty, decode(shipment_line.linsts, 'I', (shipment_line.stgqty+shipment_line.inpqty+shipment_line.pckqty),shipment_line.pckqty) onhqty, 
        rtrim(ord.btcust) btcust, rtrim( substr(lookup1.adrnam,1,30))  Ship_To_Name, 
        rtrim(ord_line.early_shpdte,'MM/DD/YYYY') early_shpdte,     
        rtrim(ord_line.late_shpdte, 'MM/DD/YYYY') cancel_date,
        decode(prtmst.rcvuom, 'CS','Case','EA','Each','IP','Inner Pack','PA','Pallet',null) rcvuom, 'ROB' whse_id
       FROM pckbat, stop, dscmst, adrmst lookup1, adrmst lookup2, adrmst lookup3, cstmst, 
           shipment_line, shipment, ord_line, ord, prtmst
       WHERE ord.st_adr_id              = lookup1.adr_id
         AND ord.bt_adr_id               = lookup2.adr_id
        AND ord.rt_adr_id               = lookup3.adr_id
        and ord.stcust = cstmst.cstnum (+)
        and ord.client_id = cstmst.client_id (+)
        AND ord.client_id               = '----'
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = shipment_line.client_id
        AND ord_line.ordnum             = shipment_line.ordnum
        AND ord_line.ordlin             = shipment_line.ordlin
        AND ord_line.ordsln             = shipment_line.ordsln
        AND ord_line.prtnum             = prtmst.prtnum
        AND prtmst.prt_client_id        = '----'
        AND shipment_line.schbat        = pckbat.schbat (+)
        AND shipment_line.ship_id       = shipment.ship_id
        AND shipment.stop_id            = stop.stop_id (+)
        AND dscmst.colnam               = 'shpsts'
        AND dscmst.locale_id            = 'US_ENGLISH'
        AND shipment.shpsts             = dscmst.colval
        AND shipment.shpsts             <> 'C'
        AND rtrim(dscmst.lngdsc) <> 'Cancelled' 
UNION 
select cponum,ordnum,ordlin,prtnum,status,shpqty,opqty,btcust,ship_to_name,early_shpdte,
cancel_date,rcvuom, whse_id
from(select distinct  rtrim(ord.cponum) cponum , rtrim(ord.ordnum) ordnum, rtrim(ord_line.ordlin) ordlin,
rtrim(ord_line.prtnum) prtnum, ord_line.shpqty shpqty, ord_line.pckqty opqty, 
decode(varGetShpSts(ord.ordnum),'P','Pending','B',decode(cstmst.bckflg,0,'Backorder Cancel','Backorder'))status,
          rtrim(ord.btcust) btcust, rtrim(substr(lookup1.adrnam,1,30))  Ship_to_Name,
       rtrim(ord_line.early_shpdte,'MM/DD/YYYY') early_shpdte,
       rtrim(ord_line.late_shpdte, 'MM/DD/YYYY') cancel_date, 
decode(prtmst.rcvuom, 'CS','Case','EA','Each','IP','Inner Pack','PA','Pallet',null) rcvuom, 'ROB' whse_id
     from adrmst lookup1, adrmst lookup2, adrmst lookup3, cstmst, ord_line, ord, shipment, prtmst, (select shipment_line.* from shipment_line, ord_line where shipment_line.client_id=ord_line.client_id
and shipment_line.ordnum=ord_line.ordnum and shipment_line.ordlin=ord_line.ordlin
and shipment_line.ordsln=ord_line.ordsln)sl
    where
      ord.client_id = '----'
    and ord.ordnum = ord_line.ordnum
      and ord.client_id = ord_line.client_id
      and ord.st_adr_id = lookup1.adr_id
     AND ord.bt_adr_id               = lookup2.adr_id
        AND ord.rt_adr_id               = lookup3.adr_id
      and ord.stcust = cstmst.cstnum (+)
      and ord.client_id = cstmst.client_id (+)
      and ord_line.ordnum=sl.ordnum(+)
      and ord_line.prtnum=prtmst.prtnum
      and prtmst.prt_client_id='----'
      and ord_line.client_id=sl.client_id(+)
      and ord_line.ordlin=sl.ordlin(+)
      and ord_line.ordsln=sl.ordsln(+)
      and ord_line.pckqty > 0
      and sl.ship_id = shipment.ship_id (+)
      and shipment.shpsts (+) <> 'C' 
      and shipment.shpsts (+) <>  'B')
union
select cponum,ordnum,ordlin,prtnum,status,shpqty,opqty,btcust,ship_to_name,early_shpdte,
cancel_date,rcvuom, whse_id from
(select distinct rtrim(ord.cponum) cponum, rtrim(ord.ordnum) ordnum, rtrim(ord_line.ordlin) ordlin,
rtrim(ord_line.prtnum) prtnum, shipment_line.shpqty shpqty, shipment_line.shpqty opqty,
'Staged-CND' status, rtrim(ord.btcust) btcust,
rtrim(substr(adrnam,1,30))  ship_to_name,
rtrim(ord_line.early_shpdte,'MM/DD/YYYY') early_shpdte,
rtrim(ord_line.late_shpdte,'MM/DD/YYYY') cancel_date,
decode(prtmst.rcvuom, 'CS','Case','EA','Each','IP','Inner Pack','PA','Pallet',null) rcvuom, 'ROB' whse_id
from adrmst, cstmst, ord, ord_line, shipment_line, pckmov, pckwrk, shipment, stop,
car_move, trlr, prtmst
where ord.stcust = cstmst.cstnum(+)
and ord.client_id = cstmst.client_id(+)
and ord.st_adr_id = adrmst.adr_id
and ord.ordnum = ord_line.ordnum
and ord_line.ordnum = shipment_line.ordnum
and ord_line.ordlin = shipment_line.ordlin
and ord_line.ordsln = shipment_line.ordsln
and ord_line.prtnum = prtmst.prtnum
and prtmst.prt_client_id='----'
and ord_line.client_id = shipment_line.client_id
and shipment_line.ship_id = shipment.ship_id
and shipment.stop_id  = stop.stop_id
and stop.car_move_id  = car_move.car_move_id
and car_move.trlr_id
=trlr.trlr_id     and trlr.trlr_stat   in  ('O','L','H','C','P')
and shipment.loddte is not null
and shipment.ship_id  =
pckwrk.ship_id
and ord.ordnum = pckwrk.ordnum
and shipment.shpsts  in ('D', 'C')     and pckwrk.cmbcod
= pckmov.cmbcod     and pckmov.arecod    in ('SSTG','WIP SUPPLY'))
order by ordnum, ordlin;

commit;
