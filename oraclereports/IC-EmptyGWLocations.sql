/* #REPORTNAME=IC Empty GW Location Report */
/* #HELPTEXT= This report lists all empty GW locations. */
/* #HELPTEXT= Requested by Inventory Control. */

 ttitle left '&1' print_time center 'IC Empty GW Location Report ' -
    right print_date skip 2
   btitle skip 1 center 'Page: ' format 999 sql.pno

/* Changed from checking against invsum to invlod */

select locmst.stoloc 
from
locmst
where locmst.arecod    = 'GWALL' 
and locmst.locsts ='E' 
and locmst.pndqvl = 0 and locmst.curqvl = 0
and asgflg = 0
and useflg = 1
and not exists(select invlod.stoloc
from invlod where locmst.stoloc = invlod.stoloc)
order by locmst.stoloc
/
