/* #REPORTNAME=FIN-KPI Picks Report */
/* #VARNAM=trndte , #REQFLG=Y */
/* #VARNAM=End-Date, #REQFLG=Y */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding Piece or Full */
/* #HELPTEXT= Case Picks. Enter Date in format DD-MON-YYYY HH24:MI:SS */
/* #HELPTEXT= Requested by The Controller */
/* #GROUPS=NOONE */

/* Author: Al Driver */
/* The below script obtains information from both production and */
/* archive simultaneously by using a union. The date parameters should be input in */
/* military time.  Obtaining the breakdown of Piece Pick lines and */
/* Full Case lines is obtained by using two inline views.  Initially, */
/* two functions were used to perform this task, but every date broken down */
/* by the second, was passed to the function and it proved to be too time */
/* consuming.  Using the inline views allows us to get the records that */
/* pertain to the exact time frame more efficiently. For Piece Picks, the script looks */
/* for records without a subucc.  For Full cases the subucc is populated. */
/* The archive script eliminates any records with the same wrkref in production */  
/* to prevent duplicates from being counted on the same day. */

set pagesize 500
set linesize 150 


ttitle left print_time -
center 'FIN-KPI Picks From &&1 To &&2 ' -
right print_date skip 2 -


column uccdte heading 'Date'
column uccdte format A25
column total heading 'Total Lines Picked'
column total format  9999999 
column pcpck heading '# Piece Pick Lines'
column pcpck format 9999999 
column fullcs heading '# Full Case Lines'
column fullcs format 9999999 
column avgln heading 'Avg # Lines per Order'
column avgln format 9999999 

alter session set nls_date_format ='DD-MON-YYYY HH24:MI:SS';

select substr(b.lstdte,1,11) uccdte,count(a.ordlin) total,
    round(count(a.ordlin)/count(distinct a.ordnum)) avgln,pckpcktotal.lns2 pcpck, fullcstotal.lns2 fullcs
    from pckwrk a, invdtl b,(select substr(pcpck.dte,1,11) dte2,sum(pcpck.lns) lns2 from
            (select to_date(b.lstdte,'DD-MON-YYYY HH24:MI:SS') dte,count(a.ordlin) lns
                                from pckwrk a,invdtl b
                                where a.wrkref = b.wrkref
                                and a.ship_line_id = b.ship_line_id
                                and a.prt_client_id = b.prt_client_id
                                and a.prtnum = b.prtnum
                                and a.wrktyp = 'P'
                                and a.appqty  > 0                          /* means it was picked */ 
                                and a.subucc is null                   /* means its a carton */
                        and to_date(b.lstdte,'DD-MON-YYYY HH24:MI:SS')
                        between to_date('&&1','DD-MON-YYYY HH24:MI:SS') and to_date('&&2','DD-MON-YYYY HH24:MI:SS')
                   group by to_date(b.lstdte,'DD-MON-YYYY HH24:MI:SS')) pcpck
   group by substr(pcpck.dte,1,11)) pckpcktotal,
   (select substr(fullcs.dte,1,11) dte2,sum(fullcs.lns) lns2 from
           (select to_date(b.lstdte,'DD-MON-YYYY HH24:MI:SS') dte,count(a.ordlin) lns
                               from pckwrk a,invdtl b
                               where a.wrkref = b.wrkref
                                and a.prtnum = b.prtnum 
                                and a.ship_line_id = b.ship_line_id
                                and a.prt_client_id = b.prt_client_id
                                and a.wrktyp = 'P'
                                and a.appqty  > 0
                                and a.subucc is not null             /* means its a full case */
                         and to_date(b.lstdte,'DD-MON-YYYY HH24:MI:SS')
                        between to_date('&&1','DD-MON-YYYY HH24:MI:SS') and to_date('&&2','DD-MON-YYYY HH24:MI:SS')
                   group by to_date(b.lstdte,'DD-MON-YYYY HH24:MI:SS')) fullcs
   group by substr(fullcs.dte,1,11)) fullcstotal
   where a.wrkref = b.wrkref
   and a.prtnum = b.prtnum
   and a.ship_line_id = b.ship_line_id
   and a.prt_client_id = b.prt_client_id
   and a.wrktyp = 'P'
   and substr(b.lstdte,1,11) = pckpcktotal.dte2
   and substr(b.lstdte,1,11) = fullcstotal.dte2
   and a.appqty  > 0
   and b.lstdte between to_date('&&1','DD-MON-YYYY HH24:MI:SS') and to_date('&&2','DD-MON-YYYY HH24:MI:SS')
   group by substr(b.lstdte,1,11),pckpcktotal.lns2,fullcstotal.lns2
union
select substr(b.lstdte,1,11) uccdte,count(a.ordlin) total,
   round(count(a.ordlin)/count(distinct a.ordnum)) avgln,pckpcktotal.lns2 pcpck, fullcstotal.lns2 fullcs
   from pckwrk@arch a, invdtl@arch b,(select substr(pcpck.dte,1,11) dte2,sum(pcpck.lns) lns2 from
           (select to_date(b.lstdte,'DD-MON-YYYY HH24:MI:SS') dte,count(a.ordlin) lns
                               from pckwrk@arch a,invdtl@arch b
                               where a.wrkref = b.wrkref
                               and a.prtnum = b.prtnum
                               and a.ship_line_id = b.ship_line_id
                               and a.prt_client_id = b.prt_client_id
                               and a.wrktyp = 'P'
                               and a.appqty  > 0
                               and a.subucc is  null
                        and not exists(select 'x' from pckwrk where wrkref = a.wrkref)
                        and to_date(b.lstdte,'DD-MON-YYYY HH24:MI:SS')
                        between to_date('&&1','DD-MON-YYYY HH24:MI:SS') and to_date('&&2','DD-MON-YYYY HH24:MI:SS')
                   group by to_date(b.lstdte,'DD-MON-YYYY HH24:MI:SS')) pcpck
   group by substr(pcpck.dte,1,11)) pckpcktotal,
   (select substr(fullcs.dte,1,11) dte2,sum(fullcs.lns) lns2 from
           (select to_date(b.lstdte,'DD-MON-YYYY HH24:MI:SS') dte,count(a.ordlin) lns
                               from pckwrk@arch a,invdtl@arch b
                              where a.wrkref = b.wrkref
                              and a.prtnum = b.prtnum
                              and a.ship_line_id = b.ship_line_id
                              and a.prt_client_id = b.prt_client_id
                              and a.wrktyp = 'P'
                              and a.appqty  > 0
                              and a.subucc is not null
                        and not exists(select 'x' from invdtl where wrkref = a.wrkref)
                         and to_date(b.lstdte,'DD-MON-YYYY HH24:MI:SS')
                        between to_date('&&1','DD-MON-YYYY HH24:MI:SS') and to_date('&&2','DD-MON-YYYY HH24:MI:SS')
                   group by to_date(b.lstdte,'DD-MON-YYYY HH24:MI:SS')) fullcs
   group by substr(fullcs.dte,1,11)) fullcstotal
   where a.wrkref = b.wrkref
   and a.prtnum = b.prtnum
   and a.ship_line_id = b.ship_line_id
   and a.prt_client_id = b.prt_client_id
   and a.wrktyp = 'P'
   and substr(b.lstdte,1,11) = pckpcktotal.dte2
   and substr(b.lstdte,1,11) = fullcstotal.dte2
   and a.appqty  > 0
   and b.lstdte between to_date('&&1','DD-MON-YYYY HH24:MI:SS') and to_date('&&2','DD-MON-YYYY HH24:MI:SS')
  group by substr(b.lstdte,1,11),pckpcktotal.lns2,fullcstotal.lns2
/
