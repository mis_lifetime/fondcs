/* #REPORTNAME= ENG - Piece Picks By Date Range Report */
/* #VARNAM=Begin-Date , #REQFLG=Y */
/* #VARNAM=End-Date, #REQFLG=Y */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding Piece Picks by date range. */
/* #HELPTEXT= Enter Date in format DD-MON-YYYY  */
/* #HELPTEXT= Requested by Engineering */
/* #GROUPS=NOONE */

/* Author: Al Driver */
/* The below script obtains information from both production and */
/* archive simultaneously by using a union. Script provides requested */
/* information (item,qty,date,location & area) for piece picks only. */
/* Area should be from CFPP only. Records in pckwrk that have been  */
/* picked (appqty > 0) and have a null subucc are piece picks.  */
/* The lstdte or lstmov fields are the date the items were picked. */
/* The archive script eliminates any records with the same wrkref in production */  
/* to prevent duplicates from being counted on the same day. */

set pagesize 5000
set linesize 300 


ttitle left print_time -
center 'ENG -Piece Picks By Date Range Report From &&1 To &&2 ' -
right print_date skip 2 -

column trndte heading 'Month'
column trndte format A11
column prtnum heading 'Item#'
column prtnum format A20
column lngdsc heading 'Description'
column lngdsc format A30
column fr_arecod heading 'Area'
column fr_arecod format A10
column frstol heading 'Source Location'
column frstol format A20
column velzon heading 'Velocity'
column velzon format A10
column pckqty heading 'Qty Picked'
column pckqty format  9999999 
column volume heading 'Volume'
column volume format  999999.99
column caslen heading 'Case Length'
column caslen format 999999.99
column caswid heading 'Case Width'
column caswid format 999999.99
column cashgt heading 'Case Height'
column cashgt format 999999.99
column untlen heading 'Piece Length'
column untlen format 999999.99
column untwid heading 'Piece Width'
column untwid format 999999.99
column unthgt heading 'Piece Height'
column unthgt format 999999.99
column untcas heading 'Pieces per Case'
column untcas format 999999999
column caspal heading 'Cases per Pallet'
column caspal format 999999999

alter session set nls_date_format ='DD-MON-YYYY';


select (substr(dlytrn.trndte,4,3) ||' '|| substr(dlytrn.trndte,8,4)) trndte, 
       prtmst.prtnum,
       substr(prtdsc.lngdsc,1,30) lngdsc,
       dlytrn.fr_arecod,
       dlytrn.frstol,
       decode(prtmst.velzon,'A','FAST','B','MEDIUM','C','SLOW','D','DEAD','UNKNOWN') velzon,
       sum(dlytrn.trnqty) pckqty,
       round(sum(dlytrn.trnqty)/decode(prtmst.untcas,0,1,prtmst.untcas),2) volume,
       ftpmst.caslen,
       ftpmst.caswid,
       ftpmst.cashgt,
       ftpmst.untlen,
       ftpmst.untwid,
       ftpmst.unthgt,
       prtmst.untcas,
       (prtmst.untpal/decode(prtmst.untcas,0,1,prtmst.untcas)) caspal
from
       ftpmst,prtmst,prtdsc,dlytrn
       where ftpmst.ftpcod = prtmst.ftpcod
       and prtmst.prtnum||'|----' = prtdsc.colval
       and prtdsc.colnam = 'prtnum|prt_client_id'
       and prtmst.prtnum = dlytrn.prtnum
       and prtmst.prt_client_id = '----'
       and dlytrn.actcod ='PCEPCK'
       and dlytrn.fr_arecod ='CFPP'
       and substr(dlytrn.frstol,1,1) ='K'
       and trunc(dlytrn.trndte)
       between to_date('&&1','DD-MON-YYYY') and to_date('&&2','DD-MON-YYYY')
group by 
      substr(dlytrn.trndte,4,3),
      substr(dlytrn.trndte,8,4),
      dlytrn.fr_arecod,
      dlytrn.frstol,
      decode(prtmst.velzon,'A','FAST','B','MEDIUM','C','SLOW','D','DEAD','UNKNOWN') ,
      prtmst.prtnum,
      substr(prtdsc.lngdsc,1,30),
      ftpmst.caslen,
      ftpmst.caswid,
      ftpmst.cashgt,
      ftpmst.untlen,
      ftpmst.untwid,
      ftpmst.unthgt,
      prtmst.untcas,
      (prtmst.untpal/decode(prtmst.untcas,0,1,prtmst.untcas))
union
select (substr(dlytrn.trndte,4,3) ||' '  || substr(dlytrn.trndte,8,4)) trndte, 
       prtmst.prtnum,
       substr(prtdsc.lngdsc,1,30) lngdsc,
       dlytrn.fr_arecod,
       dlytrn.frstol,
       decode(prtmst.velzon,'A','FAST','B','MEDIUM','C','SLOW','D','DEAD','UNKNOWN') velzon,
       sum(dlytrn.trnqty) pckqty,
       round(sum(dlytrn.trnqty)/decode(prtmst.untcas,0,1,prtmst.untcas),2) volume,
       ftpmst.caslen,
       ftpmst.caswid,
       ftpmst.cashgt,
       ftpmst.untlen,
       ftpmst.untwid,
       ftpmst.unthgt,
       prtmst.untcas,
       (prtmst.untpal/decode(prtmst.untcas,0,1,prtmst.untcas)) caspal
from 
       ftpmst,prtmst,prtdsc,dlytrn@arch dlytrn
       where ftpmst.ftpcod = prtmst.ftpcod
       and prtmst.prtnum||'|----' = prtdsc.colval
       and prtdsc.colnam = 'prtnum|prt_client_id'
       and prtmst.prtnum = dlytrn.prtnum 
       and prtmst.prt_client_id = '----'
       and dlytrn.actcod ='PCEPCK'
       and dlytrn.fr_arecod ='CFPP'
       and substr(dlytrn.frstol,1,1) ='K' 
       and trunc(dlytrn.trndte)      
       between to_date('&&1','DD-MON-YYYY') and to_date('&&2','DD-MON-YYYY')
group by 
      substr(dlytrn.trndte,4,3),
      substr(dlytrn.trndte,8,4),
      dlytrn.fr_arecod,
      dlytrn.frstol,
      decode(prtmst.velzon,'A','FAST','B','MEDIUM','C','SLOW','D','DEAD','UNKNOWN') ,
      prtmst.prtnum,
      substr(prtdsc.lngdsc,1,30),
      ftpmst.caslen,
      ftpmst.caswid, 
      ftpmst.cashgt,  
      ftpmst.untlen, 
      ftpmst.untwid,  
      ftpmst.unthgt, 
      prtmst.untcas, 
      (prtmst.untpal/decode(prtmst.untcas,0,1,prtmst.untcas)) 
/
