/* #REPORTNAME=CC-FINAL */
/* #HELPTEXT = Requested by Inventory Control */
/* #HELPTEXT = Please enter the count bach */
/* #VARNAM=Start_Batch, #REQFLG=Y */
/* #VARNAM=End_Batch, #REQFLG=Y */ 

set pagesize 5000
set linesize 200


ttitle left print_time -
center 'Confirmed Errored Cycle Count Locations'-
right print_date skip 2 -

column count heading 'Count Type'
column count format A10
column stoloc heading 'Location'
column stoloc format A15
column arecod heading 'Area Code'
column arecod format  A15
column cntdte heading 'Counted Date'
column cntdte format A20
column hdate heading 'Adjusted Date'
column hdate format A20
column cntbat heading 'Count Batch     '
column cntbat format a15
column freeze heading 'Freeze'
column freeze format 999,999,999
column bcost heading 'Freeze Cost'
column bcost format 999,999.999
column fcost heading 'Final Cost'
column fcost format 999,999.999
column final heading 'Final  '
column final format 999,999,999
column adjusted heading 'Adjusted'
column adjusted format 999,999
column bprtnum heading 'Freeze Item   '
column bprtnum format a14
column fprtnum heading 'Final Item  '
column fprtnum format a14

alter session set nls_date_format ='dd-mon-yyyy';

select count, stoloc, arecod, cntbat, cntdte, hdate, freeze, final, adjusted, bprtnum, fprtnum,
        bcost, fcost
from (select distinct f.stoloc, substr(f.cntbat,1,20) cntbat,
                nvl(locmst.arecod,'DELETED') arecod, substr(nvl(f.prtnum,'EMPTY'),1,12) fprtnum,
                substr(nvl(g.prtnum,'EMPTY'),1,12) bprtnum,
                nvl(g.untqty,0) freeze,
                decode(f.prtnum,g.prtnum,f.untqty,nvl(f.cntqty,0)) final,
                nvl(h.untqty,0) adjusted,
                to_date(to_char(g.cntdte,'dd-mon-yyyy HH24:MI:SS'),'dd-mon-yyyy HH24:MI:SS') cntdte,
                to_date(to_char(h.cntdte,'dd-mon-yyyy HH24:MI:SS'),'dd-mon-yyyy HH24:MI:SS') hdate,
                nvl(f.untcst,0)fcost,
                nvl(g.untcst,0) bcost,'F' cnttyp, 'C' count
        from usr_cnthst f, usr_cnthst g , usr_cnthst h, locmst, usr_cnthst i
        where  f.cnttyp ='E' and f.cntmod = 'A'
                and f.stoloc = g.stoloc
                and f.stoloc = locmst.stoloc(+)
                and nvl(f.untqty,0) <> nvl(g.untqty,0)
                and f.cntbat = g.cntbat
                and f.prtnum = g.prtnum
                and g.stoloc=i.stoloc
                and g.cntbat=i.cntbat
                and g.prtnum=i.prtnum
                and i.cnttyp='E' and i.cntmod='C'
                and g.untqty=i.untqty
                and f.prt_client_id = g.prt_client_id
                and g.cnttyp = 'B' and g.cntmod ='C'
		and g.cntdte=(select max(z.cntdte) from usr_cnthst z where g.cntbat=z.cntbat
		and g.stoloc=z.stoloc and z.cnttyp='B' and z.cntmod='C')
                and f.stoloc = h.stoloc
                and  f.cntbat = h.cntbat
                and h.cnttyp ='I' and h.cntmod ='I'
                and f.prtnum = h.prtnum
                and f.prt_client_id = h.prt_client_id
union
select distinct j.stoloc, substr(j.cntbat,1,20) cntbat,
                nvl(locmst.arecod,'DELETED') arecod, nvl(j.prtnum,'EMPTY') fprtnum, nvl(g.prtnum,'EMPTY') bprtnum,
                nvl(g.untqty,0) freeze,
                nvl(j.untqty,0) final,
                nvl(h.untqty,0) adjusted,
                to_date(to_char(g.cntdte,'dd-mon-yyyy HH24:MI:SS'),'dd-mon-yyyy HH24:MI:SS') cntdte,
                to_date(to_char(h.cntdte,'dd-mon-yyyy HH24:MI:SS'),'dd-mon-yyyy HH24:MI:SS') hdate,
                nvl(j.untcst,0)fcost, decode(g.untqty,null,0,g.untcst) bcost,'F' cnttyp, 'C' count
        from usr_cnthst j, usr_cnthst g , usr_cnthst h, locmst, usr_cnthst i
        where  j.cnttyp ='E' and j.cntmod = 'A'
                 and j.stoloc = g.stoloc
                 and j.stoloc = locmst.stoloc(+)
                 and j.cntbat = g.cntbat
                 and j.prtnum <> g.prtnum and j.prtnum <> 'EMPTY'
                 and j.prt_client_id = g.prt_client_id
                and j.cntbat=i.cntbat
                and j.stoloc=i.stoloc
                and i.cnttyp='C' and i.cntmod='C'
                 and g.cnttyp = 'B' and g.cntmod ='C'
		and g.cntdte=(select max(z.cntdte) from usr_cnthst z where g.cntbat=z.cntbat
		and g.stoloc=z.stoloc and z.cnttyp='B' and z.cntmod='C')
                 and j.stoloc = h.stoloc and j.cntbat = h.cntbat
                 and h.cnttyp ='I' and h.cntmod ='I'
union
select distinct f.stoloc, substr(f.cntbat,1,20) cntbat,
                nvl(locmst.arecod,'DELETED') arecod, nvl(f.prtnum,'EMPTY') fprtnum, nvl(g.prtnum,'EMPTY') bprtnum,
                nvl(g.untqty,0) freeze,
                nvl(f.untqty,0) final,
                nvl(j.untqty,0) adjusted,
                to_date(to_char(g.cntdte,'dd-mon-yyyy HH24:MI:SS'),'dd-mon-yyyy HH24:MI:SS') cntdte,
                to_date(to_char(j.cntdte,'dd-mon-yyyy HH24:MI:SS'),'dd-mon-yyyy HH24:MI:SS') hdate,
                nvl(decode(f.prtnum,g.prtnum,f.untcst,f.untcst),0) fcost,
                nvl(g.untcst,0) bcost, 'F' cnttyp, 'C' count
       from usr_cnthst f, usr_cnthst g , usr_cnthst j, locmst, usr_cnthst i
        where  f.cnttyp ='E' and f.cntmod = 'A'
              and f.stoloc = g.stoloc
              and f.stoloc = locmst.stoloc(+)
              and f.cntbat = g.cntbat
              and g.cnttyp = 'B' and g.cntmod ='C'
		and g.cntdte=(select max(z.cntdte) from usr_cnthst z where g.cntbat=z.cntbat
		and g.stoloc=z.stoloc and z.cnttyp='B' and z.cntmod='C')
           and g.cntbat=i.cntbat
           and g.stoloc=i.stoloc
           and i.cnttyp='C' and i.cntmod='C'
           and i.prtnum=f.prtnum
           and nvl(f.untqty,0) <> nvl(g.untqty,0) and g.prtnum is null
              and  f.stoloc = j.stoloc and f.cntbat = j.cntbat
              and j.cnttyp ='I' and j.cntmod ='I'
union
select distinct f.stoloc, substr(f.cntbat,1,20) cntbat,
                nvl(locmst.arecod,'DELETED') arecod, nvl(f.prtnum,'EMPTY') fprtnum, nvl(g.prtnum,'EMPTY') bprtnum,
                nvl(g.untqty,0) freeze,
                nvl(f.cntqty,0) final,
                nvl(j.untqty,0) adjusted,
                to_date(to_char(g.cntdte,'dd-mon-yyyy HH24:MI:SS'),'dd-mon-yyyy HH24:MI:SS') cntdte,
                to_date(to_char(j.cntdte,'dd-mon-yyyy HH24:MI:SS'),'dd-mon-yyyy HH24:MI:SS') hdate,
                nvl(f.untcst,0) fcost, nvl(g.untcst,0) bcost, 'F' cnttyp, 'C' count
       from usr_cnthst f, usr_cnthst g , usr_cnthst j, locmst
        where  f.cnttyp ='E' and f.cntmod = 'A' and f.prtnum is null
                  and f.stoloc = g.stoloc
                  and f.stoloc = locmst.stoloc(+)
                  and f.cntbat = g.cntbat
                  and g.cnttyp = 'B' and g.cntmod ='C'
   	and g.cntdte=(select max(z.cntdte) from usr_cnthst z where g.cntbat=z.cntbat
		and g.stoloc=z.stoloc and z.cnttyp='B' and z.cntmod='C')
                  and  f.stoloc =j.stoloc and f.cntbat = j.cntbat
                  and j.cnttyp ='I' and j.cntmod ='I'
                  and nvl(j.untqty,0) <> nvl(g.untqty,0)
union
select distinct a.adj_ref2 stoloc, a.adj_ref1 cntbat, nvl(c.arecod,' ') arecod,
nvl(a.prtnum,'EMPTY') bprtnum, 'EMPTY' fprtnum,
0 freeze, nvl(a.trnqty,0) final, nvl(a.trnqty,0) adjusted,
to_date(to_char(a.trndte,'dd-mon-yyyy HH24:MI:SS'),'dd-mon-yyyy HH24:MI:SS') cntdte,
to_date(to_char(a.trndte,'dd-mon-yyyy HH24:MI:SS'),'dd-mon-yyyy HH24:MI:SS') hdate,
b.untcst fcost,0 bcost, 'F' cnttyp, 'M' count
from dlytrn a, prtmst b, locmst c
where a.adj_ref1 between '&1' and '&2'
and a.prtnum=b.prtnum
and a.adj_ref2=c.stoloc
and b.prt_client_id='----'
and a.adj_ref1 is not null
and a.adj_ref2 is not null
and a.orgcod='----'
and a.trndte >= (select min(cntdte) from usr_cnthst where cntbat='&1'))
where cntbat between '&1' and '&2'
/
