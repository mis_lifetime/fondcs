/* #REPORTNAME=IC 3PL Pending Orders Report */
/* #HELPTEXT= This report lists inventory part display */
/* #HELPTEXT= details regarding select items.  */
/* #HELPTEXT= Requested by Joe H.-Inventory Control */

 ttitle left  print_time center 'IC 3PL Pending Orders Report ' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column prtnum format A30 
column lotnum heading 'Lot Number'
column lotnum format A20 
column invsts heading 'Inventory Status'
column invsts format A16 
column untqty heading 'Unit Quantity'
column untqty format 999999
column comqty heading 'Committed Quantity'
column comqty format 999999
column avlqty heading 'Available Quantity'
column avlqty format 999999
column pndqty heading 'Pending Quantity'
column pndqty format 999999

set linesize 200 
set pagesize 100 

select prtnum,
           lotnum,
           invsts,
            untqty,
            comqty,
            avlqty,
            pndqty from var_prtqty_view
   where prtnum in
   ('70168-93','10409HL','58921','CM10440K','CM10440','CM10402','86724W','CM10404J','86724WJ',
   '70172-93','CM10441','70173-93','86738WJ','FCF14NA','541','TR15NS','86735B','FCXS6SK',
   '5270','FW21NSC','KB2991','FC4BRC','86723WJ','86732WJ','HF86003RM','542','5230','86738B',
   '86735BK','5220','CM12004','5250','97500-10','86732B','86723WM','CM12003','KB2993',
   'KBM2975','5121','CM12005','501SNO','58929SS','CM12002','5150','5190','70655','86723W',
   '86738BK','KM412WH','PRO15N','CM10504','5210','1835','5120','5151','5200','70170-93','818',
    '86720WJ','86723BM','86738W','CEP8N','CES8CRSKBX','CM10035L','CM12001','FM2F14BBL','FUS14NS',
    'KA1PB16NJ','KA1SR16TN','KB2990','KB2999K','KG306BU','KM425OB','KQX604BJT')
/
