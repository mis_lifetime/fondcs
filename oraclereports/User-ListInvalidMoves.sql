/* #REPORTNAME=User List Invalid Moves That Should Go To Shipping  */
/* #VARNAM=lodnum , #REQFLG=Y */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= information regarding invalid moves  */
/* #HELPTEXT= Requested by Order Management */
/* #GROUPS=NOONE */

set pagesize 50
set linesize 160


ttitle left print_time -
center 'User List Invalid Moves for &1 ' -
right print_date skip 2 -


column stoloc heading 'Location '
column stoloc format A30
column lodnum heading 'User'
column lodnum format  A20
column subnum heading 'Vehicle Type'
column subnum format A20
column dtlnum heading 'RFT   '
column dtlnum format A30
column mvloc heading 'Move Location      '
column mvloc format A24

select a.stoloc, b.lodnum, b.subnum, c.dtlnum, d.stoloc mvloc
from invlod a, invsub b, invdtl c, invmov d
where a.lodnum = b.lodnum
and b.subnum = c.subnum
and c.dtlnum = d.lodnum
and b.lodnum = '&1'
order by d.stoloc
/
