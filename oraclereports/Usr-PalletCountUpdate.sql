/* This script is used to update the usr_pallet_counts table */
/* which is used in the Yearly Receiving Report. */

-- Pallets Received 

insert into usr_pallet_counts(palcnt,clsdte)
select count(distinct invlod.lodnum),trunc(rcvtrk.clsdte)                            
       from trlr,rcvtrk,rcvinv,rcvlin, invsub,invlod,invdtl                                    
       where                                    
       rcvtrk.trknum = rcvlin.trknum 
       and rcvtrk.trlr_id = trlr.trlr_id                                                                    
       and rcvlin.trknum = rcvinv.trknum                                                                     
       and rcvlin.client_id = rcvinv.client_id                                                                     
       and rcvlin.supnum = rcvinv.supnum                                                                     
       and rcvlin.invnum = rcvinv.invnum                                                                     
       and rcvlin.rcvkey = invdtl.rcvkey(+)                                                               
       and invdtl.subnum = invsub.subnum                                                           
       and invsub.lodnum = invlod.lodnum                                                            
       --and rcvtrk.expdte is not null                                                            
       and trlr.trlr_stat = 'C'                                                            
       and trunc(rcvtrk.clsdte) = trunc(sysdate) -1
group by trunc(rcvtrk.clsdte);

commit;
