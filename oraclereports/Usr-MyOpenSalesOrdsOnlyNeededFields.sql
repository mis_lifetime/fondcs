set termout off;
set heading off;
set feedback off;
set linesize 120;
set pagesize 9999;

spool Usr-MyOpenSalesOrdsOnlyNeededFields.txt


select ordnum || '~' || ordlin || '~' || cponum || '~' || prtnum || '~' || unsamt || '~' || whse output_data
from
(
SELECT  distinct rtrim(ord.ordnum) ordnum, ord_line.ordlin, rtrim(ord.cponum) cponum, ord_line.prtnum,    
        decode(shipment_line.linsts, 'I', sum((shipment_line.stgqty+shipment_line.inpqty)), sum(shipment_line.pckqty)) unsamt,
        '1000' whse
       FROM pckbat, stop, dscmst, adrmst lookup1, adrmst lookup2, adrmst lookup3, cstmst, 
           shipment_line, shipment, ord_line, ord
       WHERE ord.st_adr_id              = lookup1.adr_id
         AND ord.bt_adr_id               = lookup2.adr_id
        AND ord.rt_adr_id               = lookup3.adr_id
        and ord.stcust = cstmst.cstnum (+)
        and ord.client_id = cstmst.client_id (+)
        AND ord.client_id               = '----'
        AND ord.ordnum                  = ord_line.ordnum
        AND ord.client_id               = ord_line.client_id
        AND ord_line.client_id          = shipment_line.client_id
        AND ord_line.ordnum             = shipment_line.ordnum
        AND ord_line.ordlin             = shipment_line.ordlin
        AND ord_line.ordsln             = shipment_line.ordsln
        AND shipment_line.schbat        = pckbat.schbat (+)
        AND shipment_line.ship_id       = shipment.ship_id
        AND shipment.stop_id            = stop.stop_id (+)
        AND dscmst.colnam               = 'shpsts'
        AND dscmst.locale_id            = 'US_ENGLISH'
        AND shipment.shpsts             = dscmst.colval
        AND shipment.shpsts             <> 'C'
        AND rtrim(dscmst.lngdsc) <> 'Cancelled'
        GROUP BY ord.ordnum, 
        ord_line.ordlin,
        ord.cponum,
        ord_line.prtnum,   
        shipment_line.linsts     
UNION
(select distinct  rtrim(ord.ordnum) ordnum,
        ord_line.ordlin,
        rtrim(ord.cponum) cponum, 
        ord_line.prtnum,    
          sum(ord_line.pckqty) unsamt,'1000' whse
FROM adrmst lookup1, adrmst lookup2, adrmst lookup3, cstmst, ord_line, ord, shipment,
(select shipment_line.* from shipment_line, ord_line where shipment_line.client_id=ord_line.client_id
and shipment_line.ordnum=ord_line.ordnum and shipment_line.ordlin=ord_line.ordlin
and shipment_line.ordsln=ord_line.ordsln)sl
    where
      ord.client_id = '----'
    and ord.ordnum = ord_line.ordnum
      and ord.client_id = ord_line.client_id
      and ord.st_adr_id = lookup1.adr_id
     AND ord.bt_adr_id               = lookup2.adr_id
        AND ord.rt_adr_id               = lookup3.adr_id
      and ord.stcust = cstmst.cstnum (+)
      and ord.client_id = cstmst.client_id (+)
     and ord_line.ordnum=sl.ordnum(+)
      and ord_line.client_id=sl.client_id(+)
      and ord_line.ordlin=sl.ordlin(+)
      and ord_line.ordsln=sl.ordsln(+)
      and ord_line.pckqty > 0
      and sl.ship_id = shipment.ship_id (+)
      and shipment.shpsts (+) <> 'C'
      and shipment.shpsts (+) <>  'B' 
    group by ord.ordnum,
        ord_line.ordlin,
        ord.cponum,  
        ord_line.prtnum)
union
select ordnum, ordlin, cponum, prtnum, sum(shpqty) unsamt, whse
from
(select distinct rtrim(ord.ordnum) ordnum, ord_line.ordlin ordlin, rtrim(ord.cponum) cponum, ord_line.prtnum prtnum, shipment_line.shpqty, '1000' whse
from adrmst, cstmst, ord, ord_line, shipment_line, pckmov, pckwrk, shipment, stop,
car_move, trlr
where ord.stcust = cstmst.cstnum(+)
and ord.client_id = cstmst.client_id(+)
and ord.st_adr_id = adrmst.adr_id
and ord.ordnum = ord_line.ordnum
and ord_line.ordnum = shipment_line.ordnum
and ord_line.ordlin = shipment_line.ordlin
and ord_line.ordsln = shipment_line.ordsln
and ord_line.client_id = shipment_line.client_id
and shipment_line.ship_id = shipment.ship_id
and shipment.stop_id  = stop.stop_id
and stop.car_move_id  = car_move.car_move_id
and car_move.trlr_id
=trlr.trlr_id     and trlr.trlr_stat   in  ('O','L','H','C','P')
and shipment.loddte is not null
and shipment.ship_id  =
pckwrk.ship_id
and ord.ordnum = pckwrk.ordnum
and shipment.shpsts  in ('D', 'C')     and pckwrk.cmbcod
= pckmov.cmbcod     and pckmov.arecod    in ('SSTG','WIP SUPPLY'))
group by ordnum,
        ordlin,
        cponum,  
        prtnum,
        whse);

spool off;
set termout on;
/
