set linesize 100
set pagesize 30000

alter session set nls_date_format = 'dd-mon-yyyy';

/********************************************************************************/
/* #REPORTNAME=PI Non-Countable Locations Report                                */
/* #HELPTEXT= This report will identify records with inventory that exist       */
/* #HELPTEXT= in non-countable locations (Excluding Ship Stage areas).          */
/* #HELPTEXT= Requested for Physical Inventory                                  */
/********************************************************************************/

/* Author: Albert Driver  */


ttitle left  print_time Center 'PI Non-Countable Locations Report' skip 2 -
        left print_date skip 1 -
btitle  left 'Page: ' format 999 sql.pno

column arecod heading 'Area Code'
column arecod format a20
column stoloc heading 'Location'
column stoloc format a20
column prtnum heading 'Item'
column prtnum format a30
column untqty heading 'Unit Quantity'
column untqty format 9999999 


select a.arecod, b.stoloc, c.prtnum, sum(c.untqty) untqty
from aremst a, invlod b, invdtl c, invsub d, locmst e
where
a.arecod =e.arecod
and a.arecod <> 'SSTG' and a.arecod <> 'SHIP'
and a.cntflg  = 0
and e.stoloc = b.stoloc
and b.lodnum = d.lodnum
and d.subnum = c.subnum
group by a.arecod, b.stoloc, c.prtnum
/
