/* #REPORTNAME=IC Work Order Scrapped Detail Report */
/* #HELPTEXT= This report lists all requested */
/* #HELPTEXT= work order information concerning scrapped inventory.  */
/* #HELPTEXT= Date to be entered in the format DD-MON-YYYY */
/* #HELPTEXT= Requested by Inventory Control */
/* #GROUPS=NOONE */
/* #VARNAM=Begin-Date , #REQFLG=Y */
/* #VARNAM=End-Date , #REQFLG=Y */

ttitle left print_time -
center 'IC Workorder Scrapped Detail ' -
right print_date skip 2 -

set pagesize 500
set linesize 440

column clsdte heading 'Tran Date'
column clsdte format A11
column prtnum heading 'Item'
column prtnum format A30
column rpt_scpqty heading 'Adj Quantity'
column rpt_scpqty format  999999
column cost heading 'Adj Amount'
column cost format 999999.99 
column wkonum heading 'From Location'
column wkonum format A20 
column dstloc heading 'To Location'
column dstloc format A11 
column mod_usr_id heading 'User ID'
column mod_usr_id format A20 
column untcst heading 'Frozen Cost'
column untcst format 999999.99

alter session set nls_date_format ='dd-mon-yyyy';

select * from usr_workorder_scrap
where trunc(clsdte) between '&1' and '&2'
order by clsdte
/ 
