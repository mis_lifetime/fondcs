/* #REPORTNAME=Updates Untcas for an Order Report */
/* #VARNAM=Order-Number, #REQFLG=Y */
/* #VARNAM=Order-Line , #REQFLG=Y */
/* #VARNAM=UNTCAS,      #REQFLG=Y */
/* #HELPTEXT= This report Untcas for an Order. */
/* #HELPTEXT= Order Number, Order line, Untcas must be entered*/

ttitle left  print_time -
       center 'Report to Change untcas for an Order Report' -
       right print_date skip 1 -
btitle skip 1 center 'Page: ' format 999 sql.pno

alter session set nls_date_format ='dd-mon-yyyy';

set linesize 200

update ord_line set untcas = '&3' where ordnum ='&1' and ordlin = '&2' and client_id ='----' ; 
commit;
select ordnum, prtnum, ordlin, untcas, untpak from ord_line where ordnum ='&1' and ordlin = '&2' and client_id ='----' ;
/
