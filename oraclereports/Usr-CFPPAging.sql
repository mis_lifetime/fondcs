/* #REPORTNAME=CFPP Aging Report */
/* #VARNAM=aged_days , #REQFLG=Y */
/* #HELPTEXT= This report lists all requested information */
/* #HELPTEXT= regarding items in the CFPP area that are */
/* #HELPTEXT= discontinued. Input a number for the amount of aged days.*/
/* #HELPTEXT=  Ex: 21 = More than 3 weeks old . */ 
/* #HELPTEXT= Requested by Senior Management */
/* #GROUPS=NOONE */


ttitle left print_time center 'CFPP Aging Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column prtnum heading 'Part Number'
column prtnum format a12
column lngdsc heading 'Description'
column lngdsc format a30
column vc_prdcod heading 'Prod Class'
column vc_prdcod format a10
column stoloc heading 'Location'
column stoloc format a20
column untqty heading 'Qty'
column untqty format 999999999
column untcst heading 'Avg Landed Cost'
column untcst format 9999.00
column lndcst heading 'Landed Cost'
column lndcst format 9999.00

alter session set nls_date_format ='DD-MON-YYYY';

set pagesize 2000
set linesize 200

SELECT invsum.stoloc,prtmst.prtnum,substr(pd.lngdsc,1,30) lngdsc, sum(invdtl.untqty) untqty,prtmst.untcst, 
    a.landedcst lndcst, prtmst.vc_prdcod
    from prtdsc pd,prtmst,usr_itemcosts a,invsum,invlod,invsub,invdtl
    where pd.colval = prtmst.prtnum||'|----' 
    and pd.colnam = 'prtnum|prt_client_id'
    and pd.locale_id = 'US_ENGLISH' 
    and prtmst.prtnum = a.prtnum(+)
    and prtmst.prtnum = invsum.prtnum
    and prtmst.prt_client_id = invsum.prt_client_id
    and prtmst.prtnum = invdtl.prtnum
    and prtmst.prt_client_id = invdtl.prt_client_id
    and invsum.stoloc = invlod.stoloc
    and invsum.prtnum = invdtl.prtnum
    and invsum.prt_client_id = invdtl.prt_client_id
    and invlod.lodnum = invsub.lodnum
    and invsub.subnum = invdtl.subnum
    and substr(prtmst.vc_prdcod,2,1) ='X'
    and invsum.arecod ||'' = ('CFPP') 
    and decode(trunc(invdtl.lstdte),NULL,trunc(invdtl.adddte),trunc(invdtl.lstdte))           
    <= trunc(sysdate) - nvl('&1',0)
group by invsum.stoloc,prtmst.prtnum,substr(pd.lngdsc,1,30),prtmst.untcst
,a.landedcst,prtmst.vc_prdcod
/
