/* #REPORTNAME=IC Tower FCP On-Hand Report */
/* #HELPTEXT= This report lists all locations in TowerFCP */
/* #HELPTEXT= and TowerFCC                               */
/* #HELPTEXT= Requested by Inventory Control             */

 ttitle left '&1' print_time center 'IC Tower FCP On-Hand Report ' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column stoloc heading 'Location'
column stoloc format a15
column prtnum heading 'Item'
column prtnum format a12
column untqty heading 'Unit Qty'
column untqty format 999999
column comqty heading 'Comm. Qty'
column comqty format 999999
column pndqty heading 'Pend. Qty'
column pndqty format 999999
column arecod heading 'Area'
column arecod format a10

select distinct invsum.arecod, invsum.stoloc, 
       invsum.prtnum,
       invsum.untqty,
       invsum.comqty,
       invsum.pndqty,
       invsum.invsts
 from invsum
       where 
       invsum.arecod in ('TOWERFCP','TOWERREP')      
       order by invsum.stoloc;
