/* #REPORTNAME= FIN-Shipping Volume By Customer Report */
/* #HELPTEXT= This report generates shipping volume by */
/* #HELPTEXT= customer.  Customer # and date in the */
/* #HELPTEXT= format DD-MON-YYYY, is required. */
/* #HELPTEXT= Requested by The Controller             */

/* #VARNAM=btcust , #REQFLG=Y */
/* #VARNAM=From , #REQFLG=Y */
/* #VARNAM=To , #REQFLG=Y */

set linesize 100

ttitle left print_time -
center 'FIN-Shipping Volume By Customer Report  ' -
right print_date skip 1- 
center 'Customer: ' '&&1' skip 1 -
                center 'From: ' ' &&2 ' ' To: ' ' &&3 ' skip 2

/* Author: Al Driver */
/* report uses similar scripts used in KPI report */
/* for full case (FUL), piece pick (PCK), */
/* units shipped (UNS) and cubic volume (CUE) */

column full heading 'Full Case CTNs'
column full format 999999999 
column piece heading 'Piece Pick CTNs'
column piece format 999999999 
column qube heading 'Cubic Volume'
column qube format 999999999 
column units heading 'Units'
column units format 999999999 

alter session set nls_date_format ='DD-MON-YYYY';

select count(distinct decode(substr(subnum,1,1),'S',subnum,null)) full,
count(distinct decode(substr(subnum,1,1),'C',subnum,null)) piece,
sum((x.shpqty/x.untcas)*(x.cube/1728)) qube,sum(shpqty) units
                     from
                     (select distinct i.subnum, shipment.ship_id, i.prtnum, ord.ordnum,
                                          i.dtlnum,i.untqty shpqty,
                          (ftpmst.caslen * ftpmst.caswid * ftpmst.cashgt) cube, prtmst.untcas, trunc(tr.dispatch_dte) dispatch_dte
                                      from invdtl i,ftpmst, prtmst, shipment, shipment_line,  ord, ord_line,
                                      stop, trlr tr,car_move cr
                                      where i.ship_line_id = shipment_line.ship_line_id
                                      AND ftpmst.ftpcod = prtmst.prtnum
                                      AND prtmst.prtnum = ord_line.prtnum
                                      AND ord.ordnum                  = ord_line.ordnum
                                      AND ord.client_id               = ord_line.client_id
                                      AND ord_line.client_id          = shipment_line.client_id
                                      AND ord_line.ordnum             = shipment_line.ordnum
                                      AND ord_line.ordlin             = shipment_line.ordlin
                                      AND ord_line.ordsln             = shipment_line.ordsln
                                      AND shipment_line.ship_id       = shipment.ship_id
                                      AND shipment.stop_id                  = stop.stop_id
                                      AND stop.car_move_id            = cr.car_move_id
                                      AND cr.trlr_id                  = tr.trlr_id
                                      AND shipment.shpsts  ||''= 'C'
                                      AND ord.ordtyp                   != 'W'
                                      AND btcust = '&&1'
                                      AND trunc(dispatch_dte) between '&&2' and '&&3'
                     union
                     select distinct i.subnum, shipment.ship_id, i.prtnum, ord.ordnum,
                                          i.dtlnum,i.untqty shpqty,
                          (ftpmst.caslen * ftpmst.caswid * ftpmst.cashgt) cube, prtmst.untcas, trunc(tr.dispatch_dte) dispatch_dte
                                      from invdtl@arch i,ftpmst, prtmst,
                                      shipment@arch, shipment_line@arch,  ord@arch,ord_line@arch,
                                      stop@arch, trlr@arch tr,car_move@arch cr
                                      where i.ship_line_id = shipment_line.ship_line_id
                                      AND ftpmst.ftpcod = prtmst.prtnum
                                      AND prtmst.prtnum = ord_line.prtnum
                                      AND ord.ordnum                  = ord_line.ordnum
                                      AND ord.client_id               = ord_line.client_id
                                      AND ord_line.client_id          = shipment_line.client_id
                                      AND ord_line.ordnum             = shipment_line.ordnum
                                      AND ord_line.ordlin             = shipment_line.ordlin
                                      AND ord_line.ordsln             = shipment_line.ordsln
                                      AND shipment_line.ship_id       = shipment.ship_id
                                      AND shipment.stop_id                  = stop.stop_id
                                      AND stop.car_move_id            = cr.car_move_id
                                      AND cr.trlr_id                  = tr.trlr_id
                                      AND shipment.shpsts  ||''= 'C'
                                      AND ord.ordtyp                   != 'W'
                                      AND btcust = '&&1'
                                      AND trunc(dispatch_dte) between '&&2' and '&&3') x

/
