/* #REPORTNAME=PI Multiple Items In A Location Report */
/* #HELPTEXT= This report displays multiple items in a location */
/* #HELPTEXT= Requested by Inventory Control */


ttitle left '&1' print_time center 'PI Multiple Items in a Location Report' -
    right print_date skip 2
    btitle skip 1 center 'Page: ' format 999 sql.pno

column stoloc heading 'Location '
column stoloc format a12
column prtnum heading 'Item'
column prtnum format a12
column invsts heading 'Status'
column invsts format a6
column lotnum heading 'Lot Code'
column lotnum format a8
column untqty heading ' Qty'
column untqty format 999999
column lodnum heading 'Pallet Id  '
column lodnum format a30

set pagesize 3000
set linesize 200

truncate table trvseq;

insert into trvseq(select il.stoloc, null, 0,0, null, 0, null,null,null,null,null,0
from invlod il, invsub ib, invdtl id, locmst l, aremst a
where il.lodnum = ib.lodnum
and ib.subnum = id.subnum
and il.stoloc = l.stoloc
 and l.arecod = a.arecod
 and (l.arecod in ('CFPP','GWALL') or
a.loccod = 'P') and a.pckcod != 'N'
group by il.stoloc, l.arecod
having count(distinct prtnum ) > 1);

select a.stoloc, b.lodnum, c.prtnum, c.invsts, c.lotnum, c.untqty
from trvseq a, invlod b, invsub d, invdtl c
where a.stoloc = b.stoloc
and b.lodnum = d.lodnum
and d.subnum = c.subnum
order by a.stoloc
/
