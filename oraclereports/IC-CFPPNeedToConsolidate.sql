
/* #REPORTNAME=IC CFPP Locations Needed to Consolidate Report */
/* #HELPTEXT= This report generates locations that need  */
/* #HELPTEXT= to be consolidated in CFPP, but only lists items */
/* #HELPTEXT= that have multple locations. */
/* #HELPTEXT= Requested by Inventory Control */


ttitle left print_time center 'IC CFPP Locations Needed to Consolidate Report' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column arecod heading 'Area'
column arecod format a12
column stoloc heading 'Location'
column stoloc format a12
column untqty heading 'Unit Qty'
column untqty format 999999
column comqty heading 'Com. Qty'
column comqty format 999999
column prtnum heading 'Item'
column prtnum format a12
column invsts heading 'Status'
column invsts format a12
column asgflg heading 'Assigned'
column asgflg format a8
column untpal heading 'Unt. Pal'
column untpal format 999999
column untcas heading 'Unt. Case'
column untcas format 999999
column untpak heading 'Unt. Pak'
column untpak format 999999
column locpak heading 'Location Untpak'
column locpak format 999999
column loccas heading 'Location Untcas'
column loccas format 999999
column lotnum heading 'Lot Number'
column lotnum format a5
column fullcase heading 'Actual # of Cases'
column fullcase format 999999.99
column pcntfull heading 'Percent Full'
column pcntfull format 999.99999

set linesize 500;
set pagesize 5000;


select distinct invsum.arecod, invsum.stoloc, invsum.untqty, invsum.comqty, invsum.prtnum, invsum.untpak,  invdtl.lotnum,  decode(locmst.asgflg,1,'Y','N') asgflg, invdtl.invsts, invsum.untcas,
  prtmst.untpal,
  (invsum.untqty/prtmst.untcas) fullcase, (invsum.untqty/prtmst.untpal) pcntfull  from prtmst, invsum, locmst, invlod, invsub, invdtl 
  where invsum.arecod ||''= 'CFPP' 
  and (invsum.untqty/prtmst.untpal) <= .8 and (invsum.untqty/prtmst.untpal) ! = 0
  and prtmst.prtnum = invsum.prtnum
  and prtmst.prt_client_id = invsum.prt_client_id
  and prtmst.prtnum = invdtl.prtnum
  and prtmst.prt_client_id = invdtl.prt_client_id
  and invsum.stoloc = invlod.stoloc
  and invsum.stoloc = locmst.stoloc
  and invsum.prtnum = invdtl.prtnum
  and invsum.prt_client_id = invdtl.prt_client_id
  and invlod.lodnum = invsub.lodnum
  and invsub.subnum = invdtl.subnum        
  and exists (select count(a.stoloc) from invsum a
where a.prtnum = invsum.prtnum and a.arecod = 'CFPP'
having count(a.stoloc) > 1)        
order by invsum.prtnum
/
