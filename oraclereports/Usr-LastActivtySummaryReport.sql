/* #REPORTNAME=Usr-Last Activity Summary Report */
/* #HELPTEXT= This report lists the last activity   */
/* #HELPTEXT= for all items in our system. */
/* #HELPTEXT= Enter Dates in format DD-MON-YYYY. */
/* #HELPTEXT= Requested by Senior Management */

/* #VARNAM=created_dte , #REQFLG=N */
/* #VARNAM=last_used_dte , #REQFLG=N */
/* #VARNAM=qty_on_hand , #REQFLG=N */

column prtnum heading 'Item#'
column prtnum format  a14
column lngdsc heading 'Description'
column lngdsc format  a30
column vc_itmtyp heading 'ItmTyp'
column vc_itmtyp format  a7
column vc_itmgrp heading 'ItmGrp'
column vc_itmgrp format  a10
column vc_prdcod heading 'ItmCls'
column vc_prdcod format a10
column dis heading 'Disc?'
column dis format a5
column untcst heading 'Cost'
column untcst format 999999.99
column invsts heading 'ItemSts'
column invsts format a7 
column qty heading 'OnHandQty'
column qty format  9999999
column moddte heading 'CreatedDte'
column moddte format a12
column lstrcv heading 'LastRecdDte'
column lstrcv format a12
column lstuse heading 'LastUseDate'
column lstuse format a12

alter session set nls_date_format ='DD-MON-YYYY';
set linesize 300
set pagesize 2000

select a.prtnum, substr(f.lngdsc,1,30) lngdsc, c.vc_itmtyp, c.vc_itmgrp, c.vc_prdcod, 
decode(substr(c.vc_prdcod,2,1),'X','Y','N') dis, c.untcst,
b.invsts, nvl(sum(b.untqty-b.comqty),0) qty, c.moddte, c.lstrcv, usrGetLastActivityDate(a.lstshp,a.lstprd) lstuse
from usr_shp_hdr a, invsum b, prtmst c, locmst d, aremst e, prtdsc f
where a.prtnum=b.prtnum
and a.prtnum=c.prtnum
and c.prt_client_id='----'
and b.prt_client_id=c.prt_client_id
and a.prtnum||'|----'=f.colval
and f.colnam='prtnum|prt_client_id'
and f.locale_id='US_ENGLISH'
and b.stoloc=d.stoloc
and d.arecod=e.arecod
and e.fwiflg=1
and c.moddte <= nvl(to_date('&&1','DD-MON-YYYY'),sysdate)
and usrGetLastActivityDate(a.lstshp,a.lstprd) <= nvl(to_date('&&2','DD-MON-YYYY'),sysdate)
group by a.prtnum, substr(f.lngdsc,1,30), c.vc_itmtyp, c.vc_itmgrp, c.vc_prdcod, 
b.invsts, c.untcst, c.moddte, c.lstrcv, a.lstshp, a.lstprd
having nvl(sum(b.untqty -b.comqty),0) <= nvl('&&3',100000000)
union
select a.prtnum, substr(d.lngdsc,1,30) lngdsc, e.vc_itmtyp, e.vc_itmgrp,
e.vc_prdcod, decode(substr(e.vc_prdcod,2,1),'X','Y','N') dis, e.untcst, a.invsts,
nvl(sum(a.untqty-a.comqty),0) qty, e.moddte, e.lstrcv, a.new_expire_dte lstuse
from invsum a, locmst b, aremst c, prtdsc d, prtmst e
where a.prtnum not in (select prtnum from usr_shp_hdr)
and a.prt_client_id='----'
and a.stoloc=b.stoloc
and b.arecod=c.arecod
and a.prtnum=e.prtnum
and a.prt_client_id=e.prt_client_id
and c.fwiflg=1
and a.prtnum||'|----'=d.colval
and d.colnam='prtnum|prt_client_id'
and d.locale_id='US_ENGLISH'
and e.moddte <= nvl(to_date('&&1','DD-MON-YYYY'),sysdate)
group by a.prtnum, substr(d.lngdsc,1,30), e.vc_itmtyp, e.vc_itmgrp,
e.vc_prdcod, e.untcst, a.invsts, e.moddte, e.lstrcv, a.new_expire_dte
having nvl(sum(a.untqty -a.comqty),0) <= nvl('&&3',100000000);
