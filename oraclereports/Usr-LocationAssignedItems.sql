
/* #REPORTNAME=User Location Assigned Items Report */
/* #VARNAM=arecod , #REQFLG=Y */
/* #HELPTEXT= Enter Area */
/* #HELPTEXT = Requested by Senior Management */

ttitle left print_time center 'User Location Assigned Items' -
       right print_date skip 2
btitle skip 1 center 'Page: ' format 999 sql.pno

column stoloc heading 'Location'
column stoloc format a20
column arecod heading 'Area'
column arecod format a20
column locsts heading 'Status'
column locsts format a6
column velzon heading 'Vel Zone'
column velzon format a8
column wrkzon heading 'Work Zone'
column wrkzon format a10
column trvseq heading 'Travel Sequence'
column trvseq format a20
column rescod heading 'Resource Code'
column rescod format a50
column lochgt heading 'Height'
column lochgt format 999999
column loclen heading 'Length'
column loclen format 999999
column locwid heading 'Width'
column locwid format 999999
column locvrc heading 'Ver. Code'
column locvrc format a10
column maxqvl heading 'Max Qvl'
column maxqvl format 999999
column curqvl heading 'Cur Qty'
column curqvl format 999999
column pndqvl heading 'Pend. Qty'
column pndqvl format 999999
column trfpct heading 'Top Off %'
column trfpct format 999999
column erfpct heading 'Emer Replen %'
column erfpct format 999999
column useflg heading 'Use'
column useflg format 99
column stoflg heading 'Storage'
column stoflg format 99
column pckflg heading 'Pickable'
column pckflg format 99
column repflg heading 'Replen'
column repflg format 99
column asgflg heading 'Assign'
column asgflg format 99
column cipflg heading 'Counts in Progress'
column cipflg format 99
column cntseq heading 'Count Seq'
column cntseq format a20
column numcnt heading '# of Counts'
column numcnt format 9999999999
column abccod heading 'ABC Code'
column abccod format a8
column cntdte heading 'Count Date'
column cntdte format a15
column devcod heading 'Device Code'
column devcod format a10
column lokcod heading 'Loc. Lock Code'
column lokcod format a15
column locacc heading 'Location Access'
column locacc format a15
column lstdte heading 'Last Activity Date/Time'
column lstdte format a20
column lstcod heading 'List Code'
column lstcod format a10
column lst_usr_id heading 'Last User'
column lst_usr_id format a10  
column prtnum heading 'In Location'
column prtnum format a12
column assigned heading 'Assigned'
column assigned format a12
column onhand heading 'On Hand'
column onhand format 999999


set linesize 700
set pagesize 1000

select locmst.arecod,
locmst.stoloc,
locmst.locsts,
locmst.velzon,
locmst.wrkzon,
locmst.trvseq,
locmst.rescod,
locmst.lochgt,
locmst.loclen,
locmst.locwid,
locmst.locvrc,
locmst.maxqvl,
locmst.curqvl,
locmst.pndqvl,
locmst.trfpct,
locmst.erfpct,
locmst.useflg,
locmst.stoflg,
locmst.pckflg,
locmst.repflg,
locmst.asgflg,
locmst.cipflg,
locmst.cntseq,
locmst.numcnt,
locmst.abccod,
locmst.cntdte,
locmst.devcod,
locmst.lokcod,
locmst.locacc,
locmst.lstdte,
locmst.lstcod,
locmst.lst_usr_id,
invsum.prtnum,
(invsum.untqty-invsum.comqty) onhand,
rplcfg.prtnum assigned
from 
locmst, invsum, rplcfg
where locmst.arecod=invsum.arecod(+)
and locmst.stoloc=invsum.stoloc(+)
and locmst.arecod='&1'
and locmst.stoloc=rplcfg.stoloc(+)
order by locmst.arecod, locmst.stoloc
/
