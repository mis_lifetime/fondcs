/* #REPORTNAME=IC P&D Workqueue Validation Report */
/* #HELPTEXT= This report lists all P&D on-hand inventory */
/* #HELPTEXT= not in the workqueue */
/* #HELPTEXT= Requested by Inventory Control */
/* #GROUPS=NOONE */

set pagesize 50000
set linesize 160


ttitle left print_time -
center 'IC P&D Workqueue Validation ' -
right print_date skip 2 -


column stoloc heading 'Location '
column stoloc format A20
column lodnum heading 'Load Number'
column lodnum format  A20
column untqty heading 'Qty'
column untqty format 999999


select a.stoloc, a.lodnum, d.lotnum, sum(d.untqty) untqty
from invlod a, locmst b, invsub c, invdtl d
where a.stoloc = b.stoloc
and b.arecod ='ISTG'
and a.lodnum = c.lodnum
and c.subnum = d.subnum
and a.stoloc not in (select c.srcloc from wrkque c
where a.stoloc = c.srcloc)
group by a.stoloc, a.lodnum, d.lotnum
/
