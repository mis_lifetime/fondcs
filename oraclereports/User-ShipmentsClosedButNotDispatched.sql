set linesize 300
set pagesize 50000

alter session set nls_date_format = 'dd-mon-yyyy';

/* #REPORTNAME=User Shipments Closed But Not Dispatched Report */
/* #HELPTEXT= This report lists all shipments that are closed */
/* #HELPTEXT= but not dispatched                             */
/* #HELPTEXT= Requested by Senior Management - October 2002 */
/* #GROUPS=NOONE */

column trlr_num heading 'Trailer'
column trlr_num format a12
column trlr_stat heading 'Trailer Status'
column trlr_stat format a15
column carcod heading 'Carrier'
column carcod format a12
column ship_id heading 'Ship Id'
column ship_id format a12
column stgdte heading 'Staged'
column stgdte format a12
column loddte heading 'Shipped'
column loddte format a12
column early_shpdte heading 'Early'
column early_shpdte format a12
column late_shpdte heading 'Late'
column late_shpdte format a12
column stop_id heading 'Stop Id'
column stop_id format a12
column dispatch_dte heading 'Dispatched'
column dispatch_dte format a12
column car_move_id heading 'Car Move Id'
column car_move_id format a12
column prtnum heading 'Item'
column prtnum format a12



select distinct trlr.trlr_num,  
trlr.trlr_stat, shipment.carcod,
shipment.ship_id, pckwrk.prtnum, shipment.stgdte,  
shipment.loddte, shipment.early_shpdte,
shipment.late_shpdte,
stop.stop_id, car_move.car_move_id,         trlr.dispatch_dte
from pckmov, pckwrk, shipment, stop,
car_move, trlr   where shipment.stop_id  = stop.stop_id 
and stop.car_move_id  = car_move.car_move_id
and car_move.trlr_id
=trlr.trlr_id     and trlr.trlr_stat   in  ('O','L','H','C','P')
and shipment.loddte is not null
and shipment.ship_id  =
pckwrk.ship_id     and shipment.shpsts  in ('D', 'C')     and pckwrk.cmbcod
= pckmov.cmbcod     and pckmov.arecod    in ('SSTG','WIP SUPPLY')
order by trlr.trlr_stat, shipment.ship_id
/
