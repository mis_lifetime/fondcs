#!perl
#
# File    : sql_shw_tcl.pl
#
# Purpose : SQL Query to show table column names
#
# Author  : Steve R. Hanchar
#
# Date    : 06-Mar-2001
#
# Usage   : shw_tcl <colpfx>
#
###############################################################################
#
# Include needed sub-components
#
use strict;
#
use FindBin;
#use Getopt::Std; (unneeded);
#
###############################################################################
#
# Initialize internal parameters
#
my $par_pthsep;
my $par_sqlutl;
#
if ($^O =~ /win32/i)
{			# NT
    $par_pthsep = "\\";
    $par_sqlutl = "plus80 ".$ENV{WM_ORACLE_CONNECT};
}
else
{			# Unix
    $par_pthsep = "\/";
    $par_sqlutl = "sqlplus ".$ENV{DB_LOGIN}."/".$ENV{DB_PASSWORD};
}
#
my $par_inifil = $FindBin::Bin.$par_pthsep."sqlini.sql";
#
my $par_tmpdir = $ENV{STV_TEMP} ? $ENV{STV_TEMP} : ".";
#
###############################################################################
#
# Initialize internal variables
#
my $arg_colpfx = "";
#
###############################################################################
#
# Define print help routine
#
sub print_help()
{
    printf ("\n");
    printf ("SQL Query to show table column names\n");
    printf ("\n");
    printf ("Usage:  shw_tcl <colpfx>\n");
    printf ("\n");
    printf ("    <colpfx>) Column name prefix (can include wildcards)\n");
    printf ("              (wildcard character is \"%\")\n");
    printf ("              (default is \"$arg_colpfx\")\n");
    printf ("\n");
}
#
###############################################################################
#
# Input argument checking/processing
#
#
# Get command line arguments
#
$arg_colpfx = @ARGV[0];
#
if ($arg_colpfx eq "?")
{
    print_help();
    exit (0);
}
#
# Verify required arguments are present
#
#if (!$arg_colpfx)
#{
#    printf ("Column name prefix is not defined\n");
#    print_help ();
#    exit (1);
#}
#
#printf ("\nArgument List\n");
#printf ("ColPfx ($arg_colpfx)\n");
#printf ("\n");
#
# Convert column name prefix to upper case and append wildcard
#
$arg_colpfx =~ tr/[a-z]/[A-Z]/;
$arg_colpfx = $arg_colpfx."%";
#
###############################################################################
#
# Determine unique working file name and verify file does not already exist
#
my @dattim = localtime;
my $dattim = sprintf ("%0.2d%0.2d%0.2d%0.2d%0.2d%0.2d",
                   $dattim[5]-100, $dattim[4]+1, $dattim[3],
		   $dattim[2], $dattim[1], $dattim[0]);
#
my $wrk_filnam = $par_tmpdir.$par_pthsep."sql_shw_tcl.tmp_".$dattim;
if (-e $wrk_filnam)
{
    die "FATAL ERROR - Temporary file ($wrk_filnam) already exists\nStopped";
}
#
open (WRKFIL, "> $wrk_filnam") || die "FATAL ERROR - Could not open working file ($wrk_filnam)\nStopped";
#
###############################################################################
#
# If present, include standard SQL session initialization file
#
if (-e $par_inifil)
{
    printf (WRKFIL "@".$par_inifil);
}
#
###############################################################################
#
# Generate SQL Query Text 
#
printf (WRKFIL "
SET LINESIZE 100
SET PAGESIZE 66
SET NULL ?
ALTER SESSION SET NLS_DATE_FORMAT='YYYY-MM-DD:HH24:MI:SS';
SELECT
  SUBSTR(UC.TABLE_NAME,0,20)				\"Table\",
  /*  SUBSTR(UO.OBJECT_TYPE,0,10)			\"TabType\", */
  SUBSTR(UC.COLUMN_NAME,0,10)				\"Column\",
  SUBSTR(UC.DATA_TYPE,0,8)				\"ColType\",
  DECODE (UC.NULLABLE, 'Y', 'Y', ' ')			\"N\",
  TO_CHAR (UC.DATA_LENGTH, '99999')			\"Length\",
  NVL (TO_CHAR (UC.DATA_PRECISION, '99999'), ' ')	\"Precis\"
FROM
  USER_TAB_COLUMNS UC,
  USER_OBJECTS     UO
WHERE
  UC.TABLE_NAME  = UO.OBJECT_NAME AND
  UO.OBJECT_TYPE = 'TABLE'        AND
  UC.COLUMN_NAME LIKE '$arg_colpfx'
ORDER BY
  UC.TABLE_NAME,
  UC.COLUMN_NAME
;
EXIT
");
#
###############################################################################
#
# Invoke interactive SQL to perform SQL query
#
close (WRKFIL);
#
system ($par_sqlutl." @".$wrk_filnam) && die "FATAL ERROR - Error running interactive SQL\nStopped";
#
###############################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
if (-e $wrk_filnam)
{
    unlink ($wrk_filnam) || die "FATAL ERROR - Could not remove working file ($wrk_filnam)";
}
#
exit (0);
#
###############################################################################
