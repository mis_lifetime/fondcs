#!/usr/local/bin/perl
use lib "$ENV{MOCADIR}/scripts";
use MocaReg; 
#use strict;
use DBI;

$dbuser = MocaReg->get("Server", "dbuser");
$dbpass = MocaReg->get("Server", "dbpass");
$dbconn = MocaReg->get("Server", "dbconn"); 

# Which database are we looking at?
my ($usr, $pwd, $db) = ($dbuser, $dbpass, $dbconn);

# connect to the database
my $db_h = DBI->connect("DBI:Oracle:$db", $usr, $pwd)
	or die "Count not connect to database: " . DBI->errstr;

my $st_h = $db_h->prepare("select 'Process holding up another process '||''''||l.sid||','||s.serial#||''''||';' " .
                          " from v\$session s, v\$lock l " .
                          "where s.sid = l.sid " .
                          "  and l.request = 0 " .
                          "  and l.lmode != 4 " .
                          " and (l.id1, l.id2) in (select ll.id1, ll.id2 from v\$lock ll where ll.request != 0 and ll.id1 = l.id1 and ll.id2 = l.id2) ".
                          "order by l.sid")
	or die "Could not prepare statement: " . $db_h->errstr;

$st_h->execute()
	or die "Could not execute statement: " . $st_h->errstr;

my @row = ();
my %segments = ();

print "\n\n";
print "Database Dead Locks                SID,Serial# \n";
print '-' x 46 . "\n";
my $previous_segment_type;
my $index = 0;
while (@row = $st_h->fetchrow_array()) {
	my ($colnam) = @row;
	# this if-els block would print a blank line between the list of tables and indexes.
	printf "%-50s \n", $colnam;
}

# release the statement handle
$st_h->finish;

$db_h->disconnect;

