/* The following script performs daily updates for the */
/* Usr-LastActivityReport .  */

/*first insert all shipping and production records into temp table*/

truncate table usr_shp_hdr2
/

insert into usr_shp_hdr2(prtnum,lstshp,lstprd)
select x.prtnum,max(g.dispatch_dte),max(h.moddte) 
      from prtmst x, ord_line a,shipment_line b,shipment c,stop e,
      car_move f,trlr g,wkodtl h
       where f.trlr_id  = g.trlr_id(+)
      and e.car_move_id  = f.car_move_id(+)
      and c.stop_id = e.stop_id(+)
      and b.ship_id = c.ship_id(+)
      and a.client_id = b.client_id(+)
      and a.ordsln = b.ordsln(+)
      and a.ordlin = b.ordlin(+)
      and a.ordnum = b.ordnum(+)
      and x.prtnum = a.prtnum(+)
      and x.prtnum = h.prtnum(+)
      having not (max(g.dispatch_dte) is null
        and max(h.moddte) is null)
group by x.prtnum
/
commit;

/* then update usr_shp_hdr table with shipping and production records in temp table */
/* this proved to be more efficient to save in a temp table than to just */
/* run one update script */

 update usr_shp_hdr x
              set (x.lstshp,x.lstprd)=
              (select p.lstshp,p.lstprd
                   from usr_shp_hdr2 p
                     where p.prtnum = x.prtnum
         and (trunc(p.lstshp) > nvl(trunc(x.lstshp),'01-JAN-1900') or trunc(p.lstprd) > nvl(trunc(x.lstprd),'01-JAN-1900')))
     where exists(select 'x' from usr_shp_hdr2 r
      where r.prtnum = x.prtnum
 and (trunc(r.lstshp) > nvl(trunc(x.lstshp),'01-JAN-1900') or trunc(r.lstprd) > nvl(trunc(x.lstprd),'01-JAN-1900')))
/
commit;

/* truncate temp table, then insert sample data records */

truncate table usr_shp_hdr2
/

insert into usr_shp_hdr2(prtnum,lstsmp)
select ol.prtnum,max(o.entdte) from ord o,ord_line ol
where o.ordnum = ol.ordnum
and o.client_id = ol.client_id
and substr(cponum,1,1) = '='
group by ol.prtnum
/
commit;

/* update last sample dates */

update usr_shp_hdr x
                      set (x.lstsmp)=
                      (select y.lstsmp
                           from usr_shp_hdr2 y
                             where x.prtnum = y.prtnum
                    and trunc(y.lstsmp) > nvl(trunc(x.lstsmp),'01-JAN-1900'))
            where exists(select 'x' from usr_shp_hdr2 y
             where x.prtnum = y.prtnum
      and trunc(y.lstsmp) > nvl(trunc(x.lstsmp),'01-JAN-1900'))
/
commit;

/* then insert any new shipping or production records into usr_shp_hdr */ 


insert into usr_shp_hdr(prtnum,lstshp,lstprd)
select x.prtnum,max(g.dispatch_dte),max(h.moddte) 
      from prtmst x, ord_line a,shipment_line b,shipment c,stop e,
      car_move f,trlr g,wkodtl h
       where f.trlr_id  = g.trlr_id(+)
      and e.car_move_id  = f.car_move_id(+)
      and c.stop_id = e.stop_id(+)
      and b.ship_id = c.ship_id(+)
      and a.client_id = b.client_id(+)
      and a.ordsln = b.ordsln(+)
      and a.ordlin = b.ordlin(+)
      and a.ordnum = b.ordnum(+)
      and x.prtnum = a.prtnum(+)
      and x.prtnum = h.prtnum(+)
      and not exists(select 'X' from usr_shp_hdr y where y.prtnum 
        = x.prtnum)      
      having not (max(g.dispatch_dte) is null
        and max(h.moddte) is null)
group by x.prtnum
/
commit;

/* then insert any new sample records into usr_shp_hdr */

insert into usr_shp_hdr(prtnum,lstsmp)
select ol.prtnum,max(o.entdte) from ord o,ord_line ol
where o.ordnum = ol.ordnum
and o.client_id = ol.client_id
and substr(cponum,1,1) = '='
and not exists(select 'x' from usr_shp_hdr x where x.prtnum = ol.prtnum)
group by ol.prtnum
/
commit;
