#!/usr/bin/ksh
#
# This script will call the script Update_PoolCodes.sql to update the region code in Address Master
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql script.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the script

sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/UpdatePoolCodes.out
@Update_PoolCodes.sql
spool off
exit
//
exit 0
