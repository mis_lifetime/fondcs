# This script will truncate all the transactional data out of SeamLES.
# Need to do this prior to go live.
alter table sl_eo_data_hdr disable constraint sl_eo_data_hdr_fk_evtd;
alter table sl_evt_arg_data disable constraint sl_evt_arg_data_fk_evtd;
alter table sl_ifd_data_hdr disable constraint sl_ifd_data_hdr_fk_evtd;
alter table sl_ifd_sys_map_data disable constraint sl_ifd_sys_map_data_fk_evtd;
alter table sl_ifd_data_err disable constraint sl_ifd_data_err_fk_dwnld_seq;
alter table sl_ifd_data_hdr disable constraint sl_ifd_data_hdr_fk_dwnld;
alter table sl_eo_data_dtl disable constraint SL_EO_DATA_DTK_FK_HDR;
alter table sl_ifd_data_dtl disable constraint SL_IFD_DATA_DTL_FK_HDR;
alter table sl_async_evt_arg_data disable constraint SL_ASYNC_EVT_ARG_DATA_FK_QUE;   

truncate table sl_msg_log;
truncate table sl_ifd_data_dtl;
truncate table sl_eo_data_dtl;
truncate table sl_evt_arg_data;
truncate table sl_ifd_sys_map_data;
truncate table sl_ifd_data_err;
truncate table sl_async_evt_arg_data;
truncate table sl_async_evt_que;
truncate table sl_ifd_data_hdr;
truncate table sl_eo_data_hdr;
truncate table sl_dwnld;
truncate table sl_evt_data; 

alter table sl_eo_data_hdr enable constraint sl_eo_data_hdr_fk_evtd;
alter table sl_evt_arg_data enable constraint sl_evt_arg_data_fk_evtd;
alter table sl_ifd_data_hdr enable constraint sl_ifd_data_hdr_fk_evtd;
alter table sl_ifd_sys_map_data enable constraint sl_ifd_sys_map_data_fk_evtd;
alter table sl_ifd_data_err enable constraint sl_ifd_data_err_fk_dwnld_seq;
alter table sl_ifd_data_hdr enable constraint sl_ifd_data_hdr_fk_dwnld;
alter table sl_eo_data_dtl enable constraint SL_EO_DATA_DTK_FK_HDR;
alter table sl_ifd_data_dtl enable constraint SL_IFD_DATA_DTL_FK_HDR;
alter table sl_async_evt_arg_data enable constraint SL_ASYNC_EVT_ARG_DATA_FK_QUE; 
