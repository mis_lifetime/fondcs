#!/bin/ksh
#######################################################################

# 50_RECON - Script to compare transactions  in 2 tables. This is 
# used to create the  daily reconciliation file. This is adapted from the
# cscript used last year.

#######################################################################

# $MF_Copyright-Start$

#   DISTRIBUTION MANAGER
#   Copyright (c) 1999
#   McHugh Software International
#   Waukesha, Wisconsin

# This software is furnished under a corporate license for use on a
# single computer system and can be copied (with inclusion of the
# above copyright) only for use on such a system.

# The information in this document is subject to change without notice
# and should not be construed as a commitment by McHugh Software
# International.

# McHugh Software International assumes no responsibility for the use of
# the software described in this document on equipment which has not been
# supplied or approved by McHugh Software International.

# $MF_Copyright-End$

# $Id$

# Modification History

# $Log$

#######################################################################
#

# Set the environment to fon
. /usr/local/bin/eset fon

script_dir="$LESDIR/scripts"

#Set the home directory for the Calculations
CALC_HOME="/tmp/RECONC"

# Get the hostout directory for the 4.0 system
HOSTOUT="$LESDIR/files/hostout/fonrecon"

OLDHOSTOUT="$LESDIR/files/hostout"

# Get todays date in the correct format
datevar=`date -u +%m%d`

# Set the filename for the tar file 
tarfile="hostout$datevar.tar.Z"

cd ${OLDHOSTOUT}
#cp REC*.txt $HOSTOUT/REC*.txt

cd $CALC_HOME
dirname=tran`date +%Y%m%d`

# If the directory exists clean it up and remove it.

if [[ -d $CALC_HOME/$dirname ]]
then
 rm -rf $dirname;
fi

if [[ -d $CALC_HOME/$dirname ]]
then
  print 'Unable to remove directory. Exiting....'
  exit
fi

mkdir $dirname

# change to the hostout directory and look for the file
cd ${HOSTOUT}

if [[ ! -r $tarfile ]]
then
# check for the file in the prc directory
  cd prc 
  if [[ ! -r $tarfile ]]
  then
     print 'The tar file is missing'
     exit
  fi
fi

cp $tarfile $CALC_HOME/$dirname

cd $CALC_HOME/$dirname

uncompress $tarfile
tar -xf ${tarfile%.*}

#rm ${tarfile%.*}

if [[ ! -r $CALC_HOME/altprt.lst ]]
then
  print 'We need the altprt.lst file in the' $CALC_HOME 'directory'
  exit
fi

cp $CALC_HOME/altprt.lst $CALC_HOME/$dirname

REC_filecount=`ls REC*.txt | wc -l`

if [[ $REC_filecount > 1 ]]
then
   print 'There are more than 1 REC files in the directory. Stopping the process'
   exit
fi

# Make sure we have some file atleast to process. If nothing, just exit out

filecount=`ls *.txt | grep -v REC* | wc -l`

if [ $filecount = 0 ]
then
 print 'There are no files to process. Exiting.....'
fi

filename2=`ls REC*.txt`

print 'Removing the REC file $filename2'

# Remove the file. We will get it back later from the tar
rm $filename2 

#At this point we have all the data. We need yesterday's REC file. 
#We need to copy yesterday's file into today's directory  
#latest.pl does all the work for us. just call it. It  also replaces the transaction number with the latest.

/usr/local/bin/perl ${script_dir}/50_latest.pl 

#At this point we are ready to run the compare script

cd $CALC_HOME/$dirname

#Do the processing and store the results in a temp1 file

/usr/local/bin/perl ${script_dir}/pcom.pl -f '*.txt' -c 'awk -f$LESDIR/scripts/awk/afile <filnam>' -q > tfile
cat tfile | sort -k 1 | awk -f${script_dir}/awk/addfile > temp.file

# The temp file might contain Alternate parts. Process it
${script_dir}/replace.pl > rep.file

#sort and Add after the replacements
cat rep.file | sort -k 1 | awk -f${script_dir}/awk/addfile > temp1.file

# Verify that we still have the tar file available

if [[ ! -r ${tarfile%.*} ]]
then
  print 'The tar file does not exist. Good bye'
  exit
fi

print 'tar -xf ${tarfile%.*} ' $filename2

tar -xf ${tarfile%.*} $filename2

if [[ ! -r $filename2 ]]
then
  print 'Was unable to extract the REC file from the tar. Check it'
  exit
fi

#Do the processing for File 2 and store in a temp2 file
awk -f${script_dir}/awk/afile $filename2 | sort -k 1 | awk -f${script_dir}/awk/addfile > temp2.file

# In the first Run list all the Items that have a mismatch
print -u2 "Listing all items that have mismatched inventory..."

# Compare the 2 files with the compare.pl script
${script_dir}/compare.pl -a temp1.file -b temp2.file  > results.file

awk -f${script_dir}/awk/tranfile results.file > ADJ0* 

# Now that we are done, lets copy the tar file to the hostout directory and untar it.
#cp -p ${tarfile%.*} $HOSTOUT


#Copy the Adjustment file to the hostout directory
#cp -p ADJ0* ${HOSTOUT}

#cd ${HOSTOUT}

#tar -xf ${tarfile%.*}

exit


