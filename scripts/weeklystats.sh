#!/usr/bin/ksh
#
# This script will run the Report at 5:30am using cron.
#


. /opt/mchugh/fon/les/.profile
cd ${LESDIR}/oraclereports



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the daily reports


sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/fon/les/oraclereports/OpenSalesOrdersDetails.out
@Usr-OpenOrdsDetailReport.sql
spool off
exit
//
sql << //
set trimspool on
set pagesize 200
set linesize 400
spool /opt/mchugh/fon/les/oraclereports/OpenOrdersShipment.out
@Usr-OpenOrdsShipment.sql
spool off
exit
//
sql << //
set trimspool on
spool /opt/mchugh/fon/les/oraclereports/PalletCountUpdate.out
@Usr-PalletCountUpdate.sql
spool off
exit
//
sql << //
set trimspool on
spool /opt/mchugh/fon/les/oraclereports/WeeklyStatsUpdate.out
@Usr-WeeklyStatsUpdate.sql
spool off
exit
//
sql << //
set trimspool on
set verify off
column sysdte new_value 1 noprint
select decode(to_char(sysdate-1,'D'),1,to_char(sysdate-3,'DD-MON-YYYY'),
to_char(sysdate-1,'DD-MON-YYYY')) sysdte from dual;
spool /opt/mchugh/fon/les/oraclereports/FONDailyFlash.out
@FIN-DailyFlash.sql
spool off
exit
//
exit 0



