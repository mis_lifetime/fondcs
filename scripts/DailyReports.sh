#!/usr/bin/ksh
#
# This script will run the Reports at 6AM using cron.
#


#. /opt/mchugh/fon/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the daily reports


sql << //
set trimspool on
set linesize 100
set pagesize 500
spool /opt/mchugh/fon/les/oraclereports/cnthst_update.out
@usr_cnthst_update.sql
spool off
exit
//
sql << //
set trimspool on
spool /opt/mchugh/fon/les/oraclereports/WalmartVolume.out
@Usr-WlmVolUpdte.sql
spool off
exit
//
sql << //
set trimspool on
spool /opt/mchugh/fon/les/oraclereports/LastActivityReport.out
@Usr-LstActUpdate.sql
spool off
exit
//
sql << //
set trimspool on
spool /opt/mchugh/fon/les/oraclereports/QCLOCMoves.out
@Usr-QCLOCUpdate.sql
spool off
exit
//
sql << //
set trimspool on
spool /opt/mchugh/fon/les/oraclereports/ClassUpdate.out
@Usr-ClassUpdate.sql
spool off
exit
//
exit 0

