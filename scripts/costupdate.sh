#!/usr/bin/ksh
#
# This script will run the Cost update before the Flash every day CW 1/8/09. 
#


. /opt/mchugh/fon/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the report.




sql << //
truncate table usr_parts;
exit
//
cp /opt/mchugh/fon/les/db/data/load/parts/partsinfo.csv /opt/mchugh/fon/les/db/data/load/parts/parts.csv

/opt/mchugh/fon/moca/bin/mload -H -c /opt/mchugh/fon/les/db/data/load/parts.ctl -D /opt/mchugh/fon/les/db/data/load/parts -d parts.csv

cd ${LESDIR}/scripts

sql << //
set trimspool on
spool /opt/mchugh/fon/les/oraclereports/ItemCostsUpdate.out
@ItemCostsUpdate.sql
spool off
exit
//
exit 0




