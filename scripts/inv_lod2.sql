update var_shploc set stoloc = 'SHIP-DR291' where ship_id = 'SID0306333';

1 row updated.

SQL> commit;

Commit complete.

SQL> update invlod set stoloc = 'DR291' where lodnum = '00000000000004684114'
  2  ;
ERROR:
ORA-01756: quoted string not properly terminated


SQL> update invlod set stoloc = 'SHIP-DR291' where lodnum = '00000000000004684114';

1 row updated.

SQL> commit;

Commit complete.

SQL> select appqty, pckqty, cmbcod from pckwrk where ship_id = 'SID0306333';

    APPQTY     PCKQTY CMBCOD
---------- ---------- ----------
         2          2 CMB4673466
         2          2 CMB4714749

SQL> select * from pckmov where cmbcod in('CMB4673466', 'CMB4714749');

CMBCOD         SEQNUM ARECOD     STOLOC
---------- ---------- ---------- --------------------
RESCOD                                                 ARRQTY     PRCQTY
-------------------------------------------------- ---------- ----------
CMB4714749          2 SSTG       DR291
XXXX                                                        2          0

CMB4673466          2 SSTG       DR291
XXXX                                                        2          0


SQL> ;odnum = '00000000000004684114'
  1* select * from pckmov where cmbcod in('CMB4673466', 'CMB4714749')
SQL> update pckmov set arecod = 'SSTG', stoloc = 'SHIP-DR291' where cmbcod in('CMB4673466', 'CMB4714749');

2 rows updated.

SQL> commit;

Commit complete.

SQL> 
