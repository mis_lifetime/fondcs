#!perl
#
# File    : AddMenuItem.pl
#
# Purpose : Add Menu Item to DCS Menu Structure
#
# Author  : Steve R. Hanchar
#
# Date    : 14-Feb-2001
#
# Usage   : AddMenuItem -g <menu group> -i <item name> -o <ocx name> [-h] [-v] [-d]
#
###############################################################################
#
# Include needed sub-components
#
require "getopts.pl";
#
###############################################################################
#
# Initialize internal parameters
#
if ($^O =~ /win32/i)
{			    # NT
    $par_pthsep = "\\";
}
else
{			    # Unix
    $par_pthsep = "\/";
}
#
$par_msql_args	= "";
#
$par_ath_id	= "DCS_SUPER";
$par_ath_typ	= "R";
$par_cust_lvl	= 10;
$par_les_all	= "LES";
$par_locale_id	= "US_ENGLISH";
$par_opt_typ	= "A";
#
###############################################################################
#
# Initialize internal variables
#
$arg_mnugrp = "";
#
$arg_itmnam = "";
#
$arg_ocxnam = "";
#
$arg_vbsflg = "";
#
$arg_dbgflg = "";
#
###############################################################################
#
# Define print help routine
#
sub print_help()
{
    printf ("\n");
    printf ("Add new menu item to existing menu group.  The new item can be either a new\n");
    printf ("or existing ocx user control.  If it is a new control, the user will also\n");
    printf ("need to ensure the control is available for use (register it, etc).\n");
    printf ("\n");
    printf ("Usage:  AddMenuItem -g <menu group> -i <item name> -o <ocx name> [-h] [-v] [-d]\n");
    printf ("\n");
    printf ("    -g) Menu Group - Existing menu group to add menu item to\n");
    printf ("        (example: \"mnugrpFacility\")\n");
    printf ("\n");
    printf ("    -i) Item Name - Name of menu item to create\n");
    printf ("        (example: \"Area Maintenance\")\n");
    printf ("\n");
    printf ("    -o) Ocx Name - Name of ocx user control to add menu item for\n");
    printf ("        (format: <project file name>.<ocx object name>)\n");
    printf ("        (example: \"aremstmnt.ctlaremstmnt\")\n");
    printf ("\n");
    printf ("    -h) Help mode - display this help screen\n");
    printf ("\n");
    printf ("    -v) Verbose mode - display running status messages\n");
    printf ("\n");
    printf ("    -d) Debug mode - display debug messages\n");
    printf ("\n");
    printf ("Note: The actual menu item name will be the first part of the ocx user\n");
    printf ("      control name (example: \"aremstmnt\" for the examples given above).\n");
    printf ("      The menu item name from the command line is what the user sees.\n");
    printf ("\n");
}
#
###############################################################################
#
# Define run sql command routine
#
sub run_sql_cmd ()
{
    my ($sqlcmd) = "";
    my ($sqlerr) = "";
    my ($sqlflg) = "";
    my ($sqlrsp) = "";
    my ($sqlwrk) = "";
    #
    # Get desired sql command to run
    #
    $sqlcmd = shift @_;
    #
    printf ("\nSqlCmd: ($sqlcmd)\n") if ($arg_dbgflg);
    #
    # Determine working file name and delete any old working files
    #
    $sqlwrk = "$ENV{LESDIR}${par_pthsep}log${par_pthsep}UpdateSequences_$$.tmp";
    if (-e $sqlwrk)
    {
	printf ("\nDeleting old working file ($sqlwrk)\n") if ($arg_dbgflg);
	unlink ($sqlwrk) || die "\nFATAL ERROR - Could not delete old working file ($sqlwrk).\nStopped";
    }
    #
    # Run sql command
    #
    open   (SQLCMD, "| msql -s $par_msql_args > $sqlwrk") || die "\nFATAL ERROR - Could not start sql utility.\nStopped";
    printf (SQLCMD "[ $sqlcmd ]\n");
    printf (SQLCMD "exit\n");
    close  (SQLCMD); # || die "\nFATAL ERROR - Could not run msql utility.\nStopped";
    #
    # Open sql command results file
    #
    open (SQLRSP, $sqlwrk) || die "\nFATAL ERROR - Could not access msql utility results.\nStopped";
    #
    # Read sql command results (filter out result header & blank lines)
    #
    $sqlerr = "";
    $sqlflg = "";
    $sqlrsp = "";
    #
    while (<SQLRSP>)
    {
 	chomp;
	#
	if (/^\s*$/)
	{
	    next;  # Skip blank lines
	}
	#
	if (/^MSQL>/)
	{
	    next;  # Skip command prompt lines
	}
	#
	if (/Rows Affected\)$/)
	{
	    next;  # Skip rows affected trailer line
	}
	#
	if (/Executing... Error!/)
	{
	    $sqlerr = $_;
	}
	if ($sqlerr =~ /Executing... Error!/)
	{
	    $sqlerr = $_;
	}
	#
	if ($sqlflg)
	{
	    s/(^.*\S)\s*$/$1 /;  # Trim excess trailing blanks
	    $sqlrsp .= $_;
	}
	#
	if (/^-/)
	{
	    $sqlflg = "1";  # End of results header detected
	}
    }
    #
    # Verify no errors occurred running sql command
    #
    if (!$sqlrsp && $sqlerr)
    {
	if ($sqlerr =~ /Success/i)
	{
	    $sqlerr = "";
	}
	#
	if ($sqlerr =~ /-1403/)
	{
	    $sqlerr = "";
	}
	#
	if ($sqlerr =~ /^Command affected no rows$/i)
	{
	    $sqlerr = "";
	}
	#
	if ($sqlerr =~ /^Result: 512 - ORA-00001/i)  # Continue on this error
	{
	    printf ("SqlErr: Duplicate entry insert... continuing\n") if ($arg_vbsflg);
	    $sqlrsp = $sqlerr;
	    $sqlerr = "";
	}
    }
    #
    if (!$sqlrsp && $sqlerr)
    {
	printf ("\nFATAL ERROR - Error occurred running sql command\n");
	printf ("\nSqlCmd: ($sqlcmd)\n");
	printf ("SqlErr: ($sqlerr)\n");
	die;
    }
    #
    # Trim trailing blanks from sql command results
    #
    $sqlrsp =~ s/(^.*\S)\s*$/$1/;
    #
    # Close & delete sql command results file
    #
    close  (SQLRSP);
    unlink ($sqlwrk);
    #
    printf ("SqlRsp: ($sqlrsp)\n") if ($arg_dbgflg);
    #
    return $sqlrsp;
}
#
################################################################################
#
# Input argument checking/processing
#
#
# Get command line arguments
#
$status = &Getopts ('g:i:o:hvd');
#
# If help mode enabled, then print help screen & exit
#
if ($opt_h || !$status)
{
    &print_help ();
    exit (0);
}
#
# Load arguments into local variables
#
$arg_mnugrp = $opt_g if ($opt_g);
$arg_itmnam = $opt_i if ($opt_i);
$arg_ocxnam = $opt_o if ($opt_o);
#
$arg_vbsflg = "1"    if ($opt_d || $opt_v);
$arg_dbgflg = "1"    if ($opt_d);
#
# Verify required arguments are present
#
if (!$arg_mnugrp)
{
    printf ("Menu Group is not defined\n");
    print_help ();
    exit (1);
}
#
if (!$arg_itmnam)
{
    printf ("Item Name is not defined\n");
    print_help ();
    exit (1);
}
#
if (!$arg_ocxnam)
{
    printf ("Ocx Name is not defined\n");
    print_help ();
    exit (1);
}
#
$test = ($arg_ocxnam =~ /^([^\.]+\.[^\.]+)$/);
if (!$test)
{
    printf ("Ocx User Control format incorrect\n");
    print_help ();
    exit (1);
}
#
# If enabled, output processed input arguments
#
if ($arg_vbsflg)
{
    printf ("\nArgument List\n");
    printf ("MnuGrp ($arg_mnugrp)\n");
    printf ("ItmNam ($arg_itmnam)\n");
    printf ("OcxNam ($arg_ocxnam)\n");
    printf ("VbsFlg ($arg_vbsflg)\n");
    printf ("DbgFlg ($arg_dbgflg)\n");
    printf ("\n");
}
#
###############################################################################
#
# Verify given menu group actually exists
#
printf ("Verifying menu group exists\n") if ($arg_vbsflg);
#
$sqlcmd = "select 'exists' from les_mnu_grp where mnu_grp = '$arg_mnugrp'";
$sqlrsp = &run_sql_cmd ($sqlcmd);
if ($sqlrsp ne "exists")
{
    printf ("Menu Group ($arg_mnugrp) not found\n");
    $sqlcmd = "select '|', mnu_grp, par_grp from les_mnu_grp";
    $sqlrsp = &run_sql_cmd ($sqlcmd);
    $sqlrsp =~ s/\| /\n/g;
    $sqlhdr = "\n    Menu Group            Parent Group";
    die "Defined Menu Groups are:$sqlhdr$sqlrsp\nStopped";
}
#
###############################################################################
#
# Derive remaining required information from provided information
# Initialize argument dependent variables
#
$mls_nam = $arg_itmnam;
$mls_nam =~ s/ //g;
#
$mls_text = $arg_itmnam;
#
$frm_id = $arg_ocxnam;
$frm_id =~ s/^([^\.]+)\..*$/$1/;
#
$frm_mls = "ttl".$mls_nam;
#
$frm_prog_id = $arg_ocxnam;
#
$frm_file_nam = $arg_ocxnam;
$frm_file_nam =~ s/^([^\.]+)\..*$/$1/;
$frm_file_nam = $frm_file_nam.".ocx";
#
$appl_id = $frm_id;
#
$appl_mls = "appttl".$mls_nam;
#
$opt_nam = $frm_id;
#
$itm_mls = "mnuitm".$mls_nam;
#
if ($arg_dbgflg)
{
    printf ("\n");
    printf ("Derived values\n");
    printf ("Mls_Nam      ($mls_nam)\n");
    printf ("Mls_Text     ($mls_text)\n");
    printf ("Frm_Id       ($frm_id)\n");
    printf ("Frm_Mls      ($frm_mls)\n");
    printf ("Frm_Prog_Id  ($frm_prog_id)\n");
    printf ("Frm_File_Nam ($frm_file_nam)\n");
    printf ("Appl_Id      ($appl_id)\n");
    printf ("Appl_Mls     ($appl_mls)\n");
    printf ("Opt_Nam      ($opt_nam)\n");
    printf ("Itm_Mls      ($itm_mls)\n");
    printf ("\n");
}
#
###############################################################################
#
# Add wf_frm entry (define ocx form file & form entry point)
#
printf ("Adding wf_frm entry\n") if ($arg_vbsflg);
#
$sqlcmd = "insert into wf_frm ".
	  "  (frm_id,cust_lvl,descr_id,frm_progid,frm_file_nam) ".
	  "values ".
	  "  ('$frm_id',$par_cust_lvl,'$frm_mls','$frm_prog_id','$frm_file_nam')";
$sqlrsp = &run_sql_cmd ($sqlcmd);
#
printf ("Adding wf_frm les_mls_cat entry\n") if ($arg_vbsflg);
#
$sqlcmd = "insert into les_mls_cat ".
          "  (mls_id,locale_id,prod_id,appl_id,frm_id,vartn,srt_seq,cust_lvl,mls_text) ".
	  "values ".
	  "  ('$frm_mls','$par_locale_id','$par_les_all','$par_les_all','$par_les_all','$par_les_all',0,$par_cust_lvl,'$mls_text')";
$sqlrsp = &run_sql_cmd ($sqlcmd);
#
###############################################################################
#
# Add wf_flw entry (define ocx form as form flow starting point)
#
printf ("Adding wf_flw entry\n") if ($arg_vbsflg);
#
$sqlcmd = "insert into wf_flw ".
          "  (appl_id,cust_lvl,frm_id,static_frm_flg) ".
	  "values ".
	  "  ('$appl_id',$par_cust_lvl,'$frm_id',1)";
$sqlrsp = &run_sql_cmd ($sqlcmd);
#
###############################################################################
#
# Add wf_appl entry (define application & its starting point)
#
printf ("Adding wf_appl entry\n") if ($arg_vbsflg);
#
$sqlcmd = "insert into wf_appl ".
          "  (appl_id,cust_lvl,descr_id,start_frm_id,prod_id) ".
	  "values ".
	  "  ('$appl_id',$par_cust_lvl,'$appl_mls','$frm_id','$par_les_all')";
$sqlrsp = &run_sql_cmd ($sqlcmd);
#
printf ("Adding wf_appl les_mls_cat entry\n") if ($arg_vbsflg);
#
$sqlcmd = "insert into les_mls_cat ".
          "  (mls_id,locale_id,prod_id,appl_id,frm_id,vartn,srt_seq,cust_lvl,mls_text) ".
	  "values ".
	  "  ('$appl_mls','$par_locale_id','$par_les_all','$par_les_all','$par_les_all','$par_les_all',0,$par_cust_lvl,'$mls_text')";
$sqlrsp = &run_sql_cmd ($sqlcmd);
#
###############################################################################
#
# Add les_mnu_opt entry (define menu option & its associated application)
#
printf ("Adding les_mnu_opt entry\n") if ($arg_vbsflg);
#
$sqlcmd = "insert into les_mnu_opt ".
          "  (opt_nam,opt_typ,pmsn_mask,ena_flg,exec_nam) ".
	  "values ".
	  "  ('$opt_nam','$par_opt_typ',-1,1,'$appl_id')";
$sqlrsp = &run_sql_cmd ($sqlcmd);
#
###############################################################################
#
# Add les_mnu_itm entry (bind menu option to menu group)
#
printf ("Adding les_mnu_itm entry\n") if ($arg_vbsflg);
#
$sqlcmd = "insert into les_mnu_itm ".
          "  (mnu_grp,mnu_itm,mnu_seq,opt_nam) ".
	  "values ".
	  "  ('$arg_mnugrp','$itm_mls',0,'$opt_nam')";
$sqlrsp = &run_sql_cmd ($sqlcmd);
#
printf ("Adding les_mnu_opt sys_dsc_mst entry\n") if ($arg_vbsflg);
#
$sqlcmd = "insert into sys_dsc_mst ".
          "  (colnam,colval,locale_id,cust_lvl,mls_text) ".
	  "values ".
	  "  ('mnu_itm','$itm_mls','$par_locale_id',$par_cust_lvl,'$mls_text')";
$sqlrsp = &run_sql_cmd ($sqlcmd);
#
###############################################################################
#
# Add les_opt_ath entry (authorize generic user to access new menu option)
#
printf ("Adding les_opt_ath entry\n") if ($arg_vbsflg);
#
$sqlcmd = "insert into les_opt_ath ".
          "  (opt_nam,ath_typ,ath_id,pmsn_mask) ".
	  "values ".
	  "  ('$opt_nam','$par_ath_typ','$par_ath_id',-1)";
$sqlrsp = &run_sql_cmd ($sqlcmd);
#
###############################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
exit (0);
#
###############################################################################

