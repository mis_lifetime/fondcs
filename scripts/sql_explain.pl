#!perl
#
# File    : sql_explain.pl
#
# Purpose : SQL Explain Utility front end command file
#
# Author  : Steve R. Hanchar
#
# Date    : 06-Mar-2001
#
# Usage   : sql_explain <filnam>
#
###############################################################################
#
# Include needed sub-components
#
use strict;
#
use FindBin;
#use Getopt::Std; (unneeded);
#
###############################################################################
#
# Initialize internal parameters
#
my $par_pthsep;
my $par_sqlutl;
#
if ($^O =~ /win32/i)
{			# NT
    $par_pthsep = "\\";
    $par_sqlutl = "plus80 ".$ENV{WM_ORACLE_CONNECT};
}
else
{			# Unix
    $par_pthsep = "\/";
    $par_sqlutl = "sqlplus ".$ENV{DB_LOGIN}."/".$ENV{DB_PASSWORD};
}
#
my $par_inifil = $FindBin::Bin.$par_pthsep."sqlini.sql";
#
my $par_tmpdir = $ENV{STV_TEMP} ? $ENV{STV_TEMP} : ".";
#
###############################################################################
#
# Initialize internal variables
#
my $arg_filnam = "";
#
###############################################################################
#
# Define print help routine
#
sub print_help()
{
    printf ("\n");
    printf ("SQL Explain Utility front end command file\n");
    printf ("\n");
    printf ("Usage:  sql_explain <filnam>\n");
    printf ("\n");
    printf ("    <filnam>) SQL statement file containing an single SQL statement\n");
    printf ("              which can be run from the SQL command line utility\n");
    printf ("\n");
}
#
###############################################################################
#
# Input argument checking/processing
#
#
# Get command line arguments
#
$arg_filnam = @ARGV[0];
#
if ($arg_filnam eq "?")
{
    print_help();
    exit (0);
}
#
# Verify required arguments are present
#
if (!$arg_filnam)
{
    printf ("SQL statement file name is not defined\n");
    print_help ();
    exit (1);
}
#
#printf ("\nArgument List\n");
#printf ("FilNam ($arg_filnam)\n");
#printf ("\n");
#
###############################################################################
#
# Determine unique working file name and verify file does not already exist
#
my @dattim = localtime;
my $dattim = sprintf ("%0.2d%0.2d%0.2d%0.2d%0.2d%0.2d",
                   $dattim[5]-100, $dattim[4]+1, $dattim[3],
		   $dattim[2], $dattim[1], $dattim[0]);
#
my $wrk_filnam = $par_tmpdir.$par_pthsep."sql_explain.tmp_".$dattim;
if (-e $wrk_filnam)
{
    die "FATAL ERROR - Temporary file ($wrk_filnam) already exists\nStopped";
}
#
open (WRKFIL, "> $wrk_filnam") || die "FATAL ERROR - Could not open working file ($wrk_filnam)\nStopped";
#
###############################################################################
#
# If present, include standard SQL session initialization file
#
if (-e $par_inifil)
{
    printf (WRKFIL "@".$par_inifil);
}
#
###############################################################################
#
# Generate SQL Query Text 
#
my $plnnam = "sql_explain_$dattim";
#
open (SQLTXT, "<$arg_filnam") || die "FATAL ERROR - Could not open sql command text file ($arg_filnam) for reading.\nStopped";
my $sqltxt = join ('', <SQLTXT>);
close (SQLTXT);
#
if (!($sqltxt =~ /;\s*$/))
{
    $sqltxt =~ s/(\s*)$/;$1/;
}
#
#  Note: Blank line after \$sqltxt statement is intentional to handle cases where
#  the last line of a file is not terminated with a newline character.
#
printf (WRKFIL "
SET PAGESIZE 9999
EXPLAIN PLAN SET STATEMENT_ID = '$plnnam' FOR
$sqltxt

#
list
#
SELECT
  LPAD(' ',2*LEVEL) || OPERATION ||' '|| OPTIONS ||' '|| OBJECT_NAME Query_Plan
FROM
  PLAN_TABLE
WHERE
  STATEMENT_ID = '$plnnam'
CONNECT BY
  PRIOR ID = PARENT_ID AND
  STATEMENT_ID = '$plnnam'
START WITH
  ID = 1 AND
  STATEMENT_ID = '$plnnam';
#
DELETE
  PLAN_TABLE
WHERE
  STATEMENT_ID = '$plnnam';
#
EXIT
");
#
###############################################################################
#
# Invoke interactive SQL to perform SQL query
#
close (WRKFIL);
#
#open (TSTFIL, "<$wrk_filnam") || die "FATAL ERROR - Could not open working file ($wrk_filnam) for reading.\nStopped";
#for (<TSTFIL>)
#{
#    print;
#}
#close (TSTFIL);
#
system ($par_sqlutl." @".$wrk_filnam) && die "FATAL ERROR - Error running interactive SQL\nStopped";
#
###############################################################################
#
# Common exit point for command file.  Perform any required clean-up activities
#
if (-e $wrk_filnam)
{
    unlink ($wrk_filnam) || die "FATAL ERROR - Could not remove working file ($wrk_filnam)";
}
#
exit (0);
#
###############################################################################
