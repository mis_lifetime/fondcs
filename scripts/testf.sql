select c.prtnum, 
nvl(a.total,0) total,  nvl(a.january,0) january
from usr_totalforecast a, invsum b, prtmst c, var_alt_prtnum d, prtdsc e, usr_classdtl f
where
c.prtnum=a.prtnum(+)
and c.prtnum = 'HF86642'
and c.prtnum = b.prtnum(+)
and c.prt_client_id =b.prt_client_id(+)
and c.prtnum = d.prtnum(+)
and c.vc_prdcod = f.vc_prdcod
and c.prtnum||'|----' = e.colval
and e.colnam = 'prtnum|prt_client_id'
and e.locale_id='US_ENGLISH'
and upper('&&1') = 'TOTALFORECAST'
group by c.prtnum,  
a.total, a.january 
/
