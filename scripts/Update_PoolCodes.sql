/********************************************/
/* This is a temporary fix until Westbury   */
/* includes the pool code within their      */
/* download.                                */
/********************************************/

update adrmst a
   set a.rgncod=(select b.rgncod from usr_poolcode b
   where a.host_ext_id = b.host_ext_id)
   where exists(select 'x' from usr_poolcode c
   where a.host_ext_id = c.host_ext_id);

commit;
