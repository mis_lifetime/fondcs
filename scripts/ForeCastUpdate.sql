/* This script updates the usr_totalforecast table after the usr_forecast table is updated */
/* from the most recent Demand Solutions spreadsheet. */
/* Forecast.ctl in the /opt/mchugh/prod/les/db/data  directory inserts records into the usr_forecast table */
/* The table must be truncated prior to running forecast.ctl, and the most recent */
/* copy of forecast.csv must exists in the db/data/load/forecast directory */
/* The partmaster table must also be updated (this is done within this script) with the current min values that also */
/* exists in the spreadsheet. */

truncate  table usr_totalforecast
/

insert into usr_totalforecast(select usr_forecast.prtnum, sum(usr_forecast.total) total, sum(usr_forecast.january) january,
sum(usr_forecast.february) february, sum(usr_forecast.march) march, sum(usr_forecast.april) april, sum(usr_forecast.may) may,
sum(usr_forecast.june) june, sum(usr_forecast.july) july, sum(usr_forecast.august) august, sum(usr_forecast.september) september,
sum(usr_forecast.october) october, sum(usr_forecast.november) november, sum(usr_forecast.december) december from usr_forecast
group by usr_forecast.prtnum)
/
commit;

/* get the count from the forecasta table */
select count(*) from usr_totalforecast
/

/*get the count of matching records from the partmaster */
/*that exist in the usr_totalforecast table */
select count(*) from prtmst a
where exists(select 'x' from usr_totalforecast b
where a.prtnum=b.prtnum)
/

/* Update the partmaster with the current months quantity */
update prtmst a
set a.reopnt=(select case when substr(to_date(sysdate,'DD-MON-YYYY'),4,3)='JAN'
then (select b.january from usr_totalforecast b
where a.prtnum=b.prtnum)
when substr(to_date(sysdate,'DD-MON-YYYY'),4,3)='FEB'
then (select b.february from usr_totalforecast b
where a.prtnum=b.prtnum)
when substr(to_date(sysdate,'DD-MON-YYYY'),4,3)='MAR'
then (select b.march from usr_totalforecast b
where a.prtnum=b.prtnum)
when substr(to_date(sysdate,'DD-MON-YYYY'),4,3)='APR'
then (select b.april from usr_totalforecast b
where a.prtnum=b.prtnum)
when substr(to_date(sysdate,'DD-MON-YYYY'),4,3)='MAY'
then (select b.may from usr_totalforecast b
where a.prtnum=b.prtnum)
when substr(to_date(sysdate,'DD-MON-YYYY'),4,3)='JUN'
then (select b.june from usr_totalforecast b
where a.prtnum=b.prtnum)
when substr(to_date(sysdate,'DD-MON-YYYY'),4,3)='JUL'
then (select b.july from usr_totalforecast b
where a.prtnum=b.prtnum)
when substr(to_date(sysdate,'DD-MON-YYYY'),4,3)='AUG'
then (select b.august from usr_totalforecast b
where a.prtnum=b.prtnum)
when substr(to_date(sysdate,'DD-MON-YYYY'),4,3)='SEP'
then (select b.september from usr_totalforecast b
where a.prtnum=b.prtnum)
when substr(to_date(sysdate,'DD-MON-YYYY'),4,3)='OCT'
then (select b.october from usr_totalforecast b
where a.prtnum=b.prtnum)
when substr(to_date(sysdate,'DD-MON-YYYY'),4,3)='NOV'
then (select b.november from usr_totalforecast b
where a.prtnum=b.prtnum)
when substr(to_date(sysdate,'DD-MON-YYYY'),4,3)='DEC'
then (select b.december from usr_totalforecast b
where a.prtnum=b.prtnum)
else NULL
end forecastdte
from dual)
where exists(select 'x' from usr_totalforecast c
where a.prtnum=c.prtnum)
/
commit;

/* Get a printout of items in partmaster that do not */
/* exist in forecasta, but have a minqty in the reopnt field */
/* These should be set to 0 as per Craig */
select prtnum, reopnt from prtmst a
where not exists(select 'x' from usr_totalforecast b
where a.prtnum=b.prtnum)
and a.reopnt is not null and a.reopnt != 0
order by prtnum
/

/* Update the partmaster table with 0 in the reopnt field */
/* where the items in partmaster do not exist in the forecasta */
/* table and old values exist in this field. */

update prtmst a
set a.reopnt= 0
where not exists(select 'x' from usr_totalforecast b
where a.prtnum=b.prtnum)
and a.reopnt is not null and a.reopnt !=0
/
commit;
