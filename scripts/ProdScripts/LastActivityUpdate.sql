/*first insert all production records into temp table*/

truncate table usr_shp_hdr2
/

insert into usr_shp_hdr2(prtnum,lstshp,lstprd)
select x.prtnum,max(g.dispatch_dte),max(h.moddte) 
      from prtmst x, ord_line a,shipment_line b,shipment c,stop e,
      car_move f,trlr g,wkodtl h
       where f.trlr_id  = g.trlr_id(+)
      and e.car_move_id  = f.car_move_id(+)
      and c.stop_id = e.stop_id(+)
      and b.ship_id = c.ship_id(+)
      and a.client_id = b.client_id(+)
      and a.ordsln = b.ordsln(+)
      and a.ordlin = b.ordlin(+)
      and a.ordnum = b.ordnum(+)
      and x.prtnum = a.prtnum(+)
      and x.prtnum = h.prtnum(+)
      having not (max(g.dispatch_dte) is null
        and max(h.moddte) is null)
group by x.prtnum
/
commit;

/* then update usr_shp_hdr table with records from production in temp table */

 update usr_shp_hdr x
              set (x.lstshp,x.lstprd)=
              (select p.lstshp,p.lstprd
                   from usr_shp_hdr2 p
                     where p.prtnum = x.prtnum
                   and (trunc(p.lstshp) > trunc(x.lstshp) or trunc(p.lstprd) > trunc(x.lstprd))
          )
     where exists(select 'x' from usr_shp_hdr2 r
      where r.prtnum = x.prtnum
 and (trunc(r.lstshp) > trunc(x.lstshp) or trunc(r.lstprd) > trunc(x.lstprd)))
/
commit;

/* then insert any new records into usr_shp_hdr */ 


insert into usr_shp_hdr(prtnum,lstshp,lstprd)
select x.prtnum,max(g.dispatch_dte),max(h.moddte) 
      from prtmst x, ord_line a,shipment_line b,shipment c,stop e,
      car_move f,trlr g,wkodtl h
       where f.trlr_id  = g.trlr_id(+)
      and e.car_move_id  = f.car_move_id(+)
      and c.stop_id = e.stop_id(+)
      and b.ship_id = c.ship_id(+)
      and a.client_id = b.client_id(+)
      and a.ordsln = b.ordsln(+)
      and a.ordlin = b.ordlin(+)
      and a.ordnum = b.ordnum(+)
      and x.prtnum = a.prtnum(+)
      and x.prtnum = h.prtnum(+)
      and not exists(select 'X' from usr_shp_hdr y where y.prtnum 
        = x.prtnum)      
      having not (max(g.dispatch_dte) is null
        and max(h.moddte) is null)
group by x.prtnum
/
commit;
