#!/usr/bin/ksh
#
# This script will run the Forecasting Report at 6AM using cron.
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/oraclereports



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the daily reports



sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/FileStatus5.out
@Usr-FileStatus.sql
spool off
exit
//
sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/StagedLoad5.out
@Usr-ShppingRep.sql
spool off
exit
//
sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/Rework5.out
@IC-ReworkReport.sql
spool off
exit
//
sql << //
set trimspool on
set pagesize 500
set linesize 200
spool /opt/mchugh/prod/les/oraclereports/Expected.out
@Usr-Expected.sql
spool off
exit
//
sql << //
set trimspool on
set verify off
column sysdte new_value 1 noprint
select to_char(sysdate-1,'DD-MON-YYYY') sysdte from dual;
column sysdte new_value 2 noprint
select to_char(sysdate-1,'DD-MON-YYYY') sysdte from dual;
spool /opt/mchugh/prod/les/oraclereports/SHipmentByDateRange.out
@Usr-SHipmentByDateRange.sql
spool off
exit
//
sql << //
set trimspool on
spool /opt/mchugh/prod/les/oraclereports/InventoryAllBuildings.out
@Usr-InventoryAllBuildings.sql
spool off
exit
//
sql << //
set trimspool on
spool /opt/mchugh/prod/les/oraclereports/WestburyStaged.out
@WESTBURY-StagedLoadReport.sql
spool off
exit
//
exit 0
