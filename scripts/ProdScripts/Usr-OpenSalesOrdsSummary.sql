/* #REPORTNAME= Open Sales Orders Summary  */
/* #HELPTEXT= Report provides a summary of the information */
/* #HELPTEXT= found in the Open Orders Report from the previous day. */  
/* #HELPTEXT= Requested by Order Management */

/* Author: Al Driver */
/* This report was created to automate the process */
/* of creating the Open Orders Summary Report */
/* This was formerly a manual process. This report */
/* obtains the summary information from the */
/* usr_openords table, which is truncated and */
/* populated every night by use of the */
/* Usr-OpenOrdsReport.sql script. Detailed info */
/* can be obtained from the Usr-OpenSalesOrds.sql report. */

-- set linesize 400
set serveroutput on
declare

c_blankline varchar2(10) := chr(13);
c_col_width number := 13;

l_refdte date;
l_eomdte date;

type ordsum is record (pend_ord number,
		       pend_amt number,
		       ready_ord number,
                       ready_amt number,
                       trnfr_ord number,
                       trnfr_amt number,
                       rlsd_ord number,
		       rlsd_amt number,
		       stgd_ord number,
		       stgd_amt number,
                       stgd_cnd number,
                       stgd_cnd_amt number,
		       prtd_ord number,
		       prtd_amt number,
		       tshp_ord number,
		       tshp_amt number,
		       totl_ord number,
		       totl_amt number);
type summtab is table of ordsum index by binary_integer;
l_summ summtab;

l_shpcat_col number;

cursor c1 is
select upper(cstcat) cstcat,
       shpcat,
       unsamt,adddte,
       decode(rtrim(status),'In Process','Released','Backorder','Pending',rtrim(status)) status
  from usr_openords;

procedure print_line(i_title varchar2,
		     i_col1  number,
		     i_col2  number,
		     i_col3  number,
		     i_col4  number,
		     i_col5  number,
		     i_col6  number,
		     i_col7  number,
                     i_col8  number,
                     i_col9  number) is

c_num_fmt number;

begin
  dbms_output.put_line(rpad(i_title,35,' ') ||
		      lpad(to_char(i_col1, c_num_fmt),c_col_width,' ')||' '||
		      lpad(to_char(i_col2, c_num_fmt),c_col_width,' ')||' '||
		      lpad(to_char(i_col3, c_num_fmt),c_col_width,' ')||' '||
		      lpad(to_char(i_col4, c_num_fmt),c_col_width,' ')||' '||
		      lpad(to_char(i_col5, c_num_fmt),c_col_width,' ')||' '||
		      lpad(to_char(i_col6, c_num_fmt),c_col_width,' ')||' '||
		      lpad(to_char(i_col7, c_num_fmt),c_col_width,' ')||' '||
                      lpad(to_char(i_col8, c_num_fmt),c_col_width,' ')||' '||
                      lpad(to_char(i_col9, c_num_fmt),c_col_width,' '));
end;

begin
  dbms_output.enable(1000000);
  select max(adddte) into l_refdte from usr_openords;
  l_eomdte := last_day(l_refdte);

  for i in 0..8 loop
    l_summ(i).pend_ord := 0;
    l_summ(i).pend_amt := 0;
    l_summ(i).ready_ord := 0;
    l_summ(i).ready_amt := 0;
    l_summ(i).trnfr_ord :=0;
    l_summ(i).trnfr_amt :=0;   
    l_summ(i).rlsd_ord := 0;
    l_summ(i).rlsd_amt := 0;
    l_summ(i).stgd_ord := 0;
    l_summ(i).stgd_amt := 0;
    l_summ(i).stgd_cnd :=0;
    l_summ(i).stgd_cnd_amt :=0;
    l_summ(i).prtd_ord := 0;
    l_summ(i).prtd_amt := 0;
    l_summ(i).tshp_ord := 0;
    l_summ(i).tshp_amt := 0;
    l_summ(i).totl_ord := 0;
    l_summ(i).totl_amt := 0;
  end loop;

  for c in c1 loop
    if rtrim(c.cstcat) = 'FARB' then
      l_shpcat_col := 1;
    else
      l_shpcat_col := 0;
    end if;

    if c.shpcat = 'Before Today' and 
    trunc(c.adddte) < trunc(sysdate) - usrGetBusDaysMore(sysdate) then
      l_shpcat_col := l_shpcat_col + 1;
    elsif c.shpcat = 'Before Today' and trunc(c.adddte) between trunc(sysdate) - usrGetBusDaysMore(sysdate) 
    and trunc(sysdate) - 1 then 
    l_shpcat_col := l_shpcat_col + 3;
    elsif c.shpcat = 'This Month' then
      l_shpcat_col := l_shpcat_col + 5;
    else
      l_shpcat_col := l_shpcat_col + 7;
    end if;

    if c.status = 'Transfer' then
      l_summ(l_shpcat_col).trnfr_ord := l_summ(l_shpcat_col).trnfr_ord + 1;
      l_summ(l_shpcat_col).trnfr_amt := l_summ(l_shpcat_col).trnfr_amt + c.unsamt;
      l_summ(l_shpcat_col).tshp_ord := l_summ(l_shpcat_col).tshp_ord + 1;
      l_summ(l_shpcat_col).tshp_amt := l_summ(l_shpcat_col).tshp_amt + c.unsamt;      
      l_summ(0).trnfr_ord := l_summ(0).trnfr_ord + 1;
      l_summ(0).trnfr_amt := l_summ(0).trnfr_amt + c.unsamt; 
      l_summ(0).tshp_ord := l_summ(0).tshp_ord + 1;
      l_summ(0).tshp_amt := l_summ(0).tshp_amt + c.unsamt; 
    elsif c.status = 'Pending' then
      l_summ(l_shpcat_col).pend_ord := l_summ(l_shpcat_col).pend_ord + 1;
      l_summ(l_shpcat_col).pend_amt := l_summ(l_shpcat_col).pend_amt + c.unsamt;
      l_summ(0).pend_ord := l_summ(0).pend_ord + 1;
      l_summ(0).pend_amt := l_summ(0).pend_amt + c.unsamt;
    elsif (c.status = 'Released' or c.status = 'In Process') then
      l_summ(l_shpcat_col).rlsd_ord := l_summ(l_shpcat_col).rlsd_ord + 1;
      l_summ(l_shpcat_col).rlsd_amt := l_summ(l_shpcat_col).rlsd_amt + c.unsamt;
      l_summ(0).rlsd_ord := l_summ(0).rlsd_ord + 1;
      l_summ(0).rlsd_amt := l_summ(0).rlsd_amt + c.unsamt;
    elsif c.status = 'Ready' then
      l_summ(l_shpcat_col).ready_ord := l_summ(l_shpcat_col).ready_ord + 1;
      l_summ(l_shpcat_col).ready_amt := l_summ(l_shpcat_col).ready_amt + c.unsamt;
      l_summ(0).ready_ord := l_summ(0).ready_ord + 1;
      l_summ(0).ready_amt := l_summ(0).ready_amt + c.unsamt;  
    elsif c.status = 'Staged' then
      l_summ(l_shpcat_col).stgd_ord := l_summ(l_shpcat_col).stgd_ord + 1;
      l_summ(l_shpcat_col).stgd_amt := l_summ(l_shpcat_col).stgd_amt + c.unsamt;
      l_summ(l_shpcat_col).tshp_ord := l_summ(l_shpcat_col).tshp_ord + 1;
      l_summ(l_shpcat_col).tshp_amt := l_summ(l_shpcat_col).tshp_amt + c.unsamt;
      l_summ(0).stgd_ord := l_summ(0).stgd_ord + 1;
      l_summ(0).stgd_amt := l_summ(0).stgd_amt + c.unsamt;
      l_summ(0).tshp_ord := l_summ(0).tshp_ord + 1;
      l_summ(0).tshp_amt := l_summ(0).tshp_amt + c.unsamt;
    elsif c.status = 'Staged-CND' then
      l_summ(l_shpcat_col).stgd_cnd := l_summ(l_shpcat_col).stgd_cnd + 1;
      l_summ(l_shpcat_col).stgd_cnd_amt := l_summ(l_shpcat_col).stgd_cnd_amt + c.unsamt;
      l_summ(l_shpcat_col).tshp_ord := l_summ(l_shpcat_col).tshp_ord + 1;
      l_summ(l_shpcat_col).tshp_amt := l_summ(l_shpcat_col).tshp_amt + c.unsamt;
      l_summ(0).stgd_cnd := l_summ(0).stgd_cnd + 1;
      l_summ(0).stgd_cnd_amt := l_summ(0).stgd_cnd_amt + c.unsamt;
      l_summ(0).tshp_ord := l_summ(0).tshp_ord + 1;
      l_summ(0).tshp_amt := l_summ(0).tshp_amt + c.unsamt;
    elsif c.status = 'Printed' then
      l_summ(l_shpcat_col).prtd_ord := l_summ(l_shpcat_col).prtd_ord + 1;
      l_summ(l_shpcat_col).prtd_amt := l_summ(l_shpcat_col).prtd_amt + c.unsamt;
      l_summ(l_shpcat_col).tshp_ord := l_summ(l_shpcat_col).tshp_ord + 1;
      l_summ(l_shpcat_col).tshp_amt := l_summ(l_shpcat_col).tshp_amt + c.unsamt;
      l_summ(0).prtd_ord := l_summ(0).prtd_ord + 1;
      l_summ(0).prtd_amt := l_summ(0).prtd_amt + c.unsamt;
      l_summ(0).tshp_ord := l_summ(0).tshp_ord + 1;
      l_summ(0).tshp_amt := l_summ(0).tshp_amt + c.unsamt;
    end if;
    l_summ(l_shpcat_col).totl_ord := l_summ(l_shpcat_col).totl_ord + 1;
    l_summ(l_shpcat_col).totl_amt := l_summ(l_shpcat_col).totl_amt + c.unsamt;
    l_summ(0).totl_ord := l_summ(0).totl_ord + 1;
    l_summ(0).totl_amt := l_summ(0).totl_amt + c.unsamt;

  end loop;


-- column headings
  dbms_output.put_line(c_blankline||rpad(' ',35,' ')||
		       lpad('Before Today',c_col_width,' ')||' '||
		       lpad('Before Today',c_col_width,' ')||' '||
                       lpad('Today to EOM',c_col_width,' ')||' '||
                       lpad('Next month +',c_col_width,' ')||' '||
		       lpad('Before Today',c_col_width,' ')||' '||
		       lpad('Before Today',c_col_width,' ')||' '||
		       lpad('Today to EOM',c_col_width,' ')||' '||
		       lpad('Next month +',c_col_width,' ')||' '||
		       lpad('            ',c_col_width,' '));
  dbms_output.put_line(c_blankline||rpad(' ',35,' ')||
                       lpad('ORSI',c_col_width,' ')||' '||
                       lpad('ORSI',c_col_width,' ')||' '||
                       lpad('ORSI',c_col_width,' ')||' '||
                       lpad('ORSI',c_col_width,' ')||' '||
                       lpad('NON-ORSI',c_col_width,' ')||' '||
                       lpad('NON-ORSI',c_col_width,' ')||' '||
                       lpad('NON-ORSI',c_col_width,' ')||' '||
                       lpad('NON-ORSI',c_col_width,' ')||' '||
                       lpad('           ',c_col_width,' '));
  dbms_output.put_line(c_blankline||rpad(' ',35,' ')||
		       lpad('           ',c_col_width,' ')||' '||
		       lpad('(less than 3',c_col_width,' ')||' '||
                       lpad('            ',c_col_width,' ')||' '||
                       lpad('            ',c_col_width,' ')||' '||
		       lpad('            ',c_col_width,' ')||' '||
		       lpad('(less than 3',c_col_width,' ')||' '||
		       lpad('           ',c_col_width,' ')||' '||
		       lpad('           ',c_col_width,' ')||' '||
                       lpad('           ',c_col_width,' '));
dbms_output.put_line(c_blankline||rpad(' ',35,' ')||
                       lpad('           ',c_col_width,' ')||' '||
                       lpad('working days)',c_col_width,' ')||' '||
                       lpad('             ',c_col_width,' ')||' '||
                       lpad('             ',c_col_width,' ')||' '||
                       lpad('             ',c_col_width,' ')||' '||            
                       lpad('working days)',c_col_width,' ')||' '||
                       lpad('           ',c_col_width,' ')||' '||
                       lpad('           ',c_col_width,' ')||' '||
                       lpad('TOTALS',c_col_width,' '));  

-- detail lines
  print_line('#OF PENDING',
	     l_summ(2).pend_ord,
	     l_summ(4).pend_ord,
	     l_summ(6).pend_ord,
	     l_summ(8).pend_ord,
	     l_summ(1).pend_ord,
	     l_summ(3).pend_ord,
             l_summ(5).pend_ord,
             l_summ(7).pend_ord, 
	     l_summ(0).pend_ord);
  print_line('$VALUE OF PENDING',
	     l_summ(2).pend_amt,
	     l_summ(4).pend_amt,
	     l_summ(6).pend_amt,
	     l_summ(8).pend_amt,
	     l_summ(1).pend_amt,
	     l_summ(3).pend_amt,
             l_summ(5).pend_amt,
             l_summ(7).pend_amt, 
	     l_summ(0).pend_amt);
  dbms_output.put_line(c_blankline); 
  print_line('#OF READY',
             l_summ(2).ready_ord,
             l_summ(4).ready_ord,
             l_summ(6).ready_ord,
             l_summ(8).ready_ord,
             l_summ(1).ready_ord,
             l_summ(3).ready_ord,
             l_summ(5).ready_ord,
             l_summ(7).ready_ord,
             l_summ(0).ready_ord);
  print_line('$VALUE OF READY',
             l_summ(2).ready_amt,
             l_summ(4).ready_amt,
             l_summ(6).ready_amt,
             l_summ(8).ready_amt,
             l_summ(1).ready_amt,
             l_summ(3).ready_amt,
             l_summ(5).ready_amt,
             l_summ(7).ready_amt, 
             l_summ(0).ready_amt);
  dbms_output.put_line(c_blankline);
  print_line('#OF RELEASED',
	     l_summ(2).rlsd_ord,
	     l_summ(4).rlsd_ord,
	     l_summ(6).rlsd_ord,
	     l_summ(8).rlsd_ord,
	     l_summ(1).rlsd_ord,
	     l_summ(3).rlsd_ord,
             l_summ(5).rlsd_ord,
             l_summ(7).rlsd_ord, 
	     l_summ(0).rlsd_ord);
  print_line('$VALUE OF RELEASED',
	     l_summ(2).rlsd_amt,
	     l_summ(4).rlsd_amt,
	     l_summ(6).rlsd_amt,
	     l_summ(8).rlsd_amt,
	     l_summ(1).rlsd_amt,
	     l_summ(3).rlsd_amt,
             l_summ(5).rlsd_amt,
             l_summ(7).rlsd_amt,
	     l_summ(0).rlsd_amt);
  dbms_output.put_line(c_blankline);
 print_line('#OF TRANSFERS',
             l_summ(2).trnfr_ord,
             l_summ(4).trnfr_ord,
             l_summ(6).trnfr_ord,
             l_summ(8).trnfr_ord,
             l_summ(1).trnfr_ord,
             l_summ(3).trnfr_ord,
             l_summ(5).trnfr_ord,
             l_summ(7).trnfr_ord,
             l_summ(0).trnfr_ord);  
 print_line('#OF STAGED',
	     l_summ(2).stgd_ord,
	     l_summ(4).stgd_ord,
	     l_summ(6).stgd_ord,
	     l_summ(8).stgd_ord,
	     l_summ(1).stgd_ord,
	     l_summ(3).stgd_ord,
             l_summ(5).stgd_ord,
             l_summ(7).stgd_ord,  
	     l_summ(0).stgd_ord);
print_line('#OF STAGED-CND',
             l_summ(2).stgd_cnd,
             l_summ(4).stgd_cnd,
             l_summ(6).stgd_cnd,
             l_summ(8).stgd_cnd,
             l_summ(1).stgd_cnd,
             l_summ(3).stgd_cnd,
             l_summ(5).stgd_cnd,
             l_summ(7).stgd_cnd,
             l_summ(0).stgd_cnd);
  print_line('#OF PRINTED (loading)',
	     l_summ(2).prtd_ord,
	     l_summ(4).prtd_ord,
	     l_summ(6).prtd_ord,
	     l_summ(8).prtd_ord,
	     l_summ(1).prtd_ord,
	     l_summ(3).prtd_ord,
             l_summ(5).prtd_ord,
             l_summ(7).prtd_ord,
	     l_summ(0).prtd_ord);
  print_line('TOTAL TO SHIP NOW',
             l_summ(2).tshp_ord,
             l_summ(4).tshp_ord,
             l_summ(6).tshp_ord,
             l_summ(8).tshp_ord,
             l_summ(1).tshp_ord,
             l_summ(3).tshp_ord,
             l_summ(5).tshp_ord,
             l_summ(7).tshp_ord,
             l_summ(0).tshp_ord); 
  dbms_output.put_line(c_blankline);
 print_line('$VALUE OF TRANSFERS',
             l_summ(2).trnfr_amt,
             l_summ(4).trnfr_amt,
             l_summ(6).trnfr_amt,
             l_summ(8).trnfr_amt,
             l_summ(1).trnfr_amt,
             l_summ(3).trnfr_amt,
             l_summ(5).trnfr_amt,
             l_summ(7).trnfr_amt,
             l_summ(0).trnfr_amt); 
 print_line('$VALUE OF STAGED',
	     l_summ(2).stgd_amt,
	     l_summ(4).stgd_amt,
	     l_summ(6).stgd_amt,
	     l_summ(8).stgd_amt,
	     l_summ(1).stgd_amt,
	     l_summ(3).stgd_amt,
             l_summ(5).stgd_amt,
             l_summ(7).stgd_amt,
	     l_summ(0).stgd_amt);
print_line('$VALUE OF STAGED-CND',
             l_summ(2).stgd_cnd_amt,
             l_summ(4).stgd_cnd_amt,
             l_summ(6).stgd_cnd_amt,
             l_summ(8).stgd_cnd_amt,
             l_summ(1).stgd_cnd_amt,
             l_summ(3).stgd_cnd_amt,
             l_summ(5).stgd_cnd_amt,
             l_summ(7).stgd_cnd_amt,
             l_summ(0).stgd_cnd_amt);
  print_line('$VALUE OF PRINTED (loading)',
	     l_summ(2).prtd_amt,
	     l_summ(4).prtd_amt,
	     l_summ(6).prtd_amt,
	     l_summ(8).prtd_amt,
	     l_summ(1).prtd_amt,
	     l_summ(3).prtd_amt,
             l_summ(5).prtd_amt,
             l_summ(7).prtd_amt,
	     l_summ(0).prtd_amt);
  print_line('$VALUE TOTAL TO SHIP NOW',
             l_summ(2).tshp_amt,
             l_summ(4).tshp_amt,
             l_summ(6).tshp_amt,
             l_summ(8).tshp_amt,
             l_summ(1).tshp_amt,
             l_summ(3).tshp_amt,
             l_summ(5).tshp_amt,
             l_summ(7).tshp_amt,
             l_summ(0).tshp_amt); 
  dbms_output.put_line(c_blankline);
  print_line('TOTAL ORDERS NOT SHIPPED',
	     l_summ(2).totl_ord,
	     l_summ(4).totl_ord,
	     l_summ(6).totl_ord,
	     l_summ(8).totl_ord,
	     l_summ(1).totl_ord,
	     l_summ(3).totl_ord,
             l_summ(5).totl_ord,
             l_summ(7).totl_ord,
	     l_summ(0).totl_ord);
 dbms_output.put_line(c_blankline); 
 print_line('TOTAL VALUE NOT SHIPPED',
	     l_summ(2).totl_amt,
	     l_summ(4).totl_amt,
	     l_summ(6).totl_amt,
	     l_summ(8).totl_amt,
	     l_summ(1).totl_amt,
	     l_summ(3).totl_amt,
             l_summ(5).totl_amt,
             l_summ(7).totl_amt, 
	     l_summ(0).totl_amt);

end;
/
