#!/usr/local/bin/perl
#
# Dustin Radtke
# This file should include all database purges and archives
# A cron task or at job should be created to call this script
# a few times a day
#
$| = 1;                   # Flush output

$cmd=<<EOF;                                
set trace where activate = 1 and trcfil = 'midday_purge_data.trc'
/
remove seamles event where dayold = ".25" and evt_id = 'VC_MNT-HEARTBEAT' and evt_stat_cd = 'SC'
/
remove seamles event where dayold = ".25" and evt_id = 'VC_HEARTBEAT OUT' and evt_stat_cd = 'SC'
/
sl_purge msg_log where older_than_days = 14
/
EOF

$time = localtime;
print "$time \n $cmd";

open(MSQL,"| msql ") || die;
print MSQL $cmd;
close(MSQL);

exit(0);
