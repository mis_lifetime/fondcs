/**********************************************************************************************************/
/* This script will populate the usr_loctemp table with the contents of the aremst table, before the start*/
/* of Physical.  This should be done when the snapshot and other updates are done.                        */
/**********************************************************************************************************/

truncate table usr_loctemp;

/* Populate table with current data */

insert into usr_loctemp 
(
select * from locmst)
/
commit;
