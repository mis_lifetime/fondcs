/*This query list all complete shipment */
set pagesize 10000
set linesize 90
spool 50_shipment_completed.rpt

select distinct s.ship_id, sl.ordnum, sl.ordlin, ol.prtnum, sl.shpqty, s.loddte
from 
     ord_line ol, 
     shipment_line sl,
     shipment s
where ol.ordnum = sl.ordnum
  and ol.ordlin = sl.ordlin
  and ol.ordsln = sl.ordsln
  and ol.client_id = sl.client_id
  and ol.client_id = '----'
  and sl.ship_id = s.ship_id
  and s.shpsts  = 'C'
  and s.loddte >= to_date('1/1/2001','MM/DD/YYYY')
order by s.ship_id,  
         sl.ordnum,
         sl.ordlin;

spool off
/
