/* This script updates returns information from the  */
/* usr_rework and usr_reason tables. To be used to */
/* provide rework report for Inventory Control. This can be run */
/* after both tables have been truncated and updated. */
/* The script uses an outer join to obtain results for all */
/* parts found in the usr_rework table. */


spool Reworks.txt
set pagesize 10000
set linesize 300

select a.prtnum,a.stoloc,b.reason,b.coding		
from usr_rework a,usr_reason b		
where a.prtnum	= b.prtnum(+)		
and a.stoloc =	b.stoloc(+);		

spool off
/
