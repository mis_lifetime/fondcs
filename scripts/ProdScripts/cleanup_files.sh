#!/usr/bin/ksh
#
# This script will cleanup old files 
#
cd $HOME
find .  \( -type f -name "*.log*" -mtime +3 -print \) -o \( -type f -name "*cmdprf" -mtime +1 -print \) -o \( -type f -name "*.tmp" -mtime +3 -print \) -o \( -type f -name "core" -mtime +1 -print \) -o \( -type f -name "AllocInv.Sts*" -mtime +0 -print \) -o \( -type f -name "*.trc" -mtime +0 -print \) -o \( -type f -name "AllocLoc.Sts*" -mtime +0 -print \) | xargs rm
