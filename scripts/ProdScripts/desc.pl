#!/usr/local/bin/perl
use lib "$ENV{MOCADIR}/scripts";
use MocaReg; 
#use strict;
use DBI;

$tabnam = $ARGV[0]; 

$dbuser = MocaReg->get("Server", "dbuser");
$dbpass = MocaReg->get("Server", "dbpass");
$dbconn = MocaReg->get("Server", "dbconn"); 

# Which database are we looking at?
my ($usr, $pwd, $db) = ($dbuser, $dbpass, $dbconn);

# connect to the database
my $db_h = DBI->connect("DBI:Oracle:$db", $usr, $pwd)
	or die "Count not connect to database: " . DBI->errstr;

# Start by finding the objects that span more than $extent_limit # of extent
my $st_h = $db_h->prepare("select rtrim(column_name) ,  " .
                          "data_type||'('||ltrim(trunc(data_length))||')' ".
                          "from user_tab_columns " .
                          "where table_name = upper('$tabnam')")
	or die "Could not prepare statement: " . $db_h->errstr;

$st_h->execute()
	or die "Could not execute statement: " . $st_h->errstr;

my @row = ();
my %segments = ();

print "\n\n";
print "Column Name               Data Type\n";
print '-' x 35 . "\n";
my $previous_segment_type;
my $index = 0;
while (@row = $st_h->fetchrow_array()) {
	my ($colnam, $dattyp) = @row;
	# this if-els block would print a blank line between the list of tables and indexes.
	printf "%-20s%15s \n", $colnam, $dattyp;
}

# release the statement handle
$st_h->finish;

$db_h->disconnect;

