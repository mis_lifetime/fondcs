/* Author:  Al Driver */

/* This script is used to identify and update */
/* records in usr_cnthst where the prtnum <> 'EMPTY' */
/* and the untcst is null. */
/* The untcst should be updated with its corresponding  */
/* cost found in prtmst */

select cntbat,usr_cnthst.prtnum,usr_cnthst.untcst,stoloc,usr_cnthst.cntdte from
usr_cnthst,prtmst where usr_cnthst.prtnum = prtmst.prtnum 
and usr_cnthst.prtnum <> 'EMPTY'
and usr_cnthst.untcst is null
and prtmst.untcst is not null;


update usr_cnthst a
    set a.untcst = (select b.untcst from prtmst b
    where b.prtnum = a.prtnum and b.untcst is not null)
    where exists(select 'x' from prtmst c where c.prtnum = a.prtnum
    and c.untcst is not null)
    and a.prtnum <> 'EMPTY'
    and a.untcst is null;

commit;
