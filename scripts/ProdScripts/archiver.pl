#!/usr/local/bin/perl

########
# Notes
########

#
# Platform-independence and Compressors
#
# We want this script to be as "platform-independent" as possible.
# Earlier versions were tied to Win32 (actually, NT4's version of
# DOS) both in filesystem standards and in choice of compressor.
# In this version, we use Perl intrinsic functions for filesystem
# operations, giving platform-independence there, and we also do
# a search through the user's PATH to find the first appropriate
# compressor (gzip, compress, pkzip, etc.) in the PATH.  It's not
# difficult to add additional compressors as they become available.
#
# File Path Formats
#
# Intrinsic Perl functions like to deal with pathnames containing the 
# UNIX-style /.  However, we want our users to be able to enter the 
# DOS-style \ if they are running under DOS.  We convert the paths 
# automatically after we read them from the argument list.  Also,
# when we are searching for a compressor, if we find any \ in the
# paths that we are reading, we assume we are in the DOS-world,
# and we set up the returned command appropriately (the system()
# call still needs the OS-specific format).
#
# Filesystem File Patterns
#
# We want the user to be able to enter filepaths and have pattern
# matching work as expected for the operating system.  For example,
# the user might want to enter "*xml*.log".  However, that's not
# a valid perl regular expression.  For perl, the expression would
# be ".*xml.*\.log" or something similar to that.  We assume that the
# user is entering a filesystem pattern and convert to a perl pattern.
#


################
# Package setup
################

use DirHandle;
use strict 'vars';
use Getopt::Long;
use Env;


#################
# Constant setup
#################

my $set          = 1;
my $unset        = 0;
my $blank        = "";
my $unset_num    = -999;  # Yicky, yes, but I'll have to do.

my $seconds_per_day = (60*60*24);


########################
# Variable declarations
########################

# General variables
my $return       = -1;
my $source_dir   = $blank;
my $archive_dir  = $blank;
my $file_pattern = $blank;
my $help         = $unset;
my $dh           = new DirHandle;
my $days         = $unset_num;
my $chosen_compressor = $blank;

my $orig_file_pattern;

my $date;
my $time;
my $arch_time;

my $file;
my %FILES;

my $new_file;

my $archive_count = 0;

# Variables for use with compression
my $compressor_name = "";
my $compressor_path = "";
my $extension       = "";
my $output_file     = "";
my $command         = "";

my $use_command     = "";
my $use_output_file = "";

my $no_compress     = $unset;


###################
# Set the platform
###################

my $platform;
if($^O =~ /dos/i or $^O =~ /Win32/i)
{
   # This is DOS/Windows mode
   $platform = "windoze";
}
else
{
   # We hope it's not MacOS or Plan9 or whatever...
   $platform = "unix";
}
   


################################
# Handle command-line arguments
################################

$return = GetOptions("s=s"  => \$source_dir,
                     "a=s"  => \$archive_dir,
                     "x"    => \$no_compress,
                     "f=s"  => \$file_pattern,
                     "d=n"  => \$days,
                     "c=s"  => \$chosen_compressor,
                     "help" => \$help);

if($return == 0)
{
   usage();
   exit 1;
}

if($help == $set)
{
   usage();
   exit 0;
}

if($source_dir eq $blank)
{
   print "You have to enter the source directory.\n";
   exit 1;
}
else
{
   # Change \ to /.
   $source_dir =~ s|\\|\/|g;
}

if($archive_dir eq $blank)
{
   print "You have to enter the archive directory.\n";
   exit 1;
}
else
{
   # Change \ to /.
   $archive_dir =~ s|\\|\/|g;
}

if($file_pattern eq $blank)
{
   print "You have to enter the file pattern.\n";
   exit 1;
}
else
{
   # Convert from filesystem pattern to perl pattern.
   # This turns "*xml*.log" into ".*xml.*\.log".
   # Also, we can now search for xml?.log, etc.
   $orig_file_pattern = $file_pattern;
   $file_pattern =~ s|\.|\\.|g;
   $file_pattern =~ s|\*|\.\*|g;
   $file_pattern =~ s|\?|.|g;
}

if ($days != $unset_num)
{
   if($days <= 0)
   {
      print "The number of days has to be a positive number.\n";
      exit 1;
   }

   $arch_time = time - ($seconds_per_day * $days);
}
 

#####################
# Get date and time.
#####################

my ($sec,$min,$hour,$mday,$mon,$year,$yday,$isdst) = localtime(time);

if($mon+1 < 10){ $mon  = "0".($mon+1);}
if($mday  < 10){ $mday = "0".$mday;   }
if($hour  < 10){ $hour = "0".$hour;   }
if($min   < 10){ $min  = "0".$min;    }

$date = (1900+$year).$mon.$mday;
$time = $hour.$min;


###########################
# Print a starting message
###########################

print "\nStarting the archive process for dir ($source_dir)...\n";

if($platform eq "windoze")
{
   print "Working in a Windows/DOS-style environment.\n";
}
else
{
   print "Working in a UN*X-style environment.\n";
}

print "Date/time is ($date/$time).\n";

if($days != $unset_num)
{
   print "Files older than $days days will be archived.\n";
}

print "Files matching pattern ($orig_file_pattern) will be archived.\n";
print "Files will be archived to ($archive_dir).\n";


##############################
# Find the compressor program
##############################

if($no_compress == $set)
{
   print "Files will not be compressed.\n";
}
else
{
   # We only need to do this if they want to compress

   if($chosen_compressor ne "")
   {
      print "User has requested compressor ($chosen_compressor).\n";
   }

   if (FindCompressor($chosen_compressor,
                      \$compressor_name,
                      \$compressor_path,
                      \$extension,
                      \$output_file,
                      \$command)         ne TRUE)
   {
      print "Unable to find a compressor!\n";
      exit 1;
   }

   print "Files will be compressed using ($compressor_path).\n";

}


############################################
# Get a list of files from the file pattern
############################################

if(!opendir($dh, "$source_dir"))
{
   print "Unable to open directory $source_dir\n";
   exit 1;
}

foreach $file (sort readdir($dh))
{
   # We want to make sure that the file pattern matches the
   # WHOLE name, not just part of the name.
   if(-f "$source_dir/$file" and $file =~ /^$file_pattern$/)
   {
      if($days != $unset_num)
      {
         my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,
             $size,$atime,$mtime,$ctime,$blksize,$blocs) = stat("$source_dir/$file");

         if($mtime <= $arch_time)
         {
            $FILES{$file} = "$file";
         }
      }
      else
      {
        $FILES{$file} = "$file";
      }
   }
}

closedir($dh);


#############################################################
# Only continue if actual files exist which meet the pattern
#############################################################

if((keys %FILES) < 1)
{
   $archive_count = 0;
}
else
{

   #########################################
   # Make sure the archive directory exists
   #########################################

   if(mkdir("$archive_dir",0777))
   {
      print "Directory $archive_dir successfully created.\n";
   }
      

   #################################
   # Compress and delete each file.
   #################################

   foreach $file (sort keys %FILES)
   {  

      print "\n    *** Archiving file ($file) ... ";

      $new_file = "${date}${time}_${file}";
      if(!rename("$source_dir/$file","$source_dir/$new_file"))
      {
         print "failed to rename.";
         next;
      }
                                    
      if ($no_compress == $set)
      {
         # Just rename
         if(!rename("$source_dir/$new_file","$archive_dir/$new_file"))
         {
            print "failed to archive.";
            next;
         }
      }
      else
      {
         # Compress and then rename
         if (GetCompressorCmd($source_dir,
                               $new_file,
                               \$use_command,
                               \$use_output_file,
                               $compressor_name,
                               $compressor_path,
                               $extension,
                               $output_file,
                               $command)           ne TRUE)
         {  
            print "failed to find compressor.";
            exit 1;
         }

         if(system("$use_command") != 0)
         {
            print "failed to compress";

            # Eh, just move off the file as it is.
            if(!rename("$source_dir/$new_file",
                       "$archive_dir/$new_file"))
            {
               print " (and archive)";
               next;
            }

            $archive_count++;
            print ".";
            next;
         }

         if(! -f "$source_dir/$use_output_file")
         {
            # Assume that it hasn't been compressed, I guess.
            if(!rename("$source_dir/$new_file",
                       "$archive_dir/$new_file"))
            {
               print "failed to archive.";
               next;
            }
         }
         else
         {
            # Move the compressed file off.
            if(!rename("$source_dir/$use_output_file",
                       "$archive_dir/$use_output_file"))
            {
               print "failed to archive.";
               next;
            }
         }
      }

      $archive_count++;
      print "done";
      
   } #End of for loop.
}


################################
# And print a completed message
################################

if($archive_count > 0)
{
   print "\n\nCompleted.  A total of ($archive_count) files were archived.\n";
}
else
{
   print "\n\nCompleted.  No files were archived.\n";
}

exit 0;


###################
# Usage subroutine
###################
# Formatted for an 80-column display.

sub usage()
{
   print "\nUsage: archiver.pl -s src-dir -a arch-dir -f file-ptn [-c prog] [-x] [-d days]\n";
   print "\n\t-s src-dir \t- Directory to archive files from\n";
   print "\t-a arch-dir \t- Directory to archive files to\n";
   print "\t-f file-ptn \t- File pattern specifying files to archive\n";
   print "\t-c prog     \t- Program to use for compression (no path, i.e. \"gzip\")\n";
   print "\t-x          \t- Do not compress (use if no compressor is available)\n";
   print "\t-d days     \t- Archive files older then certain number of days\n";
   print "\t--help      \t- Show this message\n";
   print "\nArchives files identified by file-ptn from the src-dir to the arch-dir.\n";
   print "If arch-dir does not exist, it will be created.  Files will be compressed\n";
   print "using the available compressor (such pkzip or gzip) found in the PATH unless\n";
   print "-x is used.  You can attempt to force a particular program with -c; if that\n";
   print "program is not found, the archive process will stop.\n";
   print "\nThe src-dir, arch-dir and file-ptn parameters are required.  The -x and\n";
   print "days parameters are optional.\n";
   print "\nKeep in mind that you'll have to quote file patterns, i.e. -f \"*.log\".\n\n";
}


##############################
# GetCompressorCmd subroutine
##############################
# Tells the caller what command to use to compress a given file, and tells 
# the caller what the name of the compressed file will be.  Information from
# the FindCompressor subroutine is required.

sub GetCompressorCmd($$$$$$$$)
{

   ############
   # Variables
   ############

   my ($dir,
       $file,
       $use_command_ref,
       $use_output_file_ref,
       $compressor_name,
       $compressor_path,
       $extension,
       $output_file,
       $command)          = (@_);

   my $status = TRUE;


   ############################
   # Substitute as appropriate
   ############################
 
   # This will only work if we have a command
   if($command eq "" or $output_file eq "")
   {
      $status = FALSE;
      return $status;
   }

   # Place the $dir and $file into the command
   $$use_command_ref = $command;
   $$use_command_ref =~ s/\*\*\*FILE\*\*\*/$file/g;
   $$use_command_ref =~ s/\*\*\*DIR\*\*\*/$dir/g;

   # Place the $file into the output file
   $$use_output_file_ref = $output_file;
   $$use_output_file_ref =~ s/\*\*\*FILE\*\*\*/$file/g;

   # Set the slash/backslash up for Windoze
   if($platform eq "windoze")
   {
      # We need to use all \ for Windoze.
      $$use_command_ref =~ s|\/|\\|g;
      $$use_output_file_ref =~ s|\/|\\|g;
   }

   return $status;
}


############################
# FindCompressor subroutine
############################
# Searches the path to find the first known compressor available.  Information 
# about the syntax of a call to that compressor, as well as information about the
# resultant file, is returned.  This information is used by the GetCompressorCmd 
# subroutine - it puts the actual filename into the space where ***FILE*** is, 
# and puts the actual directory into the space where ***DIR*** is.

sub FindCompressor($$$$$$)
{
   ############
   # Variables
   ############

   my ($chosen_compressor,
       $compressor_name_ref,
       $compressor_path_ref,
       $extension_ref,
       $output_file_ref,
       $command_ref)          = (@_);

   my @path_list;
   my $path_entry;
   my $file;
   my $dh = new DirHandle;


   ##################
   # Search the PATH
   ##################
   # Spin through the user's path and look for a known compressor.  
   # Then set variables based on known values.

   my $found = FALSE;

   if(!defined($ENV{'PATH'}))
   {
      return $found;
   }

   if($platform eq "windoze")
   {
      @path_list = split(/[;]/, $ENV{'PATH'});
   }
   else
   {
      @path_list = split(/[:]/, $ENV{'PATH'});
   }

   SEARCH: foreach $path_entry (@path_list)
   {
      if(opendir($dh, "$path_entry"))
      {
         foreach $file (sort readdir($dh))
         {
            if(-x "$path_entry/$file" and ! -d "$path_entry/$file")
            {

               if($chosen_compressor ne "" and $file ne $chosen_compressor)
               {
                  # They've asked for a specific one, this doesn't match.
                  next;
               }

                if($file =~ /^gzip$/i or $file =~ /^gzip\.exe$/i)
                {
                  $$compressor_name_ref = $file;
                  $$compressor_path_ref = "$path_entry/$file";
                  $$extension_ref       = "gz";
                  $$output_file_ref     = "***FILE***.$extension";
                  $$command_ref         = "$compressor_path -S \".$extension\" ***DIR***/***FILE***";
                  $found = TRUE;
                  last SEARCH;
               }
               elsif($file =~ /^bzip$/i or $file =~ /^bzip\.exe$/i)
               {
                  $$compressor_name_ref = $file;
                  $$compressor_path_ref = "$path_entry/$file";
                  $$extension_ref       = "bz";
                  $$output_file_ref     = "***FILE***.$extension";
                  $$command_ref         = "$compressor_path -S \".$extension\" ***DIR***/***FILE***";
                  $found = TRUE;
                  last SEARCH;
               }
               elsif($file =~ /^pkzip.*\.exe$/i) # pkzip uses version number, i.e. pkzip25; DOS-only, so only .exe
               {
                  $$compressor_name_ref = $file;
                  $$compressor_path_ref = "$path_entry/$file";
                  $$extension_ref       = "zip";
                  $$output_file_ref     = "***FILE***.$extension";
                  $$command_ref         = "$compressor_path -add -move ***DIR***/***FILE***.$extension ***DIR***/***FILE***";
                  $found = TRUE;
                  last SEARCH;
               }
               elsif($file =~ /^zip$/i) # UN*X only, so no .exe 
               {
                  $$compressor_name_ref = $file;
                  $$compressor_path_ref = "$path_entry/$file";
                  $$extension_ref       = "zip";
                  $$output_file_ref     = "***FILE***.$extension";
                  $$command_ref         = "$compressor_path -m ***DIR***/***FILE***.$extension ***DIR***/***FILE***";
                  $found = TRUE;
                  last SEARCH;
               }
               elsif($file =~ /^compress$/i) # UN*X only, so no .exe
               {
                  $$compressor_name_ref = $file;
                  $$compressor_path_ref = "$path_entry/$file";
                  $$extension_ref       = "Z";
                  $$output_file_ref     = "***FILE***.$extension";
                  $$command_ref         = "$compressor_path -f ***DIR***/***FILE***";
                  $found = TRUE;
                  last SEARCH;
               }
            }
         }

         closedir($dh);
      }

   } # end SEARCH


   ################
   # Convert paths
   ################

   if($platform eq "windoze")
   {
      # This is DOS-mode, use backslashes
      $$compressor_name_ref =~ s|\/|\\|;
      $$compressor_path_ref =~ s|\/|\\|;
      $$extension_ref       =~ s|\/|\\|;
      $$output_file_ref     =~ s|\/|\\|;
      $$command_ref         =~ s|\/|\\|;
      $$command_ref .= " > nul";
   } 
   else
   {
      # This is (hopefully) UN*X mode.
      $$command_ref .= " 2>/dev/null >/dev/null";
   }

   return $found;

}
