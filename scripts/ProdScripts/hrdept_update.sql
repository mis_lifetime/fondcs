/* Below script inserts any new departments that exist in the usr_hrdata table */
/* that are not in the usr_hrdept table.  Script also inserts any new employees */
/* (filenum,empname) into the usr_hrhdr table that exist in usr_hrdata. */
/* This script should be run whenever */
/* new information is loaded into the usr_hrdata table. */
/* For use in the HR-Data.sql report */

/* insert new employees into usr_hrhdr */

insert into usr_hrhdr(filenum,empname)
select distinct filenum,empname from usr_hrdata a
where not exists(select 1 from usr_hrhdr b
where b.filenum = a.filenum and b.empname = a.empname);

commit;

/* insert new departments into usr_hrdept */

insert into usr_hrdept(dept)
select distinct a.dept from usr_hrdata a
where not exists(select 1 from usr_hrdept b where b.dept = a.dept);

commit;
