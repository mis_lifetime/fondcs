/* #REPORTNAME= Open Sales Orders Update Report */
/* #HELPTEXT= This report updates the Open Sales */
/* #HELPTEXT= Orders and Open Sales Orders Summary */
/* #HELPTEXT= reports with todays present information. */
/* #HELPTEXT= This should be run only if both Open Orders */
/* #HELPTEXT= reports need to be updated with todays info. */
/* #HELPTEXT= Requested by Order Management */

/* Author: Al Driver */
/* Description: This report is run nightly to update the */
/* usr_openords table. This table houses the information */
/* for the Open Sales Orders and Summary reports. A .out */
/* file under the name OpenSalesOrders5.out contains the */
/* detail information and the file OpenSalesOrdersSummary.out */
/* has the summary information.  */

column cponum heading 'PO Number'
column cponum format A20
column ordnum heading 'Order'
column ordnum format A10
column status heading 'Status'
column status format A10
column btcust heading 'Customer'
column btcust format A8
column ship_to_name  heading 'Name'
column ship_to_name  format A15
column early_shpdte heading 'Early'
column early_shpdte format A11
column adddte heading 'Added'
column adddte format A11
column cancel_date heading 'Late'
column cancel_date format A11
column unsamt heading '$'
column unsamt format '9,999,990.00'
column extdte heading 'Extension'
column extdte format A11
column ptksts heading 'Preticketing'
column ptksts format A15
column cstcat heading 'Cstcat'
column cstcat format A10
column shpcat heading 'Ship Cat.'
column shpcat format A15
column vc_numbch heading '# of Batch Attempts'
column vc_numbch format 999999

/* First truncate table that houses information */

-- set linesize 500
-- set pagesize 20000

select * from usr_openords;
