/*
  $Log: check_invalid.sql,v $
  Revision 1.1.1.1  2001/09/18 23:05:50  lh51sh
  Initial Import

# Revision 1.10  1996/08/09  18:40:33  dmp_sw
# freeze for V4_10
#
# Revision 1.9  1996/07/02  14:53:14  dmp_di
# Environment variables changed
#
# Revision 1.8  1996/04/19  14:11:07  dmp_sw
# freeze for V4_09
#
# Revision 1.7  1996/02/01  15:11:15  phx_sw
# freeze for V4_08
#
# Revision 1.6  1995/11/09  20:44:17  phx_sw
# freeze for V4_07
#
# Revision 1.5  1995/10/09  15:24:51  phx_dp
# freeze for V4_06
#
# Revision 1.4  1995/09/07  23:47:39  phx_dp
# freeze for V4.05
#
# Revision 1.3  1995/03/10  17:36:29  phx_mz
# ttitle off command
#
# Revision 1.2  1995/03/07  17:19:52  phx_mz
# Spool to lst file
#
# Revision 1.1  1995/03/07  16:54:26  phx_mz
# Initial revision
#
 * Revision 1.1  1994/10/10  19:12:05  phx_sw
 * Initial revision
 *
*/

/* Check_invalid.sql - This sql script will report any objects in the
 *                     user's database that are currently INVALID
 *
 *            Khurram Ahmad 	Original
*/

set pages   55
set lines   80
set newpage 0
set echo    off
ttitle center 'Invalid Objects in the database' right sql.pno format 999 skip 2

set underl '='

col object_type 	head 'Object|Type'
col object_name 	head 'Object|Name' 		form a30
col last_chg 	 	head 'Last Changed|Date/Time'	form a20
break on object_type skip 1

spool $DM_DBLOG/check_invalid.lst

select object_type, 
       object_name,
       to_char(last_ddl_time,'mm/dd/yyyy hh24:mi:ss') last_chg
  from user_objects
 where status = 'INVALID'
order by 1, 2
/

set underl '-'
ttitle off

spool off
