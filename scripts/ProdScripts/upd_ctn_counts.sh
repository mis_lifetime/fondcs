#!/usr/bin/ksh
#
# This script will call the script to update the piece pick and full case carton counts.
#


. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql script.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the script

sql << //
set trimspool on
set pagesize 50000
set linesize 300
spool /opt/mchugh/prod/les/oraclereports/UpdCtnCounts.out
@usr_CartonsCountsUpdate.sql
spool off
exit
//
exit 0
