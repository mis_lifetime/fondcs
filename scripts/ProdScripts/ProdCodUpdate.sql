/* This script follows the updating steps for the vc_prdcod (Product Code)  */
/* in the prtmst table, performed every Friday.  */
/* This can be run after the new prodcode.csv file is */
/* placed in /opt/mchugh/prod/les/db/data/load/prodcode directory */
/* the usr_prodcode table has been truncated and updated. */
/* If one part errors */
/* investigate, because it could cause other records to fail. */
/* INSTEAD OF RUNNING THIS SCRIPT, THERE IS A PROCEDURE CALLED VC_PRDCOD_UPDATE */


/* Do the following queries */

/* Get a count of everything in usr_prodcode (Should have all items found in Dynamics) */

select count(*) from usr_prodcode;

/* Count the items in dynamics that are also */
/* set up in DCS, but have differenct product codes (these are the only items */
/* that should be updated */

select count(a.prtnum) "Records Exist" 
 from prtmst a,usr_prodcode b
 where a.prtnum = b.prtnum
 and nvl(a.vc_prdcod,'NULL') != nvl(b.vc_prdcod,'NULL');  -- look at nulls to catch items with null vc_prdcod

/* Spool out the details of the parts which exist in Dynamics and DCS */
/* but with different vc_prdcod's */

spool ProdCodes.txt
set pagesize 1000

select a.prtnum,a.vc_prdcod
 from prtmst a,usr_prodcode b             
 where a.prtnum = b.prtnum             
 and nvl(a.vc_prdcod,'NULL') != nvl(b.vc_prdcod,'NULL');   

spool off

/* perform update in prtmst */
/* we do not update any codes to null or anything with a length greater than 4 characters */

update prtmst a
set a.vc_prdcod = (select b.vc_prdcod from usr_prodcode b
where b.prtnum = a.prtnum
and nvl(b.vc_prdcod,'NULL') != nvl(a.vc_prdcod,'NULL')
and length(b.vc_prdcod) <= 4)
where exists(select 'x' from usr_prodcode c where c.prtnum = a.prtnum
and nvl(c.vc_prdcod,'NULL') != nvl(a.vc_prdcod,'NULL')
and length(c.vc_prdcod) <= 4);



commit;

/
