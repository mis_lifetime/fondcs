/*
$Log: change_lot_to_NOLOT.sql,v $
Revision 1.1.1.1  2001/09/18 23:05:50  lh51sh
Initial Import

Revision 1.1  2001/06/11  20:22:27  20:22:27  lh51sh
System Test Start

# Karen Henriksen	5/22/2001
#
# $Log: change_lot_to_NOLOT.sql,v $
# Revision 1.1.1.1  2001/09/18 23:05:50  lh51sh
# Initial Import
#
Revision 1.1  2001/06/11  20:22:27  20:22:27  lh51sh
System Test Start

# initial version that changes the lotnum and/or vc_lotnum field of the 
# following tables from "----" to "NOLOT" for Lifetime Hoan for Gap 10.04
# tables include:
#	invdtl.lotnum
#	rcvlin.lotnum
#	invadj.lotnum
#	dlytrn.lotnum
#	cmpdtl.lotnum
#	cstprq.vc_lotnum
#
# 
*/
/*
 set feedback off;
set pagesize 60;
*/

/*
update invdtl set lotnum='NOLOT' where lotnum='----' 
--and prtnum='KJHPART1'
;
update cstprq set vc_lotnum='NOLOT' where vc_lotnum='----' 
--and prtnum='KJHPART1'
;
update rcvlin set lotnum='NOLOT' where lotnum='----' 
--and prtnum='KJHPART1'
;
update invadj set lotnum='NOLOT' where lotnum='----' 
--and prtnum='KJHPART1'
;
update dlytrn set lotnum='NOLOT' where lotnum='----' 
--and prtnum='KJHPART1'
;
update cmpdtl set lotnum='NOLOT' where lotnum='----' 
--and prtnum='KJHPART1'
;
update prtmst set lotflg=1 where lotflg=0
--and prtnum='KJHPART1'
;
*/

exit;

