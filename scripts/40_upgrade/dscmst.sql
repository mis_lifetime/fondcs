-- copy the part descriptions.
-- These are kept in dscmst on 4.0, but in prtdsc on 5.1
-- Note we don't want all of dcsmst, just the parts
insert into prtdsc
(
COLNAM,
COLVAL,
LOCALE_ID,
LNGDSC,
SHORT_DSC
)
select
'prtnum|prt_client_id',
rtrim (COLVAL)||'|----',
'US_ENGLISH',
rtrim (LNGDSC),
substr(rtrim (LNGDSC), 1, 20)
from dscmst_40
where rtrim(colnam)='prtnum'
  and lngdsc is not null
/
