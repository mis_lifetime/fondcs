-- this script set the asgflg to match only what is in rplcfg
update locmst set asgflg=1 where stoloc in (select stoloc from rplcfg)
/
update locmst set asgflg=0 where asgflg=1 and stoloc not in (select stoloc from rplcfg)
/
