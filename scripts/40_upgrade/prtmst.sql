insert into prtmst
(
PRTNUM,
PRT_CLIENT_ID,
UPCCOD,
NDCCOD,
COMCOD,
SUBCFG,
SCFPOS,
DTLCFG,
DCFPOS,
TYPCOD,
PRTFAM,
LODLVL,
ORGFLG,
REVFLG,
LOTFLG,
UNTCST,
REOQTY,
REOPNT,
PKGTYP,
STKUOM,
UNTPAK,
PAKUOM,
UNTCAS,
CASUOM,
UNTPAL,
PALUOM,
UNTLEN,
PAKLEN,
GRSWGT,
NETWGT,
FTPCOD,
ABCCOD,
FIFWIN,
VELZON,
RCVSTS,
RCVUOM,
RCVFLG,
PRDFLG,
LSTRCV,
LSTSHP,
LSTVAR,
NUMCNT,
CNTDTE,
LTLCLS,
MODDTE,
MOD_USR_ID,
UC_PRDCOD,
VC_ALCPCT,
VC_DISFLG,
VC_ITMCLS,
VC_ITMGRP,
VC_ITMTYP,
VC_MFGCEN,
VC_NCVFLG,
VC_NUMPPL,
VC_PRDCOD,
VC_QCSAMP,
VC_STDPCS,
WIP_PRTFLG,
VC_GRSWGT_EACH
)
select
rtrim (PRTNUM),
'----',
rtrim (UPCCOD),
rtrim (NDCCOD),
rtrim (COMCOD),
rtrim (SUBCFG),
SCFPOS,
rtrim (DTLCFG),
DCFPOS,
rtrim (TYPCOD),
rtrim (PRTFAM),
rtrim (LODLVL),
decode (ORGFLG, 'Y', 1, 0),
decode (REVFLG, 'Y', 1, 0),
1, --LOTFLG,
UNTCST,
REOQTY,
REOPNT,
rtrim (PKGTYP),
nvl(rtrim (STKUOM),'EA'),
UNTPAK,
nvl(rtrim (PAKUOM),'--'),
UNTCAS,
nvl(rtrim (CASUOM),'CS'),
UNTPAL,
nvl(rtrim (PALUOM), 'PA'),
UNTLEN,
PAKLEN,
NETWGT, -- GRSWGT field 4.0 has grs wgt of each in this field
NETWGT,
rtrim (FTPCOD),
rtrim (ABCCOD),
FIFWIN,
rtrim (VELZON),
rtrim (RCVSTS),
nvl(rtrim (RCVUOM),'EA'),
decode (RCVFLG, 'Y', 1, 0),
decode (PRDFLG, 'Y', 1, 0),
rtrim (LSTRCV),
rtrim (LSTSHP),
LSTVAR,
NUMCNT,
rtrim (CNTDTE),
nvl(rtrim (LTLCLS),'1'),
MODDTE,
rtrim (MODEMP),
rtrim (USR_PRDCOD),
USR_ALCPCT,
decode (USR_DISFLG, 'Y', 1, 0),
rtrim (USR_ITMCLS),
rtrim (USR_ITMGRP),
rtrim (USR_ITMTYP),
rtrim (USR_MFGCEN),
0,   -- VC_NCVFLG
USR_NUMPPL,
rtrim (USR_PRDCOD),
rtrim (USR_QCSAMP),
USR_STDPCS,
0, -- WIP_PRTFLG
substr(to_char(grswgt), 1, 8)		-- should this be a number??
from prtmst_40
/
