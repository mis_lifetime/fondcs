## This Scripts is to copy all the TXT files that are in the Hostout directory to a New location CW 12-30-03


cd /opt/mchugh/fon/les/files/hostout/fonrecon 

cp -p  *.txt /opt/mchugh/fon/les/files/hostout/txtfiles

echo " Done copying all files ending with txt " > /opt/mchugh/fon/les/files/hostout/txtfiles/txt_files.out

ll -ltr *txt > /opt/mchugh/fon/les/files/hostout/txtfiles/daily_txt_files.out

rm *.txt

echo " Done removing all files ending in txt " > /opt/mchugh/fon/les/files/hostout/txtfiles/clean_txt.out

exit 0



