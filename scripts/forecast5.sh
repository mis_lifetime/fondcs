#!/usr/bin/ksh
#
# This script will run the Forecasting Report at 6:30AM using cron.
#

# . /opt/mchugh/prod/les/oraclereports

. /opt/mchugh/prod/les/.profile
cd ${LESDIR}/oraclereports



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the daily reports

sql << //
set trimspool on
set pagesize 50000
set linesize 300
set verify off
column type new_value 1 noprint
select 'TOTALFORECAST' type from dual;
spool /opt/mchugh/prod/les/oraclereports/totalforecast5.out
@User-TotalForecastingReport.sql
spool off
exit
//
exit 0
