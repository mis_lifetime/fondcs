select c.prtnum, substr(b.lngdsc,1,25) lngdsc, c.actcod, c.reacod, decode(c.actcod,'ADJ',c.trnqty,c.trnqty*-1) adjqty, c.frstol, 
c.tostol, c.usr_id, c.dtlnum, 
    c.trndte, sum(decode(c.actcod,'ADJ',c.trnqty,c.trnqty*-1) * a.untcst) adjamt, a.untcst
    from  prtmst a,  dscmst b, dlytrn c
    where a.prtnum=c.prtnum
    and a.prt_client_id = c.prt_client_id
    and c.prtnum != 'LABOR'
    and c.reacod is not null
    and c.reacod = b.colval
    and b.colnam ='reacod'
    and c.dtlnum = 'D00015739057'
     /* and c.reacod = 'AUTHDISP' */
   and (c.actcod = 'ADJ' or (c.actcod = 'GENMOV' and c.tostol = 'PERM-CNT-LOC') or (c.actcod = 'PCEPCK' and c.reacod = 'CYCLE'))
   and trunc(trndte) = '30-jan-06'
   group
   by c.trndte, substr(b.lngdsc,1,25), c.actcod, c.reacod, c.prtnum, c.trnqty, c.frstol, c.tostol, c.usr_id, c.dtlnum, a.untcst
   having sum(c.trnqty) <> 0
  order by 10 
/
