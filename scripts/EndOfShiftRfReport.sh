#!/usr/bin/ksh
#
# This script will run for ReneeB 5:15pm using cron.
#


. /opt/mchugh/fon/les/.profile
cd ${LESDIR}/scripts



#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#

#  This is to allow for a prompt to run the daily reports




sql << //
set trimspool on
set pagesize 50 
set linesize 100  
spool /opt/mchugh/fon/les/oraclereports/EndOfShiftRfReport.out
@User-EndOfShiftRfReport.sql
spool off
exit
//
exit 0
