#include <../../../include/varddl.h>
#include <../../../include/varcolwid.h>
#include <../../../include/vartbldef.h>
#include <sqlDataTypes.h>
#use $DCSDIR/include
#include <dcscolwid.h>

create table var_wavpar
(
        vc_wave_lane           INTEGER_TY
                            PRIMARY_KEY_FIELD(var_wavpar_wave_lane_pk),
        vc_max_sid_per_wave    INTEGER_TY 
                            NOT_NULL_CONSTRAINT(var_wavpar_max_sid_nl),
        vc_pallets_per_sid     INTEGER_TY null,
        vc_carcod              STRING_TY(CARCOD_LEN) null,
        vc_cstnum              STRING_TY(CSTNUM_LEN) null,
        vc_max_cases_per_wave  INTEGER_TY null,
        vc_fluid_load_flg      FLAG_TY 
                    FLAG_CONSTRAINT(var_wavpar_fluid_load_flg_ck, vc_fluid_load_flg),
        mod_usr_id          STRING_TY(USR_ID_LEN) null,
        moddte              DATE_TY  null
                             
)        
TABLESPACE_INFO (VAR_WAVPAR_TBL_TBLSPC, VAR_WAVPAR_TBL_STORAGE)
RUN_DDL

#include <var_wavpar_pk.idx>
