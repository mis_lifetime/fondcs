#####################################################################
# This script will run three times daily and spool the information  #
# to an outfile entitled Sql Activity.  This information will then  #
# be stored in the oraclereports directory.                         #
# This script will keep track of sql statements/updates             #
#####################################################################

#!/usr/bin/ksh

. /opt/mchugh/fon/les/.profile
cd ${LESDIR}/oraclereports

#
#  This is the directory location of the sql reports.  Typing sql will enable the sql prompt.  
#
#  This is to allow for a prompt to run the report

datevar=`date  +%m%d%Y%T`

sql << //
set trimspool on
set linesize 120
set pagesize 1000


spool SQLNonProdActivity$datevar.txt

@/opt/mchugh/fon/les/scripts/SQLNonProdActivity.sql

spool off
exit
//
exit 0


