#!/usr/local/bin/perl
################################################################################
#
# $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/scripts/pfstart.pl,v $
# $Revision: 1.2 $
# $Author: devsh $
#
# Description: This script reserves the next project fix number for use and sets
#              up the project fix starter pack for the developer.
#
# $McHugh_Copyright-Start$
#
#   Copyright (c) 2000
#   McHugh Software International
#   Waukesha, Wisconsin
#
# This software is furnished under a corporate license for use on a
# single computer system and can be copied (with inclusion of the
# above copyright) only for use on such a system.
#
# The information in this document is subject to change without notice
# and should not be construed as a commitment by McHugh Software
# International.
#
# McHugh Software International assumes no responsibility for the use of
# the software described in this document on equipment which has not been
# supplied or approved by McHugh Software International.
#
# $McHugh_Copyright-End$
#
################################################################################

require 5.005;

use strict;
use English;
use Getopt::Std;
use IO::Handle;

my $pftopdir = $ENV{LESDIR}."/prjfixes";

my ($usage, $verbose);
my ($prjfix, $date, $delCommand, $rdCommand, $cpCommand);
my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime;
my ($status, $filename, $pfnum, $maxnum, $prjfix);
my ($oridir) = system ("pwd");

# Handle command line arguments.
$usage = 0;
getopts("hv") || $usage++;

if ($usage || $Getopt::Std::opt_h)
{
  printf STDERR "Usage: perl pfstart.pl [ -hv ]\n";
  printf STDERR " -h          - Help (Print this message) \n";
  printf STDERR " -v          - Verbose \n";
  exit 1;
}

$verbose = ${Getopt::Std::opt_v};

# Set up o/s specific commands.
if ($OSNAME =~ /win32/i)
{
    $delCommand = "del /q /s";
    $rdCommand  = "rd";
    $cpCommand  = "copy";
}
else
{
    $delCommand = "rm -rf";
    $rdCommand  = "rm -rf";
    $cpCommand  = "cp";
}

# Set the date for this project fix.
$date = sprintf("%02d/%02d/%04d", $mon+1, $mday, $year+1900);

# Let the user know where we are.
print "\n";
print "Reserving the next available project fix number...\n";

# Create the project fix directory on the local development server.

# Set autoflushing of stdout and stderr.
STDOUT->autoflush(1);
STDERR->autoflush(1);

# Initialize the max. project fix number.
$maxnum = 0;

# Open the project fix directory tree.
$status = opendir(DIR, "$pftopdir");
if ($status == 0)
{
    print STDERR "ERROR: Could not open the project fix directory tree\n";
    print STDERR "       $!\n";
    exit 1;
}

# Cycle through each project fix.
while (defined ($filename = readdir(DIR)))
{
    # We only care about project fix directories.
    if ($filename !~ /^PF\d*$/)
    {
	next;
    }

    # Parse out the project fix number.
    $pfnum = $filename;
    $pfnum =~ s/PF//g;

    # Is this the latest project fix number so far?
    if ($pfnum > $maxnum)
    {
	$maxnum = $pfnum;
    }
}

# Close the project fix directory tree.
closedir(DIR);

# Build the new project fix.
$prjfix = sprintf("PF%06d", ($maxnum + 1));

# Create the project fix directory.
if (! mkdir("$pftopdir/$prjfix", 0775))
{
    print STDERR "ERROR: Could not create the $prjfix directory\n";
    print STDERR "       $!\n";
    exit 1;
}

# Write a message to the user.
print "PRJFIX: $prjfix\n" if ($verbose);

# Move to project fix directory root

chdir ($pftopdir) || die "ERROR: Could not change to project fix root.\n";

# Let the user know where we are.
print "Getting the project fix starter pack...\n";

# Change into the project fix directory.
if (! chdir("$prjfix"))
{
    print STDERR "ERROR: Could not change to the project fix directory.\n";
    print STDERR "       $!\n";
    exit 1;
}

# Export the project fix module from the starter directory.

mkdir ("temp", 0775) || die "ERROR: Could not create project fix starter working directory.\n";

printf("$cpCommand $pftopdir/starter/*.* $pftopdir/$prjfix/temp\n") if ($verbose);
system("$cpCommand $pftopdir/starter/*.* $pftopdir/$prjfix/temp");
if ($? != 0)
{
    print STDERR "ERROR: Could not export the project fix starter pack.\n";
    print STDERR "       $!\n";
    exit 1;
}

# Let the user know where we are.
print "Setting up the project fix starter pack...\n";

# Modify the release notes in place.
open(INFILE,  "<temp/README.txt.in");
open(OUTFILE, ">temp/README.txt");

while (<INFILE>)
{
    $_ =~ s/<Date>/$date/g;
    $_ =~ s/<PrjFix>/$prjfix/g;
    print OUTFILE; 
}

close(INFILE);
close(OUTFILE);

# Modify the project fix script in place.
open(INFILE,  "<temp/PFxxxxxx.in");
open(OUTFILE, ">temp/PFxxxxxx");

while (<INFILE>)
{
    $_ =~ s/<PrjFix>/$prjfix/g;
    print OUTFILE; 
}

close(INFILE);
close(OUTFILE);

# Move the project fix starter pack into the project fix directory.
if (! rename("temp/prjfix.pl", "prjfix.pl"))
{
    print STDERR "ERROR: Could not move the project fix utility.\n";
    print STDERR "       $!\n";
    exit 1;
}

if (! rename("temp/README.txt", "README.txt"))
{
    print STDERR "ERROR: Could not move the starter readme file.\n";
    print STDERR "       $!\n";
    exit 1;
}

if (! rename("temp/PFxxxxxx", "$prjfix"))
{
    print STDERR "ERROR: Could not move the starter project fix script.\n";
    print STDERR "       $!\n";
    exit 1;
}

# Remove the remainder of the project fix module.
system("$delCommand temp");
if ($? != 0)
{
    print STDERR "ERROR: Could not remove temporary files.\n";
    print STDERR "       $!\n";
    exit 1;
}

system("$rdCommand temp");
if ($? != 0)
{
    print STDERR "ERROR: Could not remove temporary directory.\n";
    print STDERR "       $!\n";
    exit 1;
}

# Let the user know we're done.
print "\nA starter pack for project fix $prjfix has been created in the project fixes directory.\n\n";

chdir ($oridir);
