#
#
#SELECT=select 'ITEM # '||'@tck_prtnum' prtnum, '$'||ltrim(to_char('@tck_price',9999.99)) price from dual
#
#SELECT=select decode((select upccod lblupc from prtmst where prtnum='@tck_prtnum'),null,' ',(select upccod lblupc from prtmst where prtnum='@tck_prtnum')) lblupc from dual
#
#SELECT=select decode((select substr(lngdsc,1,30) lbldesc from prtdsc where colval='@tck_prtnum'||'|----'),null,' ',(select substr(lngdsc,1,30) lbldesc from prtdsc where colval='@tck_prtnum'||'|----')) lbldesc from dual
#
#DATA=@prtnum~@price~@lblupc~@lbldesc~
^XA^DFstdtck^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO10,20^ADN,36,10^FN1^FS;
^BY2,,50^FO20,60^BUN,50,Y,N,Y^FN3^FS;
^FO255,70^ADN,36,10^FN2^FS;
^FO15,155^ADN,36,10^FN4^FS;
^FO440,20^ADN,36,10^FN1^FS;
^BY2,,50^FO450,60^BUN,50,Y,N,Y^FN3^FS;
^FO685,70^ADN,36,10^FN2^FS;
^FO445,155^ADN,36,10^FN4^FS;
^PQ1,0,0,N^XZ;
