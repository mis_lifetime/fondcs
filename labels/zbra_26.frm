#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#
#SELECT=select ltrim(to_char(vc_retprc,'$999.99')) vc_retprc from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#
#DATA=@vc_retprc~
#
#
#
^XA^DF26^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,10;
^FO30,100^AB30,30^FN1^FS;
^FO300,100^AB30,30^FN1^FS;
^FO570,100^AB30,30^FN1^FS;
^XZ;
^PQ1,0,0,N^XZ;
