#
#SELECT=select substr(cstprt,1,3)||'-'||substr(cstprt,4,4) test1, substr(cstprt,8,4) test2, vc_lblfield_string_1 test3, vc_lblfield_string_2 test4 from ord_line where client_id='----' and prtnum='@tck_prtnum' and ordnum='@ordnum'
#
#DATA=@test1~@test2~@test3~@test4~
^XA^DFjcptck^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO85,15^ADN,40,15^FN1^FS;
^FO120,65^ADN,40,15^FN2^FS;
^FO50,115^ADN,36,10^FDO/S^FS;
^FO200,115^ADN,36,10^FN4^FS;
^FO120,165^ADN,36,10^FN3^FS;
^FO515,15^ADN,40,15^FN1^FS;
^FO550,65^ADN,40,15^FN2^FS;
^FO480,115^ADN,36,10^FDO/S^FS;
^FO630,115^ADN,36,10^FN4^FS;
^FO550,165^ADN,36,10^FN3^FS;
^PQ1,0,0,N^XZ;
