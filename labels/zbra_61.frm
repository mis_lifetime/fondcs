#SELECT=select ltrim(to_char(vc_retprc,'$999.99')) vc_retprc, ltrim(vc_lblfd3) vc_lblfd3, ltrim(vc_lblfd4) vc_lblfd4, substr(cstprt, 1, 8) cstprt from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#DATA=@vc_retprc~@cstprt~@vc_lblfd3~@vc_lblfd4~
#
^XA^DF33^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,10;
^FO5,30^AB20,20^FDDept#78^FS;
^FO30,60^AB30,30^FN1^FS;
^FO5,110^AB20,20^FDSKU^FS;
^FO70,110^AB20,20^FN2^FS;
^FO5,140^AB20,20^FN3^FS;
^FO5,170^AB20,20^FN4^FS;

^FO270,30^AB20,20^FDDept#78^FS;
^FO300,60^AB30,30^FN1^FS;
^FO270,110^AB20,20^FDSKU^FS;
^FO335,110^AB20,20^FN2^FS;
^FO270,140^AB20,20^FN3^FS;
^FO270,170^AB20,20^FN4^FS;

^FO540,30^AB20,20^FDDept#78^FS;
^FO570,60^AB30,30^FN1^FS;
^FO540,110^AB20,20^FDSKU^FS;
^FO605,110^AB20,20^FN2^FS;
^FO540,140^AB20,20^FN3^FS;
^FO540,170^AB20,20^FN4^FS;
^XZ;
^PQ1,0,0,N^XZ;

