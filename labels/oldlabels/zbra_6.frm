#SELECT=select ltrim(to_char(vc_retprc,'$999.99')) vc_retprc, ltrim(vc_lblfd4) vc_lblfd4, cstprt from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#SELECT=select min(deptno) lbldept from ord_line, shipment_line, ord where btcust = '@cstnum' and ord.ordnum = shipment_line.ordnum and shipment_line.ordlin = ord_line.ordlin and ord_line.ordnum = ord.ordnum and ord_line.ordnum = shipment_line.ordnum and ord_line.prtnum = '@prtnum'
#
#DATA=@vc_lblfd4~@cstprt~@vc_retprc~@lbldept~
#
^XA^DF6^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,10;
^FO50,60^AB20,20^FDKOHL'S^FS;
^FO10,90^AB20,20^FN4^FS;
^FO90,90^AB20,20^FN1^FS;
^FO150,90^AB20,20^FN2^FS;
^FO20,130^AB30,30^FN3^FS;

^FO330,60^AB20,20^FDKOHL'S^FS;
^FO280,90^AB20,20^FN4^FS;
^FO360,90^AB20,20^FN1^FS;
^FO420,90^AB20,20^FN2^FS;
^FO300,130^AB30,30^FN3^FS;

^FO590,60^AB20,20^FDKOHL'S^FS;
^FO550,90^AB20,20^FN4^FS;
^FO630,90^AB20,20^FN1^FS;
^FO690,90^AB20,20^FN2^FS;
^FO560,130^AB30,30^FN3^FS;
^XZ;
^PQ1,0,0,N^XZ;
