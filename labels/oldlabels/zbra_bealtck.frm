#
#SELECT=select ol.prtnum, o.cponum, ol.deptno, ol.cstprt||'-'||ol.vc_lblfield_string_2 bealsc, 'Compare at $'||ol.vc_lblfield_string_1 bealsp, '$'||ol.vc_cust_retail_price price, '300'||ol.cstprt upccod from ord_line ol, ord o, prtmst p where ol.ordnum='@ordnum' and ol.prtnum='@tck_prtnum' and ol.client_id='----' and ol.ordnum=o.ordnum and ol.prtnum=p.prtnum
#
#DATA=@prtnum~@cponum~@deptno~@bealsc~@bealsp~@price~@upccod~
^XA^DFbealtck^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH0,0;
^A0N,25,26^FO19,18^FN3^FS;
^A0N,25,26^FO81,18^FN2^FS;
^A0N,25,26^FO200,18^FN4^FS;
^A0N,25,26^FO116,130^FN5^FS;
^A0N,45,46^FO265,160^FN6^FS;
^BY2^FO113,48^BUN,56,Y,N,Y^FN7^FS;
^A0N,25,26^FO19,175^FN1^FS;
^A0N,25,26^FO443,18^FN3^FS;
^A0N,25,26^FO497,18^FN2^FS;
^A0N,25,26^FO619,18^FN4^FS;
^A0N,25,26^FO522,130^FN5^FS;
^A0N,45,46^FO676,160^FN6^FS;
^BY2^FO519,48^BUN,56,Y,N,Y^FN7^FS;
^A0N,25,26^FO443,175^FN1^FS;
^PQ1,0,0,N^XZ;
