#
#
#SELECT=select '$'||ltrim(to_char('@tck_price',9999.99)) price from dual
#
#SELECT=select 'Dept. '||deptno deptno, 'Class '||vc_lblfield_string_1 class from ord_line where client_id='----' and prtnum='@tck_prtnum' and ordnum='@ordnum'
#
#DATA=@dummy~@price~@dummy~@class~@deptno~
^XA^DFstagetck^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO70,15^ADN,40,20^FN5^FS;
^FO50,70^ADN,40,20^FN4^FS;
^FO90,130^ADN,40,20^FN2^FS;
^FO500,15^ADN,40,20^FN5^FS;
^FO480,70^ADN,40,20^FN4^FS;
^FO520,130^ADN,40,20^FN2^FS;
^PQ1,0,0,N^XZ;
