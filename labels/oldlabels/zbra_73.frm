#SELECT=select ' ' part, ' ' part2, ' ' name, '$'||a.vc_retprc price, b.upccod, substr(c.lngdsc,1,20) lngdsc, a.prtnum part3 from cstprq a, prtmst b, prtdsc c where a.prtnum='@prtnum' and a.cstnum='@cstnum' and a.prtnum=b.prtnum and b.prtnum||'|----'=c.colval and c.colnam='prtnum|prt_client_id' and c.locale_id='US_ENGLISH'
#
#DATA=@part~@part2~@name~@price~@upccod~@lngdsc~@part3~
#
#
^XA^DF73^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH0,10;
^FO25,175^ADB,30,10^FDITEM #^FS;
^FO25,85^ADB,30,10^FN7^FS;
^FO200,25^ADB,24,10^FN6^FS;
^FO140,150^ADB,54,20^FN4^FS;
^BY2,,60^FO65,55^BUB,51,Y,N,Y^FN5^FS;
^FO300,175^ADB,30,10^FDITEM #^FS;
^FO300,85^ADB,30,10^FN7^FS;
^FO475,25^ADB,24,10^FN6^FS;
^FO415,150^ADB,54,20^FN4^FS;
^BY2,,60^FO340,55^BUB,51,Y,N,Y^FN5^FS;
^FO575,175^ADB,30,10^FDITEM #^FS;
^FO575,85^ADB,30,10^FN7^FS;
^FO750,25^ADB,24,10^FN6^FS;
^FO690,150^ADB,54,20^FN4^FS;
^BY2,,60^FO615,55^BUB,51,Y,N,Y^FN5^FS;
^XZ;
^PQ1,0,0,N^XZ;
