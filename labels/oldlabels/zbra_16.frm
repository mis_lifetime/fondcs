#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#
#SELECT=select cstprt from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#DATA=@cstprt~
#
^XA^DF16^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,10;
^FO60,80^AB20,20^FDSKU #^FS;
^FO40,110^AB20,20^FN1^FS;

^FO330,80^AB20,20^FDSKU #^FS;
^FO310,110^AB20,20^FN1^FS;

^FO610,80^AB20,20^FDSKU #^FS;
^FO590,110^AB20,20^FN1^FS;
^XZ;
^PQ1,0,0,N^XZ;
