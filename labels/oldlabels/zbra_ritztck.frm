#
#
#SELECT=select 'Vendor SKU: '||'@tck_prtnum' prtnum, '$'||ltrim(to_char('@tck_price',9999.99)) price from dual
#
#SELECT=select 'CPN: '||cstprt cstprt from ord_line where client_id='----' and ordnum='@ordnum' and prtnum='@tck_prtnum'
#
#SELECT=select decode((select upccod lblupc from prtmst where prtnum='@tck_prtnum'),null,' ',(select upccod lblupc from prtmst where prtnum='@tck_prtnum')) lblupc from dual
#
#SELECT=select decode((select substr(lngdsc,1,30) lbldesc from prtdsc where colval='@tck_prtnum'||'|----'),null,' ',(select substr(lngdsc,1,30) lbldesc from prtdsc where colval='@tck_prtnum'||'|----')) lbldesc from dual
#
#DATA=@prtnum~@price~@lblupc~@lbldesc~@cstprt~
^XA^DFritztck^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO40,15^ADN,36,10^FN5^FS;
^FO40,55^ADN,36,10^FN1^FS;
^BY2,,50^FO15,95^BUN,50,Y,N,Y^FN3^FS;
^FO250,105^ADN,36,10^FN2^FS;
^FO10,170^ADN,36,10^FN4^FS;
^FO470,15^ADN,36,10^FN5^FS;
^FO470,55^ADN,36,10^FN1^FS;
^BY2,,50^FO445,95^BUN,50,Y,N,Y^FN3^FS;
^FO670,105^ADN,36,10^FN2^FS;
^FO440,170^ADN,36,10^FN4^FS;
^PQ1,0,0,N^XZ;
