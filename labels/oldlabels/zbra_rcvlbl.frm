#
#SELECT=select '@lodnum' lodnum, 'Case/Pallet: '||'@caspal' caspal, 'Item#: '||'@prtnum' lblprt, 'DEST AISLE: '||'@dest_aisle' aisle, 'DEST LOC: '||'@dstloc' dstloc, 'Orig Qty: '||'@untqty' qty, 'Truck#: '||'@trknum' truck, 'Trk Ref: '||'@trkref' trkref  from dual
#
#DATA=@lodnum~@caspal~@lblprt~@aisle~@qty~@truck~@dstloc~@trkref~
^XA^DFrcvlbl^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^BY4,2.3,255^FO100,40^BCN,,N,N,N,N^FN1^FS;
^FO15,320^ADN,74,25^FN1^FS;
^FO15,420^ADN,74,25^FN3^FS;
^FO15,520^ADN,74,25^FN2^FS;
^FO15,620^ADN,74,25^FN6^FS;
^FO15,720^ADN,74,25^FN8^FS;
^FO15,820^ADN,74,25^FN4^FS;
^FO15,920^ADN,74,25^FN7^FS;
^FO15,1020^ADN,74,25^FN5^FS;
PQ1,0,0,N^XZ;
