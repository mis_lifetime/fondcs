#
#SELECT=select 'NTO SKU # '||cstprt cstprt, 'Vendor Style # '||prtnum style from ord_line where client_id='----' and prtnum='@tck_prtnum' and ordnum='@ordnum'
#
#SELECT=select substr(lngdsc,1,30) lngdsc from prtdsc where colnam='prtnum|prt_client_id' and colval='@tck_prtnum'||'|----' and locale_id='US_ENGLISH'
#
#DATA=@cstprt~@lngdsc~@style~
^XA^DFnormtck^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,15^ADN,36,10^FDVendor Description^FS;
^FO15,55^ADN,36,10^FN2^FS;
^FO15,115^ADN,36,10^FN1^FS;
^FO15,155^ADN,36,10^FN3^FS;
^FO445,15^ADN,36,10^FDVendor Description^FS;
^FO445,55^ADN,36,10^FN2^FS;
^FO445,115^ADN,36,10^FN1^FS;
^FO445,155^ADN,36,10^FN3^FS;
^PQ1,0,0,N^XZ;
