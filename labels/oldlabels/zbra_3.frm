#SELECT=select ltrim(to_char(vc_retprc,'$999.99')) vc_retprc from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#DATA=@vc_retprc~
#
^XA^DF3^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH0,10;
^FO30,60^AB20,20^FD113^FS;
^FO30,120^AB30,30^FN1^FS;

^FO310,60^AB20,20^FD113^FS;
^FO310,120^AB30,30^FN1^FS;

^FO590,60^AB20,20^FD113^FS;
^FO590,120^AB30,30^FN1^FS;
^XZ;
^PQ1,0,0,N^XZ;
