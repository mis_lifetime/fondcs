#
#SELECT=select '@lodnum' lblload, 'Device: '||'@devcod' devcod, 'Batch: '||'@lblbat' lblbat, 'Ticketing: '||'@tck_flg' ticketing from dual
 #
#DATA=@lblload~@devcod~@lblbat~@ticketing~
^XA^DFloadlbld^FS^SZ2^MMT^MTD~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^BY2,2.3,255^FO60,300^BCN,,N,N,N,N^FN1^FS;
^FO15,600^ADN,74,25^FN1^FS;
^FO15,700^ADN,74,25^FN2^FS;
^FO15,800^ADN,74,25^FN3^FS;
^FO15,900^ADN,74,25^FN4^FS;
^PQ1,0,0,N^XZ;
