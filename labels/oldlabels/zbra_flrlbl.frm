#
#SELECT=select '@lodnum' lblload, 'Device: '||'@devcod' devcod, 'Batch: '||'@lblbat' lblbat, 'Bill To: '||'@btcust' ticketing, 'Loc: '||'@srcloc' srcloc, 'Part#: '||'@prtnum' prtnum, '@lngdsc' lngdsc, '# of Cases: '||'@num_cases' cases from dual
#
#DATA=@lblload~@devcod~@lblbat~@ticketing~@srcloc~@prtnum~@lngdsc~@cases~
^XA^DFflrlbl^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^BY4,2.3,255^FO50,300^BCN,,Y,N,N,N^FN1^FS;
^FO15,700^ADN,74,25^FN2^FS;
^FO15,800^ADN,74,25^FN3^FS;
^FO15,900^ADN,74,25^FN4^FS;
^FO15,1000^ADN,74,23^FN5^FS;
^FO15,1100^ADN,74,25^FN6^FS;
^FO15,1200^ADN,74,25^FN7^FS;
^FO15,1300^ADN,74,25^FN8^FS;
^PQ1,0,0,N^XZ;
