#Error label with ordnum, ship_id, shpseq, subsid and wrkref for incorrect format associated with an Order 
#
# Get the info.
#
#SELECT=select ordnum, wrkref, ship_id, '----' shpseq, '----' subsid from pckwrk where wrkref = '@wrkref' 
#
#SELECT=select '******** ERROR LABEL *********' text0 from dual 
#
#SELECT=select 'ORDNUM     '||'@ordnum' text1, 'WRKREF     '||'@wrkref' text2, 'SHIP ID    '||'@ship_id' text3, 'SHIP SEQ   '||'@shpseq' text4, 'SUBS ID    '||'@subsid' text5 from dual
#
#DATA=@text0~@ordnum~@wrkref~@ship_id~@shpseq~@subsid~
^XA^DFerrlbl^FS;
^LH10,0;
^FO180,150^AD,80,15^FN1^FS;
^FO25,285^AD,80,15^FDORDER :^FS;
^FO220,285^AD,80,15^FN2^FS;
^FO25,360^AD,80,15^FDWRKREF :^FS;
^FO220,360^AD,80,15^FN3^FS;
^FO25,435^AD,80,15^FDSHIP ID :^FS;
^FO240,435^AD,80,15^FN4^FS;
^FO25,510^AD,80,20^FDSHPSEQ :^FS;
^FO220,510^AD,80,20^FN5^FS;
^FO25,585^AD,80,15^FDSUBSID :^FS;
^FO220,585^AD,80,15^FN6^FS;
^PQ1,0,0,N^XZ;
