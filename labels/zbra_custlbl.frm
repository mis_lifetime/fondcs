#
#SELECT=select to_char(sysdate,'DD-MON-YYYY HH12MISS PM') prtdte, ship_id, wrktyp, subnum, ordnum, subucc, srcloc, cmbcod, vc_slotno, 'Case '||cur_cas||' of '||tot_cas_cnt xofx, prtnum, pckqty from pckwrk where wrkref='@wrkref'
#
#SELECT=select cponum from ord where ordnum='@ordnum'
#
#SELECT=select stoloc dstloc from pckmov where cmbcod='@cmbcod' and seqnum=(select max(seqnum) from pckmov where cmbcod='@cmbcod')
#
#SELECT=select '# of Items: '||decode('@wrktyp','K',(select count(distinct prtnum) from pckwrk where ctnnum='@subnum'),1) itmcnt from dual
#
#SELECT=select 'Total Qty: '||decode('@wrktyp','K',(select sum(pckqty) from pckwrk where ctnnum='@subnum'),(select pckqty from pckwrk where subucc='@subucc')) totpck from dual
#
#SELECT=select  substr('@subucc', 1, 21) lblsnbar, '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5, substr('@prtnum',1,30) lblprt from dual
#
#SELECT=select decode((select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH'),null,' ',(select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH')) lbldescr from dual
#
#SELECT=select 'RESORT SEQ: '||clst_seq prosdest1 from pckwrk where wrkref='@wrkref'
#
#SELECT=select 'SORT LOC: '||decode((select pckmov.stoloc prosdest FROM pckwrk, pckmov, poldat where pckwrk.wrkref = '@wrkref' and pckwrk.cmbcod = pckmov.cmbcod and pckmov.arecod = poldat.rtstr1 and poldat.polcod = 'USR' and poldat.polvar = 'CASE-PICK-PROCESSING' and poldat.polval = 'AREAS' and poldat.rtnum1 = 1),null, ' ', (select pckmov.stoloc prosdest FROM pckwrk, pckmov, poldat where pckwrk.wrkref = '@wrkref' and pckwrk.cmbcod = pckmov.cmbcod and pckmov.arecod = poldat.rtstr1 and poldat.polcod = 'USR' and poldat.polvar = 'CASE-PICK-PROCESSING' and poldat.polval = 'AREAS' and poldat.rtnum1 = 1)) prosdest2 from dual
#
#SELECT=select decode(clst_seq,null,'@prosdest2','@prosdest1') prosdest from pckwrk where wrkref='@wrkref'
#
#SELECT=select distinct 'Ent Date: '||to_char(entdte, 'MMDDYY') entdte, 'Reg Ship: '||to_char(early_shpdte, 'MMDDYY') regdte from ord_line where client_id='----' and ordnum='@ordnum'
#
#SELECT=select decode((select prtfam from prtmst where prtnum='@prtnum'),null,' ',(select prtfam from prtmst where prtnum='@prtnum')) prtfam from dual
#
#DATA=@prtdte~@cponum~@itmcnt~@totpck~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblsnbar~@dummy~@srcloc~@dstloc~@dummy~@xofx~@lblprt~@ship_id~@lbldescr~@prosdest~@entdte~@regdte~@prtfam~@ordnum~@pckqty~
#
^XA^DFcustlbl^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PR6^PON^PMN^CI0^LRN;
^LH10,0;
^FO20,50^ADN,54,15^FN1^FS;
^FO20,110^ADN,54,15^FDPO#:^FS;
^FO380,110^ADN,54,15^FN15^FS;
^FO20,170^ADN,130,35^FN2^FS;
^FO20,300^ADN,54,15^FN3^FS;
^FO20,370^ADN,54,15^FN4^FS;
^FO20,440^ADN,54,15^FDPicked By:^FS;
^FO15,640^GB785,0,2^FS;
^FO20,670^ADN,54,15^FDPacked By:^FS;
^FO15,870^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN5^FS;
^FO276,920^ADN,36,10^FN6^FS;
^FO331,920^ADN,36,10^FN7^FS;
^FO462,920^ADN,36,10^FN8^FS;
^FO629,920^ADN,36,10^FN9^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN10^FS;
^FO15,1244^ADN,50,15^FDRack Location:^FS;
^FO380,1244^ADN,50,15^FN12^FS;
^FO15,1304^ADN,40,15^FDCPQ:^FS;
^FO120,1304^ADN,40,15^FN24^FS;
^FO175,1304^ADN,40,15^FN16^FS;
^FO15,1344^ADN,40,15^FDDESC:^FS;
^FO150,1344^ADN,40,15^FN18^FS;
^FO15,1384^ADN,70,30^FN19^FS;
^FO15,1470^ADN,40,15^FDShip Stage:^FS;
^FO300,1470^ADN,40,15^FN13^FS;
^FO15,1524^ADN,36,10^FDSID:^FS;
^FO170,1524^ADN,36,10^FN17^FS;
^FO380,1524^ADN,36,10^FDOrd:^FS;
^FO455,1524^ADN,36,10^FN23^FS;
^FO15,1560^ADN,36,10^FN21^FS;
^FO455,1560^ADN,36,10^FN20^FS;
^FO300,1560^ADN,36,10^FN22^FS;
^PQ1,0,0,N^XZ;
