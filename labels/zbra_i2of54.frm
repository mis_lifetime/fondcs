#
#I2of5 LABELS
#
#
#SELECT=select usr_altprtmst.alt_prtnum vc_i2of5, usr_altprtmst.untpak vc_untpak, usr_altprtmst.untcas vc_untcas, substr(prtdsc.lngdsc,1,25) vc_desc, usr_altprtmst.prtnum vc_part from usr_altprtmst, prtdsc, pckwrk where usr_altprtmst.prtnum=pckwrk.prtnum and usr_altprtmst.conv_flg=1 and usr_altprtmst.prtnum||'|----'=prtdsc.colval and prtdsc.colnam='prtnum|prt_client_id' and prtdsc.locale_id='US_ENGLISH' and pckwrk.subucc='@subucc'
#
#
#DATA=@vc_i2of5~@vc_untpak~@vc_untcas~@vc_desc~@vc_part~
#
#
#
^XA^DFi2of54^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH0,0;
^BY5,,195^FO60,50^B2N,,N,N^FN1^FS;
^FO40,30^GB710,2,20^FS;
^FO40,240^GB710,2,20^FS;
^FO235,280^AB30,30^FN1^FS;
^FO10,330^AB18,18^FDITEM#:^FS;
^FO125,330^AB18,18^FN5^FS;
^FO335,330^AB18,18^FN4^FS;
^FO10,370^AB18,18^FDMASTER PACK QTY:^FS;
^FO325,370^AB18,18^FN3^FS;
^FO400,370^AB18,18^FDINNER PACK QTY:^FS;
^FO700,370^AB18,18^FN2^FS;
^XZ;
^PQ1,0,0,N^XZ;
