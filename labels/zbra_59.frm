# $Header: /mnt/dc01/mchugh/prod/cvsroot/prod/les/labels/zbra_59.frm,v 1.2 2002/03/11 16:39:34 devsh Exp $
#
# $Log: zbra_59.frm,v $
# Revision 1.2  2002/03/11 16:39:34  devsh
# mr 000
# Snapshot commit of development code to preserve uncommited changes
#
# Revision 1.1.1.1  2001/09/18 23:05:46  lh51sh
# Initial Import
#
# Revision 1.1  2000/04/26  15:51:06  15:51:06  dcsmgr (DCS Application Mgr)
# Initial revision
# 
#
#SELECT=select substr(ltrim(vc_lblfd2),1,18) vc_lblfd2, ltrim(to_char(vc_retprc,'$999.99')) vc_retprc, substr(ltrim(vc_lblfd3),1,18) vc_lblfd3, substr(ltrim(cstprt),1,12) cstprt from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#
#DATA=@vc_lblfd3~@vc_lblfd2~@vc_retprc~@cstprt~
#
#
^XA^DF59^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH6,10;
^FO195,10^ABR,30,30^FDJCPENNEY^FS;
^FO150,10^A0R,30,30^FN2^FS;
^FO125,10^A0R,30,30^FN1^FS;
^FO115,10^GB0,230,5^FS;
^FO95,80^AAR,18,10^FD0001-CD^FS;
^FO75,80^AAR,18,10^FDSUPP. 15242-1^FS;
^FO20,90^ABR,30,15^FN3^FS;
^FO0,10^AAR,18,10^FDJCPenney^FS;
^FO50,270^AA,18,10^FN4^FS;

^FO470,10^ABR,30,30^FDJCPENNEY^FS;
^FO425,10^A0R,30,30^FN2^FS;
^FO400,10^A0R,30,30^FN1^FS;
^FO390,10^GB0,230,5^FS;
^FO370,80^AAR,18,10^FD0001-CD^FS;
^FO350,80^AAR,18,10^FDSUPP. 15242-1^FS;
^FO310,90^ABR,30,15^FN3^FS;
^FO290,10^AAR,18,10^FDJCPenney^FS;
^FO330,270^AA,18,10^FN4^FS;

^FO750,10^ABR,30,30^FDJCPENNEY^FS;
^FO705,10^A0R,30,30^FN2^FS;
^FO680,10^A0R,30,30^FN1^FS;
^FO670,10^GB0,230,5^FS;
^FO650,80^AAR,18,10^FD0001-CD^FS;
^FO630,80^AAR,18,10^FDSUPP. 15242-1^FS;
^FO580,90^ABR,30,15^FN3^FS;
^FO560,10^AAR,18,10^FDJCPenney^FS;
^FO600,270^AA,18,10^FN4^FS;
^XZ;
^PQ1,0,0,N^XZ;
