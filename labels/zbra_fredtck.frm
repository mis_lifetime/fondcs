#
#
#SELECT=select 'Vendor SKU: '||'@tck_prtnum' prtnum, '$'||ltrim(to_char('@tck_price',9999.99)) price from dual
#
#SELECT=select 'FM SKU: '||cstprt cstprt, vc_lblfield_string_1 class from ord_line where client_id='----' and ordnum='@ordnum' and prtnum='@tck_prtnum'
#
#SELECT=select decode((select upccod lblupc from prtmst where prtnum='@tck_prtnum'),null,' ',(select upccod lblupc from prtmst where prtnum='@tck_prtnum')) lblupc from dual
#
#DATA=@prtnum~@price~@lblupc~@dummy~@cstprt~@class~
^XA^DFfredtck^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO10,15^ADN,36,10^FN5^FS;
^FO240,15^ADN,36,10^FN6^FS;
^FO255,170^ADN,36,10^FN2^FS;
^FO5,170^ADN,36,10^FN1^FS;
^BY2,,50^FO40,55^BUN,80,Y,N,Y^FN3^FS;
^FO10,55^ADN,36,10^FN4^FS;
^FO440,15^ADN,36,10^FN5^FS;
^FO670,15^ADN,36,10^FN6^FS;
^FO685,170^ADN,36,10^FN2^FS;
^FO435,170^ADN,36,10^FN1^FS;
^BY2,,50^FO470,55^BUN,80,Y,N,Y^FN3^FS;
^FO440,55^ADN,36,10^FN4^FS;
^PQ1,0,0,N^XZ;
