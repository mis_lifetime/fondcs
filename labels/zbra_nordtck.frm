#
#
#SELECT=select '@tck_prtnum' prtnum, ltrim(to_char('@tck_price',9999.99)) price from dual
#
#SELECT=select trunc(deptno) deptno, vc_lblfield_string_1 season from ord_line where client_id='----' and prtnum='@tck_prtnum' and ordnum='@ordnum' 
#
#DATA=@prtnum~@price~@season~@dummy~@deptno~
^XA^DFnordtck^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,15^ADN,40,15^FN5^FS;
^FO250,15^ADN,40,15^FN3^FS;
^FO90,65^ADN,40,15^FN1^FS;
^FO90,130^ADN,45,20^FN2^FS;
^FO445,15^ADN,40,15^FN5^FS;
^FO680,15^ADN,40,15^FN3^FS;
^FO520,65^ADN,40,15^FN1^FS;
^FO520,130^ADN,45,20^FN2^FS;
^PQ1,0,0,N^XZ;
