#Standard Part label with Part # and description, upccod and subnum
#
# Get the part info.
#
#SELECT=select prtmst.prtnum, prtdsc.lngdsc, prtmst.upccod, prtmst.lodlvl from prtmst, prtdsc where prtmst.prtnum= '@prtnum' and prtmst.prtnum||'----' = prtdsc.colval and prtdsc.colnam = 'prtnum|prt_client_id' and prtdsc.locale_id = 'US_ENGLISH'
#
#SELECT=select '@subnum' outsub,'S/N: ' sublbl from dual if (@lodlvl != 'L') 
#
#DATA=@prtnum~@lngdsc~@orgcod~@upccod~@outsub~@sublbl~
^XA^DFprtlbl^FS;
^LH30,30;
^FWR;
^BY4,1,150;
^FO210,20^AD,80^FDP/N:^FS;
^FO210,280^AD,80^FN1^FS;
^FO310,20^B3R,,,N^FN1^FS;
^FO530,20^AD,80^FN6^FS;
^FO530,280^AD,80^FN5^FS;
^FO630,20^B3R,,,N^FN5^FS;
^FWN;
^FO40,420^AD,40^FN3^FS;
^FO20,20^B3R,,,N^FN3^FS;
^FWI;
^FT750,1150^AD,60^FN2^FS;
^FO180,930^BUI,,,,^FN4^FS;
^XZ;
