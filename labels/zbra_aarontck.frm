#
#
#SELECT=select 'ITEM#: '||'@tck_prtnum' prtnum, '$'||ltrim(to_char('@tck_price',9999.99)) price from dual
#
#SELECT=select 'SKU#: '||cstprt cstprt from ord_line where client_id='----' and ordnum='@ordnum' and prtnum='@tck_prtnum'
#
#SELECT=select decode((select upccod lblupc from prtmst where prtnum='@tck_prtnum'),null,' ',(select upccod lblupc from prtmst where prtnum='@tck_prtnum')) lblupc from dual
#
#SELECT=select decode((select substr(lngdsc,1,30) lbldesc from prtdsc where colval='@tck_prtnum'||'|----'),null,' ',(select substr(lngdsc,1,30) lbldesc from prtdsc where colval='@tck_prtnum'||'|----')) lbldesc from dual
#
#DATA=@prtnum~@price~@lblupc~@lbldesc~@cstprt~
^XA^DFaarontck^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO65,45^ADN,30,10^FN5^FS;
^FO65,75^ADN,30,10^FN1^FS;
^BY2,,40^FO60,105^BUN,40,Y,N,Y^FN3^FS;
^FO100,15^ADN,30,10^FN2^FS;
^FO10,170^ADN,30,10^FN4^FS;
^FO495,45^ADN,30,10^FN5^FS;
^FO495,75^ADN,30,10^FN1^FS;
^BY2,4,0^FO490,105^BUN,40,Y,N,Y^FN3^FS;
^FO530,15^ADN,30,10^FN2^FS;
^FO440,170^ADN,30,10^FN4^FS;
^PQ1,0,0,N^XZ;
