#SELECT=select ltrim(to_char(vc_retprc,'$999.99')) vc_retprc from cstprq where prtnum = '@prtnum' and cstnum = '@cstnum'
#
#DATA=@vc_retprc~
#
^XA^DF9^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,10;
^FO20,70^AB20,20^FDHOMEPLACE^FS;
^FO20,120^AB30,30^FN1^FS;

^FO300,70^AB20,20^FDHOMEPLACE^FS;
^FO300,120^AB30,30^FN1^FS;

^FO580,70^AB20,20^FDHOMEPLACE^FS;
^FO580,120^AB30,30^FN1^FS;
^XZ;
^PQ1,0,0,N^XZ;
