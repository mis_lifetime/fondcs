#
#
#SELECT=select '@tck_prtnum' prtnum, '$'||ltrim(to_char('@tck_price',9999.99)) price from dual
#
#SELECT=select decode((select upccod lblupc from prtmst where prtnum='@tck_prtnum'),null,' ',(select upccod lblupc from prtmst where prtnum='@tck_prtnum')) lblupc from dual
#
#SELECT=select cstprt cstprt from ord_line where client_id='----' and ordnum='@ordnum' and prtnum='@tck_prtnum'
#
#SELECT=select decode((select substr(lngdsc,1,30) lbldesc from prtdsc where colval='@tck_prtnum'||'|----'),null,' ',(select substr(lngdsc,1,30) lbldesc from prtdsc where colval='@tck_prtnum'||'|----')) lbldesc from dual
#
#DATA=@prtnum~@price~@lblupc~@lbldesc~@cstprt~
^XA^DFshopkotck^FS^SZ2^MMT^MTT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO10,20^ADN,36,10^FN4^FS;
^BY2,,50^FO20,60^BUN,50,Y,N,Y^FN3^FS;
^FO255,60^ADN,36,10^FN2^FS;
^FO255,100^ADN,36,10^FN5^FS;
^FO225,155^ADN,36,10^FDMade In China^FS;
^FO15,155^ADN,36,10^FN1^FS;
^FO440,20^ADN,36,10^FN4^FS;
^BY2,,50^FO450,60^BUN,50,Y,N,Y^FN3^FS;
^FO685,60^ADN,36,10^FN2^FS;
^FO685,100^ADN,36,10^FN5^FS;
^FO445,155^ADN,36,10^FN1^FS;
^FO655,155^ADN,36,10^FDMade In China^FS;
^PQ1,0,0,N^XZ;
