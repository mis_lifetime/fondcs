#
#SELECT=select '@tck_prtnum' prtnum from dual
#
#SELECT=select case when (select count(*) from poldat p, ord o where p.polcod='USR' and p.polvar='LABEL-PRICE' and p.polval='BTCUST' and p.rtstr1=o.btcust and o.stcust like '%'||p.rtstr2 and o.ordnum='@ordnum')=0 then '$'||ltrim(to_char('@tck_price',9999.99)) else null end price from dual
#
#SELECT=select decode((select upccod lblupc from prtmst where prtnum='@tck_prtnum'),null,' ',(select upccod lblupc from prtmst where prtnum='@tck_prtnum')) lblupc from dual
#
#DATA=@prtnum~@price~@lblupc~
^XA^DFhbctck^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO10,20^ADN,36,10^FDStyle/Modele:^FS;
^FO10,60^ADN,36,10^FN1^FS;
^BY2,,50^FO20,100^BUN,50,Y,N,Y^FN3^FS;
^FO255,100^ADN,36,10^FN2^FS;
^FO440,20^ADN,36,10^FDStyle/Modele:^FS;
^FO440,60^ADN,36,10^FN1^FS;
^BY2,,50^FO450,100^BUN,50,Y,N,Y^FN3^FS;
^FO685,100^ADN,36,10^FN2^FS;
^PQ1,0,0,N^XZ;
