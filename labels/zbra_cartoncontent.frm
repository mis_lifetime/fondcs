#
#SELECT=select adrmst.adrnam, adrmst.adrln1, adrmst.adrln2, adrmst.adrln3, adrmst.adrcty||', '||adrmst.adrstc||' '||adrmst.adrpsz ctystzip, 'PO# '||ord.cponum po, 'DEPT: '||ord_line.deptno dept, 'Vendor ID: '||ord.usr_vendor vendor, pckwrk.subucc, 'SSCC-18: '||pckwrk.subucc sscc, 'Carton '||pckwrk.cur_cas||' of '||pckwrk.tot_cas_cnt xofx, pckwrk.subnum from pckwrk, ord_line, ord, adrmst where pckwrk.wrkref='@wrkref' and pckwrk.ordnum=ord_line.ordnum and pckwrk.ordlin=ord_line.ordlin and ord_line.client_id='----' and ord_line.ordnum=ord.ordnum and ord.stcust=adrmst.host_ext_id
#
#SELECT=select decode((select count('x') from pckwrk where subucc='@subucc' and wrktyp='P'),0, ' ',(select rpad(nvl(ol.cstprt,' '),27)||rpad(ol.prtnum,27)||lpad(pw.pckqty,5) from pckwrk pw, ord_line ol where pw.subucc='@subucc' and pw.wrktyp!='K' and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----')) lbl1 from dual
#
#SELECT=select decode((select count('x') from pckwrk where subucc='@subucc' and wrktyp='P'),1, ' ',(select rpad(nvl(cstprt,' '),27)||rpad(prtnum,27)||lpad(pckqty,5) from (select rownum row_number, cstprt, prtnum, sum(pckqty) pckqty from (select ol.cstprt, ol.prtnum, sum(pw.pckqty) pckqty from pckwrk pw, ord_line ol where pw.ctnnum='@subnum' and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' group by ol.cstprt, ol.prtnum order by ol.prtnum) group by rownum, cstprt, prtnum order by prtnum) where row_number=1)) lbl2 from dual
#
#SELECT=select decode((select count('x') from pckwrk where subucc='@subucc' and wrktyp='P'),1, ' ',(select rpad(nvl(cstprt,' '),27)||rpad(prtnum,27)||lpad(pckqty,5) from (select rownum row_number, cstprt, prtnum, sum(pckqty) pckqty from (select ol.cstprt, ol.prtnum, sum(pw.pckqty) pckqty from pckwrk pw, ord_line ol where pw.ctnnum='@subnum' and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' group by ol.cstprt, ol.prtnum order by ol.prtnum) group by rownum, cstprt, prtnum order by prtnum) where row_number=2)) lbl3 from dual
#
#SELECT=select decode((select count('x') from pckwrk where subucc='@subucc' and wrktyp='P'),1, ' ',(select rpad(nvl(cstprt,' '),27)||rpad(prtnum,27)||lpad(pckqty,5) from (select rownum row_number, cstprt, prtnum, sum(pckqty) pckqty from (select ol.cstprt, ol.prtnum, sum(pw.pckqty) pckqty from pckwrk pw, ord_line ol where pw.ctnnum='@subnum' and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' group by ol.cstprt, ol.prtnum order by ol.prtnum) group by rownum, cstprt, prtnum order by prtnum) where row_number=3)) lbl4 from dual
#
#SELECT=select decode((select count('x') from pckwrk where subucc='@subucc' and wrktyp='P'),1, ' ',(select rpad(nvl(cstprt,' '),27)||rpad(prtnum,27)||lpad(pckqty,5) from (select rownum row_number, cstprt, prtnum, sum(pckqty) pckqty from (select ol.cstprt, ol.prtnum, sum(pw.pckqty) pckqty from pckwrk pw, ord_line ol where pw.ctnnum='@subnum' and pw.ordnum=ol.ordnum and pw.ordlin=ol.ordlin and pw.ordsln=ol.ordsln and ol.client_id='----' group by ol.cstprt, ol.prtnum order by ol.prtnum) group by rownum, cstprt, prtnum order by prtnum) where row_number=4)) lbl5 from dual
#
#DATA=@adrnam~@adrln1~@adrln2~@adrln3~@ctystzip~@po~@dept~@vendor~@sscc~@xofx~@lbl1~@lbl2~@lbl3~@lbl4~@lbl5~
#
^XA^DFcartoncontent^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,15^ADN,36,10^FDTO: ^FS;
^FO65,15^ADN,54,15^FN1^FS;
^FO65,75^ADN,54,15^FN2^FS;
^FO65,135^ADN,54,15^FN3^FS;
^FO65,195^ADN,54,15^FN4^FS;
^FO65,255^ADN,54,15^FN5^FS;
^FO15,305^GB785,0,2^FS;
^FO15,315^ADN,54,15^FN6^FS;
^FO15,365^ADN,54,15^FN7^FS;
^FO15,415^ADN,54,15^FN8^FS;
^FO15,465^ADN,54,15^FN9^FS;
^FO15,515^ADN,54,15^FN10^FS;
^FO15,565^GB785,0,2^FS;
^FO15,575^ADN,54,15^FDCustPart#^FS;
^FO330,575^ADN,54,15^FDVendor SKU^FS;
^FO650,575^ADN,54,15^FDQTY^FS;
^FO15,625^GB785,0,2^FS;
^FO15,635^ADN,54,13^FN11^FS;
^FO15,635^ADN,54,13^FN12^FS;
^FO15,695^ADN,54,13^FN13^FS;
^FO15,755^ADN,54,13^FN14^FS;
^FO15,815^ADN,54,13^FN15^FS;
^PQ1,0,0,N^XZ;
