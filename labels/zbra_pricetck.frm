#
#SELECT=select '@tck_prtnum' prtnum, '$'||ltrim(to_char('@tck_price',9999.99)) price from dual
#
#DATA=@prtnum~@price~
^XA^DFpricetck^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO10,20^ADN,36,10^FN1^FS;
^FO255,70^ADN,36,10^FN2^FS;
^FO440,20^ADN,36,10^FN1^FS;
^FO685,70^ADN,36,10^FN2^FS;
^PQ1,0,0,N^XZ;
