#SELECT=select style_name, vendor_no, vendor_color_name, po_no, store_location, '$'||ltrim(to_char(comp,99999.99)) comp, '$'||ltrim(to_char(retail,99999.99)) retail, season_no, seq_no, max_seq_no, barcode, substr(barcode,1,2)||' '||substr(barcode,3,3)||' '||substr(barcode,6,7)||substr(barcode,19,1)||' '||substr(barcode,20,1) barhr from usr_tickets where po_no = '@po_no' and style_name='@style_name' 
#
#DATA=@style_name~@vendor_no~@vendor_color_name~@po_no~@store_location~@comp~@retail~@season_no~@seq_no~@max_seq_no~@barcode~@barhr~
#
^XA^DFburl12^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^POI^PMN^CI0^LRN;
^LH420,0;
^A0N,17,18^FO30,22^FDStyle^FS;
^A0N,17,18^FO114,22^FN1^FS;
^A0N,17,18^FO30,42^FDV^FS;
^A0N,17,18^FO42,42^FN2^FS; 
^A0N,17,18^FO280,20^FDLTBR^FS;
^A0N,17,18^FO114,42^FN3^FS;
^A0N,17,18^FO280,42^FN4^FS;
^A0N,17,18^FO99,82^FN5^FS;
^A0N,17,18^FO119,102^FDPast Season^FS;
^A0N,28,28^FO30,122^FDComparable Value^FS ;
^A0N,28,28^FO237,122^FN6^FS;
^A0N,34,34^FO140,147^FN7^FS ;
^A0N,17,18^FO2,180^FDSea^FS; 
^A0N,17,18^FO42,180^FN8^FS;
^A0N,17,18^FO60,180^FDLIN6^FS; 
^A0N,17,18^FO100,180^FN12^FS;
^A0N,17,18^FO310,180^FN9^FS ;
^A0N,17,18^FO335,180^FN10^FS; 
^A0N,17,18^FO325,180^FD/^FS;
^BY1^FO59,195^BCN,46,N,N,N^FN11^FS;
^PQ1,0,0,N^XZ;
