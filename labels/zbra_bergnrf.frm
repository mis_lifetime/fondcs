#
#UCC LABEL HEADER - THIS HEADER SELECTS EVERYTHING FOR ALL LABELS...
#
# Initialize the save variables for the incoming parameters to spaces/zeros
# and then fill them if the data was sent.  We will use this information if
# reprinting a label...
#
#SELECT=select ' ' SavPrtnum, ' ' SavCstprt, ' ' SavCarcod, ' ' SavStoloc, 0 SavPckqty, 0 SavNumber from dual
#SELECT=select '@prtnum' SavPrtnum from dual if (@prtnum)
#SELECT=select '@cstprt' SavCstprt from dual if (@cstprt)
#SELECT=select '@carcod' SavCarcod from dual if (@carcod)
#SELECT=select '@stoloc' SavStoloc from dual if (@stoloc)
#SELECT=select @pckqty Savpckqty from dual if (@pckqty)
#SELECT=select @number SavNumber from dual if (@number)
#
# Initialize the save variable for the wrkref.  We'll be using it to determine the flow of
# data...
#
#SELECT=select ' ' InWrkref from dual
#
#SELECT=select wrkref, wrkref InWrkref, wrktyp, pckqty, appqty, pckqty-appqty remqty, ship_id from pckwrk where wrkref='@wrkref' if (@wrkref)
#
# READ PICK WORK (PCKWRK), SHIP HEADER (SHPHDR), AND SHIP DETAIL (SHPDTL) TABLES...
#
#SELECT=select var_shpord.ship_id, var_shpord.ffcust, var_shpord.ffname, var_shpord.ffadd1, var_shpord.ffadd2, var_shpord.ffcity, var_shpord.ffstcd, var_shpord.ffposc, var_shpord.stcust, var_shpord.stname, var_shpord.stadd1, var_shpord.stadd2, var_shpord.stcity, var_shpord.ststcd, var_shpord.stposc, var_shpord.cponum, var_shpord.ordnum, var_shpord.cardsc, var_shpord.btcust, var_shpord.slscat, var_shpord.carcod, var_shpdtl_view.cstprt, var_shpdtl_view.deptno lbldept, pckwrk.srcloc, pckwrk.prtnum, pckwrk.subucc, pckwrk.cmbcod, pckwrk.devcod, pckwrk.dstloc, pckwrk.untcas, pckwrk.cur_cas, pckwrk.tot_cas_cnt from var_shpord, var_shpdtl_view, pckwrk where pckwrk.wrkref = '@wrkref' and pckwrk.ship_id = var_shpord.ship_id and pckwrk.ordnum = var_shpord.ordnum and pckwrk.ship_id = var_shpdtl_view.ship_id and pckwrk.ordnum = var_shpdtl_view.ordnum and pckwrk.ordlin = var_shpdtl_view.ordlin and pckwrk.ordsln = var_shpdtl_view.ordsln if (@InWrkref > ' ' and @wrktyp != 'R')
#
# Grab the substrings so the field lengths are correct for the labels...
#
#SELECT=select substr('@ordnum', 1, 12) lblordnum, substr('@ship_id', 1, 6) lblbolnum, substr('@ffcust', instr('@ffcust', '-', 1, 1) + 1, 20) lblffcust, substr('@ffname', 1, 30) lblffname, substr('@ffadd1', 1, 30) lblffadr1, substr('@ffadd2', 1, 30) lblffadr2, rtrim('@ffcity')||', '||'@ffstcd' lblffcstz, substr('@ffposc', 1, 5) lblffzip, substr('@stcust', instr('@stcust', '-', 1, 1) + 1, 20) lblstcust, substr('@stname', 1, 30) lblstname, substr('@stadd1', 1, 30) lblstadr1, substr('@stadd2', 1, 30) lblstadr2, rtrim('@stcity')||', '||'@ststcd' lblstcstz, substr('@stposc', 1, 5) lblstzip, substr('@cardsc', 1, 20) lblcarrier, substr('@cponum', 1, 14) lblcustpo, substr('@cstprt', 1, 14) lblitem, substr('@srcloc', 1, 12) lblsrcloc, substr('@subucc', 1, 21) lblsnbar, substr('@prtnum', 1, 6) lblprtnum, to_char(@untcas, 'fm9999') lbluntcas, substr('@upccod', 1, 12) lblupccod, to_char(sysdate,'MM/DD/YY') lbldat, to_char(@remqty, 'fm9999') lblqty from dual
#
# Fill in the Duplicate fields that appear more than once on a label...
#
#SELECT=select  '('||substr('@subucc',1,2)||')' lblsn_1, substr('@subucc',3,1) lblsn_2, substr('@subucc',4,7) lblsn_3, substr('@subucc',11,9) lblsn_4, substr('@subucc',20,1) lblsn_5 from dual
#
#SELECT=select '('||'420'||')' lbldesc1, '('||'91'||')' lbl_91, '@carcod' lblcarrier from dual
#SELECT=select to_char(cpodte, 'MMDDYY') cpodte from var_shpord where ship_id = '@ship_id'
#
#SELECT=select stoloc lbldestloc from pckwrk,pckmov where wrkref='@wrkref' and pckwrk.cmbcod = pckmov.cmbcod  and arecod in (select arecod from aremst where stgflg=1) and rownum < 2
#
#SELECT=select 'RESORT SEQ: '||clst_seq prosdest1 from pckwrk where wrkref='@wrkref'
#
#SELECT=select 'SORT LOC: '||decode((select pckmov.stoloc prosdest FROM pckwrk, pckmov, poldat where pckwrk.wrkref = '@wrkref' and pckwrk.cmbcod = pckmov.cmbcod and pckmov.arecod = poldat.rtstr1 and poldat.polcod = 'USR' and poldat.polvar = 'CASE-PICK-PROCESSING' and poldat.polval = 'AREAS' and poldat.rtnum1 = 1),null, ' ', (select pckmov.stoloc prosdest FROM pckwrk, pckmov, poldat where pckwrk.wrkref = '@wrkref' and pckwrk.cmbcod = pckmov.cmbcod and pckmov.arecod = poldat.rtstr1 and poldat.polcod = 'USR' and poldat.polvar = 'CASE-PICK-PROCESSING' and poldat.polval = 'AREAS' and poldat.rtnum1 = 1)) prosdest2 from dual
#
#SELECT=select decode(clst_seq,null,'@prosdest2','@prosdest1') prosdest from pckwrk where wrkref='@wrkref'
#
#SELECT=select decode((select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH'),null,'MIXED',(select substr(lngdsc, 1, 20)lbldescr from prtdsc where colnam='prtnum|prt_client_id' and colval= '@prtnum'||'|----' and locale_id ='US_ENGLISH')) lbldescr from dual 
#
#SELECT=select distinct 'Ent Date: '||to_char(entdte, 'MMDDYY') entdte, 'Reg Ship: '||to_char(early_shpdte, 'MMDDYY') regdte from ord_line where client_id='----' and ordnum='@ordnum'
#
#SELECT=select 'Carton '||to_char('@cur_cas', 'fm999999')||' of '||to_char('@tot_cas_cnt', 'fm999999') lblxofx from dual
#
#SELECT=select decode((select prtfam from prtmst where prtnum='@prtnum'),null,' ',(select prtfam from prtmst where prtnum='@prtnum')) prtfam from dual
#
#DATA=@lblffzip~@lblsnbar~@lblffname~@lblffadr1~@lblffadr2~@lblffcstz~@lblcustpo~@lbldept~@lblsn_1~@lblsn_2~@lblsn_3~@lblsn_4~@lblsn_5~@lblstcust~@dummy~@prtnum~@lblsrcloc~@dummy~@lblcarrier~@lbldestloc~@lbluntcas~@lblordnum~@ship_id~@dummy~@dummy~@prosdest~@lbldescr~@entdte~@regdte~@lblxofx~@prtfam~
#
^XA^DFbergnrf^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO15,54^ADN,36,10^FDLIFETIME BRANDS, INC.^FS;
^FO15,94^ADN,36,10^FD10825 PRODUCTION AVE.^FS;
^FO15,134^ADN,36,10^FDFONTANA, CA 92337^FS;
^FO320,52^GB0,227,2^FS;
^FO330,54^ADN,36,10^FDTO:^FS;
^FO380,54^ADN,54,13^FN3^FS;
^FO380,122^ADN,36,10^FN4^FS;
^FO380,168^ADN,36,10^FN5^FS;
^FO380,214^ADN,36,10^FN6^FS;
^FO621,214^ADN,36,10^FN1^FS;
^FO15,278^GB785,0,2^FS;
^FO360,278^GB0,223,2^FS;
^FO15,498^GB785,0,2^FS;
^FO15,510^ADN,74,30^FDPO#:^FS;
^FO185,510^ADN,74,30^FN7^FS;
^FO15,600^ADN,74,30^FDDEPT:^FS;
^FO235,600^ADN,74,30^FN8^FS;
^FO15,658^GB785,0,2^FS;
^FO430,668^ADN,18,10^FDFOR STORE:^FS;
^FO430,718^ADN,100,85^FN14^FS;
^FO420,658^GB0,216,2^FS;
^FO15,873^GB785,0,2^FS;
^FO15,888^ADN,18,10^FD(00) SERIAL SHIPPING CONTAINER^FS;
^FO191,920^ADN,36,10^FN9^FS;
^FO276,920^ADN,36,10^FN10^FS;
^FO331,920^ADN,36,10^FN11^FS;
^FO462,920^ADN,36,10^FN12^FS;
^FO629,920^ADN,36,10^FN13^FS;
^BY4,2.3,255^FO100,963^BCN,,N,N,Y,U^FN2^FS;
^FO380,298^ADN,18,10^FDCARRIER:^FS;
^FO15,788^ADN,36,10^FN24^FS;
^FO235,788^ADN,36,10^FN25^FS;
^FO420,340^ADN,36,15^FN19^FS;
^FO380,380^ADN,54,13^FN30^FS;
^FO15,1244^ADN,50,15^FDRack Location:^FS;
^FO380,1244^ADN,50,15^FN17^FS;
^FO15,1304^ADN,40,15^FDCPQ:^FS;
^FO120,1304^ADN,40,15^FN21^FS;
^FO175,1304^ADN,40,15^FDItem#:^FS;
^FO350,1304^ADN,40,15^FN16^FS;
^FO15,1344^ADN,40,15^FDDESC:^FS;
^FO150,1344^ADN,40,15^FN27^FS;
^FO15,1384^ADN,70,30^FN26^FS;
^FO15,1470^ADN,40,15^FDShip Stage:^FS;
^FO300,1470^ADN,40,15^FN19^FS;
^FO15,1524^ADN,36,10^FDSID:^FS;
^FO170,1524^ADN,36,10^FN23^FS;
^FO380,1524^ADN,36,10^FDOrd:^FS;
^FO455,1524^ADN,36,10^FN22^FS;
^FO15,1560^ADN,36,10^FN29^FS;
^FO455,1560^ADN,36,10^FN28^FS;
^FO300,1560^ADN,36,10^FN31^FS;
^PQ1,0,0,N^XZ;
