#
#
#SELECT=select 'ITEM #  '||'@tck_prtnum' prtnum, '$'||ltrim(to_char('@tck_price',9999.99)) price from dual
#
#SELECT=select 'CPN # '||cstprt cstprt from ord_line where client_id='----' and ordnum='@ordnum' and prtnum='@tck_prtnum'
#
#SELECT=select decode((select upccod lblupc from prtmst where prtnum='@tck_prtnum'),null,' ',(select upccod lblupc from prtmst where prtnum='@tck_prtnum')) lblupc from dual
#
#SELECT=select decode((select substr(lngdsc,1,30) lbldesc from prtdsc where colval='@tck_prtnum'||'|----'),null,' ',(select substr(lngdsc,1,30) lbldesc from prtdsc where colval='@tck_prtnum'||'|----')) lbldesc from dual
#
#DATA=@prtnum~@dummy~@lblupc~@lbldesc~@cstprt~
^XA^DFrosstck^FS^SZ2^MMT~JSN^LT0^MD5^MNY^PRE^PON^PMN^CI0^LRN;
^LH10,0;
^FO40,15^ADN,36,10^FN1^FS;
^FO40,95^ADN,36,10^FN5^FS;
^BY2,,50^FO40,135^BUN,50,Y,N,Y^FN3^FS;
^FO10,55^ADN,36,10^FN4^FS;
^FO470,15^ADN,36,10^FN1^FS;
^FO470,95^ADN,36,10^FN5^FS;
^BY2,,50^FO470,135^BUN,50,Y,N,Y^FN3^FS;
^FO440,55^ADN,36,10^FN4^FS;
^PQ1,0,0,N^XZ;
