<command>
<name>handle usr cartonization data request</name>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
publish data
 where handler_name = 'handle usr cartonization data request'
|
if (@request = 'create test data')
{
    {
        publish data
         where ctnz_request = @request
        |
        handle usr cartonization log request
         where request = 'log ctnz event'
           and ship_id = @ship_id
           and handler = @handler_name
           and ctnz_msg = 'creating headers . . .'
    }
    |
    {
        [insert
           into usr_pckwrk_ctnz(WRKREF, WRKTYP, SCHBAT, SRCLOC, DSTLOC, SRCARE, DSTARE, SHIP_LINE_ID, SHIP_ID, CLIENT_ID, ORDNUM, ORDLIN, ORDSLN, STCUST, CONCOD, CMBCOD, LBLBAT, LBLSEQ, DEVCOD, PCKQTY, APPQTY, PCKSTS, PRTNUM, PRT_CLIENT_ID, ORGCOD, REVLVL, LOTNUM, INVSTS, LODLVL, LODNUM, SUBNUM, DTLNUM, UNTCAS, UNTPAK, FTPCOD, CTNCOD, CTNNUM, CTNSEG, LODUCC, SUBUCC, VISFLG, SPLFLG, LOCFLG, LODFLG, SUBFLG, DTLFLG, PRTFLG, ORGFLG, REVFLG, LOTFLG, QTYFLG, ADDDTE, CMPDTE, REFLOC, WKONUM, WKOREV, WKOLIN, ACKDEVCOD, CUR_CAS, TOT_CAS_CNT, PIPFLG, VC_SLOTNO, VC_TCKQTY, RTCUST, INVSTS_PRG, FRSFLG, MIN_SHELF_HRS, PCKDTE, CATCH_QTY_FLG, PRTDTE, CLST_SEQ, TRAKNM, SHIP_TO_HOLD_FLG, UC_RATE_TOTAL, PS_VOID_ID, USR_CMBCOD)
         select WRKREF,
                WRKTYP,
                SCHBAT,
                SRCLOC,
                DSTLOC,
                SRCARE,
                DSTARE,
                SHIP_LINE_ID,
                SHIP_ID,
                CLIENT_ID,
                ORDNUM,
                ORDLIN,
                ORDSLN,
                STCUST,
                CONCOD,
                CMBCOD,
                LBLBAT,
                LBLSEQ,
                DEVCOD,
                PCKQTY,
                APPQTY,
                'P',
                PRTNUM,
                PRT_CLIENT_ID,
                ORGCOD,
                REVLVL,
                LOTNUM,
                INVSTS,
                LODLVL,
                LODNUM,
                SUBNUM,
                DTLNUM,
                UNTCAS,
                UNTPAK,
                FTPCOD,
                CTNCOD,
                NULL,
                CTNSEG,
                LODUCC,
                SUBUCC,
                VISFLG,
                SPLFLG,
                LOCFLG,
                LODFLG,
                SUBFLG,
                DTLFLG,
                PRTFLG,
                ORGFLG,
                REVFLG,
                LOTFLG,
                QTYFLG,
                ADDDTE,
                CMPDTE,
                REFLOC,
                WKONUM,
                WKOREV,
                WKOLIN,
                ACKDEVCOD,
                CUR_CAS,
                TOT_CAS_CNT,
                PIPFLG,
                VC_SLOTNO,
                VC_TCKQTY,
                RTCUST,
                INVSTS_PRG,
                FRSFLG,
                MIN_SHELF_HRS,
                PCKDTE,
                CATCH_QTY_FLG,
                PRTDTE,
                CLST_SEQ,
                TRAKNM,
                SHIP_TO_HOLD_FLG,
                UC_RATE_TOTAL,
                PS_VOID_ID,
                USR_CMBCOD
           from pckwrk pw
          where pw.ship_id = @ship_id
            and pw.lodlvl = 'D'
            and pw.wrktyp = 'P']
        |
        [insert
           into usr_pckmov_ctnz(cmbcod, seqnum, arecod, stoloc, rescod, arrqty, prcqty)
         select cmbcod,
                seqnum,
                arecod,
                stoloc,
                rescod,
                arrqty,
                prcqty
           from pckmov pm
          where cmbcod in (select distinct cmbcod
                             from pckwrk pw
                            where pw.ship_id = @ship_id
                              and pw.lodlvl = 'D'
                              and pw.wrktyp = 'P')]
        |
        commit;
    }
} >> create_rez
else if (@request = 'create pick headers')
{
    get usr cartonization mode values
    |
    {
        publish data
         where ctnz_request = @request
        |
        handle usr cartonization log request
         where request = 'log ctnz event'
           and ship_id = @ship_id
           and handler = @handler_name
           and ctnz_msg = 'Live(' || @is_live_flg || '): Creating New Kit Data . . .'
    }
    |
    if (@is_live_flg = 1)
    {
        [select pw.wrkref my_new_wrkref,
                pw.ctnnum my_new_ctnnum,
                pw.cmbcod my_new_cmbcod
           from pckwrk pw
          where pw.traknm = 'CARTONIZATION-START'
            and pw.ctncod is null
            and rownum < 2] catch(@?)
        |
        if (@? != 0)
        {
            generate next number
             where numcod = 'wrkref'
            |
            publish data
             where my_new_wrkref = @nxtnum
            |
            generate next number
             where numcod = 'cmbcod'
            |
            publish data
             where my_new_cmbcod = @nxtnum
            |
            generate next number
             where numcod = 'ctnnum'
            |
            publish data
             where my_new_ctnnum = @nxtnum
            |
            [select *
               from pckwrk
              where wrkref = (select min(wrkref)
                                from pckwrk
                               where ship_id = @ship_id
                                 and ordnum = @ordnum
                                 and ordlin = @ordlin
                                 and wrktyp = 'P'
                                 and lodlvl = 'D'
                                 and pcksts = 'P')
                and rownum < 2]
            |
            [insert
               into pckwrk(WRKREF, WRKTYP, SCHBAT, SRCLOC, DSTLOC, SRCARE, DSTARE, SHIP_LINE_ID, SHIP_ID, CLIENT_ID, ORDNUM, ORDLIN, ORDSLN, STCUST, CONCOD, CMBCOD, LBLBAT, LBLSEQ, DEVCOD, PCKQTY, APPQTY, PCKSTS, PRTNUM, PRT_CLIENT_ID, ORGCOD, REVLVL, LOTNUM, INVSTS, LODLVL, LODNUM, SUBNUM, DTLNUM, UNTCAS, UNTPAK, FTPCOD, CTNCOD, CTNNUM, CTNSEG, LODUCC, SUBUCC, VISFLG, SPLFLG, LOCFLG, LODFLG, SUBFLG, DTLFLG, PRTFLG, ORGFLG, REVFLG, LOTFLG, QTYFLG, ADDDTE, CMPDTE, REFLOC, WKONUM, WKOREV, WKOLIN, ACKDEVCOD, CUR_CAS, TOT_CAS_CNT, PIPFLG, VC_SLOTNO, VC_TCKQTY, RTCUST, INVSTS_PRG, FRSFLG, MIN_SHELF_HRS, PCKDTE, CATCH_QTY_FLG, PRTDTE, CLST_SEQ, TRAKNM, SHIP_TO_HOLD_FLG, UC_RATE_TOTAL, PS_VOID_ID, USR_CMBCOD)
             select @my_new_wrkref WRKREF,
                    'K' WRKTYP,
                    SCHBAT,
                    SRCLOC,
                    DSTLOC,
                    SRCARE,
                    DSTARE,
                    SHIP_LINE_ID,
                    SHIP_ID,
                    CLIENT_ID,
                    ORDNUM,
                    ORDLIN,
                    ORDSLN,
                    STCUST,
                    CONCOD,
                    @my_new_cmbcod CMBCOD,
                    LBLBAT,
                    LBLSEQ,
                    DEVCOD,
                    1 PCKQTY,
                    0 APPQTY,
                    'P',
                    'KITPART' PRTNUM,
                    PRT_CLIENT_ID,
                    ORGCOD,
                    REVLVL,
                    'NOLOT' LOTNUM,
                    'A' INVSTS,
                    'S' LODLVL,
                    NULL LODNUM,
                    @my_new_ctnnum SUBNUM,
                    NULL DTLNUM,
                    1 UNTCAS,
                    1 UNTPAK,
                    FTPCOD,
                    NULL CTNCOD,
                    NULL CTNNUM,
                    NULL CTNSEG,
                    NULL LODUCC,
                    NULL SUBUCC,
                    0 VISFLG,
                    0 SPLFLG,
                    0 LOCFLG,
                    0 LODFLG,
                    1 SUBFLG,
                    0 DTLFLG,
                    0 PRTFLG,
                    0 ORGFLG,
                    0 REVFLG,
                    0 LOTFLG,
                    0 QTYFLG,
                    SYSDATE ADDDTE,
                    NULL CMPDTE,
                    NULL REFLOC,
                    WKONUM,
                    WKOREV,
                    WKOLIN,
                    NULL ACKDEVCOD,
                    NULL CUR_CAS,
                    NULL TOT_CAS_CNT,
                    PIPFLG,
                    NULL VC_SLOTNO,
                    NULL VC_TCKQTY,
                    RTCUST,
                    'A' INVSTS_PRG,
                    0 FRSFLG,
                    MIN_SHELF_HRS,
                    NULL PCKDTE,
                    CATCH_QTY_FLG,
                    NULL PRTDTE,
                    CLST_SEQ,
                    'CARTONIZATION-START' TRAKNM,
                    NULL SHIP_TO_HOLD_FLG,
                    NULL UC_RATE_TOTAL,
                    NULL PS_VOID_ID,
                    NULL USR_CMBCOD
               from pckwrk pw
              where pw.wrkref = @wrkref
                and pw.lodlvl = 'D'
                and pw.wrktyp = 'P']
            |
            [insert
               into pckmov(cmbcod, seqnum, arecod, stoloc, rescod, arrqty, prcqty)
             select @my_new_cmbcod,
                    seqnum,
                    arecod,
                    stoloc,
                    rescod,
                    0,
                    0
               from pckmov
              where cmbcod = @cmbcod]
            |
            publish data
             where my_new_wrkref = @my_new_wrkref
               and my_new_cmbcod = @my_new_cmbcod
               and my_new_ctnnum = @my_new_ctnnum
        }
        |
        publish data
         where my_new_wrkref = @my_new_wrkref
           and my_new_cmbcod = @my_new_cmbcod
           and my_new_ctnnum = @my_new_ctnnum
    }
    else
    {
        [select pw.wrkref my_new_wrkref,
                pw.ctnnum my_new_ctnnum,
                pw.cmbcod my_new_cmbcod
           from usr_pckwrk_ctnz pw
          where pw.traknm = 'CARTONIZATION-START'
            and pw.ctncod is null
            and rownum < 2] catch(@?)
        |
        if (@? != 0)
        {
            get session variable
             where name = 'ctnz_seqnum' catch(@?)
            |
            publish data
             where my_new_wrkref = 'WRK-' || @value
            |
            publish data
             where my_new_cmbcod = 'CMB:-' || @value
            |
            publish data
             where my_new_ctnnum = 'CTN:-' || @value
            |
            [select *
               from usr_pckwrk_ctnz
              where wrkref = (select min(wrkref)
                                from usr_pckwrk_ctnz
                               where ship_id = @ship_id
                                 and ordnum = @ordnum
                                 and ordlin = @ordlin
                                 and wrktyp = 'P'
                                 and lodlvl = 'D'
                                 and pcksts = 'P')
                and rownum < 2]
            |
            [insert
               into usr_pckwrk_ctnz(WRKREF, WRKTYP, SCHBAT, SRCLOC, DSTLOC, SRCARE, DSTARE, SHIP_LINE_ID, SHIP_ID, CLIENT_ID, ORDNUM, ORDLIN, ORDSLN, STCUST, CONCOD, CMBCOD, LBLBAT, LBLSEQ, DEVCOD, PCKQTY, APPQTY, PCKSTS, PRTNUM, PRT_CLIENT_ID, ORGCOD, REVLVL, LOTNUM, INVSTS, LODLVL, LODNUM, SUBNUM, DTLNUM, UNTCAS, UNTPAK, FTPCOD, CTNCOD, CTNNUM, CTNSEG, LODUCC, SUBUCC, VISFLG, SPLFLG, LOCFLG, LODFLG, SUBFLG, DTLFLG, PRTFLG, ORGFLG, REVFLG, LOTFLG, QTYFLG, ADDDTE, CMPDTE, REFLOC, WKONUM, WKOREV, WKOLIN, ACKDEVCOD, CUR_CAS, TOT_CAS_CNT, PIPFLG, VC_SLOTNO, VC_TCKQTY, RTCUST, INVSTS_PRG, FRSFLG, MIN_SHELF_HRS, PCKDTE, CATCH_QTY_FLG, PRTDTE, CLST_SEQ, TRAKNM, SHIP_TO_HOLD_FLG, UC_RATE_TOTAL, PS_VOID_ID, USR_CMBCOD)
             select @my_new_wrkref WRKREF,
                    'K' WRKTYP,
                    SCHBAT,
                    SRCLOC,
                    DSTLOC,
                    SRCARE,
                    DSTARE,
                    SHIP_LINE_ID,
                    SHIP_ID,
                    CLIENT_ID,
                    ORDNUM,
                    ORDLIN,
                    ORDSLN,
                    STCUST,
                    CONCOD,
                    @my_new_cmbcod CMBCOD,
                    LBLBAT,
                    LBLSEQ,
                    DEVCOD,
                    1 PCKQTY,
                    0 APPQTY,
                    'P',
                    'KITPART' PRTNUM,
                    PRT_CLIENT_ID,
                    ORGCOD,
                    REVLVL,
                    'NOLOT' LOTNUM,
                    'A' INVSTS,
                    'S' LODLVL,
                    NULL LODNUM,
                    @my_new_ctnnum SUBNUM,
                    NULL DTLNUM,
                    1 UNTCAS,
                    1 UNTPAK,
                    FTPCOD,
                    NULL CTNCOD,
                    NULL CTNNUM,
                    NULL CTNSEG,
                    NULL LODUCC,
                    NULL SUBUCC,
                    0 VISFLG,
                    0 SPLFLG,
                    0 LOCFLG,
                    0 LODFLG,
                    1 SUBFLG,
                    0 DTLFLG,
                    0 PRTFLG,
                    0 ORGFLG,
                    0 REVFLG,
                    0 LOTFLG,
                    0 QTYFLG,
                    SYSDATE ADDDTE,
                    NULL CMPDTE,
                    NULL REFLOC,
                    WKONUM,
                    WKOREV,
                    WKOLIN,
                    NULL ACKDEVCOD,
                    NULL CUR_CAS,
                    NULL TOT_CAS_CNT,
                    PIPFLG,
                    NULL VC_SLOTNO,
                    NULL VC_TCKQTY,
                    RTCUST,
                    'A' INVSTS_PRG,
                    0 FRSFLG,
                    MIN_SHELF_HRS,
                    NULL PCKDTE,
                    CATCH_QTY_FLG,
                    NULL PRTDTE,
                    CLST_SEQ,
                    'CARTONIZATION-START' TRAKNM,
                    NULL SHIP_TO_HOLD_FLG,
                    NULL UC_RATE_TOTAL,
                    NULL PS_VOID_ID,
                    NULL USR_CMBCOD
               from pckwrk pw
              where pw.wrkref = @wrkref
                and pw.lodlvl = 'D'
                and pw.wrktyp = 'P']
            |
            [insert
               into usr_pckmov_ctnz(cmbcod, seqnum, arecod, stoloc, rescod, arrqty, prcqty)
             select @my_new_cmbcod,
                    seqnum,
                    arecod,
                    stoloc,
                    rescod,
                    0,
                    0
               from usr_pckmov_ctnz
              where cmbcod = @cmbcod]
            |
            publish data
             where my_new_wrkref = @my_new_wrkref
               and my_new_cmbcod = @my_new_cmbcod
               and my_new_ctnnum = @my_new_ctnnum
        }
        |
        publish data
         where my_new_wrkref = @my_new_wrkref
           and my_new_cmbcod = @my_new_cmbcod
           and my_new_ctnnum = @my_new_ctnnum
    }
    |
    publish data
     where my_new_wrkref = @my_new_wrkref
       and my_new_cmbcod = @my_new_cmbcod
       and my_new_ctnnum = @my_new_ctnnum
}
else if (@request = 'reset test data for shipment')
{
    {
        publish data
         where ctnz_request = @request
        |
        handle usr cartonization log request
         where request = 'log ctnz event'
           and ship_id = @ship_id
           and handler = @handler_name
           and ctnz_msg = null
    }
    |
    [delete
       from usr_pckmov_ctnz
      where cmbcod in (select cmbcod
                         from usr_pckwrk_ctnz
                        where ship_id = @ship_id)] catch(@?);
    [delete
       from usr_pckwrk_ctnz
      where ship_id = @ship_id] catch(@?);
    [delete
       from usr_cartonization_log
      where ship_id = @ship_id] catch(@?);
    commit
}
else
    set return status
     where status = 99987
]]>
</local-syntax>
</command>
