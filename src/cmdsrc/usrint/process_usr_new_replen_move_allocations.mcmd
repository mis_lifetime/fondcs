<command>
<name>process usr new replen move allocations</name>
<description>process usr new replen move allocations</description>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
process usr pending qvl validation catch(@?) >> myres1
|
{
    list usr new replen parts for replenishment processing catch(-1403)
    |
    publish data
     where my_prtnum = @my_prtnum
       and my_untcas = @my_untcas
       and my_untpak = @my_untpak
       and my_lotnum = @my_lotnum
       and my_invsts_prg = @my_invsts_prg
       and my_dstare = @my_dstare
}
|
if (@? = 0 and @my_prtnum and @my_dstare)
{
    /* the driver above gets us dstare level detail but we need to get information at the part level so that we make sure enough inventory is moving for the part */
    list usr new replen parts for replenishment moves
     where my_prtnum = @my_prtnum
       and my_untcas = @my_untcas
       and my_untpak = @my_untpak
       and my_lotnum = @my_lotnum
       and my_invsts_prg = @my_invsts_prg
    |
    /* see if we need to process case pick replenishment */
    if (to_number(nvl(@my_cases_for_case_picking, 0)) != 0)
    {
        /* see if there we have replen picks of there is already enough on the way and we have nothing we can allocate for case picks*/
        if (to_number(nvl(@my_total_going_to_case_pick, 0)) < to_number(nvl(@my_units_for_case_picking, 0)) and to_number(nvl(@my_units_for_case_picking, 0)) > to_number(nvl(@my_case_avail_qty, 0)) and to_number(nvl(@my_case_avail_qty, 0)) < to_number(nvl(@my_untcas, 0)))
        {
            save session variable
             where name = 'do_more_case_rpl_allocates'
               and value = 1
            |
            [select decode(@my_dstare, 'SSTG', 'RSSTG', 'RSSTG', 'SSTG', 'FSTG', 'RPSTG', 'RPSTG', 'FSTG') my_alt_dstare
               from dual]
            |
            {
                [select count(p.prtnum) foam_ind
                   from prtmst p
                  where p.prt_client_id = '----'
                    and p.prtnum = @my_prtnum
                    and upper(nvl(p.foam_gte, '0')) = '1']
                |
                if (nvl(@foam_ind, 0) = 1)
                    publish data
                     where my_rsp_clause = ' poldat.rtstr1 like ' || '''' || 'RSP%' || '''' || ' and 1 = 1  '
                       and my_rtstr1_clause = ' poldat.rtstr1 != ' || '''' || 'RCASEV' || '''' || ' and 1 = 1  '
                ELSE
                {
                    if (nvl(@my_case_pick_dstare_restriction, 'NONE') = 'NO RCASEV')
                        publish data
                         where my_rtstr1_clause = ' poldat.rtstr1 != ' || '''' || 'RCASEV' || '''' || ' and 1 = 1  '
                    ELSE
                        publish data
                         where my_rtstr1_clause = ' poldat.rtstr1 = ' || '''' || 'RCASEV' || '''' || ' and 1 = 1  '
                    |
                    publish data
                     where my_rsp_clause = ' poldat.rtstr1 not like  ' || '''' || 'RSP%' || '''' || ' and 1 = 1  '
                       and my_rtstr1_clause = @my_rtstr1_clause
                }
                |
                /* find the areas to replenish to */
                /* you will need to add logic here to figure out wether to replenish fontana or rialto */
                [select distinct rtstr1 my_rpl_dstare,
                        srtseq,
                        a.loccod my_loccod
                   from poldat,
                        aremst a
                  where polcod = 'EMERGENCY-REPLENISHMENTS'
                    and polvar = 'REPLEN-PATH-CASE'
                    and polval = 'RSSTG'
                    and rtstr2 is null
                    and poldat.rtstr1 = a.arecod
                    and @my_rtstr1_clause:raw
                    and @my_rsp_clause:raw
                  order by srtseq] catch(-1403)
                |
                if (@? = 0)
                {
                    if (nvl(@my_loccod, 'NONE') = 'P' or nvl(@my_loccod, 'NONE') = 'V')
                        publish data
                         where my_pcklvl = 'L'
                    else
                        publish data
                         where my_pcklvl = 'S'
                    |
                    get session variable
                     where name = 'do_more_case_rpl_allocates' catch(@?)
                    |
                    if (nvl(@value, 0) = 1)
                    {
                        allocate inventory and location
                         where type = 'REPLEN'
                           and prtnum = @my_prtnum
                           and prt_client_id = '----'
                           and quantity = @my_units_for_case_picking
                           and lotnum = @my_lotnum
                           and invsts_prg = @my_invsts_prg
                           and segqty = @my_units_for_case_picking
                           and dstare = @my_rpl_dstare
                           and pcklvl = @my_pcklvl
                           and splflg = 1
                           and untcas = @my_untcas
                           and untpak = @my_untpak
                           and retmov = 'NO'
                           and usr_id = 'SUPPORT'
                           and pipcod = 'N' catch(@?)
                        |
                        if (@? = 0)
                        {
                            get session variable
                             where name = 'allocated_case_qty' catch(@?)
                            |
                            [select to_number(nvl(@value, 0)) + to_number(nvl(@pckqty, 0)) my_total_allocated_qty
                               from dual]
                            |
                            if (to_number(nvl(@my_total_allocated_qty, 0)) >= to_number(nvl(@my_units_for_case_picking, 0)))
                                save session variable
                                 where name = 'do_more_case_rpl_allocates'
                                   and value = 0
                            else
                                save session variable
                                 where name = 'do_more_case_rpl_allocates'
                                   and value = 1
                            |
                            save session variable
                             where name = 'allocated_case_qty'
                               and value = @my_total_allocated_qty
                        }
                    }
                }
            }
        }
    }
    |
    /* see if we need to process piece pick replenishment */
    if (to_number(nvl(@my_cases_for_piece_picking, 0)) != 0)
    {
        {
            /* cancel counts at piece pick faces */
            [select distinct l.stoloc my_cip_stoloc
               from aremst a,
                    locmst l,
                    invsum i
              where a.arecod = l.arecod
                and l.stoloc = i.stoloc
                and i.prtnum = @my_prtnum
                and nvl(a.dtlflg, 0) = 1
                and nvl(l.cipflg, 0) = 1] catch(-1403)
            |
            if (@? = 0 and @my_cip_stoloc)
                /* cancel counts
                   where stoloc = @my_cip_stoloc catch(@?) >> my_can_res */
                publish data
                 where cancel_that_count = 1
        } >> my_res
        |
        /* see if there we have replen picks of there is already enough on the way */
        if (to_number(nvl(@my_total_going_to_piece_pick, 0)) < to_number(nvl(@my_units_for_piece_picking, 0)) and to_number(nvl(@my_pieces_needed, 0)) > to_number(nvl(@my_piece_avail_qty, 0)) and to_number(nvl(@my_piece_avail_qty, 0)) = 0)
        {
            save session variable
             where name = 'do_more_piece_rpl_allocates'
               and value = 1
            |
            [select decode(@my_dstare, 'SSTG', 'RSSTG', 'RSSTG', 'SSTG', 'FSTG', 'RPSTG', 'RPSTG', 'FSTG') my_alt_dstare
               from dual]
            |
            [select count(p.prtnum) foam_ind
               from prtmst p
              where p.prt_client_id = '----'
                and p.prtnum = @my_prtnum
                and upper(nvl(p.foam_gte, '0')) = '1']
            |
            if (nvl(@foam_ind, 0) = 1)
                publish data
                 where my_rsp_clause = ' poldat.rtstr1 like ' || '''' || 'RSP%' || '''' || ' and 1 = 1  '
            ELSE
                publish data
                 where my_rsp_clause = ' poldat.rtstr1 not like  ' || '''' || 'RSP%' || '''' || ' and 1 = 1  '
            |
            /* find the areas to replenish to */
            /* you will need to add logic here to figure out wether to replenish fontana or rialto */
            [select @my_piece_pick_dstare_restriction krill
               from dual]
            |
            publish data
             where my_replen_to_clause = 'rtstr1 != ' || '''' || 'PIPALLETPP' || '''' || ' and 1 = 1 '
               and krill = 1
            |
            [select distinct 1
               from dual
              where nvl(@my_uc_food_service_ind, 0) = 1] catch(-1403)
            |
            if (@? = 0)
                publish data
                 where my_fs_replen_to_clause = ' rtstr1 in (' || '''' || 'RFSFLOW' || '''' || ',' || '''' || 'RFSDTLPLT' || '''' || ' )'
                   and krill = 1
            else
                publish data
                 where my_fs_replen_to_clause = ' rtstr1 not in (' || '''' || 'RFSFLOW' || '''' || ',' || '''' || 'RFSDTLPLT' || '''' || ' )'
                   and krill = 1
            |
            {
                [select distinct rtstr1 my_dstare,
                        srtseq,
                        a.loccod my_loccod
                   from poldat,
                        aremst a
                  where polcod = 'EMERGENCY-REPLENISHMENTS'
                    and polvar = 'REPLEN-PATH-PIECE'
                 /* and polval = @my_dstare */
                    and polval = 'RSSTG'
                    and rtstr2 is null
                    and poldat.rtstr1 = a.arecod
                    and @my_replen_to_clause:raw
                    and @my_fs_replen_to_clause:raw
                    and @my_rsp_clause:raw
                  order by srtseq] catch(-1403)
                |
                if (@? != 0)
                    publish data
                     where my_dstare = 'XNONEX'
                |
                if (nvl(@my_loccod, 'NONE') = 'P')
                    publish data
                     where my_pcklvl = 'L'
                else
                    publish data
                     where my_pcklvl = 'S'
                |
                publish data
                 where my_orig_units_for_piece_picking = @my_units_for_piece_picking
                |
                if (instr(@my_dstare, 'RFLOW') != 0 or instr(@my_dstare, 'RRSFLOW') != 0)
                {
                    [select ceil((to_number(@my_units_for_piece_picking) / to_number(@my_untcas))) cases,
                            ceil(ceil((to_number(@my_units_for_piece_picking) / to_number(@my_untcas))) * f.caslen) length,
                            ceil(ceil(ceil((to_number(@my_units_for_piece_picking) / to_number(@my_untcas))) * f.caslen) / 96) times_through,
                            ceil(to_number(@my_units_for_piece_picking) / (ceil(ceil(ceil((to_number(@my_units_for_piece_picking) / to_number(@my_untcas))) * f.caslen) / 96))) pieces_per_allocate
                       from prtmst p,
                            ftpmst f
                      where p.prtnum = @my_prtnum
                        and p.prt_client_id = '----'
                        and p.ftpcod = f.ftpcod] catch(@?)
                    |
                    if (@times_through > 1)
                        publish data
                         where times_through = 1
                           and my_units_for_piece_picking = @pieces_per_allocate
                    else
                        publish data
                         where times_through = 1
                           and my_units_for_piece_picking = @my_units_for_piece_picking
                }
                else
                    publish data
                     where times_through = 1
                |
                do loop
                 where count = @times_through
                |
                {
                    get session variable
                     where name = 'do_more_piece_rpl_allocates' catch(@?)
                    |
                    if (nvl(@value, 0) = 1 and @my_dstare != 'XNONEX')
                    {
                        allocate inventory and location
                         where type = 'REPLEN'
                           and prtnum = @my_prtnum
                           and prt_client_id = '----'
                           and quantity = @my_units_for_piece_picking
                           and lotnum = @my_lotnum
                           and invsts_prg = @my_invsts_prg
                           and segqty = @my_units_for_piece_picking
                           and dstare = @my_dstare
                           and pcklvl = @my_pcklvl
                           and splflg = @my_splflg
                           and untcas = @my_untcas
                           and untpak = @my_untpak
                           and retmov = 'NO'
                           and usr_id = 'SUPPORT'
                           and pipcod = 'N'
                           and ovrflg = 0 catch(@?)
                        |
                        if (@? = 0)
                        {
                            get session variable
                             where name = 'allocated_piece_qty' catch(@?)
                            |
                            [select to_number(nvl(@value, 0)) + to_number(nvl(@pckqty, 0)) my_total_allocated_qty
                               from dual]
                            |
                            if (to_number(nvl(@my_total_allocated_qty, 0)) >= to_number(nvl(@my_orig_units_for_piece_picking, 0)))
                                save session variable
                                 where name = 'do_more_piece_rpl_allocates'
                                   and value = 0
                            else
                                save session variable
                                 where name = 'do_more_piece_rpl_allocates'
                                   and value = 1
                            |
                            save session variable
                             where name = 'allocated_piece_qty'
                               and value = @my_total_allocated_qty
                        }
                    }
                }
            }
        }
    }
}
|
commit
]]>
</local-syntax>
</command>
