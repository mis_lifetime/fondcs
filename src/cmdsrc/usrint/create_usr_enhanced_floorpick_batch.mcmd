<command>
<name>create usr enhanced floorpick batch</name>
<type>Local Syntax</type>
<local-syntax>
<![CDATA[
if (@tstflg = 1)
{
    set return status
     where status = 0
}
else
{
    validate usr floorpick batch printers
     where lbl_prtadr = @lbl_prtadr
       and ppw_prtadr = @ppw_prtadr
       and phz = @phz
    |
    publish data
     where lbl_prtadr = @lbl_prtadr
       and ppw_prtadr = @ppw_prtadr
    |
    [select rtnum1 p_vol
       from poldat
      where polcod = 'USR'
        and polvar = 'AUTOSTAGE'
        and polval = 'MAX-PALLET-VOLUME']
    |
    generate next number
     where numcod = 'lodnum'
    |
    save session variable
     where name = 'CURRENT-VOLUME'
       and value = 0
    |
    save session variable
     where name = 'PALLET-VOLUME'
       and value = @p_vol
    |
    save session variable
     where name = 'LABEL-BATCH'
       and value = @nxtnum
    |
    save session variable
     where name = 'BATCH-LIST'
       and value = @nxtnum
    |
    [select count(s.ship_id) fs_cnt
       from shipment s
      where s.ship_id = @ship_id
        and nvl(s.uc_food_service_ind, 0) = 1]
    |
    {
        if (nvl(@fs_cnt, 0) = 1)
            [select pw.wrkref,
                    l.trvseq mytrvseq,
                    pw.srcloc mysrcloc,
                    pw.lblseq mylblseq,
                    sum((caslen*caswid*cashgt) *(pw.pckqty / pw.untcas)) shpvol
               from locmst l,
                    ord o,
                    shipment sh,
                    ftpmst f,
                    pckwrk pw
              where pw.ship_id = @ship_id
                and sh.ship_id = pw.ship_id
                and UPPER(sh.wave_set) not like '%ASTAGE%'
                and pw.schbat = @schbat
                and pw.lblbat = '------'
                and pw.wrktyp = 'P'
                and pw.lodlvl = 'S'
                and pw.pcksts = 'R'
                and pw.appqty != pw.pckqty
                and f.ftpcod = pw.ftpcod
                and o.ordnum = pw.ordnum
                and o.client_id = pw.client_id
                and l.stoloc = pw.srcloc
                and o.btcust = @btcust
                and @+l.aisle_id
                and exists(select 1
                                 from pckmov pm
                                where pm.cmbcod = pw.cmbcod
                                  and pm.arecod in ('FSTG', 'SSTG', 'RSSTG', 'RPSTG')
                                  and pm.stoloc = @stgloc)
              group by pw.wrkref,
                    l.trvseq,
                    pw.srcloc,
                    pw.lblseq
              order by pw.lblseq,
                    l.trvseq,
                    pw.srcloc] catch(-1403)
        else
            [select pw.wrkref,
                    l.trvseq mytrvseq,
                    pw.srcloc mysrcloc,
                    pw.lblseq mylblseq,
                    sum((caslen*caswid*cashgt) *(pw.pckqty / pw.untcas)) shpvol
               from locmst l,
                    ord o,
                    shipment sh,
                    ftpmst f,
                    pckwrk pw
              where pw.ship_id = @ship_id
                and sh.ship_id = pw.ship_id
                and UPPER(sh.wave_set) not like '%ASTAGE%'
                and pw.schbat = @schbat
                and pw.lblbat = '------'
                and pw.wrktyp = 'P'
                and pw.lodlvl = 'S'
                and pw.pcksts = 'R'
                and pw.appqty != pw.pckqty
                and pw.srcloc like '%FLOOR%'
                and f.ftpcod = pw.ftpcod
                and o.ordnum = pw.ordnum
                and o.client_id = pw.client_id
                and l.stoloc = pw.srcloc
                and o.btcust = @btcust
                and @+l.aisle_id
                and exists(select 1
                                 from pckmov pm
                                where pm.cmbcod = pw.cmbcod
                                  and pm.arecod in ('FSTG', 'SSTG', 'RSSTG', 'RPSTG')
                                  and pm.stoloc = @stgloc)
              group by pw.wrkref,
                    l.trvseq,
                    pw.srcloc,
                    pw.lblseq
              order by pw.lblseq,
                    l.trvseq,
                    pw.srcloc] catch(-1403)
        |
        if (@? != 0)
        {
            set return status
             where status = 99400
        }
        |
        get session variable
         where name = 'CURRENT-VOLUME'
        |
        publish data
         where c_vol = @value
        |
        get session variable
         where name = 'PALLET-VOLUME'
        |
        publish data
         where p_vol = @value
        |
        get session variable
         where name = 'LABEL-BATCH'
        |
        publish data
         where lblbat_id = @value
        |
        [select @c_vol + @shpvol new_vol,
                case when (@c_vol + @shpvol) > @p_vol then 1
                     else 0
                end break_flg
           from dual]
        |
        if (@break_flg = 1)
        {
            generate next number
             where numcod = 'lodnum'
            |
            save session variable
             where name = 'LABEL-BATCH'
               and value = @nxtnum
            |
            publish data
             where lblbat_id = @nxtnum
            |
            get session variable
             where name = 'BATCH-LIST'
            |
            publish data
             where new_b_list = @value || ',' || @lblbat_id
            |
            save session variable
             where name = 'BATCH-LIST'
               and value = @new_b_list
            |
            save session variable
             where name = 'CURRENT-VOLUME'
               and value = @shpvol
        }
        else
        {
            save session variable
             where name = 'CURRENT-VOLUME'
               and value = @new_vol
        }
        |
        get session variable
         where name = 'LABEL-BATCH'
        |
        publish data
         where lblbat_id = @value
        |
        [update pckwrk
            set lblbat = @lblbat_id
          where wrkref = @wrkref]
        /* don't commit, it clears out all session variables */
    } >> myres_1
    |
    [select distinct 1
       from deferred_exec
      where exec_typ = 'LBLF'
        and exec_dte is null
        and (instr(deferred_cmd, @lbl_prtadr) > 0 or instr(deferred_cmd, @lblbat) > 0)] catch(-1403)
    |
    if (@? != 0)
    {
        get session variable
         where name = 'BATCH-LIST'
        |
        publish data
         where b_list = @value
           and srcloc = @stgloc
           and prtnum = 'ENHANCED'
        |
        [select 'print usr floorpick batch where lblbat = ' || '''' || @b_list || '''' || ' and btcust = ' || '''' || @btcust || '''' || ' and srcloc = ' || '''' || @srcloc || '''' || ' and prtnum = ' || '''' || @prtnum || '''' || ' and lngdsc = ' || '''' || @lngdsc || '''' || ' and ppw_prtadr = ' || '''' || @ppw_prtadr || '''' || ' and prtadr = ' || '''' || @lbl_prtadr || '''' my_deferred_command
           from dual]
        |
        generate next number
         where numcod = 'exec_id'
        |
        create deferred execution
         where exec_id = @nxtnum
           and exec_typ = 'LBLF'
           and command = @my_deferred_command catch(@?) >> my_def_exec
        |
        set return status
         where status = @?
    }
    else
    {
        set return status
         where status = 99450
    }
}
]]>
</local-syntax>
</command>
