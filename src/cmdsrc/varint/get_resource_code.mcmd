<command>
<name>get resource code</name>
<description>Get Resource Code</description>
<type>C Function</type>
<function>varGetResourceCode</function>
<argument name="arecod" alias="dstare" datatype="string">
</argument>
<argument name="restyp" datatype="string">
</argument>
<argument name="resval" datatype="string">
</argument>
<argument name="ship_id" datatype="string">
</argument>
<argument name="wkonum" datatype="string">
</argument>
<argument name="wkorev" datatype="string">
</argument>
<argument name="client_id" datatype="string">
</argument>
<argument name="wrkref" datatype="string">
</argument>
<documentation>
<remarks>
<![CDATA[
  <p>
   This command is used to determine what the resource code for
   a given area would be.  It looks at the ALLOCATE-INV policies
   to determine how the resource code is generated, and, based on
   the results, determines the resource code.
   This command may publish a resource code that is either system
   number generated, fixed, location assigned or derived.
   </p>
   <p>
   This command will accept either an area code or a resource type
   and resource value.  If the resource type and value are specified,
   then the function will used those values to generate the resource
   code, and will not need to look at the ALLOCATE-INV policies.  If
   those values are not supplied, then an area code must be passed to
   this component, and it will be determined from the ALLOCATE-INV policies
   as to how the resource code will be generated.
   </p>
   <p>
   In the case of a derived resource code, this component will first check
   to see if a value for the specified column name exists on the stack.  If
   so, that value will be used in an effort to reduce selects against the
   database.  In the case that the specified column does not exist on the
   stack, then some sort of identifier must be passed to this component
   from the remaining arguments, so the component knows which database
   tables to check when looking for the column value.
   </p>
]]>
</remarks>
<retcol name="rescod" type="COMTYP_CHAR">The resource code to be used for the specified area.</retcol>
<retcol name="restyp" type="COMTYP_CHAR">The type of resoure code used (as specified by the policy).</retcol>
<retcol name="resval" type="COMTYP_CHAR">The value, as specified by the policy,  associated with the
  resource code generation.  
  In the case of a derived resource code, this is generally the
  names of the columns to be used.</retcol>
<retcol name="stoloc" type="COMTYP_CHAR">The storage location (if assigned) for the area specified .</retcol>
<exception value="eOK">Normal successful completion</exception>
<exception value="eINT_BAD_RESOURCE_CODE">Unable to determine resource code from available data.</exception>
<exception value="eINT_NO_RESCOD_POLICY_EXISTS">Not policies exist for resource codes for specified area.</exception>
<seealso cref="allocate resource location">
</seealso>
</documentation>
</command>
