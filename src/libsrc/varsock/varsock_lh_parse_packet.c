static char RCS_Id[] = "$Id: varsock_lh_parse_packet.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varsock/varsock_lh_parse_packet.c,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: Simple component to test a VAR-level command.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

#include <moca.h>

#include <stdio.h>
#include <string.h>

#include <mocaerr.h>
#include <srvlib.h>
#include <applib.h>

#include <varsock_colwid.h>
#include <varsock_err.h>
#include <varsock_util.h>
#include <varsock_lh.h>

/* Manifest constants used in this module only */
/* names of the input parameters, used in validation of input */
static const char *const MESSAGE_STRING     = "message_string";
static const char *const MESSAGE_LENGTH     = "message_length";

/* columns names */
static const char *const READ_MORE   = "read_more";
static const char *const STATUS      = "status";
static const char *const DATA_STRING = "data_string";
static const char *const DATA_LENGTH = "data_length";
static const char *const ACK_STRING  = "ack_string";
static const char *const ACK_LENGTH  = "ack_length";
static const char *const NAK_STRING  = "nak_string";
static const char *const NAK_LENGTH  = "nak_length";

/* forward declaration of local functions. */
RETURN_STRUCT *sGetResult( RETURN_STRUCT *Result, long read_more, long status,
		const char *data_string, long data_length, const char *ack_string,
		long ack_length, const char *nak_string, long nak_length);
long sCheckMessageSyntax(const char *msg, long len, char *data_string, long *p_data_len, long *seq_number);
void sGetAckNakString(long seqence_number, char *ack_nak_string, long *p_ack_nal_string_len, char ack_nak);

/* global variables. */

/* static functions */

/* The main implementation */
LIBEXPORT 
RETURN_STRUCT *varsock_lh_parse_packet (
	char *message_string_i, /* The message packet that was send to host */
	long *message_length_i  /* The length of the message packet. This is to allow
							 for message having nulls get passed around. */
)
{
	/* locals for storing input variables */
	char message_string [ MESSAGE_STRING_LEN + 1 ];
	long message_length;

	/* The variables that contain tha return values. */
	long read_more = 1; /* by default assume that we do NOT have complete message. */
	long status = 0; /* by default assume that message does NOT have correct syntax. */
	char data_string [ DATA_STRING_LEN + 1 ];
	long data_length;
	char ack_string [ ACK_NAK_MSG_LEN + 1 ];
	long ack_length;
	char nak_string [ ACK_NAK_MSG_LEN + 1 ];
	long nak_length;
	long seq_num;

	/* scratch variables -- may get used in more than one context. */
	long retVal = !eOK;
	char buffer[ MAX_SOCKET_MSG_LEN + 1];

	/* Other variables */
	RETURN_STRUCT *Result = 0;
	const char *const fn = "varsock_lh_prase_packet";

	misTrc (T_FLOW, "%s - entered", fn);

	/* firewall the input arguments. */
	/* Even though we do NOT use the message_string we must check it to make sure
	 * that socket adapter code pass it in. */
	if (!message_string_i || !misTrimLen(message_string_i, MESSAGE_STRING_LEN)) {
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, MESSAGE_STRING);
		return APPMissingArg(MESSAGE_STRING);
	}
	else {
		ZAP(message_string);
		misTrimcpy(message_string, message_string_i, MESSAGE_STRING_LEN);
		misTrc(T_FLOW, "%s - Argument %s(%s) checks out fine.", fn, MESSAGE_STRING, message_string);
	}

	/* message_length parameter is required and it should be more than 0 */
	if (!message_length_i || !*message_length_i) {
		misTrc(T_FLOW, "%s - Required argument %s was not specified! Exiting...", fn, MESSAGE_LENGTH, message_length);
		return APPMissingArg(MESSAGE_STRING);
	}
	else {
		message_length = *message_length_i;
		misTrc(T_FLOW, "%s - checking is argument %s(%ld) is valid.", fn, MESSAGE_LENGTH, message_length);
		if (message_length <= 0) {
			ZAP(buffer);
			sprintf(buffer, "%s specified as %ld.  It must be more than 0.", MESSAGE_LENGTH, message_length);
			misTrc(T_FLOW, "%s - %s", buffer);
			return APPInvalidArg(MESSAGE_LENGTH, buffer);
		}
		misTrc(T_FLOW, "%s - argument %s(%ld) checks out fine!", fn, MESSAGE_LENGTH, message_length);
	}

	/* Log the value of input parameters */
	misTrc(T_FLOW, "%s - input values: %s(%s) %s(%ld)", fn,
			MESSAGE_STRING, message_string, MESSAGE_LENGTH, message_length);

	/* setup the return values. */
	read_more = 1;      /* by default assume that we do NOT have complete message. */
	status = 0;         /* by default assume that message does NOT have correct syntax. */
	ZAP( data_string ); /* zero the data_string. */
	data_length = 0;    /* hence, current length of the data_string is 0. */
	ZAP( ack_string );  /* zero the ack_string. */
	ack_length = 0;     /* hence, current length of the ack_string is 0. */
	ZAP( nak_string );  /* zero the nak_string. */
	nak_length = 0;     /* hence, current length of the nak_string is 0. */

	/* a complete message is received when we the last character in the packet is the <ETX> character. */
	if (message_string[message_length - 1] != ETX) {
		misTrc(T_FLOW, "%s - The last character of the message packet passed in is NOT <ETX>."
				"  Need to read more characters from the socket.", fn);
		read_more = 1;
		return sGetResult(Result, read_more, status, data_string, data_length,
				ack_string, ack_length, nak_string, nak_length);
	}

	misTrc(T_FLOW, "%s - the last character of the message string passed is <ETX>."
			"  We have received a complete message!", fn);
	read_more = 0;
	retVal = sCheckMessageSyntax(message_string, message_length, data_string, &data_length, &seq_num);
	if (retVal != eOK) {
		misTrc(T_FLOW, "%s - message does not seem to have correct syntax."
				" should return a NAK", fn);
		status = 0;
		/* we will need to build the nak_string */
		sGetAckNakString(seq_num, nak_string, &nak_length, NAK);
		return sGetResult(Result, read_more, status, data_string, data_length,
				ack_string, ack_length, nak_string, nak_length);
	}

	misTrc(T_FLOW, "%s - message seems to have correct syntax.", fn);
	status = 1;

	sGetAckNakString(seq_num, ack_string, &ack_length, ACK);
	sGetAckNakString(seq_num, nak_string, &nak_length, NAK);
	
	return sGetResult(Result, read_more, status, data_string, data_length,
			ack_string, ack_length, nak_string, ack_length);
}

/* this function takes the result values as input and sets up the results,
 * stuffs the result values in an returns the return struct back to caller. */
RETURN_STRUCT *sGetResult(
		RETURN_STRUCT *Result,
		long read_more,
		long status,
		const char *data_string,
		long data_length,
		const char *ack_string,
		long ack_length,
		const char *nak_string,
		long nak_length)
{
	const char *const fn = "varsock_lh_parse_packet::sGetResult";
	/* scratch varibles. */
	long retVal = !eOK;

	misTrc(T_FLOW, "%s - entered", fn);
	/* setup the column names in the results touple */
	Result = srvResultsInit(eOK,
			READ_MORE,   COMTYP_INT,  sizeof(long),
			STATUS,      COMTYP_INT,  sizeof(long),
			DATA_STRING, COMTYP_CHAR, DATA_STRING_LEN,
			DATA_LENGTH, COMTYP_INT,  sizeof(long),
			ACK_STRING,  COMTYP_CHAR, ACK_NAK_MSG_LEN,
			ACK_LENGTH,  COMTYP_INT,  sizeof(long),
			NAK_STRING,  COMTYP_CHAR, ACK_NAK_MSG_LEN,
			NAK_LENGTH,  COMTYP_INT,  sizeof(long),
			NULL);

	if (!Result) {
		/* srvResultInit failed! */
		misTrc(T_FLOW, "%s - Unexpected error! srcResultsInit() failed.  Exiting...", fn);

		/* return error to calling process. */
		return srvResults(eVAR_SRV_RESULTS_INIT_FAILED, NULL);
	}

	retVal = srvResultsAdd(Result, read_more, status, data_string, data_length,
			ack_string, ack_length, nak_string, nak_length);
	if (retVal != eOK) {
		misTrc(T_FLOW, "%s - unexpected error! srvResultsAdd returned error(%ld)!"
				"  Exiting...", fn, retVal);
		return srvResults(eVAR_SRV_RESULTS_ADD_FAILED, NULL);
	}

	misTrc(T_FLOW, "%s - sending back %s(%ld), %s(%ld), %s(%s), %s(%ld),"
			" %s(%s), %s(%ld) %s(%s) %s(%ld)", fn, READ_MORE, read_more, STATUS, status,
			DATA_STRING, data_string, DATA_LENGTH, data_length,
			ACK_STRING, ack_string, ACK_LENGTH, ack_length,
			NAK_STRING, nak_string, NAK_LENGTH, nak_length);

	return Result;
}

/* Takes the packet message string as an input and extracts the user data string and
 * message sequence number out of it.  The message sequence number is needed to build
 * the ack/nak packets to be sent back to the host.
 * If the message is not in a correct format, i.e.
 * - It does not start with <STX>
 * - it does not end with <ETX>
 * - the sequence number portion is not entirely composed of digits.
 * then the data_string is set to null string and seq_num is set to 0 and function
 * returns !eOK.  Also note that 0 is NOT a legal message sequence number. */
long sCheckMessageSyntax(const char *msg, long len, char *data_string, long *p_data_len, long *p_seq_num)
{
	const char *const fn = "varsock_lh_parse_packet::sCheckMessageSyntax";
	long retVal = !eOK;
	long sequence_number;
	/* used as a loop counter */
	int i;
	/* used to check if the sequence number part of the message is correctly formatted. */
	int seq_num_ok;
	char buffer[ MSG_SEQUENCE_LEN + 1];

	misTrc(T_FLOW, "%s - entered", fn);
	/* by default assume that the message syntax is screwed up.  The message format is
	 * so bad that it is not possible to ascertain what is the user data passed in the
	 * packet.  Similarly that we cannot ascertain the sequence number of the packet. */
	*data_string = 0;
	*p_data_len = 0;
	*p_seq_num= 0;

	/* the message format should be <STX>99999<variable-len-DATA-STRING><ETX> */
	/* Check if the first character is the STX */
	if (*msg != STX) {
		misTrc(T_FLOW, "%s - The first character of the message is NOT <STX>! Illegal Syntax.", fn);
		return !eOK;
	}

	/* Check if the last character is the ETX */
	if (ETX != msg[len - 1]) {
		misTrc(T_FLOW, "%s - last character of the message is NOT <ETX>! Illegal syntax.", fn);
		return !eOK;
	}

	/* the message coming in must have at least on user character long user data, i.e. the minumum
	 * packet length is 8 <STX>99999<at least one data char><ETX>. */
	if (len < MIN_MSG_LEN) {
		misTrc(T_FLOW, "%s - message length(%ld) is smaller than the minimum length"
				" of a valid message(%ld).  Incorrect syntax.  Return NAK.", fn, len, MIN_MSG_LEN);
		return !eOK;
	}
	/* check if the sequence number is indeed a number, i.e. it is composed entirly of digits. */
	/* copy the characters 2nd through 6th into the buffer, it is the sequence number. */
	strncpy(buffer, msg + 1, MSG_SEQUENCE_LEN);
	buffer[MSG_SEQUENCE_LEN] = 0;
	seq_num_ok = 1; /* assume that seq_num is correct. */
	for (i = 0; i < MSG_SEQUENCE_LEN && seq_num_ok; i++) {
		if (!isdigit(buffer[i])) {
			seq_num_ok = 0;
		}
	}

	if (!seq_num_ok) {
		misTrc(T_FLOW, "%s - characters other than digit [0-9] detected in the"
				" seqence number portion of the message.  Illegal syntax.", fn);
		return !eOK;
	}
				
	retVal = sscanf(buffer, "%ld", p_seq_num);
	if (retVal != 1) {
		misTrc(T_FLOW, "%s - some error entcountered while converting seqence number"
				" string to long using sscanf. Illegal syntax.", fn);
		return !eOK;
	}
	
	/* get the user data from the packet. */
	/* length of actual user data is the message_len minus the prefix and suffix len.
	 * Prefix is (1 char <STX> + MSG_SEQUENCE_LEN) and suffix is 1 char <ETX> */
	*p_data_len = len - 1 - MSG_SEQUENCE_LEN - 1;
	strncpy(data_string, msg + 1 + MSG_SEQUENCE_LEN, *p_data_len);
	/* for safety reasons, terminate the string. */
	data_string[*p_data_len] = 0;

	return eOK;
}

void sGetAckNakString(long seq_num, char *str, long *p_len, char ack_nak)
{
	long retVal = !eOK;
	const char *const fn = "varsock_lh_parse_packet::sGetAckNakString";

	/* the nak string format should be <ETX>99999<NAK><ETX> */
	/* if the syntax of the message is very bad, i.e. it has been corrupted during
	 * transmission then it is possible that we cannot ascertain the sequence number
	 * from the received packet.  In such cases the seq_num passed would be 0.  If so,
	 * then set it to a default value of 1. */
	if (seq_num == 0) {
		seq_num = 1;
	}
	retVal = sprintf(str, "%c%05ld%c%c", STX, seq_num, ack_nak, ETX);
	if (retVal != 8) {
		misTrc(T_FLOW, "%s - sprintf did not convert all the characters.  retVal(%ld)"
				" The nak string(%s) could be incorrect.  Return anyway!", fn, retVal, str);
	}
	/* return value of sprintf is the number of characters written, which is
	 * the lenght of the ack/nak packet. */
	*p_len = retVal;
}

