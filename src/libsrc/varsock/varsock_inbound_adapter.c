static char RCS_Id[] = "$Id: varsock_inbound_adapter.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varsock/varsock_inbound_adapter.c,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: VARSOCK-level component.  The inbound adapter code.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

/* We need the this define MOCA_ALL_SOURCE so that HP specific compile swithes
 * _XOPEN_SOURCE and _XOPEN_SOURCE_EXTENDED get defined.  Recent releases of HP
 * have chose to hide the prototypes of select(2) and fd_set behind the namespace
 * _XOPEN_SOURCE_EXTENDED. Use of MOCA_ALL_SOURCE not only defines these for 
 * this file but would reduce the porting pains. */

#define MOCA_ALL_SOURCE 
#include <moca.h>
#include <mocaerr.h>
#include <srvlib.h>
#include <oslib.h>
#include <applib.h>
#include <stdio.h>
#include <time.h>

#include <lib_include.h>

#ifdef UNIX
	#include <sys/socket.h>      /* SO_REUSEADDR used in osTCPListen() . */
#endif

#include <varsock_colwid.h>
#include <varsock_err.h>
#include <varsock_util.h>

/* Manifest constants used in this module only */
/* names of the input parameters, used in validation of input */
static const char *const SYS_ID           = "sys_id";
static const char *const PORTNUM          = "portnum";
static const char *const IDLE_TIMEOUT     = "idle_timeout";
static const char *const PARSE_PACKET_CMD = "parse_packet_cmd";
static const char *const MAX_PACKET_SIZE  = "max_packet_size";
static const char *const RUN_TRAN_FLG     = "run_tran_flg";
static const char *const TEST_MODE        = "test_mode";

/* these "#define" are used by sUpdateNfds() */
static const int ADDED = 1;
static const int REMOVED = 0;

/* forward declaration of local functions. */
/*
static long sFormatMsg(const char *cmd_name, const char *data, 
						char *message, long *min_response_len);
static int sIsItAck(const char *msg, const char *ack, const char *cmd);
*/

/* for closing the various sets of socket connections in event of an error. */
static void sCloseAllSockets(void);
static void sCloseListner(void);
static void sCloseAllAcceptedSockets(void);

static long sIncomingConnection(void);
static void sInitSocket(void);

/* used by the parse_packet_cmd to tell us if the packet had syntax errors or not! */
static const int ALL_OK = 1;
static const int NOT_OK = 0;

/* used to indicate if the transactions should be run right away or only id'ed */
static const char RUN_NOW   = 'T';
static const char RUN_LATER = 'F';

/* global variables. */
/* the socket which is in listen state. */
static SOCKET_FD listner;
/* structure to store the socket descriptors, data received over the socket,
 * ack/nak string to be send out, etc. of the accept'ed sockets. */
typedef struct fdlist_struct {
	int used;                                   /* is this slot in use or empty? */
	SOCKET_FD fd;                               /* value returned by osSockAccept. */
	char message[ MESSAGE_STRING_LEN + 1];         /* portion of message received till now. */
	long length;                               /* length of message recd so far. */
} FDLIST;

static FDLIST fdlist[ MAX_SOCKET_CONNECTIONS ];

/* used to set a slot in the fdlist array free. */
static const int CLOSE_SOCKET = 1;
static const int DONOT_CLOSE_SOCKET = 0;
static void sFreeSlot(FDLIST *fdlist, int close_socket);
static void sUseSlot(FDLIST *fdlist, SOCKET_FD new_fd);
static long sNewDataAvailable(FDLIST *fdlist, const char *parse_packet_cmd);

/* value of the highest socket escriptor that select should wait on for activity. */
static int nfds;
/* function that is used to keep this value updated. */
static void sUpdateNfds(SOCKET_FD lisnter, int);

/* 3 masks used to do select on connected sockets. */
fd_set read_mask;
fd_set write_mask;
fd_set err_mask;
/* functions that would be used to manipulate these masks. */
static void sGetSelectReadMask(fd_set *mask);
static void sGetSelectWriteMask(fd_set *mask);
static void sGetSelectErrMask(fd_set *mask);
static long sIncomingData(const fd_set *read_mask, const char *parse_packet_cmd);
static long sParseIncomingMessage(const char *cmd, const char *message, long len,
			int *p_read_more, int *p_status, char *data, long *p_data_length,
			char *ack_str, long *p_ack_length, char *nak_str, long *p_nak_length);
static long sProcessData(const char *data, long data_length);

/* SEAMLES - start */
/* Function that had to added for seamles related processing. */
static void Cleanup (void);

/* variables that had to made global to this file because some of the processing was done by other funcitons. */
slIFD_DwnldCtxt_Typ      DwnldCtxt;
char                     dwnld_name[128];
/* related to setting up the download and commit contexts. */
slObjDwnld_Dwnld_Typ     Dwnld;
slObjDwnld_DwnldPK_Typ   DwnldPK;
double                   oldCommitCtxtIdSeq;
slInAdapter_Def_Typ      InAdapter;
int                      run_now;
/* SEAMLES - end */

long test_mode;
/* The main implementation */
LIBEXPORT 
RETURN_STRUCT *varsock_inbound_adapter (
	char *sys_id_i,                /* name of the system for which this command is being run.
									* No checks are being made for this.  It passed by seamles */
	long *portnum_i,               /* Port number to connect to */
	long *idle_timeout_i,          /* number of seconds inactivity on the socket
							    	* after which the connection will be dropped */
	char *parse_packet_cmd_i,      /* name of the MOCA component to be used to
				                    * ascertain if a complete packet has been recevied */
	long *max_packet_size_i,       /* Maximum size of the message packet expected. */
	char *run_tran_flg_i,          /* Run transactions as they are received ? T/F */
	long *test_mode_i              /* Run the adapter in test mode, i.e. it would just
									  do the part realted to socket communication. */
)
{
	/* locals for storing input variables */
	long portnum;
	long idle_timeout;
	char parse_packet_cmd [ COMMAND_LEN + 1 ];
	char run_tran_flg[ RUN_TRAN_FLG_LEN + 1 ];
	long max_packet_size;

	/* scratch variables -- may get used in more than one context. */
	char buffer[1024];
	long retVal = !eOK;

	/* Other variables */
	int done = FALSE;
	/* the time structure used in select */
	struct timeval timeout;

	const char *const fn = "varsock_inbound_adapter";

	/* SEAMLES - start */
	/* variable related to SeamLES */
	sl_Status_Typ            stat = SL_STAT_OK;
	RETURN_STRUCT*           rp = 0;
	long                     argc = 0;
	char**                   argv = 0;
	long                     ii;

	char                     buf[50];
	/* SEAMLES - end */

	misTrc (T_FLOW, "%s - entered", fn);

	/* firewall the input arguments. */
	/* check if the test_mode flag has been specified. */
	if (!test_mode_i || !*test_mode_i) {
		misTrc(T_FLOW, "%s - parameter %s was not specified.  OK! it is an optional paramter.", fn, TEST_MODE);
		test_mode = 0;
	}
	else {
		test_mode = *test_mode_i;
		misTrc(T_FLOW, "%s - checking to see if argument %s(%ld) is correct.", fn, TEST_MODE, test_mode);
		if (test_mode != 0 && test_mode != 1) {
			ZAP(buffer);
			sprintf(buffer, "Invalid value specified for %s(%ld).  The valid values are 0 or 1",
					TEST_MODE, test_mode);
			misTrc(T_FLOW, "%s - %s", fn, buffer);
			return APPInvalidArg(TEST_MODE, buffer);
		}
	}
	if (test_mode) {
		misTrc(T_FLOW, "%s - %s(%ld) adapter is running in test mode.  It will receive and ack the messages."
				" But it will not attempt to ID the message with SeamLES.", fn, TEST_MODE, test_mode);
	}
	else {
		misTrc(T_FLOW, "%s - %s(%ld) adapater is NOT in test mode.  Recevied message will be ided with SeamLES.",
				fn, TEST_MODE, test_mode);
	}

	/* sys_id is required.  This argument is not passed in by the user, but it gets passed in by the seamles
	 * to the command.  Still we need to check for it.  But if adapter is in test mode this is not needed! */
	if (test_mode) {
		*sys_id_i = 0;
	}
	else {
		if (!sys_id_i) {
			misTrc(T_FLOW, "%s - required argument %s not specified. Exiting...", fn, SYS_ID);
			return APPMissingArg(SYS_ID);
		}
		else {
			misTrc(T_FLOW, "%s - checking if argument %s(%s) is valid", fn, SYS_ID, sys_id_i);
			if (strlen(misTrimLR(sys_id_i)) == 0) {
				ZAP(buffer);
				sprintf(buffer, "%s specified as %s.  It should not be null string.", SYS_ID, sys_id_i);
				misTrc(T_FLOW, "%s - %s", fn, buffer);
				return APPInvalidArg(SYS_ID, buffer);
			}
			misTrc(T_FLOW, "%s - argument %s(%s) checks out fine.", fn, SYS_ID, sys_id_i);
		}
	}

	/* portnum is required. */
	if (!portnum_i || !*portnum_i) {
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, PORTNUM);
		return APPMissingArg(PORTNUM);
	}
	else {
		portnum = *portnum_i;
		misTrc(T_FLOW, "%s - argument %s(%ld) checks out fine.", fn, PORTNUM, portnum);
	}

	/* idle_timeout is required!  In principle we could assume an arbitary value for
	 * this timer, but for now we force user to specify it. Further, we disallow a
	 * value of 0 as that would cause select to block indefinitly and the while loop
	 * would become a CPU hog. */
	if (!idle_timeout_i) {
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, IDLE_TIMEOUT);
		return APPMissingArg(IDLE_TIMEOUT);
	}
	else {
		idle_timeout = *idle_timeout_i;
		misTrc(T_FLOW, "%s - Checking if argument %s(%ld) is valid.", fn, IDLE_TIMEOUT, idle_timeout);

		if (idle_timeout <= 0) {
			ZAP(buffer);
			sprintf(buffer, "%s specified as %ld. It should be more than 0", IDLE_TIMEOUT, idle_timeout);
			misTrc (T_FLOW, "%s - %s", fn, buffer);

			return APPInvalidArg(IDLE_TIMEOUT, buffer);
		}
		misTrc(T_FLOW, "%s - Argument %s(%ld) checks out fine.", fn, IDLE_TIMEOUT, idle_timeout);
	}
	
	/* parse_packet_cmd is required. */
	if (!parse_packet_cmd_i || !misTrimLen(parse_packet_cmd_i, COMMAND_LEN)) {
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, PARSE_PACKET_CMD);
		return APPMissingArg(PARSE_PACKET_CMD);
	}
	else {
		ZAP( parse_packet_cmd );
		misTrimcpy(parse_packet_cmd, parse_packet_cmd_i, COMMAND_LEN);

		misTrc(T_FLOW, "%s - Argument %s(%s) checks out fine.", fn, PARSE_PACKET_CMD, parse_packet_cmd);
	}

	/* max packet size is required.  It decides how be allocate the various buffers etc.
	 * Its value should be more than 0.  Currently the code does NOT use this value!! */
	if (!max_packet_size_i) {
		misTrc(T_FLOW, "%s - required argument %s was not specified!  Exiting...", fn, MAX_PACKET_SIZE);
		return APPMissingArg(MAX_PACKET_SIZE);
	}
	else {
		max_packet_size = *max_packet_size_i;
		misTrc(T_FLOW, "%s - Checking if argument %s(%ld) is valid.", fn, MAX_PACKET_SIZE, max_packet_size);

		if (max_packet_size <= 0) {
			ZAP(buffer);
			sprintf(buffer, "%s specified as %ld. It should be more than 0.",
					MAX_PACKET_SIZE, max_packet_size);
			misTrc(T_FLOW, "%s - %s", fn, buffer);

			return APPInvalidArg(MAX_PACKET_SIZE, buffer);
		}
		misTrc(T_FLOW, "%s - Argument %s(%ld) checks out fine.", fn, MAX_PACKET_SIZE, max_packet_size);
	}

	/* run_tran_flg indicates if the transactions received should be run immediately?  This is required flag.
	 * The valid values are 'T' or 'F'.  This flag is not required if running in test mode */
	if (test_mode) {
		ZAP(run_tran_flg);
	}
	else {
		if (!run_tran_flg_i || !misTrimLen(run_tran_flg_i, RUN_TRAN_FLG_LEN)) {
			misTrc(T_FLOW, "%s - required argument %s was not specified!  Exiting...", fn, RUN_TRAN_FLG);
			return APPMissingArg(RUN_TRAN_FLG);
		}
		else {
			ZAP(run_tran_flg);
			misTrimcpy(run_tran_flg, run_tran_flg_i, RUN_TRAN_FLG_LEN);
			misTrc(T_FLOW, "%s - checking to see if argument %s(%s) is valid.", fn, RUN_TRAN_FLG, run_tran_flg);

			if (*run_tran_flg == RUN_NOW || *run_tran_flg == RUN_LATER) {
				misTrc(T_FLOW, "%s - Argument %s(%s) checks out fine.", fn, RUN_TRAN_FLG, run_tran_flg);
				if (*run_tran_flg == RUN_NOW) {
					run_now = 1;
				}
				else {
					run_now = 0;
				}
				misTrc(T_FLOW, "%s - setting run_now flag to %i", fn, run_now);
			}
			else {
				ZAP(buffer);
				sprintf(buffer, "%s specified as %s it should be either (%c) or (%c)", RUN_TRAN_FLG,
						run_tran_flg, RUN_NOW, RUN_LATER);
				misTrc(T_FLOW, "%s - %s", fn, buffer);
				return APPInvalidArg(RUN_TRAN_FLG, buffer);
			}
		}
	}

	misTrc(T_FLOW, "%s - input parameters: %s(%ld), %s(%ld), %s(%s) %s(%ld) %s(%s) %s(%ld)", fn,
			PORTNUM, portnum, IDLE_TIMEOUT, idle_timeout, PARSE_PACKET_CMD, parse_packet_cmd_i,
			MAX_PACKET_SIZE, max_packet_size, RUN_TRAN_FLG, run_tran_flg, TEST_MODE, test_mode);

	/* initilize the structure that would be used by the socket */
	sInitSocket();

	/* first let us setup the listner. */
	misTrc(T_FLOW, "%s - Setting up the listner", fn);
	retVal = osTCPListen(&listner, (short)portnum, MAX_SOCKET_CONNECTIONS, SO_REUSEADDR);
	if (retVal != eOK) {
		/* The listner could not be setup!  Fetal error.  Cannot recover. */

		misTrc(T_FLOW, "%s - osTCPListen: Cannot setup listner: "
			"errno(%ld), error(%s).", fn, osSockErrno(), osSockError());
		misTrc(T_FLOW, "%s - Fetal error! Cannot continue. Exiting", fn);
		return srvResults(eVAR_SOCK_LISTEN_FAILED, 0);
	}

	misTrc(T_FLOW, "%s - Socket listner sucessfully setup. listenr fd(%i).", fn, sGetSockFd(listner));

	/* update the nfds variable */
	sUpdateNfds(listner, ADDED);

	/* SEAMLES - start */
	if (!test_mode) {
		extern int osOpterr;
		extern int osOptind;
		misTrc(T_FLOW, "%s - do seamles related startup processing. Calling sl_Init()", fn);
		sl_Init ();

		misTrc(T_FLOW, "%s - Calling srvRequestKeepalive()", fn);
		srvRequestKeepalive("SL_PROC_INB_SOCKET", Cleanup );

		/* ZAP the variables. */
		memset ( &InAdapter, '\0', sizeof(slInAdapter_Def_Typ) );

		/* setup the agruments */
		if ( !(argv=calloc(20, sizeof(char*) )) )
		{
			stat = SL_STAT_NO_MEMORY;
			misTrc(T_FLOW, "%s - calloc for argv failed.", fn);
			goto end_of_function;
		}
		InAdapter.timer = slTools_StartTimer();

		/* First arg is "" */
		misDynStrcpy ( &argv[argc++], "IFDProcFile" );

		/* sys id */
		sprintf ( buf, "%s", sys_id_i );
		misDynStrcpy ( &argv[argc++], "-s" );
		misDynStrcpy ( &argv[argc++], buf );

		/* can't write to stdout or stderr, so quiet mode */
		misDynStrcpy ( &argv[argc++], "-q" );

		if ( run_tran_flg && run_tran_flg[0] == 'T' ) {
			misDynStrcpy ( &argv[argc++], "-r" );
		}

		/* Simulate one more argv for file, table kind of options */
		misDynStrcpy ( &argv[argc++], "xxx" );

	#ifdef UNIX
		SLLOG_TRC (( SLLOG_TRC_COMM, SLLOG_TRC_USE_DEFAULT_TAG, "Pseudo-Command line count = %i", argc ));
		for ( ii = 0; ii < argc; ii++ ) {
			SLLOG_TRC (( SLLOG_TRC_COMM, SLLOG_TRC_USE_DEFAULT_TAG, "   %s", argv[ii] ))
		}
	#endif

		InAdapter.slArgs.argc = argc;
		InAdapter.slArgs.argv = argv;
		misTrc(T_FLOW, "%s - (re)Initialize the osGetopts() function.", fn);
		osOpterr = 0;  /* we do not want anything going to the stderr */
		osOptind = 1;  /* start processing new list of arguments. */

		misTrc(T_FLOW, "%s - Calling slInAdapter_Args_Process()", fn);
		stat = slInAdapter_Args_Process(&InAdapter.slArgs);

		if ( stat != SL_STAT_OK ) {
			misTrc(T_FLOW, "%s - slInAdapter_Args_Process() returned error (%ld)", fn, stat);
			goto end_of_function;
		}


		misTrc(T_FLOW, "%s - Do generic Setup of the Inbound Adapter.  Calling slInAdapter_Setup()", fn);
		stat = slInAdapter_Setup(&InAdapter);
		if ( stat != SL_STAT_OK ) {
			misTrc(T_FLOW, "%s - slInAdapter_Setup() returned error (%ld)", fn, stat);
			goto end_of_function;
		}

		/* this is the name of the context used in context related seamles calls.  dwnld_name is a global vatiable. */
		sprintf(dwnld_name, "System-%s Port-%ld", sys_id_i, portnum);
	}
	/* SEAMLES - end */

	/* now go do a select on this socket and all other sockets out there. */
	while (!done) {
		/* first get the masks to be used with select. */
		sGetSelectReadMask(&read_mask);
		sGetSelectWriteMask(&write_mask); /* currently we are NOT using the writemask */
		sGetSelectErrMask(&err_mask);
		
		/* fill up the timeval struct. */
		timeout.tv_sec = idle_timeout;
		timeout.tv_usec = 0;

		/* select will block for idle_timeout seconds.  If there is not action for
		 * idle_timeout seconds then we will drop all accept'ed socket connections! */
		misTrc(T_FLOW, ""); /* put a blank line before every iteration of the while loop. */
		misTrc(T_FLOW, "%s - going into select()", fn);
		retVal = select(nfds, &read_mask, 0, 0, &timeout);
		misTrc(T_FLOW, "%s - select() returned(%ld)", fn, retVal);

		switch (retVal) {
			case -1:
				misTrc(T_FLOW, "%s - select returned an error!  Fatel error, cannot recover", fn);
				done = TRUE;
				stat = eVAR_SOCK_SELECT_FAILED;
				break;

			case 0:
				misTrc(T_FLOW, "%s - select() timed out, i.e. we waited for (%ld) seconds but there was no"
						" activity on the listner or on any of the connected sockets!", fn, idle_timeout);
				misTrc(T_FLOW, "%s - We should close all the accept'ed socket connections", fn);
				sCloseAllAcceptedSockets();
				break;
				
			default:
				misTrc(T_FLOW, "%s - select() detected some activity. Do we have a new connection coming in?", fn);
				if (FD_ISSET(sGetSockFd(listner), &read_mask)) {
					misTrc(T_FLOW, "%s - we have a new incoming connection!", fn);
					if (sIncomingConnection() != eOK) {
						misTrc(T_FLOW, "%s - some error encountered while accecpting the new incoming connection.", fn);
						done = TRUE;
						stat = eVAR_SOCK_ACCEPT_FAILED;
					}
				}
				else {
					misTrc(T_FLOW, "%s - we have data coming in on one of the connected sockets.", fn);
					if (sIncomingData(&read_mask, parse_packet_cmd) != eOK) {
						misTrc(T_FLOW, "%s - some error encountered while reading data", fn);
						done = TRUE;
						stat = eVAR_INCOMING_DATA_PROCESSING_ERROR;
					}
				}
				break;
		}
	}

end_of_function:
	sCloseAllSockets();

	/* SEAMLES - start */
	misTrc(T_FLOW, "%s - in end_of_function with stat(%ld)", fn, stat);
	if (!test_mode) {
		misTrc(T_FLOW, "%s - release the keep alive flag for this function.", fn);
		srvReleaseKeepalive ( "SL_PROC_INB_SOCKET" );

		if ( argv ) {
			for ( ii = 0; ii < argc; ii++ ) {
				SL_FREE ( argv[ii] );
			}
			SL_FREE ( argv );
		}
	}

	misTrc(T_FLOW, "%s - return error to caller via slServer_ReturnError()", fn);
	rp = slServer_ReturnError ( stat );
	/* SEAMLES - end */
	return rp;
}

static void Cleanup ( void )
{
	const char *const fn = "varsock_inbound_adapter::Cleanup";
	misTrc(T_FLOW, "%s - entered", fn);
	sCloseAllSockets();
	misTrc(T_FLOW, "%s - exiting", fn);
}


/* The function takes the command to be used to parse the message packet,
 * and the message itself.  It returns the following:
 * read_more: if one complete message has been received or more has to be read, 
 * status: If a complete message has been recd, is it of correct syntax?
 * data: the user data in the message packet stripped off of the protocol fluff
 * ack_str: The ack message that should be sent back
 * nak_str: The nakc message that should be sent back.
 */
static long sParseIncomingMessage(const char *cmd, const char *message, long len,
			int *p_read_more, int *p_status, char *data, long *p_data_len, 
			char *ack_str, long *p_ack_len, char *nak_str, long *p_nak_len)
{
	const char *const fn = "varsock_inbound_adapter::sParseIncomingMessage";
	/* Buffer has to store the data length in it and has some other stuff. */
	char buffer[MESSAGE_STRING_LEN + 200];
	long retVal = !eOK;
	/* buffer to store the edited message string which can be passed on to the server command. */
	char message_cpy[ MESSAGE_STRING_LEN + 1 ];
	/* variables for making server call */
	RETURN_STRUCT *StructPtr = 0;
	mocaDataRes *Res = 0;
	mocaDataRow *Row = 0;

	misTrc(T_FLOW, "%s - entered.", fn);

	/* MOCA does NOT allow passing around string contaning embedded NULLs.  Hence we must replace
	 * any NULLs that might have been part of the bytes read off of the socket; or else we will
	 * never be able to pass the entire message to the server component. */
	ZAP ( message_cpy );
	memcpy(message_cpy, message, len);
	sprintf(buffer, "%s where message_string = '%s' and message_length = '%ld'",
			cmd, sMaskNullsInString(message_cpy, len), len);

	retVal = srvInitiateCommand(buffer, &StructPtr);
	if (retVal != eOK)
	{
		misTrc(T_FLOW, "%s - Error executing command.  Returning false.", fn);
		return retVal; /* this is !eOK, same as what was returned by srvInitiateCommand */
	}

	misTrc(T_FLOW, "%s - command execution was sucessful, retrieve return values.", fn);
	Res = StructPtr->ReturnedData;
	Row = sqlGetRow(Res);

	*p_read_more = sqlGetLong(Res, Row, "read_more");
	*p_status = sqlGetLong(Res, Row, "status");

	memset(data, DATA_STRING_LEN + 1, 0);
	misTrimcpy(data, sqlGetString(Res, Row, "data_string"), DATA_STRING_LEN);
	*p_data_len = sqlGetLong(Res, Row, "data_length");

	memset(ack_str, RESPONSE_STRING_LEN + 1, 0);
	misTrimcpy(ack_str, sqlGetString(Res, Row, "ack_string"), RESPONSE_STRING_LEN);
	*p_ack_len = sqlGetLong(Res, Row, "ack_length");

	memset(nak_str, RESPONSE_STRING_LEN + 1, 0);
	misTrimcpy(nak_str, sqlGetString(Res, Row, "nak_string"), RESPONSE_STRING_LEN);
	*p_nak_len = sqlGetLong(Res, Row, "nak_length");

	/* free the memory held by ret struct */
	srvFreeMemory(SRVRET_STRUCT, StructPtr);
	StructPtr = 0;
	Res = 0;

	misTrc(T_FLOW, "%s - returning: read_more(%i), status(%i), data(%s), data_len(%ld),"
			" ack(%s), ack_len(%ld), nak(%s), nak_len(%ld)", fn, *p_read_more, *p_status, data,
			*p_data_len, ack_str, *p_ack_len, nak_str, *p_nak_len);
	return retVal; /* this must be eOK, same as the return value of srvInitiateCommand. */
}

/* these functions take the mask, listner FD and list of connected client FDs */
static void sGetSelectReadMask(fd_set *p_read_mask)
{
	const char *const fn = "varsock_inbound_adapter::sGetSelectReadMask";
	int i; /* scratch variable used for loop control. */
	/* zap the mask */
	FD_ZERO(p_read_mask);

	misTrc(T_FLOW, "%s - entered.", fn);
	/* we want to track data coming on listner. */
	FD_SET((unsigned int)sGetSockFd(listner), p_read_mask);
	misTrc(T_FLOW, "%s - added listner socket (%i) to the read mask.", fn, sGetSockFd(listner));

	/* we also want to track data coming in on the list of all connected sockets. */
	for (i = 0; i < MAX_SOCKET_CONNECTIONS; i++) {
		/* fd that is set to -1 is not being used yet. so skip it */
		if (fdlist[i].used == TRUE) {
			/* get the int socket descriptor from the SOCKET_FD type */
			int n = sGetSockFd(fdlist[i].fd);
			FD_SET((unsigned int)n, p_read_mask);
			misTrc(T_FLOW, "%s - added a connected socket(%i) to the read mask.", fn, n);
		}
	}
}

static void sGetSelectWriteMask(fd_set *p_write_mask)
{
	FD_ZERO(p_write_mask);
}
static void sGetSelectErrMask(fd_set *p_err_mask)
{
	FD_ZERO(p_err_mask);
}

static void sInitSocket(void)
{
	int i; /* scratch variable used for loop control. */
	/* let us setup the structure that would hold the connected discriptors. */
	/* most of the structures are static so we need not do this initilization. 
	 * But it is being done anyway to document the assumptions being made. */
	const char *const fn = "varsock_inbound_adapter::sInitSocket";
	misTrc(T_FLOW, "%s - eneted. sizeof(fdlist) is %i", fn, MAX_SOCKET_CONNECTIONS);

	for (i = 0; i < MAX_SOCKET_CONNECTIONS; i++) {
		/* Since -1 can never be a legal socket descriptor we use it as a sentinal
		 * to indicate that the perticular cell in the array is empty. */
		sFreeSlot(fdlist + i, DONOT_CLOSE_SOCKET);
	}

	/* set the value of the largest socket fd that we wnat to listen for to 0. */
	nfds = 0;
}

/* this function keeps the value of the global variable nfds upto date.
 * nfds contains the interger corresponding to the largest socket file descriptor open.
 * It is needed in a call to select. The function should be called AFTER the fdlist has
 * been polulated correctly, i.e. the used fields has been propperly updated, to reflect
 * what slots are in use and what are not in use. */
static void sUpdateNfds(SOCKET_FD fd, int action)
{
	const char *const fn = "varsock_inbound_adapter::sUpdateNfds";
	int i; /* scratch variable used for loop control, etc. */
	/* get the int socket descriptor from the SOCKET_FD type */
	int n = sGetSockFd(fd);
	/* we are adding 1 to the socket descriptor because The select() function
	 * tests file descriptors in the range of 0 to nfds -1. */
	int try_nfds = n + 1;

	misTrc(T_FLOW, "%s - input parameters: socket fd(%i), action(%i)", fn, n, action);
	misTrc(T_FLOW, "%s - current value of the largest fd (%i)", fn, nfds);

	if (action == ADDED) {
		/* we are adding a new socket.  If this is larger than the current largest
		 * ONLY then we need to use it, else we can ignore it. */
		if (try_nfds > nfds) {
			nfds = try_nfds;
			misTrc(T_FLOW, "%s - the socket being added(%i), try_nfds(%i) is the new"
					" largest fd. New value of nfds (%i)", fn, n, try_nfds, nfds);
		}
		else {
			misTrc(T_FLOW, "%s - the socket being added (%i), try_nfds (%i) is less"
					" than or equal to current value of nfds (%i)."
					" Leaving nfds unchanged.", fn, n, try_nfds, nfds);
		}
	}
	else if (action == REMOVED) {
		/* find of this was the largest socket that we were listneing for? 
		 * because only if this was THE largest, then we need to find the new
		 * largest fd, else we can simply ignore the removal of this socket fd. */
		if (nfds == try_nfds) {
			/* This was the largest socket fd that we were listening for.
			 * So now we would have to find the new largest socket fd. */
			/* start with assuming that lisnter is the largest fd. Don't add 1 yet! */
			nfds = sGetSockFd(listner);
			for (i = 0; i < MAX_SOCKET_CONNECTIONS; i++) {
				if (fdlist[i].used) {
					int p = sGetSockFd(fdlist[i].fd);
					if (p > nfds) {
						/* we are adding 1 to the socket descriptor because The
						 * select() function tests file descriptors in the range
						 * of 0 to nfds -1. */
						nfds = p;
					}
				}
			}
			nfds += 1; /* set nfds to 1 + the max fd. */
			misTrc(T_FLOW, "%s - Socket being removed (%i), try_nfds(%i) was the largest"
					" socket fd. New value of nfds(%i)", fn, n, try_nfds, nfds);
		}
		else {
			misTrc(T_FLOW, "%s - Socket being removed(%i), try_nfds(%i) is less than"
					" the current nfds. Leaving nfds (%i) unchanged.", fn, n, try_nfds, nfds);
		}
	}
	else {
		/* and illegal value was specified in action. */
		misTrc(T_FLOW, "%s - illegal value specified for action(%i).", fn, action);
	}
}

static void sCloseAllAcceptedSockets(void)
{
	const char *const fn = "varsock_inbound_adapter::sCloseAllAcceptedSockets";
	int i; /* used as a loop counter */
	misTrc(T_FLOW, "%s - closing all the accept'ed socket connections.", fn);

	/* close all connected socketed */
	for (i = 0; i < MAX_SOCKET_CONNECTIONS; i++) {
		if (fdlist[i].used == TRUE) {
			/* currently we ignore the return value of sClose. */
			sFreeSlot(fdlist + i, CLOSE_SOCKET);
			misTrc(T_FLOW, "%s - closed socket fd(%i)", fn, sGetSockFd(fdlist[i].fd));
		}
	}
}

static void sCloseListner(void)
{
	const char *const fn = "varsock_inbound_adapter::sCloseListner";
	misTrc(T_FLOW, "%s - closing the listner socket(%i) .", fn, sGetSockFd(listner));

	/* currently we ignore the return value of sClose. */
	sClose(listner);
	misTrc(T_FLOW, "%s - closed socket fd(%i)", fn, sGetSockFd(listner));
	/* in practice we need not update nfds after the listner shuts down as we would most
	 * likely exit the function after this.  But we do it anyway. */
	sUpdateNfds(listner, REMOVED);
}

static void sCloseAllSockets(void)
{
	const char *const fn = "varsock_inbound_adapter::sCloseAllSockets";

	misTrc(T_FLOW, "%s - entered", fn);

	sCloseListner();
	sCloseAllAcceptedSockets();
}

/* this function is called when select indicates that a new incoming connection is
 * pending at the listner socket.  It returns the return status of accept.  In case
 * we can sucessfully accept a connection we cannot hold on to the connection (as
 * we have reached the limit of maximum open connection we can service) still we would
 * return sucess. */
static long sIncomingConnection(void)
{
	const char *const fn = "varsock_inbound_adapter::sIncomingConnection";
	/* the index into the fdlist array where the new socket would be stored. */
	int index;
	/* scratch variable used for loop control. */
	int i;
	/* to store the retVal of the incoming connection */
	int retVal = !eOK;
	/* temporary variable to hold the descriptor of the newly accepted socket. */
	SOCKET_FD temp_fd;

	misTrc(T_FLOW, "%s - entered", fn);

	retVal = sAccept(listner, &temp_fd);
	if(retVal != eOK) {
		/* Fetal error cannot recover! */
		misTrc(T_FLOW, "%s - some problem with accept.  Returning error (!eOK) to the caller", fn);
		/* retVal contains the error code returned by sAccpet.  Returning error here would cause adapter to exit. */
		return retVal;
	}

	misTrc(T_FLOW, "%s - Sucessfully accecpted a new socket connection fd(%i)", fn, sGetSockFd(temp_fd));
	/* we have a new connection coming in! check if we can find an empty slot
	 * for a new connection coming in? */
	for (i = 0, index = -1; i < MAX_SOCKET_CONNECTIONS; i++) {
		if (fdlist[i].used == FALSE) {
			/* found an empty slot! */
			index = i;
			misTrc(T_FLOW, "%s - found an empty slot in fdlist array at %i", fn, index);
			break;
		}
	}

	if (index < 0) {
		misTrc(T_FLOW, "%s - There is not enough space to take on any more connections!"
			"we would have close it immediately", fn);
		sClose(temp_fd); /* we are ignoring the return value of sClose() */
		misTrc(T_FLOW, "%s - closed the newly accepted socket connection (%i)", fn, sGetSockFd(temp_fd));
		/* return success to the caller, i.e. return value of sAccept.  Even though we
		 * accpeted and immediately closed the connection, we must return success */
		return retVal;
	}

	/* we have got an empty slot for this new connection. */
	sUseSlot(fdlist + index, temp_fd);

	/* retVal contains the return code from sAccept which would indicate sucess */
	return retVal;
}

/* this would update the value of nfds. */
static void sFreeSlot(FDLIST *p, int close_socket)
{
	const char *const fn = "varsock_inbound_adapter::sFreeSlot";
	misTrc(T_FLOW, "%s - entered: fd (%i), close socket?(%i)", fn, sGetSockFd(p->fd), close_socket);

	p->used = FALSE;
	memset(p->message, MESSAGE_STRING_LEN, 0);
	p->length = 0;
	if (close_socket == CLOSE_SOCKET) {
		sClose(p->fd);
		sUpdateNfds(p->fd, REMOVED);
	}
}

/* this would update the value of nfds. */
static void sUseSlot(FDLIST *p, SOCKET_FD new_fd)
{
	const char *const fn = "varsock_inbound_adapter::sUseSlot";
	misTrc(T_FLOW, "%s - entered: new_fd (%i)", fn, sGetSockFd(new_fd));

	p->used = TRUE;
	p->fd = new_fd;
	memset(p->message, MESSAGE_STRING_LEN, 0);
	p->length = 0;
	sUpdateNfds(new_fd, ADDED);
}

static long sNewDataAvailable(FDLIST *p, const char *parse_packet_cmd)
{
	const char *const fn = "varsock_inbound_adapter::sNewDataAvailable";
	int read_more = 1;
	int status = NOT_OK;
	char ack_str[ RESPONSE_STRING_LEN + 1 ];
	long ack_length;
	char nak_str[ RESPONSE_STRING_LEN + 1 ];
	long nak_length;
	char data [ DATA_STRING_LEN ];
	long data_length;
	long retVal = !eOK;

	misTrc(T_FLOW, "%s - entered: socket(%i), message(%s), length(%ld)", fn,
			sGetSockFd(p->fd), p->message, p->length);

	retVal = sParseIncomingMessage(parse_packet_cmd, p->message, p->length,
			&read_more, &status, data, &data_length, ack_str, &ack_length, nak_str, &nak_length);

	if (retVal != eOK) {
		misTrc(T_FLOW, "%s - error encounterd executing command (%s).  Cannot recover,"
				" closing all sockets.", fn, parse_packet_cmd);
		/* This is serious error!  We cannot continue processing.  Return error to caller. Returning error here 
		 * would cause the adapter to exit! */
		return !eOK;
	}
	misTrc(T_FLOW, "%s - sParseIncomingMessage return values: read_more(%i), status(%i), data(%s), data_len(%ld),"
			" ack(%s), ack_len(%ld), nak(%s), nak_len(%ld)", fn, read_more, status, data,
			data_length, ack_str, ack_length, nak_str, nak_length);

	misTrc(T_FLOW, "%s - parse packet cmd(%s) executed sucessfully.", fn, parse_packet_cmd);
	if (read_more) {
		misTrc(T_FLOW, "%s - we do not have a full message(%s) yet!", fn, p->message);
		/* This is a valid condition, return success to caller. */
		return eOK;
	}

	misTrc(T_FLOW, "%s - we have read one complete message packet", fn);
	if (status == NOT_OK) {
		misTrc(T_FLOW, "%s - message had wrong syntax. Send a NAK", fn);
		retVal = sSend(p->fd, nak_str, nak_length);
		if (retVal != eOK) {
			misTrc(T_FLOW, "%s - error sending nak on socket (%i)", fn, sGetSockFd(p->fd));
		}
		else {
			misTrc(T_FLOW, "%s - nak send sucessfully to sending process."
					" Now close the socket. ", fn);
		}
		/* in either case the socket must be closed. */
		sFreeSlot(p, CLOSE_SOCKET);
		/* Our current design is that if we ever have to send out a nak over a socket then we would close that
		 * socket immediately after sending the NAK.  Now whether sending of NAK was successful or not we have
		 * already closed the socket. Hence, in either case this is a valid condition, i.e. NOT serious enough
		 * to warrent the adapter itself to shutdown.  Hence, we return success to caller. */
		return eOK;
	}

	misTrc(T_FLOW, "%s - message sysntax is correct.", fn);
	retVal = sProcessData(data, data_length);
	if (retVal != eOK) {
		misTrc(T_FLOW, "%s - some error encountered during data process. Will send out a NAK", fn);
		retVal = sSend(p->fd, nak_str, nak_length);
		if (retVal != eOK) {
			misTrc(T_FLOW, "%s - error sending nak over socket (%i)", fn, sGetSockFd(p->fd));
		}
		else {
			misTrc(T_FLOW, "%s - nak send sucessfully to sending process."
					" Now close the socket. ", fn);
		}
		/* in either case the socket must be closed. */
		sFreeSlot(p, CLOSE_SOCKET);
		/* Our current design is that if we ever have to send out a nak over a socket then
		 * we would close that socket immediately after sending the NAK.  Now whether
		 * sending of NAK was successful or not we have already closed the socket. Hence,
		 * in either case this is a valid condition, return success to caller. */
		return eOK;
	}

	misTrc(T_FLOW, "%s - Data process sucessful. Respond with an ACK", fn);
	retVal = sSend(p->fd, ack_str, ack_length);
	if (retVal != eOK) {
		misTrc(T_FLOW, "%s - error sending ACK on socket (%i)"
				" proceeding to close socket", fn, sGetSockFd(p->fd));
		sFreeSlot(p, CLOSE_SOCKET);
	}
	else {
		misTrc(T_FLOW, "%s - ACK send sucessfully to sending process.", fn);
		/* reset the message buffers for this socket. */
		memset(p->message, MESSAGE_STRING_LEN, 0);
		p->length = 0;
	}
	/* message was good and was processed correctly, So we would return success
	 * to the caller even if we have had to close the socket due to inability to
	 * send ACK successfully. */
	return eOK;
}

static long sIncomingData(const fd_set *p_mask, const char *parse_packet_cmd)
{
	const char *const fn = "varsock_inbound_adapter::sIncomingData";
	long retVal = !eOK;
	int i; /* scratch varible used for loop control */
	int in_error = 0;

	misTrc(T_FLOW, "%s - entered", fn);
	
	/* look at all the open connections that we have see if the bit corresponding
	 * to it is set in the read_mask */
	in_error = 0;
	for (i = 0; i < MAX_SOCKET_CONNECTIONS && !in_error; i++) {
		/* if the current slot is in use */
		if (fdlist[i].used == TRUE) {
			int n = sGetSockFd(fdlist[i].fd); /* socket descriptor int value */
			/* if the mask corresponding to this socket is set in the mask */
			if (FD_ISSET(n, p_mask)) {
				retVal = sReceive(fdlist[i].fd, 1, fdlist[i].message + fdlist[i].length,
						MESSAGE_STRING_LEN - fdlist[i].length);
				/* On success sReceive() returns number of characters read. */
				if (retVal != 1) {
					misTrc(T_FLOW, "%s - error encountered reading from socket (%i)", fn, n);
					sFreeSlot(fdlist + i, CLOSE_SOCKET);
				}
				else {
					/* we have read one more charcter, increment the read count. */
					fdlist[i].length++;
					misTrc(T_FLOW, "%s - one character sucessfully read from socket (%i)"
							" current data buffer is (%s), proceeding to check if"
							" message read is complete", fn, n, fdlist[i].message);
					retVal = sNewDataAvailable(fdlist + i, parse_packet_cmd);
					if (retVal != eOK) {
						in_error = 1;
					}
				}
			}
		}
	}
	return in_error ? !eOK : eOK;
}

static long sProcessData(const char *data, long data_length)
{
	const char *const fn = "varsock_inbound_adapter::sProcessData";
	sl_Status_Typ            stat = SL_STAT_OK;
	long retVal = eOK;

	/* SEAMLES - start */
	if (!test_mode) {
		sl_Bool_Typ              new_instance = SL_FALSE;
		sl_Bool_Typ              ifd_ided = SL_FALSE;

		/* related to the IFD id and processing. */
		slObjIFD_IFDSegPK_Typ    IFDSegPK;
		char                     full_dwnld_name[128];
		char                     time_str[64];
		time_t tod;

		misTrc(T_FLOW, "%s - entered.", fn);

		misTrc(T_FLOW, "%s - ZAP the various SeamLES variables.", fn);
		memset ( &Dwnld, '\0', sizeof(slObjDwnld_Dwnld_Typ) );
		memset ( &DwnldPK, '\0', sizeof(slObjDwnld_DwnldPK_Typ) );
		memset ( &DwnldCtxt, '\0', sizeof(slIFD_DwnldCtxt_Typ) );
		memset ( &IFDSegPK, '\0', sizeof(slObjIFD_IFDSegPK_Typ) );

		/* append the timestamp to the downld_name */
		if (time(&tod) == -1) {
			misTrc(T_FLOW, "%s - error getting time of Day from time() function! Exiting...", fn);
			return !eOK;
		}
		if (strftime(time_str, sizeof(time_str), "Date-%Y%b%i Time-%H:%M:%S", localtime(&tod)) == 0) {
			misTrc(T_FLOW, "%s - error converting time_t structure to ascii time string!  Exiting...", fn);
			return !eOK;
		}
		sprintf(full_dwnld_name, "%s %s", dwnld_name, time_str);
		misTrc(T_FLOW, "%s - full_dwnld_name = %s", fn, full_dwnld_name);

		misTrc(T_FLOW, "%s - Indicate new download", fn);
		stat = slObjDwnld_Dwnld_New( &InAdapter.SysDef, full_dwnld_name, &Dwnld);
		if ( stat != SL_STAT_OK ) {
			return stat;
		}
		DwnldPK = Dwnld.DwnldPK;
		misTrc(T_FLOW, "%s - slObjDwnld_Dwnld_New() returned success.", fn);

		misTrc(T_FLOW, "%s - init the context.", fn);
		stat = slIFD_DwnldCtxt_Init( &Dwnld, &InAdapter.SysDef, &DwnldCtxt );
		if ( stat != SL_STAT_OK ) {
			return stat;
		}
		oldCommitCtxtIdSeq = DwnldCtxt.CommitCtxtIdSeq;
		misTrc(T_FLOW, "%s - slIFD_DwnldCtxt_Init() returned success.", fn);
		
		misTrc(T_FLOW, "%s - now id the message(%s)", fn, data);
		stat = slIFD_IFDData_IdLoad ( &DwnldCtxt, (char *)data, &IFDSegPK, &new_instance, &ifd_ided);
		if ( stat != SL_STAT_OK ) {
 			misTrc(T_FLOW, "%s - slIFD_IFDData_IdLoad() returned error(%ld).", fn, stat);
			return stat;
		}
		misTrc(T_FLOW, "%s - slIFD_IFDData_IdLoad() returned success.", fn);

		misTrc(T_FLOW, "%s - check if message id succeeded in SeamLES", fn);
		if (!ifd_ided) {
			misTrc(T_FLOW, "%s - No identification for packet: data(%s) data_length(%ld)", fn, data, data_length);
			/* We must return error back to the caller to signal that the message ID failed. */
			retVal = !eOK;
		}
		else {
			misTrc(T_FLOW, "%s - Sucessfully Identified packet: data(%s) data_length(%ld)", fn, data, data_length);

			if (new_instance) {
				misTrc(T_FLOW, "%s - proceed to commit the transaction", fn);
				slServer_Commit ( NULL );
			}
			misTrc(T_FLOW, "%s - Old commit context sequence id was %lf. New commit context is %lf", fn,
					oldCommitCtxtIdSeq, DwnldCtxt.CommitCtxtIdSeq);
		}

		misTrc(T_FLOW, "%s - regardless of whether id was successful or not, signal download finish.", fn);
		/* Assign the DwnldSeq variable before freeing memory */
		DwnldPK = Dwnld.DwnldPK;

		/* Finish the dwnld and insert remaining stuff */
		slObjDwnld_Dwnld_Finish ( Dwnld, &InAdapter.SysDef, full_dwnld_name, &DwnldCtxt,
								/* If we would run it also, indicate that */
								InAdapter.slArgs.r_flg ? SL_DWNLD_STAT_CD_EPROC : SL_DWNLD_STAT_CD_ID);

		misTrc(T_FLOW, "%s - signal context finish.", fn);
		slIFD_DwnldCtxt_Finish( &DwnldCtxt );

		if ( stat == SL_STAT_OK ) {
			slServer_Commit ( NULL );
		}
		else {
			slServer_Rollback ( NULL );
		}

		misTrc(T_FLOW, "%s - check if the transaction is to be run immediately.  If so, run it.", fn);
		if (run_now && new_instance) {
			stat = slInAdapter_RunTransaction ( DwnldPK.DwnldSeq, &InAdapter.ClientOrServerExec, &InAdapter );
			misTrc(T_FLOW, "%s - () Run Transaction: Status: %i", fn, (int)stat);

			if( stat != SL_STAT_OK) {
				misTrc(T_FLOW, "%s - Error Running Transaction on dwnldSeq - %.0lf", fn, DwnldPK.DwnldSeq);
				slServer_Rollback ( NULL );
				/* we must pass error back to the caller. */
				retVal = stat;
			}
			else {
				misTrc(T_FLOW, "%s - transaction ran successfully", fn);
				slServer_Commit ( NULL );
			}
		}

	}
	/* SEAMLES - end */

	return retVal;
}


