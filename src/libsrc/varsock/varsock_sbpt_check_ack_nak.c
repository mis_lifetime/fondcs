static char RCS_Id[] = "$Id: varsock_sbpt_check_ack_nak.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varsock/varsock_sbpt_check_ack_nak.c,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: VARSOCK-level command.  Contains Skill Bosch specific protocol details.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

#include <moca.h>

#include <stdio.h>
#include <string.h>

#include <mocaerr.h>
#include <srvlib.h>
#include <oslib.h>
#include <applib.h>

#include <varsock_colwid.h>
#include <varsock_err.h>

#include <varsock_util.h> /* misc for socket adapter */
#include <varsock_sbpt.h>   /* specific to Skill Bosch protocol. */

/* Manifest constants used in this module only */
/* names of the input parameters, used in validation of input */
static const char *const MESSAGE_STRING    = "message_string";
static const char *const MESSAGE_LENGTH    = "message_length";
static const char *const RESPONSE_STRING   = "response_string";
static const char *const RESPONSE_LENGTH   = "response_length";

/* columns names */
static const char *const READ_MORE         = "read_more";
static const char *const ACK_COL_NAME      = "ack";

/* other #defines go here */

/* forward declaration of local functions. */

/* global variables. */

/* static functions */
static int sCompareSequenceNumbers(const char *s2, const char *s1);

/* In this protocol the length of the ACK/NAK string is fixed.  Hence, the outbound
 * socket adapter should already have the MIN_ACK_NAK_LEN available to it when it
 * would have called the the server command to get the PREFIX and SUFFIX for sending
 * the message out over the socket.  Hence, in practise it should never happen that this
 * command would get called with insufficient number of characters passed in the 
 * response string.  However, still this component defensibly checks for the length
 * of the response string received and accordingly sets and returns the READ_MORE variable.
 * Further, the ACK/NAK do NOT depend on the message being sent out: regardless of the message
 * the ACK is always one character long 'A' and NAK is always one character long 'N'.  Still
 * we would check that the messgae string and message length values are passed in correctly,
 * i.e. non-NULL.  This is to ensure that the socket adapter is not misbehaving.
 */
/* The main implementation */
LIBEXPORT 
RETURN_STRUCT *varsock_sbpt_check_ack_nak (
	char *message_string_i,  /* The message packet that was send to host */
	long *message_length_i,  /* length of the character string pointed to by message_string_i */
	char *response_string_i, /* Response received from the host system. */
	long *response_length_i  /* Length of the character string pointed to by response_string_i */
)
{
	/* scratch variables -- may get used in more than one context. */
	char buffer[1024];
	long retVal = !eOK;

	/* Other variables */
	RETURN_STRUCT *Result = 0;
	const char *const fn = "varsock_sbpt_check_ack_nak";
	/* by default assume that there are insufficient number of characters as the ACK/NAK string */
	long read_more = 1;
	/* by default assume that it is a NAK */
	long isItAck = 0;

	misTrc (T_FLOW, "In %s()", fn);

	/* firewall the input arguments. */
	/* Note that the ACK/NAK do NOT depend on the message being sent.  But we still check that is is passed
	 * so that we can raise error is the caller is making a mistake.*/
	/* message_string is required. */
	if (!message_string_i) {
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, MESSAGE_STRING);
		return APPMissingArg(MESSAGE_STRING);
	}
	else {
		/* NOTE: since we are not checking for misTrimlen(message_string), it is legal to pass in a null
		 * string as a message_string.  Though that would be very strange on part of the caller, but we
		 * allow it.  The hope is that the problem would be caught by someone looking at the log files, rather
		 * than disallowing it in this function. */
		misTrc(T_FLOW, "%s - Argument %s(%s) checks out fine.", fn, MESSAGE_STRING, message_string_i);
	}

	/* if we would have been using the value of message string, we would have got the message string "washed",
	 * i.e. have all occurances of '\127' replaced with NULLs.  Because the adapter does the NULL to '\127'
	 * conversion before sending the message string to us.  But we will skip that conversion here as we do NOT
	 * need to use the message string. */

	/* message_length is required. */
	if (!message_length_i) {
		misTrc(T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, MESSAGE_LENGTH);
		return APPMissingArg(MESSAGE_LENGTH);
	}
	else {
		misTrc(T_FLOW, "%s - checking if the argument %s(%ld) is valid", fn, MESSAGE_LENGTH, *message_length_i);
		if (*message_length_i < 0) {
			ZAP(buffer);
			sprintf(buffer, "argument %s was specified as %ld.  It should NOT be less than 0",
					MESSAGE_LENGTH, *message_length_i);
			misTrc(T_FLOW, "%s - %s", fn, buffer);
			return APPInvalidArg(MESSAGE_LENGTH, buffer);
		}
		misTrc(T_FLOW, "%s - argument %s(%ld) checks out fine.", fn, MESSAGE_LENGTH, *message_length_i);
	}

	/* response_length is required. */
	if (!response_length_i) {
		misTrc(T_FLOW, "%s - required argument %s was not specified.", fn, RESPONSE_LENGTH);
		return APPMissingArg(RESPONSE_LENGTH);
	}
	else {
		misTrc(T_FLOW, "%s - checking if argument %s(%ld) is valid.", fn, RESPONSE_LENGTH, *response_length_i);
		if (*response_length_i < 0) {
			ZAP(buffer);
			sprintf(buffer, "argument %s is specified as %ld.  It should NOT be less than 0",
					RESPONSE_LENGTH, *response_length_i);
			misTrc(T_FLOW, "%s - %s", fn, buffer);
			return APPInvalidArg(RESPONSE_LENGTH, buffer);
		}
		misTrc(T_FLOW, "%s - argument %s(%ld) checks out fine.", fn, RESPONSE_LENGTH, *response_length_i);
	}
	
	/* response_string is required. */
	if (!response_string_i) {
		misTrc (T_FLOW, "%s - Required argument %s was not specified!  Exiting...", fn, RESPONSE_STRING);
		return APPMissingArg(RESPONSE_STRING);
	}
	else {
		/* NOTE that we are NOT checking to see if the response_string itself if NULL or not.
		 * It is, hence, legal to supply a null string */
		misTrc(T_FLOW, "%s - Argument %s(%s) checks out fine.", fn, RESPONSE_STRING, response_string_i);
	}
	
	/* Log the value of input parameters */
	misTrc(T_FLOW, "%s - input values: %s(%s), %s(%ld), %s(%s), %s(%ld)",
			fn, MESSAGE_STRING, message_string_i, MESSAGE_LENGTH, *message_length_i,
			RESPONSE_STRING, response_string_i, RESPONSE_LENGTH, *response_length_i);

	/* the length of the response string should be exactly 'A' or 'N', i.e. 1 character long */
	if (*response_length_i < ACK_NAK_MSG_LEN) {
		misTrc(T_FLOW, "%s - incorrect %s(%ld).  Correct length is %i."
				" Caller should NOT have called with a zero length response string!  Assuming a NAK!",
				fn, RESPONSE_LENGTH, *response_length_i, ACK_NAK_MSG_LEN);
		/* we have not read enough to conclude if it is a ACK or a NAK. */
		read_more = 1; isItAck = 0;
	}
	else if (*response_length_i > ACK_NAK_MSG_LEN) {
		misTrc(T_FLOW, "%s - something is SERIOUSLY WRONG!  The code should NEVER reach this point.", fn);
		misTrc(T_FLOW, "%s - error in socket adapter code.  It should never be allowed to read MORE"
				" than the needed number of characters as part of the ACK/NAK.", fn);
		misTrc(T_FLOW, "%s - for now treat it as a NAK!", fn);
		/* stop reading any more and treat it as a NAK. */
		read_more = 0; isItAck = 0;
	}
	/* compare the first character of the response_string with 'N' */
	else if ( strcmp(response_string_i, NAK) == 0 ) {
		misTrc(T_FLOW, "%s - host has sent a <NAK> character in the response_string.", fn);
		/* it is a nak */
		read_more = 0; isItAck = 0;
	}
	else if ( strcmp(response_string_i, ACK) == 0 ) {
		misTrc(T_FLOW, "%s - format of the ACK/NAK response string is correct and"
				" the host has sent an <ACK> character back in the response string.", fn);
		/* it is an ack */
		read_more = 0; isItAck = 1;
	}
	else { /* response_string_i != NAK && response_string_i != ACK */
		misTrc(T_FLOW, "%s - Invalid character ASCII(%c):(%i) sent as part of the ACK/NAK message."
				"  Assuming a NAK!", fn, *response_string_i, (int)*response_string_i);
		read_more = 0; isItAck = 0;
	}

	/* setup the column names in the results touple */
	Result = srvResultsInit(eOK,
			READ_MORE, COMTYP_INT, sizeof(long),
			ACK_COL_NAME, COMTYP_INT, sizeof(long),
			NULL);

	if (!Result) {
		/* srvResultInit failed! */
		misTrc(T_FLOW, "%s - Unexpected error! srcResultsInit() failed.  Exiting...", fn);

		/* return error to calling process. */
		return srvResults(eVAR_SRV_RESULTS_INIT_FAILED, NULL);
	}

	misTrc(T_FLOW, "%s - stuffing in return values %s(%ld), %s(%ld)", fn, READ_MORE, read_more, ACK_COL_NAME, isItAck);
	retVal = srvResultsAdd(Result, read_more, isItAck);
	if (retVal != eOK) {
		misTrc(T_FLOW, "%s - unexpected error! srvResultsAdd() returned error(%ld)!"
				"  Exiting...", fn, retVal);

		srvFreeMemory(SRVRET_STRUCT, Result);
		return srvResults(eVAR_SRV_RESULTS_ADD_FAILED, 
				  NULL);
	}

	return Result;
}
