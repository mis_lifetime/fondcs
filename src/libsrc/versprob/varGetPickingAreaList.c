static char    *rcsid = "$Id: intGetPickingAreaList.c,v 1.15 2000/11/10 16:06:56 verdeyen Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/
#include <moca_app.h>

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

typedef struct _PickAreaList
{
    char            arecod[ARECOD_LEN + 1];
    char            pckcod[100];
    moca_bool_t     lodflg;
    moca_bool_t     subflg;
    moca_bool_t     dtlflg;
    
    moca_bool_t     area_lodflg;
    moca_bool_t     area_subflg;
    moca_bool_t     area_dtlflg;

    struct _PickAreaList *next;
} PICK_AREA_LIST;


static void sFreePolicyList(PICK_AREA_LIST * List)
{
    PICK_AREA_LIST *last, *cur;

    last = cur = NULL;
    for (cur = List; cur; last = cur, cur = cur->next)
	if (last)
	    free(last);

    if (last)
	free(last);

}

static long sLoadArea (char *sqlbuffer, 
		       PICK_AREA_LIST **pTop,
		       PICK_AREA_LIST **pPtr,
		       char *arecodString)
{
	
    mocaDataRes         *res;
    mocaDataRow         *row;
    char                tmpare[ARECOD_LEN + LODLVL_LEN + 10];
    long	        ret_status;
    PICK_AREA_LIST	*Top;
    PICK_AREA_LIST	*ptr;

    
    Top = *pTop;
    ptr = *pPtr;
    
    ret_status = sqlExecStr(sqlbuffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
	misTrc(T_FLOW, 
	       "(policies) No policies found");
        return (eOK);
    }
	
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        if (!sqlIsNull(res, row, "arecod"))
        {
    	    if (Top == NULL)
	    {
	        Top = (PICK_AREA_LIST *)
	    	    calloc(1, sizeof(PICK_AREA_LIST));

	        if (Top == NULL)
	        {
	    	    sqlFreeResults(res);
		    return (eNO_MEMORY);
	        }
	        ptr = Top;
	    }
	    else
	    {
	        ptr->next = (PICK_AREA_LIST *)
			calloc(1, sizeof(PICK_AREA_LIST));

	        if (ptr->next == NULL)
	        {
	    	    sqlFreeResults(res);
		    sFreePolicyList(Top);
		    return (eNO_MEMORY);
	        }
	        ptr = ptr->next;
	    }
	    if (!sqlIsNull(res, row, "arecod"))
		strncpy(ptr->arecod,
			sqlGetString(res,row,"arecod"), ARECOD_LEN);
	    if (!sqlIsNull(res, row, "pckcod"))
		strncpy(ptr->pckcod, 
			sqlGetString(res,row, "pckcod"), PCKCOD_LEN);
	    if (!sqlIsNull(res, row, "lodflg"))
		ptr->lodflg = sqlGetBoolean(res, row, "lodflg");
	    if (!sqlIsNull(res, row, "subflg"))
		ptr->subflg = sqlGetBoolean(res, row, "subflg");
	    if (!sqlIsNull(res, row, "dtlflg"))
		ptr->dtlflg = sqlGetBoolean(res, row, "dtlflg");

	    /* Save off what the area had enabled so that we can publish */
	    /* this data later on... */
	    ptr->area_lodflg = ptr->lodflg;
	    ptr->area_subflg = ptr->subflg;
	    ptr->area_dtlflg = ptr->dtlflg;

	    if (!sqlIsNull(res, row, "rtstr2") &&
	        strncmp(sqlGetString(res, row, "rtstr2"), LODLVL_LOAD,
	        misTrimLen(sqlGetString(res, row, "rtstr2"), RTSTR2_LEN)) == 0)
	    {
	        ptr->subflg = BOOLEAN_FALSE;
	        ptr->dtlflg = BOOLEAN_FALSE;
	    }
	    if (!sqlIsNull(res, row, "rtstr2") &&
		strncmp(sqlGetString(res, row, "rtstr2"), LODLVL_SUB,
		misTrimLen(sqlGetString(res, row, "rtstr2"), RTSTR2_LEN)) == 0)
	    {
	        ptr->lodflg = BOOLEAN_FALSE;
	        ptr->dtlflg = BOOLEAN_FALSE;
	    }

	    if (!sqlIsNull(res, row, "rtstr2") &&
	        strncmp(sqlGetString(res, row, "rtstr2"), LODLVL_DTL,
  	        misTrimLen(sqlGetString(res, row, "rtstr2"), RTSTR2_LEN)) == 0)
	    {
	        ptr->lodflg = BOOLEAN_FALSE;
	        ptr->subflg = BOOLEAN_FALSE;
	    }
	    
	    
	    sprintf(tmpare, "('%.*s-%.*s')", ARECOD_LEN, ptr->arecod,
	          LODLVL_LEN, sqlIsNull(res, row, "rtstr2") ? "*":
	          misTrim(sqlGetString(res, row, "rtstr2")));
	    
	    if (strlen(arecodString)) 
	    {
		strcat(arecodString, ",");
	    }
	    strcat(arecodString, tmpare);
   
        }
    }
    
    *pTop = Top;
    *pPtr = ptr;

    sqlFreeResults(res);
    return (eOK);
}

static long sLoadPickAreas(char *prtnum,
			   char *prt_client_id,
			   char *dstare,
			   PICK_AREA_LIST ** List,
			   char *arecod, 
			   char *pcktyp,
			   char *bldg_id,
			   char *client_id,
			   char *orig_pcktyp)
{
    mocaDataRes     *res;
    mocaDataRow     *row;
    char            sqlbuffer[5000];
    char            arecodString[1000];
    char            uniqueClause[1500];
    long            ret_status;
    char            *polcodStr;

    PICK_AREA_LIST *Top, *ptr = 0;

    *List = NULL;
    Top = NULL;
    memset(arecodString, 0, sizeof(arecodString));

    if (!strcmp(orig_pcktyp, ALLOCATE_REPLENISH) ||
	!strcmp(orig_pcktyp, ALLOCATE_TOPOFF_REPLEN))
    {
	sprintf(arecodString, 
		"('%.*s-*'),('%.*s-L'),('%.*s-S'),('%.*s-D')",
		ARECOD_LEN,dstare,ARECOD_LEN,dstare,ARECOD_LEN,dstare,
		ARECOD_LEN,dstare);
    }

    if (!strcmp(pcktyp, ALLOCATE_REPLENISH) ||
	!strcmp(pcktyp, ALLOCATE_TOPOFF_REPLEN))
	polcodStr = POLCOD_RPL_PREF;
    else
	polcodStr = POLCOD_PICK_PREF;

    /* If an area code was specified, then just load the
       pick characteristics of this area code... */
    if (arecod && misTrimLen(arecod, ARECOD_LEN))
    {
	sprintf(sqlbuffer,
		"select pckcod, lodflg, subflg, dtlflg "
		"  from aremst "
		" where arecod = '%.*s'",
		(int) misTrimLen(arecod, ARECOD_LEN), arecod);
	ret_status = sqlExecStr(sqlbuffer, &res);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return (ret_status);
	}
	Top = (PICK_AREA_LIST *) calloc(1, sizeof(PICK_AREA_LIST));
	if (Top == NULL)
	{
	    sqlFreeResults(res);
	    return (eNO_MEMORY);
	}
	row = sqlGetRow(res);
	strncpy(Top->arecod, arecod, ARECOD_LEN);
	Top->lodflg = sqlGetBoolean(res, row, "lodflg");
	Top->subflg = sqlGetBoolean(res, row, "subflg");
	Top->dtlflg = sqlGetBoolean(res, row, "dtlflg");
	strncpy(Top->pckcod, sqlGetString(res, row, "pckcod"), PCKCOD_LEN);
	*List = Top;
	sqlFreeResults(res);
	return (eOK);
    }

    /* 
     * First check for a dest/part number policy... 
     */

    if (prtnum && dstare)
    {
        *uniqueClause = (char)0;
        if (strlen(arecodString))
        {
	    sprintf(uniqueClause,
		    " and (aremst.arecod||'-'||poldat.rtstr2) not in (%s) ",
		    arecodString);
        }

        misTrc(T_FLOW, "(policies) Loading Part/Dest Policies Part: %s\n", prtnum);
        sprintf(sqlbuffer,
	    "select aremst.lodflg, "
	    "       aremst.subflg, aremst.dtlflg, "
	    "       poldat.rtstr2, aremst.arecod, aremst.pckcod "
	    "   from poldat, aremst  "
	    "   where poldat.polcod = '%s%s%s' "
	    "     and lower(poldat.polvar) = 'dstare_prtnum' "
	    "     and poldat.polval =  '%s'||'_'||'%s%s%s' "
	    "     %s "
	    "     and aremst.sigflg =  %d "
	    "     and aremst.pckcod != '%s' "
	    "     and poldat.rtstr1 = aremst.arecod   "
	    "   order by poldat.srtseq",
	    bldg_id, DASH, polcodStr, 
	    misTrim(dstare), prt_client_id, CONCAT_CHAR, prtnum,
	    uniqueClause,
	    BOOLEAN_TRUE,
	    PCKCOD_NOT_PICK);

        ret_status = sLoadArea (sqlbuffer, &Top, &ptr, arecodString);
        if (ret_status != eOK)
        {
            return (ret_status);
        }
    }


    /* 
     * Next check for a part number policy... 
     */
    
    if (prtnum)
    {
        misTrc(T_FLOW, "(policies) Loading policies for Part: %s\n", prtnum);

        *uniqueClause = (char)0;
        if (strlen(arecodString))
        {
	    sprintf(uniqueClause,
		    " and (aremst.arecod||'-'||poldat.rtstr2) not in (%s) ",
		    arecodString);
        }

        sprintf(sqlbuffer,
	    "select aremst.lodflg, "
	    "       aremst.subflg, aremst.dtlflg, "
	    "       poldat.rtstr2, aremst.arecod, aremst.pckcod "
	    "   from poldat, aremst  "
	    "   where poldat.polcod = '%s%s%s' "
	    "     and lower(poldat.polvar) = 'prtnum' "
	    "     and poldat.polval = '%s%s%s' "
	    "      %s "
	    "     and aremst.sigflg = %d  "
	    "     and aremst.pckcod != '%s' "
	    "     and poldat.rtstr1 = aremst.arecod   "
	    "   order by poldat.srtseq",
	    bldg_id, DASH, polcodStr, 
	    prt_client_id, CONCAT_CHAR, prtnum, 
	    uniqueClause,
	    BOOLEAN_TRUE, 
	    PCKCOD_NOT_PICK);

        ret_status = sLoadArea (sqlbuffer, &Top, &ptr, arecodString);
        if (ret_status != eOK)
        {
            return (ret_status);
        }
    }

    /*
     * Next a Part Family/Dest policy... 
     */

    if (prtnum && dstare)
    {
        *uniqueClause = (char)0;
        if (strlen(arecodString))
        {
	    sprintf(uniqueClause,
                    " and (aremst.arecod||'-'||poldat.rtstr2) not in (%s) ",
		    arecodString);
        }

        sprintf(sqlbuffer,
	    "select aremst.lodflg, "
	    "       aremst.subflg, aremst.dtlflg, "
	    "       poldat.rtstr2, aremst.arecod, aremst.pckcod "
	    " from poldat, prtmst, aremst    "
	    " where poldat.polcod = '%s%s%s' "
	    "   and lower(poldat.polvar) = 'dstare_prtfam' "
	    "   and aremst.sigflg = %d  "
	    "   and aremst.pckcod != '%s' "
	    "   and prtmst.prtnum = '%s'  "
	    "       %s "
	    "   and poldat.polval = '%s'||'_'||prtmst.prtfam "
	    "   and poldat.rtstr1 = aremst.arecod    "
	    " order by poldat.srtseq",
	    bldg_id, DASH, polcodStr, 
	    BOOLEAN_TRUE, 
	    PCKCOD_NOT_PICK,
	    prtnum, 
	    uniqueClause, 
	    misTrim(dstare));
    
        ret_status = sLoadArea (sqlbuffer, &Top, &ptr, arecodString);
        if (ret_status != eOK)
        {
            return (ret_status);
        }
    }

    /* 
     * Next a part family policy... 
     */

    if (prtnum)
    {
        *uniqueClause = (char)0;
        if (strlen(arecodString))
        {
	    sprintf(uniqueClause,
		" and (aremst.arecod||'-'||poldat.rtstr2) not in (%s) ",
		arecodString);
        }

        sprintf(sqlbuffer,
	    "select aremst.lodflg, "
	    "       aremst.subflg, aremst.dtlflg, "
	    "       poldat.rtstr2, aremst.arecod, aremst.pckcod "
	    "  from poldat, prtmst, aremst    "
	    " where poldat.polcod = '%s%s%s' "
	    "   and lower(poldat.polvar) = 'prtfam' "
	    "   and aremst.sigflg = %d  "
	    "   and aremst.pckcod != '%s'  "
	    "   and prtmst.prtnum = '%s'  "
	    "       %s "
	    "   and poldat.polval = prtmst.prtfam  "
	    "   and poldat.rtstr1 = aremst.arecod  "
	    " order by poldat.srtseq",
	    bldg_id, DASH, polcodStr, 
	    BOOLEAN_TRUE, 
	    PCKCOD_NOT_PICK, 
	    prtnum, 
	    uniqueClause);

        ret_status = sLoadArea (sqlbuffer, &Top, &ptr, arecodString);
        if (ret_status != eOK)
        {
            return (ret_status);
        }
    }

    /* 
     * First check for a client/dest policy... 
     */
    if (client_id)
    { 
        sprintf(sqlbuffer,
	    "select aremst.lodflg, "
	    "       aremst.subflg, aremst.dtlflg, "
	    "       poldat.rtstr2, aremst.arecod, aremst.pckcod "
	    "   from poldat, aremst  "
	    "   where poldat.polcod = '%s%s%s' "
	    "     and lower(poldat.polvar) = 'dstare_client_id' "
	    "     and poldat.polval =  '%s'||'_'||'%s' "
	    "     and aremst.sigflg =  %d "
	    "     and aremst.pckcod != '%s' "
	    "     and poldat.rtstr1 = aremst.arecod   "
	    "   order by poldat.srtseq",
	    bldg_id, DASH, polcodStr, 
	    misTrim(dstare), client_id,
	    BOOLEAN_TRUE,
	    PCKCOD_NOT_PICK);

        ret_status = sLoadArea (sqlbuffer, &Top, &ptr, arecodString);
        if (ret_status != eOK)
        {
            return (ret_status);
        }
    }

    /* 
     * Next check for a client Id policy... 
     */

    if (client_id)
    {
        misTrc(T_FLOW, "(policies) Loading policies for Part: %s\n", prtnum);

        *uniqueClause = (char)0;
        if (strlen(arecodString))
        {
	    sprintf(uniqueClause,
		" and (aremst.arecod||'-'||poldat.rtstr2) not in (%s) ",
		arecodString);
        }

        sprintf(sqlbuffer,
	    "select aremst.lodflg, "
	    "       aremst.subflg, aremst.dtlflg, "
	    "       poldat.rtstr2, aremst.arecod, aremst.pckcod "
	    "   from poldat, aremst  "
	    "   where poldat.polcod = '%s%s%s' "
	    "     and lower(poldat.polvar) = 'client_id' "
	    "     and poldat.polval = '%s' "
	    "      %s "
	    "     and aremst.sigflg = %d  "
	    "     and aremst.pckcod != '%s' "
	    "     and poldat.rtstr1 = aremst.arecod   "
	    "   order by poldat.srtseq",
	    bldg_id, DASH, polcodStr, 
	    client_id, 
	    uniqueClause,
	    BOOLEAN_TRUE, 
	    PCKCOD_NOT_PICK);


        ret_status = sLoadArea (sqlbuffer, &Top, &ptr, arecodString);
        if (ret_status != eOK)
        {
            return (ret_status);
        }
    }

    /* 
     * Next the System/Dest default policy... 
     */

    if (dstare)
    {
        *uniqueClause = (char)0;
        if (strlen(arecodString))
        {
	    sprintf(uniqueClause,
		" and (aremst.arecod||'-'||poldat.rtstr2) not in (%s) ",
		arecodString);
        }

        sprintf(sqlbuffer,
	    "select aremst.lodflg, "
	    "       aremst.subflg, aremst.dtlflg, "
	    "       poldat.rtstr2, aremst.arecod, aremst.pckcod "
	    " from poldat, aremst    "
	    " where poldat.polcod = '%s%s%s'  "
	    "   and poldat.polvar = 'dstare'  "
	    "   and poldat.polval = '%s'||'_'||'%s'  "
	    "       %s "
	    "   and aremst.sigflg = %d  "
	    "   and aremst.pckcod != '%s' "
	    "   and poldat.rtstr1 = aremst.arecod    "
	    " order by poldat.srtseq",
	    bldg_id, DASH, polcodStr, 
	    misTrim(dstare), POLVAL_DEFAULT,
	    uniqueClause,
	    BOOLEAN_TRUE, 
	    PCKCOD_NOT_PICK);

        ret_status = sLoadArea (sqlbuffer, &Top, &ptr, arecodString);
        if (ret_status != eOK)
        {
            return (ret_status);
        }
    }

    /* 
     * Next the system default policy... 
     */

    *uniqueClause = (char)0;
    if (strlen(arecodString))
    {
	sprintf(uniqueClause,
		" and (aremst.arecod||'-'||poldat.rtstr2) not in (%s) ",
		arecodString);
    }

    sprintf(sqlbuffer,
	    "select aremst.lodflg, "
	    "       aremst.subflg, aremst.dtlflg, "
	    "       poldat.rtstr2, aremst.arecod, aremst.pckcod "
	    " from poldat, aremst    "
	    " where poldat.polcod = '%s%s%s'  "
	    "   and poldat.polvar = '%s'  "
	    "   and poldat.polval = '%s'  "
	    "       %s "
	    "   and aremst.sigflg = %d  "
	    "   and aremst.pckcod != '%s' "
	    "   and poldat.rtstr1 = aremst.arecod    "
	    " order by poldat.srtseq",
	    bldg_id, DASH, polcodStr, 
	    POLVAR_DEFAULT, POLVAL_DEFAULT,
	    uniqueClause, BOOLEAN_TRUE, PCKCOD_NOT_PICK);

    ret_status = sLoadArea (sqlbuffer, &Top, &ptr, arecodString);
    if (ret_status != eOK)
    {
        return (ret_status);
    }
    
    if (Top == NULL && (strcmp(pcktyp, ALLOCATE_REPLENISH) &&
			strcmp(pcktyp, ALLOCATE_TOPOFF_REPLEN)))
	return (eDB_NO_ROWS_AFFECTED);  
    else if (Top == NULL && (!strcmp(pcktyp, ALLOCATE_REPLENISH) ||
			     !strcmp(pcktyp, ALLOCATE_TOPOFF_REPLEN)))
    {
	ret_status = sLoadPickAreas(prtnum,
				    prt_client_id,
				    dstare,
				    List, arecod, ALLOCATE_PICK,
				    bldg_id, 
				    client_id,
				    pcktyp);
	return (ret_status);
    }


    *List = Top;
    return (eOK);

}


LIBEXPORT 
RETURN_STRUCT *varGetPickingAreaList(char *prtnum_i,
				     char *prt_client_id_i,
				     char *dstare_i,
				     char *arecod_i,
				     char *pcktyp_i,
				     char *bldg_id_i,
				     char *client_id_i,
				     char *pcklvl_i)
{
    long ret_status;
    PICK_AREA_LIST *List, *ptr;
    RETURN_STRUCT *CmdRes;

    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char dstare[ARECOD_LEN+1];
    char arecod[ARECOD_LEN+1];
    char pcktyp[100];
    char bldg_id[BLDG_ID_LEN+1];
    char client_id[CLIENT_ID_LEN+1];

    memset(prtnum, 0, sizeof(prtnum));
    memset(prt_client_id, 0, sizeof(prt_client_id));
    memset(dstare, 0, sizeof(dstare));
    memset(arecod, 0, sizeof(arecod));
    memset(pcktyp, 0, sizeof(pcktyp));
    memset(bldg_id, 0, sizeof(bldg_id));
    memset(client_id, 0, sizeof(client_id));

    if (prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN))
	strncpy(prtnum, prtnum_i, PRTNUM_LEN);
    if (prt_client_id_i && misTrimLen(prt_client_id_i, CLIENT_ID_LEN))
	strncpy(prt_client_id, prt_client_id_i, CLIENT_ID_LEN);
    if (client_id_i && misTrimLen(client_id_i, CLIENT_ID_LEN))
	strncpy(client_id, client_id_i, CLIENT_ID_LEN);
    if (dstare_i && misTrimLen(dstare_i, ARECOD_LEN))
	strncpy(dstare, dstare_i, ARECOD_LEN);
    if (arecod_i && misTrimLen(arecod_i, ARECOD_LEN))
	strncpy(arecod, arecod_i, ARECOD_LEN);
    if (pcktyp_i && misTrimLen(pcktyp_i, sizeof(pcktyp) -1))
	strncpy(pcktyp, pcktyp_i, misTrimLen(pcktyp_i, sizeof(pcktyp) -1));

    if (bldg_id_i && misTrimLen(bldg_id_i, BLDG_ID_LEN))
        strncpy(bldg_id, bldg_id_i, BLDG_ID_LEN);
    else
        return APPInvalidArg ("", "bldg_id");


    List = NULL;
    ret_status = sLoadPickAreas(prtnum,
				prt_client_id,
				dstare,
				&List,
				arecod, 
				pcktyp,
				bldg_id,
				client_id,
				pcktyp);


    CmdRes = srvResultsInit(eOK,
			    "arecod", COMTYP_CHAR, ARECOD_LEN,
			    "pckcod", COMTYP_CHAR, PCKCOD_LEN,
			    "lodflg", COMTYP_BOOLEAN, sizeof(long),
			    "subflg", COMTYP_BOOLEAN, sizeof(long),
			    "dtlflg", COMTYP_BOOLEAN, sizeof(long),
			    "area_lodflg", COMTYP_BOOLEAN, sizeof(long),
			    "area_subflg", COMTYP_BOOLEAN, sizeof(long),
			    "area_dtlflg", COMTYP_BOOLEAN, sizeof(long),
			    NULL);

    if (ret_status != eOK)
    {   
	if (List)
	    sFreePolicyList(List);

        return(CmdRes);

/* 	return(srvSetupReturn(ret_status, "")); */
    }

    
    if (pcklvl_i && strlen(pcklvl_i))
	misTrc(T_FLOW, "(policies) Picking Level specified as: %s", pcklvl_i);

    misTrc(T_FLOW,
	   "(policies) Will search the following"
	   " area codes (in order): \n");
    for (ptr = List; ptr; ptr = ptr->next)
    {
	if (pcklvl_i && strlen(pcklvl_i))
	{
	    if ((strncmp(pcklvl_i, LODLVL_LOAD, LODLVL_LEN) == 0 &&
		 ptr->lodflg == 0) ||
		(strncmp(pcklvl_i, LODLVL_SUBLOAD, LODLVL_LEN) == 0 &&
		 ptr->subflg == 0) ||
		(strncmp(pcklvl_i, LODLVL_DETAIL, LODLVL_LEN) == 0 &&
		 ptr->dtlflg == 0))
	    {
		misTrc(T_FLOW,
		       "(policies)   => %s, lod=%d, sub=%d, "
		       "dtl=%d - SKIPPED (pcklvl)\n",
		       ptr->arecod, ptr->lodflg, ptr->subflg, ptr->dtlflg);
		continue;
	    }
	}
	misTrc(T_FLOW, 
	       "(policies)   => %s, lod=%d, sub=%d, dtl=%d",
	       ptr->arecod, ptr->lodflg, ptr->subflg, ptr->dtlflg);

	srvResultsAdd(CmdRes,
		      ptr->arecod,
		      ptr->pckcod,
		      ptr->lodflg,
		      ptr->subflg,
		      ptr->dtlflg,
		      ptr->area_lodflg,
		      ptr->area_subflg,
		      ptr->area_dtlflg);
    }

    sFreePolicyList(List);
    return(CmdRes);
}
