static const char *rcsid = "$Id: intChangeHeldPickStatus.c,v 1.7 2002/01/30 13:52:30 mzais Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>


/*
 *    sCheckStopRelease returns TRUE, the picks for the stop can be released
 *                      or FALSE, the picks for the stop cannot be released. 
 *
 */

static long sCheckStopRelease(char *stop_id,
                              long num_stops_to_release)
{
    mocaDataRes *res;
    mocaDataRow *row;

    long ret_status = eOK;
    char buffer [1000];
    char car_move_id [CAR_MOVE_ID_LEN + 1];
    long stop_seq = 1;
    long rownum = 0;

    memset(buffer, 0, sizeof(buffer));
    memset(car_move_id, 0, sizeof(car_move_id));
 
    misTrc(T_FLOW, "Checking to see if the picks for stop, '%s', can "
                   "be released. ", stop_id);

    /* First, let's get the car_move_id and stop_seq for this stop. */

    sprintf(buffer,
            "  select car_move_id  "
            "    from stop     "
            "   where stop_id = '%s'      ",
            stop_id);

    ret_status = sqlExecStr(buffer, &res);

    if (eOK != ret_status)
    {
        sqlFreeResults(res);
        return(ret_status);
    }

    row = sqlGetRow(res);

    if (!sqlIsNull(res, row, "car_move_id"))
    {
        misTrimcpy (car_move_id, sqlGetString(res, row, "car_move_id"), 
                    CAR_MOVE_ID_LEN);
    }
    else 
    {
        sqlFreeResults(res);
        return (BOOLEAN_FALSE);
    }

    sqlFreeResults(res);

    /* Now we're going to get the list of stops for this carrier move    */
    /* that aren't complete yet in order of stop sequence.               */

    sprintf(buffer,
            "  select stop_seq, "
            "         stop_id   "
            "    from stop      "
            "   where car_move_id = '%s' "
            "     and stop_cmpl_flg = 0   "
            "order by stop_seq asc ", 
            car_move_id);

    ret_status = sqlExecStr(buffer, &res);

    if (eOK != ret_status)
    {
        sqlFreeResults(res);
        return(ret_status);
    }

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        rownum++;
        if ((rownum <= num_stops_to_release) &&
            (!misCiStrcmp(sqlGetString(res, row, "stop_id"), stop_id)))
        {
            sqlFreeResults(res);
            misTrc(T_FLOW, "Okay to release picks for stop, '%s'.", stop_id);
            return (BOOLEAN_TRUE);
        }
        else if (rownum > num_stops_to_release)
        {
           sqlFreeResults(res);
           misTrc(T_FLOW, "Unable to release picks for stop, '%s'.", stop_id);
           return (BOOLEAN_FALSE);
        }
    }
    sqlFreeResults(res);
    misTrc(T_FLOW, "Unable to release picks for stop, '%s'.", stop_id);
    return (BOOLEAN_FALSE);

} /* End sCheckStopRelease */


/*
 *    sReleaseHeldPicks sets picks and replenishments identified by the
 *                      where clause to the "to_status" value 
 *
 */

static long sReleaseHeldPicks(long pckwrk_status,
                              char *to_pcksts,
                              char *pick_where_clause,
                              char *replen_where_clause)
{
    
    mocaDataRes   *rplwrk_res;
    mocaDataRow   *row;

    long ret_status = eOK;
    char buffer [1000];

    memset(buffer, 0, sizeof(buffer));
   
    if (pckwrk_status != eDB_NO_ROWS_AFFECTED)
    {

        misTrc(T_FLOW, "Changing pick status to %s ", to_pcksts);

        sprintf(buffer,
                "update pckwrk "
                "   set pcksts = '%s' "
                " where %s "
                "   and pcksts = '%s' ",
                to_pcksts,
                pick_where_clause,
                PCKSTS_HOLD);

        ret_status = sqlExecStr(buffer, NULL);

        if (eOK != ret_status && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            return(ret_status);
        }
    }

    /* Now go check for replenishments */
    sprintf(buffer,
            "select rplref "
            "  from rplwrk "
            " where %s "
            "   and pcksts = '%s' ",
            replen_where_clause,
            PCKSTS_HOLD);

    ret_status = sqlExecStr(buffer, &rplwrk_res);
    if (ret_status == eOK)
    {
        misTrc(T_FLOW, "Found replenishments, changing pcksts for replens. ");

        for (row = sqlGetRow(rplwrk_res); row; row = sqlGetNextRow(row))
        {
            sprintf(buffer,
                    "change emergency replenishment pick status "
                    "  where rplref = '%s' and pcksts = '%s'",
                    sqlGetString(rplwrk_res, row, "rplref"),
                    to_pcksts);

            ret_status = srvInitiateInline(buffer, NULL);
            if (eOK != ret_status && ret_status != eDB_NO_ROWS_AFFECTED)
            {
                sqlFreeResults(rplwrk_res);
                return (ret_status);
            }
        }
    }
    sqlFreeResults(rplwrk_res);
    return(BOOLEAN_TRUE);
}

LIBEXPORT
RETURN_STRUCT *varChangeHeldPickStatus(char *ship_id_i,
                                       char *wkonum_i,
                                       char *client_id_i,
                                       char *wkorev_i,
                                       char *to_pcksts_i,
				       char *schbat_i,
                                       char *trlr_id_i,
                                       char *car_move_id_i,
                                       char *stop_id_i,
                                       char *wrkref_i,
                                       char *pckgr1_i,
                                       moca_bool_t *bypass_stop_seq_i)
{
    RETURN_STRUCT *RetPtr = NULL;
    mocaDataRes   *res, *stop_res, *pckwrk_res;
    mocaDataRow   *row, *stop_row;
    static mocaDataRes   *polres = NULL;
    mocaDataRow   *polrow;

    char ship_id     [SHIP_ID_LEN + 1];
    char wkonum      [WKONUM_LEN + 1];
    char client_id   [CLIENT_ID_LEN + 1];
    char wkorev      [WKOREV_LEN + 1];
    char to_pcksts   [PCKSTS_LEN + 1];
    char schbat      [SCHBAT_LEN + 1];
    char trlr_id     [TRLR_ID_LEN + 1];
    char car_move_id [CAR_MOVE_ID_LEN + 1];
    char stop_id     [STOP_ID_LEN + 1];
    char check_stop_id [STOP_ID_LEN + 1];
    char wrkref      [WRKREF_LEN + 1];
    char pckgr1      [PCKGR1_LEN + 1];

    char buffer[3000];
    char pick_where_clause[2000];
    char replen_where_clause[2000];
    char stop_where_clause[2000];
    char tmp_where_clause[2000];
    long status, pckwrk_status;
    long num_stops_to_release = -1;
    moca_bool_t bypass_stop_seq = BOOLEAN_FALSE; 
    long allow_stop_to_release = BOOLEAN_TRUE;
    moca_bool_t by_wrkref = BOOLEAN_FALSE;
    moca_bool_t by_ship_id = BOOLEAN_FALSE;
    moca_bool_t by_pckgr1 = BOOLEAN_FALSE;
    moca_bool_t by_schbat = BOOLEAN_FALSE;
    moca_bool_t by_stop_id = BOOLEAN_FALSE;
    moca_bool_t by_car_move_id = BOOLEAN_FALSE;
    moca_bool_t by_trlr_id = BOOLEAN_FALSE;

    /* Initialize local variables */

    memset (ship_id, 0, sizeof (ship_id));
    memset (wkonum, 0, sizeof (wkonum));
    memset (client_id, 0, sizeof (client_id));
    memset (wkorev, 0, sizeof (wkorev));
    memset (schbat, 0, sizeof(schbat));
    memset (to_pcksts, 0, sizeof (to_pcksts));
    memset (buffer, 0, sizeof (buffer));
    memset (pick_where_clause, 0, sizeof (pick_where_clause));
    memset (replen_where_clause, 0, sizeof (replen_where_clause));
    memset (stop_where_clause, 0, sizeof (stop_where_clause));
    memset (tmp_where_clause, 0, sizeof (tmp_where_clause));
    memset (trlr_id, 0, sizeof(trlr_id));
    memset (car_move_id, 0, sizeof(car_move_id));
    memset (check_stop_id, 0, sizeof(check_stop_id));
    memset (stop_id, 0, sizeof(stop_id));
    memset (wrkref, 0, sizeof(wrkref));
    memset (pckgr1, 0, sizeof(pckgr1));

    /* Verify required arguments are present */
    if (bypass_stop_seq_i)
        bypass_stop_seq = *bypass_stop_seq_i;

    if (ship_id_i && misTrimLen(ship_id_i, SHIP_ID_LEN))
        misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);

    if (wkonum_i && misTrimLen(wkonum_i, WKONUM_LEN))
    {
        misTrimcpy(wkonum, wkonum_i, WKONUM_LEN);

        status = appGetClient(client_id_i, client_id);
        if (eOK != status)
            return (APPInvalidArg(client_id_i, "client_id"));

        if (!wkorev_i || !misTrimLen(wkorev_i, WKOREV_LEN))
            return (APPMissingArg("wkorev"));

        misTrimcpy(wkorev, wkorev_i, WKOREV_LEN);

        /* We have a work order so we can ignore the stop sequence checks. */
        bypass_stop_seq = BOOLEAN_TRUE;
        misTrc(T_FLOW, "This is for a work order - bypassing stop "
                       "sequence check. ");
    }

    if (schbat_i && misTrimLen(schbat_i, SCHBAT_LEN))
	misTrimcpy(schbat, schbat_i, SCHBAT_LEN);

    if (trlr_id_i && misTrimLen(trlr_id_i, TRLR_ID_LEN))
        misTrimcpy(trlr_id, trlr_id_i, TRLR_ID_LEN);

    if (car_move_id_i && misTrimLen(car_move_id_i, CAR_MOVE_ID_LEN))
        misTrimcpy(car_move_id, car_move_id_i, CAR_MOVE_ID_LEN);

    if (stop_id_i && misTrimLen(stop_id_i, STOP_ID_LEN))
        misTrimcpy(stop_id, stop_id_i, STOP_ID_LEN);

    if (wrkref_i && misTrimLen(wrkref_i, WRKREF_LEN))
        misTrimcpy(wrkref, wrkref_i, WRKREF_LEN);

    if (pckgr1_i && misTrimLen(pckgr1_i, PCKGR1_LEN))
        misTrimcpy(pckgr1, pckgr1_i, PCKGR1_LEN);

    /* If they didn't pass any values that we can use, return an error */
    if (!misTrimLen(wkonum, WKONUM_LEN) && 
	!misTrimLen(ship_id, SHIP_ID_LEN) &&
	!misTrimLen(schbat, SCHBAT_LEN) &&
        !misTrimLen(trlr_id, TRLR_ID_LEN) &&
        !misTrimLen(car_move_id, CAR_MOVE_ID_LEN) &&
        !misTrimLen(stop_id, STOP_ID_LEN) &&
        !misTrimLen(wrkref, WRKREF_LEN) &&
        !misTrimLen(pckgr1, PCKGR1_LEN))
        return (APPMissingArg("ship_id"));

    if (to_pcksts_i && misTrimLen(to_pcksts_i, PCKSTS_LEN))
        misTrimcpy(to_pcksts, to_pcksts_i, PCKSTS_LEN);
    else
        misTrimcpy(to_pcksts, PCKSTS_PENDING, PCKSTS_LEN);

    /* If we have not read the policy, then get this now ... */

    misTrc(T_FLOW, "Getting %s>>%s>>%s policy information ... ", 
                    POLCOD_PROC_PICK_REL,
                    POLVAR_MISC,
                    POLVAL_ALLOW_SEQ_STOP_RELEASE);

    if (polres == NULL)
    {
        sprintf(buffer,
                "select rtnum1 "
                "  from poldat "
                " where polcod = '%s' "
                "   and polvar = '%s' "
                "   and polval = '%s' ",
                POLCOD_PROC_PICK_REL,
                POLVAR_MISC,
                POLVAL_ALLOW_SEQ_STOP_RELEASE);

        status = sqlExecStr(buffer, &polres);

        if (eOK != status && eDB_NO_ROWS_AFFECTED != status)
        {
            return (srvResults(status, NULL));
        }

        if (eDB_NO_ROWS_AFFECTED == status)
        {
            sqlFreeResults(polres);
            polres = NULL;
        }
        else
        {
            misFlagCachedMemory((OSFPTR) sqlFreeResults, polres);
        }
    }

    /* The way this works is, if rtnum1 is:                               */
    /*        NULL, we'll release all picks and ignore the stop seq.      */
    /*        "0", we won't release any picks unless the bypass_stop_seq  */
    /*             value is True                                          */
    /*        "1 or more", we'll release shipments for stops in sequence. */

    if (polres)
    {
        polrow = sqlGetRow(polres);

        if (!sqlIsNull(polres, polrow, "rtnum1"))
            num_stops_to_release = sqlGetLong(polres, polrow, "rtnum1");
        else
        {
            misTrc(T_FLOW, "Policy rtnum1 is null - bypassing stop sequence "
                           "check. ");
            bypass_stop_seq = BOOLEAN_TRUE;
        }
    }
    else
    {
        bypass_stop_seq = BOOLEAN_TRUE;
        misTrc(T_FLOW, "Policy is missing - bypassing stop sequence check. ");
    }

    /* 
     * First, get a list of the ones that we will be attempting to change 
     * for the return list.
     */

    if (misTrimLen(wrkref, WRKREF_LEN))
    {
        by_wrkref = BOOLEAN_TRUE;

        sprintf(pick_where_clause,
                " wrkref = '%s' ",
                wrkref);

        sprintf(replen_where_clause,
                " ship_line_id in (select ship_line_id "
                "                    from pckwrk "
                "                   where wrkref = '%s' ) ",
                wrkref);

        misTrc(T_FLOW, "Attempting to change pick status for wrkref, '%s' ",
                        wrkref);

        /* See if this wrkref is for a work order, in which case we'll */
        /* bypass the stop sequence processing. */

        sprintf(buffer,
                "select wkonum "
                "  from pckwrk "
                " where wrkref = '%s' ",
                wrkref);

        status = sqlExecStr(buffer, &res);

        if (eOK != status)
        {
            sqlFreeResults(res);
            return (srvResults(status, NULL));
        }
        else
        {
            row = sqlGetRow(res);
            if (!sqlIsNull(res, row, "wkonum"))
            {
                bypass_stop_seq = BOOLEAN_TRUE;
                misTrc(T_FLOW, "Wrkref is for a work order - bypassing stop "
                               "sequence check. ");
            }
            else
                misTrc(T_FLOW, "Wrkref is not for a work order. ");

            sqlFreeResults(res);
        }
    }
    else if (misTrimLen(ship_id, SHIP_ID_LEN))
    {
        by_ship_id = BOOLEAN_TRUE;

        sprintf(pick_where_clause,
                " ship_id = '%s' ",
                ship_id);

        sprintf(replen_where_clause,
                "%s",
                pick_where_clause);

        misTrc(T_FLOW, "Attempting to change pick status for held picks for "
                       "shipment, '%s' ",
                        ship_id);
    }
    else if (misTrimLen(schbat, SCHBAT_LEN))
    {
        by_schbat = BOOLEAN_TRUE;
 
        sprintf(pick_where_clause,
		" schbat = '%s' ",
		schbat);

        sprintf(replen_where_clause,
                "%s",
                pick_where_clause);

        misTrc(T_FLOW, "Attempting to change pick status for held picks for "
                       "shipments allocated in schedule batch, '%s' ",
                        schbat);
    }
    else if (misTrimLen(pckgr1, PCKGR1_LEN))
    {
        by_pckgr1 = BOOLEAN_TRUE;

        sprintf(pick_where_clause,
                " ship_line_id in (select ship_line_id "
                "                    from shipment_line "
                "                   where pckgr1 = '%s') ",
                pckgr1);

        sprintf(replen_where_clause,
                "%s",
                pick_where_clause);

        misTrc(T_FLOW, "Attempting to change pick status for all "
                       " held picks for shipment lines assigned to pckgr1, "
                       " '%s' ",
                       pckgr1);
    }
    else if (misTrimLen(wkonum, WKONUM_LEN))
    {
        sprintf(pick_where_clause,
                "     wkonum = '%s' "
                " and client_id = '%s' "
                " and wkorev = '%s' ",
                wkonum,
                client_id,
                wkorev);

        sprintf(replen_where_clause,
                "%s",
                pick_where_clause);

        misTrc(T_FLOW, "Attempting to change pick status for held picks for "
                       "work order, '%s-%s-%s' ",
                        wkonum, client_id, wkorev);
    }
    else if (misTrimLen(stop_id, STOP_ID_LEN))
    {
        by_stop_id = BOOLEAN_TRUE;

        sprintf(pick_where_clause,
                " ship_id in (select ship_id "
                "               from shipment "
                "              where stop_id = '%s') ",
                stop_id);

        sprintf(replen_where_clause,
                "%s",
                pick_where_clause);

        misTrc(T_FLOW, "Attempting to change pick status for all "
                       " held picks for shipments assigned to stop, "
                       " '%s' ",
                       stop_id);
    }
    else if (misTrimLen(car_move_id, CAR_MOVE_ID_LEN))
    {
        by_car_move_id = BOOLEAN_TRUE;

        sprintf(pick_where_clause,
                " ship_id in (select ship_id "
                "               from shipment, "
                "                    stop      "
                "              where shipment.stop_id = stop.stop_id "
                "                and stop.car_move_id = '%s' ",
                car_move_id);

        sprintf(replen_where_clause,
                "%s",
                pick_where_clause);

        sprintf(tmp_where_clause, ") ");

        misTrc(T_FLOW, "Attempting to change pick status for all "
                       "held picks for shipments assigned to stops "
                       "on carrier move, "
                       " '%s' ",
                       car_move_id);
    }
    else if (misTrimLen(trlr_id, TRLR_ID_LEN))
    {
        by_trlr_id = BOOLEAN_TRUE;

        sprintf(pick_where_clause,
                " ship_id in (select ship_id "
                "               from shipment, "
                "                    stop,     "
                "                    car_move  "
                "              where shipment.stop_id = stop.stop_id "
                "                and stop.car_move_id = car_move.car_move_id "
                "                and car_move.trlr_id = '%s' ",
                trlr_id);

        sprintf(replen_where_clause,
                "%s",
                pick_where_clause);

        sprintf(tmp_where_clause, ") ");

        misTrc(T_FLOW, "Attempting to change pick status for all "
                       "held picks for shipments assigned to stops "
                       "on trailer, "
                       " '%s' ",
                       trlr_id);
    }

    /* If we're working with stop sequences here and they've set */
    /* the rtnum1 to 0, we're not going to change the picks - instead, */
    /* we'll let them manually release them.  */

    if ((!bypass_stop_seq) && (num_stops_to_release == 0))
    {
         misTrc(T_FLOW, "Rtnum1 of policy is set to 0 and the bypass_stop_seq "
                        "value is false - pcksts will not be updated "
                        "- Exiting!. ");
         return (srvResults(eOK, NULL));
    }

    /* We want to make sure that the picks are actually in a Held status.*/

    /* SI BRP 9/28/05 - Modified code to ignore index on pcksts by */
    /* adding a null string to pcksts.  This is causing a large  */
    /* performance problem when closing packages. */

    sprintf(buffer,
            "select wrkref "
            "  from pckwrk "
            " where %s %s "
            "   and pcksts||''  = '%s' ",
            pick_where_clause,
            tmp_where_clause ? tmp_where_clause : "",
            PCKSTS_HOLD);

    pckwrk_status = sqlExecStr(buffer, &pckwrk_res);

    if (eOK != pckwrk_status && 
	eDB_NO_ROWS_AFFECTED != pckwrk_status)
    {
        sqlFreeResults(pckwrk_res);
        return (srvResults(pckwrk_status, NULL));
    }
    else if (pckwrk_status == eDB_NO_ROWS_AFFECTED)
        misTrc(T_FLOW, "No picks in Held status, "
                       "but there may be replenishments to change. ");

    /* If bypass_stop_seq is false, we have to get the stops and determine */
    /* from there if we can release the picks for this shipment. */

    if (!bypass_stop_seq)
    {

        misTrc(T_FLOW, "Checking the stop sequence to see if the picks "
                       "can be 'released' from their Hold status. The policy "
                       "states that we can release %d stops ahead. ", 
                       num_stops_to_release);

        if (by_wrkref)
        {
            sprintf(stop_where_clause,
                    " ship_id in (select ship_id "
                    "               from pckwrk "
                    "              where wrkref = '%s') ",
                    wrkref);
        }
        else if (by_ship_id)
        {
            sprintf(stop_where_clause,
                    " ship_id = '%s' ",
                    ship_id);
        }
        else if (by_schbat)
        {
            sprintf(stop_where_clause,
                    " ship_id in (select ship_id "
                    "               from pckwrk "
                    "              where schbat = '%s') ",
                    schbat);
        }
        else if (by_pckgr1)
        {
            sprintf(stop_where_clause,
                    " ship_id in (select distinct ship_id "
                    "               from shipment_line "
                    "              where pckgr1 = '%s') ",
                    pckgr1);
        }
        else if (by_stop_id)
        {
            sprintf(stop_where_clause,
                    " stop_id = '%s' ",
                    stop_id);
        }
        else if (by_car_move_id)
        {
            sprintf(stop_where_clause,
                    " stop_id in (select stop_id "
                    "               from stop "
                    "              where car_move_id = '%s') ",
                    car_move_id);
        }
        else if (by_trlr_id)
        {
            sprintf(stop_where_clause,
                    " stop_id in "
                    "         (select stop_id "
                    "            from stop, "
                    "                 car_move "
                    "           where stop.car_move_id = car_move.car_move_id "
                    "             and car_move.trlr_id = '%s') ",
                    trlr_id);
        }

        sprintf(buffer,
                "select distinct stop_id "
                "  from shipment "
                " where %s ",
                stop_where_clause);

        status = sqlExecStr(buffer, &stop_res);

        if (eOK != status &&
            eDB_NO_ROWS_AFFECTED != status)
        {
            sqlFreeResults(pckwrk_res);
            sqlFreeResults(stop_res);
            return (srvResults(status, NULL));
        }
        
        for (stop_row = sqlGetRow(stop_res); stop_row; 
                                             stop_row = sqlGetNextRow(stop_row))
        {
            if (!sqlIsNull(stop_res, stop_row, "stop_id"))
            {
                misTrimcpy(check_stop_id, 
                           sqlGetString(stop_res, stop_row, "stop_id"), 
                                        STOP_ID_LEN);

                allow_stop_to_release = sCheckStopRelease(check_stop_id, 
                                                      num_stops_to_release);
 
                if (allow_stop_to_release == BOOLEAN_TRUE)
                {
                    if (by_car_move_id || by_trlr_id)
                    {
                        memset (tmp_where_clause, 0, sizeof(tmp_where_clause));

                        sprintf (tmp_where_clause, pick_where_clause);

                        sprintf (buffer, " and stop.stop_id = '%s' ) ", 
                                         check_stop_id);

                        strcat(tmp_where_clause, buffer);

                        status = sReleaseHeldPicks(pckwrk_status,
                                                   to_pcksts,
                                                   tmp_where_clause,
                                                   tmp_where_clause);
                    }
                    else if(by_schbat)
                    {
                       memset (tmp_where_clause, 0, sizeof(tmp_where_clause));

                       sprintf (tmp_where_clause, pick_where_clause);

                       sprintf (buffer, " and ship_line_id in "
                                        "        (select "
                                        "         shipment_line.ship_line_id "
                                        "           from shipment, "
                                        "                shipment_line "
                                        "          where shipment.ship_id = "
                                        "                shipment_line.ship_id "
                                        "            and shipment.stop_id = "
                                        "                             '%s' )",
                                        check_stop_id);

                       strcat(tmp_where_clause, buffer);

                       status = sReleaseHeldPicks(pckwrk_status,
                                                  to_pcksts,
                                                  tmp_where_clause,
                                                  tmp_where_clause);
                    }
                    else if(by_pckgr1)
                    {
                       memset (tmp_where_clause, 0, sizeof(tmp_where_clause));

                       sprintf (buffer, " ship_line_id in "
                                        "        (select "
                                        "         shipment_line.ship_line_id "
                                        "           from shipment, "
                                        "                shipment_line "
                                        "          where shipment.ship_id = "
                                        "                shipment_line.ship_id "
                                        "            and shipment_line.pckgr1 "
                                        "                        = '%s' "
                                        "            and shipment.stop_id = "
                                        "                             '%s' )",
                                        pckgr1,
                                        check_stop_id);

                       sprintf(tmp_where_clause, buffer);

                       status = sReleaseHeldPicks(pckwrk_status,
                                                  to_pcksts,
                                                  tmp_where_clause,
                                                  tmp_where_clause);
                    }
                    else
                    {
                        status = sReleaseHeldPicks(pckwrk_status,
                                                   to_pcksts,
                                                   pick_where_clause,
                                                   replen_where_clause);
                    }
                }

                else
                {
                   /* what if it isn't eok??*/
                }
            }
            else
            {
                misTrc(T_FLOW, "No stop defined on the shipment. Go ahead "
                               "and change the pick status. ");

                if (by_wrkref || by_ship_id)
                {
                    status = sReleaseHeldPicks(pckwrk_status,
                                               to_pcksts,
                                               pick_where_clause,
                                               replen_where_clause);
                }
                else if(by_schbat)
                {
                   memset (tmp_where_clause, 0, sizeof(tmp_where_clause));

                   sprintf (tmp_where_clause, pick_where_clause);

                   sprintf (buffer, " and ship_line_id in "
                                    "   (select shipment_line.ship_line_id, "
                                    "      from shipment, "
                                    "           shipment_line, "
                                    "           pckwrk "
                                    "     where shipment.ship_id = "
                                    "              shipment_line.ship_id "
                                    "       and shipment_line.ship_line_id =  "
                                    "                     pckwrk.ship_line_id "
                                    "            and pckwrk.schbat = '%s' "
                                    "            and shipment.stop_id is null)",
                                    schbat);

                       strcat(tmp_where_clause, buffer);

                       status = sReleaseHeldPicks(pckwrk_status,
                                                  to_pcksts,
                                                  tmp_where_clause,
                                                  tmp_where_clause);
                }
            }

        } /* End for loop */
        sqlFreeResults(stop_res);
    }

    /* If bypass_stop_seq is true, then just release the picks. */

    if (bypass_stop_seq)
    {
        misTrc(T_FLOW, "Bypassing stop sequence check, "
                       "changing pick status... ");

        if (by_car_move_id || by_trlr_id)
        {
            sprintf (buffer, ") ");
            strcat(pick_where_clause, buffer);
            strcat(replen_where_clause, buffer);
        }
        /* Now actually perform the update */

        status = sReleaseHeldPicks(pckwrk_status,
                                   to_pcksts,
                                   pick_where_clause,
                                   replen_where_clause);
    }

    if (pckwrk_status == eOK)
        RetPtr = srvAddSQLResults(pckwrk_res, pckwrk_status);
    else
    {
        sqlFreeResults(pckwrk_res);
        RetPtr = srvResults(eOK, NULL);
    }

    return (RetPtr);
}
