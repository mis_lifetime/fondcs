/*#START***********************************************************************
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved
 *#END************************************************************************/

#include <moca_app.h>

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

#include "rflib.h"
#include "applib.h"

#define MSG_HDR "RFLIB: (rfGetRFNextDepositLoad) "

/*  Setting the devcod length to allow for a stoloc to be sent as the 
**  inventory we looking for may not always be on the device.
*/
typedef struct /* Arguments */
{
    char devcod [STOLOC_LEN + 1];
    char usr_id [USR_ID_LEN + 1];
    char invtid [INVTID_LEN + 1];
} ARG;

typedef struct /* Results */
{
    char lodnum [LODNUM_LEN + 1];
    char lodlvl [LODLVL_LEN + 1];
    char stoloc [STOLOC_LEN + 1];
} RES;

typedef struct /* Working data */
{
    char lodnum       [LODNUM_LEN + 1];
    char subnum       [SUBNUM_LEN + 1];
    char ship_line_id [SHIP_LINE_ID_LEN + 1];
} WRK;

LIBEXPORT
RETURN_STRUCT *rfGetRFNextDepositLoad (char *devcod_i, 
                                       char *usr_id_i,
                                       char *invtid_i)
{
    RETURN_STRUCT *returnData;

    mocaDataRes   *resultSet;
    mocaDataRow   *resultRow;

    ARG           arg;
    RES           res;
    WRK           wrk;

    char          buffer [5000];
    char          tmpbuffer [5000];
    char          invtid_clause[1000];
    long          status;

    /* Initialize internal structures */

    memset (&arg, 0, sizeof (arg));
    memset (&res, 0, sizeof (res));
    memset (&wrk, 0, sizeof (wrk));

    /* Verify required arguments are present */

    if (!devcod_i || !misTrimLen (devcod_i, sizeof (arg.devcod) - 1))
    {
        return (APPMissingArg("devcod"));
    }
    strncpy (arg.devcod, devcod_i, sizeof (arg.devcod) - 1);
    misTrim (arg.devcod);

    /* Verify if any optional arguments are present */
    if (usr_id_i || misTrimLen (usr_id_i, sizeof (arg.usr_id) - 1))
    {
        strncpy (arg.usr_id, usr_id_i, sizeof (arg.usr_id) - 1);
        misTrim (arg.usr_id);
    }

    if (invtid_i || misTrimLen (invtid_i, sizeof (arg.invtid) - 1))
    {
        strncpy (arg.invtid, invtid_i, sizeof (arg.invtid) - 1);
        misTrim (arg.invtid);
    }

    /* Output informational diagnostic message */

    rfLogMsg (MSG_HDR, "Entering - (devcod) is (%s), (usr_id) is (%s), "
                       "(invtid) is (%s) ",
	      arg.devcod, arg.usr_id, arg.invtid);

    /* SQL Query to retrieve next allocated load (& load level) */
    /* on RF to deposit */

    /* If an invtid was specified, than use that in the select also. */
    /* We'll find out what type of invtid it is, then set the where_clause */
    /* accordingly. */

    memset (invtid_clause, 0, sizeof (invtid_clause));
    if (misTrimLen (arg.invtid, INVTID_LEN))
    {
        returnData = NULL;
        sprintf (buffer,
                 "get translated inventory identifier "
                 "where identifier = '%s' ",
                 arg.invtid);
    
        status = srvInitiateCommand (buffer, &returnData);
        if (status != eOK)
        {
            if (returnData) srvFreeMemory (SRVRET_STRUCT, returnData);
            return (srvResults (status, NULL));
        }
    
        resultSet = srvGetResults (returnData);
        resultRow = sqlGetRow (resultSet);
    
        if ((sqlGetString (resultSet, resultRow, "colnam") == "loducc") ||
            (sqlGetString (resultSet, resultRow, "colnam") == "lodnum"))
        {
            sprintf (invtid_clause, " and nxt.lodnum = '%s' ", 
                     arg.invtid);
        }
        else if ((sqlGetString (resultSet, resultRow, "colnam") == "subucc") ||
                 (sqlGetString (resultSet, resultRow, "colnam") == "subnum"))
        {
            sprintf (invtid_clause, "and nxt.subnum = '%s' ",
                     arg.invtid);
        }
        else
        {
            sprintf (invtid_clause, "and nxt.dtlnum = '%s' ",
                     arg.invtid);
        }
        srvFreeMemory (SRVRET_STRUCT, returnData);
        returnData = NULL;
    }

    if (!strlen(arg.usr_id))
        sprintf (buffer,
            "select "
            "    decode (nxt.lodlvl, '%s', nxt.dtlnum, '%s', nxt.subnum, "
            "            nxt.lodnum) resnum, "
            "    nxt.lodlvl, "
            "    nxt.pndloc stoloc, "
            "    nxt.lodnum, "
            "    nxt.subnum, "
            "    nxt.ship_line_id "
            "from "
            "    nxtloc_view nxt, "
            "    locmst      loc "
            "where "
            "    nxt.pndloc = loc.stoloc and "
            "    nxt.stoloc = '%s' "
            "    %s "
            "order by "
            "    decode (nxt.lodlvl, '%s', 1, '%s', 2, 3), "
            "    loc.trvseq, "
            "    loc.stoloc, "
	    "    nxt.adddte desc ",
            LODLVL_DETAIL, LODLVL_SUB,
            arg.devcod,
            invtid_clause,
            LODLVL_DETAIL, LODLVL_SUB);
    else
        sprintf (buffer,
            "select "
            "    decode (nxt.lodlvl, '%s', nxt.dtlnum, '%s', nxt.subnum, "
            "            nxt.lodnum) resnum, "
            "    nxt.lodlvl, "
            "    nxt.pndloc stoloc, "
            "    nxt.lodnum, "
            "    nxt.subnum, "
            "    nxt.ship_line_id "
            "from "
            "    nxtloc_view nxt, "
            "    locmst      loc, "
            "    invlod      lod "
            "where "
            "    nxt.pndloc = loc.stoloc and "
            "    nxt.stoloc = '%s' and "
            "    nxt.lodnum = lod.lodnum and"
            "    lod.lst_usr_id = '%s' "
            "    %s "
            "order by "
            "    decode (nxt.lodlvl, '%s', 1, '%s', 2, 3), "
            "    loc.trvseq, "
            "    loc.stoloc, "
	    "    nxt.adddte desc ",
            LODLVL_DETAIL, LODLVL_SUB,
            arg.devcod,
            arg.usr_id,
            invtid_clause,
            LODLVL_DETAIL, LODLVL_SUB);
    status = sqlExecStr (buffer, &resultSet);
    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
    {
        sqlFreeResults (resultSet);
        rfLogMsg (MSG_HDR, "Error (%ld) performing SQL query [%s]",
                  status, buffer);
        return (srvResults (status, NULL));
    }
    if (status == eOK)
    {
        resultRow = sqlGetRow (resultSet);
        strcpy (res.lodnum,       rfGetString (resultSet, resultRow, "resnum"));
        strcpy (res.lodlvl,       rfGetString (resultSet, resultRow, "lodlvl"));
        strcpy (res.stoloc,       rfGetString (resultSet, resultRow, "stoloc"));
        strcpy (wrk.lodnum,       rfGetString (resultSet, resultRow, "lodnum"));
        strcpy (wrk.subnum,       rfGetString (resultSet, resultRow, "subnum"));
        strcpy (wrk.ship_line_id, rfGetString (resultSet, resultRow,
                                                               "ship_line_id"));

        rfLogMsg (MSG_HDR, "RF Device (%s) next allocated load "
                           "res_lodnum/lodlvl/stoloc/lodnum/subnum/"
                           "ship_line_id is (%s/%s/%s/%s/%s/%s)",
                           arg.devcod, res.lodnum, res.lodlvl, res.stoloc,
	                   wrk.lodnum, wrk.subnum, wrk.ship_line_id);
    }
    sqlFreeResults (resultSet);

    /* If next load on RF to deposit is not known yet, then retrieve next */
    /* detail load on RF to deposit                                       */

    if (status == eDB_NO_ROWS_AFFECTED)
    {

        /* If an invtid was specified, than use that in the select also. */
    
        memset (invtid_clause, 0, sizeof (invtid_clause));
        if (misTrimLen (arg.invtid, INVTID_LEN))
        {
            sprintf (invtid_clause, " and dtl.dtlnum = '%s' ", 
                     arg.invtid);
        }

        /* SQL Query to retrieve next detail load on RF to deposit */

        sprintf (buffer,
            "select dtl.dtlnum lodnum "
            "  from invlod lod, "
            "       invsub sub, "
            "       invdtl dtl "
            "where "
            "    lod.lodnum = sub.lodnum and "
            "    sub.subnum = dtl.subnum and "
            "    sub.idmflg = 0          and "
            "    ((sub.prmflg = 1 and sub.mvsflg = 1) or "
            "     (lod.prmflg = 1 and lod.mvlflg = 1)) and "
            "    lod.stoloc = '%s' "
            "    %s "
            "order by "
            "    dtl.dtlnum ",
            arg.devcod,
            invtid_clause);
        status = sqlExecStr (buffer, &resultSet);
        if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
        {
            sqlFreeResults (resultSet);
            rfLogMsg (MSG_HDR, "Error (%ld) performing SQL query [%s]",
                      status, buffer);
            return (srvResults (status, NULL));
        }
        if (status == eOK)
        {
            resultRow = sqlGetRow (resultSet);
            strcpy (res.lodnum, rfGetString (resultSet, resultRow, "lodnum"));
            strcpy (res.lodlvl, LODLVL_DETAIL);

            rfLogMsg (MSG_HDR, "RF Device (%s) next detail load is (%s)",
                arg.devcod, res.lodnum);
        }
        sqlFreeResults (resultSet);
    }

    /* If next load on RF to deposit is not known yet, then retrieve next */
    /* subload on RF to deposit                                           */

    if (status == eDB_NO_ROWS_AFFECTED)
    {
        /* If an invtid was specified, than use that in the select also. */
    
        memset (invtid_clause, 0, sizeof (invtid_clause));
        if (misTrimLen (arg.invtid, INVTID_LEN))
        {
            sprintf (invtid_clause, " and sub.subnum = '%s' ", 
                     arg.invtid);
        }

        /* SQL Query to retrieve next subload on RF to deposit */

        sprintf (buffer,
            "select "
            "    sub.subnum lodnum "
            "from "
            "    invlod lod, "
            "    invsub sub "
            "where "
            "    lod.lodnum = sub.lodnum and "
            "    (lod.prmflg = 1 and lod.mvlflg = 1) and "
            "    lod.stoloc = '%s' "
            "    %s "
            "order by "
            "    sub.subnum ",
            arg.devcod,
            invtid_clause);
        status = sqlExecStr (buffer, &resultSet);
        if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
        {
            sqlFreeResults (resultSet);
            rfLogMsg (MSG_HDR, "Error (%ld) performing SQL query [%s]",
                      status, buffer);
            return (srvResults (status, NULL));
        }
        if (status == eOK)
        {
            resultRow = sqlGetRow (resultSet);
            strcpy (res.lodnum, rfGetString (resultSet, resultRow, "lodnum"));
            strcpy (res.lodlvl, LODLVL_SUB);

            rfLogMsg (MSG_HDR, "RF Device (%s) next subload lodnum is (%s)",
                arg.devcod, res.lodnum);
        }
        sqlFreeResults (resultSet);
    }

    /* If next load on RF to deposit is not known yet, then retrieve next */
    /* load on RF to deposit                                              */

    memset (tmpbuffer, 0, sizeof(tmpbuffer));
    if (strlen (arg.usr_id))
	sprintf (tmpbuffer, " and lst_usr_id = '%s' ", arg.usr_id);

    if (status == eDB_NO_ROWS_AFFECTED)
    {
        /* If an invtid was specified, than use that in the select also. */
    
        memset (invtid_clause, 0, sizeof (invtid_clause));
        if (misTrimLen (arg.invtid, INVTID_LEN))
        {
            sprintf (invtid_clause, " and lodnum = '%s' ", 
                     arg.invtid);
        }


        /* SQL Query to retrieve next load on RF to deposit */

        sprintf (buffer,
            "select "
            "    lodnum "
            "from "
            "    invlod "
            "where "
            "    stoloc = '%s' "
            "    %s "
            " %s"
            "order by "
            "    adddte desc ",
            arg.devcod, invtid_clause, tmpbuffer);
        status = sqlExecStr (buffer, &resultSet);
        if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
        {
            sqlFreeResults (resultSet);
            rfLogMsg (MSG_HDR, "Error (%ld) performing SQL query [%s]",
                      status, buffer);
            return (srvResults (status, NULL));
        }
        if (status == eOK)
        {
            resultRow = sqlGetRow (resultSet);
            strcpy (res.lodnum, rfGetString (resultSet, resultRow, "lodnum"));
            strcpy (res.lodlvl, LODLVL_LOAD);

            rfLogMsg (MSG_HDR, "RF Device (%s) next load lodnum is (%s)",
                arg.devcod, res.lodnum);
        }
        sqlFreeResults (resultSet);
    }

    /* If next RF load to deposit does not exist, then return with normal */
    /* success */

    if (status == eDB_NO_ROWS_AFFECTED)
    {

        /* if we had specified a specific invtid, and we couldnt find it in
           any of the selects above, then error.  Otherwise return normal. */
        if (misTrimLen (arg.invtid, INVTID_LEN))
        {
            rfLogMsg (MSG_HDR, "RF Device (%s) next invtid (%s) to deposit "
                               "not found. ",
                      arg.devcod, arg.invtid);
            return (srvResults (eINT_INVENTORY_NOT_ON_DEVICE, NULL));
        }
        else
        {
            rfLogMsg (MSG_HDR, "RF Device (%s) next load to deposit not found",
                      arg.devcod);
            return (srvResults (eOK, NULL));
        }
    }

    /* If next RF load to deposit is going to shipping and it is a subload or */
    /* detail move, then see if everything on load is going to same location  */

    if (((strcmp (res.lodlvl, LODLVL_SUB) == 0) ||
         (strcmp (res.lodlvl, LODLVL_DETAIL) == 0)))
    {
        /* SQL Query to see if everything on load is going to same location */
	/* Changed 07/05/00 because SQL Server mod was bad - JMF */
        sprintf (buffer,
                 "select distinct nxtloc_view.lodnum lodnum, "
	         "       nxtloc_view.pndloc pndloc, "
                 "       nxtloc_view.nxtare nxtare "
                 "  from nxtloc_view "
                 " where nxtloc_view.lodnum = '%s' ",
                 wrk.lodnum);

        status = sqlExecStr (buffer, &resultSet);
        if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
        {
            sqlFreeResults (resultSet);
            rfLogMsg (MSG_HDR, "Error (%ld) performing SQL query [%s]",
                      status, buffer);
            return (srvResults (status, NULL));
        }
        if ((status == eOK) && (resultSet->NumOfRows == 1))
        {
            /* Verify that they didn't pick up some inventory with no inmov */
            /* such as with a partial transfer.                             */

            sprintf (buffer,
                     "select 'x'                           "
                     "  from invdtl,                       "
                     "       invsub                        "
                     " where invdtl.subnum = invsub.subnum "
                     "   and invsub.lodnum = '%s'          "
                     "   and ((invdtl.dtlnum not in        "
                     "             (select invmov.lodnum   "
                     "                from invmov          "
                     "               where lodlvl = '%s')) "
                     "         and                         "
                     "        (invsub.subnum not in        "
                     "             (select invmov.lodnum   "
                     "                from invmov          "
                     "               where lodlvl = '%s')))",
                     wrk.lodnum,
                     LODLVL_DETAIL,
                     LODLVL_SUB);

            status = sqlExecStr (buffer, NULL);
            if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
            {
                sqlFreeResults (resultSet);
                rfLogMsg (MSG_HDR, "Error (%ld) performing SQL query [%s]",
                          status, buffer);
                return (srvResults (status, NULL));

            }
            else if (status != eOK)
            {
                /* Didn't find anything without an invmov record. */
               
                /* See if they've defined a policy to allow load level */
                /* deposit for this area. Or, see if the inventory is all */
                /* for the same shipment. If so, we'll deposit the whole */
                /* load as one. */

                resultRow = sqlGetRow (resultSet);

                if (strlen (wrk.ship_line_id) > 0)
                {
                     strcpy (res.lodnum,
                             rfGetString (resultSet, resultRow, "lodnum"));
                     strcpy (res.lodlvl, LODLVL_LOAD);

                     rfLogMsg (MSG_HDR, "Everything on load going to same "
                                   "location");
                     sqlFreeResults (resultSet);
                }
                else
                {
                    sprintf(buffer,
                            "select * "
                            "  from poldat "
                            " where polcod = '%s' "
                            "   and polvar = '%s' "
                            "   and polval = '%s' "
                            "   and rtstr1 = '%s' ",
                            POLCOD_RDTALLOPR,
                            POLVAR_RDT_MISC,
                            POLVAL_RDT_MISC_DEPSAMLOC,
                            rfGetString (resultSet, resultRow, "nxtare"));

                     status = sqlExecStr(buffer, NULL);
 
                     if (status == eOK)
                     { 
                         strcpy (res.lodnum, 
                                 rfGetString (resultSet, resultRow, "lodnum"));
                         strcpy (res.lodlvl, LODLVL_LOAD);

                         rfLogMsg (MSG_HDR, "Everything on load going to same "
                                  "location");
                     }
	             sqlFreeResults (resultSet);
                 }  
             }
        }
        /* If next RF load to deposit is going to shipping and it is a detail */
        /* move, then see if everything on subload is going to same location  */

	else if ((strcmp (res.lodlvl, LODLVL_DETAIL) == 0))
        {
            sqlFreeResults (resultSet);

            /* SQL Query to see if everything on subload is going to same */
            /* location */
	    /* Changed 07/05/00 because SQL Server mod was bad - JMF */
            sprintf (buffer,
                     "select distinct nxtloc_view.subnum lodnum, "
		     "       nxtloc_view.pndloc pndloc "
                     "  from nxtloc_view "
                     " where nxtloc_view.subnum = '%s' ",
                     wrk.subnum);

            status = sqlExecStr (buffer, &resultSet);
            if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
            {
                sqlFreeResults (resultSet);
                rfLogMsg (MSG_HDR, "Error (%ld) performing SQL query [%s]",
                          status, buffer);
                return (srvResults (status, NULL));
            }
            if ((status == eOK) && (resultSet->NumOfRows == 1))
            {

                /* Verify that they didn't pick up some inventory with */
                /* no invmov, such as with a partial transfer.         */

                sprintf (buffer,
                         "select 'x'                           "
                         "  from invdtl                        "
                         " where invdtl.subnum = '%s'          "
                         "   and invdtl.dtlnum not in         "
                         "             (select invmov.lodnum   "
                         "                from invmov          "
                         "               where lodlvl = '%s') ",
                         wrk.subnum,
                         LODLVL_DETAIL);

                status = sqlExecStr (buffer, NULL);
                if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
                {
                    sqlFreeResults (resultSet);
                    rfLogMsg (MSG_HDR, "Error (%ld) performing SQL query [%s]",
                              status, buffer);
                    return (srvResults (status, NULL));

                }
                else if (status != eOK)
                {
                    /* Didn't find anything without an invmov record. */

                    /* See if they've defined a policy to allow load level */
                    /* deposit for this area. Or, see if the inventory is */
                    /* all for the same shipment. If so, we'll deposit the */
                    /* whole sub as one. */

                    resultRow = sqlGetRow (resultSet);

                    if (strlen (wrk.ship_line_id) > 0)
                    {
                        strcpy (res.lodnum,
                                rfGetString (resultSet, resultRow, "lodnum"));
                        strcpy (res.lodlvl, LODLVL_SUBLOAD);

                        rfLogMsg (MSG_HDR, "Everything on subload going to same"
                               " location");
                    }
                    else
                    {
                        sprintf(buffer,
                                "select * "
                                "  from poldat "
                                " where polcod = '%s' "
                                "   and polvar = '%s' "
                                "   and polval = '%s' "
                                "   and rtstr1 = '%s' ",
                                POLCOD_RDTALLOPR,
                                POLVAR_RDT_MISC,
                                POLVAL_RDT_MISC_DEPSAMLOC,
                                rfGetString (resultSet, resultRow, "nxtare"));
 
                         status = sqlExecStr(buffer, NULL);

                         if (status == eOK)
                         {
                             strcpy (res.lodnum,
                                     rfGetString (resultSet,
                                                  resultRow, "lodnum"));
                             strcpy (res.lodlvl, LODLVL_SUBLOAD);

                             rfLogMsg (MSG_HDR, "Everything on subload going "
                                            "to same location");
                         }
                     }
                }
                sqlFreeResults (resultSet);
            }
        }
    }
    /* Setup return information */

    returnData = srvResultsInit (eOK,
                                 "lodnum", COMTYP_CHAR, LODNUM_LEN,
                                 "lodlvl", COMTYP_CHAR, LODLVL_LEN,
                                 "stoloc", COMTYP_CHAR, STOLOC_LEN,
                                 NULL);

    status = srvResultsAdd (returnData,
                            res.lodnum,
                            res.lodlvl,
                            res.stoloc,
                            NULL);

    /* Normal successful completion */

    rfLogMsg (MSG_HDR, "Return information (lodnum/lodlvl/stoloc) is "
                       "(%s/%s/%s)", res.lodnum, res.lodlvl, res.stoloc);

    return (returnData);
}


