#define MOCA_ALL_SOURCE 

#include <moca.h>

#include <mocaerr.h>
#include <srvlib.h>
#include <oslib.h>
#include <applib.h>

#include <stdio.h>
#include <string.h>

#define READ_BUF 12800
#define RECV_BUF 256000

int numbytes;
char nr_term_char = 0x03;
char buf[READ_BUF];
char retbuf[RECV_BUF];
//struct addrinfo *result;
//struct addrinfo *res;
//int error;
static SOCKET_FD fd = -1; /* socket descriptor of the connect'ed socket. */

static int sGetSockFd(const SOCKET_FD fd)
{
    return (int) fd;
}

static long sConnect(SOCKET_FD *fd, const char *host, int port)
{
    const char *const fn = "sConnect";
    long retVal = !eOK;
    long attempts = 0;
    /* made static to avoid it from getting allocated on the stack all the time. */
    static char buffer[5120]; /* 5K buffer. */

    /* no matter what is the specified timeout value, we have to try connect'ing once! */
    retVal =  osTCPConnect(fd, (char *)host, (short)port);
    if (retVal != eOK)
    {
        sprintf(buffer, "osSockConnect: Cannot connect: "
            "errno(%ld), error(%s).", osSockErrno(), osSockError());
        misTrc(T_FLOW, "%s - %s", fn, buffer);

        sprintf(buffer, "Attempt to connect to host(%s) on port(%i) failed!", host, port);
        misTrc(T_FLOW, "%s - %s", fn, buffer);

    }
    else /* retVal == eOK */
    {
        sprintf(buffer, "Successfully connected to host(%s) on port(%i) fd(%i)",
            host, port, sGetSockFd(*fd));
        misTrc(T_FLOW, "%s - %s", fn, buffer);
    }

    return retVal;
}

int nr_sDataOnSocket() {
    fd_set fds;
    struct timeval mytimeout;
    int readSockVal;

    misTrc(T_FLOW, "- nr_DataOnSocket: fd(%i)", (int)sGetSockFd(fd));

    FD_ZERO(&fds);
    FD_SET((unsigned int)sGetSockFd(fd), &fds);
    mytimeout.tv_sec = 0;
    mytimeout.tv_usec = 1;

    readSockVal = select((int)fd + 1, &fds, NULL, NULL, &mytimeout);

    misTrc(T_FLOW, " - select value is: (%i)", readSockVal);

    if (readSockVal > 0) {
	misTrc(T_FLOW, "- nr_sDataOnSocket: Data on Socket");
        return 1;
    }

    misTrc(T_FLOW, "- nr_sDataOnSocket: No Data on Socket");

    return 0;
}

static long sSend(SOCKET_FD fd, const char *s, int len)
{
    const char *const fn = "sSend";
    int iAlreadySent = 0;

    misTrc(T_FLOW, "%s - input parameters: fd(%i), message(%s), message length(%i)", fn,
            (int)sGetSockFd(fd), s, len);
    while (iAlreadySent < len)
    {
        long retVal = osSockSend(fd, (char *)(s + iAlreadySent), (len -  iAlreadySent), 0);
        misTrc(T_FLOW, "%s - osSockSend retVal(%ld)", fn, retVal);
        /* it should write at least one byte! If not then we assume that socket was
         * shutdown. */
        if (retVal > 0)
        {
            /* retVal contains the number of bytes actauly sent. */
            iAlreadySent += retVal;
        }
        else
        {
            /* Error sending mesage to the server, what should the client do now? */
            int err = osSockErrno();

            misTrc(T_FLOW, "%s - Error writing to socket (%i)", fn, sGetSockFd(fd));

            misTrc(T_FLOW, "%s - Error returned: osSockSend: errno(%ld), errmsg(%s)", fn,
                err, osSockError());
            return !eOK;
        }
    }
    misTrc(T_FLOW, "%s - sucessfully sent msg(%s) of length (%i)", fn, s, iAlreadySent);

    return eOK;
}

static long recv_s() {

    long totBytes = 0;
    int kill_loop = 0;

    /*
    int i = 0;
    do {
 
        misTrc(T_FLOW, "recv_s: trying socket");
        if(nr_sDataOnSocket()) break;
        
        misTrc(T_FLOW, "recv_s: socket not ready");
        usleep(1000);
        i++;
    } while (i < 901);
    */

    /*strcpy(retbuf, "RETURN: ");*/


    memset(retbuf, '\0', sizeof(retbuf));
    do {
      memset(buf, '\0', sizeof(buf));
      misTrc(T_FLOW, "recv_s: receiving");
      numbytes = osSockRecv(fd, buf, READ_BUF, 0);
      totBytes += numbytes;
      
      if(numbytes > 0) {
        strcat(retbuf, buf);
      }

      misTrc(T_FLOW, "recv_s: received %i bytes", numbytes);
      misTrc(T_FLOW, "r_msg: %s", retbuf);
      if(strchr(retbuf, nr_term_char) != NULL) 
      {
	misTrc(T_FLOW, "termchar was found!");
	kill_loop = 1;
      }
      else 
      {
	misTrc(T_FLOW, "termchar was NOT found!");
      }
    } while (kill_loop = 0);

    misTrc(T_FLOW, "recv_s: tot received %i bytes", totBytes);
    misTrc(T_FLOW, "r_msg: %s", retbuf);
}

LIBEXPORT
RETURN_STRUCT *usrSendReceiveNewRateMessage(char *malv_ip, char *malv_port, char *s_msg)
{
    char sbuf[100000];
	int i_malv_port = 0;
    misTrc(T_FLOW, "s_msg: %s", s_msg);

    misTrc(T_FLOW, "Connection to %s:%s", malv_ip, malv_port);
	
	i_malv_port = atoi(malv_port);
	if (sConnect(&fd, malv_ip, i_malv_port) != eOK) {
        misTrc(T_FLOW, "Failed Socket Connection");
        return(srvResults(99200, NULL));
    }

    memset(sbuf, '\0', sizeof(sbuf));
    strcpy(sbuf, s_msg);
    strcat(sbuf, &nr_term_char);
    
    misTrc(T_FLOW, "Sending message: %s", sbuf);
	
	if (sSend(fd, sbuf, strlen(sbuf)) != eOK) {
        misTrc(T_FLOW, "Failed sending message");
        return(srvResults(99201, NULL));
    }

    /*
    misTrc(T_FLOW, "Sending termchar");
    if (send_s(&nr_term_char) < 0) {
        misTrc(T_FLOW, "Failed sending message");
        return(srvResults(99201, NULL));
    }
    */

    misTrc(T_FLOW, "Getting Results");
    recv_s();
    
    misTrc(T_FLOW, "r_msg: %s", retbuf);

    if (osSockClose(fd) != eOK) {
        misTrc(T_FLOW, "Failed to close socket");
    }

    return srvResults(eOK,
                      "r_msg", COMTYP_CHAR, RECV_BUF, retbuf,
                      NULL);
}
