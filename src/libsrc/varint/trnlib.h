/*#START**********************************************************
 * McHugh Software International
 * Copyright 1999
 * Brookfield, WI  U.S.A.
 * All rights reserved.
 *
 *#END************************************************************/

#ifndef TRNLIB_H
#define TRNLIB_H

#include <applib.h>

#define LVL_DETAIL_PARTIAL  1
#define LVL_DETAIL          2
#define LVL_SUBLOAD         3
#define LVL_LOAD            4
#define LVL_LOCATION        5

typedef struct _resassociat {
    char arecod[ARECOD_LEN+1];
    char value[RESTRN_LEN+1];
    char restyp[RESTYP_LEN+1];

    struct _resassociat *next;
} RES_ASSOCIATE;

typedef struct _schbat_res {
    char schbat[SCHBAT_LEN+1];
    RES_ASSOCIATE *resAssc; 
                                   
    struct _schbat_res *next;
} SCHBAT_RES;

typedef struct _loc_info {
    char stoloc[STOLOC_LEN+1];
    char locsts[LOCSTS_LEN+1];
    long cipflg;
    double maxqvl;
    double pndqvl;
    double curqvl;
} LOC_INFO;

typedef struct _are_info {
    char arecod[ARECOD_LEN+1];
    char pckcod[PCKCOD_LEN+1];
    char loccod[LOCCOD_LEN+1];
    long lodflg;
    long sigflg;
    long praflg;
    long conflg;
    long fifflg;
    long subflg;
    long dtlflg;
    long expflg;
    long adjflg;
    long stgflg;
    long shpflg;
    long wipflg;
    long wip_expflg;
} ARE_INFO;

struct _dtllist {
    char dtlnum[DTLNUM_LEN+1];
    struct _dtllist *next;
};

typedef struct _dtl_info {
    char   dtlnum[DTLNUM_LEN+1];
    char   subnum[SUBNUM_LEN+1];
    char   prtnum[PRTNUM_LEN+1];
    char   prt_client_id[CLIENT_ID_LEN+1];
    char   orgcod[ORGCOD_LEN+1];
    char   invsts[INVSTS_LEN+1];
    char   revlvl[REVLVL_LEN+1];
    char   lotnum[LOTNUM_LEN+1];
    char   fifdte[MOCA_STD_DATE_LEN+1];
    long   phdflg;
    long   untqty;
    double catch_qty;
    long   untcas;
    long   untpak;
    long   alcflg;
    char   ship_line_id[SHIP_LINE_ID_LEN+1];
    long   preSplitQuantity;
    
    struct _dtllist *split;
} DTL_INFO;

typedef struct _lod_info {
    char   lodnum[LODNUM_LEN+1];
    char   stoloc[STOLOC_LEN+1];
    long   prmflg;
    long   mvlflg;
    double tot_catch_qty;          /* These are used to add catch quantity */
    char   a_dtlnum[DTLNUM_LEN+1]; /* to one of the details on the load */
} LOD_INFO;

typedef struct _sub_info {
    char   subnum[SUBNUM_LEN+1];
    char   lodnum[LODNUM_LEN+1];
    long   prmflg;
    long   phyflg;
    long   mvsflg;
    long   ctnflg;
    long   idmflg;
    double tot_catch_qty;          /* These are used to update catch qty */
    char   a_dtlnum[DTLNUM_LEN+1]; /* on one of the details at move time */
} SUB_INFO;

typedef struct _pick_mov {
    char arecod[ARECOD_LEN+1];
    char rescod[RESCOD_LEN+1];
    char stoloc[STOLOC_LEN+1];
    RES_ASSOCIATE *resAssc;

    struct _pick_mov *next;
} PICK_MOV;

typedef struct _pickList {
    char pcktyp[100];
    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char invsts[INVSTS_LEN+1];
    long pckqty;
    char pakcod[PAKCOD_LEN+1];
    char cmbcod[CMBCOD_LEN+1];

    char orgcod[ORGCOD_LEN+1];
    char revlvl[REVLVL_LEN+1];
    char lotnum[LOTNUM_LEN+1];
    long untcas;
    long untpak;

    char srcloc[STOLOC_LEN+1];
    char srcare[ARECOD_LEN+1];
    char lodlvl[LODLVL_LEN+1];
    char ftpcod[FTPCOD_LEN+1];

    struct _pickList *next;
    struct _pickList *prev;

} PICK_LIST;    
    

/* Structure used by Allocate Inventory.... */
typedef struct _pckwrkstruct {
    char pcktyp[100];
    char prt_client_id[CLIENT_ID_LEN+1];
    char prtnum[PRTNUM_LEN+1];
    char invsts[INVSTS_LEN+1];
    char invsts_prg[INVSTS_PRG_LEN+1];
    long pckqty;
    char carcod[CARCOD_LEN+1];
    char srvlvl[SRVLVL_LEN+1];
    char schbat[SCHBAT_LEN+1];
    char ship_id[SHIP_ID_LEN+1];
    char ship_line_id[SHIP_LINE_ID_LEN+1];

    char wkonum[WKONUM_LEN+1];
    char wkorev[WKOREV_LEN+1];
    char wkolin[WKOLIN_LEN+1];

    char client_id[CLIENT_ID_LEN+1];
    char ordnum[ORDNUM_LEN+1];
    char stcust[ADRNUM_LEN+1];
    char rtcust[ADRNUM_LEN+1];
    char rt_adr_id[ADR_ID_LEN+1];
    char ordlin[ORDLIN_LEN+1];
    char ordsln[ORDSLN_LEN+1];
    char cmbcod[CMBCOD_LEN+1];
    char devcod[DEVCOD_LEN+1];
    char concod[CONCOD_LEN+1];

    char orgcod[ORGCOD_LEN+1];
    char revlvl[REVLVL_LEN+1];
    char lotnum[LOTNUM_LEN+1];

    long untcas;
    long untpak;
    char srcloc[STOLOC_LEN+1];
    char dstloc[STOLOC_LEN+1];

    char srcare[ARECOD_LEN+1];
    char dstare[ARECOD_LEN+1];

    /* The following 9 fields used for efficiency when
       determining how to set flags on the pick work */
    char prt_lodlvl[LODLVL_LEN+1];
    long prt_orgflg;
    long prt_revflg;
    long prt_lotflg;
    char prt_catch_cod[CATCH_COD_LEN+1];
    long area_lodflg;
    long area_subflg;
    long area_dtlflg;
    char area_loccod[LOCCOD_LEN+1];

    char lodlvl[LODLVL_LEN+1];
    char ftpcod[FTPCOD_LEN+1];
    char lodnum[LODNUM_LEN+1];
    char subnum[SUBNUM_LEN+1];
    char wrkref[WRKREF_LEN+1];

    char refval[REFVAL_LEN+1];
    short processed;
    char upccod[UPCCOD_LEN+1];
    long splflg;
    long pipflg;
    char ordtyp[ORDTYP_LEN+1];
    char marcod[MARCOD_LEN+1];
    char ordinv[ORDINV_LEN+1];

    char fifdte[DB_STD_DATE_LEN + 1];
    long julian_fifdte;  /* Fifo date in julian days */

    long rpqflg;  /* Flag to indicate if we should round up a pick */
    long maxavl;     /* Max. available for allocation... */

    long frsflg;  /* Flag to indicate if freshness date processing is applied */
    long min_shelf_hrs;  /* Maximum number of hours until the expiration date */
    char frsdte[MOCA_STD_DATE_LEN + 1];

    long alloc_cas;     /* Allocation requested untcas from som/shp detail */
    long alloc_pak;     /* Allocation requested untpak from som/shp detail */
    long alloc_pal;     /* Allocation requested untpal from som/shp detail */

    struct _pick_movs   *pckMovs;
    SCHBAT_RES          *schbatRes;

    struct _pckwrkstruct *next;
    struct _pckwrkstruct *prev;
} PCKWRK_DATA;

typedef struct _pick_movs {
    char cmbcod[CMBCOD_LEN+1];
    PCKWRK_DATA *pckWrk;
    PICK_MOV *pckMovPath;

    struct _pick_movs *prev;
    struct _pick_movs *next;
} PICK_MOVS;


/* structure used by allocate location */
typedef struct _stoloc_list {
    char    nxtloc[STOLOC_LEN+1];
    char    lodnum[LODNUM_LEN+1];
    char    lodlvl[LODLVL_LEN+1];
    struct _stoloc_list *next;
} STOLOC_LIST;

long trnConstructOperationBitmap(char *empnum, char *devcod, 
                            char *srvmod, char **oprbmp);
long trnDeallocateInventory(char *wrkref);
long trnDeallocateLocation(char *lodnum, char *subnum, char *dtlnum,
                           char *wrkref);

long trnSelectInfoByDtl(char *dtlnum,
                        DTL_INFO *dtl_info,
                        SUB_INFO *sub_info,
                        LOD_INFO *lod_info,
                        LOC_INFO *loc_info,
                        ARE_INFO *are_info);

long trnSelectInfoBySub(char *subnum,
                        SUB_INFO *sub_info,
                        LOD_INFO *lod_info,
                        LOC_INFO *loc_info,
                        ARE_INFO *are_info);

long trnSelectInfoByLod(char *lodnum,
                        LOD_INFO *lod_info,
                        LOC_INFO *loc_info,
                        ARE_INFO *are_info);
long trnSelectInfoByLoc(char *stoloc,
                        LOC_INFO *loc_info,
                        ARE_INFO *are_info);

long trnSelectAvailQty(char *stoloc,
                       char *arecod,
                       char *prtnum, 
                       char *orgcod, 
                       char *revlvl,
                       char *lotnum,
                       char *invsts,
                       long untcas,
                       long untpak,
                       long *avail);

long trnFindDetail(long level,
                   char *key_value,
                   long qty,
                   char *detail_found,
                   char *prtnum,
                   char *prt_client_id,
                   char *orgcod,
                   char *revlvl,
                   char *lotnum,
                   char *invsts,
                   char *ship_line_id,
                   long *untcas,
                   long *untpak,
                   long phyflg_i,
                   char *dstloc_i);

long trnFindDetailByPartInfo( long level,
                              char *key_value,
                              long qty,
                              char *detail_found,
                              char *prtnum,
			      char *prt_client_id,
                              char *orgcod,
                              char *revlvl,
                              char *lotnum,
                              char *invsts,
                              char *ship_line_id,
                              long *untcas,
                              long *untpak,
                              char *prtnum_i,
                              char *prt_client_id_i,
                              char *orgcod_i,
                              char *revlvl_i,
                              char *lotnum_i,
                              char *invsts_i,
                              char *ship_line_id_i,
                              long untcas_i,
                              long untpak_i,
                              long phyflg_i,
                              char *dstloc_i);

long trnMakeDetail(char *new_detail,        
                   long level,
                   char *key_value,
                   char *prtnum,
                   char *prt_client_id,
                   char *orgcod,
                   char *revlvl,
                   char *lotnum,
                   char *invsts,
                   char *ship_line_id,
                   long untpak,
                   long untcas,
                   long untqty,
                   double catch_qty,
                   long phyflg,
                   char *dstloc);

long trnMoveLoad(LOC_INFO *srcloc_info,
                 ARE_INFO *srcare_info,
                 LOD_INFO *load_info,
                 LOC_INFO *dstloc_info,
                 ARE_INFO *dstare_info,
                 char     *movecode,
                 char     *lstcod,
                 char     *lstemp,
                 long     revflg,
		 char	  *xdkref,
		 long     *xdkqty,
                 double    catch_qty);

long trnMoveSub(LOC_INFO *srcloc_info,  
		ARE_INFO *srcare_info,
		LOD_INFO *srclod_info,
		SUB_INFO *sub_info,
		LOC_INFO *dstloc_info,
		ARE_INFO *dstare_info,
		LOD_INFO *dstlod_info,
		char *movecode,
		char *lstcod,
		char *lstemp,
		long revflg,
		char *xdkref,
		long *xdkqty,
                double catch_qty);

long trnMoveDetail(LOC_INFO *srcloc_info,       
                   ARE_INFO *srcare_info,
		   LOD_INFO *srclod_info,
                   SUB_INFO *srcsub_info,
                   DTL_INFO *detail_info,
                   LOC_INFO *dstloc_info,
                   ARE_INFO *dstare_info,
                   SUB_INFO *dstsub_info,
                   char *movecode,
                   char *lstcod,
                   char *lstemp,
                   long revflg,
                   char **sublist_out,
                   char **dtllist_out,
		   char	*xdkref,
		   long *xdkqty,
                   double catch_qty);

long trnRemoveCalendarDay(char *dcsdte);
long trnRemoveCartonCode(char *ctncod);
long trnRemoveDeviceCode(char *devcod);
long trnRemoveFootprintCode(char *ftpcod);
long trnRemoveGroupCode(char *grpcod);
long trnRemoveLoad(char *lodnum);
long trnRemoveMenu(char *mentyp, char *mencod);
long trnRemoveOperationCode(char *oprcod);
long trnRemovePartNumber(char *prtnum);
long trnRemovePartConfiguration(char *client_id,
	                        char *supnum, char *pcfkey, char *pcflin);
long trnRemovePolicy(char *polcod, char *polvar, char *polval, long srtseq);
long trnRemoveRcvInvoice(char *trknum, 
	                 char *client_id,
			 char *supnum, 
			 char *invnum);
long trnRemoveRcvInvLine(char *trknum, 
	                 char *client_id, 
			 char *supnum, char *invnum,
                         char *invlin, char *invsln);
long trnRemoveRcvInvMstrHdr(char *client_id,
	                    char *supnum, 
			    char *invnum);
long trnRemoveRcvInvMstrLine(char *client_id,
	                     char *supnum, char *invnum,
                        char *invlin, char *invsln);
long trnRemoveRcvTruck(char *trknum);
long trnRemoveSalesOrder(char *ordnum);
long trnRemoveSalesOrderLine(char *ordnum,
                 char *ordlin, char *ordsln);
long trnRemoveShipOrder(char *shipid, char *shpseq,
                        char *ordnum,
                        char *wrkordflg);
long trnRemoveShipOrderLine(char *shipid, char *shpseq,
                            char *ordnum,
                            char *ordlin, char *ordsln,
                            char *wrkordflg);
long trnRemoveScheduledCount(long seqnum);

long trnSelectGenInv(char *dst, 
                     long level, 
                     char *load, 
                     long *foundLoad, 
                     char *sub, 
		     long *foundSub);

long trnSelectInfoByLoc(char *stoloc,
                        LOC_INFO *loc_info,
                        ARE_INFO *are_info);

long trnSelectInfoByLoad(char *lodnum,
                         LOD_INFO *lod_info,
                         LOC_INFO *loc_info,
                         ARE_INFO *are_info);

long trnSelectQtyOnLoad(char *load, long *quantity);

long trnSelectQtyOnSub(char *sub, long *quantity);

long trnSelectQtyOnDtl(char *dtl, long *quantity);

long trnSelStolocForLoad(char *lodnum_i, char *stage_loc, char *storage_loc);

long trnCompleteWork(long reqnum_i, 
                     char *prvloc_i,
                     char *devcod_i,
                     char *empnum_i);

long trnCreateLoad( char *lodnum_i, char *stoloc_i,
                    long prmflg_i, long unkflg_i, long mvlflg_i,
                    char *devcod_i, char *empnum_i);

long trnCreateSub( char *subnum_i, char *lodnum_i,
                   long prmflg_i, long phyflg_i, 
                   long idmflg_i, long mvsflg_i, 
                   char *devcod_i, char *empnum_i);

long trnCreateDetail( char *dtlnum_i, char *subnum_i, char *prtnum_i,
		      char *prt_client_id,
		      char *orgcod_i, char *revlvl_i, char *invsts_i,
		      char *lotnum_i, 
		      long untpak_i, long untcas_i,
		      long untqty_i, long phdflg_i,
		      char *devcod_i, char *empnum_i,
		      char *rcvkey_i, char *ftpcod_i,
		      char *fifdte_i, char *mandte_i,
                      char *cmpkey_i, char *expire_dte_i,
		      char *age_pflnam_i, double catch_qty_i);

long trnOutputDisable(void);

long trnOutputEnable(void);

long trnOutputPrint(void);

long trnReceiveByDtl(char *trknum_i, char *dtlnum_i);

long trnReceiveByLoad(char *trknum_i, char *lodnum_i);

long trnReceiveBySub(char *trknum_i, char *subnum_i);

long trnReceiveGetOverReceiveQty(long  expqty_i,
                                 long  idnqty_i,
                                 char *supnum_i,
                                 char *prtnum_i,
                                 char *prt_client_id_i);

long trnAllocateLocation(char *lodnum, char *subnum, char *dtlnum,
			 char *arecod, char *wrkzon,
			 char *stoloc, moca_bool_t trcflg, char *trcfil,
			 char *prtnum, char *prt_client_id,
			 char *lotnum, char *revlvl,
			 char *orgcod, char *invsts, long untcas,
			 long untqty,  char *fifdte, char *type,
			 char *ftpcod_i,
			 STOLOC_LIST **LocList, long untpak,
			 char *cur_bldg_id,
			 char *xdkref, long xdkqty);

long trnAllocateInventory(char *pcktyp_i, 
                          char *prtnum_i, char *prt_client_id_i, 
                          long pckqty_i, 
                          char *orgcod_i, char *revlvl_i, char *lotnum_i,
                          char *invsts_i, 
                          char *invsts_prg_i,
                          char *schbat_i,
                          char *ship_id_i, char *ship_line_id_i, 
                          char *wkonum_i, char *wkorev_i, char *wkolin_i,
                          char *carcod_i, char *srvlvl_i,
                          char *client_id_i,
                          char *ordnum_i, char *stcust_i, char *rtcust_i,
                          char *ordlin_i, char *ordsln_i,
                          char *concod_i, char *segqty_i,
                          char *stoloc_i, char *lodnum_i, char *subnum_i,
                          char *dstare_i, char *dstloc_i,
                          PCKWRK_DATA ** Picks,
                          char *srcare_i, char *pcklvl_i,
                          char *pcksts_i,
                          moca_bool_t splflg_i, 
                          moca_bool_t *ovralcflg_i,
                          long  untcas_i, long  untpak_i, long  untpal_i,
                          long min_shelf_hrs_i,
                          moca_bool_t frsflg_i,
                          char *frsdte_i,
                          char *trace_suffix_i,
                          char *pipcod_i,
                          long skip_invlkp_i);

void trnFreePickList(PCKWRK_DATA *List);
void trnFreeStolocList(STOLOC_LIST *Locs);
long trnDetermineVerification(char *prt_lodlvl,
                  char *pck_lodlvl,
                  char *arecod,
                  long lodflg,
                  long subflg,
                  long dtlflg,
                  long prtflg,
                  long locflg,
                  long qtyflg,
                  long orgflg,
                  long revflg,
                  long lotflg);

long trnInsertPckMov(char *cmbcod_i, 
		     long seqnum_i,
		     char *arecod_i,
		     char *stoloc_i,
		     char *rescod_i,
		     long arrqty_i,
		     long prcqty_i);

long trnInsertPckWrk(char *wrkref_i, char *wrktyp_i, char *schbat_i,
		     char *srcloc_i, char *dstloc_i, char *srcare_i,
		     char *dstare_i, char *ship_id_i,
		     char *ship_line_id_i, 
		     char *client_id_i,
		     char *ordnum_i, char *ordlin_i, char *ordsln_i,
		     char *stcust_i, char *concod_i,
		     char *cmbcod_i, char *lblbat_i, char *lblseq_i,
		     char *devcod_i, long pckqty_i,  long appqty_i,
		     char *pcksts_i, char *prtnum_i, char *prt_client_id_i,
		     char *orgcod_i,
		     char *revlvl_i, char *lotnum_i, char *invsts_i,
		     char *lodlvl_i, char *lodnum_i, char *subnum_i,
		     char *dtlnum_i, long untcas_i,  long untpak_i,
		     char *ftpcod_i, char *ctncod_i, char *ctnnum_i,
		     long visflg_i, long splflg_i, long locflg_i,
		     long lodflg_i, long subflg_i, long dtlflg_i,
		     long prtflg_i, long orgflg_i, long revflg_i,
		     long lotflg_i, long qtyflg_i, char *adddte_i,
		     char *cmpdte_i, char *loducc_i, char *subucc_i);

long trnGetCstFrsDte(char *prtnum_i,
                     char *client_id_i,
                     char *stcust_i,
                     char *rtcust_i,
                     char *frsdte_o);

long trnGetInvsumUntpal(char *stoloc, char *prtnum, char *prt_client_id);
void trnFreeInvsumList();

long trnGetQtyFromQvl(char  *arecod_i, char  *loccod_i,
		      char  *stoloc_i, char  *prtnum_i,
		      char  *prt_client_id_i, 
		      char  *ftpcod_i, long  *avlqty_i,
		      long  *totqty_i, char  *pcktyp_i);

void trnSetSchbat(char *schbat);
void trnClearSchbat();


long trnUCC128ApplyIdentifier(char *wrkref_i, 
                              char *lodnum_i,
                              char *subnum_i,
                              char *dtlnum_i,
                              char *dtllist_i,
                              char **sublist_o,
                              char **dtllist_o);

long trnIsValidCodeValue(char *colnam_i,
			 char *codval_i);

long movProcessLoadMove(LOC_INFO *srcloc_info,       
			ARE_INFO *srcare_info,
			LOD_INFO *load_info,
			LOC_INFO *dstloc_info,
			ARE_INFO *dstare_info,
			char *wrkref_i,
			char *lstcod_i,
			char *lst_usr_id_i,
			char *xdkref_i,
			long *xdkqty_i,
                        double catch_qty_i);

long movProcessSubMove(LOC_INFO *srcloc_info,       
		       ARE_INFO *srcare_info,
		       LOD_INFO *srclod_info,
		       SUB_INFO *sub_info,
		       LOC_INFO *dstloc_info,
		       ARE_INFO *dstare_info,
		       LOD_INFO *dstlod_info,
		       char *wrkref_i,
		       char *lstcod_i,
		       char *lst_usr_id_i,
		       char *xdkref_i,
		       long *xdkqty_i,
                       double catch_qty_i);

long movProcessDetailMove(LOC_INFO *srcloc_info,       
			  ARE_INFO *srcare_info,
			  LOD_INFO *srclod_info,
			  SUB_INFO *srcsub_info,
			  DTL_INFO *detail_info,
			  LOC_INFO *dstloc_info,
			  ARE_INFO *dstare_info,
			  SUB_INFO *dstsub_info,
			  char *wrkref,
			  char *lstcod,
			  char *lst_usr_id_i,
			  char *xdkref_i,
			  long *xdkqty_i,
                          double catch_qty_i);

long movApplyLoadToWrkref(char *wrkref_i, LOD_INFO *load);

long movApplySubToWrkref(char *wrkref_i, SUB_INFO *sub);

long movApplyDetailToWrkref(char *wrkref_i, DTL_INFO *dtl);

long movUpdatePckmovQuantities(ARE_INFO *s_area,
			       ARE_INFO *d_area,
			       LOC_INFO *s_loc,
			       LOC_INFO *d_loc,
			       DTL_INFO *dtl,
			       SUB_INFO *sub,
			       LOD_INFO *lod,
			       moca_bool_t revflg_i);

#endif
