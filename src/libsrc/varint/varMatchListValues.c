static const char *rcsid = "$Id: varAutoFullLoad.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *  DESCRIPTION  :  This functin generates a UCC cod for each un confirmed subload 
 *                  on the load no. passed , and prints label for all unconfirmed cases. 
 *
 *#END************************************************************************/

#include <moca_app.h>
#include <stdio.h>
#include <applib.h>
#include <dcsgendef.h>
#include <dcscolwid.h>
#include <dcserr.h>
#include <dcslib.h>
#include <varerr.h>
#include <common.h>
#include <stdlib.h>


LIBEXPORT
RETURN_STRUCT  *varMatchListValues( char *field_list_i, char *value_list_i,
                char *separator_string, char *inner_string_i, char *outer_string_i, int *quoteflg_i)
{
    RETURN_STRUCT  *returnData=NULL  ;
    RETURN_STRUCT  *CurPtr = NULL ;

    char out_string[5000];
    char field_list[200];
    char value_list[5000];
    int pos1, pos2;
    int quoteflg;
    char *StringPtr1, *StringPtr2;
    if (!field_list_i || strlen(field_list_i == 0))
         return (srvSetupReturn(eSRV_INSUFF_ARGUMENTS, ""));
     
    strcpy (field_list, field_list_i);

    if (!value_list_i || strlen(value_list_i == 0))
         return (srvSetupReturn(eSRV_INSUFF_ARGUMENTS, ""));

    if (!quoteflg_i)
       quoteflg = 0;
    else
       quoteflg = *quoteflg_i;
 
 
    strcpy (value_list, value_list_i);

    pos1 = -1; 
    pos2 = -1;

    memset(out_string, 0, sizeof(out_string));
    StringPtr1 = field_list;
    StringPtr2 = value_list;

    while (pos1 != 0)
      {
        if (strlen(out_string))
                strcat(out_string, outer_string_i);

	while (StringPtr1[0] == ' ' || StringPtr1[0] == ',')
	    StringPtr1 = StringPtr1+1;

	while (StringPtr2[0] == ' ' || StringPtr2[0] == ',')
	    StringPtr2 = StringPtr2+1;

        pos1 = strstr(StringPtr1, separator_string);
        if (pos1 == 0)           
           strcat (out_string, StringPtr1);
        else
           strncat (out_string, StringPtr1,  (int)(pos1 - (int) StringPtr1 ));

	strcat(out_string, inner_string_i);

        pos2 = strstr(StringPtr2, separator_string);

	if (quoteflg == 1)
           strcat (out_string, "\'");

        if (pos2 == 0)           
           strcat (out_string, StringPtr2);
        else
           strncat (out_string, StringPtr2,  (int)(pos2 - (int) StringPtr2));

	if (quoteflg == 1)
           strcat (out_string, "\'");


      StringPtr2 = value_list + (int)(pos2 - (int)StringPtr2 + 1);
      StringPtr1 = field_list + (int)(pos1 - (int)StringPtr1 + 1);
      }

 return(srvResults(eOK, "retstring", COMTYP_CHAR, strlen(out_string)+1, out_string, NULL)); 

}
