static const char *rcsid = "$Id: varGetLabelFile.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varint/varGetLabelFile.c,v $
 *  $Revision: 1.1.1.1 $
 *
 *  Application: intGet.c
 *  Created: 03-MAY-1994
 *  $Author: lh51sh $
 *
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

LIBEXPORT 
RETURN_STRUCT *varGetLabelFile(char *filename_i, char *action_i, long *section_size_i)
{
    char path[1000];
    char cmd[1000];
    char varbuf[1024];
    RETURN_STRUCT *result = NULL;
    char *dir;

    /*
     * Don't allow slashes in the incoming filename.
     */
    if (strchr(filename_i, PATH_SEPARATOR))
    {
	return srvSetupReturn(eINVALID_ARGS, "");
    }

    if (!(dir=misExpandVars(varbuf, LES_LABELS, sizeof(varbuf), NULL)))
    {
	return srvSetupReturn(eFILE_OPENING_ERROR, "");
    }

    sprintf(path, "%s%c%s", dir, PATH_SEPARATOR, filename_i);

    sprintf(cmd, "get file content"
	         " where filnam = \"%s\""
		 "   and fulres = \"N\""
		 "   and secsiz = %d and action=\"%s\"",
		 path, section_size_i?*section_size_i:1, action_i);
    srvInitiateCommand(cmd, &result);
    return result;
}
