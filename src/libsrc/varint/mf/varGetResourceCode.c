static const char *rcsid = "$Id: intGetResourceCode.c,v 1.14 2003/01/10 19:56:05 verdeyen Exp $";
/*#START**********************************************************************
 *  Copyright (c) 2002 RedPrairie Corporation. All rights reserved.
 *#END***********************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <applib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "intlib.h"


LIBEXPORT
RETURN_STRUCT *varGetResourceCode(char *arecod_i,
                                  char *restyp_i,
                                  char *resval_i,
                                  char *ship_id_i,
				  char *wkonum_i,
				  char *wkorev_i,
				  char *client_id_i,
                                  char *wrkref_i)
{

    RETURN_STRUCT *CurPtr;
    mocaDataRes *res;
    mocaDataRow *row;
    char arecod[ARECOD_LEN + 1];
    char ship_id[SHIP_ID_LEN + 1];
    char wkonum[WKONUM_LEN+1];
    char wkorev[WKOREV_LEN+1];
    char client_id[CLIENT_ID_LEN+1];
    char restyp[RTSTR2_LEN + 1];
    char resval[POLVAL_LEN + 1];
    char stoloc[STOLOC_LEN + 1];
    char wrkref[WRKREF_LEN + 1];

    char rescod[RESCOD_LEN + 1];
    char sqlBuffer[2000];
    long ret_status;
    char *colnam;
    char Datatype;
    char *TmpPtr;

    CurPtr = NULL;

    memset(ship_id, 0, sizeof(ship_id));
    memset(arecod, 0, sizeof(arecod));
    memset(wkonum, 0, sizeof(wkonum));
    memset(wkorev, 0, sizeof(wkorev));
    memset(resval, 0, sizeof(resval));
    memset(restyp, 0, sizeof(restyp));
    memset(stoloc, 0, sizeof(stoloc));
    memset(wrkref, 0, sizeof(wrkref));
    memset(rescod, 0, sizeof(rescod));

    /*
     * We need to get either an area code, or a ressource type
     * and resource value.  If we get the area code, than we can
     * use that to figure out the resource type and resource value.
     */

    
    if (arecod_i && misTrimLen(arecod_i, ARECOD_LEN))
        misTrimcpy(arecod, arecod_i, ARECOD_LEN);

    if (restyp_i && misTrimLen(restyp_i, POLVAL_LEN))
        misTrimcpy(restyp, restyp_i, POLVAL_LEN);

    if (resval_i && misTrimLen(resval_i, RTSTR2_LEN))
        misTrimcpy(resval, resval_i, RTSTR2_LEN);

    if ((strlen(arecod) == 0) && (strlen(restyp) == 0 || strlen(resval) == 0))
    {
        misTrc(T_FLOW, "Need either an area code or a resource type");
        return APPMissingArg("arecod");
    }

    if (ship_id_i && misTrimLen(ship_id_i, SHIP_ID_LEN))
	misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);

    if (wkonum_i && misTrimLen(wkonum_i, WKONUM_LEN))
	misTrimcpy(wkonum, wkonum_i, WKONUM_LEN);

    if (wkorev_i && misTrimLen(wkorev_i, WKOREV_LEN))
	misTrimcpy(wkorev, wkorev_i, WKOREV_LEN);

    if (client_id_i && misTrimLen(client_id_i, CLIENT_ID_LEN))
	misTrimcpy(client_id, client_id_i, CLIENT_ID_LEN);

    if (wrkref_i && misTrimLen(wrkref_i, WRKREF_LEN))
        misTrimcpy(wrkref, wrkref_i, WRKREF_LEN);

    /*
     * We need to find out what sort of resource code the
     * area passed in requires.
     */

    if ((strlen(restyp) == 0) || (strlen(resval) == 0))
    {
        sprintf(sqlBuffer,
	        "list policies "
	        " where polcod = '%s' "
	        "   and polvar = '%s' "
	        "   and rtstr1 = '%s' ",
	        POLCOD_ALLOCATE_INV,
                POLVAR_ALLOCINV_RESCOD_DEF,
	        arecod);

        CurPtr = NULL;
        ret_status = srvInitiateCommand(sqlBuffer, &CurPtr);
        if (ret_status != eOK)
        {
            if (CurPtr)
  	        srvFreeMemory(SRVRET_STRUCT, CurPtr);

            if (ret_status == eDB_NO_ROWS_AFFECTED || 
                ret_status == eSRV_NO_ROWS_AFFECTED)
            {
	        return (srvResults(eINT_NO_RESCOD_POLICY_EXISTS, NULL));
            }
            return (srvResults(ret_status, NULL));
        }
        res = srvGetResults(CurPtr);
        row = sqlGetRow(res);
        misTrc(T_FLOW, "Resource type from polval is %s", 
               sqlGetString(res, row, "polval"));
        strcpy(restyp, sqlGetString(res, row, "polval"));
        if (row && !sqlIsNull(res, row, "rtstr2"))
        {
	    strcpy(resval, sqlGetString(res, row, "rtstr2"));
        }
        else
        {
	    srvFreeMemory(SRVRET_STRUCT, CurPtr);
	    misTrc(T_FLOW,"Error getting rtstr2 value of %s", restyp);
	    return (srvResults(eINT_NO_RESCOD_POLICY_EXISTS, NULL));
        }
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
    }

    misTrc(T_FLOW, "Resource type is %s", restyp);
    misTrc(T_FLOW, "Resource value is %s", resval);
    if (strcmp(restyp, POLVAL_ALLOCINV_RESCOD_DERIVE) == 0)
    {
        /*
         * Field each field defined in the rtstr2 value of the policy.
         * (Note, may be more than 1 field, delimited by hyphens.
         */

        for (colnam = strtok(resval, "-"); colnam; colnam = strtok(NULL, "-"))
        {
            misTrc(T_FLOW, "Getting value for the field %s:", colnam);

            /*
             * First, let's see if the value exists on the stack.
             * (this will save a select out of the database)
             * If not, we need to read it from the database.
             */

            if ((srvGetNeededElement(colnam, "",
				     &Datatype, (void **) &TmpPtr) == eOK) &&
                (strlen(misTrim(TmpPtr))))                         
            {
                misTrc(T_FLOW, "Using value from the stack ");

                /* Value was on the stack */
                strcat(rescod, TmpPtr); 
                misTrc(T_FLOW, "Value on stack was %s", rescod);
            }
            else
            {
                misTrc(T_FLOW, "Value was not on stack, get from database");
                /*
                 * We couldn't get the value of the column name off
                 * the stack, therefore, we'll read the database
                 * tables to get the value for the column name.
                 * Note that in this case, we assume something was sent
                 * in that tells us what table we ought to be reading
                 * from. (like ship_id, wkonum, or wrkref)
                 */

                if (strlen(ship_id) != 0)
                {
  	            sprintf(sqlBuffer,
	     	            "select shipment.*, shipment_line.* "
		            "  from shipment_line, "
                            "        shipment "
		            " where shipment.ship_id = '%s' "
                            "   and shipment_line.ship_id = shipment.ship_id ",
		            ship_id);
                }
                else if (strlen(wrkref) != 0)
                {
                    sprintf(sqlBuffer,
                            "select pckwrk.*, prtmst.lodlvl prt_lodlvl "
                            "  from prtmst, pckwrk "
                            " where pckwrk.wrkref = '%s' "
                            "   and pckwrk.prtnum = prtmst.prtnum"
                            "   and pckwrk.prt_client_id = prtmst.prt_client_id", 
                            wrkref);
                }
                else if (strlen(wkonum) != 0)
                {
	            sprintf(sqlBuffer,
		            "select wkohdr.*, wkodtl.* "
		            "  from wkodtl, wkohdr "
		            " where wkohdr.wkonum = '%s' "
		            "   and wkohdr.wkorev = '%s' "
		            "   and wkohdr.client_id = '%s' "
                            "   and wkodtl.wkonum = wkohdr.wkonum "
                            "   and wkodtl.wkorev = wkohdr.wkorev "
                            "   and wkodtl.client_id = wkohdr.client_id "
                            "   and wkodtl.seqnum    = 0 ",
		            wkonum, wkorev, client_id);
		    
                }
                else
                {
                    misTrc(T_FLOW, "Not enough values to determine rescod");
                    return (srvResults(eINT_BAD_RESOURCE_CODE, NULL));
                }
	        ret_status = sqlExecStr(sqlBuffer, &res);
                if (ret_status != eOK)
                {
                    if ((ret_status == eDB_NO_ROWS_AFFECTED) ||
                        (ret_status == eSRV_NO_ROWS_AFFECTED))
                    {
                        if (strlen(ship_id))
                            return (APPInvalidArg("ship_id", ship_id));
                        else if (strlen(wrkref))
                            return (APPInvalidArg("wrkref", wrkref));
                        else 
                            return (APPInvalidArg("wkonum", wkonum)); 
                    }
                    else
                        return (srvResults(ret_status, NULL));
                }

                row = sqlGetRow(res);
  	        if (!sqlIsNull(res, row, colnam))
	        {
	            char tmpstr[1000];

	            switch(sqlGetDataType(res, colnam))
	            {
	            case COMTYP_INT:
		        sprintf(tmpstr, "%d", sqlGetLong(res, row, colnam));
		        strcat(rescod, tmpstr);
		        break;
	            case COMTYP_FLOAT:
		        sprintf(tmpstr, "%f", sqlGetFloat(res, row, colnam));
		        strcat(rescod, tmpstr);
		        break;
	            default:
		        strcat(rescod, sqlGetString(res, row, colnam));
		        break;
	            }
	        }
                sqlFreeResults(res);
            }
        }
    }
    else if (strcmp(restyp, POLVAL_ALLOCINV_RESCOD_FIXED) == 0)
    {
        /*
         * Resource code is a fixed value, we use whatever was defined
         * in rtstr2 of the policy.
         */

        strcpy(rescod, resval);
    }
    else if (strcmp(restyp, POLVAL_ALLOCINV_RESCOD_GENERATE) == 0)
    {
        /*
         * Resource code gets generated from a sequence.
         */

        ret_status = appNextNum(resval, rescod);
        if (ret_status != eOK)
        {
            return (srvResults(ret_status, NULL));
        }

    }
    else if (strcmp(restyp, POLVAL_ALLOCINV_RESCOD_COMMAND) == 0)
    {
        misTrc(T_FLOW, "Command to execute is: %s", resval);
        ret_status = srvInitiateInline(resval, &CurPtr);
        if (ret_status != eOK)
        {
            if (CurPtr) 
                srvFreeMemory(SRVRET_STRUCT, CurPtr);

            /*
             * If the command that we executed returns no rows affected,
             * then let's return a more specific error.  This is important,
             * because things like trnAllocInv make certain assumptions
             * when it runs into NO_ROWS_AFFECTED (not that this is a 
             * recommended coding practice, but it's what 5.0 inherited
             * from the old days...).
             */

            if (ret_status == eDB_NO_ROWS_AFFECTED ||
                ret_status == eSRV_NO_ROWS_AFFECTED)
            {
                return (srvResults(eINT_BAD_RESOURCE_CODE, NULL));
            }
            else
                return (srvResults(ret_status, NULL));
        }
        else
        {
            res = srvGetResults(CurPtr);
            row = sqlGetRow(res);
            misTrimcpy(rescod, sqlGetString(res, row, "rescod"), RESCOD_LEN);
            misTrimcpy(stoloc, sqlGetString(res, row, "stoloc"), STOLOC_LEN);
        }
        srvFreeMemory(SRVRET_STRUCT, CurPtr);
    }
    else if (strcmp(restyp, POLVAL_ALLOCINV_RESCOD_LOC_ASG) == 0)
    {
       strncpy(rescod, "XXXX", strlen("XXXX")); 
       strncpy(stoloc, resval, STOLOC_LEN);
    }

    if (strlen(rescod) == 0)
    {
        misTrc(T_FLOW, "No resource code was generated");
	return(APPError(eINT_BAD_RESOURCE_CODE));
    }
    misTrc(T_FLOW, "The resource code is %s", rescod);

    CurPtr = srvResultsInit(eOK,
                           "rescod", COMTYP_CHAR, RESCOD_LEN,
                           "restyp", COMTYP_CHAR, POLVAL_LEN,
                           "resval", COMTYP_CHAR, RTSTR2_LEN,
                           "stoloc", COMTYP_CHAR, STOLOC_LEN,
                           NULL);
 

    srvResultsAdd(CurPtr,
                  rescod,
                  restyp,
                  resval,
                  stoloc);

    return (CurPtr);
}
