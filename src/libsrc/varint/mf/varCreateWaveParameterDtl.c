/*#START************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 * DESCRIPTION: This function generates the syntax to insert 
 *              a record in the var_wavpar_dtl table using the MCS
 *		base component - Process Table Action.
 *#END**************************************************************/
#include <moca_app.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <applib.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include <varerr.h>
#include <vargendef.h>

LIBEXPORT 
RETURN_STRUCT *varCreateWaveParameterDtl(long *vc_wave_lane_i,
					 char *vc_carcod_i,
					 char *vc_cstnum_i)
{
    RETURN_STRUCT *CurPtr;
    char  tablename[100];
    char  tmpvar[1000];
    char  *columnlist;
    char  *valuelist;
    long  vc_wave_lane;
    long  ret_status;
    char  vc_carcod[CARCOD_LEN] = DEFAULT_NOT_NULL_STRING;
    char  vc_cstnum[CSTNUM_LEN] = DEFAULT_NOT_NULL_STRING;
 
    CurPtr = NULL;
    if (!vc_wave_lane_i || *vc_wave_lane_i == 0) {
	return (APPMissingArg("vc_wave_lane"));
    }
    vc_wave_lane = *vc_wave_lane_i;
    
    if ((!vc_carcod_i || *vc_carcod_i == 0)
	&& (!vc_cstnum_i || *vc_cstnum_i == 0)) {
	return (srvSetupReturn (eVAR_NO_CARCOD_OR_CSTNUM, ""));
    }
    
    if (vc_carcod_i && (misTrimLen(vc_carcod_i, CARCOD_LEN) != 0)) {
	strncpy (vc_carcod, vc_carcod_i, CARCOD_LEN);
    }
    
    if (vc_cstnum_i && (misTrimLen(vc_cstnum_i, CSTNUM_LEN) != 0)) {
	strncpy (vc_cstnum, vc_cstnum_i, CSTNUM_LEN);
    }

    /* Build the insert list */
    strcpy(tablename, "var_wavpar_dtl");
    columnlist = valuelist = NULL;
    sprintf(tmpvar, "%ld",vc_wave_lane);
    if(appBuildInsertList("vc_wave_lane", tmpvar, 20, 
			&columnlist, &valuelist) != eOK) {
       if (columnlist) free(columnlist);
       if (valuelist) free(valuelist);
       return (srvSetupReturn(eNO_MEMORY, ""));
    }
 
    if(appBuildInsertList("vc_carcod", vc_carcod, CARCOD_LEN,
			&columnlist, &valuelist) != eOK) { 
        if (columnlist) free(columnlist);
        if (valuelist) free(valuelist);
        return (srvSetupReturn(eNO_MEMORY, ""));
    }
    
    if(appBuildInsertList("vc_cstnum", vc_cstnum, CSTNUM_LEN,
			&columnlist, &valuelist) != eOK) {
        if (columnlist) free(columnlist);
        if (valuelist) free(valuelist);
        return (srvSetupReturn(eNO_MEMORY, ""));
    }
    
    if(appBuildInsertList("mod_usr_id", 
		osGetVar(LESENV_USR_ID) ? osGetVar(LESENV_USR_ID):"",
		0, &columnlist, &valuelist) != eOK) {
        if (columnlist) free(columnlist);
        if (valuelist) free(valuelist);
        return (srvSetupReturn(eNO_MEMORY, ""));
    }

    if(appBuildInsertListDBKW("moddte", "sysdate", 8, 
			&columnlist, &valuelist) != eOK) {
        if (columnlist) free(columnlist);
        if (valuelist) free(valuelist);
        return (srvSetupReturn(eNO_MEMORY, ""));
    }
    
    /* Publish out the table, columns, and values for insert by */
    /* intProcessTableInsert (PROCESS TABLE INSERT) */

    CurPtr = srvResultsInit(eOK, "acttyp", COMTYP_CHAR, 1,
		"tblnam", COMTYP_CHAR, strlen(tablename),
		"collst", COMTYP_CHAR, strlen(columnlist),
		"vallst", COMTYP_CHAR, strlen(valuelist), NULL);
    srvResultsAdd(CurPtr, ACTTYP_INSERT,
		tablename,
		columnlist,
		valuelist);
    free(columnlist);
    free(valuelist);
    return (CurPtr);
}
