static const char *rcsid = "$Id: intProcessCartonization.c,v 1.33 2004/08/24 15:33:21 lnecci Exp $";
/*#START***********************************************************************
 *  
 *  Copyright (c) 2003 RedPrairie Corporation. All rights reserved.
 *  
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>

#include <applib.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <math.h>
#include <dcserr.h>

typedef struct _nestingStructure
{
    double length;
    double width;
    double height;
    double nstlen;
    double nsthgt;
    double nstwid;
    long untqty;
    char nstcls[NSTCLS_LEN+1];
    long nstseq;
    /*This is a pointer to the pick that represents the nest structure*/
    struct _piecepickstructure * piece_structure;
    struct _nestingStructure *next;
}
NESTING_DATA;

typedef struct _cartonstructure
{
    char ctncod[CTNCOD_LEN+1];
    char ctnnum[CTNNUM_LEN+1];
    char wrkref[WRKREF_LEN+1];
    char brkval[OBJECT_ID_LEN+RTSTR1_LEN+1];
    long curunt;
    double ctnvol;
    double ctnwlm;
    double curvol;
    double curwgt;
    double ctnhgt;
    double ctnlen;
    double ctnwid;
    struct _nestingStructure *nestingList;
    
}
CARTON_DATA;

typedef struct _piecepickstructure
{
    char ctnnum[CTNNUM_LEN+1];
    char wrkref[WRKREF_LEN+1];
    char cmbcod[CMBCOD_LEN+1];
    char arecod[ARECOD_LEN+1];
    char stoloc[STOLOC_LEN+1];
    char velzon[VELZON_LEN+1];
    char wrkzon[WRKZON_LEN+1];
    char trvseq[TRVSEQ_LEN+1];
    char prtnum[PRTNUM_LEN+1];
    char prt_client_id[CLIENT_ID_LEN+1];
    char ftpcod[FTPCOD_LEN+1];
    char rpkcls[RPKCLS_LEN+1];
    char ltlcls[LTLCLS_LEN+1];
    char stccod[STCCOD_LEN+1];
    long parcel_flg;
    long hazmat_flg;
    long ltlrnk;
    long stcrnk;
    double dtlvol;
    double dtllen;
    double dtlwid;
    double dtlhgt;
    double dtlwgt;
    double untlen;
    double paklen;
    long untpak;
    long pckqty;
    moca_bool_t isFirst;
    char brkval[OBJECT_ID_LEN+RTSTR1_LEN+1];
    /*used to complex cartonization to denote virtual*/
    /*In a stack of cups.  We have one non-virtual piece that is a nesting piece and many virtual pieces that hold the pick info*/
    moca_bool_t isVirtual;
    moca_bool_t isNesting;
    double nstlen;
    double nsthgt;
    double nstwid;
    char nstcls[NSTCLS_LEN+1];
    struct _cartonstructure *carton;
    struct _piecepickstructure *next;
    struct _piecepickstructure *prev;
}
PICKS_DATA;

typedef struct _cartonsegmentstructure
{
    double ctnlen;
    double ctnwid;
    double ctnhgt;
    struct _cartonsegmentstructure *next;
    struct _cartonsegmentstructure *prev;
}
CTNSEGS_DATA;

static long sSplitCartonWorkReference(short whatIfMode, 
                               PICKS_DATA * CurPck, 
                               PICKS_DATA * TopPck,                               
                               PICKS_DATA * TmpPck, 
                               PICKS_DATA * LstPck, 
                               char * new_split_wrkref,
                               char * new_split_cmbcod, 
                               char * valueStr,
                               char * fieldptr, 
                               char * buffer,
                               char * new_pcksts);

static moca_bool_t sValidateRepackClassForEntireCarton(PICKS_DATA * top_pick, 
                                                       char * ctnnum, 
                                                       mocaDataRes *CtmMstRes, 
                                                       char * ctncod);
static CARTON_DATA * sFindCartonWithMostRemainingVolume (PICKS_DATA * match_pick,
                                                         PICKS_DATA ** TopPtr,
                                                         mocaDataRes * CtnMstRes,
                                                         double pckvol,
                                                          double pckwgt);

static moca_bool_t sValidateRepackClassForNewPick(char * ctncod, 
                                                  char * rpkcls, 
                                                   mocaDataRes *CtnMstRes);

static long sDownsizeCarton(short whatIfMode,
                            char *cartonize_method,
                            long cartonize_complex_iter_limit,
                            long cartonize_complex_popup_iter_limit,
                            double cartonize_complex_popup_levels_pct,
                            double product_volume,
                            double product_weight,
                            char *force_carton_code,
                            char *carton_number,
                            char *carton_wrkref,
                            mocaDataRes * ctnmstres,
                            PICKS_DATA ** TopPck,
                            CARTON_DATA * tempCarton);

static long sPerformComplexCartonization(
                        long iteration_level,
                        long *iteration_pop_to_level,
                        long cartonize_complex_iter_limit,
                        long cartonize_complex_popup_iter_limit,
                        double cartonize_complex_popup_levels_pct,
                        long *cartonize_iteration_counter,
                        long *cartonize_poup_iter_counter,
                        char *carton_number,
                        double smallest_piece_volume,
                        PICKS_DATA * TopThisCarton,
                        CTNSEGS_DATA * TopCtnSeg);

static void sFreeUpMemory(PICKS_DATA * TopPck,
                          char *prtnum_list,
                          char *ftpcod_list,
                          char *prt_client_id_list,
                          char *brkval_list,
                          char *untqty_list);

static RETURN_STRUCT * sBuildReturnList(RETURN_STRUCT * RetCurPtr, 
                                        PICKS_DATA * TopPck, 
                                        short whatIfMode);

static moca_bool_t sValidateNestingForEntireCarton(PICKS_DATA ** TopPckPtr,
                                                   CARTON_DATA * tempCarton,
                                                   double maxvol,
                                                   double ctnlen,
                                                   double ctnwid,
                                                   double ctnhgt,
                                                   moca_bool_t update_flg);

static moca_bool_t sValidatePieceFitsInCartonByDimension(double dtllen,
                                                         double dtlwid,
                                                         double dtlhgt,
                                                         double ctnlen,
                                                         double ctnwid,
                                                         double ctnhgt);

static long sGetNestingSequence();

static long sUpdateNestingList(NESTING_DATA * CurNst, 
                   double maximumLength,
                   double maximumWidth,
                   double maximumHeight,
                   double maximumNstLen,
                   double maximumNstWid,
                   double maximumNstHgt,
                   long untqty);

static NESTING_DATA * sCreateNewNestingList(PICKS_DATA ** PtrTopPck,
                           CARTON_DATA * tempCarton,
                           char * nstcls,
                           char * rpkcls,
                           double maximumLength,
                           double maximumWidth,
                           double maximumHeight,
                           double maximumNstLen,
                           double maximumNstWid,
                           double maximumNstHgt,
                           long untqty);

static long sValidateWeightAndVolume(PICKS_DATA ** PtrTopPck,
                                     PICKS_DATA * CurPck,
                                     CARTON_DATA * tempCarton,
                                     double pckvol,
                                     double pckwgt,
                                     moca_bool_t update_flg);

static moca_bool_t sSplitIntoMultiplePiles(PICKS_DATA ** PtrTopPck,
                                           NESTING_DATA * CurNst,
                                           CARTON_DATA * tempCarton,
                                           double * old_difference,
                                           double maxvol,
                                           double ctnlen,
                                           double ctnwid,
                                           double ctnhgt,
                                           moca_bool_t update_flg,
                                           double Length,
                                           double Width,
                                           double Height,
                                           double NstLen,
                                           double NstWid,
                                           double NstHgt,
                                           char * nstcls,
                                           char * rpkcls,
                                           long newQuantity,
                                           double volume_not_added);

PICKS_DATA * sGatherCartonInformationForComplex(PICKS_DATA ** TopPck, 
                                                long * pieces_in_this_carton,
                                                double * largest_piece_dimension,
                                                double * smallest_piece_volume,
                                                char * carton_number);
static double sMax(double a, 
                   double b);

LIBEXPORT 
RETURN_STRUCT *varProcessCartonization(char *wrkref_i,
                                       char *ship_id_i,
                                       moca_bool_t *cremov_i,
                                       char *prtnum_list_i,
                                       char *prt_client_id_list_i,
                                       char *ftpcod_list_i,
                                       char *brkval_list_i,
                                       char *untqty_list_i,
                                       char *force_ctncod_i,
                                       long *force_untper_i)
{
    long ret_status;
    long rpkcls_return_status;
    PICKS_DATA *TopPck = NULL, *CurPck = NULL, *LstPck = NULL, *TmpPck = NULL, *FstPck =NULL;
    RETURN_STRUCT *RetCurPtr;
    RETURN_STRUCT *resultData;
    CARTON_DATA *tempCarton = NULL, *oldCarton = NULL;
    long ii;
    mocaDataRow *row, *tmprow, *rpkcls_row;
    mocaDataRes *res, *rpkcls_res;
    static short loadedPolicies = FALSE;
    static char group_by[RTSTR1_LEN+1];
    static char order_by[RTSTR1_LEN+1];
    static char break_on[RTSTR1_LEN+1];
    static char include_areas[RTSTR1_LEN+1];
    static char include_buffer[500];
    static char include_buffer_moca[500];
    static moca_bool_t split_qty;
    static char cartonize_method[RTSTR1_LEN+1];
    static long cartonize_complex_iteration_limit;
    static long cartonize_complex_popup_iter_limit;
    static double cartonize_complex_popup_levels_pct;
    static mocaDataRes *CtnMstRes = NULL;
    char buffer[3500];
    char rpkcls_buffer[500];
    char *sqlbuffer;
    char wrkref[WRKREF_LEN+1];
    char ship_id[SHIP_ID_LEN+1];
    moca_bool_t cremov;
    moca_bool_t RpkclsFits;
    moca_bool_t RunSplit;
    char force_ctncod[CTNCOD_LEN+1];
    long force_untper;
    char *prtnum_list, *ftpcod_list, *brkval_list, *untqty_list;
    char *prtnumPtr, *ftpcodPtr, *brkvalPtr, *untqtyPtr;
    char *prtnumNxt, *ftpcodNxt, *brkvalNxt, *untqtyNxt;
    char *prt_client_id_list;
    char *prt_client_idPtr;
    char *prt_client_idNxt;
    char new_split_wrkref[WRKREF_LEN+1];
    char new_split_cmbcod[CMBCOD_LEN+1];
    char new_wrkref[WRKREF_LEN+1];
    char new_cmbcod[CMBCOD_LEN+1];
    char new_pcksts[PCKSTS_LEN+1];
    char new_ctnnum[CTNNUM_LEN+1];
    char rpkcls[RPKCLS_LEN+1];
    char ltlcls[LTLCLS_LEN+1];
    char stccod[STCCOD_LEN+1];
    char nstcls[NSTCLS_LEN+1];
    char identifier_buffer[SHIP_ID_LEN + WRKREF_LEN + 12];
    long parcel_flg = 0;
    long hazmat_flg = 0;
    long ltlrnk = 0;
    long stcrnk = 0;
    long new_ctnnum_cnt;
    char *fieldptr;
    char valueStr[2000];
    double cur_ctn_vol;
    double cur_ctn_wgt;
    long cur_ctn_unt;
    double pckvol;
    double pckwgt;
    long untqty;
    char max_ctncod[CTNCOD_LEN+1];
    double max_ctnvol;
    double max_ctnwlm;
    char working_max_ctncod[CTNCOD_LEN+1];
    double working_max_ctnvol;
    double working_max_ctnwlm;
    long pckwrk_pndcnt;
    long loop_inc;
    long prtnum_cnt, ftpcod_cnt, brkval_cnt, untqty_cnt;
    long prt_client_id_cnt;
    short whatIfMode;
    short odd_piece_carton_break;
    double working_max_ctnlen = 0;
    double working_max_ctnhgt = 0;
    double working_max_ctnwid = 0;
    long WeightAndVolumeFit = 0;

    static long isInstalled = -1;

    misTrc(T_FLOW, "Entering intProcessCartonization ...");

    if (isInstalled < 0)
    {
        isInstalled = appIsInstalled(POLCOD_CARTONIZATION);
    }

    if (!isInstalled)
    {
        misTrc(T_FLOW, 
               "Skipping cartonization as policies indicate that it is not"
               "installed");
        return(srvResults(eOK,NULL));
    }
    
    ret_status = eOK;
    whatIfMode = FALSE;
    RetCurPtr = NULL;
    TopPck = CurPck = LstPck = NULL;
    prtnum_list = ftpcod_list = brkval_list = untqty_list = NULL;
    prt_client_id_list = NULL;

    memset(wrkref, 0, sizeof(wrkref));
    memset(ship_id, 0, sizeof(ship_id));
    memset(force_ctncod, 0, sizeof(force_ctncod));
    memset(rpkcls_buffer, 0, sizeof(rpkcls_buffer));
    memset(stccod,0,sizeof(stccod));
    memset(rpkcls,0,sizeof(rpkcls));
    memset(ltlcls,0,sizeof(ltlcls));
        memset(nstcls,0,sizeof(nstcls));
    force_untper = 0;

    if (wrkref_i && misTrimLen(wrkref_i, WRKREF_LEN))
        misTrimcpy(wrkref, wrkref_i, WRKREF_LEN);

    if (ship_id_i && misTrimLen(ship_id_i, SHIP_ID_LEN))
        misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);

    /* default cremov to true (to always create pckmovs) */
    /* unless otherwise specified */
    if (cremov_i)
        cremov = *cremov_i;
    else
        cremov = BOOLEAN_TRUE;

    if (prtnum_list_i && misTrimLen(prtnum_list_i, PRTNUM_LEN))
    {
        prtnum_list = (char *) calloc(1, strlen(prtnum_list_i) + 1);
        memset(prtnum_list, 0, strlen(prtnum_list_i) + 1);
        strncpy(prtnum_list, prtnum_list_i, strlen(prtnum_list_i));
    }
    if (prt_client_id_list_i && misTrimLen(prt_client_id_list_i, CLIENT_ID_LEN))
    {
        prt_client_id_list = (char *)calloc(1,strlen(prt_client_id_list_i) + 1);
        memset(prt_client_id_list, 0, strlen(prt_client_id_list_i) + 1);
        strncpy(prt_client_id_list, prt_client_id_list_i,
                                                strlen(prt_client_id_list_i));
    }
    if (ftpcod_list_i && misTrimLen(ftpcod_list_i, FTPCOD_LEN))
    {
        ftpcod_list = (char *) calloc(1, strlen(ftpcod_list_i) + 1);
        memset(ftpcod_list, 0, strlen(ftpcod_list_i) + 1);
        strncpy(ftpcod_list, ftpcod_list_i, strlen(ftpcod_list_i));
    }
    if (brkval_list_i && misTrimLen(brkval_list_i, RTSTR1_LEN))
    {
        brkval_list = (char *) calloc(1, strlen(brkval_list_i) + 1);
        memset(brkval_list, 0, strlen(brkval_list_i) + 1);
        strncpy(brkval_list, brkval_list_i, strlen(brkval_list_i));
    }
    if (untqty_list_i && misTrimLen(untqty_list_i, RTSTR1_LEN))
    {
        untqty_list = (char *) calloc(1, strlen(untqty_list_i) + 1);
        memset(untqty_list, 0, strlen(untqty_list_i) + 1);
        strncpy(untqty_list, untqty_list_i, strlen(untqty_list_i));
    }

    /* Force carton code and force units per can be used to force cartonization
         to use the supplied carton code and build each carton containing the
       units per carton specified. None of the other sizing or volume or weight
       logic is used. Both of these parameters must be sent in, or they are ignored.
    */
    if (force_ctncod_i && misTrimLen(force_ctncod_i, CTNCOD_LEN))
        misTrimcpy(force_ctncod, force_ctncod_i, CTNCOD_LEN);

    if (force_untper_i && *force_untper_i > 0)
        force_untper = *force_untper_i;

    if (force_untper == 0)
        memset(force_ctncod,0,sizeof(force_ctncod));
    if (strlen(force_ctncod)==0)
        force_untper = 0;


    /*  TODO:  Validate that the prt_client_id needs to be here to continue.
    */
    if (!strlen(wrkref) && !strlen(ship_id) &&
        (!prtnum_list || !strlen(prtnum_list)) &&
        (!prt_client_id_list || !strlen(prt_client_id_list)) &&
        (!ftpcod_list || !strlen(ftpcod_list)))
    {
        sFreeUpMemory((PICKS_DATA *) TopPck,
                      prtnum_list, ftpcod_list,
                      prt_client_id_list,
                      brkval_list, untqty_list);
        return(srvResults(eINT_REQ_PARAM_MISSING,NULL));
    }

    if (!strlen(wrkref) && !strlen(ship_id) &&
        (!untqty_list || !strlen(untqty_list)))
    {
        sFreeUpMemory((PICKS_DATA *) TopPck,
                      prtnum_list, ftpcod_list,
                      prt_client_id_list,
                      brkval_list, untqty_list);
         return(srvResults(eINT_REQ_PARAM_MISSING,NULL));
        
    }


    /* If we have not read our policies, get all of them */
    if (loadedPolicies == FALSE)
    {
        /* Select the group by policy.  Do this one first because */
        /* any extra processing before we determine if we are going */
        /* to release the work will slow the release process down */
        sprintf(buffer,
                "select rtstr1 "
                " from poldat "
                " where polcod = '%s'"
                "   and polvar = '%s' "
                "   and polval = '%s' "
                "   and rtstr1 is not null ",
                POLCOD_CARTONIZATION,
                POLVAR_RELEASE_RULES,
                POLVAL_CTN_GROUP_BY);
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            misTrc(T_FLOW, "Error Reading Cartonization Group By policy");
            sqlFreeResults(res);
            return(srvResults(ret_status,NULL));
        }
        row = sqlGetRow(res);
        memset(group_by, 0, sizeof(group_by));
        if (!sqlIsNull(res, row, "rtstr1"))
            strncpy(group_by,
                     sqlGetString(res, row, "rtstr1"),
                    misTrimLen(sqlGetString(res, row, "rtstr1"),
                               RTSTR1_LEN));
        sqlFreeResults(res);

        /* Next, read the order by (sort) policies */
        sprintf(buffer,
                "select rtstr1 "
                " from poldat "
                " where polcod = '%s' "
                "   and polvar = '%s' "
                "   and polval = '%s' "
                "   and rtstr1 is not null "
                " order by srtseq ",
                POLCOD_CARTONIZATION,
                POLVAR_RELEASE_RULES,
                POLVAL_CTN_SORT_BY);
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            misLogError("Error(%ld) Reading Cartonization Order By policy",
                        ret_status);
            sqlFreeResults(res);
            return(srvResults(ret_status,NULL));
        }
        row = sqlGetRow(res);
        memset(order_by, 0, sizeof(order_by));
        if (!sqlIsNull(res, row, "rtstr1"))
            strncpy(order_by,
                    sqlGetString(res, row, "rtstr1"),
                    misTrimLen(sqlGetString(res, row, "rtstr1"),
                               RTSTR1_LEN));
        sqlFreeResults(res);

        /* And read the break on policies ... */
        sprintf(buffer,
                "select rtstr1 "
                " from poldat "
                " where polcod = '%s'  "
                "   and polvar = '%s'  "
                "   and polval = '%s'  "
                "   and rtstr1 is not null "
                " order by srtseq ",
                POLCOD_CARTONIZATION,
                POLVAR_RELEASE_RULES,
                POLVAL_CTN_BREAK_ON);
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            misLogError("Error(%ld) Reading Cartonization Break On policy",
                        ret_status);
            sqlFreeResults(res);
            return(srvResults(ret_status,NULL));
        }
        row = sqlGetRow(res);
        memset(break_on, 0, sizeof(break_on));
        if (!sqlIsNull(res, row, "rtstr1"))
            strncpy(break_on,
                    sqlGetString(res, row, "rtstr1"),
                    misTrimLen(sqlGetString(res, row, "rtstr1"),
                               RTSTR1_LEN));
        else
            strcpy(break_on, "RTRIM(shipment_line.ship_id)");

        sqlFreeResults(res);

        /* And read the include source areas ... */
        /*replace the single quotes with double quotes to handle running in moca command*/
        sprintf(buffer,
                "select replace(rtstr1,'''','''''') rtstr1_moca, "
                " rtstr1 "
                " from poldat "
                " where polcod = '%s'  "
                "   and polvar = '%s'  "
                "   and polval = '%s'  "
                "   and rtstr1 is not null "
                " order by srtseq ",
                POLCOD_CARTONIZATION,
                POLVAR_RELEASE_RULES,
                POLVAL_CTN_INCLUDE_SOURCE_AREAS_LIST);
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            misLogError("Error(%ld) Reading Cartonization Include-Srcare",
                        ret_status);
            sqlFreeResults(res);
            return (srvResults(ret_status, NULL));
        }
        memset(include_areas, 0, sizeof(include_areas));
        memset(include_buffer, 0, sizeof(include_buffer));
        memset(include_buffer_moca,0,sizeof(include_buffer_moca));
        if (ret_status == eOK)
        {
            row = sqlGetRow(res);
            memset(include_areas, 0, sizeof(include_areas));
            if (!sqlIsNull(res, row, "rtstr1"))
            {
                strncpy(include_areas,
                        sqlGetString(res, row, "rtstr1"),
                        misTrimLen(sqlGetString(res, row, "rtstr1"),
                                   RTSTR1_LEN));
                sprintf(include_buffer, "and pckwrk.srcare in (%s)",
                        include_areas);
                strncpy(include_areas,
                        sqlGetString(res, row, "rtstr1_moca"),
                        misTrimLen(sqlGetString(res, row, "rtstr1_moca"),
                                   RTSTR1_LEN));
                sprintf(include_buffer_moca, "and pckwrk.srcare in (%s)",include_areas);
            }
            else
            {
                sprintf(include_buffer_moca, "and 1 = 1");
                sprintf(include_buffer, "and 1 = 1");
            }
        }
        sqlFreeResults(res);

        /* And the split picks to quantity 1 policy ... */
        sprintf(buffer,
                "select rtnum1 "
                " from poldat "
                " where polcod = '%s'  "
                "   and polvar = '%s'  "
                "   and polval = '%s'  "
                " order by srtseq ",
                POLCOD_CARTONIZATION,
                POLVAR_RELEASE_RULES,
                POLVAL_CTN_SPLIT_QTYS_ACROSS_CTNS);
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            misLogError("Error(%ld) Reading Cartonization Split Qty policy",
                        ret_status);
            sqlFreeResults(res);
            return (srvResults(ret_status, NULL));
        }
        else if (ret_status != eOK)
        {
            split_qty = BOOLEAN_TRUE;
        }
        else
        {
            row = sqlGetRow(res);
            if (!sqlIsNull(res, row, "rtnum1"))
                split_qty = sqlGetBoolean(res, row, "rtnum1");
            else
                split_qty = BOOLEAN_TRUE;
        }
        sqlFreeResults(res);

        /* And the cartonize method ... */
        sprintf(buffer,
                "select rtstr1, rtnum1, rtnum2, rtflt1 "
                " from poldat "
                " where polcod = '%s'  "
                "   and polvar = '%s'  "
                "   and polval = '%s'  "
                "   and rtstr1 is not null "
                " order by srtseq ",
                POLCOD_CARTONIZATION,
                POLVAR_RELEASE_RULES,
                POLVAL_CTN_CARTONIZE_METHOD);
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            misLogError("Error(%ld) Reading Cartonize Method policy",
                        ret_status);
            sqlFreeResults(res);
            return (srvResults(ret_status, NULL));
        }
        else if (ret_status != eOK)
        {
            memset(cartonize_method, 0, sizeof(cartonize_method));
            sprintf(cartonize_method, CTN_METHOD_VOLUME);
            cartonize_complex_iteration_limit = 0;
            cartonize_complex_popup_iter_limit = 0;
            cartonize_complex_popup_levels_pct = 0.0;
        }
        else
        {
            row = sqlGetRow(res);
            memset(cartonize_method, 0, sizeof(cartonize_method));
            cartonize_complex_iteration_limit = 0;
            if (!sqlIsNull(res, row, "rtstr1"))
                strncpy(cartonize_method,
                        sqlGetString(res, row, "rtstr1"),
                        misTrimLen(sqlGetString(res, row, "rtstr1"),
                                   RTSTR1_LEN));
            else
                sprintf(cartonize_method, CTN_METHOD_VOLUME);

            if (!sqlIsNull(res, row, "rtnum1"))
                cartonize_complex_iteration_limit = sqlGetLong(res,
                                                               row,
                                                               "rtnum1");
            if (cartonize_complex_iteration_limit < 1)
                cartonize_complex_iteration_limit = 750000;

            if (!sqlIsNull(res, row, "rtnum2"))
                cartonize_complex_popup_iter_limit = sqlGetLong(res,
                                                                row,
                                                                "rtnum2");
            if (cartonize_complex_popup_iter_limit < 1)
                cartonize_complex_popup_iter_limit = 2000;

            if (!sqlIsNull(res, row, "rtflt1"))
                cartonize_complex_popup_levels_pct = sqlGetFloat(res,
                                                                 row, 
                                                                 "rtflt1");
            if (cartonize_complex_popup_levels_pct < 1)
                cartonize_complex_popup_levels_pct = 50.0;
        }
        sqlFreeResults(res);
        if ((strcmp(cartonize_method, CTN_METHOD_COMPLEX)==0) &&
            split_qty != BOOLEAN_TRUE)
        {
            split_qty = BOOLEAN_TRUE;
            misLogWarning("Warning, cartonize method policy specifies COMPLEX"
                          ", however, the split");
            misLogWarning("qty policy is not set to 1, overriding settings "
                          "of split quantity to 1!");
        }

        /* And set our static so we know we have done it already */
        loadedPolicies = TRUE;
    }

    /* If we are processing by work reference then see if */
    /* this is the last unreleased detail pick, then we proceed. */
    /* If not last unreleased then free it and return eOK */
    if (strlen(wrkref))
    {
        misTrc(T_FLOW, "intProcessCartonization, processing for "
               "wrkref <%s> ...", wrkref);

        sprintf(buffer,
                "select count(*) pndcnt "
                " from pckwrk, locmst, shipment_line "
                " where pckwrk.pcksts    = '%s'  "
                "   and pckwrk.lodlvl    = '%s'  "
                "   and pckwrk.srcloc    = locmst.stoloc "
                "   and pckwrk.ship_line_id    = shipment_line.ship_line_id "
                "   and (%s) = "
                "   (select %s from pckwrk, locmst, shipment_line "
                "     where pckwrk.wrkref    = '%s'  "
                "       and pckwrk.ship_line_id    = shipment_line.ship_line_id  "
                "       and pckwrk.srcloc    = locmst.stoloc)",
                PCKSTS_PENDING,
                LODLVL_DETAIL,
                group_by,
                group_by,
                wrkref );
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            sqlFreeResults(res);
            sprintf(buffer,
                    "select 1 from pckwrk "
                    " where wrkref = '%s' and wkonum is not null",
                    wrkref);
            if (sqlExecStr(buffer, NULL) == eOK)
            {
                misTrc(T_FLOW, "Skipping cartonization of work order pick");
                return(srvResults(eOK, NULL));
            }

            misTrc(T_FLOW,
                   "Error counting outstanding picks - %ld!", ret_status);
            return (srvResults(ret_status, NULL));
        }

        row = sqlGetRow(res);

        pckwrk_pndcnt = (sqlGetLong(res, row, "pndcnt"));

        if (pckwrk_pndcnt > 1)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            misTrc(T_FLOW, "intProcessCartonization, this not the last "
                   "pending pick work, exiting ...");
            sqlFreeResults(res);
            return (srvResults(eOK, NULL));
        }

        sqlFreeResults(res);

        memset(new_pcksts, 0, sizeof(new_pcksts));
        strcpy(new_pcksts, PCKSTS_PENDING);
    }
    /* Else if we are processing by the four sisters */
    /* and we want to cartonize anything and put it into the */
    /* pcksts of the picks we cartonized */
    else if (strlen(ship_id))
    {
        misTrc(T_FLOW, "intProcessCartonization, processing for "
               "ship_id <%s> ... ",
               ship_id);
   
        /*It will cartonize released picks only*/
        sprintf(buffer,
                "select distinct pcksts "
                " from pckwrk, shipment_line "
                " where pckwrk.appqty < pckwrk.pckqty "
                "   and pckwrk.lodlvl = '%s' "
                "   and pckwrk.ctnnum is null "
                " %s "
                "   and pckwrk.ship_line_id = shipment_line.ship_line_id "
                "   and shipment_line.ship_id = '%s' ",
                LODLVL_DETAIL, include_buffer,
                ship_id);
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status == eDB_NO_ROWS_AFFECTED)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            sqlFreeResults(res);
            misTrc(T_FLOW, "intProcessCartonization, there is nothing "
                   "to cartonize, exiting ...");
            return (srvResults(eOK, NULL));
        }
        else if (ret_status != eOK)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            misLogError("Error selecting picks - %ld!", ret_status);
            sqlFreeResults(res);
            return (srvResults(ret_status, NULL));
        }

        row = sqlGetRow(res);

        if (res->NumOfRows > 1)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            sqlFreeResults(res);
            misTrc(T_FLOW, "intProcessCartonization, there are multiple "
                   "pcksts values to choose from, ERROR!");
            return (srvResults(eINT_PARM_MISMATCH, NULL));
        }

        memset(new_pcksts, 0, sizeof(new_pcksts));
        strncpy(new_pcksts,
                sqlGetString(res, row, "pcksts"),
                misTrimLen(sqlGetString(res, row, "pcksts"),
                           PCKSTS_LEN));

        sqlFreeResults(res);
    }
    /* Else we are playing a what if game with a list of parts or */
    /* footprints and quantities ... */
    else
    {
        whatIfMode = TRUE;

        prtnum_cnt = ftpcod_cnt = brkval_cnt = untqty_cnt = 0;
        prt_client_id_cnt = 0;
        prtnumPtr = prtnum_list;
        ftpcodPtr = ftpcod_list;
        brkvalPtr = brkval_list;
        untqtyPtr = untqty_list;
        prt_client_idPtr = prt_client_id_list;
        prtnumNxt = ftpcodNxt = brkvalNxt = untqtyNxt = NULL;
        prt_client_idNxt = NULL;
        do
        {
            if (prtnumPtr)
                prtnumNxt = strchr(prtnumPtr, ',');
            if (prtnumNxt)
                *prtnumNxt++ = '\0';

            if (ftpcodPtr)
                ftpcodNxt = strchr(ftpcodPtr, ',');
            if (ftpcodNxt)
                *ftpcodNxt++ = '\0';

            if (prt_client_idPtr)
                prt_client_idNxt = strchr(prt_client_idPtr, ',');
            if (prt_client_idNxt)
                *prt_client_idNxt++ = '\0';

            if (brkvalPtr)
                brkvalNxt = strchr(brkvalPtr, ',');
            if (brkvalNxt)
                *brkvalNxt++ = '\0';

            if (untqtyPtr)
                untqtyNxt = strchr(untqtyPtr, ',');
            if (untqtyNxt)
                *untqtyNxt++ = '\0';

            if (prtnumPtr)
                prtnum_cnt++;
            if (ftpcodPtr)
                ftpcod_cnt++;
            if (prt_client_idPtr)
                prt_client_id_cnt++;
            if (brkvalPtr)
                brkval_cnt++;
            if ((untqtyPtr) && atoi(untqtyPtr) > 0)
                untqty_cnt++;

            prtnumPtr = prtnumNxt;
            ftpcodPtr = ftpcodNxt;
            prt_client_idPtr = prt_client_idNxt;
            brkvalPtr = brkvalNxt;
            untqtyPtr = untqtyNxt;
        }
        while (untqtyPtr);

        /* If we have both parts and footprints, then they better match */
        /* ... or whichever we have better match the unit quantities */
        if ((untqty_cnt < 1) ||
            (prtnum_cnt > 0 && prtnum_cnt != untqty_cnt) ||
            (ftpcod_cnt > 0 && ftpcod_cnt != untqty_cnt) ||
            (prt_client_id_cnt > 0 && prt_client_id_cnt != untqty_cnt) ||
            (brkval_cnt > 0 && brkval_cnt != untqty_cnt))
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            return (srvResults(eINT_PARM_MISMATCH, NULL));
        }

        /* Create the linked list with quantities */
        if (prtnum_list_i && misTrimLen(prtnum_list_i, PRTNUM_LEN))
        {
            memset(prtnum_list, 0, strlen(prtnum_list_i) + 1);
            strncpy(prtnum_list, prtnum_list_i, strlen(prtnum_list_i));
        }
        if (ftpcod_list_i && misTrimLen(ftpcod_list_i, FTPCOD_LEN))
        {
            memset(ftpcod_list, 0, strlen(ftpcod_list_i) + 1);
            strncpy(ftpcod_list, ftpcod_list_i, strlen(ftpcod_list_i));
        }
        if (prt_client_id_list_i && misTrimLen(prt_client_id_list_i,CLIENT_ID_LEN))
        {
            memset(prt_client_id_list, 0, strlen(prt_client_id_list_i) + 1);
            strncpy(prt_client_id_list, prt_client_id_list_i,
                                                    strlen(prt_client_id_list_i));
        }
        if (brkval_list_i && misTrimLen(brkval_list_i, RTSTR1_LEN))
        {
            memset(brkval_list, 0, strlen(brkval_list_i) + 1);
            strncpy(brkval_list, brkval_list_i, strlen(brkval_list_i));
        }
        if (untqty_list_i && misTrimLen(untqty_list_i, RTSTR1_LEN))
        {
            memset(untqty_list, 0, strlen(untqty_list_i) + 1);
            strncpy(untqty_list, untqty_list_i, strlen(untqty_list_i));
        }
        prtnumPtr = prtnum_list;
        ftpcodPtr = ftpcod_list;
        prt_client_idPtr = prt_client_id_list;
        brkvalPtr = brkval_list;
        untqtyPtr = untqty_list;
        prtnumNxt = ftpcodNxt = brkvalNxt = untqtyNxt = NULL;
        prt_client_idNxt = NULL;
        new_ctnnum_cnt = 0;
        do
        {
            if (prtnumPtr)
                prtnumNxt = strchr(prtnumPtr, ',');
            if (prtnumNxt)
                *prtnumNxt++ = '\0';

            if (ftpcodPtr)
                ftpcodNxt = strchr(ftpcodPtr, ',');
            if (ftpcodNxt)
                *ftpcodNxt++ = '\0';

            if (prt_client_idPtr)
                prt_client_idNxt = strchr(prt_client_idPtr, ',');
            if (prt_client_idNxt)
                *prt_client_idNxt++ = '\0';

            if (brkvalPtr)
                brkvalNxt = strchr(brkvalPtr, ',');
            if (brkvalNxt)
                *brkvalNxt++ = '\0';

            if (untqtyPtr)
                untqtyNxt = strchr(untqtyPtr, ',');
            if (untqtyNxt)
                *untqtyNxt++ = '\0';

            if (atoi(untqtyPtr) < 1)
                continue;

            new_ctnnum_cnt++;

            if (split_qty == BOOLEAN_TRUE)
                loop_inc = 1;
            else
                loop_inc = atoi(untqtyPtr);

            /* Now, we are going to loop through and either put out one */
            /* structure entry for the entire quantity of the pick or */
            /* entries of quantity = 1 if it is OK to split them up */
            for (ii = 0; ii < atoi(untqtyPtr); ii += loop_inc)
            {
                if (TopPck == NULL)
                {
                    TopPck = (PICKS_DATA *) calloc(1, sizeof(PICKS_DATA));
                    if (TopPck == NULL)
                    {
                        sFreeUpMemory((PICKS_DATA *) TopPck,
                                      prtnum_list, ftpcod_list,
                                      prt_client_id_list,
                                      brkval_list, untqty_list);
                        return (srvResults(eNO_MEMORY, NULL));
                    }
                    CurPck = TopPck;
                    /*First pick will be the first real non-virtual pick*/
                    FstPck = TopPck;
                    memset(CurPck, 0, sizeof(PICKS_DATA));
                }
                else
                {
                    CurPck = (PICKS_DATA *) calloc(1, sizeof(PICKS_DATA));
                    if (CurPck == NULL)
                    {
                        sFreeUpMemory((PICKS_DATA *) TopPck,
                                      prtnum_list, ftpcod_list,
                                      prt_client_id_list,
                                      brkval_list, untqty_list);
                        return (srvResults(eNO_MEMORY, NULL));
                    }
                    memset(CurPck, 0, sizeof(PICKS_DATA));
                    CurPck->prev = LstPck;
                    LstPck->next = CurPck;
                }
                if ((prtnumPtr) && strlen(prtnumPtr))
                    strncpy(CurPck->prtnum, prtnumPtr,
                            misTrimLen(prtnumPtr, PRTNUM_LEN));
                else
                    strncpy(CurPck->prtnum, "PartNum", PRTNUM_LEN);
                if ((ftpcodPtr) && strlen(ftpcodPtr))
                    strncpy(CurPck->ftpcod, ftpcodPtr,
                            misTrimLen(ftpcodPtr, FTPCOD_LEN));
                else
                    strncpy(CurPck->ftpcod, "Footprint", FTPCOD_LEN);
                if ((prt_client_idPtr) && strlen(prt_client_idPtr))
                    strncpy(CurPck->prt_client_id, prt_client_idPtr,
                            misTrimLen(prt_client_idPtr, CLIENT_ID_LEN));
                else
                    strncpy(CurPck->prt_client_id, "PartClient", CLIENT_ID_LEN);
                if ((CurPck == FstPck) ||
                   ((strcmp(CurPck->prtnum, LstPck->prtnum) != 0) || 
                    (strcmp(CurPck->prt_client_id, LstPck->prt_client_id) != 0)))
                {
                      
                        
                    sprintf (buffer, 
                    "  select prtmst.ltlcls, "
                    "         nvl(prtmst.parcel_flg,0) parcel_flg, "
                    "         nvl(prtmst.hazmat_flg,0) hazmat_flg, "
                    "         prtmst.stccod, "
                                        "         prtmst.nstcls  "
                    "    from prtmst "
                    "   where prtmst.prtnum = '%s' "
                    "     and prtmst.prt_client_id = '%s' ", 
                    CurPck->prtnum,
                    CurPck->prt_client_id);
  
                    rpkcls_return_status = sqlExecStr(buffer, &res);
                    if (rpkcls_return_status != eOK)
                    {
                        sqlFreeResults(res);
                        misLogError(
                                "Error(%ld) Reading from Part Master ",
                                rpkcls_return_status);
                        
                        return (srvResults(rpkcls_return_status, NULL));
                    }
                    row = sqlGetRow(res);
                    misTrimcpy(nstcls,
                               sqlGetString(res, row, "nstcls"),
                               misTrimLen(sqlGetString(res, row,"nstcls"), NSTCLS_LEN));
                    if (!sqlIsNull(res, row, "stccod"))
                    {
                        misTrimcpy(stccod,
                                   sqlGetString(res, row, "stccod"),
                                   misTrimLen(sqlGetString(res, row,"stccod"), STCCOD_LEN));
                    }
                    if (!sqlIsNull(res, row, "ltlcls"))
                    {
                        misTrimcpy(ltlcls,
                                   sqlGetString(res, row, "ltlcls"),
                                   misTrimLen(sqlGetString(res, row,"ltlcls"), LTLCLS_LEN));
                    }

                    if (!sqlIsNull(res, row, "parcel_flg"))
                        parcel_flg = sqlGetLong(res, row, "parcel_flg");
                    if (!sqlIsNull(res, row, "hazmat_flg"))
                        hazmat_flg = sqlGetLong(res, row, "hazmat_flg");
                    /* Repack Class needs to do a select here to gather the
                     * data from the prtrpk */
                    /* Take a max here so that if a repack class does */
                    /* not exist we can default it to the string NULL */
                    sprintf(rpkcls_buffer,
                            "select nvl(max(rpkcls),'NULL') rpkcls " 
                            "  from prtrpk " 
                            " where prtnum = '%s' "
                            "   and prt_client_id = '%s'", 
                            CurPck->prtnum, 
                            CurPck->prt_client_id);

                    rpkcls_return_status = sqlExecStr(rpkcls_buffer,
                                                      &rpkcls_res);

                    if (rpkcls_return_status != eOK)
                    {
                        sqlFreeResults(rpkcls_res);
                        sqlFreeResults(res);
                        misLogError(
                                "Error(%ld) Reading Parts Repack Class",
                                rpkcls_return_status);
                        return (srvResults(rpkcls_return_status, NULL));
                    }

                    rpkcls_row = sqlGetRow(rpkcls_res);
                    misTrimcpy(rpkcls,
                               sqlGetString(rpkcls_res, rpkcls_row, "rpkcls"),
                               misTrimLen(sqlGetString(rpkcls_res, rpkcls_row,"rpkcls"),RPKCLS_LEN));
                                  
                    sqlFreeResults(rpkcls_res);
                    /*We also need the code master LTL rank for each part */
                    /*We take the max and default not existing to -1*/
                    /*negative numbers cannot be inputed from code maintenance*/
                    sprintf(rpkcls_buffer,
                            "select to_number(nvl(max(srtseq),-1)) ltlrnk " 
                            "  from codmst " 
                            " where colnam = 'ltlcls' "
                            "   and codval = '%s'", 
                            CurPck->ltlcls);
                               
                    rpkcls_return_status = sqlExecStr(rpkcls_buffer, &rpkcls_res);

                    if (rpkcls_return_status != eOK)
                    {
                        sqlFreeResults(rpkcls_res);
                        sqlFreeResults(res);
                        misLogError("Error(%ld) Reading from Code Master",
                                    rpkcls_return_status);
                        return (srvResults(rpkcls_return_status, NULL));
                    }

                    rpkcls_row = sqlGetRow(rpkcls_res);
                    ltlrnk = sqlGetLong(rpkcls_res, rpkcls_row, "ltlrnk");
                    sqlFreeResults(rpkcls_res);

                    /*We also need the code master STCCOD for each part */
                    /*We take the max and default not existing to -1*/
                    /*negative numbers cannot be inputed from code maintenance*/
                    sprintf(rpkcls_buffer,
                            "select to_number(nvl(max(srtseq),-1)) stcrnk " 
                            "  from codmst " 
                            " where colnam = 'stccod' "
                            "   and codval = '%s'", 
                            CurPck->stccod);
                               
                    rpkcls_return_status = sqlExecStr(rpkcls_buffer, &rpkcls_res);

                    if (rpkcls_return_status != eOK)
                    {
                        sqlFreeResults(rpkcls_res);
                        sqlFreeResults(res);
                        misLogError("Error(%ld) Reading from Code Master",
                                    rpkcls_return_status);
                        return (srvResults(rpkcls_return_status, NULL));
                    }
                    rpkcls_row = sqlGetRow(rpkcls_res);
                    stcrnk = sqlGetLong(rpkcls_res, rpkcls_row, "stcrnk");
                    sqlFreeResults(rpkcls_res);
                    sqlFreeResults(res);
                }
                memset(CurPck->stccod, 0, sizeof(CurPck->stccod));
                memset(CurPck->ltlcls, 0, sizeof(CurPck->ltlcls));
                memset(CurPck->rpkcls, 0, sizeof(CurPck->rpkcls));
                memset(CurPck->nstcls, 0, sizeof(CurPck->nstcls));
                misTrimcpy(CurPck->ltlcls,ltlcls,misTrimLen(ltlcls,LTLCLS_LEN));
                misTrimcpy(CurPck->stccod,stccod,misTrimLen(stccod,STCCOD_LEN));  
                misTrimcpy(CurPck->rpkcls,rpkcls,misTrimLen(rpkcls,RPKCLS_LEN));
                misTrimcpy(CurPck->nstcls,nstcls,misTrimLen(rpkcls,NSTCLS_LEN)); 
                CurPck->parcel_flg = parcel_flg;
                CurPck->hazmat_flg = hazmat_flg;
                CurPck->ltlrnk = ltlrnk;
                CurPck->stcrnk = stcrnk;
                LstPck = CurPck;
                CurPck->isFirst = BOOLEAN_FALSE;
                CurPck->carton = NULL;
                memset(CurPck->wrkref, 0, sizeof(CurPck->wrkref));
                sprintf(CurPck->wrkref, "WRK%5.5ld", new_ctnnum_cnt);
                strncpy(CurPck->cmbcod, "ComboCode", CMBCOD_LEN);
                strncpy(CurPck->arecod, "Area", ARECOD_LEN);
                strncpy(CurPck->stoloc, "SourceLoc", STOLOC_LEN);
                strncpy(CurPck->velzon, "V", VELZON_LEN);
                strcpy(CurPck->wrkzon, "99");
                strncpy(CurPck->trvseq, "TravelSeq", TRVSEQ_LEN);
                
                if ((brkvalPtr) && strlen(brkvalPtr))
                    strncpy(CurPck->brkval, brkvalPtr,OBJECT_ID_LEN + RTSTR1_LEN);
                else
                    strncpy(CurPck->brkval, "BreakValue", RTSTR1_LEN);

                CurPck->dtlvol = -1.0;
                CurPck->dtllen = 0.0;
                CurPck->dtlwid = 0.0;
                CurPck->dtlhgt = 0.0;
                CurPck->dtlwgt = 0.0;
                CurPck->untlen = 0.0;
                CurPck->paklen = 0.0;
                CurPck->untpak = 1;
                if (split_qty == BOOLEAN_TRUE)
                    CurPck->pckqty = 1;
                else
                    CurPck->pckqty = atoi(untqtyPtr);
            }

            prtnumPtr = prtnumNxt;
            ftpcodPtr = ftpcodNxt;
            prt_client_idPtr = prt_client_idNxt;
            brkvalPtr = brkvalNxt;
            untqtyPtr = untqtyNxt;
        }
        while (untqtyPtr);

        /* First read the parts (and join footprints) if requested */
        if (prtnum_cnt > 0)
        {
            sqlbuffer = NULL;
            sqlbuffer =
                    (char *) calloc(1, 1000 + (untqty_cnt * (PRTNUM_LEN + 3)));

            /* 04/18/2000 Hoefflin and Verdeyen looked at this piece of code */
            /* which was trying to deal with getting an input list of ftpcod */
            /* values and prtnum values differently than getting ftpcod and */
            /* prtnum list when we are in "what if" mode. What happened is */
            /* that if you sent in both, the second (shorter) select was */
            /* used and the routine would exit later because we didn't read */
            /* all of the required dimensional information. Therefore, we */
            /* just removed the check and let it work regardless of getting */
            /* both ftps and parts. */
            /* if (ftpcod_cnt == 0)        */
            sprintf(sqlbuffer,
                        "select prtmst.prtnum, ftpmst.ftpcod, "
                        "       prtmst.prt_client_id, "
                        "       prtmst.untlen untlenprt, "
                        "       prtmst.paklen paklenprt, "
                        "       (prtmst.netwgt / prtmst.untcas) netwgt, "
                        "       (prtmst.grswgt / prtmst.untcas) grswgt, "
                        "       (ftpmst.untlen * ftpmst.untwid * "
                        "        ftpmst.unthgt) / 1.0 dtlvolunt, "
                        "       (ftpmst.paklen * ftpmst.pakwid * "
                        "        ftpmst.pakhgt) / prtmst.untpak dtlvolpak, "
                        "       (ftpmst.caslen * ftpmst.caswid * "
                        "        ftpmst.cashgt) / prtmst.untcas dtlvolcas, "
                        "       ftpmst.untlen dtllen, "
                        "       ftpmst.untwid dtlwid, "
                        "       ftpmst.unthgt dtlhgt, "
                        "       prtmst.untpak, "
                            "       prtmst.ltlcls, "
                        "       prtmst.nstcls, "
                        "       ftpmst.nstlen, "
                        "       ftpmst.nstwid, "
                        "       ftpmst.nsthgt, "
                            "       nvl(prtmst.parcel_flg,0) parcel_flg, "
                            "       nvl(prtmst.hazmat_flg,0) hazmat_flg, "
                            "       prtmst.stccod "
                        "  from ftpmst, prtmst "
                        " where prtmst.ftpcod = ftpmst.ftpcod "
                        "   and (prtmst.prtnum || prtmst.prt_client_id) in (");
            
            if (prtnum_list_i && misTrimLen(prtnum_list_i, PRTNUM_LEN))
            {
                memset(prtnum_list, 0, strlen(prtnum_list_i) + 1);
                strncpy(prtnum_list, prtnum_list_i, strlen(prtnum_list_i));
            }
            if (ftpcod_list_i && misTrimLen(ftpcod_list_i, FTPCOD_LEN))
            {
                memset(ftpcod_list, 0, strlen(ftpcod_list_i) + 1);
                strncpy(ftpcod_list, ftpcod_list_i, strlen(ftpcod_list_i));
            }
            if (prt_client_id_list_i &&
                                misTrimLen(prt_client_id_list_i, CLIENT_ID_LEN))
            {
                memset(prt_client_id_list, 0, strlen(prt_client_id_list_i) + 1);
                strncpy(prt_client_id_list, prt_client_id_list_i,
                                                strlen(prt_client_id_list_i));
            }
            if (brkval_list_i && misTrimLen(brkval_list_i, RTSTR1_LEN))
            {
                memset(brkval_list, 0, strlen(brkval_list_i) + 1);
                strncpy(brkval_list, brkval_list_i, strlen(brkval_list_i));
            }
            if (untqty_list_i && misTrimLen(untqty_list_i, RTSTR1_LEN))
            {
                memset(untqty_list, 0, strlen(untqty_list_i) + 1);
                strncpy(untqty_list, untqty_list_i, strlen(untqty_list_i));
            }
            prtnumPtr = prtnum_list;
            prt_client_idPtr = prt_client_id_list;
            untqtyPtr = untqty_list;
            prtnumNxt = untqtyNxt = NULL;
            prt_client_idNxt = NULL;
            do
            {
                prtnumNxt = strchr(prtnumPtr, ',');
                if (prtnumNxt)
                    *prtnumNxt++ = '\0';
                if (prt_client_idPtr)
                    prt_client_idNxt = strchr(prt_client_idPtr, ',');
                if (prt_client_idNxt)
                    *prt_client_idNxt++ = '\0';
                untqtyNxt = strchr(untqtyPtr, ',');
                if (untqtyNxt)
                    *untqtyNxt++ = '\0';

                if (atoi(untqtyPtr) < 1)
                    continue;

                strcat(sqlbuffer, "('");
                strncat(sqlbuffer, prtnumPtr, misTrimLen(prtnumPtr,PRTNUM_LEN));
                strcat(sqlbuffer, "'||'");
                if (prt_client_idPtr)
                    strncat(sqlbuffer, prt_client_idPtr,
                                misTrimLen(prt_client_idPtr, CLIENT_ID_LEN));
                strcat(sqlbuffer, "'),");

                prtnumPtr = prtnumNxt;
                prt_client_idPtr = prt_client_idNxt;
                untqtyPtr = untqtyNxt;
            }
            while (untqtyPtr);
            sqlbuffer[strlen(sqlbuffer) - 1] = ')';

            ret_status = sqlExecStr(sqlbuffer, &res);

            free(sqlbuffer);
            if (ret_status != eOK)
            {
                sFreeUpMemory((PICKS_DATA *) TopPck,
                              prtnum_list, ftpcod_list,
                              prt_client_id_list,
                              brkval_list, untqty_list);
                misLogError("Error selecting from part master - %ld!",
                            ret_status);
                return (srvResults(ret_status, NULL));
            }

            /* Now loop through and fill up all of the information from the */
            /* results set into the structure */
            for (CurPck = TopPck; CurPck; CurPck = CurPck->next)
            {
                for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
                {
                    
                    
                    if ( (strncmp(CurPck->prtnum,
                                sqlGetString(res, row, "prtnum"),
                                misTrimLen(sqlGetString(res,
                                          row, "prtnum"), PRTNUM_LEN)) == 0) &&
                         (strncmp(CurPck->prt_client_id,
                                sqlGetString(res, row, "prt_client_id"),
                                misTrimLen(sqlGetString(res,
                                  row, "prt_client_id"), CLIENT_ID_LEN)) == 0) )
                    {                            
                        if (!sqlIsNull(res, row, "ftpcod"))
                        {
                            memset(CurPck->ftpcod, 0, sizeof(CurPck->ftpcod));
                            strncpy(CurPck->ftpcod,
                                    sqlGetString(res, row, "ftpcod"),
                                    misTrimLen(sqlGetString(res, row,
                                                    "ftpcod"), FTPCOD_LEN));
                        }
                        if (!sqlIsNull(res, row, "nstcls"))
                        {
                            strncpy(CurPck->nstcls,sqlGetString(res, row, "nstcls"),
                            misTrimLen(sqlGetString(res, row, "nstcls"),NSTCLS_LEN));
                            CurPck->isVirtual = BOOLEAN_TRUE;
                        } 
                        else
                        {
                            CurPck->isVirtual = BOOLEAN_FALSE;
                        }
                        CurPck->isNesting = BOOLEAN_FALSE; 
                        if (!sqlIsNull(res, row, "dtlvolcas"))
                            CurPck->dtlvol = sqlGetFloat(res, row, "dtlvolcas");
                        if (!sqlIsNull(res, row, "dtlvolpak") &&
                            sqlGetFloat(res, row, "dtlvolpak") > 0.0)
                            CurPck->dtlvol =
                                sqlGetFloat(res, row, "dtlvolpak");
                        if (!sqlIsNull(res, row, "dtlvolunt") &&
                            sqlGetFloat(res, row, "dtlvolunt") > 0.0)
                            CurPck->dtlvol = sqlGetFloat(res, row, "dtlvolunt");
                        if (!sqlIsNull(res, row, "dtllen"))
                            CurPck->dtllen = sqlGetFloat(res, row, "dtllen");
                        if (!sqlIsNull(res, row, "dtlwid"))
                            CurPck->dtlwid = sqlGetFloat(res, row, "dtlwid");
                        if (!sqlIsNull(res, row, "dtlhgt"))
                            CurPck->dtlhgt = sqlGetFloat(res, row, "dtlhgt");
                        if (!sqlIsNull(res, row, "netwgt"))
                            CurPck->dtlwgt = sqlGetFloat(res, row, "netwgt");
                        if (!sqlIsNull(res, row, "grswgt"))
                            CurPck->dtlwgt = sqlGetFloat(res, row, "grswgt");
                        if (!sqlIsNull(res, row, "untlenprt"))
                            CurPck->untlen = sqlGetFloat(res, row, "untlenprt");
                        if (!sqlIsNull(res, row, "paklenprt"))
                            CurPck->paklen = sqlGetFloat(res, row, "paklenprt");
                        if (!sqlIsNull(res, row, "untpak"))
                            CurPck->untpak = sqlGetLong(res, row, "untpak");
                        if (!sqlIsNull(res, row, "nstlen"))
                            CurPck->nstlen = sqlGetFloat(res, row, "nstlen");
                        if (!sqlIsNull(res, row, "nstwid"))
                            CurPck->nstwid = sqlGetFloat(res, row, "nstwid");
                        if (!sqlIsNull(res, row, "nsthgt"))
                            CurPck->nsthgt = sqlGetFloat(res, row, "nsthgt");
                        break;
                    }
                }
            }
            sqlFreeResults(res);
        }

        /* Read the footprints if only footprints where given */
        if (prtnum_cnt == 0 && ftpcod_cnt > 0)
        {
            sqlbuffer = NULL;
            sqlbuffer = (char *) calloc(1, 1000 + (untqty_cnt * (FTPCOD_LEN + 3)));

            sprintf(sqlbuffer,
                    "select ftpmst.ftpcod, "
                    "       (ftpmst.untlen * ftpmst.untwid * "
                    "        ftpmst.unthgt) / 1.0 dtlvolunt, "
                    "       (ftpmst.paklen * ftpmst.pakwid * "
                    "        ftpmst.pakhgt) / 1.0 dtlvolpak, "
                    "       (ftpmst.caslen * ftpmst.caswid * "
                    "        ftpmst.cashgt) / 1.0 dtlvolcas, "
                    "       ftpmst.untlen dtllen, "
                    "       ftpmst.untwid dtlwid, "
                    "       ftpmst.unthgt dtlhgt,  "
                    "       ftpmst.nstlen, "
                    "       ftpmst.nstwid, "
                    "       ftpmst.nsthgt  "
                    "  from ftpmst "
                    " where ftpmst.ftpcod in (");
            if (prtnum_list_i && misTrimLen(prtnum_list_i, PRTNUM_LEN))
            {
                memset(prtnum_list, 0, strlen(prtnum_list_i) + 1);
                strncpy(prtnum_list, prtnum_list_i, strlen(prtnum_list_i));
            }
            if (ftpcod_list_i && misTrimLen(ftpcod_list_i, FTPCOD_LEN))
            {
                memset(ftpcod_list, 0, strlen(ftpcod_list_i) + 1);
                strncpy(ftpcod_list, ftpcod_list_i, strlen(ftpcod_list_i));
            }
            if (prt_client_id_list_i && misTrimLen(prt_client_id_list_i,
                                                   CLIENT_ID_LEN))
            {
                memset(prt_client_id_list, 0, strlen(prt_client_id_list_i) + 1);
                strncpy(prt_client_id_list, prt_client_id_list_i,
                                            strlen(prt_client_id_list_i));
            }
            if (brkval_list_i && misTrimLen(brkval_list_i, RTSTR1_LEN))
            {
                memset(brkval_list, 0, strlen(brkval_list_i) + 1);
                strncpy(brkval_list, brkval_list_i, strlen(brkval_list_i));
            }
            if (untqty_list_i && misTrimLen(untqty_list_i, RTSTR1_LEN))
            {
                memset(untqty_list, 0, strlen(untqty_list_i) + 1);
                strncpy(untqty_list, untqty_list_i, strlen(untqty_list_i));
            }
            ftpcodPtr = ftpcod_list;
            untqtyPtr = untqty_list;
            ftpcodNxt = untqtyNxt = NULL;
            do
            {
                ftpcodNxt = strchr(ftpcodPtr, ',');
                if (ftpcodNxt)
                    *ftpcodNxt++ = '\0';
                untqtyNxt = strchr(untqtyPtr, ',');
                if (untqtyNxt)
                    *untqtyNxt++ = '\0';

                if (atoi(untqtyPtr) < 1)
                    continue;

                strcat(sqlbuffer, "'");
                strncat(sqlbuffer, ftpcodPtr, misTrimLen(ftpcodPtr, FTPCOD_LEN));
                strcat(sqlbuffer, "',");

                ftpcodPtr = ftpcodNxt;
                untqtyPtr = untqtyNxt;
            }
            while (untqtyPtr);
            sqlbuffer[strlen(sqlbuffer) - 1] = ')';
            ret_status = sqlExecStr(sqlbuffer, &res);
            free(sqlbuffer);
            if (ret_status != eOK)
            {
                sFreeUpMemory((PICKS_DATA *) TopPck,
                              prtnum_list, ftpcod_list,
                              prt_client_id_list,
                              brkval_list, untqty_list);
                misLogError("Error selecting from footprint master - %ld!",
                            ret_status);
                return (srvResults(ret_status, NULL));
            }
            /* Now loop through and fill up all of the information from the */
            /* results set into the structure */
            for (CurPck = TopPck; CurPck; CurPck = CurPck->next)
            {
                for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
                {
                    if (strncmp(CurPck->ftpcod,
                                sqlGetString(res, row, "ftpcod"),
                                misTrimLen(sqlGetString(res,
                                          row, "ftpcod"), FTPCOD_LEN)) == 0)
                    {
                        if (!sqlIsNull(res, row, "dtlvolcas"))
                            CurPck->dtlvol =
                                sqlGetFloat(res, row, "dtlvolcas");
                        if (!sqlIsNull(res, row, "dtlvolpak") &&
                        sqlGetFloat(res, row, "dtlvolpak") > 0.0)
                            CurPck->dtlvol =
                                sqlGetFloat(res, row, "dtlvolpak");
                        if (!sqlIsNull(res, row, "dtlvolunt") &&
                        sqlGetFloat(res, row, "dtlvolunt") > 0.0)
                            CurPck->dtlvol =
                                sqlGetFloat(res, row, "dtlvolunt");
                        if (!sqlIsNull(res, row, "dtllen"))
                            CurPck->dtllen = sqlGetFloat(res, row, "dtllen");
                        if (!sqlIsNull(res, row, "dtlwid"))
                            CurPck->dtlwid = sqlGetFloat(res, row, "dtlwid");
                        if (!sqlIsNull(res, row, "dtlhgt"))
                            CurPck->dtlhgt = sqlGetFloat(res, row, "dtlhgt");
                        break;
                    }
                }
            }
            sqlFreeResults(res);
        }

        /* At this point, if we have any negative volumes, we missed */
        /* something and we should just skip out of here */
        for (CurPck = TopPck; CurPck; CurPck = CurPck->next)
        {
            if (CurPck->dtlvol < 0)
            {
                misLogError("Could not find all part/footprint info"
                            " for part %s, prt_client_id %s, footprint %s!",
                            CurPck->prtnum, CurPck->prt_client_id, CurPck->ftpcod);
                sFreeUpMemory((PICKS_DATA *) TopPck,
                              prtnum_list, ftpcod_list,
                              prt_client_id_list,
                              brkval_list, untqty_list);
                return (srvResults(eINT_PARM_MISMATCH, NULL));
            }
        }
    }

    /* Select to get the largest package by volume */
    if (CtnMstRes == NULL)
    {
        /* We will now select the cartons rpkcls */
        /* We use set rpkcls to NULL when NULL to make all  */
        /* strcmps easier and not having to worry about using */
        /* isNULL all the time */
          sprintf(buffer, 
        " select nvl(ctnrpk.rpkcls, 'NULL') rpkcls, "
        "        ctnmst.ctncod, "
        "        ctnmst.ctnlen, "
        "        ctnmst.ctnwid, "
        "        ctnmst.ctnhgt, "
        "        ctnmst.ctnwlm, "
        "        (ctnmst.ctnlen * ctnmst.ctnhgt * ctnmst.ctnwid) * (ctnmst.ctnfpc/100) ctnvol "
        "   from ctnrpk, " 
        "        ctnmst "
        "  where ctnmst.ctncod = ctnrpk.ctncod (+) " 
        " order by  "
        "          ctnvol desc ");

        ret_status = sqlExecStr(buffer, &CtnMstRes);
        if (ret_status != eOK)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            misLogError("Error selecting from carton master  - %ld!",
                        ret_status);
            sqlFreeResults(CtnMstRes);
            CtnMstRes = NULL;
            return (srvResults(ret_status, NULL));
        }
        misFlagCachedMemory((OSFPTR) sqlFreeResults, CtnMstRes);
    }

    /* If we received a force_ctncod parameter, let's just verify that it is valid */
    if (strlen(force_ctncod))
    {
        for (row = sqlGetRow(CtnMstRes); row; row = sqlGetNextRow(row))
        {
          if (strncmp(force_ctncod,
                      sqlGetString(CtnMstRes,row,"ctncod"), 
                      CTNCOD_LEN) == 0)
              break;
        }
    }
    /* Bad carton code value sent in as a forced parameter */
    if (row == NULL)
    {
        sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
        misLogError("Forced carton code parameter value %s does "
                    "not exist on Carton Master", force_ctncod);
        sqlFreeResults(CtnMstRes);
        CtnMstRes = NULL;
        return (srvResults(eINT_PARM_MISMATCH, NULL));        
    }

    /* Now, get the first carton master row */
    row = sqlGetRow(CtnMstRes);

    memset(max_ctncod, 0, sizeof(max_ctncod));
    strncpy(max_ctncod, sqlGetString(CtnMstRes, row, "ctncod"),
            misTrimLen(sqlGetString(CtnMstRes, row, "ctncod"),
                       CTNCOD_LEN));
    max_ctnvol = (sqlGetFloat(CtnMstRes, row, "ctnvol"));
    max_ctnwlm = (sqlGetFloat(CtnMstRes, row, "ctnwlm"));

    /* If we are processing for real ... then get the picks ... */
    if (whatIfMode == FALSE)
    {
        memset(identifier_buffer,0,sizeof(identifier_buffer));
        if (strlen(wrkref))
            sprintf(identifier_buffer, "wrkref = '%s'",wrkref);
        else
             sprintf(identifier_buffer, "ship_id = '%s'",ship_id);    
        /*Make this a hook point for project teams to customize */
        ret_status = srvInitiateCommandFormat(&resultData,"list picks for cartonization where break_on = '%s' "
                                                                                       "  and lodlvl = '%s' "
                                                                                       "  and wrktyp = '%s' "
                                                                                       "  and include_buffer = '%s' "
                                                                                       "  and order_by = '%s' "
                                                                                       "  and %s ", 
                                               break_on,
                                               LODLVL_DETAIL,
                                               WRKTYP_PICK,
                                               include_buffer_moca,
                                               order_by,
                                               identifier_buffer);
        
        res = resultData->ReturnedData;
            
        if (ret_status != eOK)
        {
            sFreeUpMemory((PICKS_DATA *) TopPck,
                          prtnum_list, ftpcod_list,
                          prt_client_id_list,
                          brkval_list, untqty_list);
            srvFreeMemory (SRVRET_STRUCT, resultData);
            if (ret_status == eDB_NO_ROWS_AFFECTED)
            {
                ret_status = eOK;
                misTrc(T_FLOW, "wrkref %s, ship_id %s, "
                            "has no outstanding picks!",
                            wrkref, ship_id);
                return (srvResults(ret_status, NULL));
            }
            misLogError("Error selecting outstanding picks - %ld!",
                        ret_status);
            return (srvResults(ret_status, NULL));
        }
        row = sqlGetRow(res);

        /* If we were not processing by work reference, grab one for later */
        if (!strlen(wrkref))
            strncpy(wrkref,
                    sqlGetString(res, row, "wrkref"),
                    misTrimLen(sqlGetString(res, row, "wrkref"),
                               WRKREF_LEN));

        /* And load up our structure of information */
        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            if (split_qty == BOOLEAN_TRUE)
                loop_inc = 1;
            else
                loop_inc = sqlGetLong(res, row, "pckqty");

            /* Now, we are going to loop through and either put out one */
            /* structure entry for the entire quantity of the pick work */
            /* or entries of quantity = 1 if it is OK to split them up */
            for (ii = 0;
                 ii < sqlGetLong(res, row, "pckqty");
                 ii += loop_inc)
            {
                if (TopPck == NULL)
                {
                    TopPck = (PICKS_DATA *) calloc(1, sizeof(PICKS_DATA));
                    if (TopPck == NULL)
                    {
                        sqlFreeResults(res);
                        sFreeUpMemory((PICKS_DATA *) TopPck,
                                      prtnum_list, ftpcod_list,
                                      prt_client_id_list,
                                      brkval_list, untqty_list);
                        return (srvResults(eNO_MEMORY, NULL));
                    }
                    CurPck = TopPck;
                    FstPck = TopPck;
                    memset(CurPck, 0, sizeof(PICKS_DATA));
                }
                else
                {
                    CurPck = (PICKS_DATA *) calloc(1, sizeof(PICKS_DATA));
                    if (CurPck == NULL)
                    {
                        sqlFreeResults(res);
                        sFreeUpMemory((PICKS_DATA *) TopPck,
                                      prtnum_list, ftpcod_list,
                                      prt_client_id_list,
                                      brkval_list, untqty_list);
                        return (srvResults(eNO_MEMORY, NULL));
                    }
                    memset(CurPck, 0, sizeof(PICKS_DATA));
                    CurPck->prev = LstPck;
                    LstPck->next = CurPck;
                }
                LstPck = CurPck;
                CurPck->isFirst = BOOLEAN_FALSE;
                CurPck->carton = NULL;
                strncpy(CurPck->rpkcls,sqlGetString(res, row, "rpkcls"),
                       misTrimLen(sqlGetString(res, row, "rpkcls"),RPKCLS_LEN));
                if (!sqlIsNull(res, row, "nstcls"))
                {
                    strncpy(CurPck->nstcls,sqlGetString(res, row, "nstcls"),
                           misTrimLen(sqlGetString(res, row, "nstcls"),NSTCLS_LEN));
                    CurPck->isVirtual = BOOLEAN_TRUE;
                } 
                else
                {
                    CurPck->isVirtual = BOOLEAN_FALSE;
                }
                CurPck->isNesting = BOOLEAN_FALSE; 
                strncpy(CurPck->wrkref, sqlGetString(res, row, "wrkref"),
                        misTrimLen(sqlGetString(res, row, "wrkref"),
                                   WRKREF_LEN));
                strncpy(CurPck->cmbcod, sqlGetString(res, row, "cmbcod"),
                        misTrimLen(sqlGetString(res, row, "cmbcod"),
                                   CMBCOD_LEN));
                strncpy(CurPck->arecod, sqlGetString(res, row, "arecod"),
                        misTrimLen(sqlGetString(res, row, "arecod"),
                                   ARECOD_LEN));
                strncpy(CurPck->stoloc, sqlGetString(res, row, "stoloc"),
                        misTrimLen(sqlGetString(res, row, "stoloc"),
                                   STOLOC_LEN));
                strncpy(CurPck->velzon, sqlGetString(res, row, "velzon"),
                        misTrimLen(sqlGetString(res, row, "velzon"),
                                   VELZON_LEN));
                strncpy(CurPck->wrkzon, sqlGetString(res, row, "wrkzon"),
                        misTrimLen(sqlGetString(res, row, "wrkzon"),
                                   WRKZON_LEN));
                if (!sqlIsNull(res, row, "trvseq"))
                    strncpy(CurPck->trvseq,
                            sqlGetString(res, row, "trvseq"),
                        misTrimLen(sqlGetString(res, row, "trvseq"),
                                   TRVSEQ_LEN));
                strncpy(CurPck->prtnum, sqlGetString(res, row, "prtnum"),
                        misTrimLen(sqlGetString(res, row, "prtnum"),
                                   PRTNUM_LEN));
                strncpy(CurPck->prt_client_id,
                                sqlGetString(res, row, "prt_client_id"),
                     misTrimLen(sqlGetString(res, row, "prt_client_id"),
                                                               CLIENT_ID_LEN));
                if (!sqlIsNull(res, row, "ftpcod"))
                    strncpy(CurPck->ftpcod,
                            sqlGetString(res, row, "ftpcod"),
                        misTrimLen(sqlGetString(res, row, "ftpcod"),
                                   FTPCOD_LEN));
                if (!sqlIsNull(res, row, "dtlvolcas"))
                    CurPck->dtlvol = sqlGetFloat(res, row, "dtlvolcas");
                if (!sqlIsNull(res, row, "dtlvolpak") &&
                    sqlGetFloat(res, row, "dtlvolpak") > 0.0)
                    CurPck->dtlvol = sqlGetFloat(res, row, "dtlvolpak");
                if (!sqlIsNull(res, row, "dtlvolunt") &&
                    sqlGetFloat(res, row, "dtlvolunt") > 0.0)
                    CurPck->dtlvol = sqlGetFloat(res, row, "dtlvolunt");
                CurPck->dtllen = sqlGetFloat(res, row, "dtllen");
                CurPck->dtlwid = sqlGetFloat(res, row, "dtlwid");
                CurPck->dtlhgt = sqlGetFloat(res, row, "dtlhgt");
                CurPck->nstlen = sqlGetFloat(res, row, "nstlen");
                CurPck->nstwid = sqlGetFloat(res, row, "nstwid");
                CurPck->nsthgt = sqlGetFloat(res, row, "nsthgt");
                CurPck->dtlwgt = 0.0;
                if (!sqlIsNull(res, row, "netwgt"))
                    CurPck->dtlwgt = sqlGetFloat(res, row, "netwgt");
                if (!sqlIsNull(res, row, "grswgt"))
                    CurPck->dtlwgt = sqlGetFloat(res, row, "grswgt");
                CurPck->untlen = 0.0;
                if (!sqlIsNull(res, row, "untlenprt"))
                    CurPck->untlen = sqlGetFloat(res, row, "untlenprt");
                CurPck->paklen = 0.0;
                if (!sqlIsNull(res, row, "paklenprt"))
                    CurPck->paklen = sqlGetFloat(res, row, "paklenprt");
                CurPck->untpak = 0;
                if (!sqlIsNull(res, row, "untpak"))
                    CurPck->untpak = sqlGetLong(res, row, "untpak");
                if (split_qty == BOOLEAN_TRUE)
                    CurPck->pckqty = 1;
                else
                    CurPck->pckqty = sqlGetLong(res, row, "pckqty");
                if (!sqlIsNull(res, row, "brkval"))
                    strncpy(CurPck->brkval,
                            sqlGetString(res, row, "brkval"),
                        misTrimLen(sqlGetString(res, row, "brkval"),
                                   OBJECT_ID_LEN + RTSTR1_LEN));
            }
        }
        srvFreeMemory (SRVRET_STRUCT, resultData);
    }

    /* Start off at the top of the structure and move through it */
    new_ctnnum_cnt = 0;
    LstPck = TopPck;
    cur_ctn_vol = 0.0;
    cur_ctn_wgt = 0.0;
    cur_ctn_unt = 0;

    /* Spin through the result structure to first create carton pckwrk */
    /* entries and pckmov entries if desired. Upon completion of each */
    /* work reference, we will update it, and after each carton, we will */
    /* return it as part of the result set ... */

    tmprow = NULL;

    for (CurPck = TopPck; CurPck; CurPck = CurPck->next)
    {
        /* If we are forcing a unit quantity per carton, then zero out pick volume */
        /* and pick weight because we don't want to break on those. */
        if(force_untper > 0)
        {
          pckvol = 0.0;
          pckwgt = 0.0;
        }
        else
        {
          pckvol = ((double) CurPck->pckqty * CurPck->dtlvol);
          pckwgt = ((double) CurPck->pckqty * CurPck->dtlwgt);
        }
        untqty = CurPck->pckqty;

        /* Fire wall this baby a little ... if our volume or weight exceeds */
        /* our biggest carton, then just set it to that biggest carton */
        if (pckvol > max_ctnvol)
            pckvol = max_ctnvol;
        if (pckwgt > max_ctnwlm)
            pckwgt = max_ctnwlm;

        /* Now, just check to see if the current piece won't fit dimensionally */
        /* in this carton ... and see if there is another carton that it will fit */
        /* in ... if so, then break the carton ... */
        /* A future enhancement may be to downsize the max carton on the fly if our */
        /* current piece won't fit, and their is a smaller carton that will take it */
        /* based on orientation, volume and weight. We would have to make sure that */
        /* consider the ramifications of on-the-fly complex cartonization, because */
        /* that is what would be required to ensure that as we downsized, the */
        /* current contents of the carton would really fit into the smaller carton */
        odd_piece_carton_break = BOOLEAN_FALSE;
        if (CurPck != TopPck && tmprow)
        {
                if ((sValidatePieceFitsInCartonByDimension(CurPck->dtllen,
                                                           CurPck->dtlwid,
                                                           CurPck->dtlhgt,
                                                           sqlGetFloat(CtnMstRes, tmprow, "ctnlen"),
                                                           sqlGetFloat(CtnMstRes, tmprow, "ctnwid"),
                                                           sqlGetFloat(CtnMstRes, tmprow, "ctnhgt")))
                        || 
                        /* Break to new carton if does not match repack class */
                        (strncmp(CurPck->rpkcls, sqlGetString(CtnMstRes, tmprow, "rpkcls"), RPKCLS_LEN) != 0 ))   
                {
                        odd_piece_carton_break = BOOLEAN_FALSE;
                }
                else
                {
                        misTrc(T_FLOW, 
                           "Cartonization has sensed an odd dimension piece in carton '%s'",
                           new_ctnnum);

                        /* Check for a carton that this piece will actually fit in */
                        for (tmprow = sqlGetRow(CtnMstRes); tmprow; tmprow = sqlGetNextRow(tmprow))
                        {
                                if (sValidatePieceFitsInCartonByDimension(CurPck->dtllen,
                                                                          CurPck->dtlwid,
                                                                          CurPck->dtlhgt,
                                                                          sqlGetFloat(CtnMstRes, tmprow, "ctnlen"),
                                                                          sqlGetFloat(CtnMstRes, tmprow, "ctnwid"),
                                                                          sqlGetFloat(CtnMstRes, tmprow, "ctnhgt")))
                                    break;
                        }

                        /* We found an alternative, so break the carton */
                        if (tmprow != NULL)
                        {
                                odd_piece_carton_break = BOOLEAN_TRUE;
                                misTrc(T_FLOW, 
                                   "An alternative carton was found for odd piece, will break "
                                   "carton '%s' now to start odd piece in new carton.",
                                   new_ctnnum);
                        }
                }
        }

        /* If this is our first structure element, OR we have changed our */
        /* break value, OR we are over the volume or weight ... OR this has */
        /* been called with a force_untqty parameter and we are over ... */
        /* OR we don't have a force_untqty and the odd piece would fit in a */
        /* different carton, but we are currently over the weight or volume ... */
        /* OR we get a new repack class, we must split carton */
        /* Then create a new carton */
        /* Paul */
        RunSplit = BOOLEAN_FALSE;
        RpkclsFits = BOOLEAN_TRUE;
        WeightAndVolumeFit = 0;
        if (CurPck != FstPck)
            WeightAndVolumeFit = sValidateWeightAndVolume(&TopPck,
                                                          CurPck,
                                                          tempCarton,
                                                          pckvol,
                                                          pckwgt,
                                                          BOOLEAN_FALSE);
        if (CurPck != FstPck)
            RpkclsFits = sValidateRepackClassForNewPick(tempCarton->ctncod,
                                                        CurPck->rpkcls,
                                                        CtnMstRes);

            /*If the new piece will not fit in the carton for whatever reason*/
            /*It makes sence to try to put the piece the carton with the most remaining volume*/
            /*This change will deliver a lower carton count */
           
        if ((CurPck != FstPck) && 
            ((RpkclsFits == BOOLEAN_FALSE) ||
            (strcmp(CurPck->brkval, tempCarton->brkval) != 0)  ||
            (WeightAndVolumeFit == -1)))           
        {
            oldCarton = tempCarton;
            tempCarton = sFindCartonWithMostRemainingVolume(CurPck,
                                                            &TopPck,
                                                            CtnMstRes,
                                                            pckvol, 
                                                            pckwgt);
            if (tempCarton != NULL)
            {
                misTrc(T_FLOW, "Changing Current Carton");
                strncpy(new_ctnnum, tempCarton->ctnnum,CTNNUM_LEN);
                strncpy(new_wrkref, tempCarton->wrkref,WRKREF_LEN);
                /*Assume Weight And Volume fit in new carton*/
                WeightAndVolumeFit = 0;
                RpkclsFits = BOOLEAN_TRUE;
                /*If we change the carton, we want to try to split the old wrkref*/
                RunSplit = BOOLEAN_TRUE;
            }
            else
            {
                tempCarton = oldCarton;
            }
            
        }
        if (tempCarton != NULL)
            misTrc(T_FLOW, "Carton %s volume %.3f of  %.3f "
                           "and weight %.3f of %.3f "
                           "Code of %s pick weight %.3f and pick volume %.3f ",
                            tempCarton->ctnnum,
                            tempCarton->curvol,
                            tempCarton->ctnvol, 
                            tempCarton->curwgt,
                            tempCarton->ctnwlm, 
                            tempCarton->ctncod, 
                            pckwgt, 
                            pckvol);  
               if ((CurPck == FstPck) ||
            (RpkclsFits == BOOLEAN_FALSE) || 
            (strcmp(CurPck->brkval, tempCarton->brkval) != 0) ||
            (WeightAndVolumeFit == -1)                       ||
            (force_untper > 0 && (tempCarton->curunt + untqty)>force_untper) ||
            (force_untper == 0 && odd_piece_carton_break == BOOLEAN_TRUE))
               {
            /* First of all, if this is not the first entry, but the */
            /* current work reference is the same as the last work reference */
            /* then we have split across cartons and we need to copy off */
            /* the current work reference, change the quantities, and */
            /* update our structure with the new work reference/cmbcod */
            /* The split code needs to run when we switch a carton or */
            /* finish a carton */
            /* So the split code now exists as a sub function */
            ret_status = sSplitCartonWorkReference(whatIfMode, 
                                                         CurPck, 
                                                   FstPck, 
                                                   TmpPck, 
                                                   LstPck,
                                                   new_split_wrkref, 
                                                   new_split_cmbcod, 
                                                   valueStr,
                                                   fieldptr,
                                                   buffer,
                                                   new_pcksts);

            if (ret_status != eOK)
            {
                sFreeUpMemory((PICKS_DATA *) TopPck,
                             prtnum_list, ftpcod_list,
                             prt_client_id_list,
                             brkval_list, untqty_list);
                return ((RETURN_STRUCT *) srvResults(ret_status, NULL));
            }
           
            /* We will not now downsize here.  We will downsize everything 
             * at the end */
            
            /* Start a new carton by getting work ref, combo code, etc. */
            new_ctnnum_cnt++;
            if (whatIfMode == FALSE)
            {
                memset(new_ctnnum, 0, sizeof(new_ctnnum));
                ret_status = appNextNum(NUMCOD_CTNNUM, new_ctnnum);
                if (ret_status != eOK)
                {
                    sFreeUpMemory((PICKS_DATA *) TopPck,
                                  prtnum_list, ftpcod_list,
                                  prt_client_id_list,
                                  brkval_list, untqty_list);
                    return ((RETURN_STRUCT *) srvResults(ret_status, NULL));
                }

                memset(new_wrkref, 0, sizeof(new_wrkref));
                ret_status = appNextNum(NUMCOD_WRKREF, new_wrkref);
                if (ret_status != eOK)
                {
                    sFreeUpMemory((PICKS_DATA *) TopPck,
                                  prtnum_list, ftpcod_list,
                                  prt_client_id_list,
                                  brkval_list, untqty_list);
                    return ((RETURN_STRUCT *) srvResults(ret_status, NULL));
                }

                memset(new_cmbcod, 0, sizeof(new_cmbcod));
                ret_status = appNextNum(NUMCOD_CMBCOD, new_cmbcod);
                if (ret_status != eOK)
                {
                    sFreeUpMemory((PICKS_DATA *) TopPck,
                                  prtnum_list, ftpcod_list,
                                  prt_client_id_list,
                                  brkval_list, untqty_list);
                    return ((RETURN_STRUCT *) srvResults(ret_status, NULL));
                }

                /* Set the current working carton master
                 * based on this first piece */
                for (tmprow = sqlGetRow(CtnMstRes);
                     tmprow;
                     tmprow = sqlGetNextRow(tmprow))
                {
                    /* Find the first carton (largest by volume)
                     * that this piece will */
                    /* dimensionally fit in ... using all dimensions */
                    if (sValidatePieceFitsInCartonByDimension(CurPck->dtllen,
                                                              CurPck->dtlwid,
                                                              CurPck->dtlhgt,
                                                              sqlGetFloat(CtnMstRes, tmprow, "ctnlen"),
                                                              sqlGetFloat(CtnMstRes, tmprow, "ctnwid"),
                                                              sqlGetFloat(CtnMstRes, tmprow, "ctnhgt")))
                    {                            
                        if (sValidateRepackClassForNewPick(sqlGetString(CtnMstRes,
                                                           tmprow,"ctncod"),
                                                           CurPck->rpkcls,
                                                           CtnMstRes) == BOOLEAN_TRUE)
                        {   
                            break;
                        }
                    }
                }

                /* We could not find anything, so simply
                 * use the largest carton values */
                /* We could not have anything to fit
                 * so let us grab the biggest one that */
                /* matches our repack class */
                if (tmprow == NULL)
                {
                    for (tmprow = sqlGetRow(CtnMstRes); tmprow; tmprow = sqlGetNextRow(tmprow))
                    {
                        /*Lets find the first one that match our repack class */
                        if (strncmp(CurPck->rpkcls, sqlGetString(CtnMstRes, tmprow,"rpkcls"),
                                       RPKCLS_LEN) == 0 )   
                            break;
                    }
                    /* If we can't find anything that matches,
                     * we will have to use the biggest carton
                     * We will want to break on this carton,
                     * because we don't want other things with it
                     * */
                    if (tmprow == NULL)
                    {
                        memset(working_max_ctncod, 0, sizeof(working_max_ctncod));
                        strncpy(working_max_ctncod, max_ctncod,CTNCOD_LEN);
                        working_max_ctnvol = max_ctnvol;
                        working_max_ctnwlm = max_ctnwlm;
                            odd_piece_carton_break = BOOLEAN_TRUE;
                                
                        /* If a piece does not fit in any carton,
                         * we will set the length, height, and 
                         * width to 0 to avoid mixing new things
                         * in the carton
                         * */
                        working_max_ctnlen = 0;
                        working_max_ctnhgt = 0;
                        working_max_ctnwid = 0;
    
                    }
                } 
                if (tmprow != NULL)
                {
                    memset(working_max_ctncod, 0, sizeof(working_max_ctncod));
                    strncpy(working_max_ctncod, 
                            sqlGetString(CtnMstRes,tmprow,"ctncod"),
                            misTrimLen(sqlGetString(CtnMstRes, tmprow, "ctncod"),CTNCOD_LEN));
                    max_ctnvol = (sqlGetFloat(CtnMstRes, tmprow, "ctnvol"));
                    max_ctnwlm = (sqlGetFloat(CtnMstRes, tmprow, "ctnwlm"));
                    working_max_ctnvol = max_ctnvol;
                    working_max_ctnwlm = max_ctnwlm;
                    working_max_ctnlen = 
                                (sqlGetFloat(CtnMstRes, tmprow, "ctnlen"));
                    working_max_ctnhgt = 
                                (sqlGetFloat(CtnMstRes, tmprow, "ctnhgt"));
                    working_max_ctnwid = 
                                (sqlGetFloat(CtnMstRes, tmprow, "ctnwid"));
                 
                }
                misTrc(T_FLOW, 
                   "Using ctncod '%s' for carton '%s' based "
                   "on the first piece dimensions ...",
                   working_max_ctncod, new_ctnnum);
                /*Create a new carton structure to store our carton info */
                tempCarton = (CARTON_DATA *) calloc(1, sizeof(CARTON_DATA));
                memset(tempCarton->ctncod,0,sizeof(tempCarton->ctncod));
                memset(tempCarton->ctnnum,0,sizeof(tempCarton->ctnnum));
                memset(tempCarton->wrkref,0,sizeof(tempCarton->wrkref));
                memset(tempCarton->brkval,0,sizeof(tempCarton->brkval));
                strncpy(tempCarton->ctncod, working_max_ctncod, CTNCOD_LEN);
                strncpy(tempCarton->ctnnum, new_ctnnum, CTNNUM_LEN);
                strncpy(tempCarton->wrkref, new_wrkref, WRKREF_LEN);
                strncpy(tempCarton->brkval, CurPck->brkval,OBJECT_ID_LEN+RTSTR1_LEN);
                tempCarton->ctnvol=working_max_ctnvol;
                tempCarton->ctnwlm=working_max_ctnwlm;
                tempCarton->ctnhgt=working_max_ctnhgt;
                tempCarton->ctnlen=working_max_ctnlen;
                tempCarton->ctnwid=working_max_ctnwid;
                tempCarton->curvol = 0;
                tempCarton->curwgt = 0;
                CurPck->isFirst = BOOLEAN_TRUE;  
                sprintf(valueStr,
                        "'%s','%s','%s',"
                        "'1','0',"
                        "'%s','%s','%s','%s',"
                        "'1','1',"
                        "%ld, %ld, %ld,"
                        "%ld, %ld, %ld,"
                        "%ld, %ld, %ld,"
                        "%ld, %ld,"
                        "%ld, %ld,"
                        "'%s','%s','',''",
                        new_wrkref, WRKTYP_KIT, new_pcksts,
                        PRTNUM_KIT, INVSTS_AVAILABLE, LODLVL_SUB, new_ctnnum, 
                        BOOLEAN_FALSE, BOOLEAN_FALSE, BOOLEAN_FALSE,
                        BOOLEAN_FALSE, BOOLEAN_TRUE, BOOLEAN_FALSE,
                        BOOLEAN_FALSE, BOOLEAN_FALSE, BOOLEAN_FALSE,
                        BOOLEAN_FALSE, BOOLEAN_FALSE, 
                        BOOLEAN_FALSE, BOOLEAN_FALSE, 
                /* Change for repack class.
                 * We have to use the working max carton */
                        new_cmbcod, working_max_ctncod);

                /*  TODO:  The creation of kitpicks and mixing client id's 
                 *         is an open question.  It is purposely left out here.
                */
                fieldptr = "wrkref, wrktyp, pcksts, "
                        "pckqty, appqty, "
                        "prtnum, invsts, lodlvl, subnum, "
                        "untcas, untpak, "
                        "visflg, splflg, locflg, "
                        "lodflg, subflg, dtlflg, "
                        "prtflg, orgflg, revflg, "
                        "lotflg, qtyflg, "
                        "catch_qty_flg, frsflg, "
                        "cmbcod, ctncod, ctnnum, ctnseg ";

                sprintf(buffer,
                        "select * "
                        "  from pckwrk "
                        " where wrkref = '%s'",
                        CurPck->wrkref);
                ret_status = sqlExecStr(buffer, &res);
                if (ret_status != eOK)
                {
                    sFreeUpMemory((PICKS_DATA *) TopPck,
                                  prtnum_list, ftpcod_list,
                                  prt_client_id_list,
                                  brkval_list, untqty_list);
                    sqlFreeResults(res);
                    return (srvResults(ret_status, NULL));
                }
                row = sqlGetRow(res);
                ret_status = appGenerateTableEntry("pckwrk",
                                                   res, row,
                                                   fieldptr,
                                                   valueStr);
                if (ret_status != eOK)
                {
                    sFreeUpMemory((PICKS_DATA *) TopPck,
                                  prtnum_list, ftpcod_list,
                                  prt_client_id_list,
                                  brkval_list, untqty_list);
                    sqlFreeResults(res);
                    return (srvResults(ret_status, NULL));
                }
                sqlFreeResults(res);
                
                /* And if told, create the pick move entries */
                if (cremov == BOOLEAN_TRUE)
                {
                    sprintf(buffer,
                            "insert into pckmov "
                            "      (cmbcod, seqnum, arecod, stoloc, "
                            "       rescod, arrqty, prcqty) "
                            " select '%s', seqnum, arecod, stoloc, "
                            "        rescod, arrqty, prcqty "
                            "   from pckmov "
                            "  where cmbcod = '%s' ",
                            new_cmbcod,
                            CurPck->cmbcod);
                    ret_status = sqlExecStr(buffer, NULL);
                    if (ret_status != eOK)
                    {
                        sFreeUpMemory((PICKS_DATA *) TopPck,
                                      prtnum_list, ftpcod_list,
                                      prt_client_id_list,
                                      brkval_list, untqty_list);
                        return (srvResults(ret_status, NULL));
                    }
                }
            }
            else
            {
                /*what if mode*/ 
                memset(new_ctnnum, 0, sizeof(new_ctnnum));
                sprintf(new_ctnnum,
                        "CTN%5.5ld",
                        new_ctnnum_cnt);
                memset(new_wrkref, 0, sizeof(new_wrkref));
                sprintf(new_wrkref,
                        "KIT%5.5ld",
                        new_ctnnum_cnt);
                tempCarton = (CARTON_DATA *) calloc(1, sizeof(CARTON_DATA));
                /*Find the biggest carton code that matches our repack class*/
                for (row = sqlGetRow(CtnMstRes); row; row = sqlGetNextRow(row))
                {
                    if (strcmp(CurPck->rpkcls,
                               sqlGetString(CtnMstRes, row, "rpkcls"))  == 0)
                        break;
                }
                if (row)
                {
                    strncpy(working_max_ctncod,
                            sqlGetString(CtnMstRes, row, "ctncod"),
                            misTrimLen(sqlGetString(CtnMstRes, row, "ctncod"),
                                       CTNCOD_LEN));
                }
                /* No matching repack.  Just use biggest carton */
                else
                { 
                    row = sqlGetRow(CtnMstRes);
                    strncpy(working_max_ctncod,
                            sqlGetString(CtnMstRes, row, "ctncod"),
                            misTrimLen(sqlGetString(CtnMstRes, row,"ctncod"),
                                       CTNCOD_LEN));
                }
                memset(tempCarton->ctncod,0,sizeof(tempCarton->ctncod));
                memset(tempCarton->ctnnum,0,sizeof(tempCarton->ctnnum));
                memset(tempCarton->wrkref,0,sizeof(tempCarton->wrkref));
                memset(tempCarton->brkval,0,sizeof(tempCarton->brkval));
                strncpy(tempCarton->ctncod, working_max_ctncod, CTNCOD_LEN);
                strncpy(tempCarton->ctnnum, new_ctnnum, CTNNUM_LEN);
                strncpy(tempCarton->wrkref, new_wrkref, WRKREF_LEN);
                strncpy(tempCarton->brkval, CurPck->brkval,
                                                OBJECT_ID_LEN+RTSTR1_LEN);
                tempCarton->ctnvol=sqlGetFloat(CtnMstRes, row, "ctnvol");
                tempCarton->ctnwlm=sqlGetFloat(CtnMstRes, row, "ctnwlm");
                tempCarton->ctnhgt=sqlGetFloat(CtnMstRes, row, "ctnhgt");
                tempCarton->ctnwid=sqlGetFloat(CtnMstRes, row, "ctnwid");
                tempCarton->ctnlen=sqlGetFloat(CtnMstRes, row, "ctnlen");
                tempCarton->curvol = 0;
                tempCarton->curwgt = 0;
                CurPck->isFirst = BOOLEAN_TRUE; 
            }

            

            /* Reset the counters and values */
            cur_ctn_vol = 0.0;
            cur_ctn_wgt = 0.0;
            cur_ctn_unt = 0;
        }
        /* Save off the fact that this pick is part of this carton and */
        /* increment the cur_ctn_vol and cur_ctn_wgt by what we just */
        /* tossed into the current carton */
        strncpy(CurPck->ctnnum, new_ctnnum, CTNNUM_LEN);
        
        /* If we switched our carton, we should now try to split the
         * work reference */
        if (RunSplit == BOOLEAN_TRUE)
        {
              
            ret_status = sSplitCartonWorkReference(whatIfMode, 
                                                   CurPck, 
                                                   FstPck, 
                                                   TmpPck, 
                                                   LstPck,
                                                   new_split_wrkref, 
                                                   new_split_cmbcod, 
                                                   valueStr,
                                                   fieldptr,
                                                   buffer,
                                                   new_pcksts);

            if (ret_status != eOK)
            {
                sFreeUpMemory((PICKS_DATA *) TopPck,
                               prtnum_list, ftpcod_list,
                               prt_client_id_list,
                               brkval_list, untqty_list);
                return ((RETURN_STRUCT *) srvResults(ret_status, NULL));
            }
        }
        /*If we don't have a carton then set it do the current carton*/
        if (CurPck->carton == NULL)
        {
            CurPck->carton = tempCarton;
        }
        /*this will update the volume of the carton*/
          misTrc(T_FLOW,"Last: %s Current: %s Top: %s", LstPck->wrkref,CurPck->wrkref,TopPck->wrkref);
        sValidateWeightAndVolume(&TopPck,
                                  CurPck,
                                  tempCarton,
                                  pckvol,
                                  pckwgt,
                                  BOOLEAN_TRUE);
        tempCarton->curwgt += pckwgt;
        tempCarton->curunt += untqty;
       

        cur_ctn_vol += pckvol;
        cur_ctn_wgt += pckwgt;
        cur_ctn_unt += untqty;

        /* If this is the first time we have seen this work reference, */
        /* (Remember, there may be multiples in the structure */
        /* or it is absolutely the first work reference at all ... */
        /* then we need to update the pckwrk entry to point to it */
        misTrc(T_FLOW,"Last: %s Current: %s Top: %s", LstPck->wrkref,CurPck->wrkref,TopPck->wrkref);
        if ((whatIfMode == FALSE) &&
          (strcmp(LstPck->wrkref, CurPck->wrkref) != 0 || FstPck == CurPck))
        {
            sprintf(buffer,
                    "update pckwrk "
                    "   set ctnnum = '%s', "
                    "       ctnseg = '%s'  "
                    " where wrkref = '%s' ",
                    new_ctnnum,
                    CurPck->arecod,
                    CurPck->wrkref);
            ret_status = sqlExecStr(buffer, NULL);
            if (ret_status != eOK)
            {
                sFreeUpMemory((PICKS_DATA *) TopPck,
                              prtnum_list, ftpcod_list,
                              prt_client_id_list,
                              brkval_list, untqty_list);
                misLogError("wrkref %s had an error on pckwrk update!",
                            CurPck->wrkref);
                return (srvResults(ret_status, NULL));
            }
        }

        /* Save off the last pick pointer */
        LstPck = CurPck;
    }
    /*Lets downsize all cartons at the end*/
    for(CurPck = FstPck; CurPck; CurPck = CurPck->next)
    {
        if (CurPck->isFirst == BOOLEAN_TRUE)
        {
            ret_status = sDownsizeCarton(whatIfMode,
                                             cartonize_method,
                                         cartonize_complex_iteration_limit,
                                         cartonize_complex_popup_iter_limit,
                                         cartonize_complex_popup_levels_pct,
                                         CurPck->carton->curvol, 
                                         CurPck->carton->curwgt,
                                         force_ctncod,
                                         CurPck->carton->ctnnum,
                                         CurPck->carton->wrkref,
                                         CtnMstRes, 
                                         &TopPck, 
                                         CurPck->carton);

            if (ret_status != eOK)
            {
                sFreeUpMemory((PICKS_DATA *) TopPck,
                               prtnum_list, ftpcod_list,
                               prt_client_id_list,
                               brkval_list, untqty_list);
                return ((RETURN_STRUCT *) srvResults(ret_status, NULL));
            }
        }
    }
    /*If we are in What if Mode, we will build a list of cartons to return*/
    if (whatIfMode == TRUE)
        RetCurPtr = sBuildReturnList(RetCurPtr, TopPck, whatIfMode);
    sFreeUpMemory((PICKS_DATA *) TopPck,
                  prtnum_list, ftpcod_list,
                  prt_client_id_list,
                  brkval_list, untqty_list);

    if (RetCurPtr == NULL)
        RetCurPtr = (RETURN_STRUCT *) srvResults(eOK, NULL);
    return (RetCurPtr);
}

static long sDownsizeCarton(short whatIfMode,
			    char *cartonize_method,
			    long cartonize_complex_iter_limit,
			    long cartonize_complex_popup_iter_limit,
			    double cartonize_complex_popup_levels_pct,
			    double product_volume,
			    double product_weight,
			    char *force_carton_code,
			    char *carton_number,
			    char *carton_wrkref,
			    mocaDataRes * ctnmstres,
			    PICKS_DATA ** TopPck,
                            CARTON_DATA * tempCarton)

{
    mocaDataRow *row, *found_row;
    PICKS_DATA *CurPck, *TopThisCarton;
    CTNSEGS_DATA *TopCtnSeg, *CurCtnSeg;
    char buffer[500];
    long ret_status;
    long cartonize_iteration_counter;
    long cartonize_popup_iter_counter;
    long iteration_pop_to_level;
    double largest_piece_dimension;
    double smallest_piece_volume;
    double orginal_ctnvol, orginal_ctnwlm;
    char orginal_ctncod[CTNCOD_LEN+1];
    short all_pieces_fit;
    long pieces_in_this_carton;

    memset(orginal_ctncod,0,sizeof(orginal_ctncod));
    misTrc(T_FLOW, "(intProcessCartonization) Entering sDownsizeCarton ...");
    largest_piece_dimension = 0.0;
    smallest_piece_volume = 0.0;
    pieces_in_this_carton = 0;
    TopThisCarton = NULL;

    TopThisCarton = sGatherCartonInformationForComplex(TopPck, 
		                                       &pieces_in_this_carton,
					               &largest_piece_dimension,
					               &smallest_piece_volume,
						       carton_number);

    /* If we have received a forced carton code,
     * then that is what we want to set 
    /* this carton to regardless of what will actually fit ... */
    if (strlen(force_carton_code))
    {
	found_row = NULL;
	for (row = sqlGetRow(ctnmstres); row; row = sqlGetNextRow(row))
	{
	    if (strncmp(force_carton_code,
	        sqlGetString(ctnmstres,row,"ctncod"),
	        CTNCOD_LEN) == 0)
	    {
	        found_row = row;
	        break;
	    }
	}
    }
    else
    {
        row = sqlGetRow(ctnmstres);
     
	/* So, first we want to skip past the first row (biggest carton), 
	since that is what we have already cartonized into 
	to start with ... */
	found_row = NULL;
	/*tempCarton changes its carton code as we go down the list*/
	/*save these values so if the carton does not change, it can get updated with the orgin*/
	strncpy(orginal_ctncod,tempCarton->ctncod,CTNCOD_LEN);
	orginal_ctnvol=tempCarton->ctnvol;
	orginal_ctnwlm=tempCarton->ctnwlm;

	for (row = sqlGetRow(ctnmstres); row; row = sqlGetNextRow(row)) 
	{    
     
	    strncpy(tempCarton->ctncod,sqlGetString(ctnmstres, row, "ctncod"),CTNCOD_LEN);
	    tempCarton->ctnvol=sqlGetFloat(ctnmstres, row, "ctnvol");
	    tempCarton->ctnwlm=sqlGetFloat(ctnmstres, row, "ctnwlm");

	    if (tempCarton->curvol <= sqlGetFloat(ctnmstres, row, "ctnvol") &&
				 product_weight <= sqlGetFloat(ctnmstres, row, "ctnwlm") &&                          
				(sValidateRepackClassForEntireCarton(*TopPck,
                                                                     tempCarton->ctnnum,
                                                                     ctnmstres,
                                                                     sqlGetString(ctnmstres, row, "ctncod")) == BOOLEAN_TRUE ) &&
				(sValidateNestingForEntireCarton(TopPck,
					                         tempCarton,
					                         sqlGetFloat(ctnmstres, row, "ctnvol"),
					                         sqlGetFloat(ctnmstres, row, "ctnlen"),
					                         sqlGetFloat(ctnmstres, row, "ctnwid"),
					                         sqlGetFloat(ctnmstres, row, "ctnhgt"),
					                         BOOLEAN_FALSE) == BOOLEAN_TRUE ) &&  
				(largest_piece_dimension <= 
				sqlGetFloat(ctnmstres, row, "ctnlen")||
				largest_piece_dimension <= 
				sqlGetFloat(ctnmstres, row, "ctnwid")||
				largest_piece_dimension <= 
				sqlGetFloat(ctnmstres, row, "ctnhgt")))
	    {
       
	        all_pieces_fit = TRUE;
	        for (CurPck = *TopPck; CurPck; CurPck = CurPck->next)
	        {
		    if (strncmp(CurPck->ctnnum, carton_number, CTNNUM_LEN) == 0  && CurPck->isNesting == BOOLEAN_FALSE)
		    {
		        if (sValidatePieceFitsInCartonByDimension(CurPck->dtllen,
							          CurPck->dtlwid,
								  CurPck->dtlhgt,
							          sqlGetFloat(ctnmstres, row, "ctnlen"),
								  sqlGetFloat(ctnmstres, row, "ctnwid"),
						                  sqlGetFloat(ctnmstres, row, "ctnhgt")))
			    continue;
		         all_pieces_fit = FALSE;
			 break;
		    }
		}
		
		/* At this point, if all pieces fit ... according to our 
	        simple rules and volume/weight restrictions, then see 
		if we are set to perform complex cartonization ... 
		if so, and we have more than one piece in this carton 
		to deal with ... set up the information structures and
		go for it ... */
		if (all_pieces_fit == TRUE)
		{
		    /*Update the nesting Class to Have Actuals*/
		    sValidateNestingForEntireCarton(TopPck,
						    tempCarton,
					            sqlGetFloat(ctnmstres, row, "ctnvol"),
						    sqlGetFloat(ctnmstres, row, "ctnlen"),
		                                    sqlGetFloat(ctnmstres, row, "ctnwid"),
			                            sqlGetFloat(ctnmstres, row, "ctnhgt"),
				                    BOOLEAN_TRUE);
		    TopThisCarton = NULL;
		    pieces_in_this_carton = 0;
		    smallest_piece_volume = 0;
		    TopThisCarton = sGatherCartonInformationForComplex(TopPck, 
								       &pieces_in_this_carton,
								       &largest_piece_dimension,
								       &smallest_piece_volume,
								       carton_number);
					
		}

		if (all_pieces_fit == TRUE && pieces_in_this_carton > 1 &&
		   (strcmp(cartonize_method, CTN_METHOD_COMPLEX)==0))
		{    
                  
		    TopCtnSeg = NULL;
	 	    TopCtnSeg = 
			    (CTNSEGS_DATA *) calloc(1, sizeof(CTNSEGS_DATA));
		    if (TopCtnSeg == NULL)
		    {
		        return (eNO_MEMORY);
		    }
		    cartonize_iteration_counter = 0;
		    cartonize_popup_iter_counter = 0;
		    CurCtnSeg = TopCtnSeg;
		    memset(CurCtnSeg, 0, sizeof(CTNSEGS_DATA));
		    CurCtnSeg->ctnlen = sqlGetFloat(ctnmstres, row, "ctnlen");
		    CurCtnSeg->ctnwid = sqlGetFloat(ctnmstres, row, "ctnwid");
		    CurCtnSeg->ctnhgt = sqlGetFloat(ctnmstres, row, "ctnhgt");
		    
		    misTrc(T_FLOW, "(intProcessCartonization) Policies dictate "
		     	           "that we perform complex cartonization for %s "
			           "... ",
				   sqlGetString(ctnmstres, row, "ctncod"));
				    iteration_pop_to_level = 0;
		    ret_status = sPerformComplexCartonization((long) 1,
							      &iteration_pop_to_level,
							      cartonize_complex_iter_limit,
							      cartonize_complex_popup_iter_limit,
							      cartonize_complex_popup_levels_pct,
							      &cartonize_iteration_counter,
							      &cartonize_popup_iter_counter,
							      carton_number,
							      smallest_piece_volume,
							      (PICKS_DATA *)TopThisCarton,
							      (CTNSEGS_DATA *)TopCtnSeg);
		    
		    free(TopCtnSeg);
		    if (ret_status != eOK)	
                        all_pieces_fit = FALSE;
	        }
		
	        if (all_pieces_fit == TRUE)
	        {
	            found_row = row;   
	        }
	    }
	    /* When we get to the point that the volume exceeds our cartons */
	    /* break out because we ordered the select by volume descending */
	    /* We break out because we ordered by volume and repack class */
	    else if ((tempCarton->curvol > sqlGetFloat(ctnmstres, row, "ctnvol")))
	    {
	        break;
	    }
        }
    }
    if (found_row != NULL)
    {
        if (strlen(force_carton_code))
	{
            misTrc(T_FLOW, 
		   "Forcing carton '%s' to ctncod %s based on parameter ...",
		   carton_wrkref,
		   sqlGetString(ctnmstres, found_row, "ctncod"));
	}
	else
	{
	    misTrc(T_FLOW,"Downsizing carton '%s' to carton code %s ...",
	                   carton_wrkref,
		           sqlGetString(ctnmstres, found_row, "ctncod"));
	    misTrc(T_FLOW,"           ctnvol: %.3f, ctnwlm: %.3f ",
		           sqlGetFloat(ctnmstres, found_row, "ctnvol"),
		           sqlGetFloat(ctnmstres, found_row, "ctnwlm"));
	    misTrc(T_FLOW,"           ctnlen: %.3f, ctnwid: %.3f, ctnhgt: %.3f",
		           sqlGetFloat(ctnmstres, found_row, "ctnlen"),
		           sqlGetFloat(ctnmstres, found_row, "ctnwid"),
		           sqlGetFloat(ctnmstres, found_row, "ctnhgt"));
	}
	if (whatIfMode == FALSE)
	{
	    /* Since we found a smaller carton, update the carton to use it */
            sprintf(buffer,
		    "update pckwrk "
		    "   set ctncod = '%s' "
		    " where wrkref = '%s' ",
		    misTrim(sqlGetString(ctnmstres, found_row, "ctncod")),
		    carton_wrkref);
	    ret_status = sqlExecStr(buffer, NULL);
	    if (ret_status != eOK)
	    {
	        misLogError("wrkref %s had an error %ld downsizing carton ",
			    carton_wrkref, ret_status);
		return (ret_status);
	    }
            strncpy(tempCarton->ctncod,
                    sqlGetString(ctnmstres, found_row, "ctncod"),
                    CTNCOD_LEN);
            tempCarton->ctnvol=sqlGetFloat(ctnmstres, found_row, "ctnvol");
            tempCarton->ctnwlm=sqlGetFloat(ctnmstres, found_row, "ctnwlm");
	}
	else
	{
	    strncpy(tempCarton->ctncod,
                    sqlGetString(ctnmstres, found_row, "ctncod"),
                    CTNCOD_LEN);
	    tempCarton->ctnvol=sqlGetFloat(ctnmstres, found_row, "ctnvol");
	    tempCarton->ctnwlm=sqlGetFloat(ctnmstres, found_row, "ctnwlm"); 
	}
    }
    else
    {
        strncpy(tempCarton->ctncod,orginal_ctncod,CTNCOD_LEN);
        tempCarton->ctnvol = orginal_ctnvol;
        tempCarton->ctnwlm = orginal_ctnwlm;

	row = sqlGetRow(ctnmstres);
        misTrc(T_FLOW, "Leaving carton '%s' with current carton code '%s'",
                        carton_wrkref, tempCarton->ctncod);
	misTrc(T_FLOW, "        ctnvol: %.3f, ctnwlm: %.3f ",
		    sqlGetFloat(ctnmstres, row, "ctnvol"),
		    sqlGetFloat(ctnmstres, row, "ctnwlm"));
	misTrc(T_FLOW, "        ctnlen: %.3f, ctnwid: %.3f, ctnhgt: %.3f ",
		    sqlGetFloat(ctnmstres, row, "ctnlen"),
		    sqlGetFloat(ctnmstres, row, "ctnwid"),
		    sqlGetFloat(ctnmstres, row, "ctnhgt"));
    }

    /* Finally, if we are in what if mode, let's give some more data */
    if (whatIfMode == TRUE)
    {
        misTrc(T_FLOW, "***** Carton %s Summary *****", carton_number);
	for (CurPck = *TopPck; CurPck; CurPck = CurPck->next)
	{
	    if (strncmp(CurPck->ctnnum, carton_number, CTNNUM_LEN) == 0)
	    {
	        misTrc(T_FLOW, "     Prt: %s, Ftp: %s, Pck: %ld Clnt: %s ",
		               CurPck->prtnum, CurPck->ftpcod, CurPck->pckqty,
			       CurPck->prt_client_id);
		misTrc(T_FLOW, "          Vol: %.3f, Wgt: %.3f ",
			       CurPck->dtlvol, CurPck->dtlwgt);
		misTrc(T_FLOW, "          Len: %.3f, Hgt: %.3f, Wid: %.3f ",
		               CurPck->dtllen, CurPck->dtlhgt, CurPck->dtlwid);
		if (CurPck->untlen > 0 || CurPck->paklen > 0)
		    misTrc(T_FLOW, "          UntLen: %.3f, PakLen: %.3f, "
									"UntPak: %ld",
		CurPck->untlen, CurPck->paklen, CurPck->untpak);
	     }
        }
    }
    return (eOK);
}

static long sPerformComplexCartonization(
                                long iteration_level,
                                long *iteration_pop_to_level,
                                long cartonize_complex_iter_limit,
                                long cartonize_complex_popup_iter_limit,
                                double cartonize_complex_popup_levels_pct,
                                long *cartonize_iteration_counter,
                                long *cartonize_popup_iter_counter,
                                char *carton_number,
                                double smallest_piece_volume,
                                PICKS_DATA * PiecePtr,
                                CTNSEGS_DATA * TopCtnSeg)
{
    long ret_status;
    short orientation_max;
    short orientation_loop;
    short cut_orientation_max;
    short cut_orientation_loop;
    short cut_remnants_loop;
    CTNSEGS_DATA *CurCtnSeg, *TmpCurCtn, *NewTopCtn, *NewCurCtn, *NewLstCtn;
    PICKS_DATA * org_PiecePtr = NULL;
    double tmp_dtllen;
    double tmp_dtlwid;
    double tmp_dtlhgt;
    double tmp_ctnlen;
    double tmp_ctnwid;
    double tmp_ctnhgt;

    ret_status = eERROR;

    *cartonize_iteration_counter = *cartonize_iteration_counter + 1;
    *cartonize_popup_iter_counter = *cartonize_popup_iter_counter + 1;
   
    if (iteration_level < 3)
    {
        misTrc(T_FLOW, "(intProcessCartonization) Entering complex routine "
                       "for iter level %ld, limit %ld, count %ld, pop %ld ...",
                        iteration_level,
                        cartonize_complex_iter_limit,
                        *cartonize_iteration_counter,
                        *cartonize_popup_iter_counter);
        misTrc(T_FLOW, "     Piece Ptr %x, len %.3f, wid %.3f, hgt %.3f, wrkref %s ",
                       PiecePtr,
                       PiecePtr->dtllen, PiecePtr->dtlwid, PiecePtr->dtlhgt, PiecePtr->wrkref);
        for (TmpCurCtn = TopCtnSeg; TmpCurCtn; TmpCurCtn = TmpCurCtn->next)
        {
            misTrc(T_FLOW, "     Carton Ptr %x, len %.3f, wid %.3f, hgt %.3f ",
                           TmpCurCtn,
                           TmpCurCtn->ctnlen,
                           TmpCurCtn->ctnwid,
                           TmpCurCtn->ctnhgt);
        }
    }
    org_PiecePtr = PiecePtr;
    /*The pieces may be out of order , so we skip past some cartons here*/
    /*We also do not want to look at virtual pieces*/
    /*virtual pieces are pieces that are inside of a nested object*/

    while(PiecePtr->next && ((strncmp(PiecePtr->next->ctnnum,
                            carton_number,CTNNUM_LEN)!=0) || (PiecePtr->next->isVirtual == BOOLEAN_TRUE)))
    {
        PiecePtr=PiecePtr->next;
    }
                
               
    /* First, loop through all of the segments that I have ... if the piece */
    /* won't even fit in by volume, then don't waste any time rotation the */
    /* orientation of the piece ... */
    for (CurCtnSeg = TopCtnSeg; CurCtnSeg &&
            *cartonize_iteration_counter <
                                cartonize_complex_iter_limit &&
            *cartonize_popup_iter_counter <
                                cartonize_complex_popup_iter_limit &&
            ret_status != eOK &&
            ret_status != eNO_MEMORY;
         CurCtnSeg = CurCtnSeg->next)
    {
        if (org_PiecePtr->dtllen * org_PiecePtr->dtlwid * org_PiecePtr->dtlhgt >
            CurCtnSeg->ctnlen * CurCtnSeg->ctnwid * CurCtnSeg->ctnhgt)
            continue;

        /* If our piece is a cube, there is no point in rotating at all ... */
        /* else, we need to loop through because we may try 1,2,3,4,5,6 or */
        /* 1,3,5 or 1,2,4,6 etc. */
        if (org_PiecePtr->dtllen == org_PiecePtr->dtlwid &&
            org_PiecePtr->dtllen == org_PiecePtr->dtlhgt)
            orientation_max = 1;
        else
            orientation_max = 6;
        for (orientation_loop = 1;
             orientation_loop <= orientation_max &&
                    *cartonize_iteration_counter <
                                        cartonize_complex_iter_limit &&
                    *cartonize_popup_iter_counter <
                                        cartonize_complex_popup_iter_limit &&
                    ret_status != eOK &&
                    ret_status != eNO_MEMORY;
             orientation_loop ++)
        {
            /* Skip to the bottom of the loop if there is no point to */
            /* checking the current rotation of the piece ... otherwise, */
            /* get the length width, and height of the piece based on that */
            /* current orientation */
            switch (orientation_loop)
            {
                case 1:
                    tmp_dtllen = org_PiecePtr->dtllen;
                    tmp_dtlwid = org_PiecePtr->dtlwid;
                    tmp_dtlhgt = org_PiecePtr->dtlhgt;
                    break;
                    case 2:
                    if (org_PiecePtr->dtllen == org_PiecePtr->dtlwid)        continue;
                    tmp_dtllen = org_PiecePtr->dtlwid;
                    tmp_dtlwid = org_PiecePtr->dtllen;
                    tmp_dtlhgt = org_PiecePtr->dtlhgt;
                    break;
                case 3:
                    if (org_PiecePtr->dtlhgt == org_PiecePtr->dtlwid)        continue;
                    tmp_dtllen = org_PiecePtr->dtllen;
                    tmp_dtlwid = org_PiecePtr->dtlhgt;
                    tmp_dtlhgt = org_PiecePtr->dtlwid;
                    break;
                case 4:
                    if (org_PiecePtr->dtlhgt == org_PiecePtr->dtllen)        continue;
                    tmp_dtllen = org_PiecePtr->dtlhgt;
                    tmp_dtlwid = org_PiecePtr->dtllen;
                    tmp_dtlhgt = org_PiecePtr->dtlwid;
                    break;
                case 5:
                    if (org_PiecePtr->dtlhgt == org_PiecePtr->dtllen)        continue;
                    tmp_dtllen = org_PiecePtr->dtlhgt;
                    tmp_dtlwid = org_PiecePtr->dtlwid;
                    tmp_dtlhgt = org_PiecePtr->dtllen;
                    break;
                case 6:
                    if (org_PiecePtr->dtlhgt == org_PiecePtr->dtlwid)        continue;
                    tmp_dtllen = org_PiecePtr->dtlwid;
                    tmp_dtlwid = org_PiecePtr->dtlhgt;
                    tmp_dtlhgt = org_PiecePtr->dtllen;
                    break;
            }

            if (tmp_dtllen <= CurCtnSeg->ctnlen &&
                tmp_dtlwid <= CurCtnSeg->ctnwid &&
                tmp_dtlhgt <= CurCtnSeg->ctnhgt)
            {
                /* If there are more pieces for this carton ... we need to */
                /* create a next carton structure, slicing up the remnants */
                /* of the carton in each orientation and continue with the */
                /* next piece ... */

                
                if (PiecePtr->next &&
                    strncmp(PiecePtr->next->ctnnum,
                            carton_number,CTNNUM_LEN)==0)
                {
                    if (tmp_dtllen == tmp_dtlwid &&
                        tmp_dtllen == tmp_dtlhgt &&
                        CurCtnSeg->ctnlen == CurCtnSeg->ctnwid &&
                        CurCtnSeg->ctnlen == CurCtnSeg->ctnhgt)
                        cut_orientation_max = 1;
                    else
                        cut_orientation_max = 6;

                    for (cut_orientation_loop = 1;
                         cut_orientation_loop <= cut_orientation_max &&
                                ret_status != eOK &&
                                ret_status != eNO_MEMORY;
                         cut_orientation_loop ++)
                    {
                        NewTopCtn = NewCurCtn = NewLstCtn = NULL;
                        for (TmpCurCtn = TopCtnSeg; TmpCurCtn;
                             TmpCurCtn = TmpCurCtn->next)
                        {
                            for (cut_remnants_loop = 1;
                                 cut_remnants_loop <= 3;
                                 cut_remnants_loop ++)
                            {
                                tmp_ctnlen = tmp_ctnwid = tmp_ctnhgt = 0.0;
                                if (TmpCurCtn == CurCtnSeg)
                                {
                                    switch (cut_orientation_loop)
                                    {
                                        case 1:
                                            switch (cut_remnants_loop)
                                            {
                                                case 1:
                                                    tmp_ctnlen =
                                                        CurCtnSeg->ctnlen;
                                                    tmp_ctnwid =
                                                        CurCtnSeg->ctnwid;
                                                    tmp_ctnhgt =
                                                        CurCtnSeg->ctnhgt -
                                                        tmp_dtlhgt;
                                                    break;
                                                case 2:
                                                    tmp_ctnlen =
                                                        CurCtnSeg->ctnlen;
                                                    tmp_ctnwid =
                                                        CurCtnSeg->ctnwid -
                                                        tmp_dtlwid;
                                                    tmp_ctnhgt = tmp_dtlhgt;
                                                    break;
                                                case 3:
                                                    tmp_ctnlen =
                                                        CurCtnSeg->ctnlen -
                                                        tmp_dtllen;
                                                    tmp_ctnwid = tmp_dtlwid;
                                                    tmp_ctnhgt = tmp_dtlhgt;
                                                    break;
                                            }
                                            break;
                                        case 2:
                                            switch (cut_remnants_loop)
                                            {
                                                case 1:
                                                    tmp_ctnlen =
                                                        CurCtnSeg->ctnlen;
                                                    tmp_ctnwid =
                                                        CurCtnSeg->ctnwid;
                                                    tmp_ctnhgt =
                                                        CurCtnSeg->ctnhgt -
                                                        tmp_dtlhgt;
                                                    break;
                                                case 2:
                                                    tmp_ctnlen =
                                                        CurCtnSeg->ctnlen -
                                                        tmp_dtllen;
                                                    tmp_ctnwid =
                                                        CurCtnSeg->ctnwid;
                                                    tmp_ctnhgt = tmp_dtlhgt;
                                                    break;
                                                case 3:
                                                    tmp_ctnlen = tmp_dtllen;
                                                    tmp_ctnwid =
                                                        CurCtnSeg->ctnwid -
                                                        tmp_dtlwid;
                                                    tmp_ctnhgt = tmp_dtlhgt;
                                                    break;
                                            }
                                            break;
                                        case 3:
                                            switch (cut_remnants_loop)
                                            {
                                                case 1:
                                                    tmp_ctnlen =
                                                        CurCtnSeg->ctnlen;
                                                    tmp_ctnwid =
                                                        CurCtnSeg->ctnwid -
                                                        tmp_dtlwid;
                                                    tmp_ctnhgt =
                                                           CurCtnSeg->ctnhgt;
                                                    break;
                                                case 2:
                                                    tmp_ctnlen =
                                                        CurCtnSeg->ctnlen;
                                                    tmp_ctnwid = tmp_dtlwid;
                                                    tmp_ctnhgt =
                                                        CurCtnSeg->ctnhgt -
                                                        tmp_dtlhgt;
                                                    break;
                                                case 3:
                                                    tmp_ctnlen =
                                                        CurCtnSeg->ctnlen -
                                                        tmp_dtllen;
                                                    tmp_ctnwid = tmp_dtlwid;
                                                    tmp_ctnhgt = tmp_dtlhgt;
                                                    break;
                                            }
                                            break;
                                        case 4:
                                            switch (cut_remnants_loop)
                                            {
                                                case 1:
                                                    tmp_ctnlen =
                                                        CurCtnSeg->ctnlen;
                                                    tmp_ctnwid =
                                                        CurCtnSeg->ctnwid -
                                                        tmp_dtlwid;
                                                    tmp_ctnhgt =
                                                        CurCtnSeg->ctnhgt;
                                                    break;
                                                case 2:
                                                    tmp_ctnlen =
                                                        CurCtnSeg->ctnlen -
                                                        tmp_dtllen;
                                                    tmp_ctnwid = tmp_dtlwid;
                                                    tmp_ctnhgt =
                                                        CurCtnSeg->ctnhgt;
                                                    break;
                                                case 3:
                                                    tmp_ctnlen = tmp_dtllen;
                                                    tmp_ctnwid = tmp_dtlwid;
                                                    tmp_ctnhgt =
                                                        CurCtnSeg->ctnhgt -
                                                        tmp_dtlhgt;
                                                    break;
                                            }
                                            break;
                                        case 5:
                                            switch (cut_remnants_loop)
                                            {
                                                case 1:
                                                    tmp_ctnlen =
                                                        CurCtnSeg->ctnlen -
                                                        tmp_dtllen;
                                                    tmp_ctnwid =
                                                        CurCtnSeg->ctnwid;
                                                    tmp_ctnhgt =
                                                        CurCtnSeg->ctnhgt;
                                                    break;
                                                case 2:
                                                    tmp_ctnlen = tmp_dtllen;
                                                    tmp_ctnwid =
                                                        CurCtnSeg->ctnwid;
                                                    tmp_ctnhgt =
                                                        CurCtnSeg->ctnhgt -
                                                        tmp_dtlhgt;
                                                    break;
                                                case 3:
                                                    tmp_ctnlen = tmp_dtllen;
                                                    tmp_ctnwid =
                                                        CurCtnSeg->ctnwid -
                                                        tmp_dtlwid;
                                                    tmp_ctnhgt = tmp_dtlhgt;
                                                    break;
                                            }
                                            break;
                                        case 6:
                                            switch (cut_remnants_loop)
                                            {
                                                case 1:
                                                    tmp_ctnlen =
                                                        CurCtnSeg->ctnlen -
                                                        tmp_dtllen;
                                                    tmp_ctnwid =
                                                        CurCtnSeg->ctnwid;
                                                    tmp_ctnhgt =
                                                        CurCtnSeg->ctnhgt;
                                                    break;
                                                case 2:
                                                    tmp_ctnlen = tmp_dtllen;
                                                    tmp_ctnwid =
                                                        CurCtnSeg->ctnwid -
                                                        tmp_dtlwid;
                                                    tmp_ctnhgt =
                                                        CurCtnSeg->ctnhgt;
                                                    break;
                                                case 3:
                                                    tmp_ctnlen = tmp_dtllen;
                                                    tmp_ctnwid = tmp_dtlwid;
                                                    tmp_ctnhgt =
                                                        CurCtnSeg->ctnhgt -
                                                        tmp_dtlhgt;
                                                    break;
                                            }
                                            break;
                                    }
                                }
                                else
                                {
                                    if (cut_remnants_loop == 1)
                                    {
                                        tmp_ctnlen = TmpCurCtn->ctnlen;
                                        tmp_ctnwid = TmpCurCtn->ctnwid;
                                        tmp_ctnhgt = TmpCurCtn->ctnhgt;
                                    }
                                    else
                                    {
                                        continue;
                                    }
                                }
                                if (tmp_ctnlen > 0.0 &&
                                    tmp_ctnwid > 0.0 && tmp_ctnhgt > 0.0 &&
                                    (tmp_ctnlen * tmp_ctnwid * tmp_ctnhgt) >
                                    smallest_piece_volume)
                                {
                                    if (NewTopCtn == NULL)
                                    {
                                        NewTopCtn = (CTNSEGS_DATA *)
                                            calloc(1, sizeof(CTNSEGS_DATA));
                                        if (NewTopCtn == NULL)
                                        {
                                            return (eNO_MEMORY);
                                        }
                                        NewCurCtn = NewTopCtn;
                                        memset(NewCurCtn, 0,
                                               sizeof(CTNSEGS_DATA));
                                    }
                                    else
                                    {
                                        NewCurCtn = (CTNSEGS_DATA *)
                                            calloc(1, sizeof(CTNSEGS_DATA));
                                        if (NewCurCtn == NULL)
                                        {
                                            return (eNO_MEMORY);
                                        }
                                        memset(NewCurCtn, 0,
                                               sizeof(CTNSEGS_DATA));
                                        NewCurCtn->prev = NewLstCtn;
                                        NewLstCtn->next = NewCurCtn;
                                    }
                                    NewLstCtn = NewCurCtn;

                                    NewCurCtn->ctnlen = tmp_ctnlen;
                                    NewCurCtn->ctnwid = tmp_ctnwid;
                                    NewCurCtn->ctnhgt = tmp_ctnhgt;
                                }
                            }
                        }
                        if (NewTopCtn != NULL)
                        {
                            ret_status = sPerformComplexCartonization(
                                            iteration_level + 1,
                                            iteration_pop_to_level,
                                            cartonize_complex_iter_limit,
                                            cartonize_complex_popup_iter_limit,
                                            cartonize_complex_popup_levels_pct,
                                            cartonize_iteration_counter,
                                            cartonize_popup_iter_counter,
                                            carton_number,
                                            smallest_piece_volume,
                                            (PICKS_DATA *)PiecePtr->next,
                                            (CTNSEGS_DATA *)NewTopCtn);

                            /* Free up our carton segment list we just made */
                            NewLstCtn = NULL;
                            for (NewCurCtn = NewTopCtn; NewCurCtn;
                                 NewCurCtn = NewCurCtn->next)
                            {
                                if (NewLstCtn)
                                    free(NewLstCtn);
                                NewLstCtn = NewCurCtn;
                            }
                            if (NewLstCtn)
                                free(NewLstCtn);
                        }
                    }
                }
                /* Otherwise, if no pieces remain, we have found that */
                /* everything fits ... so return eOK ... */
                else
                {
                    misTrc(T_FLOW, "(intProcessCartonization) Setting status "
                                   "to eOK ... everything fits, "
                                   "iteration was %ld!",
                                   iteration_level);
                    ret_status = eOK;
                }
            }
        }
    }

    /* If we have exceeded our popup iteration limit, then we either need */
    /* to set the level we wish to pop to, or be on the lookup for hitting */
    /* that level ... */
    if (*cartonize_popup_iter_counter >= cartonize_complex_popup_iter_limit)
    {
        if (*iteration_pop_to_level == 0)
        {
            *iteration_pop_to_level = (long)(
                (double)iteration_level -
                (double)iteration_level *
                    (cartonize_complex_popup_levels_pct / 100.0));
            if (*iteration_pop_to_level < 1)
                *iteration_pop_to_level = 1;

            misTrc(T_FLOW, "(intProcessCartonization) Setting POP TO "
                           "LEVEL to %ld, popup iter %ld, cur lvl %ld, "
                           "pct %.3f!",
                           *iteration_pop_to_level,
                           *cartonize_popup_iter_counter,
                           iteration_level,
                           cartonize_complex_popup_levels_pct);
        }
        else
        {
            if (iteration_level <= *iteration_pop_to_level + 1)
            {
                misTrc(T_FLOW, "(intProcessCartonization) POPPED UP TO "
                               "LEVEL %ld!",
                               *iteration_pop_to_level);
                *cartonize_popup_iter_counter = 0;
                *iteration_pop_to_level = 0;
            }
        }
    }

    return(ret_status);
}
static void sFreeCarton(PICKS_DATA * LstPtr)
{
     NESTING_DATA * NstCls = NULL;
     NESTING_DATA * LstNst = NULL;

    /*Only free the first the carton for the first pick*/  
    if ((LstPtr->isFirst == BOOLEAN_TRUE) && (LstPtr->carton))
    {
        /*need to free Nesting Classes also*/
        for (NstCls = LstPtr->carton->nestingList; NstCls; NstCls = NstCls->next)
        {
            if (LstNst)
                free(LstNst);
            LstNst = NstCls;
                  
        }
        free(LstPtr->carton);
              
        if (LstNst)
            free(LstNst);           
    }
}
static void sFreeUpMemory(PICKS_DATA * TopPtr,
                          char *prtnum_list, char *ftpcod_list,
                          char *prt_client_id_list,
                          char *brkval_list, char *untqty_list)
{
    PICKS_DATA *LstPtr;
    PICKS_DATA *TmpPtr;

    /* Free up our working list */
    LstPtr = NULL;
    for (TmpPtr = TopPtr; TmpPtr; TmpPtr = TmpPtr->next)
    {
        if (LstPtr)
        { 
            sFreeCarton(LstPtr);
            free(LstPtr);
        }
        LstPtr = TmpPtr;
    }
    if (LstPtr)
    {   
        sFreeCarton(LstPtr);
        free(LstPtr);
    }
    /* Free up the memory we allocated */
    if (prtnum_list)
        free(prtnum_list);
    if (ftpcod_list)
        free(ftpcod_list);
    if (prt_client_id_list)
        free(prt_client_id_list);
    if (brkval_list)
        free(brkval_list);
    if (untqty_list)
        free(untqty_list);

}
/* This function returns: 
 *      TRUE if the repack class fits with the carton code
 *      FALSE if the repack class cannot be used with the carton code
 * */
static moca_bool_t sValidateRepackClassForNewPick(char * ctncod, 
                                                  char * rpkcls, 
                                                  mocaDataRes *CtnMstRes)
   
{
    mocaDataRow *row = NULL;
    for (row = sqlGetRow(CtnMstRes); row; row = sqlGetNextRow(row))
    {
        if((strcmp(rpkcls,sqlGetString(CtnMstRes, row, "rpkcls"))  == 0)  && 
           (strcmp(ctncod,sqlGetString(CtnMstRes, row, "ctncod"))  == 0))
            return BOOLEAN_TRUE;
    }
    return BOOLEAN_FALSE;
}

/*This function returns a carton with the most remaining volume */
/* that matches the current picks rpkcls and break value */
static CARTON_DATA * sFindCartonWithMostRemainingVolume(PICKS_DATA * match_pick,
                                                        PICKS_DATA * * TopPckPtr,
                                                        mocaDataRes * CtnMstRes,
                                                        double pckvol,
                                                        double pckwgt)
{
    PICKS_DATA * CurPck = NULL, * TopPtr = NULL;
    double highestVolume = 0;
    CARTON_DATA * tempCarton = NULL;
    TopPtr = *TopPckPtr;
    misTrc(T_FLOW, "(intProcessCartonization) Entering "
                   "sFindCartonWithMostRemainingVolume ... "
                   " with wrkref %s and break value %s and rpkcls %s", 
                   match_pick->wrkref, 
                   match_pick->brkval,
                   match_pick->rpkcls);

    for(CurPck = TopPtr; CurPck; CurPck = CurPck->next)
    {
       if ((strcmp(CurPck->brkval, match_pick->brkval) == 0)  &&
           (CurPck->isFirst))
           if (sValidateRepackClassForNewPick
              (CurPck->carton->ctncod,match_pick->rpkcls,CtnMstRes))
           {
               if ((sValidateWeightAndVolume(TopPckPtr,
                                             match_pick,
                                             CurPck->carton,
                                             pckvol,
                                             pckwgt,
                                             BOOLEAN_FALSE) != -1)  &&    
                   ((CurPck->carton->ctnvol - CurPck->carton->curvol)
                        > highestVolume))
                   {   
                       if (sValidatePieceFitsInCartonByDimension(CurPck->dtllen,
                                                                 CurPck->dtlwid,
                                                                 CurPck->dtlhgt,
                                                                 CurPck->carton->ctnlen,
                                                                 CurPck->carton->ctnhgt,
                                                                 CurPck->carton->ctnwid))
                       {
                           highestVolume = CurPck->carton->ctnvol - CurPck->carton->curvol;
                           tempCarton = CurPck->carton;
                       }
                   }
           }
      
    }
    if (tempCarton != NULL)
        misTrc(T_FLOW, "Departing sFindCartonWithMostRemainingVolume " 
                        " ... with wrkref %s and break value %s and "
                        "rpkcls %s and carton %s",
                        match_pick->wrkref, 
                        match_pick->brkval, 
                        match_pick->rpkcls, 
                        tempCarton->ctnnum);
    else
        misTrc(T_FLOW, "Departing sFindCartonWithMostRemainingVolume "
                       " ... with wrkref %s and break value %s and "
                       "rpkcls %s and no matching cartons",
                        match_pick->wrkref, 
                        match_pick->brkval, 
                        match_pick->rpkcls);
    return tempCarton;
}

/* This function validates the Carton Code is valid for the entire Carton
 * This function returns
 *      TRUE if the repack class is valid for the entire carton
 *      FALSE if the repack class is NOT valid for the entire carton 
 * 
 * This is used during downsizing cartons,
 * because parts of different repack classes can be mixed together,
 * however smaller cartons may be for specific repack classes
 * */


static moca_bool_t sValidateRepackClassForEntireCarton(PICKS_DATA * top_pick, 
                                                       char * ctnnum,         
                                                       mocaDataRes *CtnMstRes, 
                                                       char * ctncod)
{
    PICKS_DATA * CurPck = NULL;
    misTrc(T_FLOW, "Entering sValidateRepackClassForEntireCarton... "
                   "with carton %s and carton code %s",ctnnum,ctncod);
    for(CurPck = top_pick; CurPck; CurPck = CurPck->next)
    {
        if (strcmp(CurPck->ctnnum,ctnnum)  == 0)
            if (sValidateRepackClassForNewPick(ctncod, 
                                               CurPck->rpkcls,
                                               CtnMstRes)  == BOOLEAN_FALSE)
                 {
                     misTrc(T_FLOW, "Entering sValidateRepackClassForEntireCarton failed... ");
                     return BOOLEAN_FALSE;
                 }
    }
    misTrc(T_FLOW, "Entering sValidateRepackClassForEntireCarton succeeded.. ");
    return BOOLEAN_TRUE;
}

/* This function splits one wrkref into multiple wrkref
 * It is part of existing functionality that was moved to sub function
 * It was moved to a sub function, because it repack functionality required
 * it to be called in multiple places 
 * This function returns an error message if it errored 
 * */

static long sSplitCartonWorkReference(short whatIfMode, 
                                      PICKS_DATA * CurPck, 
                                      PICKS_DATA * TopPck,                               
                                      PICKS_DATA * TmpPck, 
                                      PICKS_DATA * LstPck, 
                                      char * new_split_wrkref,
                                      char * new_split_cmbcod, 
                                      char * valueStr,
                                      char * fieldptr, 
                                      char * buffer,
                                      char * new_pcksts)
{
    long pckwrk_pckqty_sum;
    long ret_status;
    mocaDataRow *row = NULL;
    mocaDataRes *res = NULL;

    if (whatIfMode == TRUE)
        return eOK;
    misTrc(T_FLOW, "Entering sSplitCartonWorkReference");
    if ((whatIfMode == FALSE) &&
        (CurPck != TopPck) &&
        (strcmp(CurPck->wrkref, LstPck->wrkref) == 0))
    {
        memset(new_split_wrkref, 0, sizeof(new_split_wrkref));
        ret_status = appNextNum(NUMCOD_WRKREF, new_split_wrkref);
        if (ret_status != eOK)
        {
           return ret_status;
        }

        memset(new_split_cmbcod, 0, sizeof(new_split_cmbcod));
        ret_status = appNextNum(NUMCOD_CMBCOD, new_split_cmbcod);
        if (ret_status != eOK)
        {
           return ret_status;
        }
        sprintf(valueStr,
                "'%s','%s','%s'",
                new_split_wrkref, new_pcksts, new_split_cmbcod);

        fieldptr = "wrkref, pcksts, cmbcod ";
        sprintf(buffer,
                "select * "
                "  from pckwrk "
                " where wrkref = '%s'",
                CurPck->wrkref);
        ret_status = sqlExecStr(buffer, &res);
        if (ret_status != eOK)
        {
            return ret_status;
        }
        row = sqlGetRow(res);
        ret_status = appGenerateTableEntry("pckwrk",
                                           res, row,
                                                 fieldptr,
                                           valueStr);
        if (ret_status != eOK)
        {
            return ret_status;
        }
        sqlFreeResults(res);

        sprintf(buffer,
                "insert into pckmov "
                "      (cmbcod, seqnum, arecod, stoloc, "
                "       rescod, arrqty, prcqty) "
                " select '%s', seqnum, arecod, stoloc, "
                "        rescod, arrqty, prcqty "
                "   from pckmov "
                "  where cmbcod = '%s' ",
                new_split_cmbcod,
                CurPck->cmbcod);
        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status != eOK)
        {
            return ret_status;
        }

        /* Now, we have new_split_wrkref, new_split_cmbcod ... */
        /* apply them to the structure for the work reference */
        /* from this point forward */
        pckwrk_pckqty_sum = 0;
        for (TmpPck = CurPck; TmpPck; TmpPck = TmpPck->next)
        {
            if (strncmp(TmpPck->wrkref,
                        LstPck->wrkref, WRKREF_LEN) != 0)
                break;
            strncpy(TmpPck->wrkref, new_split_wrkref, WRKREF_LEN);
            strncpy(TmpPck->cmbcod, new_split_cmbcod, CMBCOD_LEN);
            pckwrk_pckqty_sum = pckwrk_pckqty_sum + TmpPck->pckqty;
        }
        sprintf(buffer,
                "update pckwrk "
                "   set pckqty = '%ld' "
                " where appqty = '0' "
                "   and '%ld' <> '0' "
                "   and wrkref = '%s' ",
                pckwrk_pckqty_sum,
                pckwrk_pckqty_sum,
                new_split_wrkref);
        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status != eOK)
        {
            return ret_status;
        }
        /* And the old part of the work reference needs to */
        /* be totalled up and reset to that quantity */
        /* Start at the beginning and break out when we */
        /* hit the current reference number */
        pckwrk_pckqty_sum = 0;
        for (TmpPck = TopPck; TmpPck; TmpPck = TmpPck->next)
        {
            if (TmpPck == CurPck)
                break;
            if (strncmp(TmpPck->wrkref,
                        LstPck->wrkref, WRKREF_LEN) == 0)
                pckwrk_pckqty_sum = pckwrk_pckqty_sum +        TmpPck->pckqty;
                }
        sprintf(buffer,
                "update pckwrk "
                "   set pckqty = '%ld' "
                " where appqty = '0' "
                "   and '%ld' <> '0' "
                "   and wrkref = '%s' ",
                pckwrk_pckqty_sum,
                pckwrk_pckqty_sum,
                LstPck->wrkref);
        ret_status = sqlExecStr(buffer, NULL);
        if (ret_status != eOK)
        {
            return ret_status;
        }
    } /*Ends Split Carton */
    return eOK;
}

/* This function builds a return list that will get returned.
 * This is used in what if mode 
 * */
static RETURN_STRUCT * sBuildReturnList(RETURN_STRUCT * RetCurPtr,
                                        PICKS_DATA * TopPck, 
                                        short whatIfMode)
{
    long ret_status;
    char prcmod[FLAG_LEN+1];
    PICKS_DATA * CurPck = NULL;
    PICKS_DATA * TmpPck = NULL;
    char ltlcls [LTLCLS_LEN + 1];
    char stccod [STCCOD_LEN + 1];
    long parcel_flg = 0;
    long hazmat_flg = 0;
    long ltlrnk = 0;
    long stcrnk = 0;

    memset(prcmod, 0, sizeof(prcmod));
    memset(ltlcls, 0, sizeof(ltlcls));
    memset(stccod, 0, sizeof(stccod));

    misTrc(T_FLOW, "Entering sBuildReturnList ");
            
    if (whatIfMode == FALSE)
        sprintf(prcmod, "U");
    else
        sprintf(prcmod, "I");
    
    /* Check to see if we have initialized the Results Pointer. */
    if(RetCurPtr==NULL)
    {
        /* Build the record set to be returned. */
        RetCurPtr = srvResultsInit(eOK,
                       "ctnnum", COMTYP_STRING, CTNNUM_LEN,
                       "prcmod", COMTYP_STRING, FLAG_LEN,
                       "ctncod", COMTYP_STRING, CTNCOD_LEN,
                       "ctnwgt", COMTYP_FLOAT, sizeof(double),
                       "ctnvol", COMTYP_FLOAT, sizeof(double),
                       "ltlcls", COMTYP_STRING, LTLCLS_LEN,
                       "parcel_flg",COMTYP_LONG,sizeof(long),
                       "hazmat_flg",COMTYP_LONG,sizeof(long),
                       "stccod", COMTYP_STRING, STCCOD_LEN,
                       NULL);                 
    }
               
    for(CurPck = TopPck; CurPck; CurPck = CurPck->next)
    {
        if (CurPck->isFirst == BOOLEAN_TRUE)
        {
            misTrc(T_FLOW, "Building Carton %s ", CurPck->ctnnum);
            strncpy(ltlcls,CurPck->ltlcls,LTLCLS_LEN);
            ltlrnk = CurPck->ltlrnk;
            strncpy(stccod,CurPck->stccod,STCCOD_LEN);
            stcrnk = CurPck->stcrnk;
            parcel_flg = 0;
            hazmat_flg = 0;
            /*We need to find the max ltlcls and the highest parcel flag*/
            for (TmpPck = TopPck; TmpPck; TmpPck = TmpPck->next)
            {
                if ((strcmp(CurPck->ctnnum, TmpPck->ctnnum) == 0) && (CurPck->isNesting == BOOLEAN_FALSE))
                {
                    misTrc(T_FLOW, "Working Part %s ", TmpPck->prtnum);
                    /* If the ltl Rank is -1, we assume that it does not
                     * have a rank
                     * */
                    if ((ltlrnk > TmpPck->ltlrnk && 
                                TmpPck->ltlrnk != 0) || 
                                (ltlrnk == -1))
                    {
                        strncpy(ltlcls,TmpPck->ltlcls,LTLCLS_LEN);
                        ltlrnk = TmpPck->ltlrnk;
                    }
                    if ((stcrnk > TmpPck->stcrnk &&
                                TmpPck->stcrnk != 0) ||
                                (stcrnk == -1))
                    {
                        strncpy(stccod,TmpPck->stccod,STCCOD_LEN);
                        stcrnk = TmpPck->stcrnk;
                    }
                    if (TmpPck->parcel_flg == 1)
                        parcel_flg = 1;
                    if (TmpPck->hazmat_flg == 1)
                        hazmat_flg = 1;
                                 
                 }
             }
             ret_status = srvResultsAdd(RetCurPtr, 
                                        CurPck->ctnnum,
                                        prcmod,
                                        CurPck->carton->ctncod,
                                        CurPck->carton->curwgt,
                                        CurPck->carton->curvol,
                                        ltlcls,
                                        parcel_flg,
                                        hazmat_flg,
                                        stccod);


        } /*Close out if is First Carton */ 
    }
    misTrc(T_FLOW, "Departing sBuildReturnList ");
    return RetCurPtr;
}

/*Validate that a new pick will not exceed the weight and volume for a carton*/
/*This will include Nesting Validation*/
/*This will also update the nesting structures if the update flag is set*/
/*This command will return -1 if the new pick will NOT fit in the carton */

static long sValidateWeightAndVolume(PICKS_DATA ** PtrTopPck,
                                     PICKS_DATA * CurPck,
                                     CARTON_DATA * tempCarton,
                                     double pckvol,
                                     double pckwgt,
                                     moca_bool_t update_flg)
{
    double nstlen = 0, nsthgt = 0, nstwid =0, dtllen=0, dtlhgt=0, dtlwid=0, maximumLength=0, maximumWidth=0, maximumHeight=0;
    double maximumNstLen=0, maximumNstHgt=0, maximumNstWid=0, newNestingLength=0, newNestingWidth=0, newNestingHeight=0;
    double newNestingVolume=0, newNestingLengthRem=0, newNestingWidthRem=0, newNestingHeightRem=0;
    double oldNestingVolume=0;
    double old_difference=0;
   

    NESTING_DATA * CurNst = NULL, * NewCurNst = NULL;
    NESTING_DATA * tryToSplit = NULL;
    misTrc(T_FLOW, "entering sValidateWeightAndVolume");
    if ((tempCarton->curwgt + pckwgt) > tempCarton->ctnwlm)
    {
        misTrc(T_FLOW, "Weight is exceeded for wrkref = %s and carton = %s", CurPck->wrkref,tempCarton->ctnnum);
        return -1;
    }
    /*if it is virtual it means that it is a nesting part*/
    if (CurPck->isVirtual == BOOLEAN_TRUE)
    {
        misTrc(T_FLOW, "Processing a Nested Part");
        nstlen = CurPck->nstlen;
        nsthgt = CurPck->nsthgt;
        nstwid = CurPck->nstwid;
        dtllen = CurPck->dtllen;
        dtlhgt = CurPck->dtlhgt;
        dtlwid = CurPck->dtlwid;
        /*We will loop through all nesting classes in carton*/
        for (CurNst = tempCarton->nestingList; CurNst; CurNst = CurNst->next)
        {
            if (strncmp(CurNst->nstcls, CurPck->nstcls, NSTCLS_LEN) == 0 )  
            {
                oldNestingVolume =  ((CurNst->length + (CurNst->nstlen * (CurNst->untqty-1))) *
                                     (CurNst->width + (CurNst->nstwid * (CurNst->untqty-1))) *
                                     (CurNst->height + (CurNst->nsthgt * (CurNst->untqty-1))));
                maximumLength = sMax(CurNst->length,dtllen);
                maximumWidth  = sMax(CurNst->width,dtlwid);
                maximumHeight = sMax(CurNst->height,dtlhgt);
                maximumNstLen = sMax(CurNst->nstlen,nstlen);
                maximumNstHgt = sMax(CurNst->nsthgt,nsthgt);
                maximumNstWid = sMax(CurNst->nstwid,nstwid);
                newNestingLength = maximumLength + (maximumNstLen * (CurNst->untqty+CurPck->pckqty-1));
                newNestingWidth  = maximumWidth + (maximumNstWid * (CurNst->untqty+CurPck->pckqty-1));
                newNestingHeight = maximumHeight + (maximumNstHgt * (CurNst->untqty+CurPck->pckqty-1));
                newNestingVolume =  newNestingLength * newNestingWidth * newNestingHeight;
                if ((tempCarton->curvol - oldNestingVolume + newNestingVolume) > tempCarton->ctnvol)
                {
                    misTrc(T_FLOW, "Volume is exceeded before split for wrkref = %s and carton = %s with current volume of %.3f and old nesting volume of %.3f and new nesting volume of %.3f and a max volume of %.3f", CurPck->wrkref,tempCarton->ctnnum,tempCarton->curvol,oldNestingVolume,newNestingVolume,tempCarton->ctnvol);
                    return -1;    
                }
                tryToSplit = NULL;
                /*Need to validate that the new longest length will fit in the carton*/
                if (sValidatePieceFitsInCartonByDimension(newNestingLength,
                                                          newNestingWidth,
                                                          newNestingHeight,
                                                          tempCarton->ctnlen,
                                                          tempCarton->ctnwid,
                                                          tempCarton->ctnhgt) == BOOLEAN_FALSE)
                {   
                    /*The assumption is made that the Last Carton with the same nstcls will be the biggest*/
                    tryToSplit = CurNst;
                }
                else
                {
                    if (update_flg == BOOLEAN_TRUE)
                    {                            
                        tempCarton->curvol = tempCarton->curvol - oldNestingVolume + newNestingVolume;  
                        return sUpdateNestingList(CurNst, 
                                                  maximumLength,
                                                  maximumWidth,
                                                  maximumHeight,
                                                  maximumNstLen,
                                                  maximumNstWid,
                                                  maximumNstHgt,
                                                  CurPck->pckqty+CurNst->untqty);
                    }
                    return 0;
                     
                }
                 
            }
        }
         
        /*Try to split a stack of grabage cans into 2 stacks because the lengths do not fit*/
        if (tryToSplit != NULL)
        {
           if (update_flg == BOOLEAN_TRUE)
           {                           
               tempCarton->curvol = tempCarton->curvol - oldNestingVolume + newNestingVolume;
               newNestingVolume = 0;
               oldNestingVolume = 0;
           }        
           CurNst=tryToSplit;                                   
           if ((sSplitIntoMultiplePiles(PtrTopPck,
                                        CurNst,
                                        tempCarton,
                                        &old_difference,
                                        tempCarton->ctnvol,
                                        tempCarton->ctnlen,
                                        tempCarton->ctnwid,
                                        tempCarton->ctnhgt,
                                        update_flg,
                                        maximumLength,
                                        maximumWidth,
                                        maximumHeight,
                                        maximumNstLen,
                                        maximumNstWid,
                                        maximumNstHgt,
                                        CurNst->nstcls,
                                        CurPck->rpkcls,
                                        CurNst->untqty + CurPck->pckqty,
                                        newNestingVolume-oldNestingVolume) == BOOLEAN_FALSE))
           {
               misTrc(T_FLOW, "Volume is exceeded on Split for wrkref = %s and carton = %s with current volume of %.3f and old nesting volume of %.3f and new nesting volume of %.3f",
                            CurPck->wrkref,
                            tempCarton->ctnnum,
                            tempCarton->curvol,
                            oldNestingVolume,
                            newNestingVolume);
               return -1;
           }
           return 0;
             
        }
        /*if we got here this means that we are starting a new nesting list*/
        newNestingLength = CurPck->dtllen + (CurPck->nstlen * (CurPck->pckqty-1));
        newNestingWidth  = CurPck->dtlwid + (CurPck->nstwid * (CurPck->pckqty-1));
        newNestingHeight = CurPck->dtlhgt + (CurPck->nsthgt * (CurPck->pckqty-1));
        newNestingVolume =  newNestingLength * newNestingWidth * newNestingHeight;
        if ((tempCarton->curvol + newNestingVolume) > tempCarton->ctnvol)
        {
            misTrc(T_FLOW, "Volume is exceeded on new nstcls for wrkref = %s and carton = %s with current volume of %.3f and old nesting volume of %.3f and new nesting volume of %.3f", 
                           CurPck->wrkref,
                           tempCarton->ctnnum,
                           tempCarton->curvol,
                           oldNestingVolume,
                           newNestingVolume);
            return -1;
        }
        else 
        {
                   
           if (update_flg == BOOLEAN_TRUE)
           {
                           
               tempCarton->curvol = tempCarton->curvol + newNestingVolume;
               CurNst = sCreateNewNestingList(PtrTopPck,
                                              tempCarton,
                                              CurPck->nstcls,
                                              CurPck->rpkcls,
                                              CurPck->dtllen,
                                              CurPck->dtlwid,
                                              CurPck->dtlhgt,
                                              CurPck->nstlen,
                                              CurPck->nstwid,
                                              CurPck->nsthgt,
                                              CurPck->pckqty);
               newNestingVolume = 0;
           }           
           if (sSplitIntoMultiplePiles(PtrTopPck,
                                      CurNst,
                                      tempCarton,
                                      &old_difference,
                                      tempCarton->ctnvol,
                                      tempCarton->ctnlen,
                                      tempCarton->ctnwid,
                                      tempCarton->ctnhgt,
                                      update_flg,
                                      CurPck->dtllen,
                                      CurPck->dtlwid,
                                      CurPck->dtlhgt,
                                      CurPck->nstlen,
                                      CurPck->nstwid,
                                      CurPck->nsthgt,
                                      CurPck->nstcls,
                                      CurPck->rpkcls,
                                      CurPck->pckqty,
                                      newNestingVolume) == BOOLEAN_FALSE)
           {
               misTrc(T_FLOW, "Volume is exceeded on Split for wrkref = %s and carton = %s with current volume of %.3f and old nesting volume of %.3f and new nesting volume of %.3f",
                              CurPck->wrkref,
                              tempCarton->ctnnum,
                              tempCarton->curvol,
                              oldNestingVolume,
                              newNestingVolume);
               return -1;
           }
           return 0;
        }
    }        
    if ((tempCarton->curvol + pckvol) > tempCarton->ctnvol)
    {
        misTrc(T_FLOW, "Volume is exceeded for normal wrkref for wrkref = %s and carton = %s with current volume of %.3f and old nesting volume of %.3f and new nesting volume of %.3f", 
                       CurPck->wrkref,
                       tempCarton->ctnnum,
                       tempCarton->curvol,
                       oldNestingVolume,
                       newNestingVolume);
        return -1;
    }
    if (update_flg == BOOLEAN_TRUE)
         tempCarton->curvol = tempCarton->curvol + pckvol;
     return 0;
}

/*Create a new Nesting List*/
/*This command will return a pointer to the new Nesting Class List*/
       
static NESTING_DATA * sCreateNewNestingList(PICKS_DATA ** PtrTopPck,
                                            CARTON_DATA * tempCarton,
                                            char * nstcls,
                                            char * rpkcls,
                                            double maximumLength,
                                            double maximumWidth,
                                            double maximumHeight,
                                            double maximumNstLen,
                                            double maximumNstWid,
                                            double maximumNstHgt,
                                            long untqty)
{
    NESTING_DATA * NstLst = NULL;
    NESTING_DATA * LstNst = NULL;
    PICKS_DATA * NewPck = NULL;

    if (tempCarton->nestingList == NULL)
        LstNst = tempCarton->nestingList;
    else
    {
        LstNst = tempCarton->nestingList;
        while(LstNst->next != NULL)
            LstNst=LstNst->next;
    }
    NstLst = (NESTING_DATA *) calloc(1, sizeof(NESTING_DATA));
    memset(NstLst,0,sizeof(NESTING_DATA));
    NstLst->length = maximumLength;
    NstLst->width  = maximumWidth;
    NstLst->height = maximumHeight;
    NstLst->nstlen = maximumNstLen;
    NstLst->nsthgt = maximumNstHgt;
    NstLst->nstwid = maximumNstWid;
    NstLst->untqty = untqty;
    memset(NstLst->nstcls,0,sizeof(NstLst->nstcls));
    strncpy(NstLst->nstcls,nstcls,NSTCLS_LEN);
    NstLst->nstseq = sGetNestingSequence();
    NstLst->next = NULL;
  
    if (LstNst)
        LstNst->next = NstLst;
    else
        tempCarton->nestingList = NstLst;
     
    NewPck = (PICKS_DATA *) calloc(1, sizeof(PICKS_DATA));
    memset(NewPck,0,sizeof(PICKS_DATA)); 
    NewPck->dtllen = maximumLength + maximumNstLen * (untqty - 1);
    NewPck->dtlhgt = maximumHeight + maximumNstHgt * (untqty - 1);
    NewPck->dtlwid = maximumWidth + maximumNstWid * (untqty - 1);
    NewPck->dtlvol = NewPck->dtllen * NewPck->dtlwid * NewPck->dtlhgt;
    NewPck->dtlwgt =0;
    NewPck->untlen = 0;
    NewPck->paklen = 0;
    NewPck->untpak = 0;
    NewPck->pckqty = 0;
    NewPck->isFirst = BOOLEAN_FALSE;
    NewPck->isVirtual = BOOLEAN_FALSE;
    NewPck->isNesting = BOOLEAN_TRUE;
    memset(NewPck->rpkcls,0,sizeof(NewPck->rpkcls));
    strncpy(NewPck->rpkcls,rpkcls,RPKCLS_LEN);
    memset(NewPck->wrkref,0,sizeof(NewPck->wrkref));
    sprintf(NewPck->wrkref,"%ld",NstLst->nstseq);
    memset(NewPck->ctnnum,0,sizeof(NewPck->ctnnum));
    strncpy(NewPck->ctnnum,tempCarton->ctnnum,CTNNUM_LEN);
    NstLst->piece_structure = NewPck;
    NewPck->next = *PtrTopPck;
    misTrc(T_FLOW, "created nesting pointer %x",NewPck);
    misTrc(T_FLOW, "create nesting SEQUENCE %ld.  It is in carton %s. and is %.3f X %.3f X %.3f of %ld",
                  NstLst->nstseq,NewPck->ctnnum, 
                  NewPck->dtllen, 
                  NewPck->dtlwid,
                  NewPck->dtlhgt,
                  NstLst->untqty);
    *PtrTopPck = NewPck; 
    return NstLst;
    
}

/*update the nesting list structure to reflect changes*/ 
/*This command returns the nesting sequence number*/
    
static long sUpdateNestingList(NESTING_DATA * CurNst, 
                               double maximumLength,
                               double maximumWidth,
                               double maximumHeight,
                               double maximumNstLen,
                               double maximumNstWid,
                               double maximumNstHgt,
                               long untqty)
{
    PICKS_DATA * NewPck = CurNst->piece_structure;
    CurNst->length = maximumLength;
    CurNst->width  = maximumWidth;
    CurNst->height = maximumHeight;
    CurNst->nstlen = maximumNstLen;
    CurNst->nsthgt = maximumNstHgt;
    CurNst->nstwid = maximumNstWid;
    CurNst->untqty = untqty;
    NewPck->dtllen = maximumLength + maximumNstLen * (untqty - 1);
    NewPck->dtlhgt = maximumHeight + maximumNstHgt * (untqty - 1);
    NewPck->dtlwid = maximumWidth + maximumNstWid * (untqty - 1);
    NewPck->dtlvol = NewPck->dtllen * NewPck->dtlwid * NewPck->dtlhgt;
    misTrc(T_FLOW, "updating nesting SEQUENCE %ld.  It is in carton %s. and is %.3f X %.3f X %.3f of %ld",
                  CurNst->nstseq,
                  NewPck->ctnnum, 
                  NewPck->dtllen, 
                  NewPck->dtlwid,
                  NewPck->dtlhgt,
                  CurNst->untqty);
    return CurNst->nstseq;
}


/* This function returns: 
 *      A nesting Sequence number
 *This will be a sequence number
 *This will be a static number to allow for multiple instances of process cartonization to run at the same time
 *It will only reset when the system is started and stopped or it reaches its limit      
 * */

static long sGetNestingSequence()
{
    static long nstseq = 0;
    if (nstseq > 1000000000)
        nstseq = 0;
    nstseq++;
    return nstseq;       
}

/*Validate that a piece will fit in carton by rotating it around*/
/*This command returns TRUE if the piece will fit by dimension inside the carton*/

static moca_bool_t sValidatePieceFitsInCartonByDimension(double dtllen,
                                                         double dtlwid,
                                                         double dtlhgt,
                                                         double ctnlen,
                                                         double ctnwid,
                                                         double ctnhgt)
{
    if (dtllen <= ctnlen && dtlwid <= ctnwid && dtlhgt <= ctnhgt)
        return BOOLEAN_TRUE;
    if (dtllen <= ctnlen && dtlwid <= ctnhgt && dtlhgt <= ctnwid)
        return BOOLEAN_TRUE;
    if (dtllen <= ctnwid && dtlwid <= ctnlen && dtlhgt <= ctnhgt)
        return BOOLEAN_TRUE;
    if (dtllen <= ctnwid && dtlwid <= ctnhgt && dtlhgt <= ctnlen)
        return BOOLEAN_TRUE;
    if (dtllen <= ctnhgt && dtlwid <= ctnlen && dtlhgt <= ctnwid)
        return BOOLEAN_TRUE;
    if (dtllen <= ctnhgt && dtlwid <= ctnwid && dtlhgt <= ctnlen)
        return BOOLEAN_TRUE;
    misTrc(T_FLOW, "failed to fit by dimension");
    return BOOLEAN_FALSE;   
}

/*Validate that the nesting classes can fit*/
/*The update flag will update the international structures*/
/*so that complex cartonization can move around nesting structures and not actuals*/

static moca_bool_t sValidateNestingForEntireCarton(PICKS_DATA ** TopPckPtr,
                                                   CARTON_DATA * tempCarton,
                                                   double maxvol,
                                                   double ctnlen,
                                                   double ctnwid,
                                                   double ctnhgt,
                                                   moca_bool_t update_flg)
{
    PICKS_DATA * CurPck = NULL;
    NESTING_DATA * CurNst = NULL;
    double old_difference=0;
    misTrc(T_FLOW, "Entering sValidateNestingForEntireCarton... "
                   "with carton %s and carton code %s",tempCarton->ctnnum,tempCarton->ctncod);
    if (tempCarton->nestingList == NULL)
        return BOOLEAN_TRUE;
    
    /*We know the volume fits cause that is tested.  We need to validate that the dim of the nesting class fits*/
    for (CurNst = tempCarton->nestingList; CurNst; CurNst = CurNst->next)
    {
        if (sValidatePieceFitsInCartonByDimension(CurNst->length+CurNst->nstlen*(CurNst->untqty-1),
                                                  CurNst->width+CurNst->nstwid*(CurNst->untqty-1),
                                                  CurNst->height+CurNst->nsthgt*(CurNst->untqty-1),
                                                  ctnlen,
                                                  ctnwid,
                                                  ctnhgt) == BOOLEAN_TRUE)
        {  
          if (CurNst->next == NULL)
                return BOOLEAN_TRUE;
        }
        else
        {
            if (sSplitIntoMultiplePiles(TopPckPtr,
                                        CurNst,
                                        tempCarton,
                                        &old_difference,
                                        maxvol,
                                        ctnlen,
                                        ctnwid,
                                        ctnhgt,
                                        update_flg,
                                        CurNst->length,
                                        CurNst->width,
                                        CurNst->height,
                                        CurNst->nstlen,
                                        CurNst->nstwid,
                                        CurNst->nsthgt,
                                        CurNst->nstcls,
                                        CurNst->piece_structure->rpkcls,
                                        CurNst->untqty,
                                        0) == BOOLEAN_FALSE)
                return BOOLEAN_FALSE;
        }
    }
    return BOOLEAN_TRUE;
}

/*This command will attempt to split a Nesting Class into multiple piles*/
/*This command will return TRUE if the nesting structures will STILL fit inside the carton by volume*/

static moca_bool_t sSplitIntoMultiplePiles(PICKS_DATA ** PtrTopPck,
                                           NESTING_DATA * CurNst,
                                           CARTON_DATA * tempCarton,
                                           double * old_difference,
                                           double maxvol,
                                           double ctnlen,
                                           double ctnwid,
                                           double ctnhgt,
                                           moca_bool_t update_flg,
                                           double length,
                                           double width,
                                           double height,
                                           double nstlen,
                                           double nstwid,
                                           double nsthgt,
                                           char * nstcls,
                                           char * rpkcls,
                                           long newQuantity,
                                           /*We may have not added a part of the pick*/
                                           double volume_not_added)
                                           
{
    long i = 0;
    long ii = 0, piles = 0, remainder = 0;
    double newNestingVolume = 0, oldNestingVolume = 0;
 
    if (sValidatePieceFitsInCartonByDimension(length+nstlen*(newQuantity-1),
                                              width+nstwid*(newQuantity-1),
                                              height+nsthgt*(newQuantity-1),
                                              ctnlen,
                                              ctnwid,
                                              ctnhgt))
        return BOOLEAN_TRUE;

    /*If the dims do not fit, we could try to split this in half*/
    /*Lets find the highest qty that will fit in the carton */
    for (i = newQuantity - 1; i > 0;i--)
    {
        if (sValidatePieceFitsInCartonByDimension(length+nstlen*(i-1),
                                                  width+nstwid*(i-1),
                                                  height+nsthgt*(i-1),
                                                  ctnlen,
                                                  ctnwid,
                                                  ctnhgt))
            break;
    }     
    if (i == 0)
    {
        misTrc(T_FLOW, "Qty of 1 does not even fit in Carton!");
        return BOOLEAN_FALSE;
    }
    /*Do Piles of i fit inside the carton*/       
    /*We will have X piles of i and a remainder*/
    remainder =  (long) fmod(newQuantity,i);
    piles = (long) newQuantity / i;
    misTrc(T_FLOW, "We have %ld piles of %ld and a remainder of %ld ",piles,i,remainder);
    oldNestingVolume =  ((length + (nstlen * (newQuantity-1))) *
                                      (width + (nstwid * (newQuantity-1))) *
                                      (height + (nsthgt * (newQuantity-1))));
    newNestingVolume = piles * (length + (nstlen * (i-1))) *
                                      (width + (nstwid * (i-1))) *
                                      (height + (nsthgt * (i-1)));
    if (remainder > 0)
    {
        newNestingVolume = newNestingVolume + (length + (nstlen * (remainder-1))) *
                                      (width + (nstwid * (remainder-1))) *
                                      (height + (nsthgt * (remainder-1)));
    }
    misTrc(T_FLOW, "We have used %.3f of  %.3f in our carton and our new volume is  %.3f and old volume is  %.3f and old difference is  %.3f and volume not added is %.3f"
                 ,tempCarton->curvol,
                  maxvol,
                  newNestingVolume,
                  oldNestingVolume,
                  *old_difference,
                  volume_not_added);
    /*The old difference is the carton volume that got sucked up from other nesting structures that were split*/
    if ((tempCarton->curvol + *old_difference + newNestingVolume - oldNestingVolume + volume_not_added) > maxvol)
    {
        misTrc(T_FLOW, "Volume was exceeded on Split");
        return BOOLEAN_FALSE;
    }
    else
    /*The old difference is the amount of volume already lost because of split stacks of nesting classes*/
        *old_difference = *old_difference + newNestingVolume - oldNestingVolume;
    /*We then have to restack the nesting stuff to look like how it should*/
    if (update_flg == BOOLEAN_TRUE)
    {
        *old_difference = 0;
        for (ii = 0; ii < piles; ii++)
        {
            if (ii == 0)
            {
                sUpdateNestingList(CurNst,
                                   length,
                                   width,
                                   height,
                                   nstlen,
                                   nstwid,
                                   nsthgt,
                                   i);
            }
            else
            {
                sCreateNewNestingList(PtrTopPck,
                                      tempCarton,
                                      nstcls,
                                      rpkcls,
                                      length,
                                      width,
                                      height,
                                      nstlen,
                                      nstwid,
                                      nsthgt,
                                      i);    
            }
     
        }
        if (remainder > 0)
        {
            sCreateNewNestingList(PtrTopPck,
                                  tempCarton,
                                  nstcls,
                                  rpkcls,
                                  length,
                                  width,
                                  height,
                                  nstlen,
                                  nstwid,
                                  nsthgt,
                                  remainder);    
        }
        tempCarton->curvol = tempCarton->curvol-oldNestingVolume+newNestingVolume + volume_not_added;
    }
    return BOOLEAN_TRUE;
    
}
/*Gather Information used for Complex Carton and narrowing down the carton selection*/
PICKS_DATA * sGatherCartonInformationForComplex(PICKS_DATA ** TopPck, 
                                                long * pieces_in_this_carton,
                                                double * largest_piece_dimension,
                                                double * smallest_piece_volume,
                                                char * carton_number)
{
    PICKS_DATA * CurPck = NULL;
    PICKS_DATA * TopThisCarton = NULL;
    for (CurPck = *TopPck; CurPck; CurPck = CurPck->next)
    {
            if (strncmp(CurPck->ctnnum, carton_number, CTNNUM_LEN) == 0)
        {
            if (TopThisCarton == NULL && CurPck->isVirtual == BOOLEAN_FALSE)
                TopThisCarton = CurPck;
            /*If it is Virtual it is not an actual piece it is part of another structure*/
            if (CurPck->isVirtual == BOOLEAN_FALSE)
                (*pieces_in_this_carton)++;
            /*Dont count items that are a nesting structure*/
            /*Because they can be re-stacked to be smaller */           
            if (CurPck->isNesting == BOOLEAN_FALSE)
            {               
                if (*largest_piece_dimension < CurPck->dtllen)
                    *largest_piece_dimension = CurPck->dtllen;
                if (*largest_piece_dimension < CurPck->dtlwid)
                    *largest_piece_dimension = CurPck->dtlwid;
                if (*largest_piece_dimension < CurPck->dtlhgt)
                    *largest_piece_dimension = CurPck->dtlhgt;
                if (*largest_piece_dimension < CurPck->untlen)
                    *largest_piece_dimension = CurPck->untlen;
                if (*largest_piece_dimension < CurPck->paklen &&
                    CurPck->pckqty >= CurPck->untpak)
                    *largest_piece_dimension = CurPck->paklen;
            }
            if (CurPck->isVirtual == BOOLEAN_FALSE)
            {
                if (*smallest_piece_volume < .01 ||
                        *smallest_piece_volume >
                       CurPck->dtllen * CurPck->dtlwid * CurPck->dtlhgt)
                                *smallest_piece_volume =
                                   CurPck->dtllen * CurPck->dtlwid * CurPck->dtlhgt;
            }    
        }
    }
    if (*smallest_piece_volume < .01)
        *smallest_piece_volume = .01;
    return TopThisCarton;
}

/*Create a Function to return the maximum number of 2 numbers*/
static double sMax(double a, 
                   double b)
{
    if (a > b)
        return a;
    return b;
}
