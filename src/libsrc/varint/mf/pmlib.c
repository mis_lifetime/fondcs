static char *rcsid = "$Id: pmlib.c,v 1.28 2004/09/23 13:27:58 mzais Exp $";
/*#START***********************************************************************
 *
 *  Copyright (c) 2004 RedPrairie Corporation.  All rights reserved.
 *
 *#END************************************************************************/


#include <moca_app.h>

#include <stdio.h>
#include <stdarg.h>

#include "pmlib.h"

/* Parcel Manifest system data structures */

PM_CONFIG pmConfig;

/* Format context definition string start */

long pmContextStart (char *contxt)
{

    /* Write start of context string */

    sprintf (contxt, "[select");

    /* Normal successful completion */

    return (eOK);
}

/* Format context definition string end */

long pmContextEnd (char *contxt)
{

    /* If context string is empty, then return immediately */

    if (*contxt == 0)
    {
	return (eOK);
    }

    /* Remove last character (comma) from context string */

    contxt [strlen (contxt) - 1] = 0;

    /* Write end of context string */

    sprintf (contxt + strlen (contxt), " from dual] |");

    /* Normal successful completion */

    return (eOK);
}

/* Format context definition double value */

long pmContextDouble (char *contxt, double value, char *name)
{

    /* If value is undefined (zero), then return immediately */

    if (value == 0.0)
    {
	return (eOK);
    }

    /* If needed, initialize context definition string */

    if (*contxt == 0)
    {
	pmContextStart (contxt);
    }

    /* Append long value definition to context definition string */

    sprintf (contxt + strlen (contxt), " '%g' %s,", value, name);

    /* Normal successful completion */

    return (eOK);
}

/* Format context definition long value */

long pmContextLong (char *contxt, long value, char *name)
{

    /* If value is undefined (zero), then return immediately */

    if (value == 0)
    {
	return (eOK);
    }

    /* If needed, initialize context definition string */

    if (*contxt == 0)
    {
	pmContextStart (contxt);
    }

    /* Append long value definition to context definition string */

    sprintf (contxt + strlen (contxt), " '%ld' %s,", value, name);

    /* Normal successful completion */

    return (eOK);
}

/* Format context definition string value */

long pmContextString (char *contxt, char *value, char *name)
{
    char *vptr = NULL;

    long status;

    /* If value is undefined (blank), then return immediately */

    if (strlen (value) == 0)
    {
	return (eOK);
    }

    /* If needed, initialize context definition string */

    if (*contxt == 0)
    {
	pmContextStart (contxt);
    }

    /* Parse string value for aspostrophes (single quotes) */

    status = appParseForApostrophe (value, &vptr);
    if (status != eOK)
    {
	vptr = value;
    }

    /* Append string value definition to context definition string */

    sprintf (contxt + strlen (contxt), " '%s' %s,", vptr, name);

    /* Release parsed string value buffer */

    if (vptr != value)
    {
	free (vptr);
    }

    /* Normal successful completion */

    return (eOK);
}

/* Wrapper around sqlGetFloat to return zero if desired column is null */

double pmGetFloat (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam)
{
    static double default_value = 0;

    /* Return column long value (or default value if data is undefined) */

    if (sqlIsNull (resultSet, resultRow, colnam))
    {
	return (default_value);
    }
    else
    {
        return (sqlGetFloat (resultSet, resultRow, colnam));
    }
}

/* Wrapper around sqlGetLong to return zero if desired column is null */

long pmGetLong (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam)
{
    static long default_value = 0;

    /* Return column long value (or default value if data is undefined) */

    if (sqlIsNull (resultSet, resultRow, colnam))
    {
	return (default_value);
    }
    else
    {
        return (sqlGetLong (resultSet, resultRow, colnam));
    }
}

/* Wrapper around sqlGetString to return zero length string if desired column is null */

char *pmGetString (mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam)
{
    static char *default_value = "";

    /* Return column string value (or default value if data is undefined) */

    if (sqlIsNull (resultSet, resultRow, colnam))
    {
	return (default_value);
    }
    else
    {
        return (sqlGetString (resultSet, resultRow, colnam));
    }
}

/* Wrapper around sqlGetFloat to leave value unchanged if desired column is null */

long pmGetFloatIfSet (double *value, mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam)
{

    /* If column value is defined, then load column value */

    if (!sqlIsNull (resultSet, resultRow, colnam))
    {
	*value = sqlGetFloat (resultSet, resultRow, colnam);
    }

    return (eOK);
}

/* Wrapper around sqlGetLong to leave value unchanged if desired column is null */

long pmGetLongIfSet (long *value, mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam)
{

    /* If column value is defined, then load column value */

    if (!sqlIsNull (resultSet, resultRow, colnam))
    {
	*value = sqlGetLong (resultSet, resultRow, colnam);
    }

    return (eOK);
}

/* Wrapper around sqlGetString to leave value unchanged if desired column is null */

long pmGetStringIfSet (char *value, mocaDataRes *resultSet, mocaDataRow *resultRow, char *colnam)
{

    /* If column value is defined, then load column value */

    if (!sqlIsNull (resultSet, resultRow, colnam))
    {
	strcpy (value, sqlGetString (resultSet, resultRow, colnam));
    }

    return (eOK);
}

/* Get Parcel Manifest package identification information */

#ifdef MSG_HDR
    #undef MSG_HDR
#endif
#define MSG_HDR "DCSpm: (pmGetPackageIdInfo) "

long pmGetPackageIdInfo (char *carcod, char *mfsmsn, 
                         char *crtnid, char *invtid, char *wrkref, 
                         long opnflg,
                         char *dstnam,
                         char *mansts)
{
    RETURN_STRUCT   *resultData;

    mocaDataRes	    *resultSet;
    mocaDataRow	    *resultRow;

    char    buffer [PM_BUFFER_LEN];

    long    status;

    struct {
	char wrkref	    [WRKREF_LEN + 1];
	char invtyp	    [COLNAM_LEN + 1];
	char subnum	    [SUBNUM_LEN + 1];
	char prtnum	    [PRTNUM_LEN + 1];
	char prt_client_id  [CLIENT_ID_LEN + 1];
	char mansts	    [MANSTS_LEN + 1];
    } lcl;

    /* Initialize local variables */

    memset (&lcl, 0, sizeof (lcl));

    /* Load optional arguments into local variables */

    if (wrkref)
    {
	misTrimcpy (lcl.wrkref, wrkref, sizeof(lcl.wrkref));
    }

    /* If carton id is defined, then use it to get package id info */

    if (strlen (crtnid))
    {

	/* Get package id info by carton id */

	sprintf (buffer,
	    "select carcod, mfsmsn, mansts , dstnam "
            "  from manfst where crtnid='%s'",
	    crtnid);

	status = sqlExecStr (buffer, &resultSet);
	if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
	{
	    sqlFreeResults (resultSet);
	    return (status);
	}
	if (status == eDB_NO_ROWS_AFFECTED)
	{
	    sqlFreeResults (resultSet);

	    /* If package id info not found by carton id, then assume  */
	    /* carton id is actually a parcel manifest sequence number */
	    /* and use it (mfsmsn) to get package id info              */

	    sprintf (buffer,
		"select carcod, mfsmsn, mansts, dstnam "
                "  from manfst where mfsmsn='%s'",
		crtnid);

	    status = sqlExecStr (buffer, &resultSet);
	    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
	    {
		sqlFreeResults (resultSet);
		return (status);
	    }
	    if (status == eDB_NO_ROWS_AFFECTED)
	    {
		pmLogMsg (MSG_HDR, "Package (%s) not found", crtnid);
		sqlFreeResults (resultSet);
		return (status);
	    }
	}
    }

    /* Else use inventory identifier to get package id info */

    else
    {
	/* Get the part client id from the wrkref table */
	
	sprintf (buffer,
	    "select prt_client_id from pckwrk "
	    "	where wrkref='%s' ",
	    wrkref);

	status = sqlExecStr(buffer, &resultSet);
	if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
	{
	    pmLogMsg (MSG_HDR, "Error %ld selecting "
		"part client id for wrkref (%s)", status, wrkref);
	    sqlFreeResults (resultSet);
	    return (status);
	}
        if (status == eOK)
        { 
	    resultRow = sqlGetRow(resultSet);
            misTrimcpy (lcl.prt_client_id, 
	          sqlGetString(resultSet, resultRow, "prt_client_id"), 
                  sizeof(lcl.prt_client_id) );
        }
	/* Free the results from the pckwrk query above */
	sqlFreeResults (resultSet);
	resultRow = NULL;

	/* Get translated inventory identifier information */
	sprintf (buffer,
	    "get translated inventory identifier "
	    "	where id='%s'"
	    "	  and prt_client_id='%s'",
	    invtid, 
            lcl.prt_client_id);


	status = srvInitiateCommand (buffer, &resultData);
	if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
	{
	    srvFreeMemory (SRVRET_STRUCT, resultData);
	    return (status);
	}
	if (status == eDB_NO_ROWS_AFFECTED)
	{
	    srvFreeMemory (SRVRET_STRUCT, resultData);

	    /* If package id info not found by inventory id, then assume  */
	    /* inventory id is actually a parcel manifest tracking number */
	    /* and use it (traknm) to get package id info                 */

	    sprintf (buffer,
		"select carcod, mfsmsn, mansts, dstnam "
                "  from manfst where traknm='%s'",
		invtid);

	    status = sqlExecStr (buffer, &resultSet);
	    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
	    {
		sqlFreeResults (resultSet);
		return (status);
	    }
	    if (status == eDB_NO_ROWS_AFFECTED)
	    {
		pmLogMsg (MSG_HDR, "Inventory identifier (%s) not found", invtid);
		sqlFreeResults (resultSet);
		return (status);
	    }
	}

	/* If inventory identifier information was found, then process it */
	/* to obtain desired package id info                              */

	else if (status == eOK)
	{
	    if (resultData->rows != 1)
	    {
		pmLogMsg (MSG_HDR, "Inventory indentifier (%s) is not unique", invtid);
		srvFreeMemory (SRVRET_STRUCT, resultData);
		return (eINT_IDENTIFIER_NOT_RECOGNIZED);
	    }
	    resultSet = srvGetResults(resultData);
	    resultRow = sqlGetRow (resultSet);

	    /* Map inventory identifier information into local buffer */

	    misTrimcpy (lcl.invtyp, pmGetString (resultSet, resultRow, "colnam"),
		    sizeof(lcl.invtyp));

	    pmLogMsg (MSG_HDR, "Inventory identifier (%s) is type (%s)", invtid, lcl.invtyp);

	    if (strcmp (lcl.invtyp, PM_INVTYP_SUBNUM) == 0)
	    {
		misTrimcpy (lcl.subnum, pmGetString (resultSet, resultRow, "subnum"),
			sizeof(lcl.subnum));
	    }
	    else if (strcmp (lcl.invtyp, PM_INVTYP_SUBUCC) == 0)
	    {
		misTrimcpy (lcl.subnum, pmGetString (resultSet, resultRow, "subnum"),
			sizeof(lcl.subnum));
		misTrimcpy (lcl.invtyp, PM_INVTYP_SUBNUM, sizeof(lcl.invtyp));
	    }
	    else if (strcmp (lcl.invtyp, PM_INVTYP_PRTNUM) == 0)
	    {
		misTrimcpy (lcl.prtnum,        pmGetString (resultSet, resultRow, "prtnum"),
			sizeof(lcl.prtnum));
		misTrimcpy (lcl.prt_client_id, pmGetString (resultSet, resultRow, "prt_client_id"),
			sizeof(lcl.prt_client_id));
	    }
	    else
	    {
		pmLogMsg (MSG_HDR, "Inventory identifier type (%s) not supported", lcl.invtyp);
		srvFreeMemory (SRVRET_STRUCT, resultData);
		return (eINT_IDENTIFIER_NOT_RECOGNIZED);
	    }

	    /* Free intermediate result data set */

	    srvFreeMemory (SRVRET_STRUCT, resultData);

	    /* Get package id info by translated inventory identifier */

	    if (strcmp (lcl.invtyp, PM_INVTYP_SUBNUM) == 0)
	    {
		sprintf (buffer,
		    "select carcod, mfsmsn, mansts, dstnam "
                    "  from manfst where subnum='%s'",
		    lcl.subnum);
	    }
	    else if ((strcmp (lcl.invtyp, PM_INVTYP_PRTNUM) == 0) &&
		     (strlen (lcl.wrkref)))
	    {
		/* Programming Note:  This case can only be used to release  */
		/* packages on hold.  This is because the selection criteria */
		/* can select multiple packages if packages are being        */
		/* pre-manifested by part number only.                       */

		sprintf (buffer,
		    "select carcod, "
		    "    mfsmsn, "
		    "    mansts,  "
                    "    dstnam "
		    "from manfst "
		    "where "
		    "    prtnum        = '%s' and "
		    "    prt_client_id = '%s' and "
		    "    wrkref        = '%s' and "
		    "    mansts        = '%s' "
		    "order by "
		    "    crtnid ",
		    lcl.prtnum, lcl.prt_client_id, lcl.wrkref, PM_MANSTS_HELD);
	    }
	    else
	    {
		pmLogMsg (MSG_HDR, "Inventory identifier type (%s) not supported for current action", lcl.invtyp);
		return (eINT_IDENTIFIER_NOT_RECOGNIZED);
	    }

	    status = sqlExecStr (buffer, &resultSet);
	    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
	    {
		pmLogMsg (MSG_HDR, "Error %ld performing command [%s]", status, buffer);
		sqlFreeResults (resultSet);
		return (status);
	    }
	    if (status == eDB_NO_ROWS_AFFECTED)
	    {
		pmLogMsg (MSG_HDR, "Package (%s/%s) not found", invtid, lcl.wrkref);
		sqlFreeResults (resultSet);
		return (eINT_NOT_ALREADY_MANIFESTED);
	    }
	}
    }
    
    /* Retrieve package id information */

    resultRow = sqlGetRow (resultSet);

    strncpy (carcod, pmGetString (resultSet, resultRow, "carcod"), CARCOD_LEN);
    strncpy (mfsmsn, pmGetString (resultSet, resultRow, "mfsmsn"), MFSMSN_LEN);
    misTrimcpy(dstnam, sqlGetString(resultSet, resultRow, "dstnam"), DSTNAM_LEN);
    misTrimcpy(mansts, sqlGetString(resultSet, resultRow, "mansts"), MANSTS_LEN);

    misTrimcpy (lcl.mansts, pmGetString (resultSet, resultRow, "mansts"),
	    sizeof(lcl.mansts));

    sqlFreeResults (resultSet);

    /* If needed, verify package status is 'held' or 'released' */

    if (opnflg)
    {
	if ((strcmp (lcl.mansts, MANSTS_HELD) != 0) &&
	    (strcmp (lcl.mansts, MANSTS_RELEASED) !=0))
	{
	    pmLogMsg (MSG_HDR, "Package (%s/%s/%s) status is not 'held' or 'released'", crtnid, invtid, lcl.wrkref);
	    return (eDB_NO_ROWS_AFFECTED);
	}
    }

    return (eOK);
}


/* Get Parcel Manifest system policy information */

#ifdef MSG_HDR
    #undef MSG_HDR
#endif
#define MSG_HDR "DCSpm: (pmGetPolicyInfo) "
long pmGetParcelSysInfo(char *rmthst, char *sysnam, char * prcl_host, char * prcl_port, char * dstnam)
{
    mocaDataRes    *resultSet;
    mocaDataRow    *resultRow;

    char buffer[PM_BUFFER_LEN];

    long status;

    /* Build the SQL String */
    sprintf (buffer,
	    " select p1.parval || ':' || p2.parval rmthst, "
            "        p1.srvnam sysnam, "
            "        p1.parval prcl_host, "
            "        p2.parval prcl_port  "
            "   from prcl_par_data p1, prcl_par_data p2 "
	    "  where p1.srvnam = '%s'"
            "    and p2.srvnam = '%s' "
            "    and p1.parnam = '%s' "
            "    and p2.parnam = '%s' ",
	    dstnam,
            dstnam,
            PM_HOST,
            PM_PORT);

    status = sqlExecStr (buffer, &resultSet);
    if (status != eOK)
    {
	pmLogMsg (MSG_HDR, "Error %ld getting parcel system info using [%s]", status, buffer);
        sqlFreeResults (resultSet);
	return (status);
    }

    resultRow = sqlGetRow(resultSet);

    strcpy(rmthst, sqlGetString(resultSet, resultRow, "rmthst"));
    strcpy(sysnam, sqlGetString(resultSet, resultRow, "sysnam"));
    strcpy(prcl_host, sqlGetString(resultSet, resultRow, "prcl_host"));
    strcpy(prcl_port, sqlGetString(resultSet, resultRow, "prcl_port"));

    /* free memory */
    sqlFreeResults(resultSet);
    return(eOK);

}

long pmGetPolicyInfo (char *polvar, char *polval, char *polsub, char *rtstr1, char *rtstr2, long *rtnum1, long *rtnum2, double *rtflt1, double *rtflt2)
{
    mocaDataRes	    *resultSet;
    mocaDataRow	    *resultRow;

    char    buffer [PM_BUFFER_LEN];

    long    status;
    
    /* Format SQL Query string */

    sprintf (buffer,
	"select * from poldat "
	"where polcod='%s' and polvar = '%s' and polval = '%s' ",
	PM_POL_COD, polvar, polval);

    if (polsub && strlen (polsub))
    {
	sprintf (&buffer [strlen (buffer)], "and rtstr1 = '%s' ", polsub);
    }

    sprintf (&buffer [strlen (buffer)], "order by srtseq");

    /* Perform SQL Query */

    status = sqlExecStr (buffer, &resultSet);
    if (status != eOK)
    {
	pmLogMsg (MSG_HDR, "Error %ld getting policy info using [%s]", status, buffer);
        sqlFreeResults (resultSet);
	return (status);
    }
    resultRow = sqlGetRow (resultSet);

    /* Retrieve SQL Query results */

    if (rtstr1)
    {
	if (sqlIsNull (resultSet, resultRow, "rtstr1"))
	{
	    *rtstr1 = 0;
	}
	else
	{
	    strcpy (rtstr1, misTrim (sqlGetValue (resultSet, resultRow, "rtstr1")));
	}
    }

    if (rtstr2)
    {
	if (sqlIsNull (resultSet, resultRow, "rtstr2"))
	{
	    *rtstr2 = 0;
	}
	else
	{
	    strcpy (rtstr2, misTrim (sqlGetValue (resultSet, resultRow, "rtstr2")));
	}
    }

    if (rtnum1)
    {
	if (sqlIsNull (resultSet, resultRow, "rtnum1"))
	{
	    *rtnum1 = 0;
	}
	else
	{
	    *rtnum1 = sqlGetLong (resultSet, resultRow, "rtnum1");
	}
    }

    if (rtnum2)
    {
	if (sqlIsNull (resultSet, resultRow, "rtnum2"))
	{
	    *rtnum2 = 0;
	}
	else
	{
	    *rtnum2 = sqlGetLong (resultSet, resultRow, "rtnum2");
	}
    }

    if (rtflt1)
    {
	if (sqlIsNull (resultSet, resultRow, "rtflt1"))
	{
	    *rtflt1 = 0;
	}
	else
	{
	    *rtflt1 = sqlGetFloat (resultSet, resultRow, "rtflt1");
	}
    }

    if (rtflt2)
    {
	if (sqlIsNull (resultSet, resultRow, "rtflt2"))
	{
	    *rtflt2 = 0;
	}
	else
	{
	    *rtflt2 = sqlGetFloat (resultSet, resultRow, "rtflt2");
	}
    }

    /* Free SQL Query result structure & return normally */

    sqlFreeResults (resultSet);
    return (eOK);
}

/* Initialize Parcel Manifest system */

#ifdef MSG_HDR
    #undef MSG_HDR
#endif
#define MSG_HDR "DCSpm: (pmInitialize) "
long pmFedexInitialize(char * srvnam)
{
    long ret_status;
    pmLogMsg (MSG_HDR, "Checking if srvnam is FedEx");
    pmLogMsg (MSG_HDR, "srvnam =  %s", srvnam);
    memset(pmConfig.dstnam,0,sizeof(pmConfig.dstnam));
    /*Let check to see if the srvnam is FedEx*/
    /*The service name is Destination Carrier || . || destination level, so we will match */
    /* the destination carrier part of it to see if it is fedex */
    ret_status = srvInitiateCommandFormat(NULL,
                              " [select 'x' "
                              "from carxrf "
                              "where dstcar = '%s' and dstnam = '%s']",
                               srvnam, 
                               DSTNAM_FEDEX);
    if (ret_status == eOK)
    {
     
        pmGetParcelSysInfo (pmConfig.rmthst, pmConfig.sysnam, pmConfig.prcl_host, pmConfig.prcl_port, DSTNAM_FEDEX);
        misTrimcpy (pmConfig.dstnam , DSTNAM_FEDEX, sizeof(DSTNAM_FEDEX));

    }
    else
    {
        misTrimcpy (pmConfig.dstnam , DSTNAM_CONNECTSHIP, sizeof(DSTNAM_CONNECTSHIP));
        
    }
return ret_status;

}
long pmFascorInitialize(char * srvnam)
{
    long ret_status;
    pmLogMsg (MSG_HDR, "Checking if srvnam is Fascor");
    pmLogMsg (MSG_HDR, "srvnam =  %s", srvnam);
    /*Let check to see if the srvnam is FedEx*/
    /*The service name is Destination Carrier || . || destination level, so we will match */
    /* the destination carrier part of it to see if it is fedex */
    ret_status = srvInitiateCommandFormat(NULL,
                              " [select 'x' "
                              "from carxrf "
                              "where dstcar = '%s' and dstnam = '%s']",
                               srvnam, 
                               DSTNAM_FASCOR);
    /*If fascor was passed in as the service name, we want to set the service to fascor*/
    if (ret_status == eOK || (srvnam && misTrimStrncmp(DSTNAM_FASCOR, srvnam,  DSTNAM_LEN) == 0))
    {
        pmGetParcelSysInfo (pmConfig.rmthst, pmConfig.sysnam, pmConfig.prcl_host, pmConfig.prcl_port, DSTNAM_FASCOR);
        misTrimcpy (pmConfig.dstnam , DSTNAM_FASCOR, sizeof(DSTNAM_FASCOR));

    }
    
return ret_status;

}
long pmInitialize (void)
{
    long    status;

    char    buffer [RTSTR1_LEN + 1];

    /* If Parcel Manifest system is already initialized, then exit with normal success */

    if (pmConfig.iniflg)
    {
        /*We want to change this because we switch between parcel systems*/
        status = pmGetParcelSysInfo (pmConfig.rmthst, pmConfig.sysnam, pmConfig.prcl_host, pmConfig.prcl_port,DSTNAM_CONNECTSHIP);
	return (eOK);
    }

    /* Output informational diagnostic message */

    pmLogMsg (MSG_HDR, "Initializing Parcel Manifest system");

    /* Ensure Parcel Manifest data structures are cleared */

    memset (&pmConfig, 0, sizeof (pmConfig));

    /* Get Parcel Manifest remote host information */

    status = pmGetParcelSysInfo (pmConfig.rmthst, pmConfig.sysnam, pmConfig.prcl_host, pmConfig.prcl_port,DSTNAM_CONNECTSHIP);
    if (status != eOK)
    {
	pmLogMsg (MSG_HDR, "Error %ld getting Parcel Manifest system information", status);
	return (status);
    }
    
    /* Get Parcel Manifest shipper name information */

    status = pmGetPolicyInfo (PM_POL_VAR_CONFIG, PM_POL_CFG_SHNAME, NULL, pmConfig.shname, NULL, NULL, NULL, NULL, NULL);
    if (status != eOK)
    {
	pmLogMsg (MSG_HDR, "Error %ld getting Parcel Manifest shipper name information", status);
	return (status);
    }

    /* Get Parcel Manifest reference command information */

    status = pmGetPolicyInfo (PM_POL_VAR_CONFIG, PM_POL_CFG_REFCMD, NULL, pmConfig.refcmd, NULL, NULL, NULL, NULL, NULL);
    if (status != eOK)
    {
	pmLogMsg (MSG_HDR, "Error %ld getting Parcel Manifest reference command information", status);
	return (status);
    }

    /* Get Parcel Manifest weight multiplier information */

    status = pmGetPolicyInfo (PM_POL_VAR_CONFIG, PM_POL_CFG_WGTMUL, NULL, buffer, NULL, NULL, NULL, &pmConfig.wgtmul, NULL);
    if (status != eOK)
    {
	pmLogMsg (MSG_HDR, "Error %ld getting Parcel Manifest system name information", status);
	return (status);
    }
    if (pmConfig.wgtmul == 0.0)
    {
	pmConfig.wgtmul = 1.0;
    }
    if ((strcmp (buffer, "/") == 0) || (strcmp (buffer, "DIVIDEBY") == 0))
    {
	pmConfig.wgtmul = 1.0 / pmConfig.wgtmul;
    }

    /* Get Parcel Manifest default country code information */

    status = pmGetPolicyInfo (PM_POL_VAR_DEFAULT, PM_POL_DEF_DEFCTY, NULL, pmConfig.defcty, NULL, NULL, NULL, NULL, NULL);
    if (status != eOK)
    {
	pmLogMsg (MSG_HDR, "Error %ld getting Parcel Manifest default country code information", status);
	return (status);
    }

    /* Get Parcel Manifest default report prefix information */

    status = pmGetPolicyInfo (PM_POL_VAR_DEFAULT, PM_POL_DEF_DEFRPT, NULL, pmConfig.defrpt, NULL, NULL, NULL, NULL, NULL);
    if (status != eOK)
    {
	pmLogMsg (MSG_HDR, "Error %ld getting Parcel Manifest default report prefix information", status);
	return (status);
    }

    /* Verify if multiple DCS carriers are mapped to single */
    /* Parcel Manifest carrier                              */

    status = pmVerifyMultipleDcsCarriers ();
    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
    {
	pmLogMsg (MSG_HDR, "Error %ld verifying if multiple DCS carriers are mapped to single Parcel Manifest carrier", status);
	return (status);
    }
    pmConfig.mulcar = (status == eOK);

    /* Normal successful completion */

    pmConfig.iniflg = TRUE;

    return (eOK);
}


/* Format and log trace message */

long pmLogMsg (char *header, char *format, ...)
{
    va_list args;

    char buffer [5000];
    char final  [6000];

    /* Format message text */

    va_start (args, format);

    vsprintf (buffer, format, args);

    va_end (args);

    /* Prefix message header to message text */

    strcpy (final, header);
    strcat (final, buffer);

    /* Log (T_FLOW) level trace message */

    misTrc (T_FLOW, final);

    return (eOK);
}

/* Return remote host error (check for remote host being down) */

#ifdef MSG_HDR
    #undef MSG_HDR
#endif
#define MSG_HDR "DCSpm: (pmReturnRemoteHostError) "

RETURN_STRUCT *pmReturnRemoteHostError (long status_i)
{
    RETURN_STRUCT   *returnData;
    RETURN_STRUCT   *resultData;

    char    contxt [PM_CONTEXT_LEN + 1];
    char    rmthst [RTSTR1_LEN + 1];

    long    status;

    /* Branch to appropriate error processing section */

    switch (status_i)
    {

	/* Remote host connection failure */

	case (eSRV_REMOTE_CONNECT_FAILURE) :
	{

	    /* Get remote host information */

	    sprintf (contxt,
		"get pm config | get pm custom where action='DUMMY' | "
		"[ select @pm_remote_host pm_remote_host from dual ]");

	    status = srvInitiateCommand (contxt, &resultData);
	    if (status != eOK)
	    {
		pmLogMsg (MSG_HDR, 
			  "Error %ld getting customized pm "
			  "remote host name, using default value", status);
		misTrimcpy (rmthst, pmConfig.rmthst, sizeof(rmthst));
	    }
	    else
	    {
		misTrimcpy (rmthst, 
			 pmGetString (srvGetResults(resultData),
				      sqlGetRow (srvGetResults(resultData)),
				      "pm_remote_host"),sizeof(rmthst));
	    }
	    srvFreeMemory (SRVRET_STRUCT, resultData);

	    /* Output informational error message */

	    pmLogMsg (MSG_HDR, 
		      "Unable to connect to remote system (%s)", rmthst);

	    /* Initialize Moca Error return data set */

	    returnData = 
		srvErrorResults (eSRV_REMOTE_CONNECT_FAILURE,
				 "Unable to connect to remote "
				 "system ^hostname^",
				 "hostname", COMTYP_CHAR,
				 rmthst, NO_MLS_LOOKUP,
				 NULL);
	    
	    break;
	}
	
	/* Generic remote host error processing */

	default :
	{

	    /* Initialize Moca return data set with error status */

	    returnData = srvSetupReturn (status_i, "");
	    break;
	}
    }

    return (returnData);
}

/* Translate DCS carrier code from Parcel Manifest (partial) service code */

#ifdef MSG_HDR
    #undef MSG_HDR
#endif
#define MSG_HDR "DCSpm: (pmTranslateFromCarrierCode) "

long pmTranslateFromCarrierCode (char *carcod, char *service_code)
{
    mocaDataRes	    *resultSet;
    mocaDataRow	    *resultRow;

    char    buffer [PM_BUFFER_LEN];

    long    status;

    /* Format SQL query to find matching Carrier X-ref entry */
    /* Need to support both FedEx and ConnectShip */
    sprintf (buffer,
	"select carcod, srvlvl from carxrf "
	"where dstcar='%s' "
	"order by carcod, srvlvl ",
        service_code);

    /* Perform SQL query & retrieve pointer to 1st row */

    status = sqlExecStr (buffer, &resultSet);
    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
    {
        pmLogMsg (MSG_HDR, "Error %ld performing SQL query [%s]", status, buffer);
        sqlFreeResults (resultSet);
	return (status);
    }
    if (status == eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults (resultSet);  /* Not found */
	return (status);
    }
    resultRow = sqlGetRow (resultSet);

    /* Retrieve matching DCS carrier code */

    strncpy (carcod, pmGetString (resultSet, resultRow, "carcod"), CARCOD_LEN);

    /* Free SQL Query result structure & return normally */

    sqlFreeResults (resultSet);

    return (eOK);
}

/* Translate DCS carrier code into Parcel Manifest (partial) service code */

#ifdef MSG_HDR
    #undef MSG_HDR
#endif
#define MSG_HDR "DCSpm: (pmTranslateIntoCarrierCode) "

long pmTranslateIntoCarrierCode (char *service_code, char *carcod)
{
    mocaDataRes	    *resultSet;
    mocaDataRow	    *resultRow;

    char    buffer [PM_BUFFER_LEN];

    long    status;

    /* Format SQL query to find matching Carrier X-ref entry */

    sprintf (buffer,
	"select dstcar, dstlvl from carxrf "
	"where carcod='%s' "
	"order by carcod, srvlvl ",
	carcod);

    /* Perform SQL query & retrieve pointer to 1st row */

    status = sqlExecStr (buffer, &resultSet);
    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
    {
        sqlFreeResults (resultSet);
	return (status);
    }
    if (status == eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults (resultSet);  /* Not found */
	return (status);
    }
    resultRow = sqlGetRow (resultSet);

    /* Retrieve matching Parcel Manifest (partial) service code */

    strcpy (service_code, pmGetString (resultSet, resultRow, "dstcar"));

    /* Free SQL Query result structure & return normally */

    sqlFreeResults (resultSet);

    return (eOK);
}

/* Translate DCS carrier code & service level from Parcel Manifest service code */

#ifdef MSG_HDR
    #undef MSG_HDR
#endif
#define MSG_HDR "DCSpm: (pmTranslateFromServiceCode) "

long pmTranslateFromServiceCode (char *carcod, char *srvlvl,char *service_code)
{
    mocaDataRes	    *resultSet;
    mocaDataRow	    *resultRow;

    char    buffer  [PM_BUFFER_LEN];

    char    carrier [PM_SERVICE_CODE_LEN + 1];
    char    service [PM_SERVICE_CODE_LEN + 1];

    char    *sptr;
    char    *tptr;

    long    status;

    /* Initialize local variables */

    memset (carrier, 0, sizeof (carrier));
    memset (service, 0, sizeof (service));

    /* Separate service code into individual carrier and service components */

    sptr = service_code;  /* Find last delimiter character */
    while (sptr)
    {
	tptr = sptr;
	sptr = strchr (sptr + 1, PM_SERVICE_DELIMITER);
    }
    strncpy (carrier, service_code, tptr - service_code);
    strncpy (service, tptr + 1, sizeof (service) - 1);

    /* Format SQL query to find matching Carrier X-ref entry */

    sprintf (buffer,
	"select carcod, srvlvl from carxrf "
	"where dstcar='%s' and dstlvl='%s' "
	"order by carcod, srvlvl ",
	 carrier, service);

    /* Perform SQL query & retrieve pointer to 1st row */

    status = sqlExecStr (buffer, &resultSet);
    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
    {
        pmLogMsg (MSG_HDR, "Error %ld performing SQL query [%s]", status, buffer);
        sqlFreeResults (resultSet);
	return (status);
    }
    if (status == eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults (resultSet);  /* Not found */
	return (status);
    }
    resultRow = sqlGetRow (resultSet);

    /* Retrieve matching DCS carrier code & service level */

    strncpy (carcod, pmGetString (resultSet, resultRow, "carcod"), CARCOD_LEN);
    strncpy (srvlvl, pmGetString (resultSet, resultRow, "srvlvl"), SRVLVL_LEN);

    /* Free SQL query result data set & return normally */

    sqlFreeResults (resultSet);

    return (eOK);
}

/* Translate DCS carrier code & service level into Parcel Manifest service code */

#ifdef MSG_HDR
    #undef MSG_HDR
#endif
#define MSG_HDR "DCSpm: (pmTranslateIntoServiceCode) "

long pmTranslateIntoServiceCode (char *service_code, 
				 char *carcod, char *srvlvl)
{
    mocaDataRes	    *resultSet;
    mocaDataRow	    *resultRow;

    char    buffer  [PM_BUFFER_LEN];
    char    service [PM_SERVICE_CODE_LEN + 1];

    long    status;

    /* Initialize local variables */

    memset (service, 0, sizeof (service));

    /* Format SQL query to find matching Carrier X-ref entry */

    sprintf (buffer,
	"select dstcar, dstlvl from carxrf "
	"where carcod='%s' and srvlvl='%s' "
	"order by carcod, srvlvl ",
	 carcod, srvlvl);

    /* Perform SQL query & retrieve pointer to 1st row */

    status = sqlExecStr (buffer, &resultSet);
    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
    {
        pmLogMsg (MSG_HDR, "Error %ld performing SQL query [%s]", status, buffer);
        sqlFreeResults (resultSet);
	return (status);
    }
    if (status == eDB_NO_ROWS_AFFECTED)
    {
        sqlFreeResults (resultSet);  /* Not found */
	return (status);
    }
    resultRow = sqlGetRow (resultSet);

    /* Retrieve matching Parcel Manifest service code */

    sprintf (service_code, "%s%c%s",
	pmGetString (resultSet, resultRow, "dstcar"),
	PM_SERVICE_DELIMITER,
	pmGetString (resultSet, resultRow, "dstlvl"));

    /* Free SQL query result data set & return normally */

    sqlFreeResults (resultSet);

    return (eOK);
}

/* Translate DCS package type into Parcel Manifest package type */ 

long pmTranslateIntoPackageType (char *package_type, char *paktyp)
{
    if (strcmp (paktyp, "BOX") == 0)
    {
	strcpy (package_type, PM_PAKTYP_BOX);
    }
    else if (strcmp (paktyp, "LET") == 0)
    {
	strcpy (package_type, PM_PAKTYP_LETTER);
    }
    else if (strcmp (paktyp, "PAK") == 0)
    {
	strcpy (package_type, PM_PAKTYP_PAK);
    }
    else if (strcmp (paktyp, "TUB") == 0)
    {
	strcpy (package_type, PM_PAKTYP_TUBE);
    }
    else
    {
	strcpy (package_type, PM_PAKTYP_CUSTOM);
    }

    return (eOK);
}

/* Verify if current mode is GUI initialization mode */

#ifdef MSG_HDR
    #undef MSG_HDR
#endif
#define MSG_HDR "DCSpm: (pmVerifyGuiInitialization) "

long pmVerifyGuiInitialization (char *carcod)
{
    char    buffer [PM_BUFFER_LEN];

    long    status;

    /* If carcod is undefined, then exit with false status */

    if (strlen (carcod) == 0)
    {
	return (FALSE);
    }

    /* Verify matching entry exists in carhdr table */


    sprintf (buffer,
	"select 'x' from carhdr where carcod = '%s'",
	carcod);

    status = sqlExecStr (buffer, NULL);
    if (status == eDB_NO_ROWS_AFFECTED)
    {
	pmLogMsg (MSG_HDR, "GUI Initialization Mode Detected");
	status = TRUE;
    }
    else
    {
	status = FALSE;
    }

    return (status);
}

/* Verify if multiple DCS carriers are mapped to single Parcel Manifest carrier */

#ifdef MSG_HDR
    #undef MSG_HDR
#endif
#define MSG_HDR "DCSpm: (pmVerifyMultipleDcsCarriers) "

long pmVerifyMultipleDcsCarriers (void)
{
    char    buffer [PM_BUFFER_LEN];

    long    status;

    /* Format SQL query */

    sprintf (buffer,
	"select distinct "
	"    dstcar "
	"from "
	"    carxrf a "
	"where exists ( "
	"        select "
	"            b.dstcar "
	"        from "
	"            carxrf b "
	"        where "
	"            b.dstnam =  a.dstnam and "
	"            b.dstcar =  a.dstcar and "
	"            b.carcod <> a.carcod "
	"    ) ");

    /* Perform SQL query */

    status = sqlExecStr (buffer, NULL);
    if ((status != eOK) && (status != eDB_NO_ROWS_AFFECTED))
    {
        pmLogMsg (MSG_HDR, "Error %ld performing SQL query [%s]", status, buffer);
	return (status);
    }

    /* Normal successful completion */

    return (status);
}
