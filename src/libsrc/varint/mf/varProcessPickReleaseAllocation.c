static char    *rcsid = "$Id: pckProcessPickReleaseAllocation.c,v 1.18.2.1 2004/12/06 19:21:34 lnecci Exp $";
/*#START***********************************************************************
*  RedPrairie Corporation
*  Copyright 2003
*  Waukesha, Wisconsin,  U.S.A.
*  All rights reserved.
*#END************************************************************************/

#include <moca_app.h>

#include <applib.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>
#include "pckRELlib.h"

moca_bool_t gCacheLocations = 0;

typedef struct {
	char ftpcod[FTPCOD_LEN+1];
	int  length;
	mocaDataRow *row;
} NODE_SEARCH_STRUCT; 

typedef struct _alloc_queue
{
	long processed;
	char schbat[SCHBAT_LEN+1];
	char srcare[ARECOD_LEN+1];
	char cmbcod[CMBCOD_LEN+1];
	char wrktyp[WRKTYP_LEN+1];
	char ship_id[SHIP_ID_LEN+1];
	char srcloc[STOLOC_LEN+1];
	char dstare[ARECOD_LEN+1];
	char dstloc[STOLOC_LEN+1];
	char wrkref[WRKREF_LEN+1];
	char lodlvl[LODLVL_LEN+1];
	char ftpcod[FTPCOD_LEN+1];
	long pckqty;
	long untcas;
	long seqnum;
	char pm_arecod[ARECOD_LEN+1];
	char pm_stoloc[STOLOC_LEN+1];
	char pm_rescod[RESCOD_LEN+1];
	char prtnum[PRTNUM_LEN+1];
	char prt_client_id[CLIENT_ID_LEN+1];
	char pm_rowid[100];
	char wkonum[WKONUM_LEN+1];
	char wkorev[WKOREV_LEN+1];
	char client_id[CLIENT_ID_LEN+1];

	/* space information */
	double piece_volume;
	double case_volume;
	double piece_length;
	double case_length;

	/* area props */
	moca_bool_t sigflg;
	moca_bool_t stgflg;
	char loccod[LOCCOD_LEN+1];

	/* calculated measures for cmbcod */
	long cmb_tot_pckqty;      /* pckqty for cmbcod */
	double cmb_space_needed;  /* resource space needed for cmbcod */
	moca_bool_t var_facflg;   /* new facility flag MCF */  
	long lane;
	char ship_lane_id[31];
	char ship_line_id[SHIP_LINE_ID_LEN+1];
	struct _alloc_queue *next;
} ALLOC_QUEUE;

typedef struct _location_list 
{
	long processed;

	char stoloc[STOLOC_LEN+1];
	char rescod[RESCOD_LEN+1];
	char ship_lane_ids[5000];
	double pndqvl;
	double curqvl;
	double maxqvl;
	double seqnum;
	moca_bool_t asgflg;
	char wrkzon[WRKZON_LEN+1];

	char pref_rescod[RESCOD_LEN+1];
	long pref_order;
	struct _location_list *next;
} LOCATION_LIST;

typedef struct _area_list
{
	char arecod[ARECOD_LEN+1];
	LOCATION_LIST *Locations;

	struct _area_list *next;
} AREA_LIST;

typedef struct _space_reqs
{
	char arecod[ARECOD_LEN+1];
	char rescod[RESCOD_LEN+1];
	char cmp_value[RESCOD_LEN+ARECOD_LEN+1];
	char ship_lane_ids[5000];

	double total_space_needed;
	double total_space_used;
	struct _space_reqs *next;
} SPACE_REQS;

#define HASH_SIZE 563 /* prime number */

static SPACE_REQS *SpaceRequiredTable[HASH_SIZE];

static long sGenHashKey(char *cmp_value)
{
	register char *cp;
	register unsigned long hash_sum;

	for (hash_sum = 0, cp = cmp_value; *cp; cp++)
	{
		hash_sum += (hash_sum * 13) + *cp;
	}
	return(hash_sum % HASH_SIZE);
}

static void sFreeSpaceReqs(SPACE_REQS *sp)
{
	SPACE_REQS *last_sp;

	pckrel_WriteTrc("        EM:ID sFreeSpaceReqs" );

	last_sp = NULL;
	for (; sp; last_sp = sp, sp = sp->next)
	{
		if (last_sp)
			free(last_sp);
	}
	if (last_sp)
		free(last_sp);

	return;
}

static void sFreeSpaceRequiredTable()
{
	long i;

	for (i = 0; i < HASH_SIZE; i++)
	{
		if (SpaceRequiredTable[i])
			sFreeSpaceReqs(SpaceRequiredTable[i]);
		SpaceRequiredTable[i] = NULL;
	}
	return;
}

static SPACE_REQS *sGetHashEntry(char *arecod,
								 char *rescod)
{
	SPACE_REQS *sp, *last_sp;
	long hash_index;
	char cmp_value[RESCOD_LEN+ARECOD_LEN+1];

	memset(cmp_value, 0, sizeof(cmp_value));
	strcat(cmp_value, rescod);
	strcat(cmp_value, arecod);

	hash_index = sGenHashKey(cmp_value);
	sp = SpaceRequiredTable[hash_index];

	if (!sp)
	{
		sp = (SPACE_REQS *) calloc(1, sizeof(SPACE_REQS));
		strcpy(sp->arecod, arecod);
		strcpy(sp->rescod, rescod);
		strcpy(sp->cmp_value, cmp_value);

		SpaceRequiredTable[hash_index] = sp;
		return(sp);
	}

	for (last_sp = NULL; sp; last_sp = sp, sp = sp->next)
	{
		if (strcmp(cmp_value, sp->cmp_value) == 0)
			return(sp);
	}
	sp = (SPACE_REQS *) calloc(1, sizeof(SPACE_REQS));
	strcpy(sp->arecod, arecod);
	strcpy(sp->rescod, rescod);
	strcpy(sp->cmp_value, cmp_value);

	pckrel_WriteTrc("     Hash table:  added chain for cmpval: %s,"
		" hash_index: %d",
		cmp_value, hash_index);
	last_sp->next = sp;
	return(sp);
}

static void sAddToSpaceRequired(char *arecod,
								char *rescod,
								double cmb_space_needed, char *ship_lane_id)
{
	SPACE_REQS *sp;

	pckrel_WriteTrc("        EM:ID sAddToSpaceRequired" );
	pckrel_WriteTrc("        EM: ShipLaneID is %s", ship_lane_id );
	pckrel_WriteTrc("        EM: adding %f to needed", cmb_space_needed);
	pckrel_WriteTrc("        EM: arecod: %s, rescod: %s", arecod, rescod );
	sp = sGetHashEntry(arecod, rescod);
	if (ship_lane_id && strlen(ship_lane_id))    
	{        
		pckrel_WriteTrc("        EM: checking ship_lane_ids" );
		if (!strstr(sp->ship_lane_ids, ship_lane_id)) 	
		{	
			pckrel_WriteTrc("        EM: AddToSpaceReq: 1 " );
			sp->total_space_needed++;
			strcat(sp->ship_lane_ids, "-");	
			strcat(sp->ship_lane_ids, ship_lane_id);	
		}    
	}
	else {
		pckrel_WriteTrc("        EM: AddToSpaceReq: %f", cmb_space_needed );
		sp->total_space_needed += cmb_space_needed;
	}

	pckrel_WriteTrc("        EM: AddToSpceReq total needed is: %f", sp->total_space_needed );
	return;
}

static void sAddToSpaceUsed(char *arecod,
							char *rescod,
							double space_used)
{
	SPACE_REQS *sp;

	pckrel_WriteTrc("        EM:ID sAddToSpaceUsed" );
	pckrel_WriteTrc("        EM: arecod: %s, rescod: %s", arecod, rescod );
	pckrel_WriteTrc("        EM: AddToSpaceUsed: %f", space_used );

	sp = sGetHashEntry(arecod, rescod);

	pckrel_WriteTrc("        EM: FoundArecod: %s, FoundRescod: %s", sp->arecod, sp->rescod );
	pckrel_WriteTrc("        EM: AddToSpaceUsed, orig space used: %f", sp->total_space_used );

	sp->total_space_used += space_used;

	pckrel_WriteTrc("        EM: TotalUsed is now: %f", sp->total_space_used );

	return;
}

static void sFreeAllocationQueue(ALLOC_QUEUE **Queue)
{
	ALLOC_QUEUE *qp, *last_qp;

	last_qp = NULL;
	for (qp = *Queue; qp; last_qp = qp, qp = qp->next)
	{
		if (last_qp)
		{
			free(last_qp);
		}
	}
	if (last_qp)
	{
		free(last_qp);
	}
	*Queue = NULL;
	return;
}

/*
* sLoadAllocationQueue:
*
* This routine is used to load up all picks needing to be allocated
* for pick release.  It is used by the main-line
* pckProcessPickReleaseAllocation routine to load a queue which
* is traversed throughout the rest of this routine.
* 
* Information passed to this function is used to pass along to 
* the component: get pick release allocation queue which returns
* the ordered list of picks to process.  The Queue variable is
* written by this routine.  It points to a linked list of picks -
* which should be freed upon mainline exit.
*/
static long sLoadAllocationQueue(ALLOC_QUEUE **Queue,
								 long *looking_specific,
								 char *cmbcod_i,
								 char *ship_id_i,
								 char *wrkref_i,
								 char *pcksts,
								 char *schbat)
{
	char cmbcod[CMBCOD_LEN+1];
	char ship_id[SHIP_ID_LEN+1];
	char wrkref[WRKREF_LEN+1];

	char whereclause[1000];
	char tmpbuf[200];
	char buffer[5000];
	long ret_status;

	RETURN_STRUCT *CmdRes;
	mocaDataRow *row;
	mocaDataRes *res;
	ALLOC_QUEUE *head, *last, *qp;

	*Queue = NULL;
	head = NULL;
	last = NULL;

	memset(whereclause, 0, sizeof(whereclause));

	pckrel_WriteTrc("   EM: in sLoadAllocationQueue!" );

	if (cmbcod_i && misTrimLen(cmbcod_i, CMBCOD_LEN))
	{
		memset(cmbcod, 0, sizeof(cmbcod));
		misTrimcpy(cmbcod, cmbcod_i, CMBCOD_LEN);
		sprintf(tmpbuf, " and cmbcod = '%s' ", cmbcod);
		strcat(whereclause, tmpbuf);
	}
	if (ship_id_i && misTrimLen(ship_id_i, SHIP_ID_LEN))
	{
		memset(ship_id, 0, sizeof(ship_id));
		misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);
		sprintf(tmpbuf, " and ship_id = '%s' ", ship_id);
		strcat(whereclause, tmpbuf);
	}
	if (wrkref_i && misTrimLen(wrkref_i, WRKREF_LEN))
	{
		memset(wrkref, 0, sizeof(wrkref));
		misTrimcpy(wrkref, wrkref_i, WRKREF_LEN);
		sprintf(tmpbuf, " and wrkref = '%s' ", wrkref);
		strcat(whereclause, tmpbuf);
	}

	if (strlen(whereclause))
		*looking_specific = 1;

	sprintf(buffer,
		"get pick release allocation queue "
		" where pcksts = \"%s\" "
		"   and schbat = \"%s\" "
		"   %s ",
		pcksts, 
		schbat,
		strlen(whereclause) == 0 ? "" : whereclause);

	ret_status = srvInitiateCommand(buffer, &CmdRes);
	if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	{
		srvFreeMemory(SRVRET_STRUCT, CmdRes);
		return(ret_status);
	}

	if (ret_status != eOK)
	{
		srvFreeMemory(SRVRET_STRUCT, CmdRes);
		if (*looking_specific)
		{
			pckrel_WriteTrc("Didn't find any pick work/move on "
				"specific read, returning error ...");
			return(ret_status);
		}
		pckrel_WriteTrc("  ...there are currently no"
			" pick works to process! (ms=%ld)", 
			pckrel_ElapsedMs());
		return(eOK);
	}
	/* Shucks...we've got some real work to do...load up the
	* structure... 
	*/
	res = srvGetResults(CmdRes);

	pckrel_WriteTrc("  Read %ld outstanding pick work/move "
		"entries for %s (ms=%ld)",
		sqlGetNumRows(res),
		schbat,
		pckrel_ElapsedMs());

	for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	{
		qp = (ALLOC_QUEUE *)calloc(1, sizeof(ALLOC_QUEUE));
		if (qp == NULL)
		{
			srvFreeMemory(SRVRET_STRUCT, CmdRes);
			pckrel_WriteTrc("  ...unable to load queue - no memory!");
			return(eNO_MEMORY);
		}

		if (!sqlIsNull(res, row, "schbat"))
			strncpy(qp->schbat, sqlGetString(res, row, "schbat"), SCHBAT_LEN);
		if (!sqlIsNull(res, row, "srcare"))
			strncpy(qp->srcare, sqlGetString(res, row, "srcare"), ARECOD_LEN);
		if (!sqlIsNull(res, row, "cmbcod"))
			strncpy(qp->cmbcod, sqlGetString(res, row, "cmbcod"), CMBCOD_LEN);
		if (!sqlIsNull(res, row, "wrktyp"))
			strncpy(qp->wrktyp, sqlGetString(res, row, "wrktyp"), WRKTYP_LEN);
		if (!sqlIsNull(res, row, "ship_id"))
			strncpy(qp->ship_id, sqlGetString(res, row, "ship_id"), SHIP_ID_LEN);
		if (!sqlIsNull(res, row, "srcloc"))
			strncpy(qp->srcloc, sqlGetString(res, row, "srcloc"), STOLOC_LEN);
		if (!sqlIsNull(res, row, "dstare"))
			strncpy(qp->dstare, sqlGetString(res, row, "dstare"), ARECOD_LEN);
		if (!sqlIsNull(res, row, "dstloc"))
			strncpy(qp->dstloc, sqlGetString(res, row, "dstloc"), STOLOC_LEN);
		if (!sqlIsNull(res, row, "wrkref"))
			strncpy(qp->wrkref, sqlGetString(res, row, "wrkref"), WRKREF_LEN);
		if (!sqlIsNull(res, row, "lodlvl"))
			strncpy(qp->lodlvl, sqlGetString(res, row, "lodlvl"), LODLVL_LEN);
		if (!sqlIsNull(res, row, "ftpcod"))
			strncpy(qp->ftpcod, sqlGetString(res, row, "ftpcod"), FTPCOD_LEN);
		if (!sqlIsNull(res, row, "ship_line_id"))            
			strncpy(qp->ship_line_id, sqlGetString(res, row, "ship_line_id"), SHIP_LINE_ID_LEN);

		qp->lane = sqlGetLong(res, row, "lane");
		qp->pckqty = sqlGetLong(res, row, "pckqty");
		qp->untcas = sqlGetLong(res, row, "untcas");
		qp->seqnum = sqlGetLong(res, row, "seqnum");

		if (!sqlIsNull(res, row, "pm_arecod"))
			strncpy(qp->pm_arecod, 
			sqlGetString(res, row, "pm_arecod"), ARECOD_LEN);
		if (!sqlIsNull(res, row, "pm_stoloc"))
			strncpy(qp->pm_stoloc, 
			sqlGetString(res, row, "pm_stoloc"), STOLOC_LEN);
		if (!sqlIsNull(res, row, "pm_rescod"))
			strncpy(qp->pm_rescod,
			sqlGetString(res, row, "pm_rescod"), RESCOD_LEN);
		if (!sqlIsNull(res, row, "prtnum"))
			strncpy(qp->prtnum, sqlGetString(res, row, "prtnum"), PRTNUM_LEN);
		if (!sqlIsNull(res, row, "prt_client_id"))
			strncpy(qp->prt_client_id, 
			sqlGetString(res, row, "prt_client_id"), CLIENT_ID_LEN);
		if (!sqlIsNull(res, row, "pm_rowid"))
			strncpy(qp->pm_rowid, 
			sqlGetString(res, row, "pm_rowid"), 100);
		if (!sqlIsNull(res, row, "wkonum"))
			strncpy(qp->wkonum, sqlGetString(res, row, "wkonum"), WKONUM_LEN);
		if (!sqlIsNull(res, row, "wkorev"))
			strncpy(qp->wkorev, sqlGetString(res, row, "wkorev"), WKOREV_LEN);
		if (!sqlIsNull(res, row, "client_id"))
			strncpy(qp->client_id, 
			sqlGetString(res, row, "client_id"), CLIENT_ID_LEN);

		if (!sqlIsNull(res, row, "piece_volume"))
			qp->piece_volume = sqlGetFloat(res, row, "piece_volume");
		if (!sqlIsNull(res, row, "case_volume"))
			qp->case_volume = sqlGetFloat(res, row, "case_volume");
		if (!sqlIsNull(res, row, "piece_length"))
			qp->piece_length = sqlGetFloat(res, row, "piece_length");
		if (!sqlIsNull(res, row, "case_length"))
			qp->case_length = sqlGetFloat(res, row, "case_length");

		qp->var_facflg = 0;
		if (!sqlIsNull(res, row, "var_facflg"))
			qp->var_facflg = sqlGetBoolean(res, row, "var_facflg");


		if (qp->var_facflg == 0 && qp->lane > 0 && qp->lodlvl[0] == 'S')
		{
			strcpy(qp->ship_lane_id, qp->ship_id);
			sprintf(tmpbuf, "%d", qp->lane);
			strcat(qp->ship_lane_id, tmpbuf);
		}

		if (!sqlIsNull(res, row, "sigflg"))
			qp->sigflg = sqlGetBoolean(res, row, "sigflg");
		if (!sqlIsNull(res, row, "stgflg"))
			qp->stgflg = sqlGetBoolean(res, row, "stgflg");
		if (!sqlIsNull(res, row, "loccod"))
			strncpy(qp->loccod, sqlGetString(res, row, "loccod"), LOCCOD_LEN);

		if (last == NULL)
		{
			head = qp;
			last = qp;
		}
		else
		{
			last->next = qp;
			last = qp;
		}
	}
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	*Queue = head;
	return(eOK);
}

/*
* This routine is used set all location preferences for a set 
* of locations listed in a given area.  It performs its work
* based off the return of the "get pick release location preferences"
* component.  This routine allows us to send picks to staging lanes
* which match up with where an appointment is scheduled or a truck is
* already arrived.
*/
static void sSetLocationPreferences(LOCATION_LIST *Locations,
									char *arecod, char *ship_id)
{
	long ret_status;
	RETURN_STRUCT *CmdRes;
	mocaDataRow *row;
	mocaDataRes *res;
	char stoloc[STOLOC_LEN+1];
	char rescod[RESCOD_LEN+1];
	LOCATION_LIST *lp;

	ret_status = 
		srvInitiateInlineFormat(&CmdRes,
		"get pick release location preferences"
		" where arecod = '%s' "
		"   and ship_id = '%s' ",
		arecod, ship_id);

	if (ret_status != eOK)
	{
		srvFreeMemory(SRVRET_STRUCT, CmdRes);
		return;
	}

	res = srvGetResults(CmdRes);
	for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
	{
		memset(stoloc, 0, sizeof(stoloc));
		memset(rescod, 0, sizeof(rescod));

		strncpy(stoloc, sqlGetString(res, row, "stoloc"), STOLOC_LEN);
		strncpy(rescod, sqlGetString(res, row, "rescod"), RESCOD_LEN);

		for (lp = Locations; lp; lp = lp->next)
		{
			if (strncmp(lp->stoloc, stoloc, STOLOC_LEN) == 0)
			{
				strncpy(lp->pref_rescod, rescod, RESCOD_LEN);
				if (strlen(lp->rescod) > 0 &&
					strncmp(lp->rescod, "^^^^", 4) != 0 &&
					strncmp(lp->rescod, rescod, RESCOD_LEN) != 0)
				{
					pckrel_WriteTrc("   ** Note ** Loc %s is preferred for "
						"rescod: %s, ",
						lp->stoloc, lp->pref_rescod);
					pckrel_WriteTrc("              but currently assigned "
						"rescod: %s ",
						lp->rescod);
				}
				break;
			}
		}
	}
	srvFreeMemory(SRVRET_STRUCT, CmdRes);
	return;
}
/*
* This routine is used to build the list of locations available for
* use in a given area. 
*/
static long sGetLocationListForArea(char *arecod,
									char *order_by,
									char *ship_id,
									AREA_LIST **AreaListHead,
									LOCATION_LIST **ret_loc)
{
	char buffer[1000];
	long ret_status;
	RETURN_STRUCT *CmdRes;
	mocaDataRow *row;
	mocaDataRes *res;
	DATA_STRUCT *data;
	RULES_STRUCT *rules;

	LOCATION_LIST *lp, *last_lp;
	AREA_LIST *ap, *previous;

	pckrel_WriteTrc("EM: In sGetLocationListForArea!");
	pckrel_WriteTrc("EM: arecod: %s, order_by: %s, ship_id: %s", arecod, order_by, ship_id);

	ret_status = pckrel_GetConfig(&data, &rules, 0);
	if (ret_status != eOK)
		return(0);

	/* To allow use of dock door associations for assigning pick 
	release locations the system must be configured to always
	reload the location list due to changing date that will
	change the preferential location availability.  We will 
	only always reload the location list for shippable picks 
	since dock associations only work for shipment staging 
	locations now.  This could be a performance hit in sites 
	with lots of picks to release in which case this policy 
	can be turned off */

	previous = NULL;
	for (ap = *AreaListHead; ap; ap = ap->next)
	{   
		if (strncmp(ap->arecod, arecod, ARECOD_LEN) == 0)
		{
			if(rules->reloadLocs == BOOLEAN_TRUE && ship_id != NULL)
			{
				/* We need to remove the area from the list
				* and reload it to get refreshed data for the
				* location preferences.  After we remove the node
				* we drop out of the for loop to continue on with
				* to get the data again for the area */

				/*this should handle the first and only case*/
				if (ap == *AreaListHead)
				{
					/* Remove First Node */
					*AreaListHead = ap->next;                   
				}
				/*this should handle the middle and last case*/
				else
				{
					/*  Remove Middle or Last Node */
					previous->next = ap->next;                  
				}
				/*Now we need to free the locations inside the area list*/
				for (lp = ap->Locations; lp; last_lp = lp, lp = lp->next)
				{
					if (last_lp)
						free(last_lp);
				}
				free(ap);
				last_lp = NULL;
				lp = NULL;
			}
			else
			{
				/* If we are not configured to always reload the list for
				* shipment destination areas (i.e. not using preferred locations)
				* then we just return the list from the firs time the area was
				* loaded */
				*ret_loc = ap->Locations;
				return(eOK);
			}
		}
		previous = ap;   
	}

	/* Didn't find it, or we need to always need to reload - go 
	ahead and load */
	pckrel_WriteTrc("EM: Reloading Locations . . .");

	sprintf(buffer,
		"get pick release locations for area "
		" where arecod = '%s'"
		"   and order_by = '%s' and ship_id = '%s'",
		arecod, order_by, ship_id);

	CmdRes = NULL;
	ret_status = srvInitiateCommand(buffer, &CmdRes);
	if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	{
		srvFreeMemory(SRVRET_STRUCT, CmdRes);
		pckrel_WriteTrc("EM: Error (%d) loading locations for area: %s",
			ret_status, arecod);
		*ret_loc = NULL;
		return(ret_status);
	}

	ap = (AREA_LIST *) calloc(1, sizeof(AREA_LIST));
	if (ap == NULL)
	{
		srvFreeMemory(SRVRET_STRUCT, CmdRes);
		pckrel_WriteTrc("EM: Error allocating memory for area list");
		*ret_loc = NULL;
		return(eNO_MEMORY);
	}
	strncpy(ap->arecod, arecod, ARECOD_LEN);

	ap->next = *AreaListHead;
	*AreaListHead = ap;


	/* ret_status may be "no rows affected" here, so if that is the
	* case, we just want to maintain an area entry so we don't sit
	* and bash the db
	*/
	if (ret_status == eOK)
	{
		res = srvGetResults(CmdRes);
		last_lp = NULL;
		for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
		{
			lp = (LOCATION_LIST *)calloc(1, sizeof(LOCATION_LIST));
			if (lp == NULL)
			{
				pckrel_WriteTrc("EM: Error allocating memory for location list");
				*ret_loc = NULL;
				srvFreeMemory(SRVRET_STRUCT, CmdRes);
				return(eNO_MEMORY);
			}
			strncpy(lp->stoloc,
				sqlGetString(res, row, "stoloc"), STOLOC_LEN);
			if (sqlIsNull(res, row, "rescod"))
			{
				memset(lp->rescod, '^', RESCOD_LEN);
			}
			else
			{
				strncpy(lp->rescod,
					sqlGetString(res, row, "rescod"), RESCOD_LEN);
			}
			strncpy(lp->wrkzon, 
				sqlGetString(res, row, "wrkzon"), WRKZON_LEN);
			lp->maxqvl = sqlGetFloat(res, row, "maxqvl");
			lp->curqvl = sqlGetFloat(res, row, "curqvl");
			lp->pndqvl = sqlGetFloat(res, row, "pndqvl");
			lp->asgflg = sqlGetBoolean(res, row, "asgflg");
			lp->seqnum = sqlGetFloat(res, row, "trvseq");

			pckrel_WriteTrc("EM: loaded loc: %s, rescod: %s, maxqvl: %f, curqvl: %f, pndqvl: %f, asgflg: %ld, wrkzon: %s, seqnum %f",
				lp->stoloc,
				lp->rescod,
				lp->maxqvl,
				lp->curqvl,
				lp->pndqvl,
				lp->asgflg,
				lp->wrkzon,
				lp->seqnum);

			if (last_lp)
			{
				last_lp->next = lp;
			}
			else
			{
				ap->Locations = lp;
			}
			last_lp = lp;
		}
	}
	srvFreeMemory(SRVRET_STRUCT, CmdRes);

	sSetLocationPreferences(ap->Locations, arecod, ship_id);

	*ret_loc = ap->Locations;
	return(eOK);
}

static void sFreeLocationLists(AREA_LIST **AreaListHead)
{
	AREA_LIST *ap, *last_ap;
	LOCATION_LIST *lp, *last_lp;

	/* If caching is turned on...don't bother with the free */
	if (gCacheLocations)
		return;

	misTrc(T_FLOW, "Freeing the location lists");

	last_ap = NULL;
	for (ap = *AreaListHead; ap; last_ap = ap, ap = ap->next)
	{
		if (last_ap)
			free(last_ap);

		last_lp = NULL;
		for (lp = ap->Locations; lp; last_lp = lp, lp = lp->next)
		{
			if (last_lp)
				free(last_lp);
		}
		if (last_lp)
			free(last_lp);
	}
	if (last_ap)
		free(last_ap);

	*AreaListHead = NULL;
	return;
}


static double   sCalculateFtpInvVolume(ALLOC_QUEUE *qp)
{ 
	double total_volume; 
	long num_full_cases; 
	long num_partial_cases; 
	double total_cases; 
	char lodlvl[LODLVL_LEN+1]; 

	/* If the load level is at a load or subload level 
	Then we will assume the case foot print is used 
	for the left over units (i.e. less than a full case 
	is still sitting in the same size case. 

	At the detail level we will first attempt to use 
	the untlen, untwid, unthgt values. If they are 
	not there or are zero, we will use the case values 
	and divide the untqty by the untcas and then multiply 
	by the case volume. 
	/* ********************** */ 

	memset(lodlvl, 0, sizeof(lodlvl)); 
	misTrimcpy(lodlvl, qp->lodlvl, LODLVL_LEN);

	/* Calculate the number of cases.  */ 
	num_full_cases = qp->pckqty / qp->untcas;

	num_partial_cases = (qp->pckqty % qp->untcas) ? 1 : 0;

	total_cases = (double) qp->pckqty / (double) qp->untcas;

	if (strncmp(lodlvl, LODLVL_DETAIL, LODLVL_LEN)==0)
	{ 
		if (qp->piece_volume > 0)
		{ 

			total_volume = 
				(double) qp->pckqty * qp->piece_volume;
		} 
		else
		{ 
			/* This uses the number of full cases plus the fractional 
			case value. */ 
			total_volume = total_cases * qp->case_volume;
		} 
	} 
	else
	{ 
		/* This uses the number of full cases plus one for the fraction 
		of a case. */ 
		total_volume = 
			((double)num_full_cases + (double)num_partial_cases) 
			* qp->case_volume;
	} 

	return(total_volume); 

} 

static double   sCalculateFtpInvLength(ALLOC_QUEUE *qp)
{ 
	double total_length; 
	long num_full_cases; 
	long num_partial_cases; 
	double total_cases; 
	char lodlvl[LODLVL_LEN+1]; 

	/* If the load level is at a load or subload level 
	Then we will assume the case foot print is used 
	for the left over units (i.e. less than a full case 
	is still sitting in the same size case. 

	At the detail level we will first attempt to use 
	the untlen value. If it is not there or it is zero,  
	we will use the case value and divide the untqty by  
	the untcas and then multiply by the case length. 
	/* ********************** */ 

	memset(lodlvl, 0, sizeof(lodlvl)); 
	misTrimcpy(lodlvl, qp->lodlvl, LODLVL_LEN); 

	/* Calculate the number of cases.  */ 
	num_full_cases = qp->pckqty / qp->untcas;

	num_partial_cases = (qp->pckqty % qp->untcas) ? 1 : 0;

	total_cases = (double) qp->pckqty / (double) qp->untcas;

	if (strncmp(lodlvl, LODLVL_DETAIL, LODLVL_LEN)==0)
	{ 
		if (qp->piece_length > 0)
		{ 
			total_length = (double) qp->pckqty * qp->piece_length;
		} 
		else
		{ 
			total_length = total_cases * qp->case_length;
		} 
	} 
	else
	{ 
		total_length = ((double)num_full_cases + (double)num_partial_cases) 
			* qp->case_length;
	} 
	return(total_length); 
}
static void sSetCmbcodSpace(ALLOC_QUEUE *start_qp, 
							ALLOC_QUEUE *last_qp, 
							long pckqty,
							double space_needed)
{
	ALLOC_QUEUE *qp;

	qp = start_qp; 
	do 
	{
		qp->cmb_tot_pckqty = pckqty;
		qp->cmb_space_needed = space_needed;

		qp = qp->next;
	} while (qp != last_qp && qp);

	return;
}

static ALLOC_QUEUE *sCalculateSpaceForCmbcod(ALLOC_QUEUE *qp)
{
	register ALLOC_QUEUE *start_qp, *last_qp;
	long pckqty;
	double space_needed;
	DATA_STRUCT *data;
	RULES_STRUCT *rules;

	pckrel_GetConfig(&data, &rules, 0);

	pckqty = 0;
	space_needed = 0;
	start_qp = qp;
	last_qp = qp;
	for (; qp; last_qp = qp, qp = qp->next)
	{
		if (strcmp(qp->pm_rescod, start_qp->pm_rescod) != 0 ||
			strcmp(qp->pm_arecod, start_qp->pm_arecod) != 0 ||
			strcmp(qp->cmbcod, start_qp->cmbcod) != 0)
		{  
			/* we're all done */
			pckrel_WriteTrc("        EM: SpaceNeeded is  %f", space_needed );
			sSetCmbcodSpace(start_qp, 
				last_qp, 
				pckqty,
				space_needed);
			return(qp);
		}
		/* Still the same cmbcod, rescod, and arecod */
		pckqty += qp->pckqty;


		pckrel_WriteTrc("        EM: loccod is %s, lodlvl is %s", qp->loccod, qp->lodlvl );
		pckrel_WriteTrc("        EM: PALloccod is %s, PALlodlvl is %s", LOCCOD_PALLET, LODLVL_LOAD );

		if (strncmp(qp->loccod, LOCCOD_PALLET, LOCCOD_LEN) == 0)
		{
			if (strncmp(qp->lodlvl, LODLVL_LOAD, LODLVL_LEN) == 0)
			{
				pckrel_WriteTrc("        EM: Pallet Pick" );
				space_needed = 1.0;
			}
			else 
			{
				pckrel_WriteTrc("        EM: Pallet Pick else block" );
				space_needed += sCalculateFtpInvVolume(qp) / rules->palvol;
			}
		}
		else if (strncmp(qp->loccod, LOCCOD_LENGTH, LOCCOD_LEN) == 0)
		{
			pckrel_WriteTrc("        EM: Non Pallet Pick" );
			space_needed += sCalculateFtpInvLength(qp);
		}
		else
		{
			pckrel_WriteTrc("        EM: Non Pallet Pick else block" );
			space_needed += sCalculateFtpInvVolume(qp);
		}
	}

	pckrel_WriteTrc("        EM: SpaceNeeded is  %f", space_needed );
	sSetCmbcodSpace(start_qp, 
		last_qp, 
		pckqty,
		space_needed);
	return(NULL);
}
static void sSetResourceSpaceRequirements(ALLOC_QUEUE *qp)
{
	ALLOC_QUEUE *tmp_qp;

	while (qp)
	{

		pckrel_WriteTrc("        EM:ID sSetResourceSpaceRequirements %s", qp->cmbcod);
		pckrel_WriteTrc("        EM: rescod is %s, arecod is %s", qp->pm_rescod, qp->pm_arecod);
		tmp_qp = qp;
		qp = sCalculateSpaceForCmbcod(qp);
		sAddToSpaceRequired(tmp_qp->pm_arecod,
			tmp_qp->pm_rescod,
			tmp_qp->cmb_space_needed, tmp_qp->ship_lane_id);
	}
	return;
}
static void sGetSpaceReqs(ALLOC_QUEUE *qp,
						  double *cmb_space_needed,
						  double *con_space_needed,
						  long *cmb_tot_pckqty)
{
	SPACE_REQS *sp;

	pckrel_WriteTrc("        EM:ID sGetSpaceReqs" );

	sp = sGetHashEntry(qp->pm_arecod,
		qp->pm_rescod);

	pckrel_WriteTrc("        EM:  arecod: %s, rescod %s ", qp->pm_arecod, qp->pm_rescod );
	pckrel_WriteTrc("        EM:  sp->total_pace_needed: %f", sp->total_space_needed );
	pckrel_WriteTrc("        EM:  sp->total_pace_used: %f", sp->total_space_used );

	*cmb_space_needed = qp->cmb_space_needed;
	*con_space_needed = sp->total_space_needed - sp->total_space_used;
	*cmb_tot_pckqty = qp->cmb_tot_pckqty;

	pckrel_WriteTrc("        EM: setting cmb_space_needed to %f", *cmb_space_needed );
	pckrel_WriteTrc("        EM: setting con_space_needed to %f", *con_space_needed );
	pckrel_WriteTrc("        EM: setting cmb_tot_pckqty to %ld", *cmb_tot_pckqty );

	return;
}

static int  sRequiresSameSrcZone(char *src_arecod,
								 char *dst_arecod,
								 char *src_lodlvl,
								 char *fin_arecod)
{
	long            ii;
	short           found_it;
	DATA_STRUCT *data;
	RULES_STRUCT *rules;
	long ret_status;

	ret_status = pckrel_GetConfig(&data, &rules, 0);
	if (ret_status != eOK)
		return(0);

	ii = rules->move_path_start;
	found_it = FALSE;
	misTrim(src_lodlvl);
	misTrim(src_arecod);
	misTrim(dst_arecod);
	misTrim(fin_arecod);
	while (ii < rules->rel_size && found_it == FALSE)
	{
		if (strncmp(rules->rel[ii].wrktyp, "**********", WRKTYP_LEN) == 0 &&
			strcmp(src_lodlvl, rules->rel[ii].lodlvl) == 0 &&
			((strcmp(src_arecod, rules->rel[ii].srcare) == 0 &&
			strcmp(dst_arecod, rules->rel[ii].dstare) == 0) ||
			(strcmp(dst_arecod, rules->rel[ii].srcare) == 0 &&
			strcmp(src_arecod, rules->rel[ii].dstare) == 0)) &&
			strcmp(fin_arecod, rules->rel[ii].finare) == 0)
			found_it = TRUE;
		else
			ii++;
	}
	/* If we didn't find it then lets default to same zone not needed */
	if (found_it == FALSE)
		return (FALSE);

	/* If this is a same zone movement, then return true ... */
	if (strstr(rules->rel[ii].rtstr2, "SAME-ZONE"))
		return (TRUE);


	return (FALSE);
}

static int sRequiresSameDstZone(char *src_arecod,
								char *dst_arecod,
								char *src_lodlvl,
								char *fin_arecod)
{
	long            ii;
	short           found_it;
	DATA_STRUCT *data;
	RULES_STRUCT *rules;
	long ret_status;

	ret_status = pckrel_GetConfig(&data, &rules, 0);
	if (ret_status != eOK)
		return(0);

	ii = rules->move_path_start;
	found_it = FALSE;
	misTrim(src_lodlvl);
	misTrim(src_arecod);
	misTrim(dst_arecod);
	misTrim(fin_arecod);
	while (ii < rules->rel_size && found_it == FALSE)
	{
		if (strncmp(rules->rel[ii].wrktyp, "**********", WRKTYP_LEN) == 0 &&
			strcmp(src_lodlvl, rules->rel[ii].lodlvl) == 0 &&
			((strcmp(src_arecod, rules->rel[ii].srcare) == 0 &&
			strcmp(dst_arecod, rules->rel[ii].dstare) == 0) ||
			(strcmp(dst_arecod, rules->rel[ii].srcare) == 0 &&
			strcmp(src_arecod, rules->rel[ii].dstare) == 0)) &&
			strcmp(fin_arecod, rules->rel[ii].finare) == 0)
			found_it = TRUE;
		else
			ii++;
	}
	/* If we didn't find it then lets default to same zone not needed */
	if (found_it == FALSE)
		return (FALSE);

	/* If this is a same zone movement, then return true ... */
	if (strcmp(rules->rel[ii].wrktyp, rules->rel[ii + 1].wrktyp) == 0 &&
		strcmp(rules->rel[ii].lodlvl, rules->rel[ii + 1].lodlvl) == 0 &&
		strcmp(rules->rel[ii].dstare, rules->rel[ii + 1].srcare) == 0 &&
		strcmp(rules->rel[ii].finare, rules->rel[ii + 1].finare) == 0 &&
		(int) strstr(rules->rel[ii + 1].rtstr2,
		"SAME-ZONE-SECOND") > 0)
		return (TRUE);


	return (FALSE);
}
/*
* Get the "dest loc" of the pick.  Return either the pckwrk.dstloc 
* or get the next loc in the sequence.
*/

static char *sGetDestLoc(ALLOC_QUEUE *qp)
{
	if (!qp)
		return "";

	if (strlen(qp->dstloc) != 0)
	{
		return(qp->dstloc);
	}

	if (qp->next &&
		strlen(qp->next->pm_stoloc) != 0 &&
		strncmp(qp->cmbcod, qp->next->cmbcod, CMBCOD_LEN) == 0)
	{
		return(qp->next->pm_stoloc);
	}

	return "";
}
static long sGetLocationWrkzon(char *stoloc,
							   char *wrkzon_o)
{
	long ret_status;
	char buffer[1000];
	mocaDataRes *res;
	mocaDataRow *row;

	sprintf(buffer,
		"select wrkzon "
		"  from locmst "
		" where stoloc = '%s' ",
		stoloc);
	ret_status = sqlExecStr(buffer, &res);
	if (ret_status != eOK)
	{
		sqlFreeResults(res);
		pckrel_WriteTrc("Error getting "
			"zone, sts: %d"
			" - SKIPPING",
			ret_status);
		return(ret_status);
	}
	row = sqlGetRow(res);
	strncpy(wrkzon_o, sqlGetString(res, row, "wrkzon"), WRKZON_LEN);
	sqlFreeResults(res);

	return(eOK);
}
/*
* sEvaluateBestFit
*
* This function is called to evaluate the best match for resource 
* locations.  This will handle rules
*
*/

static void sEvaluateBestFit(LOCATION_LIST **best_alloc_lp,
							 LOCATION_LIST *lp,
							 double con_space_needed,
							 moca_bool_t stgflg,
							 long small_lane_size)
{

	pckrel_WriteTrc("        EM: Evaluating %s maxqvl = %f curqvl = %f pndqvl = %f", 
		lp->stoloc, lp->maxqvl, lp->curqvl, lp->pndqvl);
	pckrel_WriteTrc("        EM: Needed: %f  Small Lane Size: %ld", con_space_needed, small_lane_size);

	if (stgflg &&
		(lp->maxqvl < small_lane_size))
	{
		/* 
		* For small staging lanes, we don't want to 
		* burn up any more than 2 lanes for a single 
		* rescod allocation.  Therefore, the maxqvl on 
		* a small staging lane needs to be >= 1/2 of the
		* space of what is required for the resource code.
		*
		* As confusing as this comparison seems - it is correct.
		* The point is that we don't want a "small lanes" to 
		* get chewed up by one shipment:  I.e. we want to use
		* no more than 2 small lanes for any given shipment...which
		* is why the "odd looking" comparison below.
		*/
		if ((lp->maxqvl >= con_space_needed / 2.0))
		{
			if (*best_alloc_lp == NULL) {


				pckrel_WriteTrc("        EM: *** Setting loc to %s", lp->stoloc);
				*best_alloc_lp = lp;
			}
		}
		else
		{
			pckrel_WriteTrc("        EM: Lane %s is defined as small - maxqvl = %f,"
				"needed space = %f...skipping",
				lp->stoloc, lp->maxqvl, con_space_needed);
			return;
		}
	}
	else 
	{
		if (*best_alloc_lp == NULL) {

			pckrel_WriteTrc("        EM: *** Setting loc to %s", lp->stoloc);
			*best_alloc_lp = lp;
		}
	}

	/*
	* Regardless of whether or not we're a staging lane,
	* we now want to choose the "best" by minimizing the 
	* amount of dead space...so pick the lane with the least
	* qvl which satisfies what is needed
	*/
	pckrel_WriteTrc("        EM: checking if locs max >= needed and locs max < our current best choice");
	pckrel_WriteTrc("        EM: checking if %f >= %f and %f < %f", lp->maxqvl, 
		con_space_needed, lp->maxqvl, (*best_alloc_lp)->maxqvl);
	pckrel_WriteTrc("        EM: else check loc max < needed and loc max > current max");
	pckrel_WriteTrc("        EM: checking if %f < %f and %f > %f", lp->maxqvl, 
		con_space_needed, lp->maxqvl, (*best_alloc_lp)->maxqvl);
	pckrel_WriteTrc("        EM: BestSeqnum: %f vs inSeqnum: %f", (*best_alloc_lp)->seqnum, lp->seqnum );

	/*if ((lp->maxqvl >= con_space_needed) &&
	(lp->maxqvl < (*best_alloc_lp)->maxqvl))
	{
	pckrel_WriteTrc("        EM: *** Switching to loc %s from %s due to better fit",
	lp->stoloc, (*best_alloc_lp)->stoloc);
	*best_alloc_lp = lp;
	}
	else if (lp->maxqvl < con_space_needed && lp->maxqvl > (*best_alloc_lp)->maxqvl)
	{
	pckrel_WriteTrc("        EM: *** Switching to loc (bigger) %s from %s due to better fit",
	lp->stoloc, (*best_alloc_lp)->stoloc);
	*best_alloc_lp = lp;
	} */

	/* EM: New logic for best fit, try and fit all */
	/* EM: if loc's max qvl can fit all */
	if( lp->maxqvl >= con_space_needed ) {
		/* EM: if current loc fits all then we only switch if we are smaller to be
		a better fit */
		if ((*best_alloc_lp)->maxqvl >= con_space_needed && 
			lp->maxqvl < (*best_alloc_lp)->maxqvl) {
				pckrel_WriteTrc("        EM: *** FIT ALL Switching to loc %s from %s due to better fit SMALLER",
					lp->stoloc, (*best_alloc_lp)->stoloc);
				*best_alloc_lp = lp;
		}
		/* EM: We can fit all and the previous loc cannot, lets do it */
		else if( (*best_alloc_lp)->maxqvl < con_space_needed ){
			pckrel_WriteTrc("        EM: *** FIT ALL Switching to loc %s from %s due to better fit BIGGER",
				lp->stoloc, (*best_alloc_lp)->stoloc);
			*best_alloc_lp = lp;
		}
		else if( lp->seqnum > 0 && lp->maxqvl == (*best_alloc_lp)->maxqvl ) {
			if( (*best_alloc_lp)->seqnum == 0 || 
				lp->seqnum < (*best_alloc_lp)->seqnum ) {

				pckrel_WriteTrc("        EM: *** FIT ALL Switching to loc %s from %s due SEQUENCE",
				    lp->stoloc, (*best_alloc_lp)->stoloc);
			    *best_alloc_lp = lp;
			}
		}
	}
	else
		/* EM: loc is less then what we need for the shipment so only switch locs if its a bigger loc 
		then what we have */
	{
		if( lp->maxqvl > (*best_alloc_lp)->maxqvl ) {
			pckrel_WriteTrc("        EM: *** NOT ALL FIT Switching to loc %s from %s due to better fit BIGGER",
				lp->stoloc, (*best_alloc_lp)->stoloc);
			*best_alloc_lp = lp;
		}
		else if( lp->seqnum > 0 && lp->maxqvl == (*best_alloc_lp)->maxqvl ) {
			if( (*best_alloc_lp)->seqnum == 0 || 
				lp->seqnum < (*best_alloc_lp)->seqnum ) {

				pckrel_WriteTrc("        EM: *** FIT ALL Switching to loc %s from %s due SEQUENCE",
				    lp->stoloc, (*best_alloc_lp)->stoloc);
			    *best_alloc_lp = lp;
			}
		}
	}


	pckrel_WriteTrc("        EM: Leaving Evaluate");

	return;
}
/*
* sGetNextLoc:  Get the next matching location in the list
*               satisfying the criteria specified.
*
*   LocationList : position in the list to begin looking
*   required_zone: if a matching work zone is required, this will
*                  be non-null
*   rescod       : resource code which is requested
*   search_type  : type of search
*                   1 = looking for pre-existing matching rescod
*                   2 = looking for a preferred rescod match
*                   3 = looking for a non-preferred (by anyone) match
*                   4 = looking for any non-assigned location
*/
static LOCATION_LIST *sGetNextLoc(LOCATION_LIST *LocationList,
								  char *required_zone,
								  double cmb_space_needed,
								  char *rescod, 
								  long search_type, 
								  moca_bool_t var_facflg, 
								  char *ship_lane_id)
{

	LOCATION_LIST *lp;

	if (LocationList == NULL)
		return(NULL);

	pckrel_WriteTrc( "  EM: In sGetNextLoc Search Type is: %ld, ship_lane_id: %s", search_type, ship_lane_id );
	/* Search type = 1, existing location assigments */
	if (search_type == 1 && strlen(ship_lane_id) == 0)
	{
		for (lp = LocationList; lp; lp = lp->next)
		{
			pckrel_WriteTrc( "  EM: (1 without lane id) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );
			/* If we found a match */
			if (strncmp(rescod, lp->rescod, RESCOD_LEN) == 0 &&
				lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl)
			{
				/* if zone match is required, check that */
				if (required_zone && strlen(required_zone))
				{
					if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
					{
						/* zone match was good...return success */
						pckrel_WriteTrc("   EM: got ...%s, maxqvl: %f, pndqvl: %f", 
							lp->stoloc, lp->maxqvl, lp->pndqvl);
						return(lp);
					}
				}
				else
				{
					pckrel_WriteTrc("   EM: no zone req got ...%s, maxqvl: %f, pndqvl: %f", 
						lp->stoloc, lp->maxqvl, lp->pndqvl);
					return(lp);
				}
			}
		}
		return(NULL);
	}

	if (search_type == 1 && strlen(ship_lane_id) != 0)
	{
		pckrel_WriteTrc("   EM: (1 with lane id) Looking for Resource for pallet group: lane ID %s", ship_lane_id);

		for (lp = LocationList; lp; lp = lp->next)
		{
			pckrel_WriteTrc( "  EM: (1 with lane id) checking for the magic for loc %s and ship_lane_id: %s", 
				lp->stoloc, ship_lane_id );
			pckrel_WriteTrc( "  EM: (1 with lane id) lp->ship_lane_ids: %s", lp->ship_lane_ids );
			/* If we found a match */
			if ((strncmp(rescod, lp->rescod, RESCOD_LEN) == 0 && 
				(strlen(lp->ship_lane_ids) && strstr(lp->ship_lane_ids, ship_lane_id))) || 
				((strncmp(rescod, lp->rescod, RESCOD_LEN) == 0) && 
				((lp->curqvl + lp->pndqvl + (strlen(ship_lane_id) !=0 ? 1: cmb_space_needed)) 
				<= lp->maxqvl)))
			{

				pckrel_WriteTrc("   EM: (1 with lane id) Magic, "
					"Found Resource for pallet group: ship_lane_id %s, stoloc: %s  maxqvl: %f, pndqvl: %f", 
					ship_lane_id, lp->stoloc, lp->maxqvl, lp->pndqvl);

				/* if zone match is required, check that */
				if (required_zone && strlen(required_zone))
				{
					pckrel_WriteTrc("   EM: (1 with lane id) req zone: %s", required_zone );

					if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
					{
						/* zone match was good...return success */
						pckrel_WriteTrc("   EM: (1 with lane id) got ...%s, maxqvl: %f, pndqvl: %f", 
							lp->stoloc, lp->maxqvl, lp->pndqvl);
						return(lp);
					}
					else {
						pckrel_WriteTrc("   EM: (1 with lane id) zone fail" );
					}
				}
				else
				{
					pckrel_WriteTrc("   (1 with lane id) ...%s, maxqvl: %f, pndqvl: %f", 
						lp->stoloc, lp->maxqvl, lp->pndqvl);
					return(lp);
				}
			}
			else {
				pckrel_WriteTrc( "  EM: (1 with lane id) No Magic . . ." );
			}
		}
		return(NULL);
	} 

	/* Search type = 2, preferred locations */
	if (search_type == 2)
	{
		for (lp = LocationList; lp; lp = lp->next)
		{

			pckrel_WriteTrc( "  EM: (2) checking stoloc: %s, rescod: %s, pref_rescod: %s", 
				lp->stoloc, lp->rescod, lp->pref_rescod );
			if (strncmp(lp->pref_rescod, rescod, RESCOD_LEN) == 0 && 
				lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl)
			{
				if (strlen(lp->rescod) > 0 &&
					strncmp(lp->rescod, "^^^^", 4) != 0 &&
					strncmp(lp->rescod, rescod, RESCOD_LEN) != 0)
				{
					pckrel_WriteTrc("    ...found preferred loc: %s, but"
						" currently assigned to rescod: %s"
						" - SKIPPED",
						lp->stoloc, lp->rescod);
					continue;
				}

				/* if zone match is required, check that */
				if (required_zone && strlen(required_zone))
				{
					if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
					{
						/* zone match was good...return success */
						pckrel_WriteTrc("   EM: got ...%s, maxqvl: %f, pndqvl: %f", 
							lp->stoloc, lp->maxqvl, lp->pndqvl);
						return(lp);
					}
				}
				else
				{
					pckrel_WriteTrc("   EM: got ...%s, maxqvl: %f, pndqvl: %f", 
						lp->stoloc, lp->maxqvl, lp->pndqvl);
					return(lp);
				}
			}
		}
		return(NULL);
	}
	/* 
	* search_type = 3, available locations with no preference
	* (for anything) 
	*/
	/* EM: 08/22/2008 Shouldn't be doing any 3's anymore */
	if (search_type == 3)
	{
		if (var_facflg == 0)
		{
			pckrel_WriteTrc("    EM: ...looking in normal list first");
			for (lp = LocationList; lp; lp = lp->next)
			{ 
				pckrel_WriteTrc( "  EM: (3) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );
				if (strstr (lp->stoloc, "SHIP-N")) {
					pckrel_WriteTrc("    EM: SKIPPIN NLOC %s", lp->stoloc);
					continue;
				}

				pckrel_WriteTrc(" -- Evaluating %s, maxqvl: %f, pndqvl: %f, rescod: %s", 
					lp->stoloc, lp->maxqvl, lp->pndqvl,lp->rescod);

				if (strlen(misTrim(lp->rescod)) > 0 && (strstr(lp->rescod, "^^^^") != lp->rescod)) {
					pckrel_WriteTrc("  EM: -- skipping %s, maxqvl: %f, pndqvl: %f, rescod: %s due to rescod len", 
						lp->stoloc, lp->maxqvl, lp->pndqvl,lp->rescod);
					continue;
				}

				/*pckrel_WriteTrc("  EM: test 1 = %d", (strlen(lp->rescod) == 0 || 
				(strncmp(lp->rescod, "^^^^", 4) == 0)));

				pckrel_WriteTrc("  EM: test 2 = %d",  lp->curqvl + lp->pndqvl + strlen(ship_lane_id)>0 ? 1: cmb_space_needed <= lp->maxqvl &&
				(strlen(lp->pref_rescod) == 0));*/

				/* EM: Commenting out if below replacint with what it does when searching new facility.  Need to
				ask mark about this, it's passing on good locs.  Why cur + pnd + len( ship_lane_id )?
				*/

				/*if ((strlen(lp->rescod) == 0 || (strncmp(lp->rescod, "^^^^", 4) == 0)) &&
				lp->curqvl + lp->pndqvl + strlen(ship_lane_id)>0 ? 1: cmb_space_needed <= lp->maxqvl &&
				(strlen(lp->pref_rescod) == 0))*/
				if ((strlen(lp->rescod) == 0 ||
					strncmp(lp->rescod, "^^^^", 4) == 0) &&
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					(strlen(lp->pref_rescod) == 0))
				{
					/* if zone match is required, check that */
					if (required_zone && strlen(required_zone))
					{
						if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
						{
							/* zone match was good...return success */
							pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
								lp->stoloc, lp->maxqvl, lp->pndqvl);
							return(lp);
						}
					}
					else
					{
						pckrel_WriteTrc("   EM: got ...%s, maxqvl: %f, pndqvl: %f, rescod: %s", 
							lp->stoloc, lp->maxqvl, lp->pndqvl,lp->rescod);
						if (strcmp(rescod, lp->rescod))
							pckrel_WriteTrc("Potentially BAD RESCOD   ...%s, maxqvl: %f, pndqvl: %f, rescod: %s (should be %s)", 
							lp->stoloc, lp->maxqvl, lp->pndqvl,lp->rescod, rescod);

						return(lp);
					}
				}
			}                
		}
		else /* EM: 08/22/2008 Don't scan through New Building unless desperate ( search_type 4 ) */
		{
			pckrel_WriteTrc("    ...looking in New Facility");

			for (lp = LocationList; lp; lp = lp->next)
			{
				pckrel_WriteTrc( "  EM: (3B) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );
				if (!strstr (lp->stoloc, "SHIP-N")) {
					pckrel_WriteTrc("    EM: Skipping Non NLOC %s", lp->stoloc);
					continue;
				}

				pckrel_WriteTrc("   EM ...(considering) %s, maxqvl: %f, pndqvl: %f, rescod: %s, space needed: %f", 
					lp->stoloc, lp->maxqvl, lp->pndqvl, lp->rescod, cmb_space_needed );

				if ((strlen(lp->rescod) == 0 ||
					strncmp(lp->rescod, "^^^^", 4) == 0) &&
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					(strlen(lp->pref_rescod) == 0))
				{
					/* if zone match is required, check that */
					if (required_zone && strlen(required_zone))
					{
						if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
						{
							/* zone match was good...return success */
							pckrel_WriteTrc("   EM: Got ...%s, maxqvl: %f, pndqvl: %f", 
								lp->stoloc, lp->maxqvl, lp->pndqvl);
							return(lp);
						}
					}
					else
					{
						pckrel_WriteTrc("   EM: got ...%s, maxqvl: %f, pndqvl: %f", 
							lp->stoloc, lp->maxqvl, lp->pndqvl);

						return(lp);
					}
				}
			}                
		}

		//else
		//{
		//	pckrel_WriteTrc("    ...looking in New Facility First");
		//	for (lp = LocationList; lp; lp = lp->next)
		//	{
		//		pckrel_WriteTrc( "  EM: (3C) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );
		//		if (!strstr (lp->stoloc, "SHIP-N")) {
		//			pckrel_WriteTrc("    EM: Skipping Non NLOC %s", lp->stoloc);
		//			continue;
		//		}
		//		if ((strlen(lp->rescod) == 0 ||
		//			strncmp(lp->rescod, "^^^^", 4) == 0) &&
		//			lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
		//			(strlen(lp->pref_rescod) == 0))
		//		{
		//			/* if zone match is required, check that */
		//			if (required_zone && strlen(required_zone))
		//			{
		//				if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
		//				{
		//					/* zone match was good...return success */
		//					pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
		//						lp->stoloc, lp->maxqvl, lp->pndqvl);
		//					return(lp);
		//				}
		//			}
		//			else
		//			{
		//				pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
		//					lp->stoloc, lp->maxqvl, lp->pndqvl);
		//				return(lp);
		//			}
		//		}
		//	}                

		//	pckrel_WriteTrc("    ...looking for normal locations ");
		//	for (lp = LocationList; lp; lp = lp->next)
		//	{

		//		pckrel_WriteTrc( "  EM: (3D) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );
		//		if (strstr (lp->stoloc, "SHIP-N")) {
		//			pckrel_WriteTrc("    EM: SKIPPIN NLOC %s", lp->stoloc);
		//			continue;
		//		}
		//		if ((strlen(lp->rescod) == 0 ||
		//			strncmp(lp->rescod, "^^^^", 4) == 0) &&
		//			lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
		//			(strlen(lp->pref_rescod) == 0))
		//		{
		//			/* if zone match is required, check that */
		//			if (required_zone && strlen(required_zone))
		//			{
		//				if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
		//				{
		//					/* zone match was good...return success */
		//					pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
		//						lp->stoloc, lp->maxqvl, lp->pndqvl);
		//					return(lp);
		//				}
		//			}
		//			else
		//			{
		//				pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
		//					lp->stoloc, lp->maxqvl, lp->pndqvl);
		//				return(lp);
		//			}
		//		}
		//	}                
		//} 

		return(NULL);
	}

	if (search_type == 5)
	{
		if (var_facflg == 0)
		{
			pckrel_WriteTrc("    EM: ...looking in normal list first (var_facflg = 0)");
			for (lp = LocationList; lp; lp = lp->next)
			{
				pckrel_WriteTrc( "  EM: (5) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );
				if (strstr (lp->stoloc, "SHIP-N")) {
					pckrel_WriteTrc("    EM: SKIPPIN NLOC %s", lp->stoloc);
					continue;
				}

				pckrel_WriteTrc(" -- EM: Evaluating %s, maxqvl: %f, pndqvl: %f, rescod: %s, curqvl: %f", 
					lp->stoloc, lp->maxqvl, lp->pndqvl,lp->rescod, lp->curqvl);

				if (strlen(misTrim(lp->rescod)) > 0 && (strstr(lp->rescod, "^^^^") != lp->rescod)) {
					pckrel_WriteTrc(" EM: -- skipping %s, maxqvl: %f, pndqvl: %f, rescod: %s due to rescod len", 
						lp->stoloc, lp->maxqvl, lp->pndqvl,lp->rescod);
					continue;
				}

				pckrel_WriteTrc(" -- EM: ship_lane_id: %s, cmb_space_needed: %f, pref_rescod: %s", 
					ship_lane_id, cmb_space_needed, lp->pref_rescod );
				pckrel_WriteTrc(" -- EM: test 1: %d ", 
					(strlen(lp->rescod) == 0 ||	strncmp(lp->rescod, "^^^^", 4) == 0));
				pckrel_WriteTrc(" -- EM: test 2: %d ", 
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					(strlen(lp->pref_rescod) == 0));

				/* EM: 08/22/2008: removing this if block, replacing it with what we have in the new building section
				below.  I don't understand this logic, especially if we want to fit the whole shipment.  
				It is completely passing over empty locs that would fit the whole shipment.

				if ((strlen(lp->rescod) == 0 || (strncmp(lp->rescod, "^^^^", 4) == 0)) &&
				lp->curqvl + lp->pndqvl + strlen(ship_lane_id)>0 ? 1: cmb_space_needed >= lp->maxqvl &&
				(strlen(lp->pref_rescod) == 0))
				*/
				if ((strlen(lp->rescod) == 0 ||
					strncmp(lp->rescod, "^^^^", 4) == 0) &&
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					(strlen(lp->pref_rescod) == 0))
				{
					pckrel_WriteTrc(" -- EM: in the magic" );
					/* if zone match is required, check that */
					if (required_zone && strlen(required_zone))
					{
						pckrel_WriteTrc(" -- EM: WZ req ");
						if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
						{
							/* zone match was good...return success */
							pckrel_WriteTrc("   EM: got wzreq: ...%s, maxqvl: %f, pndqvl: %f", 
								lp->stoloc, lp->maxqvl, lp->pndqvl);
							return(lp);
						}
						pckrel_WriteTrc(" -- EM: No WZ Match" );
					}
					else
					{
						pckrel_WriteTrc("  EM: got no wzreq: (new code) ...%s, maxqvl: %f, pndqvl: %f, rescod: %s", 
							lp->stoloc, lp->maxqvl, lp->pndqvl,lp->rescod);

						return(lp);
					}
				}

				pckrel_WriteTrc(" -- EM: No Magic ");
			}                
		}
		else /* EM: 08/22/2008 Never cross to walmart side for non walmart unless desperate ( search_type = 4 ) */
		{
			pckrel_WriteTrc("    EM: ...looking in New Facility");

			for (lp = LocationList; lp; lp = lp->next)
			{
				pckrel_WriteTrc( "  EM: (5B) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );
				if (!strstr (lp->stoloc, "SHIP-N")) {
					pckrel_WriteTrc("    EM: Skipping Non NLOC %s", lp->stoloc);
					continue;
				}

				pckrel_WriteTrc("   EM: ...(considering) %s, maxqvl: %f, pndqvl: %f, rescod: %s, space needed: %f", 
					lp->stoloc, lp->maxqvl, lp->pndqvl, lp->rescod, cmb_space_needed );


				if ((strlen(lp->rescod) == 0 ||
					strncmp(lp->rescod, "^^^^", 4) == 0) &&
					lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
					(strlen(lp->pref_rescod) == 0))
				{
					/* if zone match is required, check that */
					if (required_zone && strlen(required_zone))
					{
						if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
						{
							/* zone match was good...return success */
							pckrel_WriteTrc("   EM: got wzreq: ...%s, maxqvl: %f, pndqvl: %f", 
								lp->stoloc, lp->maxqvl, lp->pndqvl);
							return(lp);
						}
					}
					else
					{
						pckrel_WriteTrc("   EM: got no wzreq: ...%s, maxqvl: %f, pndqvl: %f", 
							lp->stoloc, lp->maxqvl, lp->pndqvl);

						return(lp);
					}
				}
			}                
		}

		return(NULL);
	}

	if (search_type == 4)
	{
		/* EM: 08/22/2008: go thru non N's first */
		for (lp = LocationList; lp; lp = lp->next)
		{
			pckrel_WriteTrc( "  EM: (4A) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );

			/* EM: 08/26/2008: if we are in desperation we can still try the right sides first */
			if (var_facflg == 0) {
				if (strstr (lp->stoloc, "SHIP-N")) {
					pckrel_WriteTrc("    EM: Skipping NLOC %s", lp->stoloc);
					continue;
				}
			}
			else {
				if (!strstr (lp->stoloc, "SHIP-N")) {
					pckrel_WriteTrc("    EM: Skipping NLOC %s", lp->stoloc);
					continue;
				}
			}

			/*if ((strlen(lp->rescod) == 0 ||
			strncmp(lp->rescod, "^^^^", 4) == 0) &&
			lp->curqvl + lp->pndqvl + strlen(ship_lane_id)>0 ? 1: cmb_space_needed <= lp->maxqvl &&
			(strlen(lp->pref_rescod) > 0))
			{*/

			pckrel_WriteTrc(" -- EM: test 1: %d ", 
				(strlen(lp->rescod) == 0 ||	strncmp(lp->rescod, "^^^^", 4) == 0));
			pckrel_WriteTrc(" -- EM: test 2: %d ", 
				lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
				(strlen(lp->pref_rescod) == 0));

			/* EM: 08/26/2008: last comparison was strlen(lp->pref_rescod) > 0, changed to == 0 
			My assumption is this is are last resort.  Need to spec out what last resort is . . . */
			if ((strlen(lp->rescod) == 0 ||
				strncmp(lp->rescod, "^^^^", 4) == 0) &&
				lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
				(strlen(lp->pref_rescod) == 0))
			{
				/* if zone match is required, check that */
				if (required_zone && strlen(required_zone))
				{
					if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
					{
						/* zone match was good...return success */
						pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
							lp->stoloc, lp->maxqvl, lp->pndqvl);
						return(lp);
					}
				}
				else
				{
					pckrel_WriteTrc("   EM: loop 4 got ...%s, maxqvl: %f, pndqvl: %f", 
						lp->stoloc, lp->maxqvl, lp->pndqvl);
					return(lp);
				}
			}
		}                

		for (lp = LocationList; lp; lp = lp->next)
		{
			pckrel_WriteTrc( "  EM: (4B) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );

			/* EM: 08/26/2008: ok, we tried the right way, now lets try the wrong way . . . */
			if (var_facflg == 0) {
				if (!strstr (lp->stoloc, "SHIP-N")) {
					pckrel_WriteTrc("    EM: Skipping NLOC %s", lp->stoloc);
					continue;
				}
			}
			else {
				if (strstr (lp->stoloc, "SHIP-N")) {
					pckrel_WriteTrc("    EM: Skipping NLOC %s", lp->stoloc);
					continue;
				}
			}

			pckrel_WriteTrc(" -- EM: test 1: %d ", 
				(strlen(lp->rescod) == 0 ||	strncmp(lp->rescod, "^^^^", 4) == 0));
			pckrel_WriteTrc(" -- EM: test 2: %d ", 
				lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
				(strlen(lp->pref_rescod) == 0));

			/* EM: 08/26/2008: last comparison was strlen(lp->pref_rescod) > 0, changed to == 0 */
			if ((strlen(lp->rescod) == 0 ||
				strncmp(lp->rescod, "^^^^", 4) == 0) &&
				lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
				(strlen(lp->pref_rescod) == 0))
			{
				/* if zone match is required, check that */
				if (required_zone && strlen(required_zone))
				{
					if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
					{
						/* zone match was good...return success */
						pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
							lp->stoloc, lp->maxqvl, lp->pndqvl);
						return(lp);
					}
				}
				else
				{
					pckrel_WriteTrc("   EM: loop 4 got ...%s, maxqvl: %f, pndqvl: %f", 
						lp->stoloc, lp->maxqvl, lp->pndqvl);
					return(lp);
				}
			}
		}

		for (lp = LocationList; lp; lp = lp->next)
		{
			pckrel_WriteTrc( "  EM: (4C) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );

			/* EM: 08/26/2008: if we are in desperation we can still try the right sides first */
			if (var_facflg == 0) {
				if (strstr (lp->stoloc, "SHIP-N")) {
					pckrel_WriteTrc("    EM: Skipping NLOC %s", lp->stoloc);
					continue;
				}
			}
			else {
				if (!strstr (lp->stoloc, "SHIP-N")) {
					pckrel_WriteTrc("    EM: Skipping NLOC %s", lp->stoloc);
					continue;
				}
			}

			/*if ((strlen(lp->rescod) == 0 ||
			strncmp(lp->rescod, "^^^^", 4) == 0) &&
			lp->curqvl + lp->pndqvl + strlen(ship_lane_id)>0 ? 1: cmb_space_needed <= lp->maxqvl &&
			(strlen(lp->pref_rescod) > 0))
			{*/

			pckrel_WriteTrc(" -- EM: test 1: %d ", 
				(strlen(lp->rescod) == 0 ||	strncmp(lp->rescod, "^^^^", 4) == 0));
			pckrel_WriteTrc(" -- EM: test 2: %d ", 
				lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
				(strlen(lp->pref_rescod) == 0));

			if ((strlen(lp->rescod) == 0 ||
				strncmp(lp->rescod, "^^^^", 4) == 0) &&
				lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
				(strlen(lp->pref_rescod) > 0))
			{
				/* if zone match is required, check that */
				if (required_zone && strlen(required_zone))
				{
					if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
					{
						/* zone match was good...return success */
						pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
							lp->stoloc, lp->maxqvl, lp->pndqvl);
						return(lp);
					}
				}
				else
				{
					pckrel_WriteTrc("   EM: loop 4 got ...%s, maxqvl: %f, pndqvl: %f", 
						lp->stoloc, lp->maxqvl, lp->pndqvl);
					return(lp);
				}
			}
		}

		for (lp = LocationList; lp; lp = lp->next)
		{
			pckrel_WriteTrc( "  EM: (4D) checking stoloc: %s, rescod: %s", lp->stoloc, lp->rescod );

			/* EM: 08/26/2008: ok, we tried the right way, now lets try the wrong way . . . */
			if (var_facflg == 0) {
				if (!strstr (lp->stoloc, "SHIP-N")) {
					pckrel_WriteTrc("    EM: Skipping NLOC %s", lp->stoloc);
					continue;
				}
			}
			else {
				if (strstr (lp->stoloc, "SHIP-N")) {
					pckrel_WriteTrc("    EM: Skipping NLOC %s", lp->stoloc);
					continue;
				}
			}

			pckrel_WriteTrc(" -- EM: test 1: %d ", 
				(strlen(lp->rescod) == 0 ||	strncmp(lp->rescod, "^^^^", 4) == 0));
			pckrel_WriteTrc(" -- EM: test 2: %d ", 
				lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
				(strlen(lp->pref_rescod) == 0));

			/* EM: 08/26/2008: last comparison was strlen(lp->pref_rescod) > 0, changed to == 0 */
			if ((strlen(lp->rescod) == 0 ||
				strncmp(lp->rescod, "^^^^", 4) == 0) &&
				lp->curqvl + lp->pndqvl + cmb_space_needed <= lp->maxqvl &&
				(strlen(lp->pref_rescod) > 0))
			{
				/* if zone match is required, check that */
				if (required_zone && strlen(required_zone))
				{
					if (strncmp(required_zone, lp->wrkzon, WRKZON_LEN) == 0)
					{
						/* zone match was good...return success */
						pckrel_WriteTrc("   ...%s, maxqvl: %f, pndqvl: %f", 
							lp->stoloc, lp->maxqvl, lp->pndqvl);
						return(lp);
					}
				}
				else
				{
					pckrel_WriteTrc("   EM: loop 4 got ...%s, maxqvl: %f, pndqvl: %f", 
						lp->stoloc, lp->maxqvl, lp->pndqvl);
					return(lp);
				}
			}
		}
	}

	return(NULL);
}
/*
* sRecheckResourceAssignment
*
* This routine is used to re-validate that a resource assignment that
* we are about to make does not conflict with any pckmovs already out
* there.  This check is turned on via a policy (which sets the
* chkmov_flag) and is only really necessary when process pick release
* is being called in more than one place in the system
*/
static long sRecheckResourceAssignment(char *stoloc,
									   char *rescod,
									   char *loc_rescod,
									   char *chkmov_flag)
{
	char buffer[1000];
	long ret_status;

	if (misCiStrncmp(chkmov_flag, "Y", 1) != 0)
	{
		return(eOK);
	}

	if (strncmp(loc_rescod, "^^^^", 4) != 0)
	{
		/*
		* don't need to worry about it..we never thought it was 
		* empty to start with
		*/
		return(eOK);
	}
	pckrel_WriteTrc("  ...checking location %s "
		"to guard against rescod "
		"mixing!",
		stoloc);
	sprintf(buffer,
		"select cmbcod "
		"  from pckmov "
		" where prcqty = '0' "
		"	and rescod <> '%s' "
		"	and stoloc = '%s' ",
		rescod,
		stoloc);
	ret_status = sqlExecStr(buffer, NULL);
	if (ret_status == eOK)
	{
		pckrel_WriteTrc("FOUND a combo code "
			"with prcqty of 0 and a "
			"different rescod! "
			"Skipping ...");
		return(!eOK);
	}
	return(eOK);
}

/*
* sFindResourceLocation
*
* Given a list of locations, this routine will attempt to find the
* best matched location to use.  
*/
static LOCATION_LIST *sFindResourceLocation(LOCATION_LIST *LocationList,
											char *rescod,
											long small_lane_size,
											double con_space_needed,
											double cmb_space_needed,
											char *required_zone,
											moca_bool_t sigflg,
											moca_bool_t stgflg,
											char *chkmov_flag, 
											moca_bool_t var_facflg, 
											char *ship_lane_id, 
											char *arecod)
{
	LOCATION_LIST *alloc_lp, *best_alloc_lp, *lp;

	alloc_lp = best_alloc_lp = NULL;

	/*
	* First let's check for a location which already has
	* a matching resource code
	*/

	pckrel_WriteTrc("EM: In sFindResourceLocation, ship_lane_id: %s", ship_lane_id );


	/* EM: 08/25/2008: Ok here was the ghost in the machine for this ship lane matching.  
	You could put it in the ship_lane_ids for location 8, then fill it with pallet picks.
	Then loc B may be smaller or whatever and come up before loc A and have room but the ship_id is not
	in its list.  It will then add it to its list and BAM we have a split lane.  To beat this, I am
	just Fast tracking the search.  If we come in with a ship lane id, see if we assigned it to a loc's
	ship_lane_ids.  If we did take it.  If not, do our normal process.
	*/
	pckrel_WriteTrc("EM: checking if we can fast track ship_lane_id that came in");
	if( strlen(ship_lane_id) ) {
		for (lp = LocationList; lp; lp = lp->next)
		{
			//pckrel_WriteTrc("EM: Loop 0 Trying to fast track loc: %s, ship_ids: %s", lp->stoloc, lp->ship_lane_ids);
			if(strstr(lp->ship_lane_ids, ship_lane_id)) {
				pckrel_WriteTrc("EM: FAST TRACKED LOC: %s", lp->stoloc );
				alloc_lp = lp;
				return( alloc_lp );
			}
		}
	}

	pckrel_WriteTrc("   EM: ...looking for existing "
		"location assignment for rescod: %s",
		rescod);
	alloc_lp = sGetNextLoc(LocationList,
		required_zone,
		cmb_space_needed,
		rescod, 
		1, 0, ship_lane_id);

	if (alloc_lp) {
		pckrel_WriteTrc("   EM: adding into previous loc as there is still room %s", alloc_lp->stoloc);
		return(alloc_lp);
	}

	/*
	* Next we look for any location which is "preferred"
	*/
	pckrel_WriteTrc("   EM: ...looking for preferred "
		"location assignment for rescod: %s",
		rescod);

	for (lp = sGetNextLoc(LocationList, required_zone, 
		cmb_space_needed, rescod, 2,0, NULL);
		lp != NULL && alloc_lp == NULL;
	lp = sGetNextLoc(lp->next, required_zone, 
		cmb_space_needed, rescod, 2,0, NULL))
	{
		pckrel_WriteTrc("EM: Loop 2 loc: %s . . .", lp->stoloc);
		if (sRecheckResourceAssignment(lp->stoloc,
			rescod,
			lp->rescod,
			chkmov_flag) != eOK)
		{
			continue;
		}

		if (sigflg == BOOLEAN_TRUE ||
			(sigflg == BOOLEAN_FALSE &&
			lp->pndqvl == 0.0 &&
			lp->asgflg == BOOLEAN_FALSE))
		{
			sEvaluateBestFit(&best_alloc_lp,
				lp,
				con_space_needed,
				stgflg,
				small_lane_size);
		}
	}
	if (alloc_lp == NULL)
		alloc_lp = best_alloc_lp;

	if (alloc_lp) {
		pckrel_WriteTrc("   EM: Loop 2 allocated loc: %s", alloc_lp->stoloc);
		sAddToSpaceUsed(
			arecod,
			rescod,
			alloc_lp->maxqvl);
		return(alloc_lp);
	}


	/* 
	* Now go with the non-preferred locations that can fit the whole thing
	*
	*/
	if (strcmp("SSTG", arecod) == 0)
	{
		pckrel_WriteTrc("   EM: ...looking for big "
			"locations for rescod %s, fac flag = %d, space %f ",
			rescod, var_facflg, con_space_needed);
		/* 
		EM: 08/25/20008: Making this the main block.  Sending in cmb_space_needed, not con
		Space needed.  In theory if it were intensive this makes sense to loop twice, first
		trying to fit it all but we still loop all the locs and the best fit check is nothing
		so lets just bubble the best locs up and skip the search_type 3 loop below as we can get
		this puppy done here if we have available locs.  Keep the 4 loop incase we have to mix new 
		building / old building to get her done.
		*/

		for (lp = sGetNextLoc(LocationList, required_zone, 
			cmb_space_needed, rescod, 5, var_facflg, ship_lane_id);
			lp && alloc_lp == NULL;
		lp = sGetNextLoc(lp->next, required_zone, 
			cmb_space_needed, rescod, 5, var_facflg, ship_lane_id))
		{
			pckrel_WriteTrc("EM: Loop 5 loc: %s . . .", lp->stoloc);
			if (sRecheckResourceAssignment(lp->stoloc,
				rescod,
				lp->rescod,
				chkmov_flag) != eOK)
			{
				pckrel_WriteTrc(" EM: Loop 5 Recheck of Resource Code failed");
				continue;
			}

			if (sigflg == BOOLEAN_TRUE ||
				(sigflg == BOOLEAN_FALSE &&
				lp->pndqvl == 0.0 &&  
				lp->asgflg == BOOLEAN_FALSE))
			{				
				if (!var_facflg)
				{ 
					/* EM: 08/22/2008: Not desperate yet so don't take a loc that's too big */
					/* EM: 08/27/2008: Move into the not var_facflg section */
					pckrel_WriteTrc(" EM: Checking if loc is too big: maxqvl: %f, too_big: %f", 
						lp->maxqvl, con_space_needed * 1.2);
					if (lp->maxqvl > (con_space_needed * 1.2) ) {
						pckrel_WriteTrc(" EM: Loop 5 This location is too big, maxqvl: %f", lp->maxqvl);
						continue;
					}

					pckrel_WriteTrc("  EM: Loop 5 Best Fit Check l.maxqvl = %f, b.maxqvl:%f,  needed: %f", 
						lp->maxqvl, best_alloc_lp->maxqvl, con_space_needed);
					sEvaluateBestFit(&best_alloc_lp,
						lp,
						con_space_needed,
						stgflg,
						small_lane_size);
				}
				else
				{
					pckrel_WriteTrc("  EM: Loop 5 Setting Best Alloc lp");
					best_alloc_lp = lp;
				}
			}
		}

		if (alloc_lp == NULL)
			alloc_lp = best_alloc_lp;

		if (alloc_lp) {
			pckrel_WriteTrc("   EM: Loop 5 allocated loc: %s", alloc_lp->stoloc);
			sAddToSpaceUsed(
				arecod,
				rescod,
				alloc_lp->maxqvl);
			return(alloc_lp);
		}

		/* 
		EM: 08/22/2008: this check for too big is not working correctly.  
		Replaced with above.
		Order is max qvl so biggest loc is last, so this wipes all previous
		results.  And it doesn't shake the best_alloc setting.  I am going
		to put the check before the evaluate so it never gets set in 
		evaluate.
		*/
		//if (alloc_lp == NULL)
		//	alloc_lp = best_alloc_lp;

		//if (alloc_lp)
		//{
		//	if (alloc_lp->maxqvl <= con_space_needed * 1.2)
		//	{
		//		/*EM 08/21/2008 passing alloc_lp->rescod does not match when the 
		//		hash table was set up, pasing passed in rescod to match */
		//		sAddToSpaceUsed(arecod,
		//			alloc_lp->rescod,						   
		//			alloc_lp->maxqvl);

		//		/*sAddToSpaceUsed(arecod,
		//		rescod,
		//		alloc_lp->maxqvl);*/

		//		return(alloc_lp);
		//	}
		//	else{
		//		pckrel_WriteTrc("        EM: LOC CANNED TOO BIG" );
		//		alloc_lp = NULL;
		//	}
		//}
	}

	//pckrel_WriteTrc("   EM: ...looking for "
	//	"locations for rescod %s, fac flag = %d ",
	//	rescod, var_facflg);
	//for (lp = sGetNextLoc(LocationList, required_zone, 
	//	cmb_space_needed, rescod, 3, var_facflg, ship_lane_id);
	//	lp && alloc_lp == NULL;
	//lp = sGetNextLoc(lp->next, required_zone, 
	//	cmb_space_needed, rescod, 3, var_facflg, ship_lane_id))
	//{

	//	pckrel_WriteTrc("EM: Loop 3 stoloc: %s . . .", lp->stoloc);
	//	if (sRecheckResourceAssignment(lp->stoloc,
	//		rescod,
	//		lp->rescod,
	//		chkmov_flag) != eOK)
	//	{
	//		pckrel_WriteTrc(" EM: Recheck of Resource Code failed");
	//		continue;
	//	}


	//	if (sigflg == BOOLEAN_TRUE ||
	//		(sigflg == BOOLEAN_FALSE &&
	//		lp->pndqvl == 0.0 &&  
	//		lp->asgflg == BOOLEAN_FALSE))
	//	{

	//		/* EM: 08/22/2008: Not desperate yet so don't take a loc that's too big */
	//		pckrel_WriteTrc(" EM: Checking if loc is too big: maxqvl: %f, too_big: %f", 
	//				lp->maxqvl, con_space_needed * 1.2);
	//		if (lp->maxqvl > (con_space_needed * 1.2) ) {
	//			pckrel_WriteTrc(" EM: This location is too big, maxqvl: %f", lp->maxqvl);
	//			continue;
	//		}

	//		if (!var_facflg)
	//		{ 
	//			pckrel_WriteTrc("EM: Best Fit Check");
	//			sEvaluateBestFit(&best_alloc_lp,
	//				lp,
	//				con_space_needed,
	//				stgflg,
	//				small_lane_size);
	//		}
	//		else
	//		{
	//			pckrel_WriteTrc("EM: Setting Best Alloc lp");
	//			best_alloc_lp = lp;
	//		}
	//	}
	//}

	//if (alloc_lp == NULL)
	//	alloc_lp = best_alloc_lp;

	//if (alloc_lp)
	//{
	//	/*EM 08/21/2008 passing alloc_lp->rescod does not match when the 
	//	hash table was set up, pasing passed in rescod to match */
	//	sAddToSpaceUsed(arecod,
	//		alloc_lp->rescod,							
	//		alloc_lp->maxqvl);

	//	/*sAddToSpaceUsed(arecod,
	//	rescod,							
	//	alloc_lp->maxqvl);*/

	//	return(alloc_lp);
	//}


	/*
	* Now go with the non-preferred locations
	*
	*/
	pckrel_WriteTrc("   EM: ...looking for any available  "
		"locations for rescod %s, facflg = %d",
		rescod, var_facflg);

	for (lp = sGetNextLoc(LocationList, required_zone, 
		cmb_space_needed, rescod, 4, var_facflg, ship_lane_id);
		lp && alloc_lp == NULL;
	lp = sGetNextLoc(lp->next, required_zone, 
		cmb_space_needed, rescod, 4, var_facflg, ship_lane_id))
	{
		if (sRecheckResourceAssignment(lp->stoloc,
			rescod,
			lp->rescod,
			chkmov_flag) != eOK)
		{
			pckrel_WriteTrc(" EM: Recheck of Resource Code failed");
			continue;
		}
		if (sigflg == BOOLEAN_TRUE ||
			(sigflg == BOOLEAN_FALSE &&
			lp->pndqvl == 0.0 &&  
			lp->asgflg == BOOLEAN_FALSE))
		{
			if (!var_facflg)
				sEvaluateBestFit(&best_alloc_lp,
				lp,
				con_space_needed,
				stgflg,
				small_lane_size);
			else
				best_alloc_lp = lp;
		}
	}

	if (alloc_lp == NULL)
		alloc_lp = best_alloc_lp;

	if (alloc_lp) {
		pckrel_WriteTrc("   EM: Loop 5 allocated loc: %s", alloc_lp->stoloc);
		sAddToSpaceUsed(
			arecod,
			rescod,
			alloc_lp->maxqvl);
	}

	return(alloc_lp);
}

static void sFlagPicksForSkip(ALLOC_QUEUE *Queue,
							  char *rescod,
							  char *arecod)
{
	ALLOC_QUEUE *qp, *qp_tmp;

	/* Save the first cmbcod that we get. */
	qp_tmp = Queue;

	for (qp = Queue; qp; qp = qp->next)
	{
		if (misCiStrncmp(qp_tmp->cmbcod, qp->cmbcod, CMBCOD_LEN))
			qp_tmp = qp;

		if (qp->processed)
			continue;

		if (strncmp(qp->pm_arecod, arecod, ARECOD_LEN) == 0 &&
			strncmp(qp->pm_rescod, rescod, RESCOD_LEN) == 0)
		{
			/* 
			* Need to set all of the hops for a cmbcod
			* to processed in order to skip the pick.
			*/
			for(qp = qp_tmp; 
				qp && !misCiStrncmp(qp_tmp->cmbcod, qp->cmbcod, CMBCOD_LEN);
				qp = qp->next)
			{
				/* 
				* This is used to hold on to the previous
				* pointer value so when we pop out of this loop
				* we can get back to where we need to be pointing
				* so that the outer loop can increment this and not
				* miss any of the pointer elements.
				*/
				qp_tmp = qp;

				qp->processed = 1;

				misTrc(T_FLOW, "setting to processed: \n"
					"   cmbcod '%s' \n"
					"   seqnum %ld \n"
					"   arecod '%s' \n"
					"   rescod '%s' ",
					qp_tmp->cmbcod,
					qp_tmp->seqnum,
					qp_tmp->pm_arecod,
					qp_tmp->pm_rescod);

			}

			/* 
			* Need to back up one here because the outer FOR loop
			* will set it to qp->next and we don't want to miss any.
			*/
			qp = qp_tmp;
		}
	}
	return;
}

/*
* sAllocateLocationsForCmbcod
*
* This routine is called for each cmbcod on the allocation queue
* It will determine if a resource location is needed for a cmbcod
* and any zone matching/space requirements for that location.
* Finally, it will determine the best resource location to allocate
* and allocate it.
*/
static long sAllocateLocationsForCmbcod(ALLOC_QUEUE *qp,
										AREA_LIST **AreaListHead,
										RULES_STRUCT *rules,
										DATA_STRUCT *data,
										long looking_specific)
{
	ALLOC_QUEUE *mov_qp, *nxt_mov_qp, *prv_mov_qp, *tmp_qp;
	LOCATION_LIST *LocationList, *alloc_lp;

	char jump_srcare[ARECOD_LEN+1];
	char jump_srcloc[STOLOC_LEN+1];
	char jump_dstare[ARECOD_LEN+1];
	char jump_dstloc[STOLOC_LEN+1];
	char jump_prvare[ARECOD_LEN+1];
	char jump_prvloc[STOLOC_LEN+1];
	char next_arecod[ARECOD_LEN+1];
	char from_area[ARECOD_LEN+1];
	char to_area[ARECOD_LEN+1];
	char to_loccod[LOCCOD_LEN+1];

	char src_wrkzon[WRKZON_LEN+1];
	char dst_wrkzon[WRKZON_LEN+1];
	char required_zone[WRKZON_LEN+1];
	long ret_status;
	long alloc_something;
	char work_rescod[RESCOD_LEN+1];
	char work_arecod[ARECOD_LEN+1];
	double con_space_needed, cmb_space_needed;
	long cmb_tot_pckqty;
	char ship_lane_id[31];
	char buffer[2000];

	pckrel_WriteTrc("        EM: In sAllocateLocationsForCmbcod!" );
	pckrel_WriteTrc("        EM:  ...processing combination code %s (ms=%ld) ", 
		qp->cmbcod, pckrel_ElapsedMs());

	/* zones that we have */
	memset(jump_srcare, 0, sizeof(jump_srcare));
	memset(jump_srcloc, 0, sizeof(jump_srcloc));
	memset(jump_dstare, 0, sizeof(jump_dstare));
	memset(jump_dstloc, 0, sizeof(jump_dstloc));
	memset(jump_prvare, 0, sizeof(jump_prvare));
	memset(jump_prvloc, 0, sizeof(jump_prvloc));
	memset(next_arecod, 0, sizeof(next_arecod));
	alloc_something = FALSE;

	/* Next, lets loop through and allocate the series */
	/* of jumps that can be made */

	mov_qp = qp;
	prv_mov_qp = NULL;
	while ((mov_qp != NULL) &&
		strncmp(mov_qp->cmbcod, qp->cmbcod, CMBCOD_LEN) == 0)
	{
		/* We loop through for a given instance of a cmbcod */

		misTrc(T_FLOW, "We loop here because the mov_qp is not null (%ld) "
			"and the cmbcod hasn't changed. ",
			mov_qp);

		misTrc(T_FLOW, "EM: For cmbcod %s the processed flag = %ld", 
			mov_qp->cmbcod,
			mov_qp->processed);

		if (mov_qp->processed)
		{
			/* The processed will be set if 
			* we have already considered this row...
			* skip processing if that is the case...
			*/
			mov_qp = mov_qp->next;
			continue;
		}

		memset(src_wrkzon, 0, sizeof(src_wrkzon));
		memset(dst_wrkzon, 0, sizeof(dst_wrkzon));

		if (mov_qp == qp)
		{
			/* if we are on our first row */
			strncpy(jump_srcare, qp->srcare, ARECOD_LEN);
			strncpy(jump_srcloc, qp->srcloc, STOLOC_LEN);
			strncpy(jump_dstare, qp->pm_arecod, ARECOD_LEN);
			strncpy(jump_dstloc, qp->pm_stoloc, STOLOC_LEN);

			tmp_qp = mov_qp->next;
			if ((tmp_qp == NULL) ||
				tmp_qp->seqnum == qp->seqnum ||
				strncmp(tmp_qp->cmbcod, qp->cmbcod, CMBCOD_LEN) != 0)
			{
				memset(next_arecod, 0, sizeof(next_arecod));
			}
			else
			{
				nxt_mov_qp = tmp_qp;
				strncpy(next_arecod, tmp_qp->pm_arecod, ARECOD_LEN);
			}
			misTrc(T_FLOW, "EM: For first row:");
			misTrc(T_FLOW, "EM:  jump_srcare = '%s' ", jump_srcare);
			misTrc(T_FLOW, "EM:  jump_srcloc = '%s' ", jump_srcloc);
			misTrc(T_FLOW, "EM:  jump_dstare = '%s' ", jump_dstare);
			misTrc(T_FLOW, "EM:  jump_dstloc = '%s' ", jump_dstloc);
			if(misTrimLen(next_arecod, 1))
				misTrc(T_FLOW, "        EM:  next_arecod = '%s' ", next_arecod);
			else
				misTrc(T_FLOW, "        EM:  next_arecod = NULL ");
		}
		else
		{
			/* We are not on the first row */
			strncpy(jump_srcare, prv_mov_qp->pm_arecod, ARECOD_LEN);
			strncpy(jump_srcloc, prv_mov_qp->pm_stoloc,	STOLOC_LEN);
			strncpy(jump_dstare, mov_qp->pm_arecod, ARECOD_LEN);
			strncpy(jump_dstloc, mov_qp->pm_stoloc, STOLOC_LEN);
			tmp_qp = mov_qp->next;
			if (tmp_qp == NULL || tmp_qp->seqnum == qp->seqnum ||
				strncmp(tmp_qp->cmbcod, qp->cmbcod, CMBCOD_LEN) != 0)
			{
				memset(next_arecod, 0, sizeof(next_arecod));
			}
			else
			{
				nxt_mov_qp = tmp_qp;
				strncpy(next_arecod, tmp_qp->pm_arecod, ARECOD_LEN);
			}

			misTrc(T_FLOW, "EM: For the next row:");
			misTrc(T_FLOW, "EM:   jump_srcare = '%s' ", jump_srcare);
			misTrc(T_FLOW, "EM:   jump_srcloc = '%s' ", jump_srcloc);
			misTrc(T_FLOW, "EM:   jump_dstare = '%s' ", jump_dstare);
			misTrc(T_FLOW, "EM:   jump_dstloc = '%s' ", jump_dstloc);
			if(misTrimLen(next_arecod, 1))
				misTrc(T_FLOW, "  next_arecod = '%s' ", next_arecod);
			else
				misTrc(T_FLOW, "  next_arecod = NULL ");
		}

		if (misTrimLen(jump_dstare, ARECOD_LEN) > 0 &&
			misTrimLen(jump_dstloc, STOLOC_LEN) == 0)
		{
			memset(from_area, 0, sizeof(from_area));
			memset(to_area, 0, sizeof(to_area));
			memset(to_loccod, 0, sizeof(to_loccod));

			misTrc(T_FLOW, "EM: We have a dstare (%s), but no dstloc ",
				jump_dstare);

			/* If the next two steps are for the same 
			* area then we must be moving out and back 
			* in through staging or something similar 
			* ... if so check to see if this is really 
			* a move within the same aisle which means 
			* we can just change the pick work to be a 
			* simple move ... this code is assuming 
			* that if we are moving between two 
			* locations in the same area then the work 
			* zone rules are automatically enforced 
			*/

			if (strncmp(jump_dstare,next_arecod, ARECOD_LEN) == 0)
			{
				misTrc(T_FLOW, "EM: Jump destination area is the same as the next"
					"area ('%s') ",
					jump_dstare);

				misTrc(T_FLOW, "about to get work zone for src location '%s' ",
					jump_srcloc);

				ret_status = sGetLocationWrkzon(jump_srcloc,
					src_wrkzon);

				if (ret_status != eOK)
				{
					pckrel_WriteTrc("        EM: Error selecting srcloc "
						"wrkzon - status = %d - "
						"SKIPPING",
						ret_status);
					continue;
				}

				misTrc(T_FLOW, "EM: Source work zone is '%s' ",
					src_wrkzon);

				/* Since we really expanded our 
				* intermediate locations we really 
				* need to see if a next area exists 
				* after the arecod in variable 
				* next_arecod. If we are at the end 
				* of the list or change cmbcods then 
				* use dstloc 
				*/

				misTrc(T_FLOW, "about to get work zone for dst location.");

				ret_status = sGetLocationWrkzon(sGetDestLoc(qp),
					dst_wrkzon);

				if (ret_status != eOK)
				{
					pckrel_WriteTrc("Error selecting dstloc"
						"wrkzon, stat: %d, SKIPPING",
						ret_status);
					prv_mov_qp = mov_qp;
					mov_qp = mov_qp->next;
					continue;
				}

				misTrc(T_FLOW, "EM: destination work zone is '%s' ",
					dst_wrkzon);

				if (strcmp(src_wrkzon, dst_wrkzon) != 0)
				{
					strncpy(from_area, jump_srcare, ARECOD_LEN);
					strncpy(to_area, jump_dstare, ARECOD_LEN);
				}
				else
				{
					sprintf(buffer,
						"delete pckmov "
						" where cmbcod = '%s' "
						"   and (seqnum='%ld' or seqnum='%ld') ",
						mov_qp->cmbcod,
						mov_qp->seqnum,
						nxt_mov_qp->seqnum);
					ret_status = sqlExecStr(buffer, NULL);
					if (ret_status != eOK)
					{
						pckrel_WriteTrc("Error deleting "
							"pckmov by cmbcod");
						return (ret_status);
					}
					mov_qp->processed = 1;
					nxt_mov_qp->processed = 1;

					alloc_something = TRUE;
				}
			}
			else
			{
				strncpy(from_area, jump_srcare, ARECOD_LEN);
				strncpy(to_area, jump_dstare, ARECOD_LEN);

				misTrc(T_FLOW, "EM: setting from_area = '%s' and to_area = '%s'",
					from_area,
					to_area);

			}

			/* If we have a needed area code, */
			/* then try to allocate */

			if (strlen(to_area))
			{
				memset(work_rescod, '^', RESCOD_LEN);
				work_rescod[RESCOD_LEN] = 0;

				misTrc(T_FLOW, "EM: About to get location list for area %s ",
					to_area);

				LocationList = NULL;
				ret_status = sGetLocationListForArea(to_area,
					rules->ord_by,
					qp->ship_id,
					AreaListHead,
					&LocationList);
				if (ret_status != eOK)
				{
					pckrel_WriteTrc("Error loading locations - %d",
						ret_status);
					pckrel_WriteTrc("*Order-By "
						"policy = %s, Area = %s",
						rules->ord_by, to_area);
					return (ret_status);
				}

				misTrc(T_FLOW, "Check Pointers: \n"
					"   Location List = '%ld' \n"
					"       Area List = '%ld' \n",
					LocationList,
					AreaListHead);

				memset(required_zone, 0, sizeof(required_zone));
				if (sRequiresSameSrcZone(from_area,
					to_area,
					qp->lodlvl,
					qp->dstare))
				{
					if (strcmp(src_wrkzon, "") == 0)
					{
						ret_status = sGetLocationWrkzon(jump_srcloc,
							src_wrkzon);
						if (ret_status != eOK)
						{
							misTrc(0, "SAME-ZONE logic is being used but "
								"we failed to get a workzone.");
							continue;
						}
					}
					strncpy(required_zone, src_wrkzon, WRKZON_LEN);
				}
				else if (sRequiresSameDstZone(from_area,
					to_area,
					qp->lodlvl,
					qp->dstare))
				{
					ret_status = sGetLocationWrkzon(sGetDestLoc(qp),
						dst_wrkzon);
					if (ret_status != eOK)
					{
						misTrc(0, "EM: SAME-ZONE-SECOND logic is being used but "
							"we failed to get a destination workzone.");
					}
					else
					{
						strncpy(required_zone, dst_wrkzon, WRKZON_LEN);
					}
				}

				/* Loop through the entire result set 
				* and find what we need for the whole
				* resource code and what we need for 
				* the combination code 
				*
				* Since we are calculating for the 
				* entire rescod we should compare to 
				* the row we are on (mov_row) and not 
				* the top pointer (row) (I THINK). 
				* We are in the same cmbcod but there 
				* may be a different rescod associated 
				* to every arecod I think another 
				* assumption made here was that 
				* anything but a pallet pick would 
				* have only one wrkref per cmbcod. 
				* We will change that to accumulate 
				* the qty for pckqty and cmbcod qvl 
				* so we can put out a qvlwrk if we 
				* have to. Make sure the cmb_space 
				* and cmb_tot stay on the same level 
				* or seqnums are equal 
				*/

				cmb_tot_pckqty = 0;
				con_space_needed = cmb_space_needed = 0.0;
				memset(work_rescod, 0, sizeof(work_rescod));
				misTrimcpy(work_rescod, mov_qp->pm_rescod, RESCOD_LEN);

				memset(work_arecod, 0, sizeof(work_arecod));
				misTrimcpy(work_arecod, mov_qp->pm_arecod, ARECOD_LEN);

				sGetSpaceReqs(mov_qp,
					&cmb_space_needed,
					&con_space_needed,
					&cmb_tot_pckqty);

				pckrel_WriteTrc("        EM: con_space_needed: %f, cmb_space_needed: %f", con_space_needed, cmb_space_needed);
				misTrc(T_FLOW, "EM: About to get a location list pointer using "
					"sFindResourceLocation");

				alloc_lp = sFindResourceLocation(LocationList,
					mov_qp->pm_rescod,
					rules->small_lane,
					con_space_needed,
					cmb_space_needed,
					required_zone,
					mov_qp->sigflg,
					mov_qp->stgflg,
					rules->chkmov_flag, 
					mov_qp->var_facflg, 
					strcmp(mov_qp->pm_arecod, "SSTG") == 0 ? mov_qp->ship_lane_id : "", work_arecod);
				if (alloc_lp != NULL)
				{
					pckrel_WriteTrc("  EM: Best Fit: Using location - %s", 
						alloc_lp->stoloc);
					pckrel_WriteTrc("  EM: Best Fit: ...setting cmbcod %s, "
						"to stoloc %s (ms=%d)",
						mov_qp->cmbcod,
						alloc_lp->stoloc,
						pckrel_ElapsedMs());
					sprintf(buffer,
						"update pckmov "
						"   set stoloc = '%s' "
						" where rowid = '%s' ",
						alloc_lp->stoloc,
						mov_qp->pm_rowid);
					ret_status = sqlExecStr(buffer, NULL);
					if (ret_status != eOK)
					{
						pckrel_WriteTrc("EM: Error updating "
							"pckmov by cmb");
						return (ret_status);
					}
					else
					{
						misTrimcpy(mov_qp->pm_stoloc, alloc_lp->stoloc, STOLOC_LEN);
					}
 
					if (mov_qp->sigflg == BOOLEAN_TRUE)
					{
						pckrel_WriteTrc("      Best Fit: SIGFLAG is TRUE"); 
						sprintf(buffer,
							"insert into qvlwrk "
							" (stoloc,prtnum,untqty,"
							"pndqvl, prt_client_id) "
							" values"
							" ('%s','%s','%ld','%f','%s') ",
							alloc_lp->stoloc,
							mov_qp->prtnum,
							cmb_tot_pckqty,
							cmb_space_needed,
							mov_qp->prt_client_id);
						ret_status = sqlExecStr(buffer,
							NULL);
						if (ret_status != eOK)
						{
							pckrel_WriteTrc("EM: Error inserting "
								"qvlwrk");
							return (ret_status);
						}

						pckrel_WriteTrc("     EM: ...adding %f to pndqvl "
							"for %s (ms=%d)",
							cmb_space_needed,
							alloc_lp->stoloc,
							pckrel_ElapsedMs());
						sprintf(buffer,
							"[update locmst "
							"    set pndqvl = pndqvl + '%f' "
							"  where stoloc = '%s'] ",
							cmb_space_needed,
							alloc_lp->stoloc);

						/* Now, update the result set */
						/* values since we may re-use */
						/* the locations */
						alloc_lp->pndqvl += cmb_space_needed;
					}
					else
					{
						pckrel_WriteTrc("      EM: Best Fit: SIGFLAG is FALSE"); 
						if (strlen(mov_qp->ship_lane_id) ) 					{
							if (!strstr(alloc_lp->ship_lane_ids, mov_qp->ship_lane_id)) 
							{
								pckrel_WriteTrc("  EM: Assigning pallet group: ship_lane_id %s  to stoloc: %s", 
									mov_qp->ship_lane_id, alloc_lp->stoloc);
								strcat(alloc_lp->ship_lane_ids, "_");
								strcat(alloc_lp->ship_lane_ids, mov_qp->ship_lane_id);
								sprintf(buffer,
									"assign resource code "
									" where delta_pndqvl =  '%d' "
									"   and rescod = '%s' "
									"   and stoloc = '%s' ",
									1,
									mov_qp->pm_rescod,
									alloc_lp->stoloc);

								/* Now, update the result set */
								/* values since we may re-use */
								/* the locations */

								strcpy(alloc_lp->rescod, mov_qp->pm_rescod);
								alloc_lp->pndqvl += 1;
							}
							else
								buffer[0] = 0;
						}
						else
						{
							sprintf(buffer,
								"assign resource code "
								" where delta_pndqvl =  '%f' "
								"   and rescod = '%s' "
								"   and stoloc = '%s' ",
								cmb_space_needed,
								mov_qp->pm_rescod,
								alloc_lp->stoloc);

							/* Now, update the result set */
							/* values since we may re-use */
							/* the locations */

							strcpy(alloc_lp->rescod, mov_qp->pm_rescod);
							alloc_lp->pndqvl += cmb_space_needed;
						}
					}
					if (strlen(buffer)) {
						ret_status = srvInitiateCommand(buffer, NULL);
						if (ret_status != eOK)
						{
							pckrel_WriteTrc("  EM:Error updating locmst..");
							return(ret_status);
						}
						strncpy(jump_prvloc, alloc_lp->stoloc, STOLOC_LEN);
						alloc_something = TRUE;
					}
				}
				else if (looking_specific == TRUE)
				{
					pckrel_WriteTrc("      Best Fit: looking_specific is TRUE"); 
					pckrel_WriteTrc("Unable to find "
						"a location on specific read,"
						"returning error ...");
					return (eINT_PICK_REL_NO_LOC_AVAIL);
				}
				else
				{
					misTrc(T_FLOW, "BestFie: Location list pointer was NULL using "
						"sFindResourceLocation");

					/* Fast forward past this */
					/* combination code */
					while ((mov_qp != NULL) &&
						strncmp(mov_qp->cmbcod, 
						qp->cmbcod, CMBCOD_LEN) == 0)
					{
						mov_qp = mov_qp->next;
					}
					alloc_something = FALSE;

					strncpy(jump_prvloc, jump_dstloc, STOLOC_LEN);

					pckrel_WriteTrc("        EM: Could not allocate in "
						"%s for shipment %s, "
						"wrkref %s, rescod %s",
						to_area,
						qp->ship_id,
						qp->wrkref,
						qp->pm_rescod);
					pckrel_WriteTrc("     EM:  - flagging remaining matching picks"
						" for skip");
					sFlagPicksForSkip(qp->next,
						qp->pm_rescod,
						to_area);

				}
			}

		}
		else
		{
			strncpy(jump_prvloc, jump_dstloc, STOLOC_LEN);
		}
		prv_mov_qp = mov_qp;
		if (mov_qp)
			mov_qp = mov_qp->next;
	}

	misTrc(T_FLOW, "Done with this cmbcod in sAllocateLocationForCmbcod, "
		"returning eOK");

	return(eOK);
}

LIBEXPORT 
RETURN_STRUCT *varProcessPickReleaseAllocation(char *pcksts_i,
											   char *cmbcod_i,
											   char *wrkref_i,
											   char *ship_id_i,
											   moca_bool_t *comflg_i,
											   char *schbat_i,
											   moca_bool_t *cache_locs_i,
											   moca_bool_t *skip_rplchk_i)

{    
	char pcksts[PCKSTS_LEN+1];
	char schbat[SCHBAT_LEN+1];

	char save_relgrp[RTSTR1_LEN + 1];
	char comp_relgrp[RTSTR1_LEN + 1];
	char save_cmbcod[CMBCOD_LEN + 1];
	char comp_ship_id[SHIP_ID_LEN + 1];
	char save_ship_id[SHIP_ID_LEN + 1];

	char buffer[2000];
	long resources_required;
	long ret_status;
	long process_relgrp;
	long commit_as_we_go;
	long looking_specific;
	long resource_table_filled = 0;
	long skip_rplchk;

	DATA_STRUCT *data;
	RULES_STRUCT *rules;
	ALLOC_QUEUE *Queue, *qp;
	static AREA_LIST *AreaListHead;

	memset(pcksts, 0, sizeof(pcksts));
	memset(schbat, 0, sizeof(schbat));

	pckrel_GetConfig(&data, &rules, 0);

	pckrel_WriteTrc("   EM: in varProcessPickReleaseAllocation!" );
	pckrel_WriteTrc("   EM: pcksts_i: %s, cmbcod_i: %s, wrkref_i: %s, ship_id_i: %s", pcksts_i, cmbcod_i, wrkref_i, ship_id_i );
	pckrel_WriteTrc("   EM: comflg_i: %ld, schbat_i: %s, cache_locs_i: %ld, skip_rplchk_i: %ld", comflg_i, schbat_i, cache_locs_i, skip_rplchk_i );


	/*
	* Set the gCacheLocations global variable
	* - this will control whether or not the 
	* "sFreeLocationLists" actually does anything
	*/
	if (cache_locs_i)
	{
		gCacheLocations = *cache_locs_i;
		if (*cache_locs_i == 0 &&
			(!schbat_i || misTrimLen(schbat_i, SCHBAT_LEN) == 0))
		{
			/* 
			* Don't really need to do anything...call was made 
			* (by process pick release) to
			* reset the cache flag only.  The global flag is set to 0,
			* which will cause sFreeLocationLists to free the location
			* list.
			*/

			if (AreaListHead)
				sFreeLocationLists(&AreaListHead);

			return(srvResults(eOK, NULL));
		}
	}
	else
	{
		gCacheLocations = 0;
	}

	if (skip_rplchk_i && *skip_rplchk_i)
		skip_rplchk = *skip_rplchk_i;
	else
		skip_rplchk = 0;

	if (AreaListHead)
		sFreeLocationLists(&AreaListHead);

	if (schbat_i && misTrimLen(schbat_i, SCHBAT_LEN))
	{
		misTrimcpy(schbat, schbat_i, SCHBAT_LEN);
	}
	else
	{
		return(APPMissingArg("schbat"));
	}

	if (pcksts_i && misTrimLen(pcksts_i, PCKSTS_LEN))
		misTrimcpy(pcksts, pcksts_i, PCKSTS_LEN);
	else
		strcpy(pcksts, PCKSTS_PENDING);

	commit_as_we_go = BOOLEAN_FALSE;
	if (comflg_i)
	{
		if (*comflg_i == BOOLEAN_TRUE)    
			commit_as_we_go = BOOLEAN_TRUE;
	}

	ret_status = sLoadAllocationQueue(&Queue,
		&looking_specific,
		cmbcod_i,
		ship_id_i,
		wrkref_i,
		pcksts,
		schbat);
	if (ret_status != eOK)
	{
		return(srvResults(ret_status, NULL));
	}

	/* Loop through and apply only to replenishments and completely 
	* allocated shipments 
	*/

	memset(save_relgrp, 0, sizeof(save_relgrp));
	memset(comp_relgrp, 0, sizeof(comp_relgrp));
	memset(save_cmbcod, 0, sizeof(save_cmbcod));


	for (qp = Queue; qp; qp = qp->next)
	{
		pckrel_WriteTrc("  EM: Main Loop Begin . . .");
		pckrel_WriteTrc("   EM: qp->wrkref: %s, qp->cmbcod: %s", qp->wrkref, qp->cmbcod );

		/* Process each combination code ... */
		if (strncmp(qp->cmbcod, save_cmbcod, CMBCOD_LEN) != 0)
		{

			strncpy(save_cmbcod, qp->cmbcod, CMBCOD_LEN);

			/* First, if this is a pick ... we need to make sure 
			* that we are only releasing if the entire group has 
			* been allocated 
			*/

			resources_required = FALSE;

			/* Now, lets pull out the release grouping to see 
			* if we should even consider releasing this stuff 
			*/

			if (misCiStrncmp(rules->relgrp, "SHIP_ID", strlen("SHIP_ID")) == 0)
			{
				memset(comp_relgrp, 0, sizeof(comp_relgrp));
				if (strncmp(qp->wrktyp,
					WRKTYP_REPLENISH, WRKTYP_LEN) == 0)
				{
					strncpy(comp_relgrp, qp->schbat, SCHBAT_LEN);
				}
				else
				{
					if (strlen(qp->wkonum) != 0)
					{
						strncpy(comp_relgrp, qp->wkonum, WKONUM_LEN);
					}
					else
					{
						strncpy(comp_relgrp, qp->ship_id, SHIP_ID_LEN);
					}
				}
			}
			else if (misCiStrncmp(rules->relgrp,
				"SCHBAT", strlen("SCHBAT")) == 0)
			{
				memset(comp_relgrp, 0, sizeof(comp_relgrp));
				if (strncmp(qp->wrktyp, WRKTYP_REPLENISH, WRKTYP_LEN) == 0)
				{
					strncpy(comp_relgrp, qp->schbat, SCHBAT_LEN); 
				}
				else
				{
					strncpy(comp_relgrp, qp->schbat, SCHBAT_LEN);
				}
			}
			else
			{
				process_relgrp = TRUE;
			}

			if (strncmp(save_relgrp, comp_relgrp, RTSTR1_LEN) != 0)
			{

				strncpy(save_relgrp, comp_relgrp, RTSTR1_LEN);

				/* Only process this group if we have allocated */
				/* the same amount of product that the group of */
				/* lines are for ... */

				ret_status = pckrel_PrcReleaseGroup(&process_relgrp,
					qp->ship_id,
					qp->schbat,
					qp->wkonum,
					qp->wkorev,
					qp->client_id,
					save_relgrp,
					qp->wrkref,
					skip_rplchk);
				if (ret_status != eOK)
				{
					sFreeLocationLists(&AreaListHead);
					sFreeAllocationQueue(&Queue);
					return (srvResults(ret_status, NULL));
				}
			}
			/* Now, if we think we are supposed to process, make 
			* sure the REGISTER SHIPMENT ALLOCATION COMPLETE has 
			* been called ... this used to try and process only if 
			* we were is split mode, but if you think about it, we 
			* always need to check because in single mode, we may 
			* read and process a bunch of stuff at the top, read 
			* again in the bottom half and have different data 
			*/

			/* 
			*  We only make the checks below if we don't have a wkonum
			*/
			if (process_relgrp == TRUE &&
				strlen(qp->wkonum) == 0 &&
				strncmp(qp->wrktyp, WRKTYP_PICK, WRKTYP_LEN) == 0)
			{
				memset(comp_ship_id, 0, sizeof(comp_ship_id));
				sprintf(comp_ship_id, "%s", qp->ship_id);
				if (strcmp(comp_ship_id, save_ship_id) != 0)
				{
					sprintf(buffer,
						"select alcdte "
						"  from shipment "
						" where alcdte is not null "
						"	and ship_id = '%s' ",
						qp->ship_id);
					ret_status = sqlExecStr(buffer, NULL);
					if (ret_status != eOK &&
						ret_status != eDB_NO_ROWS_AFFECTED)
					{
						sFreeAllocationQueue(&Queue);
						pckrel_WriteTrc("Error reading for shipment."
							"alcdte ...");
						return (srvResults(ret_status, NULL));
					}
					if (ret_status != eOK)
					{
						process_relgrp = FALSE;
					}
					else
					{
						memset(save_ship_id, 0, sizeof(save_ship_id));
						sprintf(save_ship_id, "%s", qp->ship_id);
					}
				}
			}
			/*
			* Now, if we should release this grouping of picks, 
			* then begin processing them 
			*/

			if (process_relgrp == TRUE)
			{
				pckrel_WriteTrc("   EM: processing relgrp . . ." );
				if (!resource_table_filled)
				{
					pckrel_WriteTrc("   EM: populating resource table . . ." );
					sSetResourceSpaceRequirements(Queue);
					resource_table_filled = 1;
				}

				ret_status = sAllocateLocationsForCmbcod(qp,
					&AreaListHead,
					rules,
					data,
					looking_specific);

				if (ret_status != eOK)
				{
					pckrel_WriteTrc("  ERROR during cmbcod allocation: %d",
						ret_status);
					sFreeLocationLists(&AreaListHead);
					sFreeAllocationQueue(&Queue);
					sFreeSpaceRequiredTable();
					return(srvResults(ret_status, NULL));
				}

			}
			else if (looking_specific == TRUE)
			{
				pckrel_WriteTrc("  Release group not ready on "
					"specific read, returning error ...");

				sFreeLocationLists(&AreaListHead);
				sFreeAllocationQueue(&Queue);
				sFreeSpaceRequiredTable();
				return (srvResults(eINT_PICK_REL_GROUP_NOT_READY, NULL));
			}
		}

		if (commit_as_we_go)
			sqlCommit();

		pckrel_WriteTrc("  EM: Main Loop End . . .");
	} /* End Loop Through AQ */

	pckrel_WriteTrc("  EM: PROGRAM END . . .");

	sFreeLocationLists(&AreaListHead);
	sFreeAllocationQueue(&Queue);
	if (resource_table_filled)
		sFreeSpaceRequiredTable();

	return(srvResults(eOK, NULL));
}
