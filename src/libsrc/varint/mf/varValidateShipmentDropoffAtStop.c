static const char *rcsid = "$Id: varValidateShipmentDropoffAtStop.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>


#define CANNOT_COMBINE_SHIPMENT(FIELD) \
        srvErrorResults(eINT_INVALID_STOP_COMBINATION, \
                    "^errnum^ - " \
                    " Cannot add shipment to stop because the ^field^ doesn't match other shipments or the stop.  " \
                    " Check the STOP-CONSOLIDATION-RULES policy.", \
                    "errnum", COMTYP_INT, eINT_INVALID_STOP_COMBINATION, NO_MLS_LOOKUP, \
                    "field", COMTYP_CHAR, FIELD, MLS_LOOKUP, \
                    NULL)

/*
 * This routine checks the assignment of a shipment to a stop.
 *
 * It verifies that the shipment can be combined with other shipments
 * on the stop and there there are no other stops intermixed on the
 * loads for the shipment.
 *
 * Any other site specific checks should be able to be
 * accomplished as a trigger on this command.
 *
 */

LIBEXPORT
RETURN_STRUCT *  varValidateShipmentDropoffAtStop( char *  stop_id_i,
                                                   char *  ship_id_i )
{
    RETURN_STRUCT *  ret_data                    = NULL;
    mocaDataRes *    res                         = NULL;
    mocaDataRow *    row                         = NULL;
    mocaDataRes *    shipRes                     = NULL;
    mocaDataRow *    shipRow                     = NULL;

    long             ret_status                  = 0;
    long             j                           = 0;

    short            verify_count                = 0;

    char             field_names[20][20]         = {'\0'};
    char             verify_strings[20][50]      = {'\0'};
    char             sqlbuffer[5000]             = {'\0'};
    char             ship_id[SHIP_ID_LEN+1]      = {'\0'};

    /* Validate required arguments */
    if ( !stop_id_i || strlen(misTrim(stop_id_i)) == 0)
    {
        return (APPMissingArg( "stop_id" ));
    }
    if ( !ship_id_i || strlen(misTrim(ship_id_i)) == 0)
    {
        return(APPMissingArg( "ship_id_i" ));
    }
    misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);

    /* Get the policy that tells us which fields need to match in order to */
    /* allow shipments to be consolidated on a single stop. */
    ret_status = sqlExecBind("SELECT   rtstr1 "
                             "FROM     poldat "
                             "WHERE    polcod = :shipping "
                             "AND      polvar = :polmisc "
                             "AND      polval = :stop_cons "
                             "ORDER BY srtseq",
                             &res, NULL,
                             "shipping", COMTYP_CHARPTR, 
                                sizeof(POLCOD_SHIPPING), POLCOD_SHIPPING, 0,
                             "polmisc",   COMTYP_CHARPTR, 
                                sizeof(POLVAR_MISC), POLVAR_MISC, 0,
                             "stop_cons", COMTYP_CHARPTR, 
                                sizeof(POLVAL_STOP_CONSOLIDATION), 
                                POLVAL_STOP_CONSOLIDATION, 0,
                             NULL );

    if (eOK != ret_status && eDB_NO_ROWS_AFFECTED != ret_status)
    {
	sqlFreeResults(res);
        ret_data = srvResults( eERROR, NULL );
        return (ret_data);
    }

    verify_count = 0;
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        strncpy(field_names[verify_count], sqlGetString(res, row, "rtstr1"),
                misTrimLen(sqlGetString(res, row, "rtstr1"), RTSTR1_LEN) );
        verify_count++;
    }

    sqlFreeResults(res);

    /* Check if the stop passed in exists.  If it does we will validate */
    /* that the shipments carcod and adr_id match.  If it doesn't exist */
    /* we will set the these values to the values from the first shipment. */
    /* That will enforce that carcods and adr_ids for shipments int the */
    /* list match. */
        ret_status = sqlExecBind("SELECT stop_id "
                                 "FROM   stop "
                                 "WHERE  stop_id         = :stop_id ",
                                 &res, NULL,
                                 "stop_id", COMTYP_STRING, strlen(stop_id_i),
                                 stop_id_i, 0, NULL );
/*
    if ( eOK == intValTablePK( "stop", "stop_id", stop_id_i, &ret_data ) )
*/
    if ( eOK == ret_status )
    {
        /* Get some needed values from the stop and car_move */
        ret_status = sqlExecBind("SELECT stop.adr_id as rt_adr_id, "
                                 "       car_move.carcod "
                                 "FROM   stop, car_move "
                                 "WHERE  car_move.car_move_id = stop.car_move_id "
                                 "AND    stop.stop_id         = :stop_id ",
                                 &res, NULL,
                                 "stop_id", COMTYP_STRING, strlen(stop_id_i),
                                 stop_id_i, 0, NULL );
        

        if (eOK != ret_status)
        {
	    sqlFreeResults(res);
            ret_data = srvResults( ret_status, NULL );
            return (ret_data);
        }

        row = sqlGetRow(res);
        for (j = 0; j < verify_count; j++)
        {
            /* Default the verfiy strings from the stop for the case where */
            /* no shipments exists yet */
            if(strcmp(field_names[j], "rt_adr_id") == 0 
            || strcmp(field_names[j], "carcod") == 0 
            || strcmp(field_names[j], "srvlvl") == 0)
            {
		/*  If the srvlvl is required to be checked, grab it from the 
                **  shipment.
		*/
                if(strcmp(field_names[j], "srvlvl") == 0)
                {
                    ret_status = sqlExecBind("SELECT srvlvl "
                                 "FROM   shipment "
                                 "WHERE  ship_id         = :ship_id ",
                                 &shipRes, NULL,
                                 "ship_id", COMTYP_STRING, strlen(ship_id_i),
                                 ship_id_i, 0, NULL );
        

                    if (eOK != ret_status)
                    {
	                sqlFreeResults(res);
	                sqlFreeResults(shipRes);
                        ret_data = srvResults( ret_status, NULL );
                        return (ret_data);
                    }

                    shipRow = sqlGetRow(shipRes);
                    strcpy(verify_strings[j], 
                           sqlGetString(shipRes, shipRow, field_names[j]));
                }
                else
                {
                    strcpy(verify_strings[j], 
                       sqlGetString(res, row, field_names[j]));
                }
            }
        }

        sqlFreeResults(res);
    }

    /* Get information from any one of the existing shipments on this stop, */
    /* if there are any assigned, to use for stop consolidation checks.     */

    ret_status = sqlExecBind("select   sh.shpsts, sh.ship_id, sh.carcod, "
                             "         sh.srvlvl, sh.rt_adr_id, "
                             "         sh.stgdte, sh.stop_id, "
                             "         o.rtcust "
                             "from     shipment_line sl, "
                             "         ord o,  "
			     "         shipment sh "
                             "where    o.client_id    = sl.client_id "
                             "and      o.ordnum       = sl.ordnum "
                             "and      sl.ship_id     = sh.ship_id "
                             "and      sh.stop_id     = :stop_id ",
                             &res, NULL,
                             "stop_id", COMTYP_STRING, strlen(stop_id_i), 
                             stop_id_i, 0, NULL );

    if (eOK != ret_status && eDB_NO_ROWS_AFFECTED != ret_status)
    {
        ret_data = srvResults( eERROR, NULL );
	sqlFreeResults(res);
        return (ret_data);
    }

    row = sqlGetRow(res);

    if (eDB_NO_ROWS_AFFECTED != ret_status)
    {
        for (j = 0; j < verify_count; j++)
        {
        misTrc (T_FLOW, "Field Name: %s, DB: %s, string %s", 
			field_names[j], 
			sqlGetString(res, row, field_names[j]), 
			verify_strings[j]);

            strcpy(verify_strings[j], sqlGetString(res, row, field_names[j]));
        misTrc (T_FLOW, "New-Field Name: %s, DB: %s, string %s", 
			field_names[j], 
			sqlGetString(res, row, field_names[j]), 
			verify_strings[j]);

        }
    }

    sqlFreeResults(res);

    /* Get some shipment information about the shipment we are validating */
    ret_status = sqlExecBind("select   sh.shpsts, sh.ship_id, sh.carcod, "
                             "         sh.srvlvl, sh.rt_adr_id, "
                             "         sh.stgdte, o.rtcust "
                             "from     shipment_line sl, shipment sh, "
                             "         ord o "
                             "where    o.client_id    = sl.client_id "
                             "and      o.ordnum       = sl.ordnum "
                             "and      sh.ship_id     = sl.ship_id "
                             "and      sl.ship_id     = :ship_id ",
                             &res, NULL,
                             "ship_id", COMTYP_STRING, strlen(ship_id_i),
                             ship_id_i, 0, NULL );
    

    if (eOK != ret_status)
    {
	sqlFreeResults(res);
        ret_data = srvResults( ret_status, NULL );
        return (ret_data);
    }

    row = sqlGetRow(res);

    for (j = 0; j < verify_count; j++)
    {
        misTrc (T_FLOW, "Field Name: %s, DB: %s, string %s", 
			field_names[j], 
			sqlGetString(res, row, field_names[j]), 
			verify_strings[j]);

        if (strlen(misTrim(sqlGetString(res, row, field_names[j]))) > 0 &&
            strcmp(sqlGetString(res, row, field_names[j]), 
                                verify_strings[j]) != 0)
        {
            ret_data = CANNOT_COMBINE_SHIPMENT(field_names[j]);
            sqlFreeResults(res);
            return (ret_data);
        }
    }

    sqlFreeResults(res);
    
    /*
     * 8/21/2000 - BG
     * Removed the last check here that appeared to be checking to see if
     * all of the stops for any load assigned to this shipment are the 
     * same.  This would not be true in a permanent load parcel scenario,
     * and besides, the query was not performing well.
     */

    ret_data = srvResults( eOK, NULL );

    return (ret_data);
}
