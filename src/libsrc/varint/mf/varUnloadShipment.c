static const char *rcsid = "$Id: varUnloadShipment.c,v 1.3 2002/05/28 00:30:17 prod Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *  DESCRIPTION  :  This function will unload the entire shipment to the 
 *                  staging area.  It will put the shipment into a STAGED
 *                  status so that you will have to Build Carrier Move,
 *                  Print Paperwork and Load Stop the shipment again.
 *
 *#END************************************************************************/

#include <moca_app.h>
#include <stdio.h>
#include <applib.h>
#include <dcsgendef.h>
#include <dcscolwid.h>
#include <dcserr.h>
#include <dcslib.h>
#include <varerr.h>
#include <common.h>
#include <stdlib.h>



LIBEXPORT 
RETURN_STRUCT  *varUnloadShipment( char *ship_id_i ,
				   char *stoloc_i ) 
{
    RETURN_STRUCT  *returnData=NULL  ;
    RETURN_STRUCT  *CurPtr = NULL ;

    char stoloc[STOLOC_LEN+1];
    char trlr_stoloc[STOLOC_LEN+1];
    char ship_id[SHIP_ID_LEN+1] ;
    char tmp_ship_id[SHIP_ID_LEN+1] ;
    char arecod[ARECOD_LEN+1] ;
    char rescod[RESCOD_LEN+1] ;
    char loc_arecod[ARECOD_LEN+1] ;
    char loc_rescod[RESCOD_LEN+1] ;
    char lodnum[LODNUM_LEN + 1] ;
    char stop_id[STOP_ID_LEN + 1] ;

    char buffer[3500] ;
    char where_clause[500] ;

    long ret_status ;
    long stop_count ;
    long car_move_count ;
    
    mocaDataRow   *row ;
    mocaDataRes   *res ;

    memset (lodnum, 0, sizeof (lodnum));
    memset (trlr_stoloc, 0, sizeof (trlr_stoloc));
    memset (stoloc, 0, sizeof (stoloc));
    memset (ship_id, 0, sizeof (ship_id));
    memset (tmp_ship_id, 0, sizeof (tmp_ship_id));
    memset (arecod, 0, sizeof (arecod));
    memset (rescod, 0, sizeof (rescod));
    memset (loc_arecod, 0, sizeof (loc_arecod));
    memset (loc_rescod, 0, sizeof (loc_rescod));
    memset (stop_id, 0, sizeof (stop_id));
    memset (where_clause, 0, sizeof (where_clause));
    
    if ((!ship_id_i || 0 == misTrimLen(ship_id_i, SHIP_ID_LEN)) ) {
	return srvSetupReturn(eINT_REQ_PARAM_MISSING, "");
    }
    else {
        misTrimcpy(ship_id, ship_id_i, SHIP_ID_LEN);
    }

    /* MR 2382 - Alex Arifin - April 11, 02
     * Need to check if the trailer has been dispatched or not
     */
   
    sprintf(buffer, 	"select trlr.trlr_stat "
			"from trlr, car_move, stop, shipment "
			"where trlr.trlr_id = car_move.trlr_id "
			"and car_move.car_move_id = stop.car_move_id "
			"and stop.stop_id = shipment.stop_id "
			"and shipment.ship_id = '%s'", ship_id ); 
   
    ret_status = sqlExecStr(buffer, &res) ;

    if (ret_status != eOK) {
        sqlFreeResults(res);
        return (srvSetupReturn(ret_status,"")) ;
    }

    row = sqlGetRow(res);
    
    if (strncmp(sqlGetString(res,row,"trlr_stat"), TRLSTS_DISPATCHED, TRLR_STAT_LEN) == 0) {
       sqlFreeResults(res);
       return (srvSetupReturn(eINT_INVALID_TRAILER_STATUS, ""));   
    }
    
    sqlFreeResults(res); 
    /* Get the shipment and make sure that it's actually loaded.  */ 

    sprintf(buffer, "select *  from shipment where ship_id = '%s'", ship_id) ;

    ret_status = sqlExecStr(buffer, &res) ;

    if (ret_status != eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    row = sqlGetRow(res);
    misTrimcpy (stop_id, sqlGetString( res, row, "stop_id"), STOP_ID_LEN) ;

    /* LHC MR431 need to check for shipment to be either loaded or load complete */
    if (strncmp(sqlGetString(res,row,"shpsts"), SHPSTS_LOADED, SHPSTS_LEN)!= 0
      && strncmp(sqlGetString(res,row,"shpsts"), SHPSTS_LOAD_COMPLETE, SHPSTS_LEN)!= 0) {
	sqlFreeResults(res);
	return (srvSetupReturn(eINT_INVALID_SHIPMENT,"")) ;
    }

    sqlFreeResults(res);
    if ((!stoloc_i || 0 == misTrimLen(stoloc_i, STOLOC_LEN)) ) {
        misTrc (T_FLOW, "No stoloc passed in....go find one.");
	/* Get the arecod and rescod from the pckmov to determine
	 * where the shipment came from and the rescod to see if
	 * a location is still assigned to the shipment.  */ 

	sprintf(buffer, "select arecod, seqnum, rescod, stoloc " 
		" from pckwrk, pckmov "
		" where ship_id = '%s' "
		" and pckwrk.cmbcod = pckmov.cmbcod "
		" order by seqnum desc",
		ship_id) ;

	ret_status = sqlExecStr(buffer, &res) ;

	if (ret_status != eOK) {
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
        }

        /*  Pulling the first record should be sufficient.
        */
	row = sqlGetRow(res);

	misTrimcpy (arecod, sqlGetString( res, row, "arecod"), ARECOD_LEN) ;
	misTrimcpy (rescod, sqlGetString( res, row, "rescod"), RESCOD_LEN) ;
	
	sqlFreeResults(res);

	sprintf(buffer, "select stoloc from locmst "
			" where arecod = '%s' and rescod = '%s' ",
			arecod, rescod) ;

	ret_status = sqlExecStr(buffer, &res) ;

        if ((ret_status != eOK) && (ret_status != eDB_NO_ROWS_AFFECTED)) {
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
	}
	else if (ret_status == eDB_NO_ROWS_AFFECTED ) {
	    /*  This means that there is no location allocated to the
	     *  rescod and we will have to grab one.  */
	    CurPtr = NULL;

            sprintf(buffer, "list available resource locations where arecod = '%s' ",
			arecod );

	    ret_status = srvInitiateInline (buffer, &CurPtr);
		
	    if (ret_status !=eOK) {
		    sqlFreeResults(res);
		    srvFreeMemory(SRVRET_STRUCT, CurPtr );
		    return (srvSetupReturn(ret_status,"")) ;
	    }

	    /*  Grab the first available location.  */
	    res = srvGetResults(CurPtr);
	    row = sqlGetRow(res);

	    misTrimcpy (stoloc, sqlGetString( res, row, "stoloc"), STOLOC_LEN) ;

	    srvFreeMemory(SRVRET_STRUCT, CurPtr);

	    /*  Allocate the location to the resource code.  */
	    returnData = NULL;

	    misTrc (T_FLOW, "--Allocate a location b/c the stoloc was not passed.");
	    sprintf(buffer, "allocate resource location where arecod = '%s' "
			" and stoloc = '%s'  and ship_id = '%s' " ,
		    arecod , 
		    stoloc ,
		    ship_id);

	    misTrc (T_FLOW, "1-Allocate a location b/c the stoloc was not passed.");
	    ret_status = srvInitiateCommand(buffer, NULL);
	    
	    misTrc (T_FLOW, "2-Allocate a location b/c the stoloc was not passed.");
	    if (ret_status !=eOK) {
		    return (srvSetupReturn(ret_status,"")) ;
	    }

	    misTrc (T_FLOW, "3-Allocate a location b/c the stoloc was not passed.");
	}
	else {
	    /*  This means that there is a location is still allocated to the
	     *  rescod and we will use that one.  */
	    row = sqlGetRow(res);
	    misTrimcpy (stoloc, sqlGetString( res, row, "stoloc"), STOLOC_LEN) ;
	}
    }
    else {
        /* If the stoloc is passed in, validate that the location is
         * in a staging area.  */

        misTrimcpy(stoloc, stoloc_i, STOLOC_LEN );
        misTrc (T_FLOW, "Check for valid location: %s", stoloc );

        sprintf(buffer, "select aremst.arecod, stgflg, rescod " 
		" from locmst, aremst "
		" where stoloc = '%s' "
		" and aremst.arecod = locmst.arecod ",
		stoloc) ;

        ret_status = sqlExecStr(buffer, &res) ;

        if (ret_status != eOK) {
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
        }

	row = sqlGetRow(res);
	misTrimcpy (loc_arecod, sqlGetString( res, row, "arecod"), ARECOD_LEN) ;

        if (sqlGetLong(res, row, "stgflg") == BOOLEAN_TRUE ) {
            /* If the rescod is not blank for the location passed in
	     * then it has to match the rescod for the shipment.  */
	    if(!sqlIsNull(res,row, "rescod") ) {
                misTrimcpy (loc_rescod, sqlGetString( res, row, "rescod"), RESCOD_LEN) ;
	        /* Get the arecod and rescod from the pckmov to determine
	         * where the shipment came from and the rescod to see if
	         * a location is still assigned to the shipment.  */ 

	        sprintf(buffer, "select arecod, seqnum, rescod, stoloc " 
			" from pckwrk, pckmov "
			" where ship_id = '%s'  "
			" and pckwrk.cmbcod = pckmov.cmbcod"
			" order by seqnum desc",
			ship_id) ;

	        ret_status = sqlExecStr(buffer, &res) ;

	        if (ret_status != eOK) {
		    sqlFreeResults(res);
		    return (srvSetupReturn(ret_status,"")) ;
	        }
	        /*  Pulling the first record should be sufficient.  */
	        row = sqlGetRow(res);

	        misTrimcpy (arecod, sqlGetString( res, row, "arecod"), ARECOD_LEN) ;
	        misTrimcpy (rescod, sqlGetString( res, row, "rescod"), RESCOD_LEN) ;

		/*  If the rescord from the pckmov and the locmst are not the same
		 *  then it is an error.  */
	        if ( strncmp (rescod, loc_rescod, RESCOD_LEN ) != 0 ) {
        	    misTrc (T_FLOW, 
		      "The rescods from the pckmov and locmst are not the same: %s", 
			stoloc );
	            sqlFreeResults(res);
	            return (srvSetupReturn(eINT_INVALID_LOCATION,"")) ;
	        }
	        if ( strncmp (arecod, loc_arecod, ARECOD_LEN ) != 0 ) {
	            strncpy (arecod, loc_arecod, ARECOD_LEN) ;
	        }

	    }
	    else {  /* rescod is null, allocate the location. */ 
	        strncpy (arecod, loc_arecod, ARECOD_LEN) ;

		/*  Allocate the location to the resource code.  */
		returnData = NULL;

		sprintf(buffer, "allocate resource location where arecod = '%s' "
			" and stoloc = '%s'  and ship_id = '%s' " ,
		    arecod , 
		    stoloc ,
		    ship_id);

		ret_status = srvInitiateInline (buffer, NULL);
	    
		if (ret_status !=eOK) {
		    sqlFreeResults(res);
		    return (srvSetupReturn(ret_status,"")) ;
		}

	    }
        }
        else {
            misTrc (T_FLOW, "=======================================");
            misTrc (T_FLOW, "The location is invalid b/c the arecod ");
            misTrc (T_FLOW, "is not a staging area. arecod: '%s'", loc_arecod);
 	    return (srvSetupReturn(eINT_INVALID_LOCATION,"")) ;
        }

    }
  
    misTrc (T_FLOW, "=======================================");
    misTrc (T_FLOW, "Start to gather and move the inventory. ");

    /*=========================================================================*/
    /*===================== Start the moving of inventory =====================*/
    /*=========================================================================*/
    /*  Find all the loads that are on that shipment.  This assumes
     *  that there is only one shipment on the lodnum...BIG ASSUMPTION.  */
    sprintf(buffer, "select distinct invlod.lodnum, invlod.stoloc " 
		" from invlod, invsub, invdtl "
		" where invlod.lodnum = invsub.lodnum "
		" and invsub.subnum = invdtl.subnum "
		" and invdtl.ship_line_id in "
		" (select ship_line_id from shipment_line where ship_id = '%s')",
		ship_id) ;

    ret_status = sqlExecStr(buffer, &res) ;

    if ((ret_status != eOK) ) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /*******************************************************************/
    /* Move each load into the new staging lane.  */
    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row)) {
	misTrimcpy (lodnum, sqlGetString( res, row, "lodnum"), LODNUM_LEN) ;
	misTrimcpy (trlr_stoloc, sqlGetString( res, row, "stoloc"), STOLOC_LEN) ;

	sprintf(buffer, "move inventory where lodnum = '%s' "
		" and srcloc = '%s' "
		" and dstloc = '%s' "
		" and newdst = 0 "
		" and spcind = '%s' " ,
		lodnum , 
		trlr_stoloc , 
		stoloc , 
		SPCIND_PRA_REVERSE );

	ret_status = srvInitiateInline (buffer, NULL);
	
	if (ret_status !=eOK) {
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
        }
    }
    /*******************************************************************/
    /*  Count the number of shipments with the same stop_id.
    */
    sprintf(buffer, "select count(*) stop_cnt " 
		" from shipment "
		" where stop_id = '%s' ",
		stop_id) ;

    ret_status = sqlExecStr(buffer, &res) ;

    if (ret_status != eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    row = sqlGetRow(res);
    stop_count = sqlGetLong(res, row, "stop_cnt");

    CurPtr = NULL;

    /*******************************************************************/
    /*  Count the number of stops with the same car_move_id.
    */
    sprintf(buffer, "[select car_move_id from stop "
		" where stop_id = '%s'] | "
		" [select count(*) car_move_cnt " 
		" from stop "
		" where car_move_id = @car_move_id]",
		stop_id) ;

    ret_status = srvInitiateInline (buffer, &CurPtr);
	
    if (ret_status !=eOK) {
	sqlFreeResults(res);
        srvFreeMemory(SRVRET_STRUCT, CurPtr );
	return (srvSetupReturn(ret_status,"")) ;
    }

    res = srvGetResults(CurPtr);
    row = sqlGetRow(res);

    car_move_count = sqlGetLong(res, row, "car_move_cnt");

    CurPtr = NULL;

    /*******************************************************************/
    /*  Update the shipment shpsts from D->either P or S 
	    and loddte is blank and stop_id is blank.  */
    /*PWONG 10/01/2001, MR690, when unloading a trailer, clear shipment.doc_num (NULL) */
    sprintf(buffer, "update shipment " 
		"set shpsts = '%s', loddte = NULL, stop_id = NULL, doc_num = NULL "
		"where ship_id = '%s' ",
		SHPSTS_STAGED, ship_id) ;

    ret_status = sqlExecStr(buffer, NULL) ;

    if (ret_status != eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /********************************************************************
     *  Update the shipment_line linsts from C->I 
     *                  and shpqty = 0
     *			and stgqty = shpqty
    */
    sprintf(buffer, "update shipment_line " 
		"set linsts = '%s', stgqty = shpqty, shpqty = 0 "
		"where ship_id = '%s' ",
		LINSTS_INPROCESS, ship_id) ;

    ret_status = sqlExecStr(buffer, NULL) ;

    if (ret_status != eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /*******************************************************************/
    /*  Update the ord_line shpqty = 0
    */
    sprintf(buffer, "[select ordnum, ordlin, ordsln, client_id "
		"from shipment_line "
		"where ship_id = '%s'] | "
		"[update ord_line " 
		" set shpqty = 0 "
		" where ordnum = @ordnum "
		" and ordlin = @ordlin "
		" and ordsln = @ordsln "
		" and client_id  = @client_id ",
		ship_id) ;

    ret_status = srvInitiateInline (buffer, NULL);
	
    if (ret_status !=eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /*******************************************************************/
    /*  Update the pckwrk pcksts from C->R 
    */
    sprintf(buffer, "update pckwrk " 
		" set pcksts = '%s' "
		" where ship_id = '%s'  ",
		PCKSTS_RELEASED, ship_id) ;

    ret_status = sqlExecStr(buffer, NULL) ;

    if (ret_status != eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /*******************************************************************/
    /*  Update the pckmov stoloc blank->stoloc that has bee allocated
    */
    sprintf(buffer, "update pckmov "
		" set stoloc = '%s' "
		" where arecod = '%s' "
		" and cmbcod in (select cmbcod "
		" from pckwrk "
		" where ship_id = '%s') ",
		stoloc, arecod, ship_id) ;

    ret_status = sqlExecStr(buffer, NULL) ;

    if (ret_status != eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    /*******************************************************************/
    /*  Update the pckbat batsts CMPL->REL
     *			  cmpdte DATE->blank
    */
    sprintf(buffer, "update pckbat "
		" set batsts = '%s', cmpdte = NULL "
		" where schbat = (select schbat from pckwrk "
		" where ship_id = '%s' and rownum < 2) ",
		BATSTS_REL, ship_id) ;

    ret_status = sqlExecStr(buffer, NULL) ;

    if (ret_status != eOK) {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }


    /*******************************************************************/
    /*******************************************************************/
    /*  
     *  If there is only one shipment on the stop, check to see if there
     *  is only one stop on the car_move.
    */
    /*******************************************************************/
    /*******************************************************************/

    misTrc (T_FLOW, "Counts for Shipment: %s", ship_id );
    misTrc (T_FLOW, "              stops: %ld", stop_count );
    misTrc (T_FLOW, "     car_move_count: %ld", car_move_count );

    if (stop_count == 1 ) {
        /*******************************************************************/
        /*  Determine if we need to delete the car_move and trlr records.
        */

        if (car_move_count == 1) {
	    /*  There is only one stop associated with the car_move...delete.
 	    */
    	    sprintf(buffer, "delete from trlr "
		    " where trlr_id in "
		    " (select trlr_id from stop, car_move "
		    " where stop_id = '%s' "
		    " and stop.car_move_id = car_move.car_move_id) ",
		    stop_id) ;

    	    ret_status = sqlExecStr(buffer, NULL) ;
    
    	    if (ret_status != eOK) {
		    sqlFreeResults(res);
		    return (srvSetupReturn(ret_status,"")) ;
    	    }
    
	    /*  There is only one stop associated with the car_move...delete.
 	    */
    	    sprintf(buffer, "delete from car_move "
		    " where car_move_id in "
		    " (select car_move_id "
		    " from stop "
		    " where stop_id = '%s')",
		    stop_id) ;

    	    ret_status = sqlExecStr(buffer, NULL) ;

    	    if (ret_status != eOK) {
		sqlFreeResults(res);
		return (srvSetupReturn(ret_status,"")) ;
    	    }

	    /*  Remove the trailer location if the trailer is deleted.  */
	    CurPtr = NULL;

	    sprintf(buffer, "remove location where stoloc = '%s'",
		    trlr_stoloc );

	    ret_status = srvInitiateInline (buffer, NULL);
	
	    if (ret_status !=eOK) {
	        sqlFreeResults(res);
	        return (srvSetupReturn(ret_status,"")) ;
	    }

        }

        /*******************************************************************/
	/*  There is only one shipment associated with the stop...delete.  */
    	sprintf(buffer, "delete from stop where stop_id = '%s'",
		stop_id) ;

    	ret_status = sqlExecStr(buffer, NULL) ;

    	if (ret_status != eOK) {
		sqlFreeResults(res);
		return (srvSetupReturn(ret_status,"")) ;
    	}

    }
    else {
        /*******************************************************************/
        /*  Update the stop: doc_num, track_num -> blank
         *  If there is more than one shipment on the stop remove the doc_num
	 *  and the track_num and unload the shipments to a pre-printed
         *  paperwork state.
        */
        sprintf(buffer, "update stop "
		" set doc_num = NULL, track_num = NULL where stop_id = '%s'",
		    stop_id) ;

        ret_status = sqlExecStr(buffer, NULL) ;

        if (ret_status != eOK) {
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,"")) ;
        }

        /*  Find all the shipments on the stop and move them to a
         *  pre-paperwork printed state.
        */
        sprintf(buffer, "select ship_id " 
		" from shipment "
		" where stop_id = '%s'",
		stop_id) ;

       ret_status = sqlExecStr(buffer, &res) ;

       if ((ret_status != eOK) ) {
	   sqlFreeResults(res);
	   return (srvSetupReturn(ret_status,"")) ;
       }

       /*******************************************************************/
       /* Move each load into the new staging lane.
       */
       for (row = sqlGetRow(res); row; row = sqlGetNextRow(row)) {
	   misTrimcpy (tmp_ship_id, sqlGetString( res, row, "ship_id"), SHIP_ID_LEN) ;

           misTrc (T_FLOW, "Unloading shipment to PRE-PAPERWORK STATUS: %s",tmp_ship_id);

	   sprintf(buffer, "unload shipment to pre paperwork where ship_id = '%s'",
		tmp_ship_id );

	   ret_status = srvInitiateInline (buffer, NULL);
	
	   if (ret_status !=eOK) {
	       sqlFreeResults(res);
	       return (srvSetupReturn(ret_status,"")) ;
           }

       }
    }

    CurPtr = NULL;

    /*
    return (srvSetupReturn(eOK,""));
    */
    returnData = srvResultsInit (eOK, "stoloc", COMTYP_CHAR, STOLOC_LEN, NULL);	

    ret_status = srvResultsAdd (returnData,
		  stoloc, 
		  NULL );

    return(returnData);
}

