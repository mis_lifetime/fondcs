static char rcsid[] = "$Id: varPurgeShipments.c,v 1.5 2002/10/24 22:35:25 prod Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>

#include "intShipLib.h"
 
#define MODE_CANCELED 'C'
#define MODE_SHIPPED  'S'
#define MODE_ALL      'A'

static int sProcessShipment(char *ship_id, char *mode,
	                    FILE *fp)
{
    long ret_status;
    char save_lodnum[LODNUM_LEN +1];
    char save_subnum[SUBNUM_LEN +1];
    mocaDataRow *linrow;
    mocaDataRes *linres, *cntres;
    mocaDataRow *invrow=NULL;
    mocaDataRes *invres=NULL;
    RETURN_STRUCT *CurPtr=NULL;

    char sqlBuffer[3000];

    long ol_cnt=0;
    long o_not_cnt=0;
    long o_ins_cnt=0;
    long o_cnt=0;
    /*
     * Delete all of the inventory for the shipment.
     */

    sprintf(sqlBuffer,
	" select distinct "
	"        shipment_line.ship_id,   shipment_line.ordnum, "
	"        shipment_line.ordsln,    shipment_line.ordlin, "
	"        shipment_line.client_id, invlod.lodnum,"
	"        ord.rtcust,              ord.stcust, "
	"        shipment.loddte,         ord.btcust, "
	"        invsub.subnum,           invdtl.dtlnum, "
        "        invdtl.prtnum,           invdtl.prt_client_id, "
	"        invdtl.lotnum,           invdtl.orgcod, "
	"        invdtl.revlvl,           invdtl.invsts, "
	"        invdtl.untpak,           invdtl.untcas, "
	"        invdtl.untqty,           invdtl.rcvkey, "
	"        invdtl.ship_line_id,     invdtl.cmpkey  "
	"   from ord, shipment_line, "
	"        invdtl, invlod, invsub, "
	"        shipment "
	"  where shipment.ship_id           = '%s' "
	"    and ord.ordnum                 = shipment_line.ordnum "
	"    and ord.client_id              = shipment_line.client_id "
	"    and shipment.ship_id           = shipment_line.ship_id "
	"    and shipment_line.ship_line_id = invdtl.ship_line_id "
	"    and invlod.lodnum              = invsub.lodnum "
	"    and invsub.subnum              = invdtl.subnum "
	" order by invlod.lodnum, invsub.subnum, invdtl.dtlnum ",
       ship_id);
	
    ret_status = sqlExecStr(sqlBuffer, &invres);
    if ((eOK != ret_status) && (ret_status != eDB_NO_ROWS_AFFECTED))
    {
        sqlFreeResults(invres); 
        return (ret_status);	
    }
	
    memset (save_lodnum, 0, sizeof(save_lodnum));
    memset (save_subnum, 0, sizeof(save_subnum));
	
    /* Loop through inventory information and delete it */
	
    if (ret_status == eOK)
    {
        for (invrow = sqlGetRow(invres); invrow; invrow = sqlGetNextRow(invrow))
	{
	    if (fp != NULL)
	    {
	        fprintf(fp, "  ship_id='%s', client_id = '%s', ordnum='%s', \n"
			    "    lodnum='%s', subnum='%s', dtlnum='%s', \n"
			    "    prtnum='%s', prt_client_id='%s', \n"
			    "    lotnum='%s', orgcod='%s', revlvl='%s', \n"
			    "    invsts='%s', nuntpak=%ld, untcas=%ld, untqty=%ld, \n"
			    "    rcvkey='%s', ship_line_id='%s' \n",
			    sqlGetString(invres, invrow, "ship_id"),
			    sqlGetString(invres, invrow, "client_id"),
			    sqlGetString(invres, invrow, "ordnum"),
			    sqlGetString(invres, invrow, "lodnum"),
			    sqlGetString(invres, invrow, "subnum"),
			    sqlGetString(invres, invrow, "dtlnum"),
			    sqlGetString(invres, invrow, "prtnum"),
			    sqlGetString(invres, invrow, "prt_client_id"),
			    sqlGetString(invres, invrow, "lotnum"),
			    sqlGetString(invres, invrow, "orgcod"),
			    sqlGetString(invres, invrow, "revlvl"),
			    sqlGetString(invres, invrow, "invsts"),
			    sqlGetLong(invres, invrow, "untpak"),
			    sqlGetLong(invres, invrow, "untcas"),
			    sqlGetLong(invres, invrow, "untqty"),
			    sqlGetString(invres, invrow, "rcvkey"),
			    sqlGetString(invres, invrow, "ship_line_id"));
		fflush(fp);
	    }
			
	    /* Delete the inventory detail */ 
	    sprintf(sqlBuffer, 
		    "delete from invdtl "
		    " where dtlnum = '%s' ",
		    sqlGetString(invres, invrow, "dtlnum"));

	    ret_status = sqlExecStr(sqlBuffer, NULL);
	    if (ret_status != eOK)
	    {
		sqlFreeResults(invres);
	        return (ret_status); 
   	    }
	    /* Delete INVSUB if it wasn't already deleted */
	    if (strncmp(save_subnum, sqlGetString(invres, invrow, "subnum"),
			    SUBNUM_LEN))
   	    {
	        sprintf(sqlBuffer,
		        "delete from invsub "
		        "where subnum = '%s' ",
		        sqlGetString(invres, invrow, "subnum"));
		
		ret_status = sqlExecStr(sqlBuffer, NULL);
	        if (ret_status != eOK)
	        {
	   	    sqlFreeResults(invres); 
		    return (ret_status); 
		}
		strncpy(save_subnum, sqlGetString(invres, invrow, "subnum"), 
			    SUBNUM_LEN);
	    }

            /* Delete the lod number only if there are no other subs
               tied to the load. MR 4270. This is because we would
               delete a load even if there were other shipments tied
               to it and on the  II pass for that shipment, the main
               query in this function would return 1403 */
	    sprintf(sqlBuffer,
	            "delete from invlod "
		    "where lodnum = '%s' "
                    "and not exists "                       
                    " (select 1 from invsub where invsub.lodnum = '%s') ",
		     sqlGetString(invres, invrow, "lodnum"),
		     sqlGetString(invres, invrow, "lodnum"));
		
 	    ret_status = sqlExecStr(sqlBuffer, NULL);
	    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	    {
	       sqlFreeResults(invres); 
	       return (ret_status); 
	    }
            /* End MR 1470 */

	    if (!sqlIsNull(invres, invrow, "cmpkey"))
	    {
	        sprintf(sqlBuffer,
	   	        "archive components where cmpkey = '%s'",
		        sqlGetString(invres, invrow, "cmpkey"));
		 
		ret_status = srvInitiateCommand(sqlBuffer, NULL);
		if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
		{
		    sqlFreeResults(invres); 
		    return(ret_status); 
		}
		    
	        sprintf(sqlBuffer,
		        "write component shipment archive"
		        " where cmpkey = '%s' "
			    "   and client_id = '%s' "
			    "   and ordnum = '%s' "
			    "   and ordlin = '%s' "
			    "   and ordsln = '%s' "
			    "   and btcust = '%s' "
			    "   and stcust = '%s' "
			    "   and rtcust = '%s' "
			    "   and untqty = %d "
			    "   and shpdte = '%s' ",
			    sqlGetString(invres, invrow, "cmpkey"),
			    sqlGetString(invres, invrow, "client_id"),
			    sqlGetString(invres, invrow, "ordnum"),
			    sqlGetString(invres, invrow, "ordlin"),
			    sqlGetString(invres, invrow, "ordsln"),
			    sqlGetString(invres, invrow, "btcust"),
			    sqlGetString(invres, invrow, "stcust"),
			    sqlGetString(invres, invrow, "rtcust"),
			    sqlGetLong(invres, invrow, "untqty"),
			    sqlGetString(invres, invrow, "loddte")); 
		    
		ret_status = srvInitiateCommand(sqlBuffer, NULL);
		if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
		{
		    sqlFreeResults(invres); 
		    return (ret_status); 
		}
  	    }
	} 
	sqlFreeResults(invres);
    } 

    /*
     * OK, now let's get a list of all the orders that we can
     * delete for this shipment, and we'll use that list
     * to delete the notes, instructions, sources and order lines
     * We want all of the order lines that are either picked 
     * complete, or don't allow back order.
     * Also, make sure we are not grabbing order lines that exist on
     * another shipment line.
     */

    /* Roman Barykin: I'm changing "or" clause to "and" in the code below
     * "and (ord_line.bckflg = '%ld' or ord_line.ordqty >= ord_line.shpqty)"
     * just like it is in Database Archive Mntn for ORD_LINE archiving,
     * because if we didn't archive it, we don't want to delete it
     */

    /* MR 2103 Paul W. 03/14/2002 
     * If we select the order lines which has bckflg = 0 AND ordqty >= shpqty
     * like Roman B. does, the order line which has been COMPLETELY shipped does NOT get purged, 
     * e.g. an order line with ordqty = 5, shpqty = 5, bckflg = 1, and pckqty = 0.  This kind of order line
     * get archived, but they shouldn't be left behind in the PROD, because later when the
     * customer wants to delete this order, they can and which caused the order delete transaction
     * send out SHPQTY 0 ((-1) * ord_line.pckqty  
     *
     * Here is the example of the scenerios to show whether we should put the order out of system or not
     *          
     * ordqty   shpqty   bckflg   purge? 
     * ---------------------------------
     *      5        5        1        Y
     *      5        5        0        Y
     *      5        3        1        N
     *      5        3        0        Y
     *      5        0        1        N
     *      5        0        0        Y
     *
     * Note that the latest change basically removes Roman's changes and rolls back to standard product version
     *
     */ 

/*   MR 2113 Paul W. 03/20/2002 
 *   We don't want to delete the order and order line if mode is "CANCELLED" 
 *   they should be able to plan those orders again  
 */
     
if (mode != MODE_CANCELED)
{
    sprintf(sqlBuffer,  
            "select shipment_line.ship_line_id, "
            "       ord_line.ordnum, "
            "       ord_line.client_id, "
            "       ord_line.ordlin, "
            "       ord_line.ordsln "
            "  from ord_line, "
            "       shipment_line "
            " where ord_line.ordnum       = shipment_line.ordnum "
            "   and ord_line.client_id    = shipment_line.client_id "
            "   and ord_line.ordlin       = shipment_line.ordlin "
            "   and ord_line.ordsln       = shipment_line.ordsln "
            "   and shipment_line.ship_id = '%s' "
            "   and (ord_line.bckflg      = %ld " 
            "    or ord_line.shpqty >= ord_line.ordqty) "
            "   and not exists (select 'x' "
            "                     from shipment_line "
            "                    where ordnum = ord_line.ordnum "
            "                      and client_id = ord_line.client_id"
            "                      and ordsln    = ord_line.ordsln "
            "                      and ordlin    = ord_line.ordlin "
            "                      and ship_id  <> '%s') ",
            ship_id,
            BOOLEAN_FALSE,
            ship_id);

/* MR 2103 Paul W. 03/14/2002  Comment out Roman B. changes 
    sprintf(sqlBuffer, 
	    "select shipment_line.ship_line_id, "
	    "       ord_line.ordnum, "
            "       ord_line.client_id, "
	    "       ord_line.ordlin, "
	    "       ord_line.ordsln "
	    "  from ord_line, "
	    "       shipment_line "
	    " where ord_line.ordnum       = shipment_line.ordnum "
	    "   and ord_line.client_id    = shipment_line.client_id "
	    "   and ord_line.ordlin       = shipment_line.ordlin "
	    "   and ord_line.ordsln       = shipment_line.ordsln "
	    "   and shipment_line.ship_id = '%s' "
	    "   and ord_line.bckflg       = %ld  "
	    "   and ord_line.ordqty      >= ord_line.shpqty "
	    "   and           not exists (select 'x' "
	    "                               from shipment_line "
            "                              where ordnum = ord_line.ordnum "
	    "                                and client_id = ord_line.client_id"
	    "                                and ordsln    = ord_line.ordsln "
	    "                                and ordlin    = ord_line.ordlin "
	    "                                and ship_id  <> '%s') ",
	    ship_id, 
	    BOOLEAN_FALSE, 
	    ship_id);
*/ 
 
    ret_status = sqlExecStr(sqlBuffer, &linres);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	sqlFreeResults(linres);
        return (ret_status); 
    }

    for (linrow = sqlGetRow(linres); linrow; linrow = sqlGetNextRow(linrow))
    {

        /* Delete all of the order line instructions for this shipment 
         * that either are picked complete or that don't allow backorder.
         */
        sprintf(sqlBuffer,
            " delete from ord_line_ins "
            "  where client_id = '%s' "
	    "    and ordnum    = '%s' "
	    "    and ordlin    = '%s' "
	    "    and ordsln    = '%s' ",
	    sqlGetString(linres, linrow, "client_id"),
	    sqlGetString(linres, linrow, "ordnum"),
	    sqlGetString(linres, linrow, "ordlin"),
	    sqlGetString(linres, linrow, "ordsln"));
   
        ret_status = sqlExecStr(sqlBuffer, NULL);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
	    sqlFreeResults(linres);
	    return (ret_status);
        }
   
        /* Delete all of the order line notes for this shipment 
         * that either are picked complete or that don't allow backorder.
         */
        sprintf(sqlBuffer,
               "delete from ord_line_note "
               " where client_id = '%s' "
	       "   and ordnum    = '%s' "
	       "   and ordlin    = '%s' "
	       "   and ordsln    = '%s' ",
	       sqlGetString(linres, linrow, "client_id"),
	       sqlGetString(linres, linrow, "ordnum"),
	       sqlGetString(linres, linrow, "ordlin"),
	       sqlGetString(linres, linrow, "ordsln"));
   
        ret_status = sqlExecStr(sqlBuffer, NULL);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
	    sqlFreeResults(linres);
            return (ret_status); 
        }
   
        /* Delete all of the order lines for this shipment that
         * are either completely picked or do not allow back orders.
	 * We already did the check in the query above.
         */
        sprintf(sqlBuffer,
            " delete from ord_line "
            " where client_id = '%s' "
	    "   and ordnum    = '%s' "
	    "   and ordlin    = '%s' "
	    "   and ordsln    = '%s' ",
	    sqlGetString(linres, linrow, "client_id"),
	    sqlGetString(linres, linrow, "ordnum"),
	    sqlGetString(linres, linrow, "ordlin"),
	    sqlGetString(linres, linrow, "ordsln"));
                        
        ret_status = sqlExecStr(sqlBuffer, NULL);
        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
        {
	    sqlFreeResults(linres);
            return (ret_status); 
        }
   
    }
    sqlFreeResults(linres);
        
    /* Delete all of the order instructions for this shipment that 
     * have no remaining lines, keeping in mind that this order may
     * have lines on other shipments. 
     */

    sprintf(sqlBuffer,
            "[select distinct client_id, ordnum from shipment_line "
	    "  where ship_id = '%s'] | "
	    "[delete from ord_ins "
            " where client_id = @client_id " 
            "   and ordnum    = @ordnum " 
	    "   and not exists  (select 'x' "
	    "                      from ord_line ol "
	    "                     where ol.client_id = @client_id "
	    "                       and ol.ordnum    = @ordnum)] ",
            ship_id);	

    ret_status = srvInitiateCommand(sqlBuffer, &CurPtr);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
	srvFreeMemory(SRVRET_STRUCT, CurPtr);
        return (ret_status); 
    }
    cntres=srvGetResults(CurPtr);
    if ((ret_status == eOK) && (cntres))
        o_ins_cnt=sqlGetNumRows(cntres);
    else
	o_ins_cnt=0;
    
    srvFreeMemory (SRVRET_STRUCT, CurPtr);
    
    /* Delete all of the order notes for this shipment that have no 
     * have no remaining lines, keeping in mind that this order may
     * have lines on other shipments. 
     */
    sprintf(sqlBuffer,
            "[select distinct client_id, ordnum from shipment_line "
	    "  where ship_id = '%s'] | "
	    "[delete from ord_note "
            " where client_id = @client_id " 
            "   and ordnum    = @ordnum " 
	    "   and not exists  "
            "       (select 'x' "
	    "          from ord_line ol "
	    "         where ol.client_id = @client_id "
	    "           and ol.ordnum    = @ordnum)] ",
            ship_id);	

    ret_status = srvInitiateCommand(sqlBuffer, &CurPtr);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr); 
        return (ret_status); 
    }

    cntres=srvGetResults(CurPtr);
    if ((ret_status == eOK) && (cntres))
        o_not_cnt=sqlGetNumRows(cntres);
    else
	o_not_cnt=0;
  
    srvFreeMemory (SRVRET_STRUCT, CurPtr);
    
    /* Delete all of the orders for this shipment that have no 
     * have no remaining lines, keeping in mind that this order may
     * have lines on other shipments. 
     */
    sprintf(sqlBuffer,
            "[select distinct client_id, ordnum from shipment_line "
	    "  where ship_id = '%s'] | "
	    "[delete from ord "
            " where client_id = @client_id " 
            "   and ordnum    = @ordnum " 
	    "   and not exists  "
            "       (select 'x' "
	    "          from ord_line ol "
	    "         where ol.client_id = @client_id "
	    "           and ol.ordnum    = @ordnum)] catch (@?) ",
            ship_id);

    ret_status = srvInitiateCommand(sqlBuffer, &CurPtr);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        srvFreeMemory(SRVRET_STRUCT, CurPtr); 
        return (ret_status); 
    }
    
    cntres=srvGetResults(CurPtr);
    if ((ret_status == eOK) && (cntres))
        o_cnt=sqlGetNumRows(cntres);
    else
	o_cnt=0;
   
    srvFreeMemory (SRVRET_STRUCT, CurPtr);

    if (fp != NULL)
    {
        fprintf(fp,
	    "  ship_id='%s', ord_cnt=%ld, ord_ins_cnt=%ld, ord_note_cnt=%ld, \n"
	    "    ordlin_cnt=%ld \n",
	    ship_id, 
	    o_cnt, o_ins_cnt, o_not_cnt, 
	    ol_cnt);
        fflush(fp);
    }
}
/* end if mode = cancelled */

    /* MR 2103 Paul W. 03/14/2002 
     * we realized that we need to archive the pckmove to the archive system
     * Therefore, we need to purge the pick moves entries for the shipment also
     * This way, in the system, we won't have pckmovs that don't have pckwrks
     */

    sprintf(sqlBuffer, 
            " delete from pckmov "
            " where cmbcod in (select cmbcod "
            "                  from pckwrk "
            "                  where ship_id = '%s') ",
            ship_id); 
    
    ret_status = sqlExecStr(sqlBuffer, NULL);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
       return(ret_status);
    }

/* ends Paul W. */ 

    /* 
     * Delete all of the pick work for the shipment.
     */

    sprintf(sqlBuffer,
            "delete from pckwrk "
            " where ship_id = '%s' ",
            ship_id); 

    ret_status = sqlExecStr(sqlBuffer, NULL);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        return(ret_status); 
    }

    /* 
     * Delete all of the cancelled pick work for the shipment.
     */

    sprintf(sqlBuffer,
            "delete from canpck "
            " where ship_id = '%s' ",
            ship_id); 

    ret_status = sqlExecStr(sqlBuffer, NULL);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        return(ret_status); 
    }

    /* Delete all of the Shipment Line Instructions for 
     * the shipment line.
     * Note, they may already have been deleted if we have 
     * gone through this loop more than once.
     */

    sprintf(sqlBuffer,
            "delete from shipment_line_ins "
            " where (ship_line_id) in "
            "       (select shipment_line.ship_line_id "
            "          from shipment_line"
            "         where shipment_line.ship_id = '%s') ",
            ship_id); 

    ret_status = sqlExecStr(sqlBuffer, NULL);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        return(ret_status); 
    }

    /* Delete the shipment line we are on. */

    sprintf(sqlBuffer,
           "delete from shipment_line "
	   " where ship_id = '%s' ",
           ship_id);

    ret_status = sqlExecStr(sqlBuffer, NULL);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        return (ret_status); 
    }

    /* Delete all of the Manifests for  the current shipment*/
    sprintf(sqlBuffer,
            "delete from manfst "
            " where ship_id = '%s' ",
	    ship_id); 

    ret_status = sqlExecStr(sqlBuffer, NULL);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        return (ret_status); 
    }


    /* Delete all of the Shipment Instructions for 
     * the current shipment.
     */

    sprintf(sqlBuffer,
            "delete from shipment_ins "
            " where ship_id = '%s' ",
            ship_id); 

    ret_status = sqlExecStr(sqlBuffer, NULL);

    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        return (ret_status); 
    }

    /* Delete the shipment */

    sprintf(sqlBuffer,
            "delete from shipment "
            " where ship_id = '%s' ",
            ship_id); 

    ret_status = sqlExecStr(sqlBuffer, NULL);
    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
    {
        return (ret_status); 
    }

    return eOK;
}

LIBEXPORT 
RETURN_STRUCT *varPurgeShipments(long *dayold_i,
			         long *maxpur_i,
			         char *logfil_i,
				 char *car_move_id_i,
				 char *ship_id_i,
                                 char *mode_i)
{
    long ret_status;
    mocaDataRow *row=NULL;
    mocaDataRes *res=NULL, *cntres;
    mocaDataRow *shprow=NULL;
    mocaDataRes *shpres=NULL;

    char sqlBuffer[3000];
    char varbuf[1024];
    char filename[300];
    char trlr_id[TRLR_ID_LEN + 1];
    long dayold;
    long MaxPurge;
    long NumCarMoves;
    long NumPurged;
    FILE *fp;
    RETURN_STRUCT *ret_data=NULL;
    RETURN_STRUCT *CurPtr=NULL;
    long sl_cnt=0;
    long mf_cnt=0;
    long s_ins_cnt=0;
    long s_cnt=0;
    long trl_cnt=0;
    char mode;

    fp = NULL;
    dayold = 365;
    MaxPurge = 0;
    NumCarMoves = 0;
    NumPurged = 0;

    misTrc(T_FLOW, "Entering varPurgeShipments");

    /* If Carrier Move Id was passed in, validate it */
    
    if (car_move_id_i)
    {
	if (eOK != intValTablePK ("car_move", "car_move_id", car_move_id_i,
		&ret_data) )
	{
          goto CLEANUP_RETURN;
	}
        srvFreeMemory(SRVRET_STRUCT, ret_data);
    }

    if (ship_id_i)
    {
	if (eOK != intValTablePK ("shipment", "ship_id", ship_id_i,
		&ret_data) )
	{
          goto CLEANUP_RETURN;
	}
        srvFreeMemory(SRVRET_STRUCT, ret_data);
    }
    
    if (dayold_i)
	dayold = *dayold_i;
    if (maxpur_i)
	MaxPurge = *maxpur_i;

    /*
     * The mode will be one of three values:
     *   C = Only Canceled Shipments
     *   S = Only Shipped Shipments (with Carrier Move)
     *   A = (Default) Both canceled and shipped.
     */
    if (mode_i && misTrimLen(mode_i, 1))
    {
        mode = *mode_i;
    }
    else
    {
        mode = MODE_ALL;
    }
    
    /* If log file was passed in, open purge log file. */
    
    memset(filename, 0, sizeof(filename));
    if ((logfil_i) && (strlen(logfil_i)))
    {
	if (strchr(logfil_i, PATH_SEPARATOR))
	    sprintf(filename, "%s", logfil_i);
	else
	    sprintf(filename, "%s%c%s",
		    misExpandVars(varbuf, DCS_LOG, sizeof(varbuf), NULL),
		    PATH_SEPARATOR, logfil_i);
	misTrim(filename);
	fp = fopen(filename, "a");
	if (fp == NULL)
	{
	    misLogError("Unable to open the purge log file %s", filename);
	    misTrc(T_FLOW, "Unable to open the purge log file %s", filename);
	    return (srvResults(eFILE_OPENING_ERROR, NULL));
	}
    }

    /* If Carrier Move ID was not passed in,  
     * ... read the carrier moves that have actual departure date(s)
     * ... and all of it's actual departure dates are older than days old. */

    if (mode == MODE_SHIPPED || mode == MODE_ALL)
    {
        if (!car_move_id_i)
        {
	    sprintf(sqlBuffer,
		    "select car_move.car_move_id, car_move.trlr_id "
		    "  from trlr, car_move "
		    " where car_move.trlr_id = trlr.trlr_id "
	            "   and trlr.dispatch_dte < (sysdate - %d) ",
		    dayold);
        }

        /* Else if the Carrier Move ID was passed in, 
         * ... read the carrier move that was passed in.  Make sure that the 
         * associated trailer's departure  dates are older than days old. 
         */

        else
        {
	    sprintf(sqlBuffer,
		    "select car_move.car_move_id, car_move.trlr_id "
		    "  from trlr, car_move "
		    " where car_move.trlr_id     = trlr.trlr_id "
	            "   and trlr.dispatch_dte    is not null "
                    "   and car_move.car_move_id = '%s' ",
		    car_move_id_i);
        }
  
        ret_status = sqlExecStr(sqlBuffer, &res);
        if (eOK != ret_status && eDB_NO_ROWS_AFFECTED != ret_status)
        {
	    ret_data = srvResults (ret_status, NULL);
	    goto CLEANUP_RETURN;
        }
  
        /* Loop through all of the carrier moves */ 
        
        NumCarMoves = sqlGetNumRows(res);
        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
        {
            /* Verify that all shipments on the carrier move are shipped
	     * or cancelled
	     */
    
            sprintf(sqlBuffer,
		    "  select 'x' "
   	  	    "  from car_move, stop, "
		    "        shipment "
                    " where car_move.car_move_id = '%s' "
		    "   and car_move.car_move_id = stop.car_move_id "
		    "   and stop.stop_id         = shipment.stop_id "
                    "   and shipment.shpsts <> '%s' " /* MR 2113 Paul W. 03/19/2002 why are we missing 1 condition here? */
		    "   and shipment.shpsts <> '%s' ",
		    sqlGetString(res,row,"car_move_id"),
	            SHPSTS_LOAD_COMPLETE,
		    SHPSTS_CANCELLED);
            
	    ret_status = sqlExecStr(sqlBuffer, NULL);
            if (eOK == ret_status)
	    {
	        if (fp != NULL)
		    fprintf (fp, 
                             "*** SKIPPED Carrier Move, %s, not all "
			     "Shipments complete ",
			     sqlGetString(res,row,"car_move_id")); 
	        /*
	         * Adjust the count of carrier moves that are being purged.
	         */

	        --NumCarMoves;
  	        continue;

	    }    

	    if (fp != NULL)
	        fprintf(fp, 
                        "\nSTART OF CARRIER NUMBER: %s \n",
			sqlGetString(res,row,"car_move_id")); 

	
	    /* Select all of the Shipments for carrier move */
	    sprintf(sqlBuffer,
	            "select shipment.ship_id "
	            "  from shipment, stop, car_move "
	            " where car_move.car_move_id = '%s' "
	            "   and car_move.car_move_id = stop.car_move_id "
	            "   and shipment.stop_id     = stop.stop_id ",
	            sqlGetString(res,row,"car_move_id"));
    
	    ret_status = sqlExecStr(sqlBuffer, &shpres);
	    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	    {
	        ret_data = srvResults (ret_status, &shpres);
	        goto CLEANUP_RETURN;
	    }
	    else
	        s_cnt = sqlGetNumRows(shpres);
    
	    for (shprow = sqlGetRow(shpres); 
                 shprow; 
                 shprow = sqlGetNextRow(shprow))
	    {
	        /*
	         * And now, process everything for the shipment.
	         */

	        ret_status = sProcessShipment(sqlGetString(shpres, 
                                                           shprow, 
                                                           "ship_id"), mode, fp);
	        if (ret_status != eOK)
		    goto CLEANUP_RETURN;
	    }    

    	    sqlFreeResults (shpres);
    	    shpres = NULL;
	    if (fp != NULL)
	    {
	        fprintf(fp,
		        "  car_move_id='%s', ship_cnt=%ld, ship_ins_cnt=%ld, \n"
		        "    ship_line_cnt=%ld ",
		        sqlGetString(res, row, "car_move_id"),
		        s_cnt, s_ins_cnt, 
		        sl_cnt);
	        fflush(fp);
	    }

	    /* Delete all of the Stops for carrier move */
	    sprintf(sqlBuffer,
	           "delete from stop "
	           " where stop.car_move_id = '%s' ",
	            sqlGetString(res,row,"car_move_id"));
	    ret_status = sqlExecStr(sqlBuffer, NULL);
	    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	    {
	        ret_data = srvResults (ret_status, NULL);
	        goto CLEANUP_RETURN;
	    }

	    /* Delete the Trailer records if any exist */
	    if (misTrimLen(sqlGetString(res, row, "trlr_id"), TRLR_ID_LEN) > 0)
	    {
	        misTrimcpy(trlr_id, 
                           sqlGetString(res, row, "trlr_id"), TRLR_ID_LEN);
    
	        /* Delete the storage location (created using trlr_id) */
	        sprintf(sqlBuffer,
	                "delete from locmst "
		        " where stoloc = '%s'",
		        trlr_id);
	        ret_status = sqlExecStr(sqlBuffer, NULL);
	        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	        {
	            ret_data = srvResults (ret_status, NULL);
	            goto CLEANUP_RETURN;
	        }

	        /* Delete the trailer notes */
	        sprintf(sqlBuffer,
	                "delete from trlr_note "
		        " where trlr_id = '%s'",
		        trlr_id);
	        ret_status = sqlExecStr(sqlBuffer, NULL);
	        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	        {
	            ret_data = srvResults (ret_status, NULL);
	            goto CLEANUP_RETURN;
	        }

	        /* Delete the trailer records */
	        sprintf(sqlBuffer,
	                "delete from trlr "
		        " where trlr_id = '%s'",
		        trlr_id);
	        ret_status = sqlExecStr(sqlBuffer, &cntres);
	        if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	        {
	            ret_data = srvResults (ret_status, NULL);
	            goto CLEANUP_RETURN;
	        }
	        if ((ret_status == eOK) && (cntres))
	            trl_cnt=sqlGetNumRows(cntres);
	        else
	            trl_cnt=0;
    	        sqlFreeResults (cntres);
    	        cntres = NULL;
	    }

	    /* Delete the carrier move */ 
	    sprintf(sqlBuffer,
	           "delete from car_move "
	           " where car_move.car_move_id = '%s' ",
	           sqlGetString(res,row,"car_move_id"));

	    ret_status = sqlExecStr(sqlBuffer, NULL);
	    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED)
	    {
	        ret_data = srvResults (ret_status, NULL);
	        goto CLEANUP_RETURN;
	    }
	
	    if (fp != NULL)
	    {
	        fprintf(fp,
		        "car_mov_cnt=%ld, \n"
		        "    trl_cnt=%ld \n", 
		        NumCarMoves, 
		        trl_cnt);
	        fflush(fp);
	    }

	    /* Commit after every 20 carrier moves */
	
	    NumPurged++;
	    if (NumPurged / 20 * 20 == NumPurged)
	    {
	        ret_status = sqlCommit();
	        if (ret_status != eOK)
	        {
	            ret_data = srvResults (ret_status, NULL);
	            goto CLEANUP_RETURN;
	        }
	    }
	
	    /* Stop purging if max purge reached */
            if (maxpur_i)
                if (NumPurged >= MaxPurge)
	            break;
        }
    }

    /*
     * OK, now we need to take care of purging shipments that may
     * have been cancelled, but never got assigned to a carrier move.
     */

    if (mode == MODE_CANCELED || mode == MODE_ALL)
    {
        if (!ship_id_i)
        {
            sprintf(sqlBuffer,
	            " select ship_id"
	            "  from shipment "
	            " where shpsts = '%s' "
	            "   and loddte < sysdate - %d "
	            "   and stop_id   is null ",
	            SHPSTS_CANCELLED, 
	            dayold);
        }
        else
        {
            sprintf(sqlBuffer,
	            " select ship_id"
	            "  from shipment "
	            " where shpsts = '%s' "
	            "   and ship_id = '%s' "
	            "   and stop_id   is null ",
	            SHPSTS_CANCELLED, 
	            ship_id_i);
        }
    
        ret_status = sqlExecStr(sqlBuffer, &shpres);
        if ((eOK != ret_status) && (ret_status != eDB_NO_ROWS_AFFECTED))
        {
            ret_data = srvResults (ret_status, NULL);
            goto CLEANUP_RETURN;
        }

        for (shprow = sqlGetRow(shpres); shprow; shprow = sqlGetNextRow(shprow))
        {
            /*
	     * Remove the shipment
	     */

            ret_status = sProcessShipment(sqlGetString(shpres, 
                                                       shprow, 
                                                       "ship_id"), mode,  fp);
	    if (ret_status != eOK)
            {
	        ret_data = srvResults(ret_status, NULL);
	        goto CLEANUP_RETURN;
            }
        }
    }

    if (NumPurged)
    {
	ret_status = sqlCommit();
	if (ret_status != eOK)
	{
	    if (fp)
		fclose(fp);
	    return (srvResults(ret_status, NULL));
	}
    }

    ret_data = srvResults(eOK, 
	                  "numava", COMTYP_INT, sizeof(long), NumCarMoves,
			  "num_shipments", COMTYP_INT, sizeof(long), s_cnt,
	                  "numpur", COMTYP_INT, sizeof(long), NumPurged,
	                  "filnam", COMTYP_CHAR, strlen(filename), filename,
			  NULL);

CLEANUP_RETURN:
    if (fp != NULL)
	fclose(fp);

    if (res) sqlFreeResults (res);
    if (shpres) sqlFreeResults (shpres);
    
    misTrc(T_FLOW, "Exiting varPurgeShipments");
    return (ret_data);
}
