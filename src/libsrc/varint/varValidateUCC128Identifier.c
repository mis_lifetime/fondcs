static const char *rcsid = "$Id: varValidateUCC128Identifier.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>
#include <dcserr.h>
#include <varerr.h>
#include <common.h>

LIBEXPORT 
RETURN_STRUCT *varValidateUCC128Identifier(char *subucc_i)
{
    long return_status;
    RETURN_STRUCT *CurPtr;
    long  ret_status ;
    char buffer[2000];
    int i;
    int sum1 = 0; 
    int sum2 = 0;
    int totsum = 0;
    int chksum = 0;
    int CheckSum;
    char TempString[3];
    char  subucc[UCCCOD_LEN+1];

    mocaDataRes   *res;

    misTrimcpy(subucc, subucc_i, UCCCOD_LEN );

    sprintf( buffer,
	    " select 'check-on'  "
	    "   from poldat  "
	    "  where polcod = 'VAR' " 
	    "    and polvar = 'LOADNUM-CHECK' " 
	    "    and polval = 'ON-OR-OFF' "
	    "    and rtstr1 = 'ON' ");

    ret_status = sqlExecStr(buffer, &res) ;
    
    if (ret_status !=eOK) 
    {
	sqlFreeResults(res);
	return (srvSetupReturn(eOK,""));
    }
    sqlFreeResults(res);

    /* SUBUCC should always be 20 characters */
    if (strlen(subucc) != UCCCOD_LEN) 
    {
	return (srvSetupReturn(eVAR_INVALID_UCC ,"")) ;
    }


    TempString[0] = subucc[19] ;
    
    CheckSum = atoi(TempString) ;

    /* Here is an example...
       uccnum = 01234567890
       Step 1) Starting at position 1, add up the numbers in the odd
       numbered positions.  0+2+4+6+8+0 = 20
       Step 2) Multiply the result by 3. 20*3 = 60
       Step 3) Starting and position 2, add up the numbers in the even
       numbered positions. 1+3+5+7+9 = 25
       Step 4) Add the results of step 2 and 3. 60 + 25 = 85
       Step 5) The check digit is the smallest number, when added to the 
       value obtained in Step 4 provides a number which is a multiple
       of 10.  85 + ? = 90.
       Step 6) The moculo 1- check digit for this uccnum is 5. */


    /* Step 1 */
    memset(TempString, 0, sizeof(TempString));
    for (i = 0; i < ((int)strlen(subucc) -1); i = i + 2)
    {
	TempString[0] = subucc[i] ;
	sum1 = sum1 + atoi(TempString);
    }

    /* Step 2 */
    sum1 = sum1 * 3;

    /* Step 3 */
    for (i = 1; i < ((int)strlen(subucc) -1) ; i = i + 2)
    {
	TempString[0] = subucc[i] ;
	sum2 = sum2 + atoi(TempString);
    }

    /* Step 4 */
    totsum = sum1 + sum2;

    /* Step 5 */
    chksum = (10 - (totsum % 10)) % 10;
    
    if (chksum != CheckSum) 
    {
	return (srvSetupReturn(eVAR_INVALID_UCC ,"")) ;
    }

    return (srvSetupReturn(eOK,""));
}
