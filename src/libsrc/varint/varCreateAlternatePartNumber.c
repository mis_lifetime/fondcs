static const char *rcsid = "$Id: varCreateAlternatePartNumber.c,v 1.3 2003/12/03 06:17:44 prod Exp $";
/*#START*************************************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 * DESCRIPTION  :  This functin generates a columnlist and valuelist insert 
 *                 a record in the var_ALT_PRTNUM table . Actual Insert is done 
 *                 by Process Table Action (MCS Base Component ) 
 *#END***************************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <applib.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <dcserr.h>



LIBEXPORT 
RETURN_STRUCT *varCreateAlternatePartNumber(char *altnum_i,
                                            char *altlot_i,
				            char *prtnum_i)
{
    RETURN_STRUCT *CurPtr;
    char  tablename[100];
    char  tmpvar[1000];
    char  *columnlist;
    char  *valuelist;
    char  altnum[PRTNUM_LEN+1] ;
    char  prtnum[PRTNUM_LEN+1] ;
    char  altlot[LOTNUM_LEN+1] ;
    long  ret_status;
 
        
    CurPtr = NULL;

    if (!altnum_i )
    	return (APPMissingArg("altnum"));
    
    misTrimcpy(altnum, altnum_i, PRTNUM_LEN );

    if (!prtnum_i )
    	return (APPMissingArg("prtnum"));
    
    misTrimcpy(prtnum, prtnum_i, PRTNUM_LEN );


    if (!altlot_i )
    	return (APPMissingArg("altlot"));
    
    misTrimcpy(altlot, altlot_i, LOTNUM_LEN );
    /* Build the insert list */

    strcpy(tablename, "var_alt_prtnum");
    columnlist = valuelist = NULL;
    
    sprintf(tmpvar, "%s",altnum) ;

    if (eOK != appBuildInsertList("altnum",tmpvar,PRTNUM_LEN , 
			          &columnlist, &valuelist))
    {
       if (columnlist) free(columnlist);
       if (valuelist) free(valuelist);
       return (srvSetupReturn(eNO_MEMORY, ""));
    }
    
    sprintf(tmpvar, "%s",altlot) ;

    if (eOK != appBuildInsertList("vc_altlot",tmpvar,LOTNUM_LEN, 
			          &columnlist, &valuelist))
    {
       if (columnlist) free(columnlist);
       if (valuelist) free(valuelist);
       return (srvSetupReturn(eNO_MEMORY, ""));
    }

    sprintf(tmpvar, "%s",prtnum) ;

    if (eOK != appBuildInsertList("prtnum",tmpvar,PRTNUM_LEN, 
			          &columnlist, &valuelist))
    {
       if (columnlist) free(columnlist);
       if (valuelist) free(valuelist);
       return (srvSetupReturn(eNO_MEMORY, ""));
    }
    
    /* Publish out the table, columns, and values for insert by */
    /* intProcessTableInsert (PROCESS TABLE INSERT) */

    CurPtr = srvResultsInit(eOK,
		            "acttyp", COMTYP_CHAR, 1,
			    "tblnam", COMTYP_CHAR, strlen(tablename),
			    "collst", COMTYP_CHAR, strlen(columnlist),
			    "vallst", COMTYP_CHAR, strlen(valuelist),
			    NULL);

    srvResultsAdd(CurPtr,
		  ACTTYP_INSERT,
		  tablename,
		  columnlist,
		  valuelist);

    free(columnlist);
    free(valuelist);

    return (CurPtr);
}

