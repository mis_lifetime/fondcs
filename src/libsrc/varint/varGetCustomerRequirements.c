static const char *rcsid = "$Id: varGetCustomerRequirements.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varint/varGetCustomerRequirements.c,v $
 *  $Revision: 1.1.1.1 $
 *
 *  Application: intGet.c
 *  Created: 03-MAY-1994
 *  $Author: lh51sh $
 *
 *#END************************************************************************/

#include <moca_app.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <applib.h>
#include <dcslib.h>
#include <varerr.h>
#include <common.h>
#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>
#include <srvconst.h>



LIBEXPORT 
RETURN_STRUCT *varGetCustomerRequirements(char *btcust_i,
				  	  char *rtcust_i,
				  	  char *stcust_i,
				  	  char *prtnum_i,
				  	  char *prt_client_id_i,
				  	  char *client_id_i,
				  	  char *ordnum_i)
{
    char btcust[ BTCUST_LEN+1 ];
    char stcust[ STCUST_LEN+1 ];
    char rtcust[ RTCUST_LEN+1 ];
    char part_num[ PRTNUM_LEN+1 ];
    char prt_client_id[ CLIENT_ID_LEN+1 ];
    char client_id[ CLIENT_ID_LEN+1 ];
    char ordnum[ ORDNUM_LEN+1 ];

    long ret_status;
    mocaDataRes *res;
    mocaDataRow *row;

    char buffer[2000];
    RETURN_STRUCT *CurPtr;

    moca_bool_t carflg = BOOLEAN_NOTSET;
    moca_bool_t bckflg = BOOLEAN_NOTSET;
    moca_bool_t splflg = BOOLEAN_NOTSET; 
    moca_bool_t stdflg = BOOLEAN_NOTSET;
    moca_bool_t parflg = BOOLEAN_NOTSET;
    char shipby[SHIPBY_LEN+1]; 
    char manfid[MANFID_LEN+1]; 
    char deptno[DEPTNO_LEN+1]; 
    char cargrp[CARGRP_LEN+1];
    char cstprt[CSTPRT_LEN+1];
    char vc_lotnum[LOTNUM_LEN+1];
    char tmp_manfid[MANFID_LEN+1]; 
    char tmp_deptno[DEPTNO_LEN+1]; 
    long untcas, untpak, untpal;
    long check_order=0;

    char hdrcst[ BTCUST_LEN+1 ];
    char prtcst[ BTCUST_LEN+1 ];

    static char customer1[20]= "^";
    static char customer2[20]= "";
    static char customer3[20]= "";
    char customer[20];

    char customer1_cus[ STCUST_LEN +1];
    char customer2_cus[ STCUST_LEN +1];
    char customer3_cus[ STCUST_LEN +1];

    memset( btcust, 0, sizeof( btcust ) );
    memset( stcust, 0, sizeof( stcust ) );
    memset( rtcust, 0, sizeof( rtcust ) );
    memset( part_num, 0, sizeof(part_num));
    memset( prt_client_id, 0, sizeof(prt_client_id));
    memset( client_id, 0, sizeof(client_id));
    memset( ordnum, 0, sizeof( ordnum ) );

    memset(hdrcst, 0, sizeof(hdrcst));
    memset(prtcst, 0, sizeof(prtcst));
    memset( shipby, 0, sizeof( shipby ) );
    memset( manfid, 0, sizeof( manfid ) );
    memset( deptno, 0, sizeof( deptno ) );
    memset( cargrp, 0, sizeof( cargrp ) );
    memset( cstprt, 0, sizeof( cstprt ) );
    memset( vc_lotnum, 0, sizeof(vc_lotnum));    
    memset( customer, 0, sizeof( customer ) );
    memset(customer1_cus, 0, sizeof(customer1_cus));
    memset(customer2_cus, 0, sizeof(customer2_cus));
    memset(customer3_cus, 0, sizeof(customer3_cus));

    untcas = 0;
    untpak = 0;
    untpal = 0;

    res  = NULL;

    if (btcust_i  && misTrimLen(btcust_i, BTCUST_LEN))
	misTrimcpy(btcust, btcust_i, BTCUST_LEN);

    if (rtcust_i  && misTrimLen(rtcust_i, RTCUST_LEN))
	misTrimcpy(rtcust, rtcust_i, RTCUST_LEN);

    if (stcust_i  && misTrimLen(stcust_i, STCUST_LEN))
	misTrimcpy(stcust, stcust_i, STCUST_LEN);

    if (client_id_i  && misTrimLen(client_id_i, CLIENT_ID_LEN))
        misTrimcpy(client_id, client_id_i, CLIENT_ID_LEN);

    if (ordnum_i  && misTrimLen(ordnum_i, ORDNUM_LEN))
        misTrimcpy(ordnum, ordnum_i, ORDNUM_LEN);
   
    if (strcmp(customer1,"^")==0 ) 
    { 
        /*  We need to find out if a policy exists, which will determine if
        **  we need to check the customer part requirements table for a lotnum.
        */
    	sprintf(buffer,
		"select rtstr1 "
		"  from poldat "
		" where polcod = 'CUSTOMER-REQUIREMNT-PROCESSING'"
		"   and polvar = 'PREFERENCE'"
		"   and polval = 'SORT-ORDER' order by srtseq");
    	ret_status = sqlExecStr(buffer, &res);

    	if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED) 
	{
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,""));
        }
        else if (ret_status != eOK )
        {
	    /*  If the policy does not exist, default the order.  */
	    strncpy(customer1, "stcust", 6);
	    strncpy(customer2, "rtcust", 6);
	    strncpy(customer3, "btcust", 6);
        }
        else
        {
	    row = sqlGetRow(res);
	    strncpy(customer1, sqlGetString(res, row, "rtstr1"), 6);

	    row = sqlGetNextRow(row);
	    strncpy(customer2, sqlGetString(res, row, "rtstr1"), 6);

	    row = sqlGetNextRow(row);
	    strncpy(customer3, sqlGetString(res, row, "rtstr1"), 6);
	    misTrc(T_FLOW, 
		   " cust order is: %s; %s; %s)", 
		   customer1, customer2, customer3);
        }

        sqlFreeResults(res);
    }

    /*  If there are no customer numbers, but there is an ordnum, 
     *  client_id, etc
     *  Query the ord table for the customer numbers and re-verify that
     *  the customer information exists.
    */
     
    if ((misTrimLen(stcust, STCUST_LEN) == 0) &&
        (misTrimLen(rtcust, RTCUST_LEN) == 0) &&
	(misTrimLen(btcust, BTCUST_LEN) == 0))
    {
	if ((misTrimLen(ordnum, ORDNUM_LEN) > 0)       &&
	    (misTrimLen(client_id, CLIENT_ID_LEN) > 0))
	{
	    check_order = 1;
	    sprintf(buffer,
		    "select btcust, stcust, rtcust "
		    "  from ord "
		    " where ordnum    = '%s' "
		    "   and client_id = '%s' ",
		    ordnum,
		    client_id);
	}
	else
	{
	    return (srvSetupReturn(eINVALID_ARGS, ""));
	}
    }


    if ( check_order )
    {
	ret_status = sqlExecStr(buffer, &res);

	if (ret_status != eOK )
	{
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,""));
	}

	/*  Gather the customer information and move on.  */

	row = sqlGetRow(res);
	strncpy(btcust, sqlGetString(res, row, "btcust"), BTCUST_LEN);
	strncpy(stcust, sqlGetString(res, row, "stcust"), STCUST_LEN);
	strncpy(rtcust, sqlGetString(res, row, "rtcust"), RTCUST_LEN);
	sqlFreeResults(res);
	res = NULL;
    }

    /*  Search for the customers based on the policy.  If the policy is 
    **  incorrect do not search for bogus information.
    */
    if (misCiStrncmp(customer1, "stcust", 6) == 0) 
	strncpy (customer1_cus, stcust, STCUST_LEN);
    else if (misCiStrncmp(customer1, "rtcust", 6) == 0) 
	strncpy (customer1_cus, rtcust, RTCUST_LEN);
    else if (misCiStrncmp(customer1, "btcust", 6) == 0) 
	strncpy (customer1_cus, btcust, BTCUST_LEN);

    misTrc(T_FLOW, 
	   " customer1 is: %s; cust is: %s", customer1, customer1_cus);


    if (misCiStrncmp(customer2, "stcust", 6) == 0) 
	strncpy (customer2_cus, stcust, STCUST_LEN);
    else if (misCiStrncmp(customer2, "rtcust", 6) == 0) 
	strncpy (customer2_cus, rtcust, RTCUST_LEN);
    else if (misCiStrncmp(customer2, "btcust", 6) == 0) 
	strncpy (customer2_cus, btcust, BTCUST_LEN);

    if (misCiStrncmp(customer3, "stcust", 6) == 0) 
	strncpy (customer3_cus, stcust, STCUST_LEN);
    else if (misCiStrncmp(customer3, "rtcust", 6) == 0) 
	strncpy (customer3_cus, rtcust, RTCUST_LEN);
    else if (misCiStrncmp(customer3, "btcust", 6) == 0) 
	strncpy (customer3_cus, btcust, BTCUST_LEN);
    
    sprintf (buffer,
	     "  select '1', cstnum, bckflg, parflg, splflg,"
	     "         carflg, stdflg, shipby, cargrp, manfid, deptno "
	     "    from cstmst "
	     "   where cstnum = '%s' "
	     "     and client_id = '%s' "
	     "union all "
	     "  select '2', cstnum, bckflg, parflg, splflg, "
	     "         carflg, stdflg, shipby, cargrp, manfid, deptno "
	     "    from cstmst "
	     "    where cstnum = '%s' "
	     "      and client_id = '%s' "
	     "union all "
	     "  select '3', cstnum, bckflg, parflg, splflg, "
	     "         carflg, stdflg, shipby, cargrp, manfid, deptno "
	     "    from cstmst "
	     "   where cstnum = '%s' "
	     "     and client_id = '%s' ",
	     (customer1_cus ? customer1_cus : ""), client_id, 
	     (customer2_cus ? customer2_cus : ""), client_id, 
	     (customer3_cus ? customer3_cus : ""), client_id);
    
    ret_status = sqlExecStr(buffer, &res);
    /*  If there is nothing found we assume that there are no customer
     *  requirements and we move-on.  				*/

    if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED) 
    {
	sqlFreeResults(res);
	res = NULL;
	return (srvSetupReturn(ret_status,""));
    }
    else if ( ret_status == eOK ) 
    {
        for (row = sqlGetRow(res); row; row = sqlGetNextRow(row)) 
	{
	    if (bckflg == BOOLEAN_NOTSET)
		bckflg = sqlGetBoolean(res, row, "bckflg");
	    if (parflg == BOOLEAN_NOTSET)
		parflg = sqlGetBoolean(res, row, "parflg");
	    if (splflg == BOOLEAN_NOTSET)
		splflg = sqlGetBoolean(res, row, "splflg");
	    if (stdflg == BOOLEAN_NOTSET)
		stdflg = sqlGetBoolean(res, row, "stdflg");

	    if (!shipby || misTrimLen(shipby, SHIPBY_LEN) == 0)
		strncpy(shipby, sqlGetString(res, row, "shipby"), SHIPBY_LEN);

	    if (!cargrp || misTrimLen(cargrp, CARGRP_LEN) == 0)
		strncpy(cargrp, sqlGetString(res, row, "cargrp"), CARGRP_LEN);

	    if (carflg == BOOLEAN_NOTSET)
		carflg = sqlGetBoolean(res, row, "carflg");

	    if (!manfid || misTrimLen(manfid, MANFID_LEN) == 0) 
		strncpy(manfid, sqlGetString(res, row, "manfid"), MANFID_LEN);

	    if (!deptno || misTrimLen(deptno, DEPTNO_LEN) == 0)
		strncpy(deptno, sqlGetString(res, row, "deptno"), DEPTNO_LEN);
	    
	    if (!hdrcst || misTrimLen(hdrcst, BTCUST_LEN) == 0) 
		strncpy(hdrcst, sqlGetString(res, row, "cstnum"), BTCUST_LEN);
	}
    }
    sqlFreeResults(res);
    res = NULL;

    if ((prtnum_i && misTrimLen(prtnum_i, PRTNUM_LEN) > 0) &&
        (prt_client_id_i && misTrimLen(prt_client_id_i, CLIENT_ID_LEN) > 0))
    {

/*LHCSTART Paul W. 08/08/2001*/
/* add vc_lotnum */
	strncpy(part_num, prtnum_i, PRTNUM_LEN);
	strncpy(prt_client_id, prt_client_id_i, CLIENT_ID_LEN);
	sprintf (buffer,
		 "  select '1', cstnum, untcas, untpak, untpal, "
		 "         cstprt, manfid, deptno, vc_lotnum "
		 "    from cstprq "
		 "   where cstnum = '%s' "
		 "     and client_id = '%s' "
		 "     and prtnum = '%s' "
		 "     and prt_client_id = '%s' "
		 "union all "
		 "  select '2', cstnum, untcas, untpak, untpal, "
		 "         cstprt, manfid, deptno, vc_lotnum "
		 "    from cstprq "
		 "   where cstnum = '%s' "
		 "     and client_id = '%s' "
		 "     and prtnum = '%s' "
		 "     and prt_client_id = '%s' "
		 "union all "
		 "  select '3', cstnum, untcas, untpak, untpal,"
		 "         cstprt, manfid, deptno, vc_lotnum "
		 "   from cstprq "
		 "  where cstnum = '%s' "
		 "    and client_id = '%s' "
		 "    and prtnum = '%s' "
		 "    and prt_client_id = '%s' ",
		 (customer1_cus ? customer1_cus : ""), client_id, 
		 part_num, prt_client_id,
		 (customer2_cus ? customer2_cus : ""), client_id, 
		 part_num, prt_client_id,
		 (customer3_cus ? customer3_cus : ""), client_id, 
		 part_num, prt_client_id );
/*LHCEND Paul W. 08/08/2001*/ 
	ret_status = sqlExecStr(buffer, &res);
	
	/*  If there is nothing found we assume that there are
	 *  no customer requirements and we move-on.
	 */
	if (ret_status != eOK && ret_status != eDB_NO_ROWS_AFFECTED) 
	{
	    sqlFreeResults(res);
	    return (srvSetupReturn(ret_status,""));
	}
	else if ( ret_status == eOK ) 
	{
	    /*  Customer Part Requirements */
	    memset (tmp_manfid, 0, sizeof(tmp_manfid));
	    memset (tmp_deptno, 0, sizeof(tmp_deptno));
            for (row = sqlGetRow(res); row; row = sqlGetNextRow(row)) 
	    {
	  	if (!cstprt || misTrimLen(cstprt, CSTPRT_LEN) == 0)
		    strncpy(cstprt, 
			    sqlGetString(res, row, "cstprt"), CSTPRT_LEN);
 
        /*        if (!vc_lotnum || misTrimLen(vc_lotnum, LOTNUM_LEN) == 0) */
                    strncpy(vc_lotnum, 
                            sqlGetString(res, row, "vc_lotnum"), LOTNUM_LEN);

		/*  Since the manfid/deptno can come from the header or
		 *  the detail on a download, we must check both.  If 
		 *  the manfid/deptno is filled in on the cstreq but not
		 *  on the cstprq for that part we do not want to 
		 *  overwrite the information.  If the the cstprq is
		 *  filled in, we need to overwrite the cstreq.
		 */
	  	if (!tmp_manfid || misTrimLen(tmp_manfid, MANFID_LEN) == 0)
		    strncpy(tmp_manfid, 
			    sqlGetString(res, row, "manfid"), MANFID_LEN);

	  	if (!tmp_deptno || misTrimLen(tmp_deptno, DEPTNO_LEN) == 0)
		    strncpy(tmp_deptno, 
			    sqlGetString(res, row, "deptno"), DEPTNO_LEN);
		if (untcas == 0)
		    untcas = sqlGetLong(res, row, "untcas");
		if (untpak == 0)
		    untpak = sqlGetLong(res, row, "untpak");
		if (untpal == 0)
		    untpal = sqlGetLong(res, row, "untpal");

		/*  This field holds the first customer number that was
		 *  used to find the customer part requirements 
		 */
		if (!prtcst || misTrimLen(prtcst, BTCUST_LEN) == 0)
		    strncpy(prtcst,
			    sqlGetString(res, row, "cstnum"), BTCUST_LEN); 
	    }

	    /* conditionally overwrite customer requirements based on 
	     * existence of part requirements 
	     */
	    if (misTrimLen(tmp_manfid, MANFID_LEN) > 0)
		strncpy(manfid, tmp_manfid, MANFID_LEN);

	    if (misTrimLen(tmp_deptno, DEPTNO_LEN) > 0)
		strncpy(deptno, tmp_deptno, DEPTNO_LEN);
		    
	}

	sqlFreeResults(res); 
	res = NULL;
    }

    /* Now go and find the customer and part special
     * handling instructions and put them into the 
     * notes tables 
     */
    
    CurPtr = srvResultsInit(eOK,
	                    "client_id", COMTYP_CHAR, CLIENT_ID_LEN,
			    "hdrcst",    COMTYP_CHAR, BTCUST_LEN,
			    "prtcst",    COMTYP_CHAR, BTCUST_LEN,
			    "manfid",    COMTYP_CHAR, MANFID_LEN,
			    "deptno",    COMTYP_CHAR, DEPTNO_LEN,
			    "cargrp",    COMTYP_CHAR, CARGRP_LEN,
			    "shipby",    COMTYP_CHAR, SHIPBY_LEN,
			    "stdflg",    COMTYP_BOOLEAN,  sizeof(moca_bool_t),
			    "splflg",    COMTYP_BOOLEAN,  sizeof(moca_bool_t),
			    "parflg",    COMTYP_BOOLEAN,  sizeof(moca_bool_t),
			    "bckflg",    COMTYP_BOOLEAN , sizeof(moca_bool_t),
			    "carflg",    COMTYP_BOOLEAN,  sizeof(moca_bool_t),
			    "untcas",    COMTYP_INT,  sizeof(long),
			    "untpak",    COMTYP_INT,  sizeof(long),
			    "untpal",    COMTYP_INT,  sizeof(long),
			    "cstprt",    COMTYP_CHAR, CSTPRT_LEN,
/*LHCSTART Paul W. 08/08/2001*/ 
                            "vc_lotnum", COMTYP_CHAR, LOTNUM_LEN,
/*LHCSEND Paul W. 08/08/2001*/ 
			    NULL);
			    

    srvResultsAdd(CurPtr,
	          client_id,
		  hdrcst, prtcst,
		  manfid, deptno, cargrp,
		  shipby, stdflg, splflg, 
		  parflg, bckflg, carflg, untcas,
		  untpak, untpal, cstprt, vc_lotnum );

    return (CurPtr);

}
