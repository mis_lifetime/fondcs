static char *rcsid = "$Id: varConsolidateShipmentLinesByConstraints.c,v 1.4 2002/02/27 20:40:56 prod Exp $"; 
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 1999 McHugh Software International, Inc.
 *  Shelton, Connecticut,  U.S.A.
 *  All rights reserved.
 *
 *  Application:   tmint
 *  Created:	07/08/99
 *
 *#END************************************************************************/

#include <moca_app.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <limits.h>
#define TM_DEF_MAX_WEIGHT 40000
#define TM_MAX_DELTA_WEIGHT 1000 

#include <applib.h>

#include <dcserr.h>
#include <dcscolwid.h>
#include <dcsgendef.h>

typedef struct tm_row_struct {
  char ship_line_id[SHIP_LINE_ID_LEN + 1];
  long pckqty;
  long untcas;
  long untpak;
  moca_bool_t case_splflg;
  moca_bool_t pack_stdflg;

  }  TM_ROW_STRUCT;

typedef struct new_shipments {
  char ship_id[SHIP_ID_LEN+1];
  char old_ship_id[SHIP_ID_LEN+1];
  double curvol;
  double curwgt;
  struct new_shipments *next;
} NEW_SHIP_STRUCT;

static double def_max_delta_volume = -10.0;
static double def_max_volume = -10.0;
static double max_weight = -10.0;
static double delta_weight = -10.0;

static int var_SplitShipmentByVolume(char *ship_id, double max_volume, double shpvol, char *ordnum, 
	NEW_SHIP_STRUCT **NewShipments, double shpwgt);
static int var_ChangeShipmentByOrdnum(char *new_ship_id, char *ship_id,char *ordnum);
static int var_SplitShpwthByVolume(char *shpwth, 
			   char *ship_id, 
			   char *new_ship_id, 
			   double max_volume, 
			   double *shpwthvol,
			   double *shpvol,
			   double *new_shpvol,
			   long numrows,
			   TM_ROW_STRUCT *row_array,
			   mocaDataRes *res);

static int var_SplitLineByVolume(char *ship_line_id, 
			 char *ship_id, 
			 char *new_ship_id, 
			 double max_volume, 
			 double *linvol,
			 double *shpvol,
			 double *new_shpvol,
			 long *pckqty,
			 long untcas,
			 long untpak,
			 moca_bool_t case_splflg,
			 moca_bool_t pack_stdflg);

static long var_CreateNewLine(mocaDataRes *res, 
                     mocaDataRow *row, 
		     char *new_ship_id, 
		     long qty_fraction);

static long var_RemoveShipmentLine(char *ship_line_id);


static void sInitialize(void)
{
	long ret_status;
    char sqlBuffer[1000];
    mocaDataRes *res;
    mocaDataRow *row;

    if (def_max_delta_volume < 0)
    {
        /* it wasn't passed in, so try the policy */
    sprintf(sqlBuffer,  "select rtflt1 from poldat where polcod = 'VAR' and "
			"polvar = 'ORDER-CONSOLIDATION' and polval = 'MAX-DELTA-VOLUME'");

    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status == eOK)
		{
    	row = sqlGetRow(res);
		def_max_delta_volume = sqlGetFloat(res, row, "rtflt1");
		}
	else			
        {
            /* No policy either, use default */
        def_max_delta_volume = 500; 
        }
    }

    if (def_max_volume < 0)
    {
        /* it wasn't passed in, so try the policy */
    sprintf(sqlBuffer,  "select rtflt1 from poldat where polcod = 'VAR' and "                
                        "polvar = 'ORDER-CONSOLIDATION' and polval = 'MAX-VOLUME'");

    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status == eOK)
		{
    	row = sqlGetRow(res);
		def_max_volume = sqlGetLong(res, row, "rtflt1");
		}
	else			
        {
            /* No policy either, use default */
            def_max_volume = 1000000000; 
        }
    }
     sprintf(sqlBuffer,  "select rtflt1 from poldat where polcod = 'ORDER-CONSOLIDATION' and "
                           "polvar = 'DEFAULT' and polval = 'MAX-WEIGHT'");

       ret_status = sqlExecStr(sqlBuffer, &res);
       if (ret_status == eOK)
       {
           row = sqlGetRow(res);
           max_weight = sqlGetLong(res, row, "rtflt1");
       }
       else
       {
            max_weight = TM_DEF_MAX_WEIGHT;
       }

     sprintf(sqlBuffer,  "select rtflt1 from poldat where polcod = 'ORDER-CONSOLIDATION' and "
                           "polvar = 'DEFAULT' and polval = 'DELTA-WEIGHT'");

       ret_status = sqlExecStr(sqlBuffer, &res);
       if (ret_status == eOK)
       {
           row = sqlGetRow(res);
           delta_weight = sqlGetLong(res, row, "rtflt1");
       }
       else
       {
           delta_weight = TM_DEF_MAX_WEIGHT;
       }

}
/* Consoldiate and split shipment lines based on volume */
LIBEXPORT 
RETURN_STRUCT *varConsolidateShipmentLinesByConstraints(char *cons_batch_i, 
						  double *max_volume_i)
{
    int ret_status;
    char ship_id[SHIP_ID_LEN + 1];
    char ordnum[ORDNUM_LEN + 1];
    char cons_batch[CONS_BATCH_LEN + 1];
    char sqlBuffer[1000];
    double shpvol;
    double shpwgt;
    double max_volume;
    double ship_id_vol;
    double ship_id_wgt;
    mocaDataRes *res;
    mocaDataRow *row;
    int found;
    NEW_SHIP_STRUCT *NextShipment, *Last, *OldShipment = NULL,  *NewShipments = NULL;

    if (!cons_batch_i && !misTrimLen(cons_batch_i, CONS_BATCH_LEN))
        return (APPMissingArg("cons_batch"));

    sInitialize();


    /* assume each shipment will fit in truck */
    misTrimcpy(cons_batch, cons_batch_i, CONS_BATCH_LEN);
    sprintf(sqlBuffer,
            "select ship_id, ord.ordnum, vc_maxshpvol maxvol, "
			" sum((caswid*cashgt*caslen)*ceil((sl.pckqty / decode(ol.untcas,0,p.untcas,ol.untcas )))) shpvol, "
	    " sum(p.grswgt * sl.pckqty / p.untcas ) shpwgt "
            "  from shipment_line sl, ord_line ol, ord, cstmst c, prtmst p, ftpmst f  "
            " where sl.cons_batch = '%s'                                  "
            "   and sl.client_id = ol.client_id                           "
            "   and ord.client_id = ol.client_id                          "
            "   and sl.ordnum = ol.ordnum                                 "
            "   and ord.ordnum = ol.ordnum                                "
            "   and ord.btcust = c.cstnum                                 "
            "   and sl.ordlin = ol.ordlin                                 "
            "   and sl.ordsln = ol.ordsln                                 "
            "   and ol.prt_client_id = p.prt_client_id                    "
            "   and ol.prtnum = p.prtnum                                  "
            "   and p.ftpcod = f.ftpcod                                   "
            "group by ship_id, ord.ordnum, vc_maxshpvol                               ",
            cons_batch);

    ret_status = sqlExecStr(sqlBuffer, &res);
    if (ret_status != eOK)
    {
	sqlFreeResults(res);
        return (srvResults(ret_status, NULL));
    }


    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        /* only process those shipments that don't fit */

	memset(ship_id, 0, sizeof(ship_id));
 	strcpy(ship_id, sqlGetString(res, row, "ship_id"));
	shpvol = sqlGetFloat(res, row, "shpvol");
	shpwgt = sqlGetFloat(res, row, "shpwgt");
	misTrimcpy(ordnum, sqlGetString(res, row, "ordnum"), ORDNUM_LEN);

    max_volume = -1;

	if(!sqlIsNull(res,row,"maxvol")) 
		max_volume = (float) sqlGetLong(res, row, "maxvol");
	else
		if (max_volume_i && *max_volume_i > 0)
			max_volume = *max_volume_i;
		else
			max_volume = def_max_volume;

        misLogInfo(" MCF: ship_id: %s, Ord: %s max_vol = %f, max_wgt = %f", 
	ship_id, ordnum, 	max_volume, max_weight); 
    	if (!OldShipment)
	   {
	   OldShipment = calloc(1, sizeof(NEW_SHIP_STRUCT));
 	   strcpy(OldShipment->ship_id, sqlGetString(res, row, "ship_id"));
           misLogInfo(" MCF: Creating OldShipment Struct [%s]", OldShipment->ship_id ); 
	   OldShipment->curvol = shpvol;
	   }
	else
	   {
	   found =0;
	   NextShipment = OldShipment;
	   do
	     {
	      Last = NextShipment;
	      if (!strcmp(NextShipment->ship_id, ship_id))
		{
        	misLogInfo(" MCF: Found Usable NewShipment Struct "); 
		found = 1;
		ship_id_vol = NextShipment->curvol;
		ship_id_wgt = NextShipment->curwgt;
        	misLogInfo(" MCF: Found Usable NewShipment (%s), max ([%f],[%f]), Struct [%f], wgt [%f] ", ship_id, max_volume, max_weight,  ship_id_vol, ship_id_wgt); 
		found = 1;
		if ((max_volume + def_max_delta_volume < ship_id_vol + shpvol) ||( max_weight + delta_weight < ship_id_wgt + shpwgt))
			{
	    		/* this shipment exceeds the max, have to split it up. */

        	        misLogInfo(" MCF(main): Shipment (%s) Exceeds with [%f] and [%f] or [%f] and [%f]", ship_id, ship_id_vol,shpvol, ship_id_wgt, shpwgt); 
	    		misTrimcpy(ship_id, sqlGetString(res, row, "ship_id"), SHIP_ID_LEN);
	    		misTrimcpy(ordnum, sqlGetString(res, row, "ordnum"), ORDNUM_LEN);
	    		ret_status = var_SplitShipmentByVolume(ship_id, max_volume, shpvol, ordnum, 
				&NewShipments, shpwgt);
	    		if (ret_status != eOK)
	    		   {
				sqlFreeResults(res);
	       			return (srvResults(ret_status, NULL));
	    		   }
			}
			else
			    NextShipment->curvol += shpvol;
		}
 	     NextShipment = NextShipment->next;
	     } while (NextShipment && !found);	

	     if (!found)
		{
           	misLogInfo(" MCF: Creating New OldShipment Struct "); 
	   	NextShipment = calloc(1, sizeof(NEW_SHIP_STRUCT));
 	   	strcpy(NextShipment->ship_id, ship_id);
	   	NextShipment->curvol = shpvol;
	   	NextShipment->curwgt = shpwgt;
	   	Last->next = NextShipment;
	   	}
	   }

    }

    NextShipment = NewShipments;
    while (NextShipment)
       {
	Last = NextShipment;
	NextShipment=NextShipment->next;
	free(Last);
       }

    sqlFreeResults(res);
    return (srvResults(eOK, NULL));
}

static int var_SplitShipmentByVolume(char *ship_id, double max_volume, double shpvol, char *ordnum, 
	NEW_SHIP_STRUCT **NewShipments, double shpwgt)
{
    int ret_status;
    long numrows, i;
    long pckqty;
    long old_pckqty;
    long untcas;
    long untpak;
    double linvol;
    double new_shpvol;
    double shpwthvol;
    double old_shpwthvol;
#define SHPWTH_LEN 10
    char shpwth[SHPWTH_LEN + 1];
    char new_ship_id[SHIP_ID_LEN + 1];
    char ship_line_id[SHIP_LINE_ID_LEN + 1];
    char sqlBuffer[2000];
    moca_bool_t case_splflg;
    moca_bool_t pack_stdflg;
    mocaDataRes *res;
    mocaDataRow *row;
    mocaDataRes *linres;
    mocaDataRow *linrow;
    TM_ROW_STRUCT *row_array;
    NEW_SHIP_STRUCT *NextShipment, *Last;
    int found; 

    found = 0;
    NextShipment = *NewShipments;
    if (!NextShipment)
	{
        misLogInfo(" MCF: Creating NewShipment Struct "); 
	NextShipment = calloc(1, sizeof(NEW_SHIP_STRUCT));
        ret_status = appNextNum("ship_id", NextShipment->ship_id);
    	if (ret_status != eOK)
		return ret_status;

 	strcpy(new_ship_id, NextShipment->ship_id);	
 	strcpy(NextShipment->old_ship_id, ship_id);	
	NextShipment->curvol = shpvol;
	NextShipment->curwgt = shpwgt;
	*NewShipments = NextShipment;
	}
    else
      {
        do {

	    Last = NextShipment;
	    if (!strcmp(ship_id, NextShipment->old_ship_id) && (NextShipment->curvol + shpvol <= 
		max_volume + def_max_delta_volume) && 
			(NextShipment->curwgt + shpwgt <= max_weight + delta_weight))
		{
        	misLogInfo(" MCF: Found Usable NewShipment Struct - ship_id [%s], old [%s], new[%s] ", 
			ship_id, NextShipment->old_ship_id, NextShipment->ship_id ); 
 		strcpy(new_ship_id, NextShipment->ship_id);	
		found = 1;
		NextShipment->curvol += shpvol;
		NextShipment->curwgt += shpwgt;
		}

 	   NextShipment = NextShipment->next;
	} while (NextShipment && !found );

     if (!found)
	{
       	misLogInfo(" MCF: Adding New NewShipment Struct "); 
	NextShipment = calloc(1, sizeof(NEW_SHIP_STRUCT));
        ret_status = appNextNum("ship_id", NextShipment->ship_id);
    	if (ret_status != eOK)
		return ret_status;

 	strcpy(new_ship_id, NextShipment->ship_id);	
	strcpy(NextShipment->old_ship_id, ship_id);
	NextShipment->curvol = shpvol;
	NextShipment->curwgt = shpwgt;
	Last->next  = NextShipment;
	}	
      }


ret_status = var_ChangeShipmentByOrdnum (new_ship_id, ship_id, ordnum);
return (ret_status);
}

static int var_SplitShpwthByVolume(char *shpwth, 
			   char *ship_id, 
			   char *new_ship_id, 
			   double max_volume, 
			   double *shpwthvol,
			   double *shpvol,
			   double *new_shpvol,
			   long numrows,
			   TM_ROW_STRUCT *row_array,
			   mocaDataRes *res)
{
    int ret_status;
    long i;
    long qty_fraction;
    double vol_fraction;
    double vol_avail;
    double vol_ratio;
    mocaDataRow *row;

    vol_avail = max_volume - (*new_shpvol);

    if (vol_avail >= (*shpwthvol))
    {
	/* all lines in shpwth group fit in new shipment,
	 * just move them over to new shipment
	 */
	for (i = 0; i < numrows; i++)
	{
	    ret_status = var_ChangeShipmentOnShipmentLine(new_ship_id,
					     row_array[i].ship_line_id);
	    if (ret_status != eOK)
	        return (ret_status);

	    row_array[i].pckqty = 0;
	}

	(*new_shpvol) += (*shpwthvol);
	(*shpvol) -= (*shpwthvol);
	(*shpwthvol) = 0;
    }
    /*
    else
    if (vol_avail < def_max_delta_volume)
	{
	    ret_status = appNextNum("ship_id", new_ship_id);
	    if (ret_status != eOK)
		return ret_status;

	    *new_shpvol = 0;
	    vol_avail = max_volume;
	}
	if (vol_avail >= (*shpwthvol))
	{
	    for (i = 0; i < numrows; i++)
	    {
		ret_status = var_ChangeShipmentOnShipmentLine(new_ship_id,
						 row_array[i].ship_line_id);
		if (ret_status != eOK)
		    return (ret_status);

		row_array[i].pckqty = 0;
	    }

	    (*new_shpvol) += (*shpwthvol);
	    (*shpvol) -= (*shpwthvol);
	    (*shpwthvol) = 0;
	}
	/*
	else
	{
	    vol_ratio = vol_avail / (*shpwthvol);
	    for (i = 0; i < numrows; i++)
	    {

		qty_fraction = (long) floor(((double) row_array[i].pckqty) 
		                            * vol_ratio);

		if (row_array[i].case_splflg == BOOLEAN_FALSE)
		{
		    qty_fraction = (long) floor(qty_fraction / 
			                        row_array[i].untcas) * 
		                          row_array[i].untcas;
		}
		else if (row_array[i].pack_stdflg == BOOLEAN_TRUE)
		{
		    qty_fraction = (long) floor(qty_fraction / 
			                        row_array[i].untpak) * 
		                          row_array[i].untpak;
		}
		vol_ratio = MIN(vol_ratio, qty_fraction/row_array[i].pckqty);
	    }

	    if (vol_ratio > 0)
	    {
	        for (i = 0, row = sqlGetRow(res); 
		     row && i < numrows; 
		     row = sqlGetNextRow(row), i++)
		{
		    qty_fraction = (long) ceil(row_array[i].pckqty * 
			                       vol_ratio);

		    if (row_array[i].case_splflg == BOOLEAN_FALSE)
		    {
		        qty_fraction = (long) ceil(qty_fraction /
			                           row_array[i].untcas) * 
			                      row_array[i].untcas;
		    }
		    else if (row_array[i].pack_stdflg == BOOLEAN_TRUE)
		    {
		        qty_fraction = (long) ceil(qty_fraction /
			                           row_array[i].untpak) * 
			                      row_array[i].untpak;
		    }

		    ret_status = var_CreateNewLine(res, row, 
			                          new_ship_id, 
						  qty_fraction);
		    if (ret_status != eOK)
			return(ret_status);

		    vol_fraction = qty_fraction * ((*shpwthvol) / 
			                           (row_array[i].pckqty));

		    *new_shpvol += vol_fraction;
		    *shpwthvol -= vol_fraction;
		    *shpvol -= vol_fraction;
		    row_array[i].pckqty -= qty_fraction;
		}
	    }
	    else if (*new_shpvol == 0)
	    {
		return eINT_INVALID_SHIPMENT;
	    }
	    else
	    {
		ret_status = appNextNum("ship_id", new_ship_id);
		if (ret_status != eOK)
		    return ret_status;

		*new_shpvol = 0;
	    }
	} 
    if shpwth group doesn't fit in pre-existing shipment */

    return eOK;
}

static int var_SplitLineByVolume(char *ship_line_id, 
			 char *ship_id, 
			 char *new_ship_id, 
			 double max_volume, 
			 double *linvol,
			 double *shpvol,
			 double *new_shpvol,
			 long *pckqty,
			 long untcas,
			 long untpak,
			 moca_bool_t case_splflg,
			 moca_bool_t pack_stdflg)
{
    int ret_status;
    long qty_fraction;
    char sqlBuffer[1000];
    double vol_fraction;
    double vol_avail;
    double vol_ratio;
    mocaDataRes *res;
    mocaDataRow *row;

    vol_avail = max_volume - (*new_shpvol);
    // if (vol_avail >= (*linvol))

    if (1 != 1)
    {
        /* whole line fits, move whole quantity. */
        qty_fraction = (*pckqty);
    }
    else
    {
        /* Check if enough of the line will fit 
	 * to make the split worthwhile.
	 * Otherwise, just start a new shipment 
	 */
        if (vol_avail < def_max_delta_volume)
	{
	    ret_status = appNextNum("ship_id", new_ship_id);
	    if (ret_status != eOK)
		return ret_status;

	    *new_shpvol = 0;
	    vol_avail = max_volume;
	}

	/* We may have just changed the volume available.
	 * Check again if we can put the whole line in without changes
	 */
	if (1==1)
	{
	    /* whole line fits, move whole quantity. */
	    qty_fraction = (*pckqty);
	}
	else
	{
	    vol_ratio = vol_avail / (*linvol);
	    qty_fraction = (long) floor(((double) *pckqty) * vol_ratio);

	    if (qty_fraction)
	    {
		/* part of the line will fit, 
		 * evaluate constraints to see how much
		 *
		 * Can we split a case?
		 * Can we split a package ?
		 */

		if (case_splflg == BOOLEAN_FALSE)
		{
		    /* Can not split cases */
		    qty_fraction = (long) floor(qty_fraction / untcas) * 
		                          untcas;
		}
		else if (pack_stdflg == BOOLEAN_TRUE)
		{
		    /* Must use standard packages */
		    qty_fraction = (long) floor(qty_fraction / untpak) * 
		                          untpak;
		}
	    }
	}
    }

    if (qty_fraction)
    {
        /* part of the line will fit, even after constraint checks 
	 * Make a new line out of this line
	 */

        sprintf(sqlBuffer, 
	        "select * from shipment_line where ship_line_id = '%s'",
		ship_line_id);

	ret_status = sqlExecStr(sqlBuffer, &res);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return (ret_status);
	}

	row = sqlGetRow(res);

        ret_status = var_CreateNewLine(res, row, new_ship_id, qty_fraction);
	if (ret_status != eOK)
	{
	    sqlFreeResults(res);
	    return(ret_status);
	}

        vol_fraction = qty_fraction * ((*linvol) / (*pckqty));

        *new_shpvol += vol_fraction;
	*linvol -= vol_fraction;
        *shpvol -= vol_fraction;
        *pckqty -= qty_fraction;

	sqlFreeResults(res);
    }
    else if (*new_shpvol == 0)
    {
        /* no fraction of the line will fit, 
	 * even though this is a new shipment.
	 * Therefore, bomb out.
	 */
        return eINT_INVALID_SHIPMENT;
    }
    else
    {
	ret_status = appNextNum("ship_id", new_ship_id);
	if (ret_status != eOK)
	    return ret_status;

	*new_shpvol = 0;
    }

    return eOK;
}

static long var_CreateNewLine(mocaDataRes *res, 
                      mocaDataRow *row, 
		      char *new_ship_id, 
		      long qty_fraction)
{
    long ret_status;
    char new_ship_line_id[SHIP_LINE_ID_LEN + 1];
    char new_values[100];

    ret_status = appNextNum("ship_line_id", new_ship_line_id);
    if (ret_status != eOK)
	return (ret_status);

    sprintf(new_values, 
	    "'%s','%s',%ld", 
	    new_ship_line_id,
	    new_ship_id, 
	    qty_fraction);

    misLogInfo(" DBG1: new_ship_line_id=%s, new_ship_id=%s ",
    		new_ship_line_id, new_ship_id);

    ret_status = appGenerateCommandWithRes("create shipment line",
					   res, row, 
					   "ship_line_id, ship_id, pckqty",
					   new_values, 
					   NULL); 

    return(ret_status);
}

static long var_RemoveShipmentLine(char *ship_line_id)
{
   RETURN_STRUCT *CurPtr = NULL; /* kjh Feb27 new for MR1810 */
   char buffer[2000];		 /* kjh Feb27 new for MR1810 */
   int ret_status;	         /* kjh Feb27 new for MR1810 */
/* kjh Feb27 orig is removed    char sqlBuffer[100]; */

/* kjh Feb27 below is orig and we need fix for MR1810
    sprintf(sqlBuffer, 
	    "remove shipment line where ship_line_id = '%s'",
	    ship_line_id);

    return (srvInitiateCommand(sqlBuffer, NULL));
   kjh Feb27 above is orig and we need fix for MR1810*/
/* kjh Feb27 below is replacement to fix MR1810 from tmRemoveShipmentLine.c */
    /*delete instructions */
    sprintf (buffer, "list shipment line instructions "
                     " where ship_line_id = '%s' | "
                     " remove special handling instructions ",
                     ship_line_id );

    ret_status = srvInitiateInline(buffer, &CurPtr);
    if (eOK != ret_status && eDB_NO_ROWS_AFFECTED != ret_status)
        return CurPtr;
    srvFreeMemory(SRVRET_STRUCT, CurPtr);

    /* delete the rows */
    sprintf(buffer,
            "delete shipment_line where ship_line_id = '%s' ",
            ship_line_id);
    ret_status = sqlExecStr(buffer, NULL);
    return (ret_status);
/* kjh Feb27 above is replacement to fix MR1810 */

}

static int var_ChangeShipmentOnShipmentLine(char *new_ship_id, char *ship_line_id)
{
    char sqlBuffer[1000];
    
    if (!strlen(ship_line_id))
       return eOK;

    sprintf(sqlBuffer,
            "update shipment_line "
            " set ship_id = '%s'  "
            "where ship_line_id = '%s' ",
            new_ship_id,
            ship_line_id);

    return (sqlExecStr(sqlBuffer, NULL));
}

static int var_ChangeShipmentByOrdnum(char *new_ship_id, char *ship_id,char *ordnum)
{
    char sqlBuffer[1000];
    
    sprintf(sqlBuffer,
            "update shipment_line "
            " set ship_id = '%s'  "
            "where ship_id = '%s' and ordnum = '%s' ",
            new_ship_id, ship_id, ordnum);

    return (sqlExecStr(sqlBuffer, NULL));
}
