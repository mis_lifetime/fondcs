static const char *rcsid = "$Id: varAutoFullLoad.c,v 1.1.1.1 2001/09/18 23:05:54 lh51sh Exp $";
/*#START***********************************************************************
 *  McHugh Software International
 *  Copyright 2001
 *  Waukesha, Wisconsin,  U.S.A.
 *  All rights reserved.
 *  DESCRIPTION  :  This functin generates a UCC cod for each un confirmed subload 
 *                  on the load no. passed , and prints label for all unconfirmed cases. 
 *
 *#END************************************************************************/

#include <moca_app.h>
#include <stdio.h>
#include <applib.h>
#include <dcsgendef.h>
#include <dcscolwid.h>
#include <dcserr.h>
#include <dcslib.h>
#include <varerr.h>
#include <common.h>
#include <stdlib.h>



LIBEXPORT 
RETURN_STRUCT  *varAutoFullLoad( char *laneloc_i ,
			   	 char *ship_id_i) 
{
    RETURN_STRUCT  *returnData=NULL  ;
    RETURN_STRUCT  *CurPtr = NULL ;

    char laneloc[STOLOC_LEN+1];
    char ship_id[SHIP_ID_LEN+1] ;
    
    char from_loc[STOLOC_LEN] = "";
    char to_loc[STOLOC_LEN] = "";
    
    mocaDataRow   *row ;
    mocaDataRes   *res ;

    char buffer[3500] ;
    char lodnum[LODNUM_LEN + 1] ;

    long ret_status ;
    
    
    misTrimcpy(ship_id, ship_id_i, LODNUM_LEN );
    misTrimcpy(laneloc, laneloc_i, STOLOC_LEN );
  
    /* get all the slots for that lane and ship_id  */ 

    sprintf(buffer,
	    "select distinct rtstr1 from_loc, rtstr2 to_loc, invlod.lodnum   " 
	    "  from poldat, invlod, invsub, invdtl, shipment_line "
	    " where poldat.polcod ='VAR' "
	    "   and poldat.polvar ='SHIP-STAGE-BUILD-LOAD' "
	    "   and poldat.polval ='%s'  "
	    "   and poldat.rtstr1 = invlod.stoloc "
	    "   and invlod.lodnum = invsub.lodnum "
	    "   and invsub.subnum = invdtl.subnum " 
	    "   and invdtl.ship_line_id = shipment_line.ship_line_id "
	    "   and shipment_line.ship_id = '%s' " , 
	    laneloc , 
	    ship_id) ;

    ret_status = sqlExecStr(buffer, &res) ;

    if (ret_status != eOK) 
    {
	sqlFreeResults(res);
	return (srvSetupReturn(ret_status,"")) ;
    }

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
	misTrimcpy (from_loc , sqlGetString( res, row, "from_loc"), STOLOC_LEN) ;
	strcpy (to_loc, sqlGetString( res, row, "to_loc"), STOLOC_LEN) ;
	strcpy (lodnum, sqlGetString( res, row, "lodnum")) ;


	sprintf(buffer,
		" move inventory where srclod = '%s' "
		"  and srcloc = '%s'  "
		"  and dstloc = '%s'  " ,
		lodnum , 
		from_loc , 
		to_loc );

	ret_status = srvInitiateCommand (buffer, &returnData);
	
	if (ret_status !=eOK) 
		    return (srvSetupReturn(ret_status,"")) ;


	/* 08/15/2001 Patrick Pape Start 
	 * MR 615 
	 * Lifetime does not want to create the work anymore to move the pallet
	 */
#if 0
	sprintf( buffer,
		 " create work where srcloc = '%s'  "
		 "    and oprcod = '%s'  "
	         "    and lodnum = '%s'  "
		 "    and dstloc = '%s'  ",
		 to_loc,
		 OPRCOD_PICK,
		 lodnum,
		 to_loc );

		ret_status = srvInitiateCommand (buffer, &returnData);
	
	if (ret_status !=eOK) 
		    return (srvSetupReturn(ret_status,"")) ;
#endif

	/* 08/15/2001 Patrick Pape Stop */



    }

    return (srvSetupReturn(ret_status,"")) ;
}







	

