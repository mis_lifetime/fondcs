static char    *rcsid = "$Id: varListStagedShipments.c,v 1.9 2003/08/21 21:40:07 prod Exp $";
/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/libsrc/varint/varListStagedShipments.c,v $
 *  $Revision: 1.9 $
 *
 *  Khairul Zainal 01/23/2002 MR1296, MR1671 - Added vc_extdte, early_shpdte, 
 *  late_shpdte, adrnam2 as additional output.
 *  Also added rtcust and cponum as additional input parameters and output.
 *  MR1713 - Wildcard operation for stcust.
 *
 *#END************************************************************************/
#include <moca_app.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <dcscolwid.h>
#include <dcsgendef.h>
#include <vargendef.h>
#include <dcserr.h>

//#include "intlib.h"
static RETURN_STRUCT *sSetupEmptyReturn(long ret_status)
{
    RETURN_STRUCT  *CmdRes;

    CmdRes = srvResultsInit(ret_status,
                            "stoloc",    COMTYP_CHAR, STOLOC_LEN,
                            "ship_id",   COMTYP_CHAR, SHIP_ID_LEN,
                            "stcust",    COMTYP_CHAR, STCUST_LEN,
                            "adrnam",    COMTYP_CHAR, ADRNAM_LEN,
                            "carcod",    COMTYP_CHAR, CARCOD_LEN,
                            "srvlvl",    COMTYP_CHAR, SRVLVL_LEN,
                            "stop_id",   COMTYP_CHAR, STOP_ID_LEN,
                            "shpsts",    COMTYP_CHAR, SHPSTS_LEN,
                            "carflg",    COMTYP_BOOLEAN, sizeof(long),
                            "client_id", COMTYP_CHAR, CLIENT_ID_LEN,
                            "ordnum",    COMTYP_CHAR, ORDNUM_LEN,
                            "car_move_id",    COMTYP_CHAR, CAR_MOVE_ID_LEN,
                            "trlr_num",    COMTYP_CHAR, TRLR_NUM_LEN,
                            "trlr_id",    COMTYP_CHAR, TRLR_ID_LEN,
//LHSTART-kzainal - initialize or set up empty return result.
			    "rtcust",    COMTYP_CHAR, RTCUST_LEN,
                            "adrnam2",    COMTYP_CHAR, ADRNAM_LEN,
			    "cponum",    COMTYP_CHAR, CPONUM_LEN,
			    "early_shpdte", COMTYP_CHAR, MOCA_STD_DATE_LEN+8,
			    "late_shpdte", COMTYP_CHAR, MOCA_STD_DATE_LEN+8,
			    "vc_extdte", COMTYP_CHAR, MOCA_STD_DATE_LEN+8,
//LHSTOP-kzainal 
                            NULL);

    return (CmdRes);

}

LIBEXPORT
RETURN_STRUCT *varListStagedShipments (char *dstare_i,
                                       char *stcust_i,
				       char *carcod_i,
				       char *srvlvl_i,
				       char *ship_id_i,
	 			       char *client_id_i,
				       char *ordnum_i,
				       char *list_type_i,
//LHSTART-kzainal - add input parameters.
 			               char *rtcust_i,
			               char *cponum_i)
//LHSTOP-kzainal 
{

    RETURN_STRUCT  *ret_data=NULL, *ArePtr=NULL;
    RETURN_STRUCT  *Cmdres;
    RETURN_STRUCT  *CurPtr;
    mocaDataRes	   *res, *res2, *res3, *res4;
    mocaDataRow	   *row, *row3, *row4;
    
    char    sqlBuffer[4000];
    char    dstare_string[100];
    char    arecod_list_string[200];
    char    stcust_string[100];
    char    ship_id_string[100];
    char    client_id_string[100];
    char    ordnum_string[100];
    char    carcod_string[100];
    char    srvlvl_string[100];
    char    arecod_list[1000];
    char    like_clause[500];
    char    list_type[100];
 
//LHSTART-kzainal - declare variable.
    char            rtcust_string[100];
    char            cponum_string[100];
//LHSTOP-kzainal 

    SRV_ARGSLIST    *args;
    char            name[ARGNAM_LEN + 1];
    char            dtype;
    void            *data;
    int             oper;
    char            tmpvar[100];
    
    char car_move_id[CAR_MOVE_ID_LEN+1];
    char trlr_num[TRLR_NUM_LEN+1];
    char trlr_id[TRLR_ID_LEN+1];

    int     ret_status;
    short   passed_in_ordinfo = FALSE;

    memset(sqlBuffer, 0, sizeof(sqlBuffer));
    memset(dstare_string, 0, sizeof(dstare_string));
    memset(stcust_string, 0, sizeof(stcust_string));
    memset(carcod_string, 0, sizeof(carcod_string));
    memset(srvlvl_string, 0, sizeof(srvlvl_string));
    memset(ship_id_string, 0, sizeof(ship_id_string));
    memset(client_id_string, 0, sizeof(client_id_string));
    memset(ordnum_string, 0, sizeof(ordnum_string));
    memset(list_type, 0, sizeof(list_type));
    memset(arecod_list_string, 0, sizeof(arecod_list_string));
    memset(like_clause, 0, sizeof(like_clause));

//LHSTART-kzainal - initialize variable.
    memset(rtcust_string, 0, sizeof(rtcust_string));
    memset(cponum_string, 0, sizeof(cponum_string));
//LHSTOP-kzainal

    if (dstare_i)
	sprintf (dstare_string, " and locmst.arecod = '%s' ", dstare_i);
    if (stcust_i)
    {
	sprintf (stcust_string, " and ord.stcust = '%s' ", stcust_i);
        passed_in_ordinfo = TRUE;
    }
    if (carcod_i)
	sprintf (carcod_string, " and shipment.carcod = '%s' ", carcod_i);
    if (srvlvl_i)
	sprintf (srvlvl_string, " and shipment.srvlvl = '%s' ", srvlvl_i);
    if (ship_id_i)
	sprintf (ship_id_string, " and shipment.ship_id = '%s' ", ship_id_i);
    if (client_id_i)
	sprintf (client_id_string, " and ord.client_id = '%s' ", client_id_i);
    if (ordnum_i)
    {
	sprintf (ordnum_string, " and ord.ordnum = '%s' ", ordnum_i);
        passed_in_ordinfo = TRUE;
    }
    if (list_type_i && misTrimLen(list_type_i, 100))
        misTrimcpy(list_type, list_type_i, 100);

//LHSTART-kzainal 
    if (rtcust_i)
    {
        sprintf(rtcust_string, " and ord.rtcust = '%s' ", rtcust_i);
        passed_in_ordinfo = TRUE;
    }
    /* MR 0000. Raman Parthasarathy. Change for Performance tuning */
    if (cponum_i)
    {
	sprintf(cponum_string, " and ord.cponum = '%s' ", cponum_i);
        passed_in_ordinfo = TRUE;
    }
//LHSTOP-kzainal
    
    /*
     * Since the above arguments will only get passed into the function if
     * the operator was an "=", see if the command had a like clause in the
     * where clause, if so, we'll want to use it.
     */

    args = NULL;
    while (eOK == srvEnumerateArgList(&args, name, &oper, &data, &dtype))
    {
        memset (tmpvar, 0, sizeof(tmpvar));

        /* If it's not an equal dsign */
        /* We will look for specific column names, because when creating the
         * where clause, we will have to specify a table name.
         */

        if (oper == OPR_LIKE)
        {
            if (!misCiStrcmp(name, "ship_id"))
            {
                sprintf(tmpvar, " and shipment.%s like '%s' ",
                                name, (char *)data);
                strcat(like_clause, tmpvar);
            }
            else if (!misCiStrcmp(name, "ordnum"))
            {
                sprintf(tmpvar, " and shipment_line.%s like '%s' ",
                                 name, (char *)data);
                strcat(like_clause, tmpvar);
            }
//LHSTART- kzainal - wildcard for rtcust, cponum.
	    else if (!misCiStrcmp(name, "rtcust"))
	    {
	        sprintf(tmpvar, " and ord.%s like '%s' ", 
			         name, (char *)data);
	        strcat(like_clause, tmpvar);
                passed_in_ordinfo = TRUE;
	    }
	    else if (!misCiStrcmp(name, "cponum"))
	    {
	        sprintf(tmpvar, " and ord.%s like '%s' ", 
			         name, (char *)data);
	        strcat(like_clause, tmpvar);
                /* MR 0000. Raman Parthasarathy. Change for Performance tuning */
                passed_in_ordinfo = TRUE;
	    }
//LHSTOP-kzainal 
//LHSTART- MR1713 - kzainal - wildcard for stcust.
	    else if (!misCiStrcmp(name, "stcust"))
	    {
	        sprintf(tmpvar, " and ord.%s like '%s' ", 
			         name, (char *)data);
	        strcat(like_clause, tmpvar);
                passed_in_ordinfo = TRUE;
	    }
//LHSTOP-kzainal 
        }
    }
    srvFreeArgList(args);
    ret_status = eOK;
    
    /* If we didn't get a specific arecod to look at, find out what areas
     * we're processing for and build an 'in' list.
     * The list type argument allows us to specify that we want to list
     * staged shipments for a specific group of areas.  For example, if
     * want to only list those used for Ship Dock Operations, we would set the
     * type = 'SHIP DOCK'.
     * If no area code was passed in, and no type was passed in, we'll
     * list released shipments for all staging areas.
     */
    
    ArePtr = NULL;
    memset (arecod_list, 0, sizeof(arecod_list));
    
    if (!dstare_i)
    {
        ArePtr = NULL;
        memset (arecod_list, 0, sizeof(arecod_list));
        misTrc(T_FLOW, "No area code passed, building a list of areas.");

        if (strlen(list_type))
        {
            if (strcmp(list_type, "SHIP DOCK") == 0)
            {
                misTrc(T_FLOW, 
		       "List staged shipments for staging areas only");
                sprintf (sqlBuffer,
                         "list shipment staging areas");
            }
        }
        else
        {
            sprintf(sqlBuffer,
                    "list areas where aremst.stgflg = 1");
        }

        ret_status = srvInitiateInline(sqlBuffer, &Cmdres);
	if (eOK != ret_status)
        {
            srvFreeMemory(SRVRET_STRUCT, Cmdres);
            CurPtr = sSetupEmptyReturn(ret_status);
            return (CurPtr);
        }
        res2 = srvGetResults(Cmdres);
        for (row = sqlGetRow(res2); row; row = sqlGetNextRow(row))
        {
            if (strlen(arecod_list))
                strcat(arecod_list, ",'");
            else
                strcpy(arecod_list, "'");
           strcat(arecod_list, sqlGetString (res2, row, "arecod"));
           strcat(arecod_list, "'");
       }
       srvFreeMemory(SRVRET_STRUCT, Cmdres);
       if (strlen(arecod_list))
       {
	   sprintf(arecod_list_string, 
		   "[select arecod from aremst where arecod in (%s)] |",
		   arecod_list);
       }
    }
	
    /* And now get the moves */
	    
    
    memset (sqlBuffer, 0, sizeof (sqlBuffer));
    /* MR 0000. Raman PArthasarathy. Changes for Performance tuning */
    if (passed_in_ordinfo == TRUE)
    {
       sprintf(sqlBuffer,
	    " %s "
	    "[select distinct invlod.stoloc stoloc, shipment.ship_id, "
	    "        ord.stcust, adrmst.adrnam, shipment.carcod, "
	    "        shipment.srvlvl, shipment.shpsts, "
	    "        ord.carflg, ord.client_id, ord.ordnum, "
//LHSTART-kzainal - add rtcust, ardnam2, cponum, vc_extdte, early_shpdte, late_shpdte to select.
//                  Also add ord_line, adrmst2, to from clause.
	    "        shipment.stop_id, "
            "        ord.rtcust, adrmst2.adrnam adrnam2, ord.cponum, "
            "        to_char(ord_line.early_shpdte, 'MM/DD/YYYY HH24\:MI\:SS') early_shpdte, "
            "        to_char(ord_line.late_shpdte, 'MM/DD/YYYY HH24\:MI\:SS') late_shpdte, "
            "        to_char(ord_line.vc_extdte, 'MM/DD/YYYY HH24\:MI\:SS') vc_extdte "
	    "   from adrmst, adrmst adrmst2,"
	    "        ord, ord_line, shipment_line, shipment, "
	    "        invdtl,invsub, invlod, locmst "
//	    "   from ord, "
//	    "        adrmst,  shipment, shipment_line, "
//	    "        invdtl,invsub, invlod, locmst "
//LHSTOP-kzainal 
	    "  where shipment.ship_id        = shipment_line.ship_id "
//LHSTART-kzainal - add adrnam2, vc_extdte, early_shpdte, late_shpdte to where clause. Also commented 2 lines below.
//	    "    and shipment_line.client_id = ord.client_id "
//	    "    and shipment_line.ordnum    = ord.ordnum "
 	    "    and ord.ordnum              = ord_line.ordnum "
	    "    and ord.client_id           = ord_line.client_id "
	    "    and ord_line.ordnum         = shipment_line.ordnum "
	    "    and ord_line.client_id      = shipment_line.client_id "
	    "    and ord_line.ordlin         = shipment_line.ordlin "
	    "    and ord_line.ordsln         = shipment_line.ordsln "
	    "    and ord.rt_adr_id           = adrmst2.adr_id "
//LHSTOP-kzainal 
	    "    and shipment.shpsts        in ('%s','%s','%s', '%s') "
	    "    and ord.st_adr_id           = adrmst.adr_id "
	    "    and invdtl.ship_line_id     = shipment_line.ship_line_id "
	    "    and invdtl.subnum           = invsub.subnum "
	    "    and invsub.lodnum           = invlod.lodnum "
	    "    and invlod.stoloc           = locmst.stoloc "
	    "    and locmst.arecod||'' = @arecod "
	    "    %s "
//LHSTART-kzainal - add rtcust and cponum parameter.
	    "    %s "
	    "    %s "
//LHSTOP-kzainal 
	    "    %s "
	    "    %s "
	    "    %s "
	    "    %s "
	    "    %s "
	    "    %s "
	    "    %s ] catch (-1403)",
	    /* "   order by invlod.stoloc, shipment.ship_id] catch (-1403)", */
	    arecod_list_string,
	    SHPSTS_STAGED, SHPSTS_PRINTED, SHPSTS_TRANSFER, SHPSTS_LOADING, 
	    like_clause,
//LHSTART-kzainal - add rtcust and cponum parameter.
	    rtcust_string,cponum_string,
//LHSTOP-kzainal 
	    dstare_string,stcust_string, carcod_string, srvlvl_string, 
	    ship_id_string, client_id_string, ordnum_string);
    }
    else
    {
       sprintf(sqlBuffer,
	    " %s "
	    "[select distinct invlod.stoloc stoloc, shipment.ship_id, "
	    "        ord.stcust, adrmst.adrnam, shipment.carcod, "
	    "        shipment.srvlvl, shipment.shpsts, "
	    "        ord.carflg, ord.client_id, ord.ordnum, "
//LHSTART-kzainal - add rtcust, ardnam2, cponum, vc_extdte, early_shpdte, late_shpdte to select.
//                  Also add ord_line, adrmst2, to from clause.
	    "        shipment.stop_id, "
            "        ord.rtcust, adrmst2.adrnam adrnam2, ord.cponum, "
            "        to_char(ord_line.early_shpdte, 'MM/DD/YYYY HH24\:MI\:SS') early_shpdte, "
            "        to_char(ord_line.late_shpdte, 'MM/DD/YYYY HH24\:MI\:SS') late_shpdte, "
            "        to_char(ord_line.vc_extdte, 'MM/DD/YYYY HH24\:MI\:SS') vc_extdte "
	    "   from adrmst, adrmst adrmst2,"
	    "        ord, ord_line, shipment_line, shipment, "
	    "        invdtl,invsub, invlod, locmst "
//	    "   from ord, "
//	    "        adrmst,  shipment, shipment_line, "
//	    "        invdtl,invsub, invlod, locmst "
//LHSTOP-kzainal 
	    "  where shipment.ship_id        = shipment_line.ship_id "
//LHSTART-kzainal - add adrnam2, vc_extdte, early_shpdte, late_shpdte to where clause. Also commented 2 lines below.
//	    "    and shipment_line.client_id = ord.client_id "
//	    "    and shipment_line.ordnum    = ord.ordnum "
 	    "    and ord.ordnum              = ord_line.ordnum "
	    "    and ord.client_id           = ord_line.client_id "
	    "    and ord_line.ordnum         = shipment_line.ordnum "
	    "    and ord_line.client_id      = shipment_line.client_id "
	    "    and ord_line.ordlin         = shipment_line.ordlin "
	    "    and ord_line.ordsln         = shipment_line.ordsln "
	    "    and ord.rt_adr_id           = adrmst2.adr_id "
//LHSTOP-kzainal 
	    "    and shipment.shpsts        in ('%s','%s','%s', '%s') "
	    "    and ord.st_adr_id           = adrmst.adr_id "
	    "    and invdtl.ship_line_id     = shipment_line.ship_line_id "
	    "    and invdtl.subnum           = invsub.subnum "
	    "    and invsub.lodnum           = invlod.lodnum "
	    "    and invlod.stoloc           = locmst.stoloc "
	    "    and locmst.arecod||'' = @arecod "
	    "    %s "
//LHSTART-kzainal - add rtcust and cponum parameter.
	    "    %s "
	    "    %s "
//LHSTOP-kzainal 
	    "    %s "
	    "    %s "
	    "    %s "
	    "    %s "
	    "    %s "
	    "    %s "
	    "    %s ] catch (-1403)",
	    /* "   order by invlod.stoloc, shipment.ship_id] catch (-1403)", */
	    arecod_list_string,
	    SHPSTS_STAGED, SHPSTS_PRINTED, SHPSTS_TRANSFER, SHPSTS_LOADING, 
	    like_clause,
//LHSTART-kzainal - add rtcust and cponum parameter.
	    rtcust_string,cponum_string,
//LHSTOP-kzainal 
	    dstare_string,stcust_string, carcod_string, srvlvl_string, 
	    ship_id_string, client_id_string, ordnum_string);
    }
    /* End MR 0000, Raman Parthasarathy. Chanegs for Perfomance tuning */
 	   
    ret_status = srvInitiateCommand(sqlBuffer, &Cmdres);
    CurPtr = sSetupEmptyReturn(ret_status);
    if (ret_status != eOK) 
    {
        srvFreeMemory(SRVRET_STRUCT, Cmdres);
        return (CurPtr);
    }
    res = srvGetResults(Cmdres);

    /* 
     * Now, for the rows retrieved, see if there is a stop and carrier
     * move associated.
     */

    for (row = sqlGetRow(res); row; row = sqlGetNextRow(row))
    {
        memset (car_move_id, 0, sizeof (car_move_id));
        memset (trlr_num, 0, sizeof (trlr_num));
        memset (trlr_id, 0, sizeof (trlr_id));

        if (misTrimLen(sqlGetString(res, row, "stop_id"), STOP_ID_LEN) > 0)
	{
        
	    memset (sqlBuffer, 0, sizeof (sqlBuffer));
            sprintf(sqlBuffer,
		    " select car_move.car_move_id, car_move.trlr_id "
		    "   from car_move, stop "
		    "  where stop.stop_id  = '%s' "
		    "    and stop.car_move_id = car_move.car_move_id (+) ",
		    sqlGetString(res,row,"stop_id"));
 	   
            ret_status = sqlExecStr(sqlBuffer, &res3);
            if (ret_status == eOK)
	    {
                row3 = sqlGetRow(res3);
                strcpy (car_move_id, sqlGetString(res3,row3,"car_move_id"));
                if (misTrimLen(sqlGetString(res3, 
					    row3, "trlr_id"), TRLR_ID_LEN) > 0)
	        {
                    strcpy (trlr_id, sqlGetString(res3,row3,"trlr_id"));
	            memset (sqlBuffer, 0, sizeof (sqlBuffer));
                    sprintf(sqlBuffer,
			    " select trlr_num "
			    "   from trlr "
			    "  where trlr.trlr_id  = '%s' ",
			    trlr_id);
 	   
                    ret_status = sqlExecStr(sqlBuffer, &res4);
		    if (ret_status == eOK)
		    {
                        row4 = sqlGetRow(res4);
                        strcpy (trlr_num, sqlGetString(res4,row4,"trlr_num"));
		    }
                }
	    }
        }
    /* publish all we have selected*/

        ret_status = srvResultsAdd(CurPtr,
				   sqlGetString(res,row,"stoloc"),  
				   sqlGetString(res,row,"ship_id"),  
				   sqlGetString(res,row,"stcust"), 
				   sqlGetString(res,row,"adrnam"),
				   sqlGetString(res,row,"carcod"),
				   sqlGetString(res,row,"srvlvl"),
				   sqlGetString(res,row,"stop_id"),
				   sqlGetString(res,row,"shpsts"),
				   sqlGetBoolean(res,row,"carflg"),
				   sqlGetString(res,row,"client_id"),
				   sqlGetString(res,row,"ordnum"),
				   car_move_id,
				   trlr_num,
				   trlr_id,
//LHSTART-kzainal - set return result.
				   sqlGetString(res,row,"rtcust"),
				   sqlGetString(res,row,"adrnam2"),
				   sqlGetString(res,row,"cponum"),
				   sqlGetValue(res,row,"early_shpdte"),
				   sqlGetValue(res,row,"late_shpdte"),
				   sqlGetValue(res,row,"vc_extdte"));
//LHSTOP-kzainal 
    }
    srvFreeMemory(SRVRET_STRUCT, Cmdres);
	
    return (CurPtr);
}


