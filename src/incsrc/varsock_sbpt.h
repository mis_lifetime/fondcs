/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/incsrc/varsock_sbpt.h,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: VARSOCK-level header file, specific to the Skill Bosch project.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END************************************************************************/

#ifndef VARSOCK_SBPT_H__
#define VARSOCK_SBPT_H__

/* This file contains the items specific to the TCP/IP communication protocol
 * used by Skill Bosch Project conveyor systems. */

/* These are the components of the message header. */
#define MAILBOX_NAME_LEN     31
#define DATA_LENGTH_LEN      3
#define NULL_CHARACTER_LEN   1
#define HEADER_LENGTH        35

/* other misc defines related to the protocol. */
#define ACK_NAK_MSG_LEN      2
#define ACK                  "A+"
#define ACK_STRING_LEN       (strlen(ACK))
#define NAK                  "N+"
#define NAK_STRING_LEN       (strlen(NAK))
#define TO_MAILBOX_NAME      "FROM_DCS"
#define PREFIX_LEN            35
#define SUFFIX_LEN            0

/* protocol does not allow for message length more than 0xfff */
#define DATA_PORTION_LEN     0xFFF

/* structure definitions used by this protocol. */
struct msg_header {
	char mailbox_name[MAILBOX_NAME_LEN];
	char len_str[DATA_LENGTH_LEN];
	char null_term;
	char data_portion[DATA_PORTION_LEN];
};

/* other global variables. */


#endif /* VARSOCK_SBPT_H__ */

