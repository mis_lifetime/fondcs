/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/incsrc/usrcolwid.h,v $
 *  $Revision: 1.4 $
 *  $Author: prod $
 *
 *  Description: User-level column width definitions.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END*************************************************************************/

#ifndef USRCOLWID_H
#define USRCOLWID_H

/* A */

/* B */

/* C */

/* D */

/* E */

/* F */

/* G */
#define GENTYP_LEN	10

/* H */

/* I */

/* J */

/* K */

/* L */

/* M */

/* N */

/* O */

/* P */

/* Q */

/* R */

/* S */

/* T */

/* U */

/* V */

/* W */

/* X */

/* Y */

/* Z */

#endif
