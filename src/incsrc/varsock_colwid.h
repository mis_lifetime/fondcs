/*#START***********************************************************************
 *
 *  $Source: /mnt/dc01/mchugh/prod/cvsroot/prod/les/src/incsrc/varsock_colwid.h,v $
 *  $Revision: 1.1.1.1 $
 *  $Author: lh51sh $
 *
 *  Description: VARSOCK-level column width definitions.
 *
 *  $McHugh_Copyright-Start$
 *
 *  Copyright (c) 1999
 *  McHugh Software International
 *  All Rights Reserved
 *
 *  This software is furnished under a corporate license for use on a
 *  single computer system and can be copied (with inclusion of the
 *  above copyright) only for use on such a system.
 *
 *  The information in this document is subject to change without notice
 *  and should not be construed as a commitment by McHugh Software
 *  International.
 *
 *  McHugh Software International assumes no responsibility for the use of
 *  the software described in this document on equipment which has not been
 *  supplied or approved by McHugh Software International.
 *
 *  $McHugh_Copyright-End$
 *
 *#END*************************************************************************/

#ifndef VARSOCK_COLWID_H__
#define VARSOCK_COLWID_H__

/* A */

/* B */

/* C */
#define COMMAND_LEN          200

/* D */
#define DATA_STRING_LEN      MAX_SOCKET_MSG_LEN

/* E */

/* F */

/* G */

/* H */
#define HOSTID_LEN            80

/* I */

/* J */

/* K */

/* L */

/* M */
#define MAX_SOCKET_MSG_LEN             5119            /* 5K - 1 */
#define MESSAGE_STRING_LEN     MAX_SOCKET_MSG_LEN
#define MAX_SOCKET_CONNECTIONS 20

/* N */

/* O */

/* P */

/* Q */

/* R */
#define RESPONSE_STRING_LEN MAX_SOCKET_MSG_LEN
#define RUN_TRAN_FLG_LEN       1

/* S */
#define SOCKET_TRACE_FILENAME_LEN         1024         /* 1k  */

/* T */

/* U */

/* V */

/* W */

/* X */

/* Y */

/* Z */

#endif /* VARSOCK_COLWID_H__ */
